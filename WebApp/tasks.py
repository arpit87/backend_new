from __future__ import absolute_import, unicode_literals
from celery import shared_task
from google.oauth2 import service_account
from google.auth.transport.requests import AuthorizedSession
from WebApp.models import AppTemplateDetails, WebAppDynamicLinkAnalytics
import logging
from Platform.uid_code_converter import decode_uidcode
celerylogger = logging.getLogger('celery')
from datetime import date, timedelta
from UrlShortner.models import URLDetails


@shared_task
def setwelcomelinkanalyticsatmidnight():
    allwebapp = AppTemplateDetails.objects.all()
    for thiswebapp in allwebapp:
        urlid = decode_uidcode(thiswebapp.hostshortlink.split('/')[-1])
        urldetails = URLDetails.objects.get(urlid=urlid)

        #one entry for each date for each app
        try:
            previousdayclickdata = WebAppDynamicLinkAnalytics.objects.get(created_on=(date.today()+timedelta(days=-1)), appdetails=thiswebapp.appdetails, event="WELCOMESMSLINKCLICK")
            clickstilllastday = previousdayclickdata.count
        except:
            clickstilllastday = 0
        try:
            todayclickdata = WebAppDynamicLinkAnalytics.objects.get(created_on=date.today(), appdetails=thiswebapp.appdetails, event="WELCOMESMSLINKCLICK")
            todayclickdata.count = urldetails.clicks-clickstilllastday
            todayclickdata.save()
        except:
            WebAppDynamicLinkAnalytics.objects.create(appdetails=thiswebapp.appdetails, count=urldetails.clicks-clickstilllastday,
                                                      platform="unknown", event="WELCOMESMSLINKCLICK")

@shared_task
def getdynamiclinkappanalytics():
    # dynamic link clicks
    # Specify the required scope
    scopes = [
        "https://www.googleapis.com/auth/firebase"
    ]

    # Authenticate a credential with the service account
    credentials = service_account.Credentials.from_service_account_file("digiclinics-servicekey.json", scopes=scopes)

    # Use the credentials object to authenticate a Requests session.
    authed_session = AuthorizedSession(credentials)

    allwebapp = AppTemplateDetails.objects.all()
    for thiswebapp in allwebapp:
        urlsafedynamiclink = thiswebapp.dynamiclink.replace(':','%3A').replace('/','%2F')
        fulllink = "https://firebasedynamiclinks.googleapis.com/v1/"+urlsafedynamiclink+"/linkStats?durationDays=1"
        response = authed_session.get(fulllink)
        try:
            resjson = response.json()
            linkEvetStatsArray = resjson['linkEventStats']
            for event in linkEvetStatsArray:
                    WebAppDynamicLinkAnalytics.objects.create(appdetails =thiswebapp.appdetails, count=int(event['count']), platform=event['platform'], event=event['event'])
        except Exception as e:
            print(str(e))
            celerylogger.error(str(e))
