from WebApp.models import AppTemplateDetails, WebAppDynamicLinkAnalytics
from django.contrib import admin


class AppTemplateDetailsModelAdmin(admin.ModelAdmin):
    list_display = ('appdetails','brandname','host','templatetype')

admin.site.register(AppTemplateDetails, AppTemplateDetailsModelAdmin)

class WebAppDynamicLinkAnalyticsModelAdmin(admin.ModelAdmin):
    list_display = ('created_on','appdetails','platform','count','event')

admin.site.register(WebAppDynamicLinkAnalytics, WebAppDynamicLinkAnalyticsModelAdmin)