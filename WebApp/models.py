from django.db import models
# Create your models here.
from Doctor.models import AppDetails


class AppTemplateDetails(models.Model):
    CLINIC = 1
    HOSPITAL = 2
    DEMO = 3
    TEMPLATE_TYPE = ((CLINIC, 'Clinic'), (HOSPITAL,'Hospital'), (DEMO,"Demo"),)
    appdetails = models.OneToOneField(AppDetails, on_delete=models.CASCADE, primary_key=True)
    host = models.CharField(unique=True, max_length=100, null=True)
    imagesstr = models.TextField(default="")
    logo =  models.CharField(max_length=200,null=True,blank=True)
    brandname =  models.CharField(max_length=20,null=True, blank=True)
    slogan =  models.CharField(max_length=200,default="Delivering best of care")
    templatetype = models.IntegerField(choices=TEMPLATE_TYPE, default=CLINIC)
    speciality = models.CharField(max_length=50, default="default")
    contactnumbers = models.CharField(max_length=50, default="")
    dynamiclink = models.CharField( max_length=100, null=True)
    hostshortlink = models.CharField( max_length=50, default="")
    adminemail = models.CharField( max_length=50, default="")


    def __str__(self):
      return '{}-{}'.format(self.brandname, self.host)

class WebAppDynamicLinkAnalytics(models.Model):
    created_on = models.DateField(auto_now_add=True)
    platform =  models.CharField(max_length=20)
    appdetails = models.ForeignKey(AppDetails,on_delete=models.CASCADE)
    count = models.IntegerField(default=0)
    event = models.CharField(max_length=20)