import logging
from django.core.exceptions import ObjectDoesNotExist

from Platform.uid_code_converter import encode_id
from UrlShortner.views import create_short_url
from backend_new import  config
from Platform import backendlogger
from django.http import HttpResponse
from WebApp.models import AppTemplateDetails
from django.views.decorators.csrf import csrf_exempt
import json
from backend_new.Constants import ServerConstants
import base64
requestlogger = logging.getLogger('apirequests')
from Platform import utils
from Platform.views import authenticateURL
from Doctor.models import AppDetails
from django.contrib.auth.models import Group, User
from Person.models import PersonDetails, AdminAppMapping, AnalystAppMapping
@csrf_exempt
def getAppTemplateDetails(request):
    data = json.loads(request.body)
    logmessage=[]

    kwargs = {}
    appdetails_req = None
    try:
        appuid_req = data[ServerConstants.APPUID]
        appdetails_req = AppDetails.objects.get(appuid=appuid_req)
        kwargs['appdetails'] = appdetails_req
    except KeyError:
        pass

    host_req= None
    try:
        host_req = data[ServerConstants.HOST]
        kwargs['host'] = host_req
    except KeyError:
        pass

    if not appdetails_req and not host_req:
        backendlogger.error('getAppTemplateFromHost', data, 'host/appid  not sent', logmessage)
        return HttpResponse("app/appid not found", status=402)

    try:
        apptemplatedetails = AppTemplateDetails.objects.get(**kwargs)
    except:
        if appdetails_req is not None:
            # here we create webapptemplate to cater to existing apps
            imagesstr = ",".join([
                'https://storage.googleapis.com/' + config.GCSAPPTEMPLATEIMAGESBUCKET + '/default/bg1.jpg',
                'https://storage.googleapis.com/' + config.GCSAPPTEMPLATEIMAGESBUCKET + '/default/bg2.jpg',
                'https://storage.googleapis.com/' + config.GCSAPPTEMPLATEIMAGESBUCKET + '/default/bg3.jpg',
                'https://storage.googleapis.com/' + config.GCSAPPTEMPLATEIMAGESBUCKET + '/default/bg4.jpg'])

            apptemplatedetails = AppTemplateDetails.objects.create(appdetails=appdetails_req,host=appdetails_req.applocation, imagesstr=imagesstr)
        else:
            backendlogger.error('getAppTemplateFromHost', data, 'no appid , apptemplate  autocreate error ', logmessage)
            return HttpResponse("no appid , apptemplate  autocreate error", status=404)

    jsondata = {
                     ServerConstants.APPUID:apptemplatedetails.appdetails.appuid,
                     ServerConstants.IMAGESSTR: apptemplatedetails.imagesstr,
                     ServerConstants.LOGO: apptemplatedetails.logo,
                     ServerConstants.BRANDNAME: apptemplatedetails.brandname,
                     ServerConstants.ISLICENSEVALID: apptemplatedetails.appdetails.islicensevalid(),
                     ServerConstants.HOST: apptemplatedetails.host,
                     ServerConstants.SLOGAN: apptemplatedetails.slogan,
                     ServerConstants.TEMPLATETYPE: apptemplatedetails.templatetype,
                     ServerConstants.SPECIALITY: apptemplatedetails.speciality,
                     ServerConstants.CONTACTNUMBERS: apptemplatedetails.contactnumbers,
                     ServerConstants.DYNAMICLINK: apptemplatedetails.dynamiclink,
                     ServerConstants.HOSTSHORTURL: apptemplatedetails.hostshortlink,
                     }
    jsonstr = json.dumps(jsondata)
    paramsencrypted = str(base64.urlsafe_b64encode((''.join(chr(ord(a) + 10) for a in jsonstr)).encode("utf-8")),"utf-8")
    httpoutput = utils.successJson({ServerConstants.APPTOKEN: paramsencrypted})
    backendlogger.info('getAppTemplateFromHost', data, httpoutput, logmessage)
    # requestlogger.info('getAppDetailsResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE,status=200)

@csrf_exempt
def createWebApp(request):
    data = json.loads(request.body)
    logmessage = []
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('createWebApp', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    backendlogger.info('createWebApp', data, {}, logmessage)
    # requestlogger.info('updateAppTemplateDetails;%s'%(str(data)))

    kwargs = {}

    try:
        app_loc_req = data[ServerConstants.APPLOCATION]
        kwargs['applocation'] = app_loc_req
    except KeyError:
        pass

    kwargs['apptype'] = "webapp"

    kwargs['appdescription'] = "client webapp"


    try:
        app_licensestartdate = data[ServerConstants.LICENSESTARTDATE]
        kwargs['licensestartdate'] = app_licensestartdate
    except KeyError:
        return HttpResponse("License start time not provided", status=403)

    try:
        app_licenseenddate = data[ServerConstants.LICENSEENDDATE]
        kwargs['licenseenddate'] = app_licenseenddate
    except KeyError:
        return HttpResponse("License end time not provided", status=403)

    kwargs['licenseenabled'] = True

    try:
        partner = data[ServerConstants.PARTNER]
        kwargs['partner'] = partner
    except KeyError:
        pass

    kwargs['payuauthtoken'] = "lI6y4+RkUzNo80Vvah4S07bKp6h1mEaJLiqdPFFSNUY="
    try:
        payu_auth = data[ServerConstants.PAYUAUTHTOKEN]
        if payu_auth != "":
            kwargs['payuauthtoken'] = payu_auth
    except KeyError:
        pass

    try:
        brandname_req = data[ServerConstants.BRANDNAME]
    except KeyError:
        brandname_req = ""

    try:
        contactnumbers_req = data[ServerConstants.CONTACTNUMBERS]
    except KeyError:
        contactnumbers_req = ""

    try:
        host_req = data[ServerConstants.HOST]
    except KeyError:
        return HttpResponse("Host not provided", status=403)
    
    try:
        templatetype_req = data[ServerConstants.TEMPLATETYPE]
    except KeyError:
        return HttpResponse("Tempalte type not provided", status=403) 

    try:
        slogan_req = data[ServerConstants.SLOGAN]
    except KeyError:
        slogan_req = ""

    try:
        speciality_req = data[ServerConstants.SPECIALITY]
    except KeyError:
        return HttpResponse("Speciality not provided", status=403)

    # first check is host exists
    if AppTemplateDetails.objects.filter(host=host_req).exists():
        backendlogger.error('createApp', data, "Host exists", logmessage)
        return HttpResponse("Host exists", status=403)

    try:
        newapp = AppDetails.objects.create(**kwargs)
        newapp.appuid = encode_id(newapp.appid)
        newapp.save()
    except Exception as e:
        backendlogger.error('createApp', data, str(e), logmessage)
        return HttpResponse("Error creating app", status=403)

    appid_str = str(newapp.appid)
    newapp.apptag = appid_str + "-" +newapp.applocation
    djangouser = User.objects.create_user(username="app" + appid_str, password="app" + appid_str,
                                          first_name="app", last_name=appid_str)
    newapp.djangouser = djangouser
    newapp.save()

    # add to app group
    g = Group.objects.get(name='appgroup')
    g.user_set.add(djangouser)
    
    # if perosnid given then make mapping
    try:
        personuid_req = data[ServerConstants.PERSONUID]
        person = PersonDetails.objects.get(personuid = personuid_req)
        AdminAppMapping.objects.create(appdetails = newapp, person = person)
        AnalystAppMapping.objects.create(appdetails = newapp, person = person)
    except :
        pass

    
    # create app template details
    imagesstr = ",".join([
        'https://storage.googleapis.com/' + config.GCSAPPTEMPLATEIMAGESBUCKET + '/' + speciality_req + '/bg1.jpg',
        'https://storage.googleapis.com/' + config.GCSAPPTEMPLATEIMAGESBUCKET + '/' + speciality_req + '/bg2.jpg',
        'https://storage.googleapis.com/' + config.GCSAPPTEMPLATEIMAGESBUCKET + '/' + speciality_req + '/bg3.jpg',
        'https://storage.googleapis.com/' + config.GCSAPPTEMPLATEIMAGESBUCKET + '/' + speciality_req + '/bg4.jpg'])

    dynamicLink = utils.createfcmdynamiclink("https://" + host_req, config.DIGITALPATIENT_ANDROIDPACKAGE)
    hostshortlink = create_short_url(host_req)

    AppTemplateDetails.objects.create(appdetails=newapp, host = host_req, brandname = brandname_req, slogan = slogan_req,
                                      templatetype = templatetype_req, imagesstr = imagesstr, speciality = speciality_req,
                                      contactnumbers = contactnumbers_req, dynamiclink = dynamicLink , hostshortlink = hostshortlink)


    httpoutput = utils.successJson(dict({ServerConstants.APPUID: newapp.appuid, ServerConstants.APPTAG: newapp.apptag}))
    backendlogger.info('createWebApp', data, httpoutput, logmessage)
    # requestlogger.info('updateAppTemplateDetailsResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE, status=200)
    

@csrf_exempt
def deleteAppImage(request):
    data = json.loads(request.body)
    logmessage = []
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('deleteAppImage', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    logmessage.append('deleteAppImage;%s' % (str(data)))
    # requestlogger.info('updateAppTemplateDetails;%s'%(str(data)))

    try:
        appuid_req = data[ServerConstants.APPUID]
    except KeyError:
        return HttpResponse("Please provide appid ", status=403)

    try:
        appdetails = AppDetails.objects.get(appuid=appuid_req)
    except ObjectDoesNotExist:
        return HttpResponse("app not found", status=404)

    try:
        apptemplatedetails = appdetails.apptemplatedetails
    except ObjectDoesNotExist:
        return HttpResponse("apptemplate not found", status=404)

    try:
        imageurl_req = data[ServerConstants.IMAGEURL]
    except KeyError:
        return HttpResponse("Please provide imageurl ", status=403)

    imagearray = apptemplatedetails.imagesstr.split(',')

    try:
        imagearray.remove(imageurl_req)
        apptemplatedetails.imagesstr = ",".join(imagearray)
        apptemplatedetails.save()
    except:
        backendlogger.info('deleteAppImage', data, "image not found in list", logmessage)

    httpoutput = utils.successJson(dict())
    backendlogger.info('deleteAppImage', data, httpoutput, logmessage)
    # requestlogger.info('updateAppTemplateDetailsResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE, status=200)




@csrf_exempt
def updateAppTemplateDetails(request):
    # requestlogger.info('================updateAppTemplateDetails=====================')
    data = json.loads(request.body)
    logmessage = []
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('updateAppTemplateDetails', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    logmessage.append('updateAppTemplateDetails;%s'%(str(data)))
    # requestlogger.info('updateAppTemplateDetails;%s'%(str(data)))

    try:
        appuid_req = data[ServerConstants.APPUID]
    except KeyError:
        return HttpResponse("Please provide appid ", status=403)

    try:
        appdetails = AppDetails.objects.get(appuid=appuid_req)
    except ObjectDoesNotExist:
        return HttpResponse("app not found", status=404)

    try:
        apptemplatedetails = appdetails.apptemplatedetails
    except ObjectDoesNotExist:
        backendlogger.error('updateAppTemplateDetails', data, 'AppTEmpalte  not found', logmessage)
        return HttpResponse("AppTEmpalte not found", status=404)

    try:
        brandname = data[ServerConstants.BRANDNAME]
        apptemplatedetails.brandname = brandname
    except KeyError:
        pass

    try:
        host = data[ServerConstants.HOST]
        apptemplatedetails.host = host
    except KeyError:
        pass

    try:
        templatetype = data[ServerConstants.TEMPLATETYPE]
        apptemplatedetails.templatetype = templatetype
    except KeyError:
        pass

    try:
        slogan = data[ServerConstants.SLOGAN]
        apptemplatedetails.slogan = slogan
    except KeyError:
        pass

    try:
        speciality = data[ServerConstants.SPECIALITY]
        apptemplatedetails.speciality = speciality
    except KeyError:
        pass

    try:
        contactnumbers = data[ServerConstants.CONTACTNUMBERS]
        apptemplatedetails.contactnumbers = contactnumbers
    except KeyError:
        pass

    try:
        hostshortlink = data[ServerConstants.HOSTSHORTURL]
        apptemplatedetails.hostshortlink = hostshortlink
    except KeyError:
        pass

    apptemplatedetails.save()

    jsondata = dict({ServerConstants.APPUID:appuid_req})

    httpoutput = utils.successJson(jsondata)
    backendlogger.info('updateAppTemplateDetails', data, httpoutput, logmessage)
    # requestlogger.info('updateAppTemplateDetailsResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE,status=200)

