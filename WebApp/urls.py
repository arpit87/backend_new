from django.conf.urls import url

from WebApp import views

urlpatterns = [
    url(r'^getAppTemplateDetails/', views.getAppTemplateDetails, name='getAppTemplateDetails'),
    url(r'^updateAppTemplateDetails/', views.updateAppTemplateDetails, name='updateAppTemplateDetails'),
    url(r'^deleteAppImage/', views.deleteAppImage, name='deleteAppImage'),
    url(r'^createWebApp/', views.createWebApp, name='createWebApp'),
]