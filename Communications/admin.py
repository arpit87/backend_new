from django.contrib import admin
from Communications.models import CallLog, BulkSMSLog
from import_export import resources
from import_export.admin import ImportExportModelAdmin,ImportExportMixin
# Register your models here.
class CallLogModelAdmin(admin.ModelAdmin):
    list_display = ('doctor','healthcase','status','created')

admin.site.register(CallLog, CallLogModelAdmin)


class BulkSMSLogModelAdmin(admin.ModelAdmin):
    list_display = ('appdetails','count','created_at')

admin.site.register(BulkSMSLog,BulkSMSLogModelAdmin)







