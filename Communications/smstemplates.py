from backend_new.Constants import PreferredLanguageConstant , SmsTemplateType
SMSTEMPLATES = {
    SmsTemplateType.WELCOME : {
        PreferredLanguageConstant.ENGLISH : ("Welcome to {}\nTo contact doctor from comfort of home click on link\n{}","1707160102153396379"),
        PreferredLanguageConstant.HINDI: ("{} में आपका स्वागत है\nघर बैठे डॉक्टर से ऑनलाइन संपर्क करने के लिए लिंक पर क्लिक करें\n{} ","1707160102159728497"),
        PreferredLanguageConstant.GUJARATI: ("{} માં સ્વાગત છે\nઘરેથી ડ તબીબ ક્ટરનો સંપર્ક કરવા માટે લિંક પર ક્લિક કરો\n{}","1707160102163721301"),
        PreferredLanguageConstant.MARATHI: ("{} आपले स्वागत आहे. घरोघरी डॉक्टरांशी संपर्क साधण्यासाठी लिंकवर क्लिक करा\n{}","1707160102167743787")
    },
    SmsTemplateType.PROMOTION: {
        PreferredLanguageConstant.ENGLISH: ("{}\nTo contact doctor click on link\n{}","1707160102173818741"),
        PreferredLanguageConstant.HINDI: ("{}\nडॉक्टर से संपर्क के लिए क्लिक करें\n{} ","1707160102177745342"),
        PreferredLanguageConstant.GUJARATI: ("{}\nઘરેથી ડ તબીબ ક્ટરનો સંપર્ક કરવા માટે લિંક પર ક્લિક કરો\n{}","1707160102181076812"),
        PreferredLanguageConstant.MARATHI: ("{}\nघरोघरी डॉक्टरांशी संपर्क साधण्यासाठी लिंकवर क्लिक करा\n{}","1707160102181076812")
    },
     SmsTemplateType.NEWHEALTHCASEPATIENT: {
        PreferredLanguageConstant.ENGLISH: ("We have received your case. Our doctors will respond to you in few hours. For followup click link\n{}","1707160102190333827"),
        PreferredLanguageConstant.HINDI: ("हमें आपका केस मिल गया है. डॉक्टर कुछ घंटों में जवाब देंगे। देखने के लिए इस लिंक पर क्लिक करें\n{} ",""),
        PreferredLanguageConstant.GUJARATI: ("અમને તમારો કેસ મળ્યો છે. ડોકટરો થોડા કલાકોમાં જવાબ આપશે. જોવા માટે આ લિંકને ક્લિક કરો\n{}","1707160102334695633"),
        PreferredLanguageConstant.MARATHI: ("आम्हाला आपला केस प्राप्त झाला आहे. डॉक्टर काही तासांत प्रतिसाद देतील. पाहण्यासाठी या दुव्यावर क्लिक करा\n{}","1707160102338939281")
    },
    SmsTemplateType.NEWHEALTHCASEHEALTHCHECKUP: {
        PreferredLanguageConstant.ENGLISH: ("Click link to see health case\n{}","1707160102344154870"),
        PreferredLanguageConstant.HINDI: ("स्वास्थ्य डेटा देखने के लिए लिंक पर क्लिक करें\n{} ","1707160102349418018"),
        PreferredLanguageConstant.GUJARATI: ("આરોગ્ય ડેટા જોવા માટે લિંકને ક્લિક કરો\n{}","1707160102352385020"),
        PreferredLanguageConstant.MARATHI: ("आरोग्याचा डेटा पाहण्यासाठी दुव्यावर क्लिक करा\n{}","1707160102358414727")
    },
    SmsTemplateType.NEWHEALTHCASEDOCTOR: {
        PreferredLanguageConstant.ENGLISH: ("New Case\nPatient: {}\n{}",""),
        PreferredLanguageConstant.HINDI: ("नया केस\nपेशेंट: {}\n{} ",""),
        PreferredLanguageConstant.GUJARATI: ("નવો કેસ\nદર્દી:{}\n{}",""),
        PreferredLanguageConstant.MARATHI: ("नवीन केस\nरुग्ण: {}\n{}","")
    },
     SmsTemplateType.DOCTORRESPONSE: {
         PreferredLanguageConstant.ENGLISH: ("Response by Dr {}\n{}","1707160102410238510"),
         PreferredLanguageConstant.HINDI: ("डॉ द्वारा जवाब\nडॉ:{}\nक्लिक करें\n{}","1707160102416967678"),
         PreferredLanguageConstant.GUJARATI: ("डॉ જવાબ આપ્યો છે\nडॉ:{}\nક્લિક કરો\n{}","1707160102416967678"),
         PreferredLanguageConstant.MARATHI: ("डॉ प्रत्युत्तर दिले आहे\nडॉ:{}\nक्लिक करा\n{}","1707160102425918415")
    },
    SmsTemplateType.PATIENTRESPONSE: {
        PreferredLanguageConstant.ENGLISH: ("Reply by patient\nPatient: {}. Click to see \n{} ","1707160102429759655"),
        PreferredLanguageConstant.HINDI: ("पेशेंट ने जवाब दिया है\nपेशेंट:{}\n{}","1707160102432360403"),
        PreferredLanguageConstant.GUJARATI: ("દર્દી જવાબ આપ્યો છે\nદર્દી:{}\n{}","1707160102435692564"),
        PreferredLanguageConstant.MARATHI: ("पेशंट प्रत्युत्तर दिले आहे\nपेशंट:{}\n{}","1707160102439689640")
    },
     SmsTemplateType.PRESCRIPTIONBYDOCTOR: {
        PreferredLanguageConstant.ENGLISH: ("Prescription\nDr {}\n{}",""),
        PreferredLanguageConstant.HINDI: ("प्रिस्क्रिप्शन\nडॉ:{}\nक्लिक करें\n{}",""),
        PreferredLanguageConstant.GUJARATI: ("પ્રિસ્ક્રિપ્શન\nडॉ:{}\nક્લિક કરો\n{}",""),
        PreferredLanguageConstant.MARATHI: ("प्रिस्क्रिप्शन\nडॉ:{}\nक्लिक करा\n{}","")
    },
    SmsTemplateType.VIDEOBYDOCTOR: {
        PreferredLanguageConstant.ENGLISH: ("Video reply by Dr {}\nClick\n{}",""),
        PreferredLanguageConstant.HINDI: ("वीडियो जवाब\nडॉ {}\nक्लिक करें\n{}",""),
        PreferredLanguageConstant.GUJARATI: ("વિડિઓ જવાબ\nડક્ટર {}\nક્લિક કરો\n{}",""),
        PreferredLanguageConstant.MARATHI: ("व्हिडिओ प्रत्युत्तर\nडॉ {}\nक्लिक करा\n{}","")
    },
    SmsTemplateType.VIDEOBYPATIENT: {
        PreferredLanguageConstant.ENGLISH: ("Video by patient\nPatient: {}\n{}",""),
        PreferredLanguageConstant.HINDI: ("पेशेंट द्वारा वीडियो\nपेशेंट: {}\n{}",""),
        PreferredLanguageConstant.GUJARATI: ("દર્દી દ્વારા વિડિઓ\nદર્દી: {}\n{}",""),
        PreferredLanguageConstant.MARATHI: ("रुग्णाला द्वारे व्हिडिओ\nरुग्णाला: {}\n{}","")
    },
    SmsTemplateType.APPOINTMENTBOOKED: {
        PreferredLanguageConstant.ENGLISH: ("Appointment Booked\nDr:{}\nDate:{}\nTime:{}",""),
        PreferredLanguageConstant.HINDI: ("अपॉइंटमेंट बुक\nडॉ:{}\nतारीख:{}\nसमय:{}",""),
        PreferredLanguageConstant.GUJARATI: ("એપોઇન્ટમેન્ટ બુક કરાવી\nडॉ:{}\nતારીખ:{}\nસમય:{}",""),
        PreferredLanguageConstant.MARATHI: ("अपॉइंटमेंट बुक\nडॉ:{}\nतारीख:{}\nवेळ:{}","")
    },
    SmsTemplateType.VIDEOCALLBOOKED: {
        PreferredLanguageConstant.ENGLISH: ("Video Call Booked\nDate:{}\nTime:{}\nCode:{}\nClick to start\n{}",""),
        PreferredLanguageConstant.HINDI: ("वीडियो कॉल बुक\nतारीख:{}\nसमय:{}\nकोड:{}\nशुरू करने के लिए क्लिक करें\n{}",""),
        PreferredLanguageConstant.GUJARATI: ("વિડિઓ કલ બુક કરાવી\nતારીખ:{}\nસમય:{}\nકોડ:{}\nપ્રારંભ કરવા માટે ક્લિક કરો\n{}",""),
        PreferredLanguageConstant.MARATHI: ("व्हिडिओ कॉल बुक\nतारीख:{}\nवेळ:{}\nकोड:{}\nप्रारंभ करण्यासाठी क्लिक करा\n{}",""),
    },
    SmsTemplateType.PAYMENTREQUEST: {
        PreferredLanguageConstant.ENGLISH: ("Payment request\n{}\nAmt:{}\nClick to pay\n{}",""),
        PreferredLanguageConstant.HINDI: ("भुगतान अनुरोध\n{}\nरकम:{}\nभुगतान के लिए क्लिक करें\n{}",""),
        PreferredLanguageConstant.GUJARATI: ("ચુકવણી વિનંતી\n{}\nરકમ:{}\nચુકવવા ક્લિક કરો\n{}",""),
        PreferredLanguageConstant.MARATHI: ("पैसे विनंती\n{}\nरक्कम:{}\nदेण्यासाठी क्लिक करा\n{}","")
    }
}

def getwelcometemplate(preferred_language, brandname, link):
    tuple = SMSTEMPLATES[SmsTemplateType.WELCOME][preferred_language]
    return (tuple[0].format(brandname, link),tuple[1])


def getpromotiontemplate(preferred_language, brandname, link):
    tuple = SMSTEMPLATES[SmsTemplateType.PROMOTION][preferred_language]
    return (tuple[0].format(brandname, link), tuple[1])


def getnewhealthcasepatienttemplate(preferred_language, link):
    tuple = SMSTEMPLATES[SmsTemplateType.NEWHEALTHCASEPATIENT][preferred_language]
    return (tuple[0].format(link),tuple[1])


def getnewhealthcasehealthcheckuptemplate(preferred_language, link):
    tuple = SMSTEMPLATES[SmsTemplateType.NEWHEALTHCASEHEALTHCHECKUP][preferred_language]
    return (tuple[0].format(link),tuple[1])


def getnewhealthcasedoctortemplate(preferred_language, patientname, link):
    tuple = SMSTEMPLATES[SmsTemplateType.NEWHEALTHCASEDOCTOR][preferred_language]
    return (tuple[0].format(patientname, link),tuple[1])


def getprescriptiontemplate(preferred_language, doctorname, link):
    tuple = SMSTEMPLATES[SmsTemplateType.PRESCRIPTIONBYDOCTOR][preferred_language]
    return (tuple[0].format(doctorname, link),tuple[1])


def getpatientresponsetemplate(preferred_language, patientname, link):
    tuple = SMSTEMPLATES[SmsTemplateType.PATIENTRESPONSE][preferred_language]
    return (tuple[0].format(patientname, link),tuple[1])


def getdoctorresponsetemplate(preferred_language, doctorname, link):
    tuple = SMSTEMPLATES[SmsTemplateType.DOCTORRESPONSE][preferred_language]
    return (tuple[0].format(doctorname, link),tuple[1])


def getappointmenttemplate(preferred_language, doctorname, date,time):
    tuple = SMSTEMPLATES[SmsTemplateType.APPOINTMENTBOOKED][preferred_language]
    return (tuple[0].format(doctorname,date, time),tuple[1])


def getdoctorvideotemplate(preferred_language, doctorname, link):
    tuple = SMSTEMPLATES[SmsTemplateType.VIDEOBYDOCTOR][preferred_language]
    return (tuple[0].format(doctorname, link),tuple[1])


def getpatientvideotemplate(preferred_language, patientname, link):
    tuple = SMSTEMPLATES[SmsTemplateType.VIDEOBYPATIENT][preferred_language]
    return (tuple[0].format(patientname, link),tuple[1])


def getvccodetemplate(preferred_language, date,time, code, link):
    tuple = SMSTEMPLATES[SmsTemplateType.VIDEOCALLBOOKED][preferred_language]
    return (tuple[0].format(date, time, code ,link),tuple[1])


def getpaymentrequesttemplate(preferred_language, paymentfor, amount, link):
    tuple = SMSTEMPLATES[SmsTemplateType.PAYMENTREQUEST][preferred_language]
    return (tuple[0].format(paymentfor, amount, link),tuple[1])
