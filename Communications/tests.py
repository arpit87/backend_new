from Communications.models import SMSAPPDATA
from django.test import TestCase
from Doctor.models import DoctorDetails
from Person.models import PersonDetails
from Communications import gcm
from django.test.utils import override_settings

import json
import Platform.Constants
from backend_new.Constants import ServerConstants,FileTypeConstants,FileStorageConstants

from django.test.client import MULTIPART_CONTENT

from django.conf import settings
from django.contrib.auth.models import Person
import pylibmc
import backend_new.config
from backend_new import config
from backend_new.config import AUTHKEY, AUTHSECRET
# Create your tests here.

class SmsAPITestCase(TestCase):
    multi_db = True
    def setUp(self):
        backend_new.config.ISTESTMODE = True
        SMSAPPDATA.objects.create(pk=1,gcmregid="APA91bHEXXMQ0eib7X0XsPiLjeHvbggpTtahmOTKBgxydy9hiayMrMhGt25Wln_kLhUxVGGVdlM9cthMwsa41U4-h5k5HXAHBEY6DMzYhniD2uqeCymKfbcrp7DA-4w57btLyr-Y4ynh1SL1vO7phf_JsrPAhKAOGA")
        djangouser = Person.objects.create(username="Doctoryolohealth",password="abc")
        djangouser1 = Person.objects.create(username="Doctoryolohealth1",password="abc")
        # QBSESSIONDATA.objects.create(application_id=32324,token="asf",expiry=12456)
        Doctornew = DoctorDetails.objects.create(djangouser=djangouser,doctoruid=1,name="arpit",phone="9769465241",skillstr="1,2",docs="testdoc1,testdoc2,http://yolohealth.in/static/uploadedfiles/Doctordocs/file1.png")
        Doctornew1 = DoctorDetails.objects.create(djangouser=djangouser1,doctoruid=11,name="arpit",phone="9769465241",skillstr="1")
        usernew=PersonDetails.objects.create(personuid=1, name='arpit', phone="9769465241")

    # def test_SendSMSToDoctor(self):
    #     listDoctorids=[1,11]
    #     result = gcm.sendGCMToSendSMSToDoctors(listDoctorids,"hello how are you")
    #     self.assertEqual(result,200)

    # def test_SendSMSToPerson(self):
    #     listpersonids=1
    #     result = gcm.sendGCMToSendSMSToPersons(listpersonids,"hello how are you")
    #     self.assertEqual(result,200)

    def test_setSMSGCMRegistrationId(self):
        TESTGCM = str("APA91bG1lxx6X4ic-yRSpOojVlRQR0Vbqxs1TkV0snM-GHuD3jBlJvVD-9l1bOy4wMVZVXzkq3oKDUDGbXb901MXYGvLN8_VGd3-wHXHTiPKSkznUti0Xhe4Mi1puPhgolGRBR705RI0xEBQe7L1cy9_Zf6e3W2iFQ")
        input = json.dumps({ServerConstants.FCMTOKEN:TESTGCM, "authkey": AUTHKEY, "authsecret": AUTHSECRET})
        response = self.client.post('/Communications/setSMSGCMRegistrationId/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        thisuserdata = SMSAPPDATA.objects.get(pk=1)
        self.assertEqual(thisuserdata.gcmregid,TESTGCM)

@override_settings(EMAIL_BACKEND='django.core.mail.backends.smtp.EmailBackend')
class EmailAPITest(TestCase):
    def setUp(self):
        backend_new.config.ISTESTMODE = True

    def test_contactUsFromUpload(self):
        input = json.dumps({ServerConstants.CONTACTUSEMAIL:"arpit.mishra@yolohealth.in",
                            ServerConstants.CONTACTUSPHONE:"9769465241",
                            ServerConstants.CONTACTUSMESSAGE:"testmsg",
                             ServerConstants.CONTACTUSSUBJECT:"Contact Us EMail"})

        response = self.client.post('/Communications/contactUsEmail/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)

    def test_contactUsFromPartner(self):
        input = json.dumps({ServerConstants.CONTACTUSEMAIL:"arpit.mishra@yolohealth.in",
                            ServerConstants.CONTACTUSPHONE:"9769465241",
                            ServerConstants.CONTACTUSMESSAGE:"testmsg",
                            ServerConstants.CONTACTUSNAME:"arpit",
                            ServerConstants.COMPANYNAME:"Ur-e-clinic"})

        response = self.client.post('/Communications/contactUsEmail/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)

    def test_sendIPEmail(self):
        input = json.dumps({ServerConstants.IPADDRESS:"192.165.11.123",
                            ServerConstants.KIOSKID:"101",
                            ServerConstants.KIOSKLOCATION:"Powai Plaza"})

        response = self.client.post('/Communications/sendIPEmail/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)

    def test_sendAppReport(self):
        reportstr = {"name":"Guest","age":15,"gender":"Male","kiosklocation":"Yolo-Test-Prod","height":"--","weight":"--","date":"24 Apr 2019","pulse":83,"oxygensat":99,"pulseShowRange":{"low":60,"normal":90},"pulseRange":"Normal","oxygensatRange":"Normal","temperature":"98.4","temperatureincelsius":"36.9","temperatureRange":"Normal","healthscore":"97","healthscoreSmiley":"satisfy.png","location":"Yolo-Test-Prod","partnerphone":"1800 1020 333","partneremail":"support@yolohealth.in","partnerwebsite":"www.yolohealth.in","partneraddress":"Yolohealth, Plot-3A, Balewadi Phata,Baner,Pune"}
        input = json.dumps({ServerConstants.REPORTDATA:json.dumps(reportstr),
                            ServerConstants.RECIPIENT:"akash.wankhede@yolohealth.in",
                            ServerConstants.REPORTTYPE: 1,
                            ServerConstants.PARTNER: "yolo",
                            })

        response = self.client.post('/Communications/sendAppReport/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        print("test_sendAppReport response: ", response.content)
        self.assertEqual(response.status_code,200)

    def test_sendAppReport1(self):
        reportstr = {"name":"Guest","age":15,"gender":"Male","kiosklocation":"Yolo-Test-Prod","height":"--","weight":"--","date":"24 Apr 2019","pulse":83,"oxygensat":99,"pulseShowRange":{"low":60,"normal":90},"pulseRange":"Normal","oxygensatRange":"Normal","temperature":"98.4","temperatureincelsius":"36.9","temperatureRange":"Normal","healthscore":"97","healthscoreSmiley":"satisfy.png","location":"Yolo-Test-Prod","partnerphone":"1800 1020 333","partneremail":"support@yolohealth.in","partnerwebsite":"www.yolohealth.in","partneraddress":"Yolohealth, Plot-3A, Balewadi Phata,Baner,Pune"}
        input = json.dumps({ServerConstants.REPORTDATA:json.dumps(reportstr),
                            ServerConstants.RECIPIENT:"akash.wankhede@yolohealth.in",
                            ServerConstants.REPORTTYPE: 1,
                            ServerConstants.PARTNER: "yolo",
                            })

        response = self.client.post('/Communications/sendAppReport/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        print("test_sendAppReport1 response: ", response.content)
        self.assertEqual(response.status_code,200)

    def test_sendAppBasicReport(self):
        #reportstr="{\"name\":\"VINEET KHURANA\",\"age\":28,\"gender\":\"Male\",\"height\":\"6'0\",\"weight\":\"65.00\",\"bmi\":\"21.00\",\"bmiRange\":\"Obese\",\"fat\":\"20.00\",\"fatRange\":\"Acceptable\",\"muscle\":\"35.00\",\"muscleRange\":\"Normal\",\"hydration\":\"50.00\",\"hydrationRange\":\"\",\"systolic\":123,\"diastolic\":71,\"bpRange\":\"Normal\",\"pulse\":\"70 bpm\",\"oxygensat\":\"--\",\"temperature\":\"--\",\"date\":\"17 Dec 2016\",\"bonemass\":\"--\",\"bonemassRange\":\"\",\"pulse1\":\"70 bpm\",\"pulse2\":\"--\",\"bpSmiley\":\"satisfy.png\",\"oxygensatRange\":\"\",\"temperatureRange\":\"\",\"pulseRange\":\"Low\",\"pulseSmiley\":\"sad.png\",\"healthscore\":\"97\",\"healthscoreSmiley\":\"satisfy.png\"}",
        reportstr = "{\"name\":\"Demo\",\"age\":29,\"gender\":\"Male\",\"height\":\"5'9\\\"\",\"weight\":\"64.00\",\"bmi\":\"21.00\",\"bmiRange\":\"Obese\",\"fat\":\"20.00\",\"fatRange\":\"Acceptable\",\"muscle\":\"35.00\",\"muscleRange\":\"Normal\",\"hydration\":\"50.00\",\"hydrationRange\":\"Normal\",\"systolic\":120,\"diastolic\":80,\"bpRange\":\"Pre-hypertension\",\"pulse\":70,\"oxygensat\":98,\"temperature\":\"96.0\",\"date\":\"8,Apr 2016\",\"bonemass\":\"3.40\",\"pulse1\":\"72 bpm\",\"pulse2\":70,\"bmiSmiley\":\"satisfy.png\",\"bpSmiley\":\"emoticon.png\",\"fatSmiley\":\"emoticon.png\",\"muscleSmiley\":\"satisfy.png\",\"hydrationSmiley\":\"sad.png\",\"bonemassRange\":\"Low\",\"bonemassSmiley\":\"sad.png\",\"oxygensatRange\":\"Normal\",\"oxygensatSmiley\":\"satisfy.png\",\"temperatureRange\":\"Normal\",\"temperatureSmiley\":\"satisfy.png\",\"pulseRange\":\"Normal\",\"pulseSmiley\":\"satisfy.png\",\"healthscore\":\"86\",\"healthscoreSmiley\":\"satisfy.png\",\"fvc1\": 4.76, \"pef\": 439.2, \"fvc\": 4.77}"
        input = json.dumps({ServerConstants.REPORTDATA:reportstr,
                            ServerConstants.RECIPIENT:"akash.wankhede@yolohealth.in",
                            ServerConstants.REPORTTYPE:1,
                            ServerConstants.PARTNER:"yolo"
                            })

        response = self.client.post('/Communications/sendAppReport/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        print("test_sendAppBasicReport response: ", response.content)
        self.assertEqual(response.status_code,200)

    def test_sendAppBloodReport(self):
        reportstr ="{\"name\":\"Demo\",\"age\":29,\"gender\":\"Male\",\"glucose_level\":\"103\",\"pulse\":70,\"oxygensat\":98,\"hb\":15.02,\"hct\":41,\"tc\":159,\"hdl\":35,\"ldl\":52,\"hdlratio\":3.25,\"nonhdl\":124,\"date\":\"9,Apr 2016\",\"height\":\"--\",\"tg\":114,\"glucose_levelRangeRandom\":\"Normal\",\"glucose_levelRangeSmiley\":\"satisfy.png\",\"hemoglobinRange\":\"Normal\",\"hemoglobinSmiley\":\"satisfy.png\",\"hematocritRange\":\"Low\",\"hematocritSmiley\":\"sad.png\",\"bpRange\":\"\",\"oxygensatRange\":\"Normal\",\"oxygensatSmiley\":\"satisfy.png\",\"pulseRange\":\"Normal\",\"pulseSmiley\":\"satisfy.png\",\"healthscore\":\"90\",\"healthscoreSmiley\":\"satisfy.png\",\"lipidSmiley\":\"satisfy.png\",\"hiv\":\"Negative\", \"dengue\":\"Negative\"}"
         # "{\"name\":\"SURYA MOHAN\",\"age\":38,\"gender\":\"Male\",\"glucose_level\":103,\"glucose_levelRangeRandom\":\"Noraml\",\"glucose_levelRangeSmiley\":\"satisfy.png\",\"pulse\":82,\"oxygensat\":97,\"hb\":\"--\",\"hct\":\"--\",\"tc\":208,\"hdl\":34,\"ldl\":143,\"hdlratio\":4.205883,\"nonhdl\":174,\"date\":\"2 Feb 2016\",\"height\":\"6'1\\\"\",\"tg\":155,\"pulse2\":82,\"hemoglobinRange\":\"\",\"hematocritRange\":\"\",\"bpRange\":\"\",\"oxygensatRange\":\"Normal\",\"oxygensatSmiley\":\"satisfy.png\",\"pulseRange\":\"Normal\",\"pulseSmiley\":\"satisfy.png\"}"
        input = json.dumps({ServerConstants.REPORTDATA:reportstr,
                            ServerConstants.RECIPIENT:"akash.wankhede@yolohealth.in",
                            ServerConstants.REPORTTYPE:7
                            })

        response = self.client.post('/Communications/sendAppReport/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        print("test_sendAppBloodReport response: ", response.content)
        self.assertEqual(response.status_code,200)

    def test_sendAppReport2(self):
        reportstr = "{\"name\":\"Demo\",\"age\":29,\"gender\":\"Male\",\"height\":\"5'9\\\"\",\"weight\":\"--\",\"bmi\":\"21.00\",\"bmiRange\":\"Obese\",\"fat\":\"20.00\",\"fatRange\":\"Acceptable\",\"muscle\":\"35.00\",\"muscleRange\":\"Normal\",\"hydration\":\"50.00\",\"hydrationRange\":\"Normal\",\"systolic\":120,\"diastolic\":80,\"bpRange\":\"Pre-hypertension\",\"pulse\":70,\"oxygensat\":98,\"temperature\":\"96.0\",\"date\":\"8,Apr 2016\",\"bonemass\":\"3.40\",\"pulse1\":\"72 bpm\",\"pulse2\":70,\"bmiSmiley\":\"satisfy.png\",\"bpSmiley\":\"emoticon.png\",\"fatSmiley\":\"emoticon.png\",\"muscleSmiley\":\"satisfy.png\",\"hydrationSmiley\":\"sad.png\",\"bonemassRange\":\"Low\",\"bonemassSmiley\":\"sad.png\",\"oxygensatRange\":\"Normal\",\"oxygensatSmiley\":\"satisfy.png\",\"temperatureRange\":\"Normal\",\"temperatureSmiley\":\"satisfy.png\",\"pulseRange\":\"Normal\",\"pulseSmiley\":\"satisfy.png\",\"healthscore\":\"86\",\"healthscoreSmiley\":\"satisfy.png\"}"
        input = json.dumps({ServerConstants.REPORTDATA:reportstr,
                            ServerConstants.RECIPIENT:"akash.wankhede@yolohealth.in",
                            })

        response = self.client.post('/Communications/sendAppReport2/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        print("test_sendAppReport2 response: ", response.content)
        self.assertEqual(response.status_code,200)

    def test_doctorVCBookingEmail(self):
        input = json.dumps({
                      ServerConstants.USERNAME:"hiren",
                      ServerConstants.PHONE:9769465241,
                      ServerConstants.DOCTORNAME:"Arpit",
                      ServerConstants.DOCTORUID:1,
                      ServerConstants.KIOSKID:101,
                      ServerConstants.DATE:"2015-10-20",
                      ServerConstants.STARTTIME:"03:15",
                      ServerConstants.SLOTNO:2})
        response = self.client.post('/Communications/doctorVCBookingEmail/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        print("test_doctorVCBookingEmail response: ", response.content)
        self.assertEqual(response.status_code,200)

    def test_doctorVCRequestEmail(self):
        input = json.dumps({
                      ServerConstants.USERNAME:"hiren",
                      ServerConstants.PHONE:9769465241,
                      ServerConstants.DOCTORUID:1,
                      ServerConstants.DOCTORNAME:"Arpit",
                      ServerConstants.DOCTORPHONE:9769465241
                      })
        response = self.client.post('/Communications/doctorVCRequestEmail/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        print("test_doctorVCRequestEmail response: ", response.content)
        self.assertEqual(response.status_code,200)

class OTPAPITest (TestCase):
    def test_getOTP(self):
        input = dict({ServerConstants.PHONE:'919168586666'})
        response = self.client.get('/Communications/getOTP/',input)
        self.assertEqual(response.status_code,200)

    def test_verifyOTP(self):
        cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
        cache.set('9168586666_otp',7416)
        input = dict({ServerConstants.PHONE:9168586666,
                      ServerConstants.OTP:7416})
        response = self.client.get('/Communications/verifyOTP/',input)
        self.assertEqual(response.status_code,200)

    def test_universalverifyOTP(self):
        cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
        cache.set('9168586666_otp',7416)
        input = dict({ServerConstants.PHONE:9168586666,
                      ServerConstants.OTP:123321})
        response = self.client.get('/Communications/verifyOTP/',input)
        self.assertEqual(response.status_code,200)

    def test_qrlogin(self):
        input = dict({ServerConstants.KIOSKID:100,
                      ServerConstants.PHONE:9168586666})
        response = self.client.get('/Communications/qrlogin/',input)
        self.assertEqual(response.status_code,200)


    def test_verifyQRLogin(self):
        cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
        cache.set("QRcode_100",'9168586666')
        input = dict({ServerConstants.KIOSKID:100})
        response = self.client.get('/Communications/verifyQRLogin/',input)
        self.assertEqual(response.status_code,200)


    def test_sendtoken(self):
        input = json.dumps({ServerConstants.PHONE:919168586666,
                            ServerConstants.TOKENNUMBER:24,
                            })

        response = self.client.post('/Communications/sendtoken/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
