import json
import os

import pdfkit
from django.core.exceptions import ObjectDoesNotExist
from django.template.loader import render_to_string

import Communications.utils
import Platform
from BodyVitals.bodyvitals_utils import apply_dynamic_ranges
from BodyVitals.views import getVitalReport
from Platform.views import authenticateURL
from Doctor.models import AppDetails , DoctorDetails
from Person.models import PersonDetails
from HealthCase.models import HealthCase
from UrlShortner.views import create_short_url
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import base64
from  Platform import utils
from Communications.smstemplates import getwelcometemplate, getvccodetemplate
from VideoConference.models import VCCodeDetails
from WebApp.models import AppTemplateDetails
from backend_new.Constants import ServerConstants, ReportTypeConstants, PreferredLanguageConstant
import logging
from Communications.models import BulkSMSLog


from django.core.mail import send_mail, EmailMessage
from django.template import Context,loader
import pylibmc
import requests
from backend_new import config, settings
from Platform import backendlogger

logger = logging.getLogger(__name__)
requestlogger = logging.getLogger('apirequests')


@csrf_exempt
def licenseRenewEmail(request):
    # requestlogger.info('================licenseRenewEmail=====================')
    #authenticated,djangouserstr = authenticateURL(request)
    #if not authenticated:return HttpResponse("Error authenticating user", status=401)
    data = json.loads(request.body)
    logmessage = []
    if(request.method != "POST"):
        httpresonse = utils.errorJson("Wrong http method type")
        backendlogger.error('licenseRenewEmail', data, 'wrong http method type', logmessage)
        return HttpResponse(httpresonse, content_type=ServerConstants.RESPONSE_JSON_TYPE)

    logmessage.append('licenseRenewEmailRequest;%s'%(str(data)))
    # requestlogger.info('licenseRenewEmailRequest;%s'%(str(data)))

    try:
        message = data[ServerConstants.CONTACTUSMESSAGE]
        applocation = data[ServerConstants.APPLOCATION]
        subject = data[ServerConstants.CONTACTUSSUBJECT]

        c= Context({
            'applocation': applocation,
            'subject': subject,
            'message': message
        })

        template = loader.get_template("license_renewal.html")

        recipient = config.EMAIL_TO
        logmessage.append('recipient:'+recipient)
        logmessage.append('EMAIL_FROM:'+config.EMAIL_FROM)
        #send mail
        successful = send_mail(  subject+" request received",template.render(c.flatten()),config.EMAIL_FROM,[recipient],False)
        logmessage.append('licenseRenewEmailResponse;%s'%(successful))
        # requestlogger.info('licenseRenewEmailResponse;%s'%(successful))
        if(successful==1):
            backendlogger.info('licenseRenewEmail', data, 'successful=1', logmessage)
            return HttpResponse(utils.successJson({}), status=200)
        else:
            backendlogger.error('licenseRenewEmail', data, 'successful!=1', logmessage)
            return HttpResponse("Error sending mail", status=400)
    except KeyError:
        backendlogger.error('licenseRenewEmail', data, 'licenseRenewEmail;%s' % ("400"), logmessage)
        # requestlogger.info('contactUsEmailResponse;%s'%("400"))
        return HttpResponse("Error sending mail",status=400)



@csrf_exempt
def verifyOTP(request):
    data = json.loads(request.body)
    logmessage = []

    phone = data[ServerConstants.PHONE]
    otp = data[ServerConstants.OTP]

    logmessage.append('phone:%s;otp:%s;'%(phone,otp))
    # requestlogger.info('phone:%s;otp:%s;'%(phone,otp))

    cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
    cacheotp = cache.get(phone+"_otp")

    if(str(otp)==config.BYPASSOTP):
        #universal otp
        backendlogger.info('verifyOTP', data, 'universal otp verified used for phone:' + phone, logmessage)
        httpoutput_otp = utils.successJson(dict())
        return HttpResponse(httpoutput_otp, content_type=ServerConstants.RESPONSE_JSON_TYPE, status=200)
    elif(cacheotp == None):
        backendlogger.error('verifyOTP', data, 'otp not found, send again', logmessage)
        # requestlogger.info('otp not found, send again ')
        return HttpResponse('OTP not found, send again', status=403)

    if str(cacheotp) == str(otp):
        backendlogger.info('verifyOTP', data, 'otp verified', logmessage)
        httpoutput = utils.successJson(dict())
        return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE, status=200)
    else:
        backendlogger.error('verifyOTP', data, 'otp doesnt match', logmessage)
        # requestlogger.info('OTP doesnt match ')
        return HttpResponse('OTP doesnt match', status=403)

@csrf_exempt
def getOTP(request):
    # requestlogger.info("================get OTP============")
    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value

    phone = request.GET[ServerConstants.PHONE]

    senderid="GAMVTC"
    try:
        senderid=phone = request.GET[ServerConstants.SENDERID]
    except:
        pass

    cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
    cacheotp = cache.get(phone+"_otp")
    if cacheotp == None:
        otp = utils.getRandomOTP()
    else:
        otp = cacheotp

    if config.SMSSERVICE == "bypass":
        otp = config.BYPASSOTP

    cache.set(phone+"_otp",otp,600)

    if config.SMSSERVICE == "weberleads":
        #send on mobile
        token = config.WEBERLEADS_TOKEN
        text = "Your verification code is:"+ str(otp)
        url = "http://weberleads.in/http-tokenkeyapi.php?"
        params = dict({"authentic-key":token,"senderid":senderid,
                       "route":2,"number":phone,
                       "message":text})
        response = requests.get(url,params)
        if(response.status_code == 200):
            httpoutput = utils.successJson(dict())
            requestlogger.info('otp %s send to mobile %s'%(otp,phone))
            requestlogger.info('httpoutput %s'%(httpoutput))
            return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE,status=200)
        else:
            requestlogger.info('error occured in sending otp:'+response.content)
            return HttpResponse("OTP not sent", status=403)
    elif config.SMSSERVICE == "bypass":
        httpoutput = utils.successJson(dict())
        requestlogger.info('bypass otp %s set for mobile %s' % (otp, phone))
        requestlogger.info('httpoutput %s' % (httpoutput))
        return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE, status=200)
    elif config.SMSSERVICE == "sumit":
        if senderid == None:
            senderid = "GAMVTC"
        url = "http://site.bulksmsnagpur.net/sendsms?"
        text = "Your OTP is:" + str(otp)
        params = dict({"uname": config.SUMITUSERNAME,"pwd": config.SUMITPASSWORD ,"senderid": senderid,
                       "route": "T", "to": phone,"peid":"1401552490000023642","tempid":"1707161824547457400",
                       "msg": text})

        response = requests.get(url, params)
        if (response.status_code == 200):
            httpoutput = utils.successJson(dict())
            requestlogger.info('otp %s send to mobile %s' % (otp, phone))
            requestlogger.info('httpoutput %s' % (httpoutput))
            return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE, status=200)
    else:
        backendlogger.error('sms service unknown')
        return HttpResponse('sms service unknown', status=403)

@csrf_exempt
def sendSMS(request):

    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:return HttpResponse("Error authenticating user", status=401)

    data = json.loads(str(request.body.decode("utf-8")))

    phone = data[ServerConstants.PHONE]
    msg = data[ServerConstants.MESSAGE]

    try:
        senderid = data[ServerConstants.SENDERID]
    except:
        senderid = "GAMVTC"

    done = Communications.utils.sendSMS(msg,phone, senderid)
    if done:
        httpoutput = utils.successJson(data)
    else:
        httpoutput = utils.errorJson(data)
    requestlogger.info('sendSMS;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

@csrf_exempt
def sendVCReminderSMS(request):

    data = json.loads(request.body)
    logmessage = []
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('sendVCReminderSMS', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        vccodeid_req = data[ServerConstants.VCCODEID]
        vccodedetails_req = VCCodeDetails.objects.get(id=vccodeid_req)
    except:
        backendlogger.error('sendVCReminderSMS', data, 'invalid vccodeid', logmessage)
        return HttpResponse("invalid vcodeid", status=403)


    dateformatted = vccodedetails_req.date.strftime("%d %b")
    timeformatted = vccodedetails_req.slotbooking.starttime.strftime("%I:%M %p")
    Communications.utils.sendSMS(getvccodetemplate(vccodedetails_req.healthcase.person.preferredlanguage,
                            dateformatted, timeformatted, vccodedetails_req.vccode,
                            vccodedetails_req.shorturlpatient),
                                 vccodedetails_req.healthcase.person.phone, vccodedetails_req.healthcase.appdetails.appsmssenderid,
                                 vccodedetails_req.healthcase.person.preferredlanguage != PreferredLanguageConstant.ENGLISH)
    httpoutput = Platform.utils.successJson(dict({ServerConstants.URL: vccodedetails_req.shorturlpatient}))
    backendlogger.info('sendVCReminderSMS', data, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

@csrf_exempt
def sendWelcomeSMS(request):

    data = json.loads(request.body)
    logmessage = []
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('sendWelcomeSMS', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        appuid_req = data[ServerConstants.APPUID]
        appdetails = AppDetails.objects.get(appuid = appuid_req)
        senderid = appdetails.appsmssenderid
    except (KeyError, ObjectDoesNotExist):
        return HttpResponse("app not found",status=403)

    try:
        phone_req = data[ServerConstants.PHONE]
    except KeyError:
        return HttpResponse("phone not sent",status=403)

    try:
        preferredlanguage_req = data[ServerConstants.PREFERREDLANGUAGE]
    except:
        preferredlanguage_req = PreferredLanguageConstant.ENGLISH

    try:
        brandname = appdetails.apptemplatedetails.brandname
        link = appdetails.apptemplatedetails.hostshortlink
    except:
        backendlogger.error('sendWelcomeSMS', data, 'Apptemplate not found', logmessage)
        return HttpResponse("Apptemplate not found", status=402)

    done = Communications.utils.sendSMS(getwelcometemplate(preferredlanguage_req, brandname, link ), phone_req, senderid, preferredlanguage_req != PreferredLanguageConstant.ENGLISH)

    if done:
        httpoutput = utils.successJson(dict())
        backendlogger.info('sendWelcomeSMS', data, httpoutput, logmessage)
        return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)
    else:
        backendlogger.error('sendWelcomeSMS', data, 'Error sendig sms', logmessage)
        return HttpResponse("Error sending sms", status=403)

def sendBulkSMS(request):
    data = json.loads(request.body)
    logmessage = []
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('sendBulkSMS', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        appuid_req = data[ServerConstants.APPUID]
        appdetails_req = AppDetails.objects.get(appuid=appuid_req)
    except KeyError:
        backendlogger.error('sendBulkSMS', data, 'appid not sent/valid', logmessage)
        return HttpResponse("appid not sent/valid",status=403)

    try:
        username_req = data[ServerConstants.USERNAME]
    except KeyError:
        backendlogger.error('sendBulkSMS', data, 'username not sent', logmessage)
        return HttpResponse("username not sent",status=403)

    try:
        password_req = data[ServerConstants.PASSWORD]
    except KeyError:
        backendlogger.error('sendBulkSMS', data, 'password not sent', logmessage)
        return HttpResponse("password not sent",status=403)

    try:
        senderid_req = data[ServerConstants.SENDERID]
    except:
        backendlogger.error('sendBulkSMS', data, 'senderid not sent', logmessage)
        return HttpResponse("senderid not sent", status=403)

    try:
        isunicode_req = data[ServerConstants.ISUNICODE]
    except:
        backendlogger.error('sendBulkSMS', data, 'isunicode not sent', logmessage)
        return HttpResponse("isunicode not sent", status=403)

    try:
        to_req = data[ServerConstants.TO]
    except:
        backendlogger.error('sendBulkSMS', data, 'to not sent', logmessage)
        return HttpResponse("to not sent", status=403)

    try:
        msg_req = data[ServerConstants.MSG]
    except:
        backendlogger.error('sendBulkSMS', data, 'msg not sent', logmessage)
        return HttpResponse("msg not sent", status=403)

    try:
        route_req = data[ServerConstants.ROUTE]
    except:
        backendlogger.error('sendBulkSMS', data, 'route not sent', logmessage)
        return HttpResponse("route not sent", status=403)

    if isunicode_req:
        url = "http://sumit.bulksmsnagpur.net/sendunicodesms/"
    else:
        url = "http://sumit.bulksmsnagpur.net/sendsms/"

    params = dict({"uname": username_req, "pwd": password_req,
                   "senderid": senderid_req, "to": to_req,
                   "msg": msg_req, "route": route_req})

    response = requests.get(url, params)
    count = len(response.text.split(','))
    BulkSMSLog.objects.create(appdetails=appdetails_req, count=count)
    jsondata = dict({ServerConstants.COUNT: count})
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('sendBulkSMS', data, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def sendReportEmailByReportID(request):
    # requestlogger.info('================sendReportSms=====================')
    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('sendReportEmail', logrequest, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        reportid_req = request.GET[ServerConstants.REPORTID]
        # requestlogger.info('sendReportSms:reportis;%s'%(str(reportid_req)))
        logmessage.append('sendReportEmail:reportis;%s'%(str(reportid_req)))
    except KeyError:
        backendlogger.error('sendReportEmail', logrequest, 'reportid not sent in request', logmessage)
        # requestlogger.info('getKioskReport:reportid/userid not sent in request')
        return HttpResponse("reportid not sent in request",status=403)

    try:
        email_req = request.GET[ServerConstants.EMAIL]
        # requestlogger.info('sendReportSms:userid;%s'%(str(userid_req)))
        logmessage.append('sendReportEmail:email;%s'%(str(email_req)))
    except KeyError:
        backendlogger.error('sendReportEmail', logrequest, 'sendReportEmail:userid not found:', logmessage)
        # requestlogger.info('sendReportSms:userid not found:'+userid_req)
        return HttpResponse("sendReportEmail:email not found:",status=403)


    res = getVitalReport(request)
    reportdata = json.loads(res.content)
    requestlogger.info('sendReportEmail;%s' % (str(reportdata)))
    try:
        partner = reportdata[ServerConstants.PARTNER]
    except:
        partner = ""

    try:
        reporttype = reportdata[ServerConstants.REPORTTYPE]
    except:
        reporttype = None

    subject_name = "Your Health Report"

    if reporttype == ReportTypeConstants.BASICREPORT:
        css_file = "emailtemplate.css"
        report_template = "vitalsreport.html"
    elif reporttype == ReportTypeConstants.PRESCRIPTION:
        css_file = "prescription.css"
        report_template = "prescription.html"
        subject_name = "Your Prescription"

    print("report_template: ", report_template)
    # requestlogger.info('reportdata:%s;to:%s'%(reportdata,recipient))
    css = os.path.join(settings.CSS_DIR, css_file)
    reportpath = os.path.join(settings.STATIC_ROOT, settings.REPORTS_DIR,
                              email_req + '_report.pdf')
    reporttemplate = loader.get_template(report_template)
    if reporttype in [1, 3]:  # Basic health checkup OR Heart test
        reportdata = apply_dynamic_ranges(reportdata)
    # requestlogger.info(reportdata)
    reportdata["reporticons"] = settings.REPORT_ICONS + "/"
    reportdata["reporticonspartner"] =  settings.REPORT_ICONS + "/partner/"
    reportdata["partner"] = partner

    # if float(reportdata["healthscore"])>85:
    #     reportdata["healthscoreSmiley"]=happy
    # elif float(reportdata["healthscore"])>70:
    #     reportdata["healthscoreSmiley"] = unhappy
    # else:
    #     reportdata["healthscoreSmiley"] = sad

    c = Context(reportdata)
    filledreport = reporttemplate.render(c.flatten())
    # logmessage.append("filledreport:==="+filledreport)
    # requestlogger.info("filledreport:==="+filledreport)

    options = {
        'page-size': 'A4',
        'encoding': "UTF-8",
        'margin-top': '0in',
        'margin-right': '0in',
        'margin-bottom': '0in',
        'margin-left': '0in',
        'no-outline': None
    }
    requestlogger.info("Template:"+report_template)
    pdfkit.from_string(filledreport, reportpath, css=css, options=options)
    email = EmailMessage()
    email.subject = subject_name
    email.body = "Dear User\n\nSeasons Greetings \n\nKindly find attached "+subject_name+".\n\nWe wish you a healthy life ahead!\n\nWarm Regards,\nTeam "+partner
    email.from_email = partner + "<" + config.EMAIL_FROM+ ">"
    email.to = [email_req]
    email.attach_file(reportpath)
    try:
        response = email.send()
        requestlogger.info(str(response))
        if response:
            backendlogger.info('sendKioskReport', reportdata, 'report sent', logmessage)
            return HttpResponse(utils.successJson(dict()), status=200)
        else:
            backendlogger.error('sendKioskReport', reportdata, 'error sending report to' + email_req, logmessage)
            # requestlogger.info('error sending report to'+recipient)
            return HttpResponse('error sending report', status=400)
    except Exception as e:
        requestlogger.info(str(e))
        backendlogger.error('sendKioskReport', reportdata, 'error sending report to' + email_req, logmessage)
        # requestlogger.info('error sending report to'+recipient)
        return HttpResponse('error sending report', status=400)

@csrf_exempt
def smsuploadreportlink(request):
    data = json.loads(request.body)
    logmessage = []

    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('smsuploadreportlink', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)
    try:
        uid = data[ServerConstants.UID]
        role = data[ServerConstants.ROLE]
    except KeyError:
        backendlogger.error('smsuploadreportlink', data, 'role/id not found', logmessage)
        return HttpResponse("role/id not found", status=403)

    try:
        healthcaseid_req = data[ServerConstants.HEALTHCASEID]
        healthcase_req = HealthCase.objects.get(healthcaseid=healthcaseid_req)
    except (ObjectDoesNotExist, KeyError):
        backendlogger.error('smsuploadreportlink', data, 'healthcase consult not found', logmessage)
        return HttpResponse("healthcase consult not found",status= 403)

    try:
        appuid_req = data[ServerConstants.APPUID]
        appdetails_req = AppDetails.objects.get(appuid=appuid_req)
    except (ObjectDoesNotExist, KeyError):
        backendlogger.error('smsuploadreportlink', data, 'healthcase consult not found', logmessage)
        return HttpResponse("healthcase consult not found", status=403)

    if str(role).lower() == "doctor":
        try:
            doctor = DoctorDetails.objects.get(doctoruid=uid)
            djangouser = doctor.djangouser
            phone = doctor.phone
        except ObjectDoesNotExist:
            backendlogger.error('getcommenthealthcase', data,
                                'doctoruid/corresponding mover not found:id:' + uid, logmessage)
            return HttpResponse("doctoruid/corresponding mover not found:id:" + uid, status=403)
    else:
        try:
            person = PersonDetails.objects.get(personuid=uid)
            djangouser = person.djangouser
            phone = person.name
        except ObjectDoesNotExist:
            backendlogger.error('getcommenthealthcase', data,
                                'personuid/corresponding user not found:id:' + uid, logmessage)
            return HttpResponse("personuid/corresponding user not found:id:" + uid, status=403)

    # settoken of 15 days for this url to be valid
    appsessiontoken = utils.getRandomCharacters(10)
    cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
    cache.set(key=appsessiontoken, val=djangouser.username, time=60 * 60 * 24 * 15)


    try:
        origin = request.headers['Origin']
        if "localhost" in origin:
            origin = config.DOMAIN
    except:
        origin = config.DOMAIN
    urlparamstr = "healthcaseid=" + healthcaseid_req + "&role=" + role + "&uid=" + str(
        uid) + "&appsessiontoken=" + appsessiontoken + "&takepicarg=true"
    urlparamsencrypted = str(base64.urlsafe_b64encode((''.join(chr(ord(a) + 10) for a in urlparamstr)).encode("utf-8")),
                             "utf-8")
    healthcaseurl = origin + "/healthcasetimeline?token=" + urlparamsencrypted
    # healthcaseurl = origin + "/healthcasetimeline?" + urlparamstr
    shorturl = create_short_url(healthcaseurl)
    Communications.utils.sendSMS(" Patient: {}\nClick to upload picture:\n{}".format(healthcase_req.person.name, shorturl), phone, appdetails_req.appsmssenderid, None)

    jsondata = dict({ServerConstants.PHONE: phone, ServerConstants.URL: shorturl})
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('smsuploadreportlink', data, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

@csrf_exempt
def sendWebAppEmail(request):

    data = json.loads(request.body)
    logmessage = []
    if(request.method != "POST"):
        httpresonse = utils.errorJson("Wrong http method type")
        backendlogger.error('licenseRenewEmail', data, 'wrong http method type', logmessage)
        return HttpResponse(httpresonse, content_type=ServerConstants.RESPONSE_JSON_TYPE)

    logmessage.append('sendWebAppAdminEmail;%s'%(str(data)))
    # requestlogger.info('licenseRenewEmailRequest;%s'%(str(data)))

    try:
        patientname = data[ServerConstants.PATIENTNAME]
    except:
        patientname = ""

    try:
        patientphone = data[ServerConstants.PATIENTPHONE]
    except:
        patientphone = ""

    try:
        patientemail = data[ServerConstants.PATIENTEMAIL]
    except:
        patientemail = ""

    try:
        subject = data[ServerConstants.SUBJECT]
    except:
        subject = ""

    try:
        message = data[ServerConstants.MESSAGE]
    except:
        message = ""

    try:
        date = data[ServerConstants.DATE]
    except:
        date = ""

    try:
        appuid = data[ServerConstants.APPUID]
        apptemplate = AppTemplateDetails.objects.get(appdetails__appuid = appuid)
        recipient = apptemplate.adminemail
    except KeyError as e:
        backendlogger.error('sendWebAppAdminEmail', data, str(e), logmessage)
        # requestlogger.info('contactUsEmailResponse;%s'%("400"))
        return HttpResponse("Error sending mail", status=400)

    try:

        c= dict({
            ServerConstants.PATIENTNAME: patientname,
            ServerConstants.PATIENTEMAIL: patientemail,
            ServerConstants.PATIENTPHONE: patientphone,
            ServerConstants.MESSAGE: message,
            ServerConstants.SUBJECT: subject,
            ServerConstants.DATE: date,
        })

        msg_html = render_to_string("webappemail.html",c)

        logmessage.append('recipient:'+recipient)
        logmessage.append('EMAIL_FROM:'+config.EMAIL_FROM)
        #send mail
        msg = EmailMessage(subject=subject, body=msg_html, from_email="Digiclinics <"+ config.EMAIL_FROM+">", to=[recipient])
        msg.content_subtype = "html"
        successful = msg.send()
        logmessage.append('sendWebAppAdminEmail;%s'%(successful))
        # requestlogger.info('licenseRenewEmailResponse;%s'%(successful))
        if(successful==1):
            backendlogger.info('sendWebAppAdminEmail', data, 'successful=1', logmessage)
            return HttpResponse(utils.successJson({}), status=200)
        else:
            backendlogger.error('sendWebAppAdminEmail', data, 'successful!=1', logmessage)
            return HttpResponse("Error sending mail", status=400)
    except Exception as e:
        backendlogger.error('sendWebAppAdminEmail', data, str(e), logmessage)
        # requestlogger.info('contactUsEmailResponse;%s'%("400"))
        return HttpResponse("Error sending mail",status=400)

