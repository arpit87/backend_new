import requests, json, threading, time, pylibmc
import xmltodict
from django.http import HttpResponse
from Platform.views import authenticateURL
from backend_new import config
from Platform import backendlogger, utils
from backend_new.Constants import ServerConstants
from Communications.models import CallLog
from Doctor.models import DoctorDetails, DoctorAppMapping
from HealthCase.models import HealthCase , uploadedaudio
from Person.models import PersonDetails
import logging
from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer, AsyncWebsocketConsumer
import pylibmc
from channels.db import database_sync_to_async
from channels.layers import get_channel_layer
from channels.db import database_sync_to_async
requestlogger = logging.getLogger('apirequests')

ExotelAPIs = {
    "outbound_call": "https://{}:{}@api.exotel.in/v1/Accounts/gamvtechnologies1/Calls/connect.json",
    "add_to_whitelist": "https://{}:{}@api.exotel.in/v1/Accounts/gamvtechnologies1/CustomerWhitelist/",
    "get_call_details": "https://{}:{}@api.exotel.in/v1/Accounts/gamvtechnologies1/Calls/{}",
}

def get_statusid(status):
    status_map = {"queued": 1, "in-progress": 2, "completed": 3, "failed": 4, "busy": 5, "no-answer": 6}
    if status not in status_map.keys():
        return None
    return status_map[status]

def exotel_callback(request):
    data = json.loads(request.body)
    requestlogger.info("exotel_callback:"+str(data))

    callsid = data[ServerConstants.CALLSID]
    eventtype = data[ServerConstants.EVENTTYPE]
    status = data[ServerConstants.EXOSTATUS]
    starttime = data[ServerConstants.EXOSTARTTIME]
    endtime = data[ServerConstants.EXOENDTIME]
    recordingurl = data[ServerConstants.RECORDINGURL]
    conversationduration = data[ServerConstants.CONVERSATIONDURATION]
    thislog = CallLog.objects.get(callsid = callsid)
    thislog.status = get_statusid(status)
    thislog.recordingurl = recordingurl
    thislog.save()
    cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
    channelname = cache.get(callsid)
    if eventtype == "terminal" and channelname is not None:
        requestlogger.info("Call end , will send to channel:" + channelname)
        channel_layer = get_channel_layer()
        msg = {"type": "update.callstatus", ServerConstants.MESSAGE_TYPE: ServerConstants.CALLSTATUS,ServerConstants.STATUS: status,
               ServerConstants.AUDIOURL: recordingurl, ServerConstants.CALLDURATION: conversationduration+10}
        requestlogger.info('sending msg to channel:{}, msg:{}'.format(channelname, str(msg)))
        async_to_sync(channel_layer.send)(channelname, msg)
    else:
        requestlogger.info("No channel found for callsid:" + callsid)

    return HttpResponse(status=200)

@database_sync_to_async
def make_outbound_call(channel_name, doctoruid, healthcaseid, command):
    requestlogger.info("in make_outbound_call ")

    try:
        doctor = DoctorDetails.objects.get(doctoruid = doctoruid)
    except  Exception as e:
        requestlogger.info(str(e))
        raise Exception('Invalid doctor id')

    try:
        healthcase = HealthCase.objects.get(healthcaseid = healthcaseid)
    except  Exception as e:
        requestlogger.info(str(e))
        raise Exception('Invalid healthcase id')

    if command == "call_patient":
        from_number = doctor.phone
        to_number = healthcase.person.phone
    elif command == "call_doctor":
        from_number = healthcase.person.phone
        to_number = doctor.phone
    else:
        raise Exception('Invalid call direction')

    # if testmode:
    #     req_params = {'From': "7218072091", 'To': "8982952675", 'CallerId': callerid}
    callerid = config.EXOTEL_CALLERID

    requestlogger.info("Add from number to whitelist ")
    input = {ServerConstants.VIRTUALNUMBER: callerid, ServerConstants.NUMBER: from_number}
    # Add number to whitelist
    response_str, status = add_to_whitelist(input)
    requestlogger.info("response_str: " + response_str)
    requestlogger.info("status: " + str(status))

    requestlogger.info("Add to number to whitelist ")
    input = {ServerConstants.VIRTUALNUMBER: callerid, ServerConstants.NUMBER: to_number}
    # Add number to whitelist
    response_str, status = add_to_whitelist(input)
    requestlogger.info("response_str: " + response_str)
    requestlogger.info("status: "+ str(status))

    req_params = {'From': from_number, 'To': to_number, 'CallerId': callerid,
                  "StatusCallback": config.DOMAIN + "/api/Communications/exotel_callback/",
                  "StatusCallbackEvents[0]":"terminal",
                  "StatusCallbackContentType": "application/json"}
    requestlogger.info("outbound call params: " + str(req_params))
    try:
        response = requests.post(ExotelAPIs["outbound_call"].format(config.EXOTEL_API_KEY, config.EXOTEL_API_TOKEN), data=req_params)
        requestlogger.info("make_outbound_call response.content: " + str(json.loads(response.content)))
    except Exception as e:
        raise Exception('Cloud telephony server error:{}'.format(e))

    # Create call log
    try:
        outbound_call_response = json.loads(response.content)
        statusid = get_statusid(outbound_call_response["Call"].get("Status"))
        CallLog.objects.create(doctor=doctor, healthcase=healthcase, callsid=outbound_call_response["Call"].get("Sid"), recording_url=outbound_call_response["Call"].get("RecordingUrl"), status= statusid)
        cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
        cache.set(outbound_call_response["Call"].get("Sid"), channel_name, 60 * 30)
    except Exception as e:
        raise Exception('Save call log error:{}'.format(e))

    return doctor, healthcase, outbound_call_response["Call"].get("Status")
    # threading.Thread(target=periodic_api_call, args=(outbound_call_response["Call"].get("Sid"), data, doctorid_list)).start()
    # requestlogger.info(response.content)
    # httpoutput = utils.successJson(response.content.decode("utf-8"))
    # backendlogger.info("make_outbound_call", data, httpoutput, logmessage)
    # return HttpResponse(response.content, content_type=Platform.Constants.RESPONSE_JSON_TYPE)

def add_to_whitelist(data):
    logmessage = []
    logmessage.append('add_to_whitelist;%s'%(str(data)))
    try:
        virtual_number = data[ServerConstants.VIRTUALNUMBER]
    except KeyError:
        backendlogger.error("add_to_whitelist", data, "virtual_number not found", logmessage)
        return "virtual number not found", False

    try:
        number = data[ServerConstants.NUMBER]
    except KeyError:
        backendlogger.error("add_to_whitelist", data, "number not found", logmessage)
        return "Number not found", False

    try:
        language = data[ServerConstants.LANGUAGE]
    except KeyError:
        language = "en"

    try:
        req_params = {ServerConstants.VIRTUALNUMBER: virtual_number, ServerConstants.NUMBER: number, ServerConstants.LANGUAGE: language}
        response = requests.post(ExotelAPIs["add_to_whitelist"].format(config.EXOTEL_API_KEY, config.EXOTEL_API_TOKEN), data=req_params)
        response_dict = xmltodict.parse(response.content)
        requestlogger.info("Message: " + str(response_dict))
        requestlogger.info("Message: "+ response_dict["TwilioResponse"]["Result"]["Message"])
        requestlogger.info("Message: "+response_dict["TwilioResponse"]["Result"]["Succeeded"])
    except Exception as e:
        requestlogger.info("add_to_whitelist: Exception: "+ str(e))
        backendlogger.info("add_to_whitelist", data, str(e), logmessage)
        return str(e), False

    if response_dict["TwilioResponse"]["Result"]["Succeeded"]:
        return "Added to whitelist", True
    else:
        return response_dict["TwilioResponse"]["Result"]["Message"], False

def get_call_details_by_sid(callsid, call_inprogress):
    try:
        response = requests.get(ExotelAPIs["get_call_details"].format(config.EXOTEL_API_KEY, config.EXOTEL_API_TOKEN, callsid))
        call_info= get_call_status(response)
        if call_info["status"] != "in-progress":
            call_inprogress = False
    except Exception as e:
        requestlogger.info("get_call_details_by_sid: ", str(e))
        backendlogger.error('get_call_details_sid', str(callsid), str(e), "")
        return {}, None
    return call_info, call_inprogress

def periodic_api_call(callsid, req_params, doctorid_list):
    call_inprogress = True
    while call_inprogress:
        requestlogger.info("periodic_api_call: call_inprogress", call_inprogress)
        time.sleep(2)
        call_info, call_inprogress = get_call_details_by_sid(callsid, call_inprogress)
        send_call_status(doctorid_list, call_info["status"], callsid)
        if not call_inprogress:
            req_params["status"] = get_statusid(call_info["status"])
            req_params["recording_url"] = call_info["recording_url"]
            # create_call_log(req_params)
            break

def get_call_status(response):
    try:
        response_dict = xmltodict.parse(response.content)
        return {"status": response_dict["TwilioResponse"]["Call"]["Status"], "recording_url": response_dict["TwilioResponse"]["Call"]["RecordingUrl"]}
    except Exception as e:
        requestlogger.info("get_call_status: ", str(e))
        return None

def get_call_details(request):
    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('get_call_details', logrequest, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    callsid = request.GET.get("callsid")
    if not callsid:
        backendlogger.error('get_call_details', logrequest, 'Invalid request', logmessage)
        return HttpResponse("Invalid request",status=403)

    try:
        response = requests.get(ExotelAPIs["get_call_details"].format(config.EXOTEL_API_KEY, config.EXOTEL_API_TOKEN, callsid))
    except Exception as e:
        backendlogger.error('get_call_details_sid', str(callsid), str(e), "")
        return HttpResponse("Invalid request",status=403)

    jsondata = dict({ServerConstants.CALLDETAILS: response.content})
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('get_call_details', logrequest, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

def send_call_status(doctorid_list, status, callsid):
    req_params = { "status": status, ServerConstants.NOTIFICATIONTYPE: "change_call_status", "callsid": callsid}
    requestlogger.info("send_call_status req_params: ", req_params)
    response = requests.post(config.NOTIFICATION_SERVER, json=req_params, verify=False)
    requestlogger.info("response: ", response.content)

class ExotelCallConsumer(AsyncWebsocketConsumer):
    command = None
    doctor = None
    healthcase = None
    statustext = None
    async def connect(self):
        requestlogger.info("connected")
        doctoruid = self.scope['url_route']['kwargs']['doctoruid']
        healthcaseid = self.scope['url_route']['kwargs']['healthcaseid']
        requestlogger.info('Got connect for doctoruid:{},  healthcaseid:{}'.format(str(doctoruid), healthcaseid))
        await self.accept()
        # await self.send(text_data=json.dumps({'type': 'connection_message', 'status': 'connected'}))

    async def receive(self, **kwargs):
        text_data_json = json.loads(kwargs['text_data'])
        self.command = text_data_json['command']
        doctoruid = self.scope['url_route']['kwargs']['doctoruid']
        healthcaseid = self.scope['url_route']['kwargs']['healthcaseid']
        audiocaachename = "audio_" + str(doctoruid) + "_" + healthcaseid
        cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
        cache.get(audiocaachename)
        cache.set(audiocaachename, self.channel_name, 60 * 10)  # expiry time in seconds

        try:
            requestlogger.info("calling make_outbound_call ")
            self.doctor,self.healthcase, self.statustext = await make_outbound_call(self.channel_name, doctoruid, healthcaseid, self.command)
        except Exception as e:
            cache.delete(audiocaachename)
            requestlogger.info(str(e))

        await self.update_callstatus({ServerConstants.STATUS: self.statustext})

    async def disconnect(self, close_code):
        requestlogger.info("disconnect" )
        doctoruid = self.scope['url_route']['kwargs']['doctoruid']
        healthcaseid = self.scope['url_route']['kwargs']['healthcaseid']
        audiocaachename = "audio_" + str(doctoruid) + "_" + healthcaseid
        cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
        cache.delete(audiocaachename)

    async def update_callstatus(self, message):
        requestlogger.info("got update_callstatus msg:" + str(message))
        if message[ServerConstants.STATUS] == "completed":
            await self.saveaudiourl(message[ServerConstants.AUDIOURL])
            await self.send(text_data=json.dumps(
                {ServerConstants.MESSAGE_TYPE: 'call_status', ServerConstants.COMMAND: "update"}
            ))
            await self.close()
        elif message[ServerConstants.STATUS] == "failed" or message[ServerConstants.STATUS] == "busy" or message[ServerConstants.STATUS] == "no-answer"  :
            await self.send(text_data=json.dumps(
                {ServerConstants.MESSAGE_TYPE: 'call_status', ServerConstants.COMMAND: message[ServerConstants.STATUS]}
            ))
            await self.close()
        else :
            await self.send(text_data=json.dumps(
                {ServerConstants.MESSAGE_TYPE: 'call_status', ServerConstants.COMMAND: message[ServerConstants.STATUS]}
            ))

    @database_sync_to_async
    def saveaudiourl(self,audiourl):
        if self.command == "call_patient":
            uploadedaudio.objects.create(healthcase=self.healthcase, audiourl=audiourl, doctor = self.doctor)
        elif self.command == "call_doctor":
            uploadedaudio.objects.create(healthcase=self.healthcase, audiourl=audiourl, person = self.healthcase.person)





