from django.db import models
from django import forms
from HealthCase.models import HealthCase
from Doctor.models import DoctorDetails, AppDetails
from datetime import datetime, time, timezone


class CallLog(models.Model):
    QUEUED = 1
    INPROGRESS = 2
    COMPLETED = 3
    FAILED = 4
    BUSY = 5
    NOANSWER = 6

    STATUSES = ((QUEUED, "queued"), (INPROGRESS, "in-progress"), (COMPLETED, "completed"),
                             (FAILED, "failed"), (BUSY, "busy"), (NOANSWER, "no-answer"))

    status = models.IntegerField(choices=STATUSES)
    healthcase = models.ForeignKey(HealthCase, on_delete=models.CASCADE)
    doctor = models.ForeignKey(DoctorDetails, on_delete=models.CASCADE)
    callsid = models.CharField(max_length=100, null=True, blank=True) # Exotel's call sid
    recording_url = models.CharField(max_length=1000, null=True, blank=True)
    comment = models.TextField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
       return "{} {} {}".format(self.get_status_display(), self.healthcase, self.recording_url)

    def get_dict(self):
        result = {}
        result["status"] = self.get_status_display() if self.status else None
        result["report"] = self.healthcase.healthcaseid if self.healthcase else None
        result["doctor"] = self.doctor.doctoruid if self.doctor else None
        result["callsid"] = self.callsid if self.callsid else None
        result["recording_url"] = self.recording_url if self.recording_url else None
        result["doctor_name"] = self.doctor.name if self.doctor else None
        result["comment"] = self.comment if self.comment else None
        result["created"] = self.created
        result["updated"] = self.updated
        return result


class BulkSMSLog(models.Model):
    appdetails = models.ForeignKey(AppDetails, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    count = models.IntegerField(default=0)

