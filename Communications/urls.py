from django.conf.urls import url

from Communications import views , exotel

urlpatterns = [
                       url(r'^licenseRenewEmail/',views.licenseRenewEmail,name='licenseRenewEmail'),
                       url(r'^getOTP/',views.getOTP,name='getOTP'),
                       url(r'^verifyOTP/',views.verifyOTP,name='verifyOTP'),
                       url(r'^sendSMS/',views.sendSMS,name='sendSMS'),
                       url(r'^sendReportEmailByReportID/', views.sendReportEmailByReportID, name='sendReportEmailByReportID'),

                        #welcome sms
                        url(r'^sendWelcomeSMS/',views.sendWelcomeSMS,name='sendWelcomeSMS'),
                        url(r'^sendVCReminderSMS/',views.sendVCReminderSMS,name='sendVCReminderSMS'),
                        url(r'^sendBulkSMS/', views.sendBulkSMS, name='sendBulkSMS'),

                        # Exotel
                        url(r'^make/outbound/call/', exotel.make_outbound_call, name='make_outbound_call'),
                        url(r'^add/to/whitelist/', exotel.add_to_whitelist, name='add_to_whitelist'),
                        url(r'^get/call/details/', exotel.get_call_details, name='get_call_details'),
                        url(r'^exotel_callback/', exotel.exotel_callback, name='exotel_callback'),

                        url(r'^smsuploadreportlink/', views.smsuploadreportlink, name='smsuploadreportlink'),

                        # doctor webapp email
                        url(r'^sendWebAppEmail/', views.sendWebAppEmail, name='sendWebAppEmail'),

]