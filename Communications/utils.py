__author__ = 'root'
from backend_new import config
import requests
import logging
import json
import urllib.request
import urllib.parse

requestlogger = logging.getLogger('apirequests')

def sendSMS(message_templateid, phone, senderid, unicode):

    if not config.SENDSMSNOTIFICATIONS or "localhost" in config.DOMAIN:
        return False

    message = message_templateid[0]
    templateid = message_templateid[1]


    if config.SMSSERVICE == "weberleads":
        if senderid == None:
            senderid = "GAMVTC"
        token = config.WEBERLEADS_TOKEN
        url = "http://weberleads.in/http-tokenkeyapi.php?"
        params = dict({"authentic-key": token, "senderid": senderid,
                       "route": 2, "number": phone,
                       "message": message})
        if unicode:
            params['unicode'] = 2
        response = requests.get(url, params)
        requestlogger.info("weberleads response:" + str(response.content))
        return response
    elif config.SMSSERVICE == "textlocal":
        data = urllib.parse.urlencode({'apikey': apikey, 'numbers': numbers,
                                       'message': message, 'sender': sender})
        data = data.encode('utf-8')
        request = urllib.request.Request("https://api.textlocal.in/send/?")
        f = urllib.request.urlopen(request, data)
        fr = f.read()
        requestlogger.info("textlocal response:" + str(fr))
        return fr
    elif config.SMSSERVICE == "sumit":
        if senderid == None:
            senderid = "GAMVTC"
        if unicode:
            url = "http://site.bulksmsnagpur.net/sendunicodesms?"
        else:
            url = "http://site.bulksmsnagpur.net/sendsms?"
        params = dict({"uname": config.SUMITUSERNAME,"pwd": config.SUMITPASSWORD ,"senderid": senderid,
                       "route": "T", "to": phone,"peid":"1401552490000023642","tempid":templateid,
                       "msg": message})

        response = requests.get(url, params)
        requestlogger.info("sumit response:" + str(response.content))
        return response


def createshorturl(longurl):
    requestlogger.info("creating short url for:"+longurl)
    linkRequest = {
        "destination": longurl
    }

    requestHeaders = {
        "Content-type": "application/json",
        "apikey": config.REBRANDLY_API_KEY,
    }

    r = requests.post("https://api.rebrandly.com/v1/links",
                      data=json.dumps(linkRequest),
                      headers=requestHeaders)

    shortURL = ""
    if (r.status_code == requests.codes.ok):
        link = r.json()
        shortURL = link["shortUrl"]
        requestlogger.info("Long URL was %s, short URL is %s" % (link["destination"], link["shortUrl"]))
    else:
        requestlogger.error("rebrandly error:"+str(r.content))
    return "https://" + shortURL

class dummyresponse(object):
    status_code = 1

    def __init__(self):
        return

BLANKREPORTDATA = dict({
    # Blood Glucose
    "glucose_level": "--",
    "glucose_levelRangeRandom": "--",
    "glucose_testtype": "--",
    "glucose_level_mmolPerL": "--",
    "glucose_levelRangeSmiley": "blankimg.png",
    # BMI
    "BMIrangesforage": {"underweight": "18.5",
                        "normal": "25",
                        "overweight": "30"},
    "bmi": "--",
    "bmiRange": "",
    "bmiSmiley": "blankimg.png",
    # FAT
    "fat": "--",
    "fatRange": "",
    "fatSmiley": "blankimg.png",
    # Hydration
    "hydration": "--",
    "hydrationRange": "",
    "hydrationSmiley": "blankimg.png",
    # Muscle
    "muscle": "--",
    "muscleRange": "",
    "muscleSmiley": "blankimg.png",
    "weight": "--",
    # Bonemass
    "bonemass": "--",
    "bonemassRange": "",
    "bonemassSmiley": "blankimg.png",
    # Blood Pressure
    "systolicRange":{
        "low":"< 90","normal":"90 - 120","prehyper":"120 - 129","high":"> 140"
    },
    "diastolicRange":{
        "low":"< 60","normal":"60 - 79","prehyper":"80 - 89","high":"> 90"
    },
    "systolic": "--",
    "diastolic": "--",
    "bpRange": "",
    "bpSmiley": "blankimg.png",
    # Pulse
    "pulseShowRange":{"normal":"60 - 90"},
    "pulse": "--",
    "pulseRange": "",
    "pulseSmiley": "blankimg.png",
    # Oxygen Saturation
    "oxygensat": "--",
    "oxygensatRange": "",
    "oxygensatSmiley": "blankimg.png",
    # tempereture
    "temperature": "--",
    "temperatureRange": "",
    "temperatureSmiley": "blankimg.png",
    # Hemoglobin
    "hb": "--",
    "hemoglobinRange": "",
    "hemoglobinSmiley": "blankimg.png",
    # Hematocrit
    "hct": "--",
    "hematocritRange": "",
    "hematocritSmiley": "blankimg.png",
    # Lipid data
    "hdl": "--",
    "ldl": "--",
    "hdlratio": "--",
    "tc": "--",
    "tg": "--",
    "lipidSmiley": "blankimg.png",
    # Eye Checkup Data
    "farvision": "--",
    "farvisionRange": "--",
    "nearvision": "--",
    "nearvisionRange": "--",
    "nearvisionJscore": "--",
    "nearvisionJscoreRange": "--",
    # Urine Test Color Dict
    # "ureinetestcolors":{
    #         "glucose":{
    #             "Negative" : "#ffffcc",
    #             "5 TRACE" : "#8CC487",
    #             "15 +" : "#7BB056",
    #             "30 ++" : "#8C9043",
    #             "60 +++" : "#817636",
    #             "110 ++++" : "#77553A"
    #         },
    #         "bilirubin":{
    #             "Negative" : "#F6E7A4",
    #             "SMALL +" : "#EDD8A3",
    #             "MODRATE ++" : "#C8C39B",
    #             "LARGE +++" : "#BFAD95"
    #         },
    #         "ketone":{
    #             "Negative" : "#D7B697",
    #             "TRACE 5" : "#E8AF9C",
    #             "SMALL 15" : "#D6807F",
    #             "MODRATE 30" : "#B26167",
    #             "LARGE 80" : "#864855",
    #             "LARGE 160":"#613640"
    #         },
    #         "gravity":{
    #             "1.000" : "#114948",
    #             "1.005" : "#267148",
    #             "1.010" : "#5F7C50",
    #             "1.015" : "#77883A",
    #             "1.020" : "#8C9336",
    #             "1.025" : "#8B8F30",
    #             "1.030" : "#B8A037"
    #         },
    #         "blood":{
    #             "Negative" : "#E5BE3F",
    #             "NON HEMOLYZED 10 TRACE" : "#E6BD38",
    #             "HEMOLYZED 10 TRACE" : "#C1BF44",
    #             "SMALL 25" : "#92AB45",
    #             "MODRATE 80" : "#578846",
    #             "LARGE 200" : "#39533A"
    #         },
    #         "ph":{
    #             "5.0" : "#F1814A",
    #             "6.0" : "#ECA452",
    #             "6.5" : "#DCC053",
    #             "7.0" : "#B5C15B",
    #             "7.5" : "#81A54D",
    #             "8.0" : "#4C9B6B",
    #             "8.5" : "#1C7A7C"
    #         },
    #         "protein":{
    #             "Negative" : "#DCE977",
    #             "TRACE" : "#BAD36D",
    #             "0.3 +" : "#A5C177",
    #             "1.0 ++" : "#90B991",
    #             "3.0 +++" : "#70AF9A",
    #             ">= 20.0 ++++" : "#599C8A"
    #         },
    #         "urobilinogen":{
    #             "3.2" : "#FDC382",
    #             "1.6" : "#F9A988",
    #             "1" : "#000000",
    #             "32" : "#F69C99",
    #             "64" : "#F48787",
    #             "128" : "#F48E9C"
    #         },
    #         "nitrite":{
    #             "Negative" : "#FFFFDA",
    #             "Positive" : "#FFCEC5",
    #             "Positive" : "#FFCEC5"
    #         },
    #         "leukocytes":{
    #             "Negative" : "#F6E7A4",
    #             "TRACE 15" : "#F6E7A4",
    #             "SMALL 70" : "#BCB7A3",
    #             "MODRATE 125" : "#8F7590",
    #             "LARGE 500" : "#816490"
    #         }
    #     },
    # Healthscore
    "healthscore": "--",
    "healthscoreSmiley": "blankimg.png",
})

# reportdata: "{"name":"Guest","age":31,"gender":"Male","kiosklocation":"Yolo-Baner","height":"5'9\"","weight":80.7,"date":"16 Nov 2018","bmi":26.27,"fat":25.28,"vfat":12,"muscle":38.97,"hydration":55.06,"kcal":1690.54,"bage":37.4,"prot":17.25,"bonemass":4.07,"BMIrangesforage":{"underweight":18,"normal":27,"overweight":30},"bmiRange":"Normal","bmrtext":"You have healthy BMR","nutrientRange":{"normal":16,"high":18},"bageRange":{"good":34,"optimal":37},"muscleWeight":31.44879,"fatRange":"Overweight","muscleRange":"Normal","hydrationRange":"Normal","bonemassRange":"Normal","bageRangeinf":"Bad","vfatRange":"High","protRangeinf":"Ideal","systolic":130,"diastolic":80,"pulse1":0,"systolicRange":{"low":90,"normal":130,"prehyper":140},"diastolicRange":{"low":60,"normal":90,"prehyper":100},"sysbpRange":"Normal","diabpRange":"Normal","pulse":90,"oxygensat":90,"pulseShowRange":{"low":60,"normal":90},"pulseRange":"High","oxygensatRange":"Normal","temperature":"97.0","temperatureincelsius":"36.1","temperatureRange":"Normal","healthscore":"83","healthscoreSmiley":"emoticon.png","location":"Yolo-Baner","partnerphone":"1800 1020 333","partneremail":"support@yolohealth.in","partnerwebsite":"www.yolohealth.in","partneraddress":"Yolohealth, Plot-3A, Balewadi Phata,Baner,Pune"}"
