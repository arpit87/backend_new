DATABASE_HOST=$2
ENC_PASS=$3
# Path in which to create the backup (will get cleaned later)
BACKUP_PATH="/home/yolo/mongodump/"

# DB name
DB_NAME="yolonotifdb"

# Google Cloud Storage Bucket Name
BUCKET_NAME=$1

# Backup filename
DAY=$(date +"%d")
echo "Date: $DAY"
FILENAME=mongodbbackupfordate$DAY

# Create the backup
mongodump --host $DATABASE_HOST --db $DB_NAME -o ${BACKUP_PATH}${FILENAME}
echo "Done backing up the database to a file."

rm "${$BACKUP_PATH}${FILENAME}.7z"
echo "Removed old compressed file."

echo "Starting compression of new file..."

cd $BACKUP_PATH

# Archive and compress
7z a -p${ENC_PASS} -mx=9 -mhe -t7z ${BACKUP_PATH}${FILENAME}.7z ${BACKUP_PATH}${FILENAME}


echo "Done compressing the backup file."

echo "Uploading the new backup..."
gsutil cp ${BACKUP_PATH}${FILENAME}.7z gs://${BUCKET_NAME}/
echo "New backup uploaded."
rm -r ${BACKUP_PATH}${FILENAME}
echo "All done."

