#!/home/yolo/yolo_server/yolovenv/bin/python2.7

# ----------------- Setup django environment -----------------
import os, sys, django
project_path = os.path.split(os.path.dirname(os.path.realpath(__file__)))[-2] # path to manage.py
sys.path.append(project_path)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "backend_new.settings")
django.setup()

from Platform import utils
from WebApp.models import AppTemplateDetails
from backend_new import config
import sys
from Communications.utils import sendSMS
from Communications.smstemplates import getwelcometemplate


def main():
    filename = sys.argv[1]
    print("opening file:"+ filename)
    with open("/home/arpit/Downloads/"+filename) as f:
        index = 0
        for line in f:
            if line != "" and len(line) == 11:
                phone = line[:-1]
                index = index + 1


                done = sendSMS(getwelcometemplate(0, "Pulse Clinic", "http://gamv.tech/jntgeh9"), phone, "GAMVTC", False)

                if done:
                    print(str(index) + " sent sms to:" + phone)
                else:
                    print(str(index) + " error send sms to:" + phone)


if __name__ == '__main__':
    main()
