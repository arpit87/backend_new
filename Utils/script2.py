#!/home/yolo/yolo_server/yolovenv/bin/python2.7

# ----------------- Setup django environment -----------------
import os, sys, django


project_path = os.path.split(os.path.dirname(os.path.realpath(__file__)))[-2] # path to manage.py
sys.path.append(project_path)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "backend_new.settings")
django.setup()


from Person.models import PersonDetails
from Doctor.models import DoctorDetails,  AppDetails
from HealthCase.models import HealthCase, commentshealthcase, attachedreporthealthcase
from HealthPackage.models import HealthPackageEnrollment
from Payment.models import HealthPackageInvoiceDetails
from Platform.uid_code_converter import encode_id, decode_uidcode
import BodyVitals.models

def main():
    doctorsconsulted()
    appsinteracted()
    # healthpackageinvoice()
    # healthpackageid()
    # healthcaseid()
    # appuid()
    # reportid()
    # perscriptoinid()
    # imagereportid()
    # doctoruid()
    # personuid()
    # commenteruid()
    # uploaderuid()

def doctorsconsulted():
    allhealthcase = HealthCase.objects.all()
    for thishealthcase in allhealthcase:
        if thishealthcase.doctor is not None:
            thishealthcase.doctorsconsulted.add(thishealthcase.doctor)

    print("doctorsconsulted done")

def appsinteracted():
    allperson = PersonDetails.objects.all()
    for person in allperson:
        if person.appuid:
            appdetails = AppDetails.objects.get(appuid = person.appuid)
            person.appsinteracted.add(appdetails)
            person.save()
    print("person appsinteracted done")

def healthpackageinvoice():
    allhealthpackageinvoice = HealthPackageInvoiceDetails.objects.all()
    for thishealthpackageinvoice in allhealthpackageinvoice:
        thishealthpackageinvoice.invoicedetailsarray = thishealthpackageinvoice.invoicedetailsarray.replace("'", '"')
        thishealthpackageinvoice.save()
    print("healthpackageinvoice done")

def healthpackageid():
    allhealthpackageenrollment = HealthPackageEnrollment.objects.all()
    for thishealthpackage in allhealthpackageenrollment:
        thishealthpackage.enrolluid = encode_id(thishealthpackage.enrollid)
        thishealthpackage.save()
    print("healthpackage done")

def healthcaseid():
    allhealthcase = HealthCase.objects.all()
    for thishealthcase in allhealthcase:
        thishealthcase.healthcaseid = encode_id(thishealthcase.id)
        thishealthcase.save()
    print("healthcaseid done")

def appuid():
    allapps = AppDetails.objects.all()
    for app in allapps:
        app.appuid = encode_id(app.appid)
        app.save()
        allpersonthisapp = PersonDetails.objects.filter(appid = app.appid)
        for person in allpersonthisapp:
            person.appuid = app.appuid
            person.save()
    print("appuid done")


def reportid():
    allreport = BodyVitals.models.report.objects.all()
    for report in allreport:
        oldreportid = report.reportid
        newreportid = encode_id(report.id)
        # infd if this report attached to any healthcase
        allattachedreports = attachedreporthealthcase.objects.filter(attachedreportid = oldreportid, attachedreporttype = 1)
        for thisattachedreport in allattachedreports:
            thisattachedreport.attachedreportid = newreportid
            thisattachedreport.save()

        report.reportid = newreportid
        report.save()
    print("reportid done")


def perscriptoinid():
    allpres = BodyVitals.models.prescription.objects.all()
    for pres in allpres:
        oldreportid = pres.prescriptionid
        newreportid = encode_id(pres.id)
        # infd if this report attached to any healthcase
        allattachedreports = attachedreporthealthcase.objects.filter(attachedreportid=oldreportid,
                                                                     attachedreporttype=2)
        for thisattachedreport in allattachedreports:
            thisattachedreport.attachedreportid = newreportid
            thisattachedreport.save()

        pres.prescriptionid = newreportid
        pres.save()
    print("prescriptionid done")


def imagereportid():
    allimages = BodyVitals.models.imagereportdetails.objects.all()
    for thisimage in allimages:
        oldreportid = thisimage.imagereportid
        newreportid = encode_id(thisimage.id)
        # infd if this report attached to any healthcase
        allattachedreports = attachedreporthealthcase.objects.filter(attachedreportid=oldreportid,
                                                                     attachedreporttype=3)
        for thisattachedreport in allattachedreports:
            thisattachedreport.attachedreportid = newreportid
            thisattachedreport.save()

        thisimage.imagereportid = newreportid
        thisimage.save()
    print("imagereportid done")


def doctoruid():
    alldoctor = DoctorDetails.objects.all()
    for doctor in alldoctor:
        if doctor.doctorid:
            doctor.doctoruid = encode_id(doctor.doctorid)
            doctor.save()
    print("doctoruid done")


def personuid():
    allperson = PersonDetails.objects.all()
    for person in allperson:
        if person.personid:
            person.personuid = encode_id(person.personid)
            person.save()
    print("persionuid done")


def commenteruid():
    allcomments = commentshealthcase.objects.all()
    for comment in allcomments:
        if comment.commenterid:
            comment.commenteruid = encode_id(comment.commenterid)
            comment.save()
    print("commenteruid done")


def uploaderuid():
    allattachedreports = attachedreporthealthcase.objects.all()
    for report in allattachedreports:
        if report.uploaderid:
            report.uploaderuid = encode_id(report.uploaderid)
            report.save()
    print("uploaderuid done")


if __name__ == '__main__':
    main()