from Doctor.models import DoctorDetails
from django.test import TestCase, override_settings
import backend_new.config
from Person.models import PersonDetails
import os
# Create your tests here.

class UtilsAPITestCase(TestCase):
	multi_db = True
	def setUp(self):
		backend_new.config.ISTESTMODE = True
		os.environ.setdefault("GOOGLE_APPLICATION_CREDENTIALS", "/home/yolo/yolo_server/yoloprod-servicekey.json")
		# medicine.objects.create(medicineid=1,generic_name="ascorbic",medicine_name="ascorbic acid")
		# citygeobounds.objects.create(cityname="Pune",lowlongitude=73.740732,highlongitude=73.986551,lowlatitude=18.414499,highlatitude=18.634560)

	def test_getSliderImageList(self):
		response = self.client.get('/Utils/getSliderImage/')
		self.assertEqual(response.status_code,200)
		

	def test_getMedicineSuggestion(self):
		input = {"searchword":"abc"}
		response = self.client.get('/Utils/getMedicineSuggestion/',input)
		self.assertEqual(response.status_code,200)
		

	def test_getMedicineSuggestionNew(self):
		input = {"searchword":"a"}
		response = self.client.get("/Utils/getMedicineSuggestionNew/",input)
		#as right now no database is inputed in setup
		self.assertEqual(response.status_code,200)
		

	def test_getMedicinedetails(self):
		input ={"medicineid":1}
		response = self.client.get("/Utils/getMedicinedetails/",input)
		self.assertEqual(response.status_code,200)

	def test_getCityFromLatLong(self):
		input ={"latitude":18.446700,"longitude":73.751232}
		response = self.client.get("/Utils/getCityFromLatLong/",input)
		self.assertEqual(response.status_code,200)
		print(response.content)
		