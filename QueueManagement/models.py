from __future__ import unicode_literals
from datetime import datetime

from django.db import models
from Person.models import PersonDetails
from Doctor.models import DoctorDetails, AppDetails
from django.forms.models import model_to_dict
from backend_new.Constants import  ServerConstants
from django.contrib.auth.models import User
# Create your models here.



class TVAppDetails(models.Model):
    tvappid = models.AutoField(primary_key=True, null=False)
    appdeviceid = models.CharField(max_length=20, null=True, blank=True)
    applocation = models.CharField(max_length=50, null=True, blank=True)
    apptag = models.CharField(max_length=50, null=True, blank=True)
    apptype = models.IntegerField(null=True, blank=True)  #1 for WebApp
    appdescription = models.CharField(max_length=255, null=True, blank=True)
    licensestarttime = models.CharField(max_length=12, null=True, blank=True)
    licenseendtime = models.CharField(max_length=12, null=True, blank=True)
    licenseenabled = models.BooleanField(default=False)
    licensekey = models.CharField(max_length=128, null=True,blank=True)
    djangouser = models.OneToOneField(User,null=True, on_delete=models.CASCADE)
    partner = models.CharField(max_length=50)
    queueappdetails = models.ForeignKey(AppDetails,null=True, on_delete=models.CASCADE)

    def __str__(self):
      return '{}-{}'.format(self.tvappid, self.applocation)

    def islicensevalid(self):
        timenow = datetime.now()
        if self.licensestarttime is not None and self.licenseendtime is not None and self.licenseenabled:
            licensestarttime = datetime.strptime(self.licensestarttime, '%m/%d/%Y')
            licenseendtime = datetime.strptime(self.licenseendtime, '%m/%d/%Y')
            if licensestarttime < timenow and licenseendtime > timenow :
                return True
            else:
                return False
        else:
            return False

#doctor!= None and appdetails=None: waiting queue for doctor
#doctor== None and appdetails!=None: waiting queue for app for health chechup
class WaitingQueue(models.Model):
    WAITING = 1
    COMPLETED = 2
    REMOVED = 3
    ENGAGED = 4
    DIED = 5
    DROPPED = 6
    STATUSES = ((WAITING, 'Waiting'), (ENGAGED,'Engaged'),(COMPLETED,'Completed'), (REMOVED,'Removed'), (DIED,'Died') , (DROPPED,'Dropped'))

    active = models.BooleanField(default=True)
    status = models.IntegerField(choices=STATUSES, default=WAITING)
    doctor = models.ForeignKey(DoctorDetails, null=True, on_delete=models.CASCADE)
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    appdetails = models.ForeignKey(AppDetails, on_delete=models.CASCADE, null=True)
    token_number = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    engaged_at = models.DateTimeField(null=True)
    completed_at = models.DateTimeField(null=True)

    def __str__(self):
       return '{}-{}'.format(str(self.person.name),str(self.get_status_display()))

    # class Meta:
    #     unique_together = ('doctor', 'person', 'status', 'active')

    def get_todays_waiting_list(self, Doctor=None):
        start = datetime.today().replace(hour=00, minute=00, second=00)
        end = datetime.today().replace(hour=23, minute=59, second=59)
        kwargs = {}
        kwargs['created_at__range'] = (start, end)
        if Doctor:
            kwargs['doctor'] = Doctor
        res = WaitingQueue.objects.filter(**kwargs)
        return res

    def get_waiting_count(self):
        start = datetime.today().replace(hour=00, minute=00, second=00)
        end = datetime.today().replace(hour=23, minute=59, second=59)
        res = WaitingQueue.objects.filter(created_at__range=(start, end))
        return res

    def get_dict(self):
        result = {}
        result['id'] = self.id
        result['status'] = self.status
        result['status_name'] = self.get_status_display()
        result[ServerConstants.USERNAME] = self.person.name
        result[ServerConstants.AGE] = self.person.get_age()
        result[ServerConstants.PERSONUID] = self.person.personuid
        result[ServerConstants.PHONE] = self.person.phone
        result[ServerConstants.GENDER] = self.person.gender
        result[ServerConstants.PROFILEPIC] = self.person.profilepic
        result[ServerConstants.TOKENNUMBER] = self.token_number
        result['created_at'] = self.created_at.strftime('%d %b %Y %H:%M')
        return result
