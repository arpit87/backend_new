import logging

from asgiref.sync import async_to_sync
from django.shortcuts import render
from django.utils.safestring import mark_safe
import json
from datetime import datetime
from django.http import HttpResponse

from QueueManagement.models import  WaitingQueue
from Person.models import PersonDetails
from Doctor.models import DoctorAppMapping, DoctorDetails , AppDetails
from backend_new.Constants import ServerConstants
from backend_new.config import DOCTORCONSULT_DEFAULT_WAITING_TIME, HEALTHCHECKUP_DEFAULT_WAITING_TIME
from Platform.views import authenticateURL
from Platform import backendlogger, utils
import Communications.utils
from channels.layers import get_channel_layer
from VideoConference.models import VCCodeDetails
import pylibmc

requestlogger = logging.getLogger('apirequests')


def get_doctors(request):
    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('get_doctors', logrequest, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        appuid = request.GET[ServerConstants.APPUID]
    except KeyError as e:
        logmessage.append("WebApp Id not sent")
        backendlogger.error('get_doctors', logrequest, logmessage, logmessage)
        return HttpResponse("WebApp Id not sent", status=403)

    try:
        consultation_type = request.GET[ServerConstants.CONSULTATIONTYPE]
    except KeyError as e:
        logmessage.append("consultation_type not sent")
        backendlogger.error('get_doctors', logrequest, logmessage, logmessage)
        return HttpResponse("consultation_type not sent", status=403)

    print('appuid: ', appuid)
    doctor_mappings = DoctorAppMapping.objects.filter(appdetails__appuid=appuid, consultation_type=consultation_type)
    data = []
    for doctor_mapping in doctor_mappings:
        waiting_list = get_todays_waiting_list(doctor_mapping.doctor, appuid,WaitingQueue.WAITING)
        item = doctor_mapping.get_mapping()
        item['waiting_count'] = waiting_list.count()
        item['waiting_list'] = [patient.get_dict() for patient in waiting_list]
        data.append(item)

    data.sort(key=lambda x: -x['waiting_count'])
    httpoutput = utils.successJson(dict({ServerConstants.DOCTORLIST:data}))
    backendlogger.info('get_doctors', logrequest, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


def generate_token(request):
    data = json.loads(request.body)
    logmessage = []
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('generate_token', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        doctoruid = data[ServerConstants.DOCTORUID]
        doctor = DoctorDetails.objects.get(doctoruid=doctoruid)
    except Exception as e:
        doctor =  None

    try:
        appuid = data[ServerConstants.APPUID]
        appdetails = AppDetails.objects.get(appuid=appuid)
    except Exception as e:
        appdetails = None

    person = None
    try:
        vccode_req = data[ServerConstants.VCCODE]
        date_req = data[ServerConstants.DATE]
        vcdetails = VCCodeDetails.objects.get(vccode=vccode_req, date=date_req)
        doctor = vcdetails.slotbooking.doctoravailability.doctor
        person = vcdetails.slotbooking.person
    except Exception as e:
        pass

    if not appdetails and not doctor:
        logmessage.append("Send atleast one of appid/doctoruid/vccode")
        backendlogger.error('generate_token', data, logmessage, logmessage)
        return HttpResponse("appid/doctoruid", status=403)

    if person is None:
        try:
            personuid = data[ServerConstants.PERSONUID]
            person = PersonDetails.objects.get(personuid=personuid)
        except Exception as e:
            logmessage.append("User not found")
            backendlogger.error('generate_token', data, logmessage, logmessage)
            return HttpResponse("User not found", status=403)

    start = datetime.today().replace(hour=00, minute=00, second=00)
    end = datetime.today().replace(hour=23, minute=59, second=59)

    cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})

    #if generate token called again for same user then retuen his existig token
    try:
        waitingobject = WaitingQueue.objects.get(doctor=doctor, person=person, appdetails = appdetails, created_at__range=(start, end),status__in=[WaitingQueue.WAITING,WaitingQueue.ENGAGED])
        logmessage.append("User already in waiting list")
        if doctor is not None:
            cache.set("doctor" + str(doctor.doctoruid) + "q"+str(waitingobject.token_number), person.personuid, 60) # 30 sec
            queue_number = calc_queue_number(waitingobject.token_number, "doctor"+str(doctor.doctoruid)+"q")
            eta = DOCTORCONSULT_DEFAULT_WAITING_TIME * queue_number
        else:
            cache.set("app" + str(appdetails.appuid) + "q" + str(waitingobject.token_number), person.personuid, 60*60) # 3600 sec
            queue_number = calc_queue_number(waitingobject.token_number, "app"+str(appdetails.appuid)+"q")
            eta = HEALTHCHECKUP_DEFAULT_WAITING_TIME * queue_number
        data = {ServerConstants.ETA: eta, ServerConstants.TOKENNUMBER: waitingobject.token_number,
                ServerConstants.QUEUE_NUMBER: queue_number}
        httpoutput = utils.successJson(data)
        backendlogger.info('generate_token', data, httpoutput, logmessage)
        return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)
    except:
        pass

    todays_list_count = WaitingQueue.objects.filter(doctor=doctor, appdetails=appdetails, created_at__range=(start, end)).count()

    try:
        token_number = todays_list_count+1
        WaitingQueue.objects.create(doctor=doctor, person=person, appdetails=appdetails, token_number=token_number, status=WaitingQueue.WAITING)
        if doctor is not None:
            # set low expiry for doctorq cache as we update it by heartbeat
            cache.set("doctor" + str(doctor.doctoruid) + "q"+str(token_number), person.personuid, 60) # 30 sec
            queue_number = calc_queue_number(token_number, "doctor" + str(doctor.doctoruid) + "q")
        else:
            # appq is manually managed so keep expiry high
            cache.set("app" + str(appdetails.appuid) + "q" + str(token_number), person.personuid, 60*60) # 3600 sec
            queue_number = calc_queue_number(token_number, "app" + str(appdetails.appuid) + "q")
    except Exception as e:
        logmessage.append(str(e))
        backendlogger.error('generate_token', data, logmessage, logmessage)
        return HttpResponse("Error while adding user to waiting list", status=403)

    if doctor:
        eta = DOCTORCONSULT_DEFAULT_WAITING_TIME * queue_number
    else:
        eta = HEALTHCHECKUP_DEFAULT_WAITING_TIME*queue_number

    jsondata = {ServerConstants.ETA: eta, ServerConstants.TOKENNUMBER: token_number, ServerConstants.QUEUE_NUMBER: queue_number}

    # update existing connection of new entry in queue
    channel_layer = get_channel_layer()
    msg = {"type": "group.message", "command": "update_queue", }
    if appdetails is not None:
        print('sending msg to group:{}, msg:{}'.format("app"+str(appdetails.appuid), str(msg)))
        async_to_sync(channel_layer.group_send)("app"+str(appdetails.appuid)+"q",msg)
        msg = "Your token number is {} and your expected waiting time is {} minutes".format(token_number, eta/60)
        Communications.utils.sendSMS(msg, person.phone, appdetails.appsmssenderid, None)
    else:
        async_to_sync(channel_layer.group_send)("doctor"+str(doctor.doctoruid)+"q",msg)

    if doctor is not None:
        print('sending msg to group:{}, msg:{}'.format("doctorq"+str(doctor.doctoruid), str(msg)))
        async_to_sync(channel_layer.group_send)("doctorq"+str(doctor.doctoruid),msg )

    httpoutput = utils.successJson(jsondata)
    backendlogger.info('generate_token', data, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

def calc_queue_number(for_token_number,queuename) :
    cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
    # we will check format <queuename>_<token_number> existance above for_token_number
    queue_number = 0
    for i in range(1,for_token_number + 1):  # range(1,n) goes from 1 to n-1
        if cache.get(queuename+str(i)) is not None:
            requestlogger.info("got : " + queuename+str(i))
            queue_number = queue_number + 1

    return queue_number

def get_todays_waiting_list(doctor, appuid, status):
    start = datetime.today().replace(hour=00, minute=00, second=00)
    # end = person.created_at
    end = datetime.today().replace(hour=23, minute=59, second=59)
    kwargs = {}
    kwargs['created_at__range'] = (start, end)
    kwargs['status'] = status
    if appuid:
        kwargs['appdetails__appuid'] = appuid
    if doctor:
        kwargs['doctor'] = doctor
    res = WaitingQueue.objects.filter(**kwargs).order_by('id')
    return res

def getAllWaitlistUsers(request):
    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getAllWaitlistUsers', logrequest, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    kwargs = {}
    appdetails = None
    doctor = None

    try:
        appuid_req = request.GET[ServerConstants.APPUID]
        appdetails = AppDetails.objects.get(appuid=appuid_req)
        kwargs['appdetails'] = appdetails
    except Exception as e:
        pass

    try:
        doctoruid_req = request.GET[ServerConstants.DOCTORUID]
        doctor = DoctorDetails.objects.get(doctoruid=doctoruid_req)
        kwargs['doctor'] = doctor
    except Exception as e:
        pass

    if not appdetails and not doctor:
        logmessage.append("Send atleast one of appid/doctoruid")
        backendlogger.error('generate_token', logrequest, logmessage, logmessage)
        return HttpResponse("appid/doctoruid", status=403)

    start = datetime.today().replace(hour=00, minute=00, second=00)
    end = datetime.today().replace(hour=23, minute=59, second=59)
    kwargs['created_at__range'] = (start, end)

    allwaitlist = WaitingQueue.objects.filter(**kwargs).order_by('id')
    data = []
    for queue_person in allwaitlist:
        data.append(queue_person.get_dict())

    httpoutput = utils.successJson(dict({ServerConstants.PERSONLIST: data}))
    backendlogger.info('getAllWaitlistUsers', logrequest, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

def get_waiting_persons(request):
    data = json.loads(request.body)
    logmessage = []
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('get_waiting_persons', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        doctoruid = data[ServerConstants.DOCTORUID]
        doctor = DoctorDetails.objects.get(doctoruid=doctoruid)
    except Exception as e:
        doctor = None

    try:
        appuid = data[ServerConstants.APPUID]
    except Exception as e:
        appuid = None

    if not appuid and not doctor:
        logmessage.append("Send atleast one of appid/doctoruid")
        backendlogger.error('get_waiting_persons', data, logmessage, logmessage)
        return HttpResponse("atleast appid/doctoruid", status=403)

    status_req = None
    try:
        status_req = data[ServerConstants.STATUS]
        final_list = get_todays_waiting_list(doctor, appuid, status_req)
    except Exception as e:
        waiting_list = get_todays_waiting_list(doctor, appuid, WaitingQueue.WAITING)
        engaged_list = get_todays_waiting_list(doctor, appuid, WaitingQueue.ENGAGED)
        completed_list = get_todays_waiting_list(doctor, appuid, WaitingQueue.COMPLETED)
        final_list = engaged_list | waiting_list | completed_list

    final_list = final_list.order_by('token_number')
    data = []
    cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
    for queue_person in final_list:
        if status_req == WaitingQueue.WAITING and doctor != None:
            # check waiting status in cache too if its doctor i,e waitlist for VC
            if cache.get("doctor" + str(doctor.doctoruid) + "q" + str(queue_person.token_number)) == None:
                # means this person didnt sent heartbeat so this object has expired in cache so set its status to died
                queue_person.status = WaitingQueue.DIED
                queue_person.save()
                continue

        data.append(queue_person.get_dict())

    httpoutput = utils.successJson(dict({ServerConstants.PERSONLIST:data}))
    backendlogger.info('get_waiting_persons', data, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


def toggle_patient_queue_status(request):
    data = json.loads(request.body)
    logmessage = []
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('toggle_patient_queue_status', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        personuid_req = data[ServerConstants.PERSONUID]
        person_req = PersonDetails.objects.get(personuid = personuid_req)
    except Exception as e:
        logmessage.append("personuid not found")
        backendlogger.error('toggle_patient_queue_status', data, logmessage, logmessage)
        return HttpResponse("personuid not found", status=403)

    try:
        doctoruid_req = data[ServerConstants.DOCTORUID]
        doctor_req = DoctorDetails.objects.get(doctoruid = doctoruid_req)
    except :
        doctor_req = None
        doctoruid_req = None

    try:
        appuid_req = data[ServerConstants.APPUID]
        appdetails_req = AppDetails.objects.get(appuid=appuid_req)
    except:
        appdetails_req = None

    try:
        status = data[ServerConstants.STATUS]
    except Exception as e:
        logmessage.append("status not found")
        backendlogger.error('toggle_patient_queue_status', data, logmessage, logmessage)
        return HttpResponse("status not found", status=403)

    start = datetime.today().replace(hour=00, minute=00, second=00)
    end = datetime.today().replace(hour=23, minute=59, second=59)
    try:
        queue_obj = WaitingQueue.objects.get( person=person_req, doctor=doctor_req , appdetails=appdetails_req , created_at__range=(start, end), status__in=[WaitingQueue.WAITING, WaitingQueue.ENGAGED])
        queue_obj.status = status
        if status == WaitingQueue.ENGAGED:
            queue_obj.engaged_at = datetime.now()
        elif status == WaitingQueue.DROPPED: # this happens in doctor queue only
            queue_obj.status = WaitingQueue.WAITING  # we put him again in waiting queue
            # increase the waiting queue cache by 1.5 min to allow patient to call back without getting out of queue
            cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
            token_number = queue_obj.token_number
            doctorq = "doctor" + str(doctor_req.doctoruid) + "q"
            cache.touch(doctorq + str(token_number), 90)
        elif status == WaitingQueue.REMOVED:
            cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
            cache.delete("doctor" + str(doctoruid_req) + "q" + str(queue_obj.token_number))
        elif status == WaitingQueue.COMPLETED:
            queue_obj.completed_at = datetime.now()
            # delete from cache if its doctor queue
            if doctor_req is not None:
                cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
                cache.delete("doctor"+str(doctoruid_req)+"q"+str(queue_obj.token_number))

        queue_obj.save()
    except Exception as e:
        print('Exception ', e)
        logmessage.append(str(e))
        backendlogger.error('toggle_patient_queue_status', data, logmessage, logmessage)
        return HttpResponse("Inconsistent data", status=403)

    channel_layer = get_channel_layer()
    msg = {"type": "group.message", "command": "update_queue", }
    if doctor_req != None:
        doctorq = "doctor" + str(queue_obj.doctor.doctoruid)+"q"
        print('sending msg to group:{}, msg:{}'.format( doctorq, str(msg)))
        async_to_sync(channel_layer.group_send)(doctorq, msg)
    else:
        appq = "app" + str(queue_obj.appdetails.appuid)+"q"
        print('sending msg to group:{}, msg:{}'.format(appq, str(msg)))
        async_to_sync(channel_layer.group_send)(appq, msg)

    httpoutput = utils.successJson(dict())
    backendlogger.info('toggle_patient_queue_status', data, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

def get_current_token(request):
    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value

    try:
        appuid = request.GET[ServerConstants.APPUID]
    except Exception as e:
        backendlogger.error('WebApp ID not sent', logrequest, logmessage, logmessage)
        return HttpResponse("WebApp ID not sent", status=403)

    try:
        moverid = request.GET[ServerConstants.DOCTORUID]
        mover = DoctorDetails.objects.get(moverid=moverid)
    except Exception as e:
        mover = None

    start = datetime.today().replace(hour=00, minute=00, second=00)
    # end = person.created_at
    end = datetime.today().replace(hour=23, minute=59, second=59)
    current_token_number = WaitingQueue.objects.filter(created_at__range=(start,end),appdetails__appuid=appuid,doctor=mover).exclude(status=WaitingQueue.WAITING).count()

    person_token_number = None
    eta = None
    try:
        personuid = request.GET[ServerConstants.PERSONUID]
        try:
            person_token_number = WaitingQueue.objects.get(created_at__range=(start,end),appdetails__appuid=appuid,doctor=mover,person__personuid=personuid,status=WaitingQueue.WAITING).token_number
        except Exception as e:
            person_token_number = None
    except Exception as e:
        pass

    if person_token_number:
        eta = HEALTHCHECKUP_DEFAULT_WAITING_TIME*(person_token_number-current_token_number)

    data = {"current_token_number": current_token_number, "person_token_number": person_token_number, "eta": eta}
    httpoutput = utils.successJson(data)
    backendlogger.info('get_current_token', logrequest, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)
