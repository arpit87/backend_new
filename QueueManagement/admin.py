from QueueManagement.models import WaitingQueue, TVAppDetails
from django.contrib import admin


class WaitlistModelAdmin(admin.ModelAdmin):
    list_display = ('doctor','person','token_number','appdetails','status', 'created_at', 'updated_at')

admin.site.register(WaitingQueue, WaitlistModelAdmin)


class TVAppDetailsModelAdmin(admin.ModelAdmin):
    list_display = ('tvappid','appdeviceid','applocation','apptag','apptype','appdescription','licensestarttime','licenseendtime')

admin.site.register(TVAppDetails, TVAppDetailsModelAdmin)

