from django.conf.urls import url
from django.urls import path
from QueueManagement import views

urlpatterns = [
    url(r'^getDoctors',views.get_doctors,name='getDoctors'),
    url(r'^generateToken',views.generate_token,name='generateToken'),
    url(r'^getWaitingPersons', views.get_waiting_persons, name='getWaitingPersons'),
    url(r'^getAllWaitlistUsers',views.getAllWaitlistUsers,name='getAllWaitlistUsers'),
    url(r'^get/current/token/',views.get_current_token,name='get_current_token'),
    url(r'^togglePersonQueueStatus',views.toggle_patient_queue_status,name='togglePersonQueueStatus'),
 ]

         