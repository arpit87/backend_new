import asyncio
import json

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer, AsyncWebsocketConsumer
from django.core.exceptions import ObjectDoesNotExist
from firebase_admin._messaging_utils import UnregisteredError
from requests import HTTPError
from Platform.fcm_message import send_to_token
from Platform.models import DoctorFCMTokenMapping
from QueueManagement.models import TVAppDetails
from Doctor.models import AppDetails
from channels.db import database_sync_to_async
from QueueManagement.views import calc_queue_number
from backend_new import config
from backend_new.Constants import ServerConstants
from firebase_admin._messaging_utils import UnregisteredError
import pylibmc
import logging
websocketlogger = logging.getLogger('websocket')

class WaitingQueueConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        print("connected")
        deviceid = self.scope['url_route']['kwargs']['deviceid']
        print('Connecting to device:'+str(deviceid))
        queueappid = await self.get_queue_app_id(deviceid)
        if queueappid is None:
            await self.send(text_data=json.dumps(
                {ServerConstants.MESSAGE_TYPE: 'connection_message', 'status': 'queue app not found'}
            ))
            await self.disconnect()
        else:
            appq = "app" + str(queueappid) + "q"
            print('adding to group:{}, channel:{}'.format(appq,self.channel_name))
            await self.channel_layer.group_add( appq, self.channel_name )
            await self.accept()
            await self.send(text_data=json.dumps(
                {ServerConstants.MESSAGE_TYPE: 'connection_message', 'status': 'connected', 'queueappid':queueappid }
            ))

    @database_sync_to_async
    def get_queue_app_id(self,deviceid):
        appuid = None
        try:
            appuid =  AppDetails.objects.get(appdeviceid=deviceid).appuid
        except:
            tvapp =TVAppDetails.objects.get(appdeviceid = deviceid)
            appuid = tvapp.queueappdetails.appuid
        finally:
            return appuid

    async def receive(self, **kwargs):
        print("receive", kwargs['text_data'])
        text_data_json = json.loads(kwargs['text_data'])
        message = text_data_json['message']
        deviceid = self.scope['url_route']['kwargs']['deviceid']
        print('Got message from device:{}, msg:{}'.format(str(deviceid),message))
        await self.send(text_data=json.dumps({
            'message': message
        }))

    async def disconnect(self, close_code):
        print("disconnect", close_code)
        deviceid = self.scope['url_route']['kwargs']['deviceid']
        print('disconnecting from device:' + str(deviceid))
        queueappid = await self.get_queue_app_id(deviceid)
        if queueappid is not None:
            print('removing from group:{}, channel:{}'.format("app"+str(queueappid)+"q", self.channel_name))
            await self.channel_layer.group_discard(
                "app"+str(queueappid)+"q",
                self.channel_name
            )

    async def group_message(self, message):
        print("got group msg:"+str(message))
        if message['command'] == "update_queue":
            await self.send(text_data=json.dumps(
                {ServerConstants.MESSAGE_TYPE: 'queue_message', 'command': 'update'}
            ))

class DoctorWaitingQueueConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        websocketlogger.info("connected")
        doctoruid = self.scope['url_route']['kwargs']['doctoruid']
        doctorq = "doctor"+doctoruid+"q"
        print('Connecting to queue:'+ doctorq)
        print('adding to group:{}, channel:{}'.format(doctorq,self.channel_name))
        await self.channel_layer.group_add(doctorq, self.channel_name )
        await self.accept()
        await self.send(text_data=json.dumps(
            {ServerConstants.MESSAGE_TYPE: 'connection_message', 'status': 'connected', 'doctorq':doctoruid }
        ))

    async def receive(self, **kwargs):
        websocketlogger.info("receive:", kwargs['text_data'])
        doctoruid = self.scope['url_route']['kwargs']['doctoruid']
        text_data_json = json.loads(kwargs['text_data'])
        try:
            type = text_data_json[ServerConstants.MESSAGE_TYPE]
            websocketlogger.info('Got message for que of doctor doctoruid:{}, type:{}'.format(doctoruid,type))
            cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
            if type == "heartbeat":
                token_number = text_data_json[ServerConstants.TOKENNUMBER]
                doctorq = "doctor"+doctoruid+"q"
                cache.touch(doctorq+str(token_number),30)  #increase the expiry by 30 sec
                queue_number = calc_queue_number(token_number,doctorq)
                # get doctor online status
                status = cache.get("onlinestatusdoctor_" + doctoruid)
                isbusy = cache.get("onlinestatusdoctor_" + doctoruid + "_busy")

                if status is not None:
                    isonline = True
                else:
                    isonline = False

                if isbusy == 1:
                    isbusy = True
                else:
                    isbusy = False

                # send fcm to doctor in case he is offline
                await self.send_fcm_to_doctor(doctoruid, text_data_json[ServerConstants.HEALTHCASEID], text_data_json[ServerConstants.USERNAME])

                websocketlogger.info('sending waiting queue nuber'+ str(queue_number))

                datatosend = { ServerConstants.MESSAGE_TYPE: "queue_number", ServerConstants.QUEUE_NUMBER : queue_number,
                               ServerConstants.ISONLINE: isonline, ServerConstants.ISBUSY: isbusy ,
                               ServerConstants.ETA : queue_number*config.DOCTORCONSULT_DEFAULT_WAITING_TIME}
                await self.send(text_data=json.dumps(datatosend))
                websocketlogger.info('send data : {}'.format(json.dumps(datatosend)))
            elif type == "doctorheartbeat":
                queue_size = text_data_json["queue_size"]
                last_token = text_data_json["last_token"]
                # check if anyone dropped before last token.
                # this is to notify and update doctors waiting queue of any drop offs
                patientqueuecount = 0
                for i in range(1,last_token+1):
                    if cache.get("doctor"+doctoruid+"q"+str(i)) is not None:
                        patientqueuecount = patientqueuecount + 1
                websocketlogger.info('last_token:{}, queue_size:{}, patientqueuecount:{}'.format(last_token, queue_size, patientqueuecount))
                if patientqueuecount != queue_size:
                    await self.send(text_data=json.dumps(
                        {ServerConstants.MESSAGE_TYPE: 'queue_message', 'command': 'update'}
                    ))
        except:
            websocketlogger.info("not found message_type in:", kwargs['text_data'])

    @database_sync_to_async
    def send_fcm_to_doctor(self, doctoruid, healthcaseid, username):
        try:
            doctormapping_req = DoctorFCMTokenMapping.objects.get(doctordetails__doctoruid = doctoruid)
            doctorfcmtoken = doctormapping_req.fcmtoken
            try:
                fcmresponse = send_to_token(doctorfcmtoken,
                                            {"command": "incomingcall", "healthcaseid": healthcaseid,
                                             "caller": username})
                websocketlogger.info("send fcm msg  response:"+fcmresponse)
            except (ObjectDoesNotExist, HTTPError, UnregisteredError) as e:
                websocketlogger.info(str(e))
                pass

        except (ObjectDoesNotExist, HTTPError, UnregisteredError) as e:
            websocketlogger.error("fcm error:" + str(e))

    async def disconnect(self, close_code):
        websocketlogger.info("disconnect:code:"+ str(close_code))
        doctoruid = self.scope['url_route']['kwargs']['doctoruid']
        websocketlogger.info("disconnecting from doctoruid:" + doctoruid)
        if doctoruid is not None:
            websocketlogger.info('removing from group:{}, channel:{}'.format("doctor"+doctoruid+"q", self.channel_name))
            await self.channel_layer.group_discard(
                "doctor"+doctoruid+"q",
                self.channel_name
            )

    async def group_message(self, message):
        print("got group msg:"+str(message))
        if message['command'] == "update_queue":
            await self.send(text_data=json.dumps(
                {ServerConstants.MESSAGE_TYPE: 'queue_message', 'command': 'update'}
            ))