import datetime
import json
import time
from datetime import timedelta

from django.conf import settings
from django.contrib.auth.models import User
from django.test import TestCase, override_settings
from django.utils import timezone

import Platform
import Platform.Constants
import backend_server.config
import backend_server.Constants
from Booking.models import (BookingDetails, BookingTableMover,
                            CompletedBooking, Services, SlotBooking, VCDetails)
from Booking.utils import getKioskAvailabilityString
from Mover.models import (DoctorAvailability, MoverDetails,
                          MoverLocationAndAvailability, AppDetails)
from Platform.models import MOVERAPPDATA, USERAPPDATA
from User.models import UserDetails

# Create your tests here.

class BookingAPITestCase(TestCase):
    multi_db = True
    def setUp(self):
        backend_server.config.ISTESTMODE = True
        MOVERAPPDATA.objects.create(moverappuuid = "123",moverid=1,gcmregid="testgcm")
        USERAPPDATA.objects.create(userappuuid = "1234",userid=1,gcmregid="testgcm")
        djangouser = User.objects.create(username="moveryolohealth",password="abc")
        djangouser1 = User.objects.create(username="moveryolohealth1",password="abc")
        # QBSESSIONDATA.objects.create(application_id=32324,token="asf",expiry=12456)
        movernew = MoverDetails.objects.create(djangouser=djangouser,moverid=1,name="arpit",phone="9769465241",skillstr="1,2",docs="testdoc1,testdoc2,http://yolohealth.in/static/uploadedfiles/moverdocs/file1.png")
        movernew1 = MoverDetails.objects.create(djangouser=djangouser1,moverid=11,name="arpit1",phone="978637273",skillstr="1")
        MoverLocationAndAvailability.objects.create(mover=movernew,isavailable=True,latitude=12.7,longitude=19.5)
        MoverLocationAndAvailability.objects.create(mover=movernew1,isavailable=True,latitude=12.7,longitude=19.5)
        usernew=UserDetails.objects.create(userid=1,name='arpit',phone="9167636253")
        bookingnew=BookingDetails.objects.create(bookingid=1,bookingfor=2,landmark="Gopal Sharma",flat="b1701",
                                                 locality="powai",user=usernew,datetime=timezone.now()+timedelta(minutes=60),bookingstatus=0)
        bookingpending=BookingDetails.objects.create(bookingid=3,bookingfor=2,landmark="Gopal Sharma",flat="b1701",
                                                 locality="powai",user=usernew,datetime=timezone.now()+timedelta(minutes=60),bookingstatus=0)
        bookingaccepted=BookingDetails.objects.create(bookingid=2,bookingfor=2,landmark="Gopal Sharma",flat="b1701",
                                                 locality="powai",user=usernew,datetime=timezone.now()+timedelta(minutes=60),bookingstatus=1,mover=movernew)

        BookingTableMover.objects.create(mover=movernew, booking=bookingnew,
                                         bookingstatusmover=backend_server.Constants.BookingConstants.PENDING)
        BookingTableMover.objects.create(mover=movernew, booking=bookingaccepted,
                                         bookingstatusmover=backend_server.Constants.BookingConstants.ACCEPTED)
        service = Services.objects.create(serviceid=1,servicetag="Consulatation",servicetitle="Doctor Visit",
                                servicecharges=1000,servicedescription="Doctor general visit")
        #completebooking
        bookingacomplete=BookingDetails.objects.create(bookingid=4,bookingfor=0,landmark="Gopal Sharma",flat="b1701",
                                                 locality="powai",user=usernew,datetime=timezone.now()+timedelta(minutes=60),bookingstatus=5,mover=movernew)
        compeltedbooking = CompletedBooking.objects.create(booking = bookingacomplete,service=service)

        DoctorAvailability.objects.create(availabilityid = 2,mover = movernew,slotbooked="000000",slotidsbooked="",
                                          date = "2016-03-26",starttime= "12:00",endtime="13:00",slotlength=10)

        DoctorAvailability.objects.create(availabilityid = 3,mover = movernew,slotbooked="000000",slotidsbooked="",
                                          date = datetime.date.today()+datetime.timedelta(days=1),starttime= "13:00",endtime="14:00",slotlength=10)

        DoctorAvailability.objects.create(availabilityid = 4,mover = movernew,slotbooked="0010",slotidsbooked="50",
                                          date = datetime.date.today()+datetime.timedelta(days=3),starttime= "11:00",
                                          endtime="12:00",slotlength=15)
        DoctorAvailability.objects.create(availabilityid = 5,mover = movernew,slotbooked="0010",slotidsbooked="60",
                                          date = datetime.date.today(),starttime= "11:00",
                                          endtime="12:00",slotlength=15)
        kiosk = AppDetails.objects.create(kioskid=101, kiosklocation="IOC", kiosktag="IOC-1", kiosktype=1, kioskdescription="IOC", glucosestrips=10, hbstrips=10, lipidstrips=10)
        SlotBooking.objects.create(slotbookingid=10,kiosk=kiosk,user=usernew,mover=movernew,date="2016-10-07",starttime="11:30",slotlength=15,slotno=3)
        SlotBooking.objects.create(slotbookingid=20,kiosk=kiosk,user=usernew,mover=movernew,
                                   date=datetime.date.today()+datetime.timedelta(days=1),starttime="13:15",slotlength=10,slotno=3)
        SlotBooking.objects.create(slotbookingid=30,kiosk=kiosk,user=usernew,mover=movernew,
                                   date=datetime.date.today()+datetime.timedelta(days=1),starttime="8:45",slotlength=15,slotno=3)
        SlotBooking.objects.create(slotbookingid=40,kiosk=kiosk,user=usernew,mover=movernew,
                                   date=datetime.date.today()+datetime.timedelta(days=-3),starttime="11:30",slotlength=15,slotno=3)
        SlotBooking.objects.create(slotbookingid=50,kiosk=kiosk,user=usernew,mover=movernew,
                                   date=datetime.date.today()+datetime.timedelta(days=3),starttime="11:30",slotlength=15,slotno=3)
        SlotBooking.objects.create(slotbookingid=60,kiosk=kiosk,user=usernew,mover=movernew,
                                   date=datetime.date.today(),starttime="11:30",slotlength=15,slotno=3)

        #cancelslotbooking:
        DoctorAvailability.objects.create(availabilityid = 7,mover = movernew,slotbooked="0000",slotidsbooked="",
                                          date = "2016-03-26",starttime= "13:00",
                                          endtime="14:00",slotlength=15)

    # def test_newBooking(self):
    #     bookingtime =timezone.localtime(timezone.now()+timedelta(minutes=60))
    #     input = json.dumps({backend_server.Constants.ServerConstants.USER_ID:1,
    #                         backend_server.Constants.ServerConstants.BOOKINGFOR:2,
    #                         backend_server.Constants.ServerConstants.LANDMARK:'Gopal sharma',
    #                         backend_server.Constants.ServerConstants.FLAT:"b1701",
    #                         backend_server.Constants.ServerConstants.LOCALITY:"powai",
    #                         backend_server.Constants.ServerConstants.DATETIME:time.mktime(bookingtime.timetuple())})
    #     response = self.client.post('/Booking/newBooking/',input,Platform.Constants.RESPONSE_JSON_TYPE)
    #     self.assertEqual(response.status_code,200)
    #     #

    def test_acceptBooking(self):
        input = json.dumps({backend_server.Constants.ServerConstants.MOVER_ID:1,
                            backend_server.Constants.ServerConstants.BOOKINGID:1,
                            backend_server.Constants.ServerConstants.BOOKINGSTATUSMOVER:1,
                            })
        response = self.client.post('/Booking/acceptBooking/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        #

    def test_cancelBooking(self):

        input = json.dumps({
                            backend_server.Constants.ServerConstants.BOOKINGID:2,
                            backend_server.Constants.ServerConstants.BOOKINGSTATUS:4,
                           })
        response = self.client.post('/Booking/cancelBooking/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        #

    def test_getAllBookingsOfStatus(self):
        input = dict({
                            backend_server.Constants.ServerConstants.BOOKINGSTATUS:1,
                           })
        response = self.client.get('/Booking/getAllBookingsOfStatus/',input)
        self.assertEqual(response.status_code,200)

    def test_getAllBookings(self):
        response = self.client.get('/Booking/getAllBookingsOfStatus/')
        self.assertEqual(response.status_code,200)
        
    def test_getBookingDetails(self):
        input = dict({backend_server.Constants.ServerConstants.BOOKINGID:1,
                      })
        response = self.client.get('/Booking/getBookingDetails/',input)
        self.assertEqual(response.status_code,200)

    def test_bookingServed(self):
        input = json.dumps({backend_server.Constants.ServerConstants.BOOKINGID:2,
                            backend_server.Constants.ServerConstants.SERVICEID:1,
                            })
        response = self.client.post('/Booking/bookingServed/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)

    def test_getAllUpcomingBookingStatusMover(self):
        response = self.client.get('/Booking/getAllUpcomingBookingStatusMover/')
        self.assertEqual(response.status_code,200)
        
    def test_updateBookingMover(self):
        input = json.dumps({backend_server.Constants.ServerConstants.BOOKINGID:2,
                            backend_server.Constants.ServerConstants.MOVERPHONE:978637273 ,
                            })
        response = self.client.post('/Booking/updateBookingMover/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        modifiedbooking = BookingDetails.objects.get(bookingid = 2)
        self.assertEqual(modifiedbooking.mover.phone,str(978637273))

    def test_updatePendingBookingMover(self):
        input = json.dumps({backend_server.Constants.ServerConstants.BOOKINGID:3,
                            backend_server.Constants.ServerConstants.MOVERPHONE:978637273 ,
                            })
        response = self.client.post('/Booking/updateBookingMover/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        modifiedbooking = BookingDetails.objects.get(bookingid = 3)
        self.assertEqual(modifiedbooking.mover.phone,str(978637273))

    def test_updateBookingDateTime(self):
        newdatetime = timezone.now()+timedelta(minutes=120)
        input = json.dumps({backend_server.Constants.ServerConstants.BOOKINGID:2,
                            backend_server.Constants.ServerConstants.DATETIME: time.mktime(newdatetime.timetuple()),
                            })
        response = self.client.post('/Booking/updateBookingDateTime/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        #newbooking = BookingDetails.objects.get(bookingid=2)
        #self.assertEqual(newbooking.datetime,newdatetime)

    def test_rateBooking(self):
        input = json.dumps({backend_server.Constants.ServerConstants.BOOKINGID:4,
                            backend_server.Constants.ServerConstants.RATING:4 ,
                            })
        response = self.client.post('/Booking/rateBooking/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        modifiedbooking = CompletedBooking.objects.get(booking__bookingid = 4)
        self.assertEqual(modifiedbooking.rating,4)

    def test_slotBookingMoverUserPhone(self):
        input = json.dumps({backend_server.Constants.ServerConstants.MOVERPHONE:9769465241,
                            backend_server.Constants.ServerConstants.USER_ID:1,
                            backend_server.Constants.ServerConstants.KIOSKID:1,
                            backend_server.Constants.ServerConstants.AVAILABILITYID:3,
                            backend_server.Constants.ServerConstants.SLOTNO:4,
                            backend_server.Constants.ServerConstants.DATE: "2016-03-26",
                            backend_server.Constants.ServerConstants.STARTTIME: "13:30",
                            backend_server.Constants.ServerConstants.SLOTLENGTH:10})
        response = self.client.post('/Booking/slotBooking/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        docav = DoctorAvailability.objects.get(availabilityid=3)
        self.assertEqual(docav.slotbooked,"000100")
        self.assertEqual(response.status_code,200)

    def test_slotBooking(self):
        input = json.dumps({backend_server.Constants.ServerConstants.MOVER_ID:1,
                            backend_server.Constants.ServerConstants.USER_ID:1,
                            backend_server.Constants.ServerConstants.KIOSKID:1,
                            backend_server.Constants.ServerConstants.AVAILABILITYID:2,
                            backend_server.Constants.ServerConstants.SLOTNO:4,
                            backend_server.Constants.ServerConstants.DATE: "2016-03-26",
                            backend_server.Constants.ServerConstants.STARTTIME: "12:30",
                            backend_server.Constants.ServerConstants.SLOTLENGTH:10})
        response = self.client.post('/Booking/slotBooking/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        docav = DoctorAvailability.objects.get(availabilityid=2)
        self.assertEqual(docav.slotbooked,"000100")
        self.assertEqual(response.status_code,200)
        input = json.dumps({backend_server.Constants.ServerConstants.MOVER_ID:1,
                            backend_server.Constants.ServerConstants.USER_ID:1,
                            backend_server.Constants.ServerConstants.KIOSKID:1,
                            backend_server.Constants.ServerConstants.AVAILABILITYID:2,
                            backend_server.Constants.ServerConstants.SLOTNO:3,
                            backend_server.Constants.ServerConstants.DATE: "2016-03-26",
                            backend_server.Constants.ServerConstants.STARTTIME: "12:20",
                            backend_server.Constants.ServerConstants.SLOTLENGTH:10})
        response = self.client.post('/Booking/slotBooking/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        docav = DoctorAvailability.objects.get(availabilityid=2)
        self.assertEqual(docav.slotbooked,"001100")
        self.assertEqual(response.status_code,200)
        input = json.dumps({backend_server.Constants.ServerConstants.MOVER_ID:1,
                            backend_server.Constants.ServerConstants.USER_ID:1,
                            backend_server.Constants.ServerConstants.KIOSKID:1,
                            backend_server.Constants.ServerConstants.AVAILABILITYID:2,
                            backend_server.Constants.ServerConstants.SLOTNO:1,
                            backend_server.Constants.ServerConstants.DATE: "2016-03-26",
                            backend_server.Constants.ServerConstants.STARTTIME: "12:00",
                            backend_server.Constants.ServerConstants.SLOTLENGTH:10})
        response = self.client.post('/Booking/slotBooking/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        docav = DoctorAvailability.objects.get(availabilityid=2)
        self.assertEqual(docav.slotbooked,"101100")
        self.assertEqual(response.status_code,200)
        
    def test_slotBooking_slotalreadybooked(self):
        input = json.dumps({backend_server.Constants.ServerConstants.MOVER_ID:1,
                            backend_server.Constants.ServerConstants.USER_ID:1,
                            backend_server.Constants.ServerConstants.KIOSKID:1,
                            backend_server.Constants.ServerConstants.AVAILABILITYID:2,
                            backend_server.Constants.ServerConstants.SLOTNO:5,
                            backend_server.Constants.ServerConstants.DATE: "2016-03-26",
                            backend_server.Constants.ServerConstants.STARTTIME: "10:00",
                            backend_server.Constants.ServerConstants.SLOTLENGTH:15})
        response = self.client.post('/Booking/slotBooking/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        input = json.dumps({backend_server.Constants.ServerConstants.MOVER_ID:1,
                            backend_server.Constants.ServerConstants.USER_ID:1,
                            backend_server.Constants.ServerConstants.KIOSKID:1,
                            backend_server.Constants.ServerConstants.AVAILABILITYID:2,
                            backend_server.Constants.ServerConstants.SLOTNO:5,
                            backend_server.Constants.ServerConstants.DATE: "2016-03-26",
                            backend_server.Constants.ServerConstants.STARTTIME: "10:00",
                            backend_server.Constants.ServerConstants.SLOTLENGTH:15})
        response = self.client.post('/Booking/slotBooking/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,400)

    def test_kioskavailabilitystring(self):
        date = "2016-10-07"
        kioskid=101
        starttime = datetime.time(11,0)
        endtime=datetime.time(12,0)
        slotlength=10
        avstr = getKioskAvailabilityString(date,kioskid,starttime,endtime,slotlength)
        self.assertEqual(avstr,"000110")

    def test_kioskavailabilitystringdoesntexist(self):
        date = "2016-10-10"
        kioskid=101
        starttime = datetime.time(11,0)
        endtime=datetime.time(12,0)
        slotlength=10
        avstr = getKioskAvailabilityString(date,kioskid,starttime,endtime,slotlength)

    def test_setVCDetails(self):
        input = json.dumps({backend_server.Constants.ServerConstants.USER_ID:1,
                            backend_server.Constants.ServerConstants.DOCTORMOVERID:1 ,
                            backend_server.Constants.ServerConstants.KIOSKID:101,
                            })
        response = self.client.post('/Booking/setVCDetails/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        
        data = json.loads(response.content)
        vcdetails = VCDetails.objects.get(vcid=data["body"][backend_server.Constants.ServerConstants.VCID])
        self.assertEqual(vcdetails.user.userid,1)

    def test_setVCDetailsAdmin(self):
        input = json.dumps({backend_server.Constants.ServerConstants.USER_ID:1,
                            backend_server.Constants.ServerConstants.HOSPITALADMINMOVERID:1 ,
                            backend_server.Constants.ServerConstants.KIOSKID:101,
                            })
        response = self.client.post('/Booking/setVCDetails/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        
        data = json.loads(response.content)
        vcdetails = VCDetails.objects.get(vcid=data["body"][backend_server.Constants.ServerConstants.VCID])
        self.assertEqual(vcdetails.user.userid,1)

    def test_updateVCDetails(self):
        input = json.dumps({backend_server.Constants.ServerConstants.USER_ID:1,
                            backend_server.Constants.ServerConstants.HOSPITALADMINMOVERID:1 ,
                            backend_server.Constants.ServerConstants.KIOSKID:101,
                            })
        response = self.client.post('/Booking/setVCDetails/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        
        data = json.loads(response.content)
        vcid=data["body"][backend_server.Constants.ServerConstants.VCID]
        #update
        enddatetime = datetime.datetime.now() + timedelta(minutes=15)
        input = json.dumps({backend_server.Constants.ServerConstants.VCID:vcid,
                            backend_server.Constants.ServerConstants.DOCTORMOVERID:11,
                            backend_server.Constants.ServerConstants.ENDTIME:enddatetime.time().strftime('%H:%M:%S') ,
                            })
        response = self.client.post('/Booking/setVCDetails/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)

    def test_updateVCDetails2(self):
        input = json.dumps({backend_server.Constants.ServerConstants.USER_ID:1,
                            backend_server.Constants.ServerConstants.HOSPITALADMINMOVERID:1 ,
                            backend_server.Constants.ServerConstants.KIOSKID:101,
                            })
        response = self.client.post('/Booking/setVCDetails/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        
        data = json.loads(response.content)
        vcid=data["body"][backend_server.Constants.ServerConstants.VCID]
        #update
        enddatetime = datetime.datetime.now() + timedelta(minutes=15)
        input = json.dumps({backend_server.Constants.ServerConstants.VCID:vcid,
                            backend_server.Constants.ServerConstants.ENDTIME:enddatetime.time().strftime('%H:%M:%S') ,
                            })
        response = self.client.post('/Booking/setVCDetails/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        
    def test_getALLVCsBookedForDate_withEndDate(self):
        input = dict({
                      backend_server.Constants.ServerConstants.ENDDATE: datetime.date.today() + datetime.timedelta(days=5)
                      })
        response = self.client.get('/Booking/getAllVCBookingsForDates/',input)
        self.assertEqual(response.status_code,200)

    def test_getALLVCsBookedForDate_withStartDate(self):
        input = dict({
                      backend_server.Constants.ServerConstants.STARTDATE: datetime.date.today() + datetime.timedelta(days=-5)
                      })
        response = self.client.get('/Booking/getAllVCBookingsForDates/',input)
        self.assertEqual(response.status_code,200)
        
    def test_getALLVCsBookedForDate(self):
        response = self.client.get('/Booking/getAllVCBookingsForDates/')
        self.assertEqual(response.status_code,200)
        
    def test_cancelSlotbooking(self):
        input = json.dumps({backend_server.Constants.ServerConstants.MOVER_ID:1,
                            backend_server.Constants.ServerConstants.USER_ID:1,
                            backend_server.Constants.ServerConstants.KIOSKID:1,
                            backend_server.Constants.ServerConstants.AVAILABILITYID:7,
                            backend_server.Constants.ServerConstants.SLOTNO:3,
                            backend_server.Constants.ServerConstants.DATE: "2016-03-26",
                            backend_server.Constants.ServerConstants.STARTTIME: "13:30",
                            backend_server.Constants.ServerConstants.SLOTLENGTH:15})
        response = self.client.post('/Booking/slotBooking/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        data = json.loads(response.content)
        docav = DoctorAvailability.objects.get(availabilityid=7)
        self.assertEqual(docav.slotidsbooked, str(data["body"][backend_server.Constants.ServerConstants.SLOTBOOKINGID]))
        input = json.dumps({
                            backend_server.Constants.ServerConstants.SLOTNO:3,
                            backend_server.Constants.ServerConstants.SLOTBOOKINGID:data["body"][backend_server.Constants.ServerConstants.SLOTBOOKINGID],
                           })
        response = self.client.post('/Booking/cancelSlotBooking/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        docav = DoctorAvailability.objects.get(availabilityid=7)
        
    def test_getVCs(self):
        input = dict({
                     # backend_server.Constants.ServerConstants.STARTDATE:datetime.date.today()+datetime.timedelta(days=-5)
                      })
        response = self.client.get('/Booking/getVCs/',input)
        self.assertEqual(response.status_code,200)

    def test_getVCAppointmentDetails(self):
        input = dict({
            backend_server.Constants.ServerConstants.DATE:20,
            backend_server.Constants.ServerConstants.MONTH:6,
            backend_server.Constants.ServerConstants.YEAR:2018,
            backend_server.Constants.ServerConstants.KIOSKSTR: '100,101',
        })
        response = self.client.get('/Booking/getVCAppointmentDetails/',input)
        self.assertEqual(response.status_code,200)
