__author__ = 'root'
from Booking.models import SlotBooking
from django.core.exceptions import ObjectDoesNotExist
from datetime import datetime,timedelta
from Mover.models import AppDetails
from django.http import HttpResponse
def getKioskAvailabilityString(date,kioskid_req,starttime_req,endtime_req,slotlength):
    #get todays slots
    starttime = datetime.strptime(str(starttime_req),"%H:%M:%S")
    endtime = datetime.strptime(str(endtime_req),"%H:%M:%S")
    try:
        kiosk1 = AppDetails.objects.get(kioskid=kioskid_req)
    except ObjectDoesNotExist:
        return HttpResponse("kiosk not found",status = 404)

    todaysbookedslotsforthiskiosk = SlotBooking.objects.filter(date=date,kiosk=kiosk1,
                                                               starttime__range=(starttime,endtime))

    #make all slot starttime array
    totalslots = endtime-starttime
    totalslots = int(totalslots.seconds/(60*slotlength))

    if len(todaysbookedslotsforthiskiosk)==0:
        return "0"*totalslots

    docslotstarttimearray = list()
    for i in range(0,totalslots):
        docslotstarttimearray.append(starttime+timedelta(minutes=i*slotlength))

    kioskavstr=""
    for docslotstarttime in docslotstarttimearray:
        thisslottaken = False
        docslotendtime = docslotstarttime + timedelta(minutes=slotlength)
        for slotbooked in todaysbookedslotsforthiskiosk:
            if thisslottaken:
                continue
            slotbookedstarttime = datetime.strptime(str(slotbooked.starttime),"%H:%M:%S")
            slotbookedendtime = slotbookedstarttime + timedelta(minutes=slotbooked.slotlength)
            if(docslotstarttime > slotbookedstarttime and docslotstarttime < slotbookedendtime ):
                kioskavstr = kioskavstr + "1"
                thisslottaken = True
                continue
            if(docslotendtime > slotbookedstarttime and docslotendtime < slotbookedendtime ):
                kioskavstr = kioskavstr + "1"
                thisslottaken = True
                continue

        if not thisslottaken:
            kioskavstr = kioskavstr + "0"

    return kioskavstr




