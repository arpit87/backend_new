import backend_new.Constants
from django.db import models
from Doctor.models import DoctorDetails, AppDetails
from Person.models import PersonDetails
from datetime import timedelta , datetime
from django.utils import timezone
from backend_new.Constants import AppointmentTypeConstant , AppointmentStatusConstant
from import_export.admin import ImportExportModelAdmin,ImportExportMixin

# Create your models here.
class DoctorAvailability(models.Model):
    recurringid = models.IntegerField(null=True)
    availabilityid = models.AutoField(primary_key=True,null=False)
    doctor = models.ForeignKey(DoctorDetails, on_delete=models.CASCADE)
    date = models.DateField() #yyyy-mm-dd
    starttime = models.TimeField() #hh:mm
    endtime = models.TimeField()
    slotlength = models.IntegerField(default=10)

    def __str__(self):
      return str(self.doctor.name)

class SlotBooking(models.Model):
    APPOINTMENT_TYPE = ((AppointmentTypeConstant.VC, 'Video Conference'), (AppointmentTypeConstant.INCLINIC, 'In-Clinic'))
    CONSULT_STATUS = ((AppointmentStatusConstant.PENDING, 'Pending'), (AppointmentStatusConstant.STARTED, 'Started')
                      , (AppointmentStatusConstant.DROPPED, 'Dropped'), (AppointmentStatusConstant.COMPLETED, 'Completed'),)

    slotbookingid = models.AutoField(primary_key=True,null=False)
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    starttime = models.TimeField()
    consultstatus = models.IntegerField(choices=CONSULT_STATUS, default=AppointmentStatusConstant.PENDING)
    type = models.IntegerField(choices=APPOINTMENT_TYPE, default=AppointmentTypeConstant.VC)
    appdetails = models.ForeignKey(AppDetails, null=True, on_delete=models.CASCADE)
    doctoravailability = models.ForeignKey(DoctorAvailability, on_delete=models.CASCADE , related_name="bookedslots")

    def __str__(self):
       return str(self.slotbookingid)

    class Meta:
        unique_together = ('starttime', 'doctoravailability',)







