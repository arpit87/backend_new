from django.contrib import admin
from Appointments.models import SlotBooking
from Appointments.models import DoctorAvailability, SlotBooking
from import_export import resources
from import_export.admin import ImportExportModelAdmin,ImportExportMixin

class SlotBookingModelAdmin(admin.ModelAdmin):
    list_display = ('slotbookingid','person','starttime','consultstatus','appdetails','doctoravailability')

admin.site.register(SlotBooking,SlotBookingModelAdmin)

class DoctorAvailabilityResource(resources.ModelResource):
    class Meta:
        model = DoctorAvailability

class DoctorAvailabilityModelAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ('availabilityid', 'doctor', 'date', 'starttime', 'endtime', 'slotlength')
    resource_class = DoctorAvailabilityResource

admin.site.register(DoctorAvailability, DoctorAvailabilityModelAdmin)
