import json
import datetime
import calendar
import time
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.http import HttpResponse
from django.shortcuts import render
from django.db.models import Q
# Create your views here.
from django.views.decorators.csrf import csrf_exempt
from Platform import utils
from Platform.views import authenticateURL
from Appointments.models import SlotBooking , DoctorAvailability
from backend_new import config
from backend_new.Constants import ServerConstants, PreferredLanguageConstant
from Person.models import PersonDetails
from Doctor.models import DoctorDetails,AppDetails
import logging
from operator import itemgetter
import time
import Communications.utils
from django.utils import timezone
from Platform import backendlogger
from django.forms.models import model_to_dict
#from backend_new.Booking.tasks import add
from Communications.smstemplates import getappointmenttemplate
logger = logging.getLogger(__name__)
requestlogger = logging.getLogger('apirequests')


@csrf_exempt
def slotBooking(request):
    # requestlogger.info('===================slotBooking==================')
    data = json.loads(request.body)
    logmessage = []
    authenticated,djangopersonstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('slotBooking', data, 'Error authenticating person', logmessage)
        return HttpResponse("Error authenticating person", status=401)

    logmessage.append('slotBooking;%s'%(str(data)))
    # requestlogger.info('slotBooking;%s'%(str(data)))

    try:
        personuid_req = data[ServerConstants.PERSONUID]
        try:
            person = PersonDetails.objects.get(personuid=personuid_req)
        except ObjectDoesNotExist:
            backendlogger.error('slotBooking', data, 'personuid not found', logmessage)
            return HttpResponse("personuid not found.Are you registered?",status=404)
    except KeyError:
            backendlogger.error('slotBooking', data, 'Provide personuid / person phone number', logmessage)
            return HttpResponse("Provide personuid",status = 403)

    try:
        appuid_req = data[ServerConstants.APPUID]
        appdetails_req = AppDetails.objects.get(appuid=appuid_req)
    except:
        appdetails_req = None
        # if appdetails id not sent then its physical clinic visit booking
        pass

    starttime_req = data[ServerConstants.STARTTIME]

    try:
        availableID_req = data[ServerConstants.AVAILABILITYID]
        doctoravailability = DoctorAvailability.objects.get(availabilityid=availableID_req)
    except:
        # if availability ID
        res = setDoctorAvailability(request)
        requestlogger.info(res.content)
        if res.status_code == 200 :
            jsondata = json.loads(res.content)
            try:
                doctoravailability = DoctorAvailability.objects.get(availabilityid=jsondata[ServerConstants.AVAILABILITYID])
            except KeyError:
                backendlogger.error('slotBooking', data, 'Slot already Booked', logmessage)
                return HttpResponse('Slot already Booked', status=403)
        else:
            backendlogger.error('slotBooking', data, res.content, logmessage)
            return res

    # create new slot
    slotbooking = SlotBooking.objects.create(person=person,appdetails=appdetails_req,
                                             starttime=starttime_req,
                                             doctoravailability=doctoravailability)

    doctorphone_req =  doctoravailability.doctor.phone
    personphone_req = person.phone
    formattedtime = datetime.time( int(starttime_req.split(":")[0]), int(starttime_req.split(":")[1]),0).strftime("%I:%M %p")
    #send slot booking message to Doctor
    # Communications.utils.sendSMS(doctor_message,doctorphone_req, None)

    #send slot booking message to Person/patient
    Communications.utils.sendSMS(getappointmenttemplate(person.preferredlanguage,doctoravailability.doctor.name,doctoravailability.date.strftime("%d%b"),formattedtime) ,
                                 personphone_req, None, person.preferredlanguage != PreferredLanguageConstant.ENGLISH)

    jsonObj = dict({ServerConstants.SLOTBOOKINGID:slotbooking.slotbookingid})

    httpoutput = utils.successJson(jsonObj)

    backendlogger.info('slotBooking', data, httpoutput, logmessage)
    # requestlogger.info('slotBookingResponse;success')
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)

@csrf_exempt
def setSlotBookingStatus(request):
    # requestlogger.info('===================cancelSlotBooking==================')

    data = json.loads(request.body)
    logmessage = []

    authenticated,djangopersonstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('setBookingStatus', data, 'Error authenticating person', logmessage)
        return HttpResponse("Error authenticating person", status=401)

    logmessage.append('cancelSlotBooking;%s'%(str(data)))
    # requestlogger.info('cancelSlotBooking;%s'%(str(data)))

    slotbookingid_req = data[ServerConstants.SLOTBOOKINGID]
    try:
        slotdetails = SlotBooking.objects.get(slotbookingid=slotbookingid_req)
    except ObjectDoesNotExist:
        backendlogger.error('setBookingStatus', data, 'SlotID not found.Are you booked Slot?', logmessage)
        return HttpResponse("SlotID not found.Are you booked Slot?",status=404)

    try:
        status = data[ServerConstants.STATUS]
    except:
        backendlogger.error('setBookingStatus', data, 'Status not sent', logmessage)
        return HttpResponse("Status not sent", status=403)

    slotdetails.consultstatus = status
    slotdetails.save()

    httpoutput = utils.successJson(dict())
    backendlogger.info('setBookingStatus', data, httpoutput, logmessage)
    # requestlogger.info('slotBookingResponse;success')
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def cancelSlotBooking(request):
    # requestlogger.info('===================cancelSlotBooking==================')

    data = json.loads(request.body)
    logmessage = []

    authenticated,djangopersonstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('cancelSlotBooking', data, 'Error authenticating person', logmessage)
        return HttpResponse("Error authenticating person", status=401)

    logmessage.append('cancelSlotBooking;%s'%(str(data)))
    # requestlogger.info('cancelSlotBooking;%s'%(str(data)))

    slotbookingid_req = data[ServerConstants.SLOTBOOKINGID]
    slotnum_req = data[ServerConstants.SLOTNO]
    try:
        slotdetails = SlotBooking.objects.get(slotbookingid=slotbookingid_req)
    except ObjectDoesNotExist:
        backendlogger.error('cancelSlotBooking', data, 'SlotID not found.Are you booked Slot?', logmessage)
        return HttpResponse("SlotID not found.Are you booked Slot?",status=404)


    doctoravail = DoctorAvailability.objects.get(slotidsbooked__contains=slotbookingid_req)

    slotbookingid_array = doctoravail.slotidsbooked.split(',')
    slotbookingid_array.remove(str(slotbookingid_req)) #else its unicode
    newslotidbooked = ','.join(slotbookingid_array)

    slotbooked_req = doctoravail.slotbooked
    slot = len(slotbooked_req) - int(slotnum_req)

    if int(slotbooked_req,2) & 0<<slot:
        return HttpResponse("Slot not booked",status=400)
    newslotBooked_String = bin(int(slotbooked_req,2) ^ 1<<slot)[2:].zfill(len(slotbooked_req)) # e.g '000010000000' = str(bin(128)[2:]).zfill(12)) (binary digits)

    doctoravail.slotidsbooked = newslotidbooked
    doctoravail.slotbooked = newslotBooked_String
    doctoravail.save()

    slotdetails.delete()

    httpoutput = utils.successJson(dict())
    # requestlogger.info('cancelSlotBookingResponse;success')

    backendlogger.info('cancelSlotBooking', data, httpoutput, logmessage)
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)


def getSlotBookingDetails(request):
    # filtering with doctoruid only
    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value
    authenticated,djangopersonstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getSlotBookingDetails', logrequest, 'Error authenticating person', logmessage)
        return HttpResponse("Error authenticating person", status=401)


    try:
        doctoruid_req = request.GET[ServerConstants.DOCTORUID]
    except ObjectDoesNotExist:
        backendlogger.error('getSlotBookingDetails', logrequest, 'please provide doctor id', logmessage)
        return HttpResponse("please provide doctor id", status=401)

    try:
        doctor = DoctorDetails.objects.get(doctoruid=doctoruid_req)
    except ObjectDoesNotExist:
        backendlogger.error('getSlotBookingDetails', logrequest, 'doctor not found', logmessage)
        return HttpResponse("doctor not found", status=401)

    try:
        starttimestamp_req = request.GET[ServerConstants.STARTTIME_TIMESTAMP]
    except ObjectDoesNotExist:
        backendlogger.error('getSlotBookingDetails', logrequest, 'please provide starttime', logmessage)
        return HttpResponse("please provide starttime", status=401)

    try:
        endtimestamp_req = request.GET[ServerConstants.ENDTIME_TIMESTAMP]
    except ObjectDoesNotExist:
        backendlogger.error('getSlotBookingDetails', logrequest, 'please provide endttime', logmessage)
        return HttpResponse("please provide endttime", status=401)

    startdate_obj = datetime.datetime.fromtimestamp(float(starttimestamp_req))
    enddate_obj = datetime.datetime.fromtimestamp(float(endtimestamp_req))
    if(startdate_obj.date() != enddate_obj.date()):
        backendlogger.error('getSlotBookingDetails', logrequest, 'please provide starttime and endtime of same date', logmessage)
        return HttpResponse("please provide starttime and endtime of same date", status=401)
    date_req = startdate_obj.date()
    starttime_req = startdate_obj.time()
    endtime_req = enddate_obj.time()
    slotobj = SlotBooking.objects.filter(doctor=doctor, date=date_req,starttime__range=(starttime_req,endtime_req))
    slotDetailList = list()
    for slot in slotobj:
        starttim = datetime.datetime.strptime(str(slot.starttime), "%H:%M:%S")
        starttim = datetime.datetime.combine(slot.date,starttim.time())
        endtim = starttim + datetime.timedelta(minutes=slot.slotlength)
        slotDetailList.append({
            ServerConstants.NAME:slot.person.name,
            ServerConstants.PHONE:slot.person.phone,
            ServerConstants.STARTTIME:slot.starttime,
            ServerConstants.ENDTIME:endtim.time(),
        })
    jsondata = dict({ServerConstants.SLOTDETAILS:slotDetailList})
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getSlotBookingDetails', logrequest, httpoutput, logmessage)
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def setDoctorAvailability(request):
    # requestlogger.info('===================setDoctorAvailability==================')
    data = json.loads(request.body)
    logmessage = []
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('setDoctorAvailability', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    logmessage.append('setDoctorAvailability;%s'%(str(data)))
    # requestlogger.info('setDoctorAvailability;%s'%(str(data)))

    #get doctor by phone or id
    try:
        doctoruid_req = data[ServerConstants.DOCTORUID]
        try:
           doctor = DoctorDetails.objects.get(doctoruid=doctoruid_req)
        except ObjectDoesNotExist:
            backendlogger.error('setDoctorAvailability', data, 'doctoruid not found. Are you registered?', logmessage)
            return HttpResponse("doctoruid not found. Are you registered?",status=404)
    except KeyError:
        try:
            doctorphone_req = data[ServerConstants.DOCTORPHONE]
            try:
                doctor = DoctorDetails.objects.get(phone=doctorphone_req)
            except ObjectDoesNotExist:
                backendlogger.error('setDoctorAvailability', data, 'Doctor phone not found. Are you registered?', logmessage)
                return HttpResponse("Doctor phone not found. Are you registered?",status=404)
        except KeyError:
            backendlogger.error('setDoctorAvailability', data, 'Provide id / doctor phone number', logmessage)
            return HttpResponse("Provide id / doctor phone number",status = 403)

    date_req = data[ServerConstants.DATE]
    starttime_req = data[ServerConstants.STARTTIME]
    endtime_req = data[ServerConstants.ENDTIME]
    # check if the doctor availability is already booked between starttime and endtime

    slotlength_req = data[ServerConstants.SLOTLENGTH]

    try:
        month_req = data[ServerConstants.MONTH]
    except:
        month_req = None

    try:
        weeks_req = data[ServerConstants.WEEKS]
    except:
        weeks_req = None

    try:
        days_req = data[ServerConstants.DAYS]
    except:
        days_req = None


    fmt = '%H:%M'
    d1 = datetime.datetime.strptime(starttime_req, fmt)
    d2 = datetime.datetime.strptime(endtime_req, fmt)

    # diff = d2 -d1
    # diff_minutes = (diff.days * 24 * 60) + (diff.seconds/60)
    # intervals = int(diff_minutes/int(slotlength_req))

    checkAvailabilityobj = DoctorAvailability.objects.filter(
        (Q(starttime__gte=d1) & Q(starttime__lt=d2)) | (Q(endtime__gt=d1) & Q(endtime__lte=d2)),
        doctor=doctor, date=date_req,
    )
    if(checkAvailabilityobj):
        jsonObj = dict({
            ServerConstants.MESSAGE: "Availability already booked.",
        })
        httpoutput = utils.successJson(jsonObj)
        backendlogger.info('setDoctorAvailability', data, httpoutput, logmessage)
        return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)

    if(month_req and weeks_req and days_req):
        recurringid = int(time.mktime(datetime.now().timetuple()))
        daysarray = days_req.split(',')
        date_req = datetime.datetime.strptime(date_req, "%Y-%m-%d")
        enddat = date_req + datetime.timedelta(weeks=int(weeks_req),days=int(month_req)*30)
        # custom_list = list()
        while enddat >= date_req:
            if str(date_req.strftime("%w")) in daysarray:
                doctoravailability = DoctorAvailability.objects.create(doctor=doctor,date=date_req,starttime=starttime_req,
                                                           endtime=endtime_req,slotlength=slotlength_req, recurringid=recurringid)
            date_req = date_req + datetime.timedelta(days=1)

        jsonObj = dict({
            ServerConstants.RECURRINGID:recurringid,
            # ServerConstants.CUSTOMLIST:custom_list
        })
        httpoutput = utils.successJson(jsonObj)
        backendlogger.info('setDoctorAvailability', data, httpoutput, logmessage)
        return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)

    doctoravailability = DoctorAvailability.objects.create(doctor=doctor,date=date_req,starttime=starttime_req,
                                                           endtime=endtime_req,slotlength=slotlength_req)
    doctoravailability.recurringid = doctoravailability.availabilityid
    doctoravailability.save()
    jsonObj = dict({
        ServerConstants.AVAILABILITYID:doctoravailability.availabilityid,
        ServerConstants.RECURRINGID:doctoravailability.recurringid
    })

    httpoutput = utils.successJson(jsonObj)
    backendlogger.info('setDoctorAvailability', data, httpoutput, logmessage)
    # requestlogger.info('setDoctorAvailabilityResponse;success')
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)

def getUpcomingConsultationsByDateOrMonth(request):
    # requestlogger.info('===================getUpcomingConsultationsByMonth==================')
    data = json.loads(request.body)
    logmessage = []
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getUpcomingConsultationsByDate', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)
    try:
        doctoruid_req = data[ServerConstants.DOCTORUID]
    except KeyError:
        backendlogger.error('getUpcomingConsultationsByDate', data, 'Provide Doctor ID', logmessage)
        return HttpResponse("Please provide doctoruid", status=400)

    try:
        date_req = int(data[ServerConstants.DATE])
    except KeyError:
        date_req = None
        pass

    try:
        month_req = int(data[ServerConstants.MONTH])
    except KeyError:
        backendlogger.error('getUpcomingConsultationsByDate', data, 'Provide Month', logmessage)
        return HttpResponse("Please provide month", status=400)

    try:
        year_req = int(data[ServerConstants.YEAR])
    except KeyError:
        backendlogger.error('getUpcomingConsultationsByDate', data, 'Provide year', logmessage)
        return HttpResponse("Please provide year", status=400)

    weekday = ["Monday","Tuesday","Wednesday",
        "Thursday","Friday","Saturday","Sunday"]

    datelist = list()
    if date_req:
        days = [datetime.date(year_req, month_req,date_req)]
    else:
        #availablity of kiosk by doctor slot booked in
        num_days = calendar.monthrange(year_req, month_req)[1]
        days = [datetime.date(year_req, month_req,day) for day in range(1,num_days)]
    for day in days:
        doctoravailabilities = DoctorAvailability.objects.filter(date=day,doctor__doctoruid=doctoruid_req).order_by('starttime')
        doctoravailabilitylist = list()
        for thisavailability in doctoravailabilities:
            bookedslots = thisavailability.bookedslots.all()
            bookedslotlist = list()
            for thisslot in bookedslots:
                appuid = None
                applocation = ""
                if thisslot.appdetails is not None :
                    appuid = thisslot.appdetails.appuid
                    applocation = thisslot.appdetails.applocation
                bookedslotlist.append( {
                    ServerConstants.USERNAME:thisslot.person.name,
                    ServerConstants.PHONE:thisslot.person.phone,
                    ServerConstants.PERSONUID:thisslot.person.personuid,
                    ServerConstants.SLOTBOOKINGID:thisslot.slotbookingid,
                    ServerConstants.STARTTIME:thisslot.starttime,
                    ServerConstants.CONSULTSTATUS:thisslot.get_consultstatus_display(),
                    ServerConstants.APPUID: appuid,
                    ServerConstants.APPLOCATION: applocation,
                })
            starttime = datetime.datetime.strptime(str(thisavailability.starttime), "%H:%M:%S")
            starttime = datetime.datetime.combine(thisavailability.date,starttime.time())
            endtime = datetime.datetime.strptime(str(thisavailability.endtime), "%H:%M:%S")
            endtime = datetime.datetime.combine(thisavailability.date,endtime.time())
            starttim = time.mktime(starttime.timetuple())
            endtim = time.mktime(endtime.timetuple())
            doctoravailabilitylist.append({
                ServerConstants.AVAILABILITYID:thisavailability.availabilityid,
                ServerConstants.RECURRINGID:thisavailability.recurringid,
                ServerConstants.DOCTORNAME:thisavailability.doctor.name,
                ServerConstants.DAY:weekday[thisavailability.date.weekday()],
                ServerConstants.STARTTIME:thisavailability.starttime,
                ServerConstants.ENDTIME:thisavailability.endtime,
                ServerConstants.STARTTIME_TIMESTAMP:starttim,
                ServerConstants.ENDTIME_TIMESTAMP:endtim,
                ServerConstants.DATE:thisavailability.date,
                ServerConstants.BOOKEDSLOTLIST:bookedslotlist,
                ServerConstants.SLOTLENGTH: thisavailability.slotlength,

            })

        datelist.append({ServerConstants.DATE:str(day.day), ServerConstants.AVAILABILITYLIST :doctoravailabilitylist})
    jsondata = dict({ServerConstants.UPCOMINGCONSULTLIST:datelist})
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getUpcomingConsultationsByMonth', data, httpoutput, logmessage)
    # requestlogger.info('getUpcomingConsultationsByMonth;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)



def deleteDoctorAvailability(request):
    data = json.loads(request.body)
    logmessage = []

    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('deleteDoctorAvailability', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        availabilityid_req = data[ServerConstants.AVAILABILITYID]
    except KeyError:
        availabilityid_req = None

    try:
        recurringid_req = data[ServerConstants.RECURRINGID]
    except KeyError:
        recurringid_req = None

    try:
        daysarray = data[ServerConstants.DAYS]
    except KeyError:
        daysarray = None

    if(availabilityid_req or recurringid_req):
        try:
            if(recurringid_req):
                if(daysarray):
                    doctoravailability = DoctorAvailability.objects.filter(
                        date__week_day=daysarray,
                        recurringid=recurringid_req
                    )
                    doctoravailability.delete()
                else:
                    doctoravailability = DoctorAvailability.objects.filter(
                        recurringid=recurringid_req
                    )
                    doctoravailability.delete()
            else:
                doctoravailability = DoctorAvailability.objects.get(availabilityid=availabilityid_req)
                doctoravailability.delete()
        except:
            backendlogger.error('deleteDoctorAvailability', data, 'given id not found', logmessage)
            return HttpResponse("given id not found", status=401)
    else:
        backendlogger.error('deleteDoctorAvailability', data, 'please provide either availability or recurringid', logmessage)
        return HttpResponse("please provide either availability or recurringid", status=401)
    # delete the particular from the model
    httpoutput = utils.successJson(dict())
    backendlogger.info('deleteDoctorAvailability', data, httpoutput, logmessage)
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)

# @csrf_exempt
# def getDoctorAvailabilityForDateAndApp(request):
#     # requestlogger.info('===================getDoctorAvailabilityForDateAndApp==================')
#     # authenticated,djangouserstr = authenticateURL(request)
#     logrequest = {}
#     logmessage = []
#     for key, value in request.GET.items():
#         logrequest[key] = value
#     # if not authenticated:
#     #     backendlogger.error('getDoctorAvailabilityForDateAndApp', logrequest, 'Error authenticatign user', logmessage)
#     #     return HttpResponse("Error authenticating user", status=401)
#
#     date_req = request.GET[ServerConstants.DATE]
#     try:
#         appuid_req = request.GET[ServerConstants.appuid]
#     except KeyError:
#         appuid_req = None
#
#     availabledoctors = DoctorAvailability.objects.filter(date=date_req)
#
#     #modify availability accrding to kiosk availability
#     doctorlist = list()
#     for thisdoctor in availabledoctors:
#         if(appuid_req!=None):
#             kioskav = getAppAvailabilityString(date_req,appuid_req,thisdoctor.starttime,thisdoctor.endtime,thisdoctor.slotlength)
#             logmessage.append('kioskav;%s'%(kioskav))
#             #requestlogger.info('thisdoctor.slotbooked;%s'%(thisdoctor.slotbooked))
#             #taking binary or with kiosk availability
#             thisdoctor.slotbooked = bin(int(thisdoctor.slotbooked,2)^int(kioskav,2))[2:].zfill(len(kioskav))
#             logmessage.append('slotbooked;%s'%(thisdoctor.slotbooked))
#             # requestlogger.info('slotbooked;%s'%(thisdoctor.slotbooked))
#         doctorlist.append({
#             ServerConstants.DOCTORNAME:thisdoctor.doctor.name,
#             ServerConstants.DOCTOR_ID:thisdoctor.doctor.doctoruid,
#             ServerConstants.AVAILABILITYID:thisdoctor.availabilityid,
#             ServerConstants.SKILLSTR:thisdoctor.doctor.skillstr,
#             ServerConstants.PROFILEPIC:thisdoctor.doctor.profilepic,
#             ServerConstants.QBID:thisdoctor.doctor.qbid,
#             ServerConstants.REGISTRATIONNUM:thisdoctor.doctor.registrationnum,
#             ServerConstants.SPECIALIZATION:thisdoctor.doctor.specialization,
#             ServerConstants.QUALIFICATION:thisdoctor.doctor.qualification,
#             ServerConstants.EXPERIENCE:thisdoctor.doctor.experience,
#             ServerConstants.STARTTIME:str(thisdoctor.starttime),
#             ServerConstants.ENDTIME:str(thisdoctor.endtime),
#             ServerConstants.SLOTLENGTH:thisdoctor.slotlength,
#             ServerConstants.SLOTBOOKED:thisdoctor.slotbooked
#         })
#
#     jsondata = dict({ServerConstants.DOCTORLIST:doctorlist,
#                      ServerConstants.DATE:date_req,
#                      ServerConstants.appuid:appuid_req})
#     httpoutput = utils.successJson(jsondata)
#     backendlogger.info('getDoctorAvailabilityForDateAndApp', logrequest, httpoutput, logmessage)
#     # requestlogger.info('getDoctorAvailabilityForDateAndApp;%s'%(str(httpoutput)))
#     return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)
#
# @csrf_exempt
# def getCurrentAppBookingforDate(request):
#     # requestlogger.info('===================getCurrentAppBookingForDate==================')
#     logrequest = {}
#     logmessage = []
#     for key, value in request.GET.items():
#         logrequest[key] = value
#     authenticated,djangouserstr = authenticateURL(request)
#     if not authenticated:
#         backendlogger.error('getCurrentAppBookingforDate', logrequest, 'Error authenticating user', logmessage)
#         return HttpResponse("Error authenticating user", status=401)
#
#     try:
#         date_req = request.GET[ServerConstants.DATE]
#     except KeyError:
#         backendlogger.error('getCurrentAppBookingforDate', logrequest, 'Provide Date', logmessage)
#         return HttpResponse("Provide Date",status = 400)
#
#     availabledoctors = DoctorAvailability.objects.filter(date=date_req)
#
#     #availablity of kiosk by doctor slot booked, for one kiosk
#     doctorlist = list()
#     for thisdoctor in availabledoctors:
#         doctorlist.append({
#             ServerConstants.DOCTORNAME:thisdoctor.doctor.name,
#             ServerConstants.STARTTIME:thisdoctor.starttime,
#             ServerConstants.ENDTIME:thisdoctor.endtime,
#         })
#
#     jsondata = dict({ServerConstants.DOCTORLIST:doctorlist,
#                      ServerConstants.DATE:date_req})
#     httpoutput = utils.successJson(jsondata)
#     backendlogger.info('getCurrentAppBookingforDate', logrequest, httpoutput, logmessage)
#     # requestlogger.info('getCurrentAppBookingForDate;%s'%(str(httpoutput)))
#     return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)
#
# @csrf_exempt
# def getDoctorAvailabilityForDateAndCity(request):
#     logrequest = {}
#     logmessage = []
#     for key, value in request.GET.items():
#         logrequest[key] = value
#     authenticated,djangouserstr = authenticateURL(request)
#     if not authenticated:
#         backendlogger.error('getDoctorAvailabilityForDateAndCity', logrequest, 'Error authenticating user', logmessage)
#         return HttpResponse("Error authenticating user", status=401)
#
#     date_req = request.GET[ServerConstants.DATE]
#     try:
#         cityname_req = request.GET[ServerConstants.CITYNAME]
#     except KeyError:
#         cityname_req = None
#
#     availabledoctors = DoctorAvailability.objects.filter(date=date_req,doctor__city=cityname_req.lower())
#
#     #modify availability accrding to kiosk availability
#     doctorlist = list()
#     for thisdoctor in availabledoctors:
#         doctorlist.append({
#             ServerConstants.DOCTORNAME:thisdoctor.doctor.name,
#             ServerConstants.DOCTOR_ID:thisdoctor.doctor.doctoruid,
#             ServerConstants.AVAILABILITYID:thisdoctor.availabilityid,
#             ServerConstants.SKILLSTR:thisdoctor.doctor.skillstr,
#             ServerConstants.PROFILEPIC:thisdoctor.doctor.profilepic,
#             ServerConstants.QBID:thisdoctor.doctor.qbid,
#             ServerConstants.REGISTRATIONNUM:thisdoctor.doctor.registrationnum,
#             ServerConstants.SPECIALIZATION:thisdoctor.doctor.specialization,
#             ServerConstants.QUALIFICATION:thisdoctor.doctor.qualification,
#             ServerConstants.EXPERIENCE:thisdoctor.doctor.experience,
#             ServerConstants.STARTTIME:str(thisdoctor.starttime),
#             ServerConstants.ENDTIME:str(thisdoctor.endtime),
#             ServerConstants.SLOTLENGTH:thisdoctor.slotlength,
#             ServerConstants.SLOTBOOKED:thisdoctor.slotbooked
#         })
#
#     jsondata = dict({ServerConstants.DOCTORLIST:doctorlist,
#                      ServerConstants.DATE:date_req,
#                      })
#     httpoutput = utils.successJson(jsondata)
#     backendlogger.info('getDoctorAvailabilityForDateAndCity', logrequest, httpoutput, logmessage)
#     # requestlogger.info('getDoctorAvailabilityForDateAndApp;%s'%(str(httpoutput)))
#     return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)

