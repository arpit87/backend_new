from django.conf.urls import url

from Appointments import views

urlpatterns = [


                       #VID CONF BOOKING
                       url(r'^slotBooking/',views.slotBooking,name='slotBooking'),
                       url(r'^cancelSlotBooking/',views.cancelSlotBooking,name='cancelSlotBooking'),
                       url(r'^getSlotBookingDetails/', views.getSlotBookingDetails, name='getSlotBookingDetails'),
                       url(r'^getUpcomingConsultationsByDateOrMonth', views.getUpcomingConsultationsByDateOrMonth, name="getUpcomingConsultationsByDateOrMonth"),
                       url(r'^setDoctorAvailability/', views.setDoctorAvailability, name='setDoctorAvailability'),
                       url(r'^deleteDoctorAvailability/', views.deleteDoctorAvailability, name='deleteDoctorAvailability'),
                       url(r'^setSlotBookingStatus/', views.setSlotBookingStatus, name='setSlotBookingStatus'),

]