from Doctor.models import DoctorDetails
from django.test import TestCase, override_settings

import base64
import json
import Platform
from backend_new.Constants import ServerConstants,FileTypeConstants,FileStorageConstants
import urllib3
from django.test.client import MULTIPART_CONTENT

from django.conf import settings
from django.contrib.auth.models import User
import backend_new.config
from Person.models import PersonDetails , AppDetails
import time
from django.utils import timezone
import Platform.Constants
import os


class UploadAPITestCase(TestCase):
    multi_db = True
    def setUp(self):
        backend_new.config.ISTESTMODE = True
        djangouser = User.objects.create(username="Doctoryolohealth",password="abc")
        djangouser1 = User.objects.create(username="Doctoryolohealth1",password="abc")
        usernew=PersonDetails.objects.create(djangouser=djangouser, personuid=1, name='arpit', phone="9736737273", dob_year=1987)
        # QBSESSIONDATA.objects.create(application_id=32324,token="asf",expiry=12456)
        Doctornew = DoctorDetails.objects.create(djangouser=djangouser,doctoruid=1,name="arpit",phone="978637272",skillstr="1,2",docs="testdoc1,testdoc2,http://yolohealth.in/static/uploadedfiles/Doctordocs/file1.png")
        Doctornew1 = DoctorDetails.objects.create(djangouser=djangouser1,doctoruid=11,name="arpit",phone="978637272",skillstr="1")
        kiosk = AppDetails.objects.create(kioskid=50, kiosktag="Mumbai-50", kiosklocation="Mumbai")
        os.environ.setdefault("GOOGLE_APPLICATION_CREDENTIALS", "/home/yolo/yolo_server/yoloprod-servicekey.json")

    def test_uploadFiles(self):
        with open("/home/yolo/yolo_server/testdata/logo.png", encoding="utf8", errors='ignore') as fp:
            input = {ServerConstants.PERSONTYPE:FileTypeConstants.DoctorPROFILEPIC,
                     ServerConstants.DOCTORUID:1,
                     ServerConstants.FILENAMES:"file.png",
                     "file.png":fp,
                }
            response = self.client.post('/Upload/uploadFiles/',input,content_type=MULTIPART_CONTENT)
            self.assertEqual(response.status_code,200)

    def test_uploadFilesWithURL(self):
        input = {ServerConstants.PERSONTYPE:FileTypeConstants.DERMASCOPEIMG,
                 ServerConstants.PERSONUID:1,
                 ServerConstants.FILEURLS:"https://yolohealth.in/images/partner/yolo/logonavbar_yolo.png",
            }
        response = self.client.post('/Upload/uploadFilesWithURL/',input)
        self.assertEqual(response.status_code,200)
        print(response.content)


    def test_uploadDocFiles(self):
        fp1 = open("/home/yolo/yolo_server/testdata/logo.png", 'rb')
        fp2 = open("/home/yolo/yolo_server/testdata/logo.png", 'rb')

        #data = f.read()
        input = {ServerConstants.PERSONTYPE:FileTypeConstants.DoctorDOCPIC,
                 ServerConstants.DOCTORUID:1,
                 ServerConstants.FILENAMES:"file1.png,file2.png",
                 "file1.png":fp1,
                 "file2.png":fp2,
            }
        response = self.client.post('/Upload/uploadFiles/',input,content_type=MULTIPART_CONTENT)
        self.assertEqual(response.status_code,200)



    def test_uploadGCSFiles(self):
        fp1 = open("/home/yolo/yolo_server/testdata/logo.png", 'rb')
        fp2 = open("/home/yolo/yolo_server/testdata/logo.png", 'rb')

        #data = f.read()
        input = {ServerConstants.PERSONTYPE:FileTypeConstants.IMAGEREPORT,
                 ServerConstants.PERSONUID:1,
                 ServerConstants.FILENAMES:"file1.png,file2.png",
                 "file1.png":fp1,
                 "file2.png":fp2,
                 "file1.png"+ServerConstants.REPORTTITLE:"file one xray",
                 "file2.png"+ServerConstants.REPORTTITLE:"file two xray",
                 "file1.png"+ServerConstants.REPORTDESCRIPTION:"hand",
                 "file2.png"+ServerConstants.REPORTDESCRIPTION:"knee",
            }
        response = self.client.post('/Upload/uploadFiles/',input,content_type= MULTIPART_CONTENT)
        self.assertEqual(response.status_code,200)


    def test_uploadDermaImg(self):
        input = {
                 ServerConstants.PERSONTYPE:FileTypeConstants.DERMASCOPEIMG,
                 ServerConstants.PERSONUID:1,
                 ServerConstants.FILENAMES:"dermascopeimg.png",
                 ServerConstants.IMGDATA:base64.b16encode(b"kjhk"),
                 "dermascopeimg.png"+ServerConstants.REPORTTITLE:"Dermascope Img",
                 "dermascopeimg.png"+ServerConstants.REPORTDESCRIPTION:"Dermascope Img Des",
                 ServerConstants.APPSECRET: backend_new.config.APPSECRET,
                 ServerConstants.KIOSKID:50
            }
        response = self.client.post('/Upload/uploadImageDataBase64/',input)
        self.assertEqual(response.status_code,200)



    def test_uploadDermaMultipleImg(self):
        input = {
                 ServerConstants.PERSONTYPE:FileTypeConstants.DERMASCOPEIMG,
                 ServerConstants.PERSONUID:1,
                 ServerConstants.FILENAMES:"dermascopeimg.png",
                 ServerConstants.IMGDATA:"kjhk,asdfasd",
                 "dermascopeimg.png"+ServerConstants.REPORTTITLE:"Dermascope Img",
                 "dermascopeimg.png"+ServerConstants.REPORTDESCRIPTION:"Dermascope Img Des",
                 ServerConstants.APPSECRET: backend_new.config.APPSECRET,
            }
        response = self.client.post('/Upload/uploadImageDataBase64/',input)
        self.assertEqual(response.status_code,200)


    def test_uploadEcgImg(self):
        #first save ecg report
        reportdata = dict({ServerConstants.BPMACHINE:{"systolic":80,"diastolic":120,"pulse":72},
                           ServerConstants.OXIMETERMACHINE:{"oxygensat":154,"pulse":72},
                           ServerConstants.SPIROMETERMACHINE:{"pef":315,"fvc":0.60,"fvc1":0.65},
                           ServerConstants.URINEMACHINE:{"glucose":"5 TRACE","ketone":"SMALL 15","bilirubin":"SMALL +","gravity":"1.000","ph":"7.0","blood":"HEMOLYZED 10 TRACE","urobilinogen":"Nagativ","protein":"TRACE","nitrite":"Positive","leukocytes":"SMALL 70"},
                           ServerConstants.HIVMACHINE:{"c":1,"t1":0,"t2":0},
                           ServerConstants.WEIGHTMACHINE:{"bmi":12,"fat":17,"weight":65.55,"muscle":25,"hydration":55,"bonemass":2.5},
                           ServerConstants.HEIGHTMACHINE:{"height":168},
                           ServerConstants.TEMPERATUREMACHINE:{"temperature":39.5},
                           ServerConstants.HEMOGLOBINMACHINE:{"hb":13.5,"hct":40},
                           ServerConstants.BLOODGLUCOSEMACHINE:{"glucose_level":89, "testtype": 'fasting'},
                           ServerConstants.HEALTHSCORE:85,
                           ServerConstants.ECGMACHINE:{"lead1":"1026,948,898,841,788,729,612,541","lead2":"1234,2154,2124","lead3":"","avr":"","avl":"","avf":"","v1":"","v2":"","v3":"","v4":"","v5":"","v6":""}
                           })
        input = json.dumps({ServerConstants.PERSONUID:1,
                            ServerConstants.REPORTDATA:reportdata,
                            ServerConstants.REPORTTYPE:0,
                            ServerConstants.DATETIME:time.mktime(timezone.now().timetuple()),
                            ServerConstants.KIOSKID:50})

        response = self.client.post('/BodyVitals/setReportForUser/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        responsejson = json.loads(response.content)
        reportid = responsejson["body"][ServerConstants.REPORTID]
        input = {
                 ServerConstants.PERSONTYPE:FileTypeConstants.ECGLEADIMG,
                 ServerConstants.PERSONUID:1,
                 ServerConstants.FILENAMES:"lead2",
                 ServerConstants.IMGDATA:base64.b16encode(b"data:image/png;base64,kjhk"),
                 "lead2"+ServerConstants.REPORTTITLE:"ECG LEAD2",
                 "lead2"+ServerConstants.REPORTDESCRIPTION:"ECG Img Lead2",
                 ServerConstants.APPSECRET: backend_new.config.APPSECRET,
                 ServerConstants.REPORTID:reportid
            }
        response = self.client.post('/Upload/uploadImageDataBase64/',input)
        self.assertEqual(response.status_code,200)
        input = dict({ServerConstants.PERSONUID:1,
                      ServerConstants.MACHINESTR:"bpmachine,temperaturemachine,ecgmachine"
                            })
        response = self.client.get('/BodyVitals/getLatestUserReport/',input)
        self.assertEqual(response.status_code,200)


    def test_uploadEcgImgBlob(self):
        #first save ecg report
        reportdata = dict({ServerConstants.BPMACHINE:{"systolic":80,"diastolic":120,"pulse":72},
                           ServerConstants.OXIMETERMACHINE:{"oxygensat":154,"pulse":72},
                           ServerConstants.SPIROMETERMACHINE:{"pef":315,"fvc":0.60,"fvc1":0.65},
                           ServerConstants.URINEMACHINE:{"glucose":"5 TRACE","ketone":"SMALL 15","bilirubin":"SMALL +","gravity":"1.000","ph":"7.0","blood":"HEMOLYZED 10 TRACE","urobilinogen":"Nagativ","protein":"TRACE","nitrite":"Positive","leukocytes":"SMALL 70"},
                           ServerConstants.HIVMACHINE:{"c":1,"t1":0,"t2":0},
                           ServerConstants.WEIGHTMACHINE:{"bmi":12,"fat":17,"weight":65.55,"muscle":25,"hydration":55,"bonemass":2.5},
                           ServerConstants.HEIGHTMACHINE:{"height":168},
                           ServerConstants.TEMPERATUREMACHINE:{"temperature":39.5},
                           ServerConstants.HEMOGLOBINMACHINE:{"hb":13.5,"hct":40},
                           ServerConstants.BLOODGLUCOSEMACHINE:{"glucose_level":89, "testtype": 'fasting'},
                           ServerConstants.HEALTHSCORE:85,
                           ServerConstants.ECGMACHINE:{"lead1":"1026,948,898,841,788,729,612,541","lead2":"1234,2154,2124","lead3":"","avr":"","avl":"","avf":"","v1":"","v2":"","v3":"","v4":"","v5":"","v6":""}
                           })
        input = json.dumps({ServerConstants.PERSONUID:1,
                            ServerConstants.REPORTDATA:reportdata,
                            ServerConstants.REPORTTYPE:0,
                            ServerConstants.DATETIME:time.mktime(timezone.now().timetuple()),
                            ServerConstants.KIOSKID:50})

        response = self.client.post('/BodyVitals/setReportForUser/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        responsejson = json.loads(response.content)
        reportid = responsejson["body"][ServerConstants.REPORTID]
        fp1 = open("/home/yolo/yolo_server/testdata/lead1.svg")
       #data = f.read()
        input = {
                 ServerConstants.REPORTID:reportid,
                 ServerConstants.PERSONTYPE:FileTypeConstants.ECGLEADIMG,
                 ServerConstants.PERSONUID:1,
                 ServerConstants.FILENAMES:"lead1.svg",
                 "lead1.svg":fp1,
                }
        response = self.client.post('/Upload/uploadFiles/',input,content_type= MULTIPART_CONTENT)
        self.assertEqual(response.status_code,200)
        input = dict({ServerConstants.PERSONUID:1,
                      ServerConstants.MACHINESTR:"bpmachine,temperaturemachine,ecgmachine"
                            })
        response = self.client.get('/BodyVitals/getLatestUserReport/',input)
        self.assertEqual(response.status_code,200)
        print(response.content)

    def test_createofflineconsultanduploadofflinevideo(self):

        input = json.dumps({
                 ServerConstants.PERSONUID:1,
                 ServerConstants.PROBLEM:"Cough",
                 ServerConstants.KIOSKID:50
                })
        response = self.client.post('/HealthPrograms/startnewofflineconsult/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        responsejson = json.loads(response.content)
        consultid = responsejson["body"][ServerConstants.HEALTHCASEID]

        fp1 = open("/home/yolo/yolo_server/testdata/forest.mp4", 'rb')
       #data = f.read()
        input = {
                 ServerConstants.HEALTHCASEID:consultid,
                 ServerConstants.PERSONTYPE:FileTypeConstants.CONSULTVIDEO,
                 ServerConstants.PERSONUID:1,
                 ServerConstants.FILENAMES:"abc.mp4",
                 "abc.mp4":fp1,
                }
        response = self.client.post('/Upload/uploadFiles/',input,content_type= MULTIPART_CONTENT)
        self.assertEqual(response.status_code,200)


    # def test_uploadHeartBeatFiles(self):
    #     fp1 = open("/home/yolo/yolo_server/testdata/heartbeat.mp3")

    #     #data = f.read()
    #     input = {ServerConstants.PERSONTYPE:FileTypeConstants.HEARTBEATSOUND,
    #              ServerConstants.personuid:1,
    #              ServerConstants.FILENAMES:"heartbeat.mp3",
    #              "heartbeat.mp3":fp1,
    #              "heartbeat.mp3"+ServerConstants.REPORTTITLE:"heart beat",
    #              "heartbeat.mp3"+ServerConstants.REPORTDESCRIPTION:"heart beat",
    #         }
    #     response = self.client.post('/Upload/uploadFiles/',input,content_type= MULTIPART_CONTENT)
    #     self.assertEqual(response.status_code,200)
    #     print(response.content)


