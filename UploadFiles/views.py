import json
import requests
from datetime import datetime

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.core.exceptions import ObjectDoesNotExist
import time
import re
from django.utils import timezone
from django.http import HttpResponse
from firebase_admin._messaging_utils import UnregisteredError
from requests import HTTPError

from Doctor.models import DoctorDetails
from Lab.models import LabReport, BookedInvestigation
from Person.models import PersonDetails
from BodyVitals.models import imagereportdetails,AppDetails, report ,ecg
from HealthCase.models import HealthCase , uploadedvideo, attachedreporthealthcase, uploadedaudio, HealthCaseConstant
from Platform.fcm_message import send_to_token
from Platform.uid_code_converter import encode_id
from WebApp.models import AppTemplateDetails
# Create your views here.
from django.utils.datastructures import MultiValueDictKeyError
from django.views.decorators.csrf import csrf_exempt, requires_csrf_token
from django.db import IntegrityError

import os
from base64 import b64decode
import base64
from backend_new.Constants import ServerConstants, FileTypeDIRStorageMap, \
    FileTypeConstants, BookedInvestigationConstant, PreferredLanguageConstant
import logging
from django.conf import settings
from backend_new import config
from google.cloud import storage
from google.cloud.exceptions import NotFound
import tempfile
from Platform import backendlogger, utils
import Communications.utils
import uuid
import Communications
from Communications.smstemplates import getpatientvideotemplate , getdoctorvideotemplate

class SaveFileURLToModalError(Exception):
    def __init__(self, msg):
        self.msg = msg
    def __str__(self):
        return repr(self.msg)

logger = logging.getLogger(__name__)
requestlogger = logging.getLogger('apirequests')

@csrf_exempt
def uploadFiles(request):

    logmessage = []
    logmessage.append('uploading files..heha')

    #there is something in files?
    if request.method != 'POST':
        backendlogger.error('uploadFiles', 'wrong http method', 'no files attached', logmessage)
        # requestlogger.info("no files present")
        return HttpResponse("No files attached", status=400)

    logmessage.append("files present")
    # requestlogger.info("files present")
    filetype = request.POST[ServerConstants.FILETYPE]
    filenames = request.POST[ServerConstants.FILENAMES] #comma separated abc.png,pqr.png

    logmessage.append("filetype:%r,filenames%r"%(filetype,filenames))
    # requestlogger.info("filetype:%r,filenames%r"%(filetype,filenames))

    filestoragedir = FileTypeDIRStorageMap[filetype]
    filestoragepath = os.path.join(settings.MEDIA_ROOT,filestoragedir)
    filenamelist = filenames.split(',')

    allsavedfiles = ''
    for filename in filenamelist:
        filenametosave = utils.getRandomId(8)+str(time.time())+filename

        fullpathondisk = save_uploaded_file_to_disk(request.FILES[filename],filestoragepath,filenametosave)
        logmessage.append("saved local to disk")
        # requestlogger.info("saved local to disk")
        if filestoragedir==config.GCSIMAGEREPORTBUCKET  or filestoragedir==config.GCSCONSULTVIDEOBUCKET  or filestoragedir==config.GCSAUDIOREPORTBUCKET:
            logmessage.append("saving to GCS")
            try:
                thispersonuid = request.POST[ServerConstants.PERSONUID]
                PersonDetails.objects.get(personuid = thispersonuid)
                foldername =  "person" + str(thispersonuid)
            except:
                try:
                    thishealthcaseid = request.POST[ServerConstants.HEALTHCASEID]
                    healthcase = HealthCase.objects.get(healthcaseid = thishealthcaseid)
                    foldername = "person" + str(healthcase.person.personuid)
                except:
                    backendlogger.error('uploadFiles', 'error', 'person/personuid wrong/not sent', logmessage)
                    # requestlogger.info("no files present")
                    return HttpResponse("person/personuid wrong/not sent", status=403)

            savedfileurl = save_uploaded_file_to_gcloud(fullpathondisk,filestoragedir,filenametosave, foldername)

        elif filestoragedir==config.GCSAPPTEMPLATEIMAGESBUCKET:
            logmessage.append("saving to GCS")
            try:
                thisappid = request.POST[ServerConstants.APPUID]
                AppDetails.objects.get(appuid=thisappid)
                foldername =  "app" + str(thisappid)
            except:
                backendlogger.error('uploadFiles', 'error', 'app/appid wrong/not sent', logmessage)
                # requestlogger.info("no files present")
                return HttpResponse("app/appid wrong/not sent", status=403)

            savedfileurl = save_uploaded_file_to_gcloud(fullpathondisk, filestoragedir, filenametosave, foldername)
        else:
            savedfileurl = config.DOMAIN+os.path.join(settings.STATIC_URL,settings.UPLOADEDFILES_DIR,
                               filestoragedir,filenametosave)

        if allsavedfiles=='':
            allsavedfiles = savedfileurl
        else:
            allsavedfiles =','.join([allsavedfiles,savedfileurl])

    #save url to models
    try:
        #report id could be null to eg for profile pic
        # requestlogger.info("Will call save_uploaded_filepath_to_model")
        reportid = save_uploaded_filepath_to_model(request,allsavedfiles)
    except SaveFileURLToModalError as e:
        return HttpResponse(e.msg,status="404")

    jsondata = dict({ServerConstants.SAVEDFILESURL :allsavedfiles,
                     ServerConstants.REPORTID:reportid})
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('uploadFiles', dict(), httpoutput, logmessage)
    # requestlogger.info('uploadFilesResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

@csrf_exempt
def uploadFilesWithURL(request):

    logrequest = {}
    logmessage = []
    logmessage.append('uploading files..heha')

    #there is something in files?
    if request.method != 'POST':
        backendlogger.error('uploadFiles', 'wrong http method', 'no files attached', logmessage)
        # requestlogger.info("no files present")
        return HttpResponse("No files attached", status=400)

    # requestlogger.info("files present")
    filetype = request.POST[ServerConstants.FILETYPE]
    fileurls = request.POST[ServerConstants.FILEURLS] #comma separated urls of files to save

    logmessage.append("filetype:%r,fileurls%r"%(filetype,fileurls))
    # requestlogger.info("filetype:%r,filenames%r"%(filetype,filenames))

    filestoragedir = FileTypeDIRStorageMap[filetype]
    filestoragepath = os.path.join(settings.MEDIA_ROOT,filestoragedir)
    fileurllist = fileurls.split(',')

    allsavedfiles = ''
    for fileurl in fileurllist:
        filenametosave = str(uuid.uuid4())+".png"
        fullpathondisk = save_url_file_to_disk(fileurl,filestoragepath,filenametosave)
        logmessage.append("saved local to disk")
        # requestlogger.info("saved local to disk")
        if filestoragedir==config.GCSIMAGEREPORTBUCKET \
            or filestoragedir==config.GCSCONSULTVIDEOBUCKET :
            logmessage.append("saving to GCS")
            # requestlogger.info("saving to GCS")
            try:
                thispersonuid = request.POST[ServerConstants.PERSONUID]
                PersonDetails.objects.get(personuid = thispersonuid)
                foldername =  "person" + str(thispersonuid)
            except:
                try:
                    thishealthcaseid = request.POST[ServerConstants.HEALTHCASEID]
                    healthcase = HealthCase.objects.get(healthcaseid=thishealthcaseid)
                    foldername = "person" + str(healthcase.person.personuid)
                except:
                    backendlogger.error('uploadFiles', 'error', 'person/personuid wrong/not sent', logmessage)
                    # requestlogger.info("no files present")
                    return HttpResponse("person/personuid wrong/not sent", status=403)

            savedfileurl = save_uploaded_file_to_gcloud(fullpathondisk,filestoragedir,filenametosave, foldername)
            # if filestoragedir==config.GCSCONSULTVIDEOBUCKET:
            #     savedfileurl = config.FQDNDOMAIN+".global.prod.fastly.net"+"/"+filenametosave
        else:
            savedfileurl = config.DOMAIN+os.path.join(settings.STATIC_URL,settings.UPLOADEDFILES_DIR,
                               filestoragedir,filenametosave)

        if allsavedfiles=='':
            allsavedfiles = savedfileurl
        else:
            allsavedfiles =','.join([allsavedfiles,savedfileurl])

    #save url to models
    try:
        #report id could be null to eg for profile pic
        # requestlogger.info("Will call save_uploaded_filepath_to_model")
        reportid = save_uploaded_filepath_to_model(request,allsavedfiles)
    except SaveFileURLToModalError as e:
        return HttpResponse(e.msg,status="404")

    jsondata = dict({ServerConstants.SAVEDFILESURL :allsavedfiles,
                     ServerConstants.REPORTID:reportid})
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('uploadFiles', dict(), httpoutput, logmessage)
    # requestlogger.info('uploadFilesResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

@csrf_exempt
def save_uploaded_file_to_disk(f, path, filename):

    backendlogger.info("save_uploaded_file_to_disk", {'f':f, "path":path, 'filename':filename}, "handle_uploaded_file path%r,filename:%r"%(path,filename), " ")
    fullpath = ''.join([path,'/',filename])
    file = open(fullpath, 'wb')
    with file as destination:
        for chunk in f.chunks():
            destination.write(chunk)
    file.close()
    return fullpath

@csrf_exempt
def save_url_file_to_disk(url, path, filename):

    backendlogger.info("save_url_file_to_disk", {'url':url, "path":path, 'filename':filename}, "handle_url_file path%r,filename:%r"%(path,filename), " ")
    fullpath = ''.join([path,'/',filename])
    file = open(fullpath, 'wb')
    r = requests.get(url, allow_redirects=True)
    with file as destination:
            destination.write(r.content)
    file.close()
    return fullpath

def save_uploaded_filepath_to_model(request,savedfileurl):
    # requestlogger.info("===========save_uploaded_filepath_to_model==========")
    filetype = request.POST[ServerConstants.FILETYPE]
    logmessaage = []
    logmessaage.append("filetype:%r"%(filetype))
    backendlogger.info('save_uploaded_filepath_to_model', {'savedfileurl': savedfileurl},
                     "filetype:%r"%(filetype), logmessaage)
    # requestlogger.info("===========save_uploaded_filepath_to_model==========")
    # requestlogger.info("filetype:%r"%(filetype))
    if(filetype in [FileTypeConstants.DOCTORPROFILEPIC,FileTypeConstants.DOCTORDOCPIC,FileTypeConstants.DOCTORSIGNPIC]):
        #Doctor related images saved in this indent
        try:
            thisdoctoruid = request.POST[ServerConstants.DOCTORUID]
            backendlogger.info('save_uploaded_filepath_to_model', {'savedfileurl':savedfileurl}, 'profilepic for Doctor', logmessaage)
            logmessaage.append("profilepic for Doctor:%r"%(thisdoctoruid))
            # requestlogger.info("profilepic for Doctor:%r"%(thisDoctorid))
        except KeyError:
            backendlogger.error('save_uploaded_filepath_to_model', {'savedfileurl':savedfileurl}, 'doctoruid label not found in request', logmessaage)
            # requestlogger.info("doctoruid label not found in request")
            raise SaveFileURLToModalError("doctoruid label not found in request")
        try:
            Doctor = DoctorDetails.objects.get(doctoruid = thisdoctoruid)
            logmessaage.append("got Doctor:%r"%(thisdoctoruid))
            # requestlogger.info("got Doctor:%r"%(thisDoctorid))
            if filetype == FileTypeConstants.DOCTORPROFILEPIC:

                backendlogger.info('save_uploaded_filepath_to_model', {'savedfileurl': savedfileurl}, "profilepic for Doctor:%r,saved at:%r"%(thisdoctoruid,savedfileurl), logmessaage)
                # logmessaage.append("profilepic for Doctor:%r,saved at:%r"%(thisDoctorid,savedfileurl))
                # requestlogger.info("profilepic for Doctor:%r,saved at:%r"%(thisDoctorid,savedfileurl))
                Doctor.profilepic = savedfileurl
                Doctor.save()
            elif filetype == FileTypeConstants.DOCTORDOCPIC:
                logmessaage.append("docpic for Doctor:%r,saved at:%r"%(thisdoctoruid,savedfileurl))
                backendlogger.info('save_uploaded_filepath_to_model', {'savedfileurl': savedfileurl}, "docpic for Doctor:%r,saved at:%r" % (thisdoctoruid, savedfileurl), logmessaage)
                # requestlogger.info("docpic for Doctor:%r,saved at:%r"%(thisDoctorid,savedfileurl))
                alldocs = Doctor.docs
                if alldocs == None or alldocs == "":
                    alldocs = savedfileurl
                else:
                    alldocs = ','.join([alldocs,savedfileurl])
                Doctor.docs = alldocs
                Doctor.save()
            #elif for sign pic
            elif filetype == FileTypeConstants.DOCTORSIGNPIC:
                logmessaage.append("docpic for Doctor:%r,saved at:%r"%(thisdoctoruid,savedfileurl))
                backendlogger.info('save_uploaded_filepath_to_model', {'savedfileurl': savedfileurl}, "docpic for Doctor:%r,saved at:%r" % (thisdoctoruid, savedfileurl), logmessaage)
                Doctor.signpic = savedfileurl
                Doctor.save()
        except ObjectDoesNotExist:
            backendlogger.error('save_uploaded_filepath_to_model', {'savedfileurl': savedfileurl}, "Doctor not found for id:%r"%(thisdoctoruid), logmessaage)
            # logmessaage.append("Doctor not found for id:%r"%(thisDoctorid))
            # requestlogger.info("Doctor not found for id:%r"%(thisDoctorid))
            raise SaveFileURLToModalError("doctoruid:%r not found in database"%(thisdoctoruid))
    #scanned reports handled here
    elif (filetype in [FileTypeConstants.IMAGEREPORT]):
        person = None
        try:
            personuid_req = request.POST[ServerConstants.PERSONUID]
            try:
                person = PersonDetails.objects.get(personuid=personuid_req)
            except ObjectDoesNotExist:
                backendlogger.error('save_uploaded_filepath_to_model',
                                 {'savedfileurl': savedfileurl},
                                 "user not found for id:%r"%(personuid_req),
                                 logmessaage
                                 )
                raise SaveFileURLToModalError("user not found for id:%r"%(personuid_req))
        except KeyError:
                backendlogger.error('save_uploaded_filepath_to_model', {'savedfileurl': savedfileurl}, "provide personuid/ phone", logmessaage)
                raise SaveFileURLToModalError("Provide personuid")

        logmessaage.append("scanned report for personuid:%r,to be saved at:%r"%(person.personuid,savedfileurl))

        reporttype = filetype

        try:
            title = request.POST[ServerConstants.IMAGEREPORTTITLE]
        except KeyError:
            title = "N/A"

        try:
            description = request.POST[ServerConstants.IMAGEREPORTDESCRIPTION]
        except KeyError:
            description = None

        try:
            appuid = request.POST[ServerConstants.APPUID]
            appdetails = AppDetails.objects.get(appuid=appuid)
        except KeyError:
            appdetails = None  # Default kiosk 100

        try:
            backendlogger.info('save_uploaded_filepath_to_model', {'savedfileurl': savedfileurl}, "saving imagereport title:"+title+",reportimgurl:"+savedfileurl, logmessaage)
            createuploaddata = imagereportdetails.objects.create (person = person, reportimgurl=savedfileurl, reporttitle=title, reporttype=reporttype,
                                                  reportdescription = description,
                                                   appdetails=appdetails)
            createuploaddata.imagereportid = encode_id(createuploaddata.id)
            createuploaddata.save()
        except IntegrityError as e:
            backendlogger.error('save_uploaded_filepath_to_model', {'savedfileurl': savedfileurl}, "Intergerity error", logmessaage)
            raise SaveFileURLToModalError("IntegrityError")

        try:
            healthcaseid = request.POST[ServerConstants.HEALTHCASEID]
            try:
                healthcase = HealthCase.objects.get(healthcaseid=healthcaseid)
            except (ObjectDoesNotExist, KeyError):
                backendlogger.error('uploadImage', dict(), 'healthcase consult not found', logmessaage)
                raise SaveFileURLToModalError("healthcase consult not found")
            try:
                reporttype_req = 3
                uploaderrole_req = request.POST[ServerConstants.UPLOADERROLE]
                uploaderuid_req = request.POST[ServerConstants.UPLOADERUID]
            except  KeyError as e:
                backendlogger.error('uploadImage', dict(), str(e), logmessaage)
                raise SaveFileURLToModalError('uploaderrole/uploaderid not sent')

            try:
                attachedreporthealthcase.objects.create(healthcase=healthcase, attachedreportid=createuploaddata.imagereportid,
                                                        attachedreporttype=reporttype_req,
                                                        attachedreporttitle=title,
                                                        attachedreportdescription=description,
                                                        uploaderrole=uploaderrole_req,
                                                        uploaderuid=uploaderuid_req)
            except Exception as e:
                logmessaage.append(e)
                backendlogger.error('uploadImage', dict(), str(e), logmessaage)
                raise SaveFileURLToModalError('report already attached')

            if uploaderrole_req.lower() == "doctor" or uploaderrole_req.lower() == "hospitalcoordinator":
                Communications.utils.sendSMS("Doctor has uploaded {}. Click to view: {}".format(title, healthcase.shorturldoctor),
                                             healthcase.person.phone, None, None)
        except:
            pass
        return createuploaddata.imagereportid

    elif (filetype in [FileTypeConstants.USERPROFILEPIC]):

        # user related images will be saved in this indent
        try:
            thispersonuid = request.POST[ServerConstants.PERSONUID]
            backendlogger.info('save_uploaded_filepath_to_model', {'savedfileurl': savedfileurl},
                            'profilepic for user', logmessaage)
            logmessaage.append("profilepic for user:%r" % (thispersonuid))
            # requestlogger.info("profilepic for Doctor:%r"%(thisDoctorid))
        except KeyError:
            backendlogger.error('save_uploaded_filepath_to_model', {'savedfileurl': savedfileurl},
                             'personuid label not found in request', logmessaage)
            # requestlogger.info("doctoruid label not found in request")
            raise SaveFileURLToModalError("personuid label not found in request")

        try:
            person = PersonDetails.objects.get(personuid=thispersonuid)
            logmessaage.append("got Doctor:%r" % (thispersonuid))
            # requestlogger.info("got Doctor:%r"%(thisDoctorid))
            if filetype == FileTypeConstants.USERPROFILEPIC:

                backendlogger.info('save_uploaded_filepath_to_model', {'savedfileurl': savedfileurl},
                                "profilepic for User:%r,saved at:%r" % (thispersonuid, savedfileurl), logmessaage)
                # logmessaage.append("profilepic for Doctor:%r,saved at:%r"%(thisDoctorid,savedfileurl))
                # requestlogger.info("profilepic for Doctor:%r,saved at:%r"%(thisDoctorid,savedfileurl))
                person.profilepic = savedfileurl
                person.save()
        except ObjectDoesNotExist:
            backendlogger.error('save_uploaded_filepath_to_model', {'savedfileurl': savedfileurl},
                             "Doctor not found for id:%r" % (thispersonuid), logmessaage)
            # logmessaage.append("Doctor not found for id:%r"%(thisDoctorid))
            # requestlogger.info("Doctor not found for id:%r"%(thisDoctorid))
            raise SaveFileURLToModalError("doctoruid:%r not found in database" % (thispersonuid))

    elif (filetype in [FileTypeConstants.ECGLEADIMG,FileTypeConstants.ECGIMG]):
        reportid_req = request.POST[ServerConstants.REPORTID]
        filename_req = request.POST[ServerConstants.FILENAMES]
        backendlogger.info("ecg upload ",reportid_req+" "+filename_req,savedfileurl,"")
        try:
            thisecgreport = report.objects.get(reportid=reportid_req)
            backendlogger.info("reportid got ecg:",str(thisecgreport.ecg),savedfileurl,"")
            thisecgreport.ecg.ecg_img=savedfileurl
            thisecgreport.ecg.save()
        except ObjectDoesNotExist:
            backendlogger.error('save_uploaded_filepath_to_model', {'savedfileurl': savedfileurl},
                             "report not found for id:%r"%(reportid_req),
                             logmessaage
                             )
            raise SaveFileURLToModalError("report not found for id:%r"%(reportid_req))
        return reportid_req

    elif (filetype in [FileTypeConstants.LABREPORT]):

        try:
            personuid_req = request.POST[ServerConstants.PERSONUID]
            try:
                person = PersonDetails.objects.get(personuid=personuid_req)
            except ObjectDoesNotExist:
                backendlogger.error('save_uploaded_filepath_to_model',
                                 {'savedfileurl': savedfileurl},
                                 "user not found for id:%r" % (personuid_req),
                                 logmessaage
                                 )
                # logmessaage.append("user not found for id:%r"%(personuid_req))
                # requestlogger.info("user not found for id:%r"%(personuid_req))
                raise SaveFileURLToModalError("user not found for id:%r" % (personuid_req))
        except KeyError:
            backendlogger.error('save_uploaded_filepath_to_model', {'savedfileurl': savedfileurl}, "provide personuid",
                             logmessaage)
            # logmessaage.append("provide personuid or phone")
            # requestlogger.info("provide personuid or phone")
            raise SaveFileURLToModalError("Provide personuid")

        logmessaage.append("lab report for personuid:%r,to be saved at:%r" % (person.personuid, savedfileurl))

        try:
            uploaderrole_req = request.POST[ServerConstants.UPLOADERROLE]
            uploaderuid_req = request.POST[ServerConstants.UPLOADERUID]
        except  KeyError as e:
            backendlogger.error('uploadImage', dict(), str(e), logmessaage)
            raise SaveFileURLToModalError('uploaderrole/uploaderid not sent')

        # requestlogger.info("scanned report for personuid:%r,to be saved at:%r"%(user.personuid,savedfileurl))
        reportid = utils.getRandomId(8)

        try:
            title = request.POST[ServerConstants.LABREPORTTITLE]
        except KeyError:
            title = "N/A"

        try:
            backendlogger.info('save_uploaded_filepath_to_model', {'savedfileurl': savedfileurl},
                            "saving lab report title:" + title + ",reporturl:" + savedfileurl + ",id:" + reportid,
                            logmessaage)


            createuploaddata = LabReport(person=person, reporturl=savedfileurl, reporttitle=title,
                                         uploaderrole = uploaderrole_req, uploaderuid=uploaderuid_req)
            createuploaddata.save()

            if request.POST[ServerConstants.BOOKINGID]:
                bookingid = request.POST[ServerConstants.BOOKINGID]
                try:
                    booked_investigation = BookedInvestigation.objects.get(id=bookingid)
                    booked_investigation.labreport = createuploaddata
                    booked_investigation.status = BookedInvestigationConstant.COMPLETED
                    booked_investigation.save()
                    investigationname = booked_investigation.investigation.investigationname
                    UPLOADED_REPORT_MESSAGE = 'The results for the Diagnostic test: "{}" are available now. Please visit the clinic to view the results'
                    msg = UPLOADED_REPORT_MESSAGE.format(investigationname)
                    print('Length of msg: ', len(msg))
                    Communications.utils.sendSMS(msg, person.phone, None, None)
                except Exception as e:
                    requestlogger.info("exception:"+str(e))
                    backendlogger.error('save_uploaded_filepath_to_model', {'savedfileurl': savedfileurl},
                                     "Intergerity error", logmessaage)
            return reportid
        except IntegrityError as e:
            requestlogger.info("exception:" + str(e))
            backendlogger.error('save_uploaded_filepath_to_model', {'savedfileurl': savedfileurl}, "Intergerity error",
                             logmessaage)
            raise SaveFileURLToModalError("IntegrityError")
    elif (filetype in [FileTypeConstants.CONSULTVIDEO]):
        try:
            healthcaseid_req = request.POST[ServerConstants.HEALTHCASEID]
        except KeyError:
            backendlogger.error('save_uploaded_filepath_to_model', {'savedfileurl': savedfileurl},
                             "consultid not sent:%r", logmessaage
                             )
            raise SaveFileURLToModalError("healthcaseid not found")

        try:
            personuid_req = request.POST[ServerConstants.PERSONUID]
            person = PersonDetails.objects.get(personuid=personuid_req)
        except (KeyError, ObjectDoesNotExist):
            person = None

        try:
            doctoruid_req = request.POST[ServerConstants.DOCTORUID]
            doctor = DoctorDetails.objects.get(doctoruid=doctoruid_req)
        except (KeyError, ObjectDoesNotExist):
            doctor = None

        try:
            thishealthcase = HealthCase.objects.get(healthcaseid=healthcaseid_req)
            uploadedvideo.objects.create(healthcase=thishealthcase, videourl=savedfileurl, person=person,
                                         doctor=doctor)
            thishealthcase.save()

            if doctor != None: # means response by doctor so notify patient
                Communications.utils.sendSMS(getdoctorvideotemplate(thishealthcase.person.preferredlanguage, thishealthcase.doctor.name,thishealthcase.shorturlpatient),
                                             thishealthcase.person.phone, None, thishealthcase.person.preferredlanguage != PreferredLanguageConstant.ENGLISH )
                thishealthcase.status = HealthCaseConstant.ONGOING_DOCTORRESPONDED
                thishealthcase.save()
            elif thishealthcase.status is not HealthCaseConstant.NEW:
                    thishealthcase.status = HealthCaseConstant.ONGOING_PERSONRESPONDED
                    thishealthcase.save()
                    # send fcm to doctor
                    try:
                        doctorfcmtoken = thishealthcase.doctor.doctorfcmtokenmapping.fcmtoken
                        send_to_token(doctorfcmtoken,
                                      {"command": "patientresponse", "shorturldoctor": thishealthcase.shorturldoctor,
                                       "patientname": thishealthcase.person.name,  "patientid": str(thishealthcase.person.personid)})
                    except (ObjectDoesNotExist, HTTPError, UnregisteredError):
                        Communications.utils.sendSMS(getpatientvideotemplate(thishealthcase.person.preferredlanguage,
                                                      thishealthcase.person.name, thishealthcase.shorturldoctor),
                                                     thishealthcase.doctor.phone, None,
                                                     thishealthcase.person.preferredlanguage != PreferredLanguageConstant.ENGLISH)



        except ObjectDoesNotExist:
            backendlogger.error('save_uploaded_filepath_to_model', {'savedfileurl': savedfileurl},
                             "offline consultif not found for id:%r" % (healthcaseid_req),
                             logmessaage
                             )
            raise SaveFileURLToModalError("offline consult id not found for id:%r" % (healthcaseid_req))
    elif (filetype in [FileTypeConstants.APPBACKGROUNDIMAGE]):
        try:
            appuid_req = request.POST[ServerConstants.APPUID]
        except KeyError:
            backendlogger.error('save_uploaded_filepath_to_model', {'savedfileurl': savedfileurl},
                                "consultid not sent:%r", logmessaage
                                )
            raise SaveFileURLToModalError("healthcaseid not found")

        try:
            thisapptemplatedetails = AppTemplateDetails.objects.get(appdetails__appuid=appuid_req)
            thisapptemplatedetails.imagesstr = ",".join([thisapptemplatedetails.imagesstr,savedfileurl])
            thisapptemplatedetails.save()

        except ObjectDoesNotExist:
            backendlogger.error('save_uploaded_filepath_to_model', {'savedfileurl': savedfileurl},
                                "apptemplate not found for appidid:%r" % (appuid_req),
                                logmessaage
                                )
            raise SaveFileURLToModalError("app tempalte not foudn for app id not found for id:%r" % (appuid_req))
    elif (filetype in [FileTypeConstants.APPLOGO]):
        try:
            appuid_req = request.POST[ServerConstants.APPUID]
        except KeyError:
            backendlogger.error('save_uploaded_filepath_to_model', {'savedfileurl': savedfileurl},
                                "consultid not sent:%r", logmessaage
                                )
            raise SaveFileURLToModalError("healthcaseid not found")

        try:
            thisapptemplatedetails = AppTemplateDetails.objects.get(appdetails__appuid=appuid_req)
            thisapptemplatedetails.logo = savedfileurl
            thisapptemplatedetails.save()

        except ObjectDoesNotExist:
            backendlogger.error('save_uploaded_filepath_to_model', {'savedfileurl': savedfileurl},
                                "apptemplate not found for appidid:%r" % (appuid_req),
                                logmessaage
                                )
            raise SaveFileURLToModalError("app tempalte not foudn for app id not found for id:%r" % (appuid_req))
    elif (filetype in [FileTypeConstants.AUDIOREPORT]):
        try:
            healthcaseid_req = request.POST[ServerConstants.HEALTHCASEID]
        except KeyError:
            backendlogger.error('save_uploaded_filepath_to_model', {'savedfileurl': savedfileurl},
                             "consultid not sent:%r", logmessaage
                             )
            raise SaveFileURLToModalError("healthcaseid not found")

        try:
            personuid_req = request.POST[ServerConstants.PERSONUID]
            person = PersonDetails.objects.get(personuid=personuid_req)
        except (KeyError, ObjectDoesNotExist):
            person = None

        try:
            doctoruid_req = request.POST[ServerConstants.DOCTORUID]
            doctor = DoctorDetails.objects.get(doctoruid=doctoruid_req)
        except (KeyError, ObjectDoesNotExist):
            doctor = None

        try:
            thishealthcase = HealthCase.objects.get(healthcaseid=healthcaseid_req)
            uploadedaudio.objects.create(healthcase=thishealthcase, audiourl=savedfileurl, person=person,
                                         doctor=doctor)
            thishealthcase.save()

            if doctor != None: # means response by doctor so notify patient
                thishealthcase.status = HealthCaseConstant.ONGOING_DOCTORRESPONDED
                thishealthcase.save()
            elif thishealthcase.status is not HealthCaseConstant.NEW:
                    thishealthcase.status = HealthCaseConstant.ONGOING_PERSONRESPONDED
                    thishealthcase.save()

        except ObjectDoesNotExist:
            backendlogger.error('save_uploaded_filepath_to_model', {'savedfileurl': savedfileurl},
                             "offline consultif not found for id:%r" % (healthcaseid_req),
                             logmessaage
                             )
            raise SaveFileURLToModalError("offline consult id not found for id:%r" % (healthcaseid_req))


@csrf_exempt
def save_uploaded_file_to_gcloud(localfilename,bucketname,filename,foldername):
    credfile = (os.environ.get("GOOGLE_APPLICATION_CREDENTIALS",
                     None))
    logrequest = {'localfilename':localfilename, 'bucketname':bucketname, 'filename':filename}
    logmessage = []
    if credfile:
        logmessage.append("made credentials:"+credfile)
        # requestlogger.info("made credentials:"+credfile)
    else:
        logmessage.append("cred file none")
        # requestlogger.info("cred file none")
    gcs = storage.Client.from_service_account_json(config.GCPSERVICEKEYPATH)
    logmessage.append("made client")
    # requestlogger.info("made client")
    try:
        bucket = gcs.get_bucket(bucketname)
        logmessage.append("got bucket")
        # requestlogger.info("got bucket")
    except NotFound:
        bucket = gcs.create_bucket(bucketname)
        logmessage.append("creating bucket")
        # requestlogger.info("creating bucket")

    foldername = foldername +'/'
    blob = bucket.blob(foldername)
    blob.upload_from_string('', content_type='application/x-www-form-urlencoded;charset=UTF-8')
    # blob = storage.Blob(filename, bucket=bucket)
    logmessage.append("uploading files")
    # requestlogger.info("uploading file")
    # blob = storage.Blob(filename, bucket=bucket)
    # logmessage.append("uploading files")
    #requestlogger.info("uploading file")

    content_type=None
    if filename[len(filename)-4:len(filename)] == ".png":
        content_type = "image/png"
    elif filename[len(filename) - 4:len(filename)] == ".jpg":
        content_type = "image/jpg"
    elif filename[len(filename) - 4:len(filename)] == ".pdf":
        content_type = "application/pdf"
    elif filename[len(filename)-4:len(filename)] == ".svg":
        content_type = "image/svg+xml"

    blob = bucket.blob(str(foldername) + filename)
    blob.upload_from_filename(localfilename,content_type=content_type)
    blob.make_public()
    backendlogger.info('save_uploaded_file_to_gcloud', logrequest, 'file uplaoded', logmessage)
    # requestlogger.info("file uploaded")
    return blob.public_url

@csrf_exempt
def save_uploaded_voicefile_to_gcloud(localfilename,bucketname,filename,personuid):
    credfile = (os.environ.get("GOOGLE_APPLICATION_CREDENTIALS",
                     None))
    logrequest = {'localfilename':localfilename, 'bucketname':bucketname, 'filename':filename}
    logmessage = []
    if credfile:
        logmessage.append("made credentials:"+credfile)
        # requestlogger.info("made credentials:"+credfile)
    else:
        logmessage.append("cred file none")
        # requestlogger.info("cred file none")
    gcs = storage.Client.from_service_account_json(config.GCPSERVICEKEYPATH)
    logmessage.append("made client")
    # requestlogger.info("made client")
    bucketname=bucketname
    foldername='user'+str(personuid)+'/'
    try:
        bucket = gcs.get_bucket(bucketname)
        logmessage.append("got bucket")
        # requestlogger.info("got bucket")
    except NotFound:
        bucket = gcs.create_bucket(bucketname)
        logmessage.append("creating bucket")
        # requestlogger.info("creating bucket")
    blob = bucket.blob('foldername')
    blob.upload_from_string('', content_type='application/x-www-form-urlencoded;charset=UTF-8')
    # blob = storage.Blob(filename, bucket=bucket)
    logmessage.append("uploading files")
    #requestlogger.info("uploading file")

    content_type=None
    if filename[len(filename)-4:len(filename)] == ".png":
        content_type = "image/png"
    elif filename[len(filename)-4:len(filename)] == ".svg":
        content_type = "image/svg+xml"

    blob = bucket.blob(str(foldername) + filename)
    blob.upload_from_filename(localfilename,content_type=content_type)
    blob.make_public()
    backendlogger.info('save_uploaded_file_to_gcloud', logrequest, 'file uplaoded', logmessage)
    # requestlogger.info("file uploaded")
    return blob.public_url


@csrf_exempt
def uploadImageDataBase64(request):

   # data = request.body
    logmessage = []
    logmessage.append("uploading imd data")
    filetype = request.POST[ServerConstants.FILETYPE]
    allimgdata_req = request.POST[ServerConstants.IMGDATA]
    logmessage.append("got imgdata,filetype,filename")
    filestoragedir = FileTypeDIRStorageMap[filetype]
    filestoragepath = os.path.join(settings.MEDIA_ROOT,filestoragedir)

    appuid_req = None
    try:
      appuid_req = request.POST[ServerConstants.APPUID]
    except:
        pass

    data = dict({
        ServerConstants.FILETYPE:filetype,
    })

    if(filetype == FileTypeConstants.DERMASCOPEIMG or filetype == FileTypeConstants.OTOSCOPESCOPEIMG or filetype == FileTypeConstants.ENDOSCOPEIMG or filetype == FileTypeConstants.FUNDUSIMG):
        #there could be multiple file data in imgdata_req so loop over them
        imgdataarray = allimgdata_req.split(",")
        allsavedfiles=""
        for imgdata_req in imgdataarray:
            imgdata_req += "==="
            #img_data = imgdata_req.decode("base64")
            img_data= base64.decodebytes(imgdata_req.encode())
            filenametosave = str(uuid.uuid4())+".png"
            filenametosave = utils.getRandomId(8)+str(time.time())+filenametosave
            fullpathondisk = ''.join([filestoragepath,'/',filenametosave])
            with open(fullpathondisk, "wb") as imagefile:
                imagefile.write(img_data)

            logmessage.append("saved local to disk")
            logmessage.append("file storage dir:"+filestoragedir)
            if filestoragedir==config.GCSIMAGEREPORTBUCKET:
                logmessage.append("saving to GCS")
                try:
                    thispersonuid = request.POST[ServerConstants.PERSONUID]
                    PersonDetails.objects.get(personuid=thispersonuid)
                    foldername = "person" + str(thispersonuid)
                except:
                    backendlogger.error('uploadFiles', 'error', 'person/personuid wrong/not sent', logmessage)
                    # requestlogger.info("no files present")
                    return HttpResponse("person/personuid wrong/not sent", status=403)
                savedfileurl = save_uploaded_file_to_gcloud(fullpathondisk, config.GCSIMAGEREPORTBUCKET, filenametosave,foldername)

            else:
                savedfileurl = config.DOMAIN+os.path.join(settings.STATIC_URL,settings.UPLOADEDFILES_DIR,
                               filestoragedir,filenametosave)

            if allsavedfiles=='':
                allsavedfiles = savedfileurl
            else:
                allsavedfiles =','.join([allsavedfiles,savedfileurl])


        #save url to models
        try:
            reportid = save_uploaded_filepath_to_model(request,allsavedfiles)
        except SaveFileURLToModalError as e:
            # backendlogger.error('uploadImageDataBase64', data, e.msg, logmessage)
            return HttpResponse(e.msg,status="404")

        jsondata = dict({ServerConstants.SAVEDFILESURL :allsavedfiles,
                         ServerConstants.REPORTID:reportid})
        httpoutput = utils.successJson(jsondata)
        backendlogger.info('uploadImageDataBase64', data, httpoutput, logmessage)
        # requestlogger.info('uploadImageDataBase64Response;%s'%(str(httpoutput)))
        return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

    elif(filetype == FileTypeConstants.USERPROFILEPIC):
        imgdata_req = allimgdata_req
        imgdata_req += '==='
        #img_data = imgdata_req.decode("base64")
        img_data = base64.decodebytes(imgdata_req.encode())
        filestoragedir = FileTypeDIRStorageMap[filetype]
        filestoragepath = os.path.join(settings.MEDIA_ROOT, filestoragedir)
        filenametosave = str(uuid.uuid4())+".png"
        fullpathondisk = ''.join([filestoragepath,'/',filenametosave])

        filetosave = open(fullpathondisk,"wb")
        filetosave.write(img_data)
        filetosave.close()

        savedfileurl = config.DOMAIN + os.path.join(settings.STATIC_URL, settings.UPLOADEDFILES_DIR,
                                                    filestoragedir, filenametosave)

        # save url to models
        try:
            save_uploaded_filepath_to_model(request, savedfileurl)
        except SaveFileURLToModalError as e:
            # backendlogger.error('uploadImageDataBase64', data, e.msg, logmessage)
            return HttpResponse(e.msg, status="404")

        if appuid_req:
            channel_layer = get_channel_layer()
            msg = {"type": "group.message", "command": "update_queue", }
            print('sending msg to group:{}, msg:{}'.format(str(appuid_req), str(msg)))
            async_to_sync(channel_layer.group_send)(str(appuid_req), msg)

        # temp.close()  # this deletes the tmp file
        jsondata = dict({ServerConstants.SAVEDFILESURL: savedfileurl})
        httpoutput = utils.successJson(jsondata)
        backendlogger.info('uploadImageDataBase64', data, httpoutput, logmessage)
        # requestlogger.info('uploadImageDataBase64Response;%s'%(str(httpoutput)))
        return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

    elif(filetype == FileTypeConstants.ECGLEADIMG):
        imgdata_req = allimgdata_req
        imgdata_req += "==="
        #img_data = imgdata_req.decode("base64")
        img_data = base64.decodebytes(imgdata_req.encode())
        temp = tempfile.NamedTemporaryFile()
        # img_file = open(temp.name, "wb")
        temp.write(img_data)
        temp.flush()
        filenametosave = str(uuid.uuid4())+".png"
        fullpathondisk = temp.name

        logmessage.append("saving to GCS")
        # requestlogger.info("saving to GCS")
        try:
            thispersonuid = request.POST[ServerConstants.PERSONUID]
            PersonDetails.objects.get(personuid=thispersonuid)
            foldername = "person" + str(thispersonuid)
        except:
            backendlogger.error('uploadFiles', 'error', 'person/personuid wrong/not sent', logmessage)
            # requestlogger.info("no files present")
            return HttpResponse("person/personuid wrong/not sent", status=403)
        savedfileurl = save_uploaded_file_to_gcloud(fullpathondisk, config.GCSIMAGEREPORTBUCKET, filenametosave,foldername)

        #save url to models
        try:
            save_uploaded_filepath_to_model(request,savedfileurl)
        except SaveFileURLToModalError as e:
            # backendlogger.error('uploadImageDataBase64', data, e.msg, logmessage)
            return HttpResponse(e.msg,status="404")

        temp.close() #this deletes the tmp file
        jsondata = dict({ServerConstants.SAVEDFILESURL :savedfileurl})
        httpoutput = utils.successJson(jsondata)
        backendlogger.info('uploadImageDataBase64', data, httpoutput, logmessage)
        # requestlogger.info('uploadImageDataBase64Response;%s'%(str(httpoutput)))
        return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)
    elif(filetype == FileTypeConstants.IMAGEREPORT):
        imgdata_req = allimgdata_req
        imgdata_req += "==="
        #Made by Arnav. Changes in Base 64
        #img_data = imgdata_req.decode("base64")
        img_data = base64.decodebytes(imgdata_req.encode())
        temp = tempfile.NamedTemporaryFile()
        # img_file = open(temp.name, "wb")
        temp.write(img_data)
        temp.flush()
        filenametosave = str(uuid.uuid4())+".png"
        fullpathondisk = temp.name

        logmessage.append("saving to GCS")
        # requestlogger.info("saving to GCS")
        try:
            thispersonuid = request.POST[ServerConstants.PERSONUID]
            PersonDetails.objects.get(personuid=thispersonuid)
            foldername = "person" + str(thispersonuid)
        except:
            backendlogger.error('uploadFiles', 'error', 'person/personuid wrong/not sent', logmessage)
            # requestlogger.info("no files present")
            return HttpResponse("person/personuid wrong/not sent", status=403)
        savedfileurl = save_uploaded_file_to_gcloud(fullpathondisk, config.GCSIMAGEREPORTBUCKET, filenametosave,foldername)
        requestlogger.info("gcloud savefileurl:"+savedfileurl)
        #save url to models
        try:
            reportid = save_uploaded_filepath_to_model(request,savedfileurl)
        except SaveFileURLToModalError as e:
            backendlogger.error('uploadImageDataBase64', data, e.msg, logmessage)
            return HttpResponse(e.msg,status="404")

        temp.close() #this deletes the tmp file
        jsondata = dict({ServerConstants.SAVEDFILESURL :savedfileurl,ServerConstants.REPORTID :reportid})
        httpoutput = utils.successJson(jsondata)
        backendlogger.info('uploadImageDataBase64', data, httpoutput, logmessage)
        # requestlogger.info('uploadImageDataBase64Response;%s'%(str(httpoutput)))
        return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)
    elif (filetype == FileTypeConstants.APPBACKGROUNDIMAGE):
        imgdata_req = allimgdata_req
        imgdata_req += "==="
        # Made by Arnav. Changes in Base 64
        # img_data = imgdata_req.decode("base64")
        img_data = base64.decodebytes(imgdata_req.encode())
        temp = tempfile.NamedTemporaryFile()
        # img_file = open(temp.name, "wb")
        temp.write(img_data)
        temp.flush()
        filenametosave = str(uuid.uuid4()) + ".png"
        fullpathondisk = temp.name

        logmessage.append("saving to GCS")
        # requestlogger.info("saving to GCS")
        try:
            thisappid = request.POST[ServerConstants.APPUID]
            AppDetails.objects.get(appuid=thisappid)
            foldername = "app" + str(thisappid)
        except:
            backendlogger.error('uploadFiles', 'error', 'person/personuid wrong/not sent', logmessage)
            # requestlogger.info("no files present")
            return HttpResponse("person/personuid wrong/not sent", status=403)
        savedfileurl = save_uploaded_file_to_gcloud(fullpathondisk, config.GCSAPPTEMPLATEIMAGESBUCKET, filenametosave, foldername)
        requestlogger.info("gcloud savefileurl:" + savedfileurl)
        # save url to models
        try:
            save_uploaded_filepath_to_model(request, savedfileurl)
        except SaveFileURLToModalError as e:
            backendlogger.error('uploadImageDataBase64', data, e.msg, logmessage)
            return HttpResponse(e.msg, status="404")

        temp.close()  # this deletes the tmp file
        jsondata = dict({ServerConstants.SAVEDFILESURL: savedfileurl})
        httpoutput = utils.successJson(jsondata)
        backendlogger.info('uploadImageDataBase64', data, httpoutput, logmessage)
        # requestlogger.info('uploadImageDataBase64Response;%s'%(str(httpoutput)))
        return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)
    elif (filetype == FileTypeConstants.APPLOGO):
        imgdata_req = allimgdata_req
        imgdata_req += "==="
        # Made by Arnav. Changes in Base 64
        # img_data = imgdata_req.decode("base64")
        img_data = base64.decodebytes(imgdata_req.encode())
        temp = tempfile.NamedTemporaryFile()
        # img_file = open(temp.name, "wb")
        temp.write(img_data)
        temp.flush()
        filenametosave = str(uuid.uuid4()) + ".png"
        fullpathondisk = temp.name

        logmessage.append("saving to GCS")
        # requestlogger.info("saving to GCS")
        try:
            thisappid = request.POST[ServerConstants.APPUID]
            AppDetails.objects.get(appuid=thisappid)
            foldername = "app" + str(thisappid)
        except:
            backendlogger.error('uploadFiles', 'error', 'person/personuid wrong/not sent', logmessage)
            # requestlogger.info("no files present")
            return HttpResponse("person/personuid wrong/not sent", status=403)
        savedfileurl = save_uploaded_file_to_gcloud(fullpathondisk, config.GCSAPPTEMPLATEIMAGESBUCKET, filenametosave, foldername)
        requestlogger.info("gcloud savefileurl:" + savedfileurl)
        # save url to models
        try:
            save_uploaded_filepath_to_model(request, savedfileurl)
        except SaveFileURLToModalError as e:
            backendlogger.error('uploadImageDataBase64', data, e.msg, logmessage)
            return HttpResponse(e.msg, status="404")

        temp.close()  # this deletes the tmp file
        jsondata = dict({ServerConstants.SAVEDFILESURL: savedfileurl})
        httpoutput = utils.successJson(jsondata)
        backendlogger.info('uploadImageDataBase64', data, httpoutput, logmessage)
        # requestlogger.info('uploadImageDataBase64Response;%s'%(str(httpoutput)))
        return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)