from django.conf.urls import url

from UploadFiles import views

urlpatterns = [
                       url(r'^uploadFilesWithURL/',views.uploadFilesWithURL,name='uploadFilesWithURL'),
                       url(r'^uploadFiles/',views.uploadFiles,name='uploadFiles'),
                       url(r'^uploadImageDataBase64/',views.uploadImageDataBase64,name='uploadImageDataBase64'),
               ]