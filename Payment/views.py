# Create your views here.
import json

from firebase_admin._messaging_utils import UnregisteredError
from requests import HTTPError

import backend_new.Constants , backend_new.config
import time
import collections
from django.core.exceptions import ObjectDoesNotExist

from django.http import HttpResponse,HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from operator import itemgetter

from HealthPackage.models import HealthPackage, HealthPackageEnrollment
from Platform import utils
import backend_new.Constants
from Platform.fcm_message import send_to_token
from Platform.views import authenticateURL
from Doctor.models import Services
from Payment.models import HealthPackageInvoiceDetails,HealthPackageTransactionDetails, HealthCaseInvoiceDetails, HealthCaseTransactionDetails
from Person.models import PersonDetails
from Doctor.models import DoctorDetails,AppDetails
from backend_new.Constants import ServerConstants , HealthCaseConstant, PreferredLanguageConstant
from Communications.smstemplates import getpaymentrequesttemplate
from django.utils import timezone
from Platform import backendlogger
from datetime import datetime , timedelta
import logging
from HealthCase.models import HealthCase
import requests
import json
import Communications.utils
import pytz
from backend_new import settings
import  HealthPackage.utils

requestlogger = logging.getLogger('apirequests')
@csrf_exempt
def generateHealthPackageInvoice(request):
    logmessage = []
    data = json.loads(request.body)
    # requestlogger.info('================generateInvoice=====================')
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('generateHealthPackageInvoice', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    logmessage.append('generateHealthPackageInvoice;%s'%(str(data)))

    try:
        personlist_req = data[ServerConstants.PERSONLIST]
    except KeyError:
        backendlogger.error('generateHealthPackageInvoice', data, 'Please provide proper personlist', logmessage)
        return HttpResponse("Please provide proper personlist", status=400)

    try:
        appuid_req = data[ServerConstants.APPUID]
        appdetails_req = AppDetails.objects.get(appuid=appuid_req)
    except KeyError:
        backendlogger.error('generateHealthPackageInvoice', data, 'Please provide proper appuid', logmessage)
        return HttpResponse("Please provide proper appid", status=400)


    try:
        enrolluid_req = data[ServerConstants.ENROLLUID]
        healthpackageenrollment_req = HealthPackageEnrollment.objects.get(enrolluid=enrolluid_req)
    except KeyError:
        backendlogger.error('generateHealthPackageInvoice', data, 'Please provide proper enrolluid', logmessage)
        return HttpResponse("Please provide proper enrolluid", status=400)

    try:
        agentid_req = data[ServerConstants.AGENT_ID]
        agent_req = PersonDetails.objects.get(personuid=agentid_req)
        if not agent_req.djangouser.groups.filter(name='packagesalesagentgroup').exists():
            backendlogger.error('enrollUserForHealthProgram', data, 'Invalid sales agent', logmessage)
            return HttpResponse("Invalid sales agent", status=403)
    except KeyError:
        agent_req = None


    invoicedetailsarray = []
    for person in personlist_req:
        description = person[ServerConstants.USERNAME] + " -- " + healthpackageenrollment_req.healthpackage.packagename + " | "+ datetime.today().strftime('%d, %b %Y') + " to "  + (datetime.today()+timedelta(days=365)).strftime('%d, %b %Y')
        invoicedetailsarray.append({"description": description ,"amount":healthpackageenrollment_req.healthpackage.packagefee})

    healthPackageInvoice = HealthPackageInvoiceDetails.objects.create(appdetails=appdetails_req,invoicedetailsarray=json.dumps(invoicedetailsarray)
                                               ,packagesalesagent=agent_req,invoiceamount=healthpackageenrollment_req.healthpackage.packagefee,healthpackageenrollment=healthpackageenrollment_req)

    jsondata = dict({
                     ServerConstants.INVOICENUM:healthPackageInvoice.invoicenum,
                     })

    httpoutput = utils.successJson(jsondata)
    backendlogger.info('generateHealthPackageInvoice', data, httpoutput, logmessage)
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def generateHealthCaseInvoice(request):
    logmessage = []
    data = json.loads(request.body)
    # requestlogger.info('================generateInvoice=====================')
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('generateHealthCaseInvoice', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    logmessage.append('generateHealthCaseInvoice;%s'%(str(data)))

    try:
        healthcaseid_req = data[ServerConstants.HEALTHCASEID]
        healthcase_req = HealthCase.objects.get(healthcaseid=healthcaseid_req)
    except KeyError:
        backendlogger.error('generateHealthCaseInvoice', data, 'Please provide proper healthcaseid', logmessage)
        return HttpResponse("Please provide proper packageid", status=400)


    try:
        doctoruid_req = data[ServerConstants.DOCTORUID]
        doctor_req = DoctorDetails.objects.get(doctoruid=doctoruid_req)
    except KeyError:
        doctor_req = None

    try:
        invoiceamount_req = data[ServerConstants.INVOICEAMOUNT]
        invoicedetailsarray = data[ServerConstants.INVOICEDETAILSARRAY]
    except KeyError:
        backendlogger.error('generateHealthCaseInvoice', data, 'Please provide invoice amount and details', logmessage)
        return HttpResponse("Please provide amount and details", status=400)


    healthCaseInvoice = HealthCaseInvoiceDetails.objects.create(healthcase = healthcase_req, appdetails=healthcase_req.appdetails ,invoicedetailsarray=json.dumps(invoicedetailsarray)
                                               ,doctor=doctor_req,invoiceamount=invoiceamount_req)

    healthcase_req.lastupdated = timezone.now()
    healthcase_req.save()

    jsondata = dict({
                     ServerConstants.INVOICENUM:healthCaseInvoice.invoicenum,
                     })

    httpoutput = utils.successJson(jsondata)
    backendlogger.info('generateInvoice', data, httpoutput, logmessage)
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)

@csrf_exempt
def makeHealthPackageTransaction(request):

    # requestlogger.info('================makeTransaction=====================')

    data = json.loads(request.body)
    logmessage = []
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('makeHealthPackageTransaction', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    logmessage.append('makeHealthPackageTransaction;%s'%(str(data)))
    # requestlogger.info('makeHealthPackageTransaction;%s'%(str(data)))

    try:
        invoicenum_req = data[ServerConstants.INVOICENUM]
        healthpackageinvoicedetails_req = HealthPackageInvoiceDetails.objects.get(invoicenum = invoicenum_req)
    except :
        backendlogger.error('makeHealthPackageTransaction', data, 'Please provide valid invoice number', logmessage)
        return HttpResponse("Please provide valid invoice number", status=400)

    try:
        transactiontype_req = data[ServerConstants.TRANSACTIONTYPE]
        transactiondescription_req = data[ServerConstants.TRANSACTIONDESCRIPTION]
    except KeyError:
        backendlogger.error('makeHealthPackageTransaction', data, 'Please provide transaction details', logmessage)
        return HttpResponse("Please provide transaction details", status=400)


    transactiondetails = HealthPackageTransactionDetails.objects.create(healthpackageinvoicedetails = healthpackageinvoicedetails_req,
                                                                        transactionamount=healthpackageinvoicedetails_req.invoiceamount,
                                                                        transactiondescription = transactiondescription_req,
                                                                        transactiontype = transactiontype_req)

    outputjson = dict()
    if transactiontype_req == HealthPackageTransactionDetails.PAYU:
        transactionextras_req = data[ServerConstants.TRANSACTIONEXTRAS]
        customername = transactionextras_req[ServerConstants.USERNAME]
        customerMobileNumber = transactionextras_req[ServerConstants.PHONE]
        confirmSMSPhone = transactionextras_req[ServerConstants.AGENTPHONE][2:]
        datatosend = {
            "customerName": customername,
            "customerMobileNumber": customerMobileNumber,
            "amount": healthpackageinvoicedetails_req.invoiceamount,
            "confirmSMSPhone": confirmSMSPhone,
            "description": transactiondescription_req,
            "invoiceReferenceId": "hpkg"+str(transactiondetails.id)
        }
        payuauthtoken = transactiondetails.healthpackageinvoicedetails.appdetails.payuauthtoken
        if payuauthtoken != None:
            res = requests.post(url="https://www.payumoney.com/payment/payment/smsInvoice", data=datatosend,  headers={'authorization' : payuauthtoken })
            if res.status_code == 200:
                resdata = json.loads(res.content)
                backendlogger.info('makeHealthPackageTransaction:got payu response:', datatosend, resdata, logmessage)
                if resdata['errorCode'] is None:
                    outputjson[ServerConstants.URL] = resdata['result']['url']
                else:
                    backendlogger.error('makeHealthPackageTransaction', datatosend, res, logmessage)
                    return HttpResponse("PayU error", status=400)
            else:
                backendlogger.error('makeHealthPackageTransaction', datatosend, res, logmessage)
                return HttpResponse("PayU error", status=400)
        else:
            backendlogger.error('makeHealthPackageTransaction', data, "Payu auth token not found", logmessage)
            return HttpResponse("Payu auth token not found", status=400)

        # send sms to patient
        preferredlanguage = healthpackageinvoicedetails_req.healthpackageenrollment.person.preferredlanguage
        Communications.utils.sendSMS(getpaymentrequesttemplate(preferredlanguage, healthpackageinvoicedetails_req.appdetails.partner, healthpackageinvoicedetails_req.invoiceamount,
                                        outputjson[ServerConstants.URL]),
                                     healthpackageinvoicedetails_req.healthpackageenrollment.person.phone,
                                     healthpackageinvoicedetails_req.appdetails.appsmssenderid,
                                     preferredlanguage != PreferredLanguageConstant.ENGLISH)

    else:
        # enroll person
        transactiondetails.transactionstatus = 1
        transactiondetails.save()
        healthpackageinvoicedetails_req.paymentdone = True
        healthpackageinvoicedetails_req.save()
        timezone = pytz.timezone(settings.TIME_ZONE)
        healthpackageinvoicedetails_req.healthpackageenrollment.startdate = timezone.localize(datetime.today())
        healthpackageinvoicedetails_req.healthpackageenrollment.enddate = timezone.localize(datetime.today()) + timedelta(days=healthpackageinvoicedetails_req.healthpackageenrollment.healthpackage.packageduration)
        healthpackageinvoicedetails_req.healthpackageenrollment.enrollment_status = HealthPackageEnrollment.ENROLLED
        healthpackageinvoicedetails_req.healthpackageenrollment.save()

        HealthPackage.utils.takeactiononpackageenrollment(healthpackageinvoicedetails_req.healthpackageenrollment)

        message = ''.join([ 'Dear ',healthpackageinvoicedetails_req.healthpackageenrollment.person.name,', Welcome to '+ healthpackageinvoicedetails_req.appdetails.partner +' . You have been enrolled for ',healthpackageinvoicedetails_req.healthpackageenrollment.healthpackage.packagename,'. Please visit website for details'])
        Communications.utils.sendSMS(message,healthpackageinvoicedetails_req.healthpackageenrollment.person.phone,healthpackageinvoicedetails_req.appdetails.appsmssenderid, None)

    outputjson.update({ServerConstants.TRANSACTIONID:transactiondetails.id,
                     ServerConstants.TRANSACTIONAMOUNT:transactiondetails.transactionamount,
                     ServerConstants.TRANSACTIONSTATUS:transactiondetails.transactionstatus,
                     ServerConstants.TRANSACTIONTYPE:transactiondetails.transactiontype,
                     })

    httpoutput = utils.successJson(outputjson)
    backendlogger.info('makeHealthPackageTransaction', data, httpoutput, logmessage)
    # requestlogger.info('makeHealthPackageTransaction;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)

@csrf_exempt
def makeHealthCaseTransaction(request):

    data = json.loads(request.body)
    logmessage = []
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('makeHealthCaseTransaction', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    logmessage.append('makeHealthCaseTransaction;%s'%(str(data)))
    # requestlogger.info('makeTransaction;%s'%(str(data)))

    try:
        healthcaseinvoicenum_req = data[ServerConstants.INVOICENUM]
        healthcaseinvoicedetails_req = HealthCaseInvoiceDetails.objects.get(invoicenum= healthcaseinvoicenum_req)
    except KeyError:
        backendlogger.error('makeHealthCaseTransaction', data, 'Please provide healthcaseinvoicenum', logmessage)
        return HttpResponse("Please provide healthcaseinvoicenum", status=400)

    try:
        transactiontype_req = data[ServerConstants.TRANSACTIONTYPE]
    except KeyError:
        backendlogger.error('makeHealthCaseTransaction', data, 'Please provide transaction amount/type', logmessage)
        return HttpResponse("Please provide transaction amount/type", status=400)

    try:
        transactiondescription_req = data[ServerConstants.TRANSACTIONDESCRIPTION]
    except KeyError:
        transactiondescription_req = ""

    try:
         #userful when cash payment..straight send status 1
        transactionstatus_req = data[ServerConstants.TRANSACTIONSTATUS]
    except KeyError:
        transactionstatus_req = 0

    try:
        smsbyname = "Dr" + healthcaseinvoicedetails_req.doctor.name
    except:
        smsbyname = "Clinic " + healthcaseinvoicedetails_req.appdetails.applocation


    transactiondetails = HealthCaseTransactionDetails.objects.create(
                                                           transactionamount=healthcaseinvoicedetails_req.invoiceamount,
                                                           transactionstatus = transactionstatus_req,
                                                           transactiontype = transactiontype_req,
                                                           transactiondescription = transactiondescription_req,
                                                           healthcaseinvoicedetails = healthcaseinvoicedetails_req)


    outputjson = dict()
    if transactiontype_req == HealthCaseTransactionDetails.PAYUSMS:
        transactionextras_req = data[ServerConstants.TRANSACTIONEXTRAS]
        customername = transactionextras_req[ServerConstants.USERNAME]
        customerMobileNumber = transactionextras_req[ServerConstants.PHONE]
        confirmSMSPhone = transactionextras_req[ServerConstants.DOCTORPHONE][2:]
        datatosend = {
            "customerName": customername,
            "customerMobileNumber": customerMobileNumber,
            "amount": healthcaseinvoicedetails_req.invoiceamount,
            "confirmSMSPhone": confirmSMSPhone,
            "description": transactiondescription_req,
            "invoiceReferenceId": transactiondetails.transactionid
        }
        payuauthtoken = transactiondetails.healthcaseinvoicedetails.appdetails.payuauthtoken
        if payuauthtoken != None:
            res = requests.post(url="https://www.payumoney.com/payment/payment/smsInvoice", data=datatosend,  headers={'authorization' : payuauthtoken })
            if res.status_code == 200:
                resdata = json.loads(res.content)
                backendlogger.info('makeTransaction:got payu response:', datatosend, resdata, logmessage)
                if resdata['errorCode'] is None:
                    outputjson[ServerConstants.URL] = resdata['result']['url']
                else:
                    backendlogger.error('makeTransaction', datatosend, res, logmessage)
                    return HttpResponse("PayU error", status=400)
            else:
                backendlogger.error('makeTransaction', datatosend, res, logmessage)
                return HttpResponse("PayU error", status=400)
        else:
            backendlogger.error('makeTransaction', data, "Payu auth token not found", logmessage)
            return HttpResponse("Payu auth token not found", status=400)

    # send sms to patient
    preferredlanguage = healthcaseinvoicedetails_req.healthcase.person.preferredlanguage
    Communications.utils.sendSMS(getpaymentrequesttemplate(preferredlanguage, smsbyname, healthcaseinvoicedetails_req.invoiceamount,outputjson[ServerConstants.URL]),
                                 healthcaseinvoicedetails_req.healthcase.person.phone, healthcaseinvoicedetails_req.appdetails.appsmssenderid, preferredlanguage != PreferredLanguageConstant.ENGLISH )

    httpoutput = utils.successJson(outputjson)
    backendlogger.info('makeHealthCaseTransaction', data, httpoutput, logmessage)
    # requestlogger.info('makeTransaction;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def updateTransactionStatus(request):
    # requestlogger.info('================updateTransactionStatus=====================')
    #0 pending, 1 success, -1 failure
    data = json.loads(request.body)
    logmessage = []

    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('updateTransactionStatus', data, 'Error authenticaitng user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    logmessage.append('updateTransactionStatus;%s'%(str(data)))
    # requestlogger.info('updateTransactionStatus;%s'%(str(data)))

    try:
        transactionid_req = data[ServerConstants.TRANSACTIONID]
        transactionstatus_req = data[ServerConstants.TRANSACTIONSTATUS]
        transactiondescription_req = data[ServerConstants.TRANSACTIONDESCRIPTION]
    except KeyError:
        return HttpResponse("Please provide transactionid,transactionstatus", status=400)


    transactiondetails = HealthPackageTransactionDetails.objects.get(transactionid = transactionid_req)
    transactiondetails.transactionstatus = transactionstatus_req
    transactiondetails.transactiondescription = transactiondescription_req
    transactiondetails.save()

    jsondata = dict({ServerConstants.TRANSACTIONID:transactiondetails.transactionid,
                     ServerConstants.TRANSACTIONAMOUNT:transactiondetails.transactionamount,
                     ServerConstants.TRANSACTIONSTATUS:transactiondetails.transactionstatus,
                     ServerConstants.TRANSACTIONTYPE:transactiondetails.transactiontype,

                     })

    httpoutput = utils.successJson(jsondata)
    backendlogger.info('updateTransactionStatus', data, httpoutput, logmessage)
    # requestlogger.info('updateTransactionStatus;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)

@csrf_exempt
def payu_paymentstatus(request):
    # requestlogger.info('================updateTransactionStatus=====================')
    # 0 pending, 1 success, -1 failure
    data = json.loads(request.body)
    logmessage = []

    try:
        token = request.headers['authtoken']
        if token != "dgkgAdkgj34445==":
            backendlogger.error("payu_paymentstatus",data,"Not auth",logmessage)
            return HttpResponse("Not authenticated", status=401)
    except:
        backendlogger.error("payu_paymentstatus", data, "Not auth", logmessage)
        return HttpResponse("Not authenticated", status=401)

    requestlogger.info("Got payu webhook callback")
    logmessage.append("Got payu callback")

    merchantTransactionId = data["merchantTransactionId"]
    if merchantTransactionId.startswith('hpkg'):
        healthpackagetransactionid = int(merchantTransactionId[4:])
        logmessage.append("healthpackagetransactionid:" + str(healthpackagetransactionid))
        try:
            thistransaction = HealthPackageTransactionDetails.objects.get(id=healthpackagetransactionid)
            if data["status"] == 'Success':

                thistransaction.transactionstatus = 1
                thistransaction.save()
                thistransaction.healthpackageinvoicedetails.paymentdone = True
                thistransaction.healthpackageinvoicedetails.save()
                timezone = pytz.timezone(settings.TIME_ZONE)
                thistransaction.healthpackageinvoicedetails.healthpackageenrollment.startdate = timezone.localize(datetime.today())
                thistransaction.healthpackageinvoicedetails.healthpackageenrollment.enddate = timezone.localize(
                    datetime.today()) + timedelta(
                    days=thistransaction.healthpackageinvoicedetails.healthpackageenrollment.healthpackage.packageduration)
                thistransaction.healthpackageinvoicedetails.healthpackageenrollment.enrollment_status = HealthPackageEnrollment.ENROLLED
                thistransaction.healthpackageinvoicedetails.healthpackageenrollment.save()

                HealthPackage.utils.takeactiononpackageenrollment(thistransaction.healthpackageinvoicedetails.healthpackageenrollment)

                message = ''.join(['Dear ', thistransaction.healthpackageinvoicedetails.healthpackageenrollment.person.name,
                                   ', Welcome to ' + thistransaction.healthpackageinvoicedetails.appdetails.partner + ' . You have been enrolled for ',
                                   thistransaction.healthpackageinvoicedetails.healthpackageenrollment.healthpackage.packagename, '. Please visit website for details'])
                Communications.utils.sendSMS(message, thistransaction.healthpackageinvoicedetails.healthpackageenrollment.person.phone, thistransaction.healthpackageinvoicedetails.appdetails.appsmssenderid, None)
            else:
                thistransaction.transactionstatus = -1
                thistransaction.save()
        except Exception as e:
            backendlogger.error("payu_paymentstatus", data, str(e), logmessage)
    else:
        healthcasetransactionid = merchantTransactionId
        try:
            thistransaction = HealthCaseTransactionDetails.objects.get(transactionid = healthcasetransactionid)
            if data["status"] == 'Success':
                thistransaction.transactionstatus = 1
                thistransaction.healthcaseinvoicedetails.paymentdone = True
                thistransaction.save()
                thistransaction.healthcaseinvoicedetails.save()
                # update healthcase status
                if thistransaction.healthcaseinvoicedetails.status is not HealthCaseConstant.NEW:
                    thistransaction.healthcaseinvoicedetails.healthcase.status = HealthCaseConstant.ONGOING_PERSONRESPONDED
                    thistransaction.healthcaseinvoicedetails.healthcase.save()
                # send fcm to doctor
                try:
                    doctorfcmtoken = thistransaction.healthcaseinvoicedetails.healthcase.doctor.doctorfcmtokenmapping.fcmtoken
                    send_to_token(doctorfcmtoken,
                                  {"command": "patientpayment", "shorturldoctor": thistransaction.healthcaseinvoicedetails.healthcase.shorturldoctor,
                                   "patientname": thistransaction.healthcaseinvoicedetails.healthcase.person.name,
                                   "patientid": str(thistransaction.healthcaseinvoicedetails.healthcase.person.personid)})
                except (ObjectDoesNotExist, HTTPError, UnregisteredError):
                    Communications.utils.sendSMS("{} paid Rs {} for healthcase\n{}".format(
                        thistransaction.healthcaseinvoicedetails.healthcase.person.name,
                        thistransaction.healthcaseinvoicedetails.invoiceamount,
                        thistransaction.healthcaseinvoicedetails.healthcase.shorturldoctor),
                                                 thistransaction.healthcaseinvoicedetails.doctor.phone,
                                                 thistransaction.healthcaseinvoicedetails.appdetails.appsmssenderid, None)
            else:
                thistransaction.transactionstatus = -1
                thistransaction.save()
        except:
            backendlogger.error("payu_paymentstatus", data, "HealthCaseTransactionDetails not found ", logmessage)

    httpoutput = utils.successJson({})
    backendlogger.info('payu_paymentstatus', data, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)




