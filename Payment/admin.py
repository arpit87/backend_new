# Register your models here.
from django.contrib import admin
from Payment.models import HealthPackageInvoiceDetails, HealthPackageTransactionDetails, HealthCaseTransactionDetails, HealthCaseInvoiceDetails
from import_export import resources
from import_export.admin import ImportExportMixin

class HealthPackageInvoiceDetailsResource(resources.ModelResource):
    class Meta:
        model = HealthPackageInvoiceDetails

class TransactionDetailsResource(resources.ModelResource):
    class Meta:
        model = HealthPackageTransactionDetails

class HealthPackageInvoiceDetailsModelAdmin(ImportExportMixin,admin.ModelAdmin):
    list_display = ('invoicenum','packagesalesagent','invoiceamount','created_at','updated_at','paymentdone','healthpackageenrollment')
    resource_class = HealthPackageInvoiceDetailsResource

admin.site.register(HealthPackageInvoiceDetails,HealthPackageInvoiceDetailsModelAdmin)

class TransactionDetailsModelAdmin(ImportExportMixin,admin.ModelAdmin):
    list_display = ('id','healthpackageinvoicedetails','transactiontype','transactionamount','datetime_generated',"transactionstatus",'datetime_updated')
    resource_class = TransactionDetailsResource

admin.site.register(HealthPackageTransactionDetails, TransactionDetailsModelAdmin)

class HealthCaseInvoiceDetailsResource(resources.ModelResource):
    class Meta:
        model = HealthCaseInvoiceDetails

class HealthCaseInvoiceDetailsModelAdmin(ImportExportMixin,admin.ModelAdmin):
    list_display = ('invoicenum','doctor', 'appdetails' ,'invoiceamount','updated_at','paymentdone', 'created_at' ,'healthcase')
    resource_class = HealthCaseInvoiceDetailsResource

admin.site.register(HealthCaseInvoiceDetails,HealthCaseInvoiceDetailsModelAdmin)


class HealthCaseTransactionDetailsResource(resources.ModelResource):
    class Meta:
        model = HealthCaseTransactionDetails

class HealthCaseTransactionDetailsModelAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = (
    'transactionid', 'healthcaseinvoicedetails','transactiontype',  'transactiondescription','transactionamount', 'datetime_generated', "transactionstatus",
    'datetime_updated')
    resource_class = HealthCaseTransactionDetailsResource

admin.site.register(HealthCaseTransactionDetails, HealthCaseTransactionDetailsModelAdmin)