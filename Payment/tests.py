import datetime
from django.test import TestCase
from django.contrib.auth.models import Group


# Create your tests here.

#Test URLS
from Booking.models import BookingDetails, BookingTableMover,Services,CompletedBooking,VCDetails
from Platform.models import MOVERAPPDATA,USERAPPDATA
import json
import Platform.Constants
from User.models import PersonDetails
import backend_server.Constants
from Mover.models import MoverDetails, MoverLocationAndAvailability,AppDetails
from Platform.tasks import updateQBSessionToken
from django.contrib.auth.models import User
from Accounts.models import HomeCareInvoiceDetails,VCInvoiceDetails,KioskInvoiceDetails
import backend_server.config
import datetime
import requests


class AccountsAPITestCase(TestCase):
    multi_db = True
    def setUp(self):
        backend_server.config.ISTESTMODE = True

        djangouser = User.objects.create(username="arpit",password="abc")
        djangouser1 = User.objects.create(username="arpit",password="abc")
        movernew = MoverDetails.objects.create(djangouser=djangouser,moverid=1,name="arpit",phone="978637272",skillstr="1,2",docs="testdoc1,testdoc2,http://yolohealth.in/static/uploadedfiles/moverdocs/file1.png",servicesoffered = 1)
        usernew=PersonDetails.objects.create(personuid=1,name='arpit',phone="9736737272")

        appdetails = AppDetails.objects.create(kioskid=1, kiosklocation="IOC", kiosktag="IOC-1", kiosktype=1, kioskdescription="IOC", glucosestrips=10, hbstrips=10, lipidstrips=10)


        docservice = Services.objects.create(serviceid=1,servicetag="Consulatation",servicetitle="Doctor Visit",
                                servicecharges=1000,servicedescription="Doctor general visit")
        Group.objects.create(name="admingroup")
        Group.objects.create(name="usergroup")
        Group.objects.create(name="movergroup")
        Group.objects.create(name="doctorgroup")

    def test_generateInvoice(self):
        input = json.dumps({backend_server.Constants.ServerConstants.BOOKINGID:2})
        response = self.client.post('/Accounts/generateInvoice/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        

    def test_getInvoice(self):
        input = dict({backend_server.Constants.ServerConstants.RECEIPTID:2})
        response = self.client.get('/Accounts/getInvoice/',input)
        self.assertEqual(response.status_code,200)
        

    def test_getInvoiceFromBookingID(self):
        input = dict({backend_server.Constants.ServerConstants.BOOKINGID:2})
        response = self.client.get('/Accounts/getInvoice/',input)
        self.assertEqual(response.status_code,200)
        

    def test_sendInvoiceToUser(self):
        input = json.dumps({backend_server.Constants.ServerConstants.RECEIPTID:2,
                            backend_server.Constants.ServerConstants.PERSONUID:1
                            })
        response = self.client.post('/Accounts/sendInvoiceToUser/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        

    #tried by heet
    def test_getAllInvoiceList(self):
        response = self.client.get('/Accounts/getAllInvoiceList/')
        self.assertEqual(response.status_code,200)
        

    def test_generateVCInvoice(self):
        input = json.dumps({backend_server.Constants.ServerConstants.MOVER_ID:1,
                            backend_server.Constants.ServerConstants.PERSONUID:1,
                            backend_server.Constants.ServerConstants.VCID:2,
                            backend_server.Constants.ServerConstants.KIOSKID:1})
        response = self.client.post('/Accounts/generateVCInvoice/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        

    def test_generateReportInvoiceInvoice(self):
        input = json.dumps({backend_server.Constants.ServerConstants.PERSONUID:1,
                            backend_server.Constants.ServerConstants.KIOSKID:1,
                            backend_server.Constants.ServerConstants.CHECKUPSTR: "BP,Weight,hemoglobin,Bp,consultation", })
        response = self.client.post('/Accounts/generateReportInvoiceInvoice/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        

    def test_reportInvoiceInvoicePaymentdone(self):
        input = json.dumps({backend_server.Constants.ServerConstants.PERSONUID:1,
                            backend_server.Constants.ServerConstants.KIOSKID:1,
                            backend_server.Constants.ServerConstants.CHECKUPSTR: "BP,Weight,hemoglobin,Bp", })
        response = self.client.post('/Accounts/generateReportInvoiceInvoice/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        responsejson = json.loads(response.content)
        receiptid = responsejson["body"][backend_server.Constants.ServerConstants.RECEIPTID]
        #make cash payment
        input = json.dumps({backend_server.Constants.ServerConstants.PERSONUID:1,
                            backend_server.Constants.ServerConstants.TRANSACTIONID: "123132",
                            backend_server.Constants.ServerConstants.TRANSACTIONTYPE: "Cash",
                            backend_server.Constants.ServerConstants.TRANSACTIONAMOUNT:200,
                            backend_server.Constants.ServerConstants.TRANSACTIONSTATUS:1
                            })
        response = self.client.post('/Accounts/makeTransaction/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        #now make paid
        responsejson = json.loads(response.content)
        transactionid = responsejson["body"][backend_server.Constants.ServerConstants.TRANSACTIONID]
        input = json.dumps({backend_server.Constants.ServerConstants.RECEIPTID:receiptid,
                            backend_server.Constants.ServerConstants.TRANSACTIONID:transactionid})
        response = self.client.post('/Accounts/reportInvoicePaymentdone/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)

        

    def test_getAllVCInvoiceList(self):
        response = self.client.get('/Accounts/getAllVCInvoiceList/')
        self.assertEqual(response.status_code,200)
        

    def test_getAllVCInvoiceListWithMoverid(self):
        input = dict({
            backend_server.Constants.ServerConstants.MOVER_ID:1,
        })
        response = self.client.get('/Accounts/getAllVCInvoiceList/',input)
        self.assertEqual(response.status_code,200)
        

    def test_getAllVCInvoiceListWithKioskStr(self):
        input = dict({
            backend_server.Constants.ServerConstants.KIOSKSTR: "1",
        })
        response = self.client.get('/Accounts/getAllVCInvoiceList/',input)
        self.assertEqual(response.status_code,200)
        

    def test_getAllInvoiceInvoiceForUser(self):
        input = json.dumps({backend_server.Constants.ServerConstants.PERSONUID:1,
                            backend_server.Constants.ServerConstants.KIOSKID:1,
                            backend_server.Constants.ServerConstants.CHECKUPSTR: "BP,Weight,hemoglobin,Bp"})
        response = self.client.post('/Accounts/generateReportInvoiceInvoice/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        responsejson = json.loads(response.content)

        input1 = json.dumps({backend_server.Constants.ServerConstants.PERSONUID:1,
                             backend_server.Constants.ServerConstants.KIOSKID:1,
                             backend_server.Constants.ServerConstants.CHECKUPSTR: "BP,Weight,hemoglobin"})
        response = self.client.post('/Accounts/generateReportInvoiceInvoice/',input1,Platform.Constants.RESPONSE_JSON_TYPE)
        responsejson = json.loads(response.content)

        input2 = dict({backend_server.Constants.ServerConstants.PERSONUID:1
                       })
        response = self.client.get('/Accounts/getAllInvoiceInvoiceForUser/',input2)
        self.assertEqual(response.status_code,200)
        

    def test_getAllInvoiceInvoiceForAdmin(self):
        input2 = dict({
            backend_server.Constants.ServerConstants.KIOSKID:1,
            backend_server.Constants.ServerConstants.STARTDATE:datetime.datetime(1, 2, 1),
            backend_server.Constants.ServerConstants.ENDDATE:datetime.datetime.now(),
            backend_server.Constants.ServerConstants.PAYMENTDONE: False
        })
        response = self.client.get('/Accounts/getAllInvoiceInvoiceForAdmin/',input2)
        self.assertEqual(response.status_code,200)
        

    def test_getInvoiceInvoiceById(self):
        input = json.dumps({backend_server.Constants.ServerConstants.PERSONUID:1,
                            backend_server.Constants.ServerConstants.KIOSKID:1,
                            backend_server.Constants.ServerConstants.CHECKUPSTR: "BP,Weight,hemoglobin", })
        response = self.client.post('/Accounts/generateReportInvoiceInvoice/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        responsejson = json.loads(response.content)
        
        input = dict({backend_server.Constants.ServerConstants.RECEIPTID:responsejson["body"][backend_server.Constants.ServerConstants.RECEIPTID]
                      })
        response = self.client.get('/Accounts/getInvoiceInvoiceById/',input)
        self.assertEqual(response.status_code,200)
        

    def test_makeTransaction(self):
        input = json.dumps({backend_server.Constants.ServerConstants.PERSONUID:1,
                            backend_server.Constants.ServerConstants.TRANSACTIONID: "123132",
                            backend_server.Constants.ServerConstants.TRANSACTIONTYPE: "Debit",
                            backend_server.Constants.ServerConstants.TRANSACTIONAMOUNT:200})
        response = self.client.post('/Accounts/makeTransaction/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        

    def test_updateTransactionStatus(self):
        input = json.dumps({backend_server.Constants.ServerConstants.PERSONUID:1,
                            backend_server.Constants.ServerConstants.TRANSACTIONID: "123132",
                            backend_server.Constants.ServerConstants.TRANSACTIONTYPE: "Debit",
                            backend_server.Constants.ServerConstants.TRANSACTIONAMOUNT:200})
        response = self.client.post('/Accounts/makeTransaction/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)

        responsejson = json.loads(response.content)
        input = json.dumps({backend_server.Constants.ServerConstants.TRANSACTIONID:responsejson["body"][backend_server.Constants.ServerConstants.TRANSACTIONID],
                            backend_server.Constants.ServerConstants.TRANSACTIONSTATUS:1
                            })
        response = self.client.post('/Accounts/updateTransactionStatus/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        

    # def test_getAllVCInvoiceListForDateTime(self):
    #     input = dict({
    #         backend_server.Constants.ServerConstants.KIOSKID:1,
    #         backend_server.Constants.ServerConstants.STARTDATE:datetime.datetime(1,2,1),
    #         backend_server.Constants.ServerConstants.ENDDATE:datetime.datetime.now(),
    #     })
    #     response = self.client.get('/Accounts/getAllVCInvoiceListForDateTime/',input)
    #     self.assertEqual(response.status_code,200)
    #     