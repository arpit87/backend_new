from django.conf.urls import url

from Payment import views

urlpatterns = [
                       url(r'^generateHealthPackageInvoice/', views.generateHealthPackageInvoice, name='generateHealthPackageInvoice'),
                       url(r'^generateHealthCaseInvoice/', views.generateHealthCaseInvoice, name='generateHealthCaseInvoice'),
                       url(r'^makeHealthPackageTransaction/', views.makeHealthPackageTransaction, name='makeHealthPackageTransaction'),
                       url(r'^makeHealthCaseTransaction/',views.makeHealthCaseTransaction,name='makeHealthCaseTransaction'),
                       url(r'^updateTransactionStatus/',views.updateTransactionStatus,name='updateTransactionStatus'),

                       #payucallback
                        url(r'^payu/paymentstatus/', views.payu_paymentstatus, name='payu_paymentstatus'),
]