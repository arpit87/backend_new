from django.db import models
from Doctor.models import DoctorDetails,AppDetails
from Person.models import PersonDetails
from datetime import datetime
from HealthCase.models import HealthCase
from HealthPackage.models import HealthPackageEnrollment
# Create your models here.


class HealthPackageInvoiceDetails(models.Model):
    invoicenum = models.AutoField(primary_key=True)
    appdetails = models.ForeignKey(AppDetails, null=True, on_delete=models.CASCADE)
    invoicedetailsarray = models.CharField(max_length=2056,null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    invoiceamount = models.IntegerField(default=0,null= False)
    paymentdone = models.BooleanField(default=False,null=False,blank=False)
    packagesalesagent = models.ForeignKey(PersonDetails, null=True, related_name="packagesalesagent", on_delete=models.CASCADE)
    healthpackageenrollment = models.ForeignKey(HealthPackageEnrollment, null= True, related_name='healthpackageinvoice', on_delete=models.SET_NULL)

    def __str__(self):
        return str(self.invoicenum)


class HealthPackageTransactionDetails(models.Model):
    CASH = 0
    CREDITCARD = 1
    DEBITCARD = 2
    UPI = 3
    PAYU = 4
    TRANSACTIONTYPE = ((CASH, 'CASH'), (CREDITCARD, 'Credit Card'), (DEBITCARD, 'Debit Card'),
                (UPI, 'UPI'),(PAYU, 'PAYU'))

    PENDING = 0
    SUCCESS = 1
    FAILURE = -1
    TRANSACTIONTSTATUS= ((PENDING, 'Pending'), (SUCCESS, 'Success'), (FAILURE, 'Failure'))

    transactiondescription = models.CharField(max_length=50,null=False)
    datetime_generated = models.DateTimeField()
    transactionamount = models.IntegerField(default=0,null= False)
    transactiontype = models.IntegerField(choices=TRANSACTIONTYPE,null=False)
    transactionstatus = models.IntegerField(choices=TRANSACTIONTSTATUS,default=PENDING)
    datetime_updated = models.DateTimeField()
    healthpackageinvoicedetails = models.ForeignKey(HealthPackageInvoiceDetails, null = True, on_delete=models.CASCADE)

    def __str__(self):
        return str('{}-{}'.format(self.id,self.transactionstatus))

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.datetime_generated = datetime.now()
        self.datetime_updated = datetime.now()
        return super(HealthPackageTransactionDetails, self).save(*args, **kwargs)




class HealthCaseInvoiceDetails(models.Model):
    invoicenum = models.AutoField(primary_key=True)
    appdetails = models.ForeignKey(AppDetails, null=True, on_delete=models.CASCADE)
    invoicedetailsarray = models.CharField(max_length=2056,null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    invoiceamount = models.IntegerField(default=0,null= False)
    paymentdone = models.BooleanField(default=False)
    doctor = models.ForeignKey(DoctorDetails, null=True, related_name="healthcaseinvoice", on_delete=models.CASCADE)
    healthcase = models.ForeignKey(HealthCase, null=False, on_delete=models.CASCADE)
    healthpackageenrollment = models.ForeignKey(HealthPackageEnrollment, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return str(self.invoicenum)

class HealthCaseTransactionDetails(models.Model):
    CASH = 0
    CREDITCARD = 1
    DEBITCARD = 2
    UPI = 3
    PAYUSMS = 4
    COVEREDINPACKAGE = 5
    TRANSACTIONTYPE = ((CASH, 'CASH'), (CREDITCARD, 'Credit Card'), (DEBITCARD, 'Debit Card'),
                (UPI, 'UPI'),(PAYUSMS, 'PAYUSMS'),(COVEREDINPACKAGE, 'COVEREDINPACKAGE'))

    PENDING = 0
    SUCCESS = 1
    FAILURE = -1
    TRANSACTIONTSTATUS = ((PENDING, 'Pending'), (SUCCESS, 'Success'), (FAILURE, 'Failure'))

    transactionid = models.AutoField(primary_key=True)
    transactiondescription = models.CharField(max_length=50, null=True, blank=True)
    datetime_generated = models.DateTimeField(auto_now_add=True)
    transactionamount = models.IntegerField(default=0,null= False)
    transactiontype = models.IntegerField(choices=TRANSACTIONTYPE,null=False)
    transactionstatus = models.IntegerField(choices=TRANSACTIONTSTATUS,null= False)
    datetime_updated = models.DateTimeField(auto_now=True)
    healthcaseinvoicedetails = models.ForeignKey(HealthCaseInvoiceDetails, null = False, on_delete=models.CASCADE)

    def __str__(self):
        return str('{}-{}'.format(self.transactionid,self.get_transactionstatus_display()))




