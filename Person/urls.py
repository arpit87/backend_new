from django.conf.urls import url

from Person import views

urlpatterns = [
                       url(r'^createPerson/',views.createPerson,name='createPerson'),
                       url(r'^getPersonDetails/',views.getPersonDetails,name='getPersonDetails'),
                       url(r'^updatePerson/',views.updatePerson,name='updatePerson'),
                       url(r'^verifyFingerPrint/',views.verifyFingerPrint,name='verifyFingerPrint'),
                       url(r'^getAllPersonList/',views.getAllPersonList,name='getAllPersonList'),
                       url(r'^mapPersontypeToAppid/',views.mapPersontypeToAppid,name='mapPersontypeToAppid'),
                       url(r'^getAllAppMappingForPersonType/', views.getAllAppMappingForPersonType, name='getAllAppMappingForPersonType'),
                       url(r'^assignRemoveGroup/',views.assignRemoveGroup,name='assignRemoveGroup'),
                       url(r'^setdoctorpersonmapping/',views.setdoctorpersonmapping,name='setdoctorpersonmapping'),
                       url(r'^getdoctorpersonmapping/',views.getdoctorpersonmapping,name='getdoctorpersonmapping'),
                       url(r'^getpersondoctormapping/',views.getpersondoctormapping,name='getpersondoctormapping'),
                       url(r'^getDetalisFromPersonUID/',views.getDetalisFromPersonUID,name='getDetalisFromPersonUID'),
                       ]
