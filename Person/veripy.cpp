//build command :
//g++ -shared -DLINUX3 -I./ -I./include -I /usr/include/python3.6/  -D_FDU05 -o veripy.so veripy.cpp  -lsgfplib -lsgnfiq -lsgfpamx -lsgfdu05 -lusb -lpthread  -fPIC


#include <Python.h>
#include "include/sgfplib.h"
#include <string.h>
#include <iostream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <fstream>
#include<unistd.h>
using namespace std;


static const std::string base64_chars =
             "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
             "abcdefghijklmnopqrstuvwxyz"
             "0123456789+/";
//LPSGFPM  sgfplib = NULL;

static inline bool is_base64(BYTE c) {
  return (isalnum(c) || (c == '+') || (c == '/'));
}

std::vector<BYTE> base64_decode(std::string const& encoded_string) {
  int in_len = encoded_string.size();
  int i = 0;
  int j = 0;
  int in_ = 0;
  BYTE char_array_4[4], char_array_3[3];
  std::vector<BYTE> ret;

  while (in_len-- && ( encoded_string[in_] != '=') && is_base64(encoded_string[in_])) {
    char_array_4[i++] = encoded_string[in_]; in_++;
    if (i ==4) {
      for (i = 0; i <4; i++)
        char_array_4[i] = base64_chars.find(char_array_4[i]);

      char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
      char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
      char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

      for (i = 0; (i < 3); i++)
          ret.push_back(char_array_3[i]);
      i = 0;
    }
  }

  if (i) {
    for (j = i; j <4; j++)
      char_array_4[j] = 0;

    for (j = 0; j <4; j++)
      char_array_4[j] = base64_chars.find(char_array_4[j]);

    char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
    char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
    char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

    for (j = 0; (j < i - 1); j++) ret.push_back(char_array_3[j]);
  }

  return ret;
}

static PyObject* py_veripy_v2(PyObject* self, PyObject* args){

	const char *text_stream1;
	const char *text_stream2;
	if(!PyArg_ParseTuple(args, "ss", &text_stream1, &text_stream2)){
		std::cout << "Unable to fetch arguments.\n";
		return Py_BuildValue("i", -1);
	}

    string datatodecode1 = text_stream1;
    string datatodecode2 = text_stream2;

    std::vector<BYTE> testbuffer1;
    std::vector<BYTE> testbuffer2;

    HSGFPM  m_hFPM; // handle for SGFPM
    DWORD err = SGFPM_Create(&m_hFPM);

    DWORD devname = SG_DEV_FDU05;
    err = SGFPM_Init(m_hFPM, devname);

    //Extra Added By Arnav
//   err = sgfplib->Init(SG_DEV_AUTO);

    BYTE *isoMinutiaeBuffer1;
    BYTE *isoMinutiaeBuffer2;
    BOOL matched=false;
    DWORD score;

	testbuffer1 = base64_decode(datatodecode1);
	int buffersize1 = testbuffer1.size();
	isoMinutiaeBuffer1 = (BYTE*) malloc(buffersize1);
	for(int i=0;i<testbuffer1.size();i++){
		isoMinutiaeBuffer1[i]=testbuffer1[i];
	}

    testbuffer2 = base64_decode(datatodecode2);
	int buffersize2 = testbuffer2.size();
	isoMinutiaeBuffer2 = (BYTE*) malloc(buffersize2);
	for(int i=0;i<testbuffer2.size();i++){
		isoMinutiaeBuffer2[i]=testbuffer2[i];
	}

    err = SGFPM_SetTemplateFormat(m_hFPM, TEMPLATE_FORMAT_ISO19794);
    err  = SGFPM_MatchTemplate(m_hFPM, isoMinutiaeBuffer1,isoMinutiaeBuffer2, SL_NORMAL, &matched);

	if (matched == true)
	  return Py_BuildValue("i", 1); // Matched successfully
	else
	  return Py_BuildValue("i", 0);
  }

// Bind Python function names to our C functions
static PyMethodDef veripy_methods[] = {
	//{"myFunction", py_myFunction, METH_VARARGS},
	{"veripy_v2", py_veripy_v2, METH_VARARGS},
	{NULL, NULL}
};

// Python calls this to let us initialize our module
//extern "C" void initveripy(){
//	(void) Py_InitModule("veripy", veripy_methods);
//}

// Extra Added. For Python 3.
static struct PyModuleDef veripymodule = {
    PyModuleDef_HEAD_INIT,
    "veripy", /* name of module */
    NULL,          /* module documentation, may be NULL */
    -1,          /* size of per-interpreter state of the module, or -1 if the module keeps state in global variables. */
    veripy_methods
};

PyMODINIT_FUNC PyInit_veripy(void){
    return PyModule_Create(&veripymodule);
}

