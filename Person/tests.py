from datetime import datetime
from django.test import TestCase
from django.contrib.auth.models import Group, User


import json
from backend_new.Constants import ServerConstants
from Person.models import PersonDetails,AnalystAppMapping,PersonExternalIDMapping
from backend_new import config
from Doctor.models import DoctorDetails, AppDetails,DoctorAppMapping
from django.contrib.auth.models import User
import pylibmc
import requests


class PersonAPITestCase(TestCase):
    multi_db = True
    def setUp(self):
        config.ISTESTMODE = True
        djangouser = User.objects.create_user(username="Doctor978637272",password="abc")
        djangouser1 = User.objects.create_user(username="Doctor978637273",password="abc")
        djangouser2 = User.objects.create_user(username="user9736737272",password="abc")
        djangouser3 = User.objects.create_user(username="user9736737273",password="abc")
        # QBSESSIONDATA.objects.create(application_id=32324,token="asf",expiry=12456)
        Doctornew = DoctorDetails.objects.create(djangouser=djangouser,doctoruid=1,name="arpit",phone="978637272",skillstr="1,2",docs="testdoc1,testdoc2,http://arpit.in/static/uploadedfiles/Doctordocs/file1.png")
        Doctornew1 = DoctorDetails.objects.create(djangouser=djangouser1,doctoruid=11,name="arpit",phone="978637273",skillstr="1")

        personnew=PersonDetails.objects.create(djangouser=djangouser2, personuid=100, name='arpit', appuid=100, phone="9736737272", dob_year=1987, gender="m", fingerprint="AQAAABQAAAAUAQAAAQASAAMAZAAAAAAADAEAAGXQ/aK27qM4AftEy9m42XVIbIT0a5NCZhwlZu69amzA3MDIhpCIpRBHc046OoLFGd4jejcY0033PS4oijaW*/P0T3NvKnvk0lU0Pu2b/lD/xDwc3wZH6iYFz16Xw7ja63ipWmivehdeVb9leggSPYI9QJBKyaezkc5SCYkQDCLs2hdIe6bUYAE4JBQQ9JL4s2jFd4HnYKQF/IlquoQsumh*xjamdX5*StBeUFRU8/p1E8DvqSLKHsSFk0TTZIowWxeIuKCVvaW6juxiPem/cfk8kJqvKliFFuxMKWfN9tSXhvrDDqc0UrGHBIGpIG9tBSVuizjRV4uqrDIYBfsdarNdCn2sgrPKhK9yTIruH5n/")
        personnew3=PersonDetails.objects.create(djangouser=djangouser3, personuid=12, name='arpit', phone="9736737273", dob_year=1987, appuid=80, hierarchy='master', fingerprint="Rk1SACAyMAAAAAFiAAABLAGQAMUAxQEAABBKNoB6ADn4AEC0AEr0AECkAFRxAEBnAGcAAECxAHNyAEB3AHT+AEDVAHTxAEAqAICWAED1AIpxAEDSAIruAICfAJXyAEBbAJWHAEA/AJsHAEBvAJyKAEBaAKEHAEClAKfqAIAyAKuXAEBZAK6NAECMALL0AEBvALSQAICNAMTuAEA1AMynAECcANfZAICYAOBkAEBBAOKzAIBIAOi5AIDUAOvUAIApAO6xAIB/APWsAEDSAPvKAECEAPuuAEBeAP86AEBnAQYpAEBcAQkoAIB6AQsrAIBQAQ81AICFARGkAEBQARYQAEDDAR64AEDOAS")
        personnew1=PersonDetails.objects.create(djangouser=djangouser3, personuid=13, name='arpit2', phone="9736737273", dob_year=1987, appuid=80, hierarchy='slave', fingerprint="AQAAABQAAAAUAQAAAQASAAMAZAAAAAAADAEAAGXQ/aK27qM4AftEy9m42XVIbIT0a5NCZhwlZu69amzA3MDIhpCIpRBHc046OoLFGd4jejcY0033PS4oijaW*/P0T3NvKnvk0lU0Pu2b/lD/xDwc3wZH6iYFz16Xw7ja63ipWmivehdeVb9leggSPYI9QJBKyaezkc5SCYkQDCLs2hdIe6bUYAE4JBQQ9JL4s2jFd4HnYKQF/IlquoQsumh*xjamdX5*StBeUFRU8/p1E8DvqSLKHsSFk0TTZIowWxeIuKCVvaW6juxiPem/cfk8kJqvKliFFuxMKWfN9tSXhvrDDqc0UrGHBIGpIG9tBSVuizjRV4uqrDIYBfsdarNdCn2sgrPKhK9yTIruH5n/")
        personnew1=PersonDetails.objects.create(djangouser=djangouser3, personuid=14, name='arpit', phone="9736737273", dob_year=1987, appuid=80, hierarchy='master', fingerprint="Rk1SACAyMAAAAAFiAAABLAGQAMUAxQEAABBKNoB6ADn4AEC0AEr0AECkAFRxAEBnAGcAAECxAHNyAEB3AHT+AEDVAHTxAEAqAICWAED1AIpxAEDSAIruAICfAJXyAEBbAJWHAEA/AJsHAEBvAJyKAEBaAKEHAEClAKfqAIAyAKuXAEBZAK6NAECMALL0AEBvALSQAICNAMTuAEA1AMynAECcANfZAICYAOBkAEBBAOKzAIBIAOi5AIDUAOvUAIApAO6xAIB/APWsAEDSAPvKAECEAPuuAEBeAP86AEBnAQYpAEBcAQkoAIB6AQsrAIBQAQ81AICFARGkAEBQARYQAEDDAR64AEDOAS")

        kiosknew = AppDetails.objects.create(appuid=80, applocation="IOC", apptag="IOC-80", apptype=1, appdescription="IOC")

        AnalystAppMapping.objects.create(appdetails= kiosknew, person= personnew)
        DoctorAppMapping.objects.create(appdetails =kiosknew, doctor= Doctornew)

        Group.objects.create(name="persongroup")
        Group.objects.create(name="doctorgroup")
        admingroup = Group.objects.create(name="admingroup")
        admingroup.user_set.add(djangouser2)
        analystgroup = Group.objects.create(name="analystgroup")
        analystgroup.user_set.add(djangouser3)

        PersonExternalIDMapping.objects.create(person=personnew,externalsystem='aadhar',externalid="386612151787")

    def test_createPerson(self):
        cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
        otp = 123321 #this is universal otp
        cache.set("9876534123"+"_otp",otp,10)
        input = json.dumps({ServerConstants.USERNAME: 'Arpit',
                            ServerConstants.PHONE: '919876534123',
                            ServerConstants.OTP: config.BYPASSOTP})
        response = self.client.post('/Person/createPerson/',input,ServerConstants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)

    def test_createPersonWithHierarchy(self):
        cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
        otp = 123321 #this is universal otp
        cache.set("9876543210"+"_otp",otp,10)
        input = json.dumps({ServerConstants.USERNAME: 'Nilesh',
                            ServerConstants.USERAPPUUID: '4321',
                            ServerConstants.PHONE: '9876543210',
                            ServerConstants.OTP: config.BYPASSOTP,
                            ServerConstants.HIERARCHY: "father"})
        response = self.client.post('/Person/createPerson/',input,ServerConstants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)

    def test_createPersonForKiosk(self):
        cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
        otp = 123321 #this is universal otp
        cache.set("9876534123"+"_otp",otp,10)
        input = json.dumps({ServerConstants.USERNAME: 'Arpit',
                            ServerConstants.FINGERPRINTDATA: '1234',
                            ServerConstants.PHONE: '9876534123',
                            ServerConstants.GENDER: 'M',
                            ServerConstants.AGE:28,
                            ServerConstants.APPUID:80,
                            ServerConstants.EMAIL: 'arpit@arpit.in',
                            ServerConstants.OTP: config.BYPASSOTP
                            })
        response = self.client.post('/Person/createPerson/',input,ServerConstants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        person = PersonDetails.objects.get(phone ="9876534123")
        self.assertEqual(person.fingerprint,"1234")
        self.assertEqual(person.appuid,80)
        self.assertEqual(response.status_code,200)

    def test_createPersonForKioskWithDOB(self):
        cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
        otp = 123321 #this is universal otp
        cache.set("9876534123"+"_otp",otp,10)
        input = json.dumps({ServerConstants.USERNAME: 'Arpit',
                            ServerConstants.FINGERPRINTDATA: '1234',
                            ServerConstants.PHONE: '9876534123',
                            ServerConstants.GENDER: 'M',
                            ServerConstants.DOB: "1987-05-26",
                            ServerConstants.APPUID:80,
                            ServerConstants.EMAIL: 'arpit@arpit.in',
                            ServerConstants.OTP: config.BYPASSOTP
                            })
        response = self.client.post('/Person/createPerson/',input,ServerConstants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        person = PersonDetails.objects.get(phone ="9876534123")
        self.assertEqual(person.fingerprint,"1234")
        self.assertEqual(person.appuid,80)

        self.assertEqual(response.status_code,200)

    def test_createPersonForExternalKiosk(self):
        cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
        otp = 123321 #this is universal otp
        cache.set("9876534123"+"_otp",otp,10)
        input = json.dumps({ServerConstants.USERNAME: 'Arpit',
                            ServerConstants.FINGERPRINTDATA: '1234',
                            ServerConstants.PHONE: '9876534123',
                            ServerConstants.GENDER: 'M',
                            ServerConstants.AGE:28,
                            ServerConstants.APPUID:80,
                            ServerConstants.EMAIL: 'arpit@arpit.in',
                            ServerConstants.OTP: config.BYPASSOTP,
                            ServerConstants.EXTERNALID: 'PAT00001',
                            ServerConstants.EXTERNALSYSTEM: 'glocal'
                            })
        response = self.client.post('/Person/createPerson/',input,ServerConstants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        person = PersonDetails.objects.get(phone ="9876534123")
        self.assertEqual(person.fingerprint,"1234")
        self.assertEqual(person.appuid,80)
        externalmapping = PersonExternalIDMapping.objects.get(person=person,externalsystem="glocal")
        self.assertEqual(externalmapping.externalid,"PAT00001")
        self.assertEqual(response.status_code,200)


    def test_getPersonDetails(self):
        input = dict({ServerConstants.PERSONUID:100})
        response = self.client.get('/Person/getPersonDetails/',input)

        self.assertEqual(response.status_code,200)

    def test_updatePersonProfile(self):
        input = json.dumps({ServerConstants.PERSONUID:100,
                            ServerConstants.FINGERPRINTDATA: "asdfasdfasfsafasf",
                            ServerConstants.AGE:30,
                            ServerConstants.EMAIL: "arpit@gmail.com"})
        response = self.client.post('/Person/updatePersonProfile/',input,ServerConstants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        thisuserdata = PersonDetails.objects.get(personuid=100)
        self.assertEqual(thisuserdata.email,"arpit@gmail.com")


    def test_setDoctorAnalystAndAdminToAppMapping(self):
        input = json.dumps({ServerConstants.APPUID: '80',
                            ServerConstants.PHONE: '9736737273',
                            ServerConstants.PERSONTYPE: 'analyst',
                            ServerConstants.GROUPACTION: 'add',  #
                            })
        response = self.client.post('/Person/setDoctorAnalystAndAdminToAppMapping/',input,ServerConstants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        kioskdeatils = AnalystAppMapping.objects.get(person__personuid = 100)
        self.assertEqual(kioskdeatils.appdetails.apptag,"IOC-80")


    def test_getAllAppMapping(self):
        input = dict({ServerConstants.PERSONTYPE: 'analyst'})
        response = self.client.get('/Person/getAllAppMapping/',input)

        self.assertEqual(response.status_code,200)

    def test_addAndRemoveGroupToPerson(self):
        input = json.dumps({ServerConstants.GROUPNAME: 'analystgroup',
                            ServerConstants.PERSONUID:100,
                            ServerConstants.GROUPACTION: 'add',
                            })
        response = self.client.post('/Person/addAndRemoveGroupToPerson/',input,ServerConstants.RESPONSE_JSON_TYPE)

        self.assertEqual(response.status_code,200)
        person = PersonDetails.objects.get(personuid=100)
        djangouser = person.djangouser
        djangouser_inanalystgrp = djangouser.groups.filter(name='analystgroup').exists()
        self.assertEqual(djangouser_inanalystgrp,True)



    def test_addAndRemoveGroupToPersonById(self):
        input = json.dumps({ServerConstants.GROUPNAME: 'analystgroup',
                            ServerConstants.PERSONUID:100,
                            ServerConstants.GROUPACTION: 'add',
                            })
        response = self.client.post('/Person/addAndRemoveGroupToPerson/',input,ServerConstants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        person = PersonDetails.objects.get(personuid=100)
        djangouser = person.djangouser
        djangouser_inanalystgrp = djangouser.groups.filter(name='analystgroup').exists();
        self.assertEqual(djangouser_inanalystgrp,True)


    def test_getSearchPersonList(self):
        input =dict({ServerConstants.PAGEOFFSET: '0',
                     ServerConstants.PAGELIMIT: '10',
                     ServerConstants.SEARCHPARAMETER: 'Name',
                     ServerConstants.SEARCHPARAMETERVALUE: 'arpit'
                     })
        response = self.client.get('/Person/getAllPersonList/',input)
        self.assertEqual(response.status_code,200)


    def test_getAllPersonList(self):
        input =dict({ServerConstants.PAGEOFFSET: '0',
                     ServerConstants.PAGELIMIT: '10',
                     })
        response = self.client.get('/Person/getAllPersonList/',input)
        self.assertEqual(response.status_code,200)


    def test_getAllPersonListGroupName(self):
        input =dict({ServerConstants.PAGEOFFSET: '0',
                     ServerConstants.PAGELIMIT: '10',
                     ServerConstants.GROUPNAME: 'analystgroup',
                     })
        response = self.client.get('/Person/getAllPersonList/',input)
        self.assertEqual(response.status_code,200)


    def test_getAllPersonListKioskId(self):
        input =dict({ServerConstants.PAGEOFFSET: '0',
                     ServerConstants.PAGELIMIT: '10',
                     ServerConstants.APPUID:80
                     })
        response = self.client.get('/Person/getAllPersonList/',input)
        self.assertEqual(response.status_code,200)


    def test_getAllPersonListApp(self):
        input =dict({ServerConstants.PAGEOFFSET: '0',
                     ServerConstants.PAGELIMIT: '10',
                     ServerConstants.SEARCHPARAMETER: 'Phone',
                     ServerConstants.SEARCHPARAMETERVALUE: '9736737273'
                     })
        response = self.client.get('/Person/getAllPersonList/',input)
        print(response.content)
        self.assertEqual(response.status_code,200)


    def test_setdoctorpersonmapping(self):
        input = json.dumps({
            ServerConstants.PERSONUID:100,
            ServerConstants.DOCTORUID:1,
            ServerConstants.AUTH_LEVEL: "all"
        })

        response = self.client.post('/Person/setdoctorpersonmapping/', input, ServerConstants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code, 200)



    def test_getdoctorpersonmapping(self):

        input = json.dumps({
            ServerConstants.PERSONUID:100,
            ServerConstants.DOCTORUID:1,
            ServerConstants.AUTH_LEVEL: "all"
        })

        response = self.client.post('/Person/setdoctorpersonmapping/', input, ServerConstants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code, 200)

        input = dict({ServerConstants.DOCTORUID: 1})
        response = self.client.get('/Person/getdoctorpersonmapping/',input)
        self.assertEqual(response.status_code,200)





