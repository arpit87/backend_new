# Register your models here.
from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin,ImportExportMixin

from Person.models import PersonDetails,AnalystAppMapping, DoctorPersonMapping, \
    AdminAppMapping,QBDetails , PersonExternalIDMapping, HealthWorkerLocationMapping, HIMSAppMapping

# Creating import-export resource
class PersonResource(resources.ModelResource):

    class Meta:
        model = PersonDetails


class PersonDetailsModelAdmin(ImportExportMixin,admin.ModelAdmin):
    list_display = ('personid','personuid','name','phone','email','dob_year','gender','hierarchy','datetime_joined')
    search_fields = ('personid','personuid','name','phone','email',)
    resource_class = PersonResource

admin.site.register(PersonDetails, PersonDetailsModelAdmin)


class AnalystAppMappingModelAdmin(admin.ModelAdmin):
    list_display = ('person','appdetails')

admin.site.register(AnalystAppMapping, AnalystAppMappingModelAdmin)

class HealthWorkerLocationMappingModelAdmin(admin.ModelAdmin):
    list_display = ('person','location')

admin.site.register(HealthWorkerLocationMapping, HealthWorkerLocationMappingModelAdmin)


class AdminAppMappingModelAdmin(admin.ModelAdmin):
    list_display = ('person','appdetails')

admin.site.register(AdminAppMapping, AdminAppMappingModelAdmin)


class DoctorPersonMappingModelAdmin(admin.ModelAdmin):
    list_display = ('doctor', 'person', 'authorization_level')

admin.site.register(DoctorPersonMapping, DoctorPersonMappingModelAdmin)

class QBDetailsModelAdmin(admin.ModelAdmin):
    list_display = ('phone', 'qbid', 'qbusername','qbpassword')

admin.site.register(QBDetails, QBDetailsModelAdmin)

class PersonExternalIDMappingModelAdmin(admin.ModelAdmin):
    list_display = ('person', 'externalsystem', 'externalid')

admin.site.register(PersonExternalIDMapping, PersonExternalIDMappingModelAdmin)

class HIMSAppMappingModelAdmin(admin.ModelAdmin):
    list_display = ('person','appdetails')

admin.site.register(HIMSAppMapping, HIMSAppMappingModelAdmin)




