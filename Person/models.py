from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

# Create your models here.

from Doctor.models import AppDetails , DoctorDetails
from datetime import datetime
from backend_new import config,Constants
import os
from django.conf import settings
from backend_new.Constants import PreferredLanguageConstant

class PersonDetails(models.Model):
    PREFERRED_LANGUAGE = (
        (PreferredLanguageConstant.ENGLISH, 'English'), (PreferredLanguageConstant.HINDI, 'Hindi'),
        (PreferredLanguageConstant.GUJARATI, 'Gujarati') , (PreferredLanguageConstant.MARATHI, 'Marathi'))
    djangouser = models.ForeignKey(User,null=True, on_delete=models.CASCADE)
    qbusername = models.CharField(max_length=20,default="")
    qbpassword = models.CharField(max_length=20,default="")
    qbid = models.IntegerField(null=True,default=0)
    personid = models.AutoField(primary_key=True,null=False)
    personuid = models.CharField(unique=True,db_index=True ,null=True, max_length=10)
    name = models.CharField(max_length=255,null=False)
    phone = models.CharField(db_index=True,max_length=20,null=False)
    email = models.CharField(max_length=50,null=True,blank=True)
    address1 = models.CharField(max_length=511,null=True,blank=True)
    address2 = models.CharField(max_length=511,null=True,blank=True)
    datetime_joined = models.DateTimeField(auto_now_add=True)
    appuid = models.CharField(max_length=10, default="")
    appsinteracted = models.ManyToManyField(AppDetails, related_name="appsinteracted")
    fingerprint = models.CharField(max_length=2048,null=True,blank=True)
    dob_year = models.IntegerField(default=2000)
    dob = models.DateField(blank=True,null=True)
    gender = models.CharField(max_length=10,default="")
    userheight = models.FloatField(default=0.0)
    profilepic = models.CharField(max_length=255,default=config.DOMAIN+os.path.join(settings.STATIC_URL,settings.UPLOADEDFILES_DIR,
                               Constants.FileStorageConstants.USERPROFILEPICDIR,"default_user.png"))
    #hierarchy master or slave
    hierarchy=models.CharField(max_length=20,default="master")
    userweight = models.FloatField(default=0.0)
    preferredlanguage =  models.IntegerField(choices=PREFERRED_LANGUAGE, default=PreferredLanguageConstant.ENGLISH)


    class Meta:
        app_label = "Person"

    def __str__(self):
        return "{}-{}".format(self.name,self.personuid)

    def get_age(self):
        return timezone.now().year - self.dob_year

class QBDetails(models.Model):
    phone = models.CharField(primary_key=True,db_index=True,max_length=20,null=False)
    qbid = models.IntegerField(null=True,default=0)
    qbusername = models.CharField(max_length=20,default="")
    qbpassword = models.CharField(max_length=20,default="")

    def __str__(self):
        return self.phone

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.phone:
            self.datetime = datetime.now()
        return super(QBDetails, self).save(*args, **kwargs)


class AnalystAppMapping(models.Model):
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    appdetails = models.ForeignKey(AppDetails, on_delete=models.CASCADE)

    class Meta:
        unique_together = ("person","appdetails")

    def __str__(self):
      return str(self.person.personuid)

class HealthWorkerLocationMapping(models.Model):
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    location = models.CharField(max_length=50)

    class Meta:
        unique_together = ("person","location")

    def __str__(self):
      return str(self.person.personuid)


class AdminAppMapping(models.Model):
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    appdetails = models.ForeignKey(AppDetails, on_delete=models.CASCADE)

    class Meta:
        unique_together = ("person","appdetails")

    def __str__(self):
      return str(self.person.personuid)



class DoctorPersonMapping(models.Model):
    doctor = models.ForeignKey(DoctorDetails, on_delete=models.CASCADE)
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    authorization_level = models.CharField(max_length=50, default="all")
    istracked = models.BooleanField(default=False)

    class Meta:
        unique_together = ('doctor', 'person')

    def __str__(self):
        return "{0}-{1}".format(self.doctor.name, self.person.name)


class PersonExternalIDMapping(models.Model):
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    externalsystem = models.CharField(max_length=50, null=False)
    externalid = models.CharField(unique=True, max_length=50, null=False)

    def __str__(self):
        return "{0}=>{1}".format("yolo-"+str(self.person.personuid),self.externalsystem+":"+self.externalid)

    class Meta:
        unique_together = (("externalsystem","externalid"),)

class HIMSAppMapping(models.Model):
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    appdetails = models.ForeignKey(AppDetails, on_delete=models.CASCADE)

    class Meta:
        unique_together = ("person","appdetails")

    def __str__(self):
      return str(self.person.personid)
