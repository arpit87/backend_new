# Create your views here.
import json
import logging
import os
import time
import uuid
from datetime import datetime, timedelta
import datetime as dt
import base64
import pylibmc
import requests
from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import Group, User
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.db import IntegrityError
from django.http import HttpResponse
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.forms.models import model_to_dict
from Communications.smstemplates import getwelcometemplate
from operator import itemgetter
# import adt_a04
import Communications.utils
from HealthPackage.models import PackageSalesAgentRegisterPersonMapping, HealthPackageEnrollment, \
    PackageSalesAgentAppMapping
from backend_new.Constants import ServerConstants , FileStorageConstants, \
    FileTypeConstants, FileTypeDIRStorageMap, PreferredLanguageConstant
from Doctor.models import DoctorAppMapping
from Platform import utils, backendlogger
from Platform.views import  authenticateURL
from Person.models import (AdminAppMapping, AnalystAppMapping,
                           DoctorPersonMapping, PersonDetails, PersonExternalIDMapping)
# from Person.veripy import *
from backend_new import config
from Doctor.models import AppDetails , DoctorDetails
from Platform.uid_code_converter import encode_id, decode_uidcode


def get_localize_datetime(server_datetime):
    return datetime.fromtimestamp((time.mktime(timezone.localtime(server_datetime).timetuple()))).strftime('%d %b %Y %H:%M')



requestlogger = logging.getLogger('apirequests')


@csrf_exempt
def createPerson(request):
    data = json.loads(request.body)
    logmessage = []
    logmessage.append('createPersonRequest;%s'%(str(data)))

    name_req = data[ServerConstants.USERNAME]
    phone_req = data[ServerConstants.PHONE]

    try:
        email_req = data[ServerConstants.EMAIL]
    except KeyError:
        email_req = ""

    #try first dob else look for age
    try:
        dob_req = data[ServerConstants.DOB]
        #requestlogger.info("dob:"+dob_req)
        dob_year_req = int(dob_req.split('-')[0])
        dob = datetime.strptime(dob_req, "%Y-%m-%d").date()
    except KeyError:
        try:
            age_req = int(data[ServerConstants.AGE])
            dob_year_req = timezone.now().year - age_req
            dob=datetime.now() - timedelta(days=365*age_req) # calc approx dob
            dob=dob.date()
        except KeyError:
            dob_year_req = 0
            dob = None

    try:
        gender_req = data[ServerConstants.GENDER]
    except KeyError:
        gender_req = ""

    try:
        fingerprint_req = data[ServerConstants.FINGERPRINTDATA]
    except KeyError:
        fingerprint_req = ""

    try:
        appuid_req = data[ServerConstants.APPUID]
    except KeyError:
        appuid_req = ""

    try:
        preferredlanguage_req = data[ServerConstants.PREFERREDLANGUAGE]
    except KeyError:
        preferredlanguage_req = PreferredLanguageConstant.ENGLISH

    try:
        hierarchy_req = data[ServerConstants.HIERARCHY]
    except KeyError:
        hierarchy_req = None

    try:
        profilepic_req = data[ServerConstants.PROFILEPIC]
        profilepic_req += '==='
        # img_data = imgdata_req.decode("base64")
        img_data = base64.decodebytes(profilepic_req.encode())
        filetype = FileTypeConstants.USERPROFILEPIC
        filestoragedir = FileTypeDIRStorageMap[filetype]
        filestoragepath = os.path.join(settings.MEDIA_ROOT, filestoragedir)
        filenametosave = str(uuid.uuid4()) + ".png"
        fullpathondisk = ''.join([filestoragepath, '/', filenametosave])

        filetosave = open(fullpathondisk, "wb")
        filetosave.write(img_data)
        filetosave.close()

        savedfileurl = config.DOMAIN + os.path.join(settings.STATIC_URL, settings.UPLOADEDFILES_DIR,
                                                    filestoragedir, filenametosave)
    except KeyError:
        profilepic_req = None

    ### for otp
    try:
        otp = data[ServerConstants.OTP]
    except KeyError:
        otp = None

    ### healthpackage
    try:
        agentid_req = data[ServerConstants.AGENT_ID]
        agent_req = PersonDetails.objects.get(personuid=agentid_req)
        username = "person" + agent_req.phone
        logmessage.append('checking username %s, as agent' % (username))
        djangouser = User.objects.get(username=username)
        if djangouser is not None and djangouser.groups.filter(name='packagesalesagentgroup').exists():
            print("got agent")
            pass
        else:
            raise Exception('Invalid agent in request')
    except KeyError:
        logmessage.append('no agentid')
        agent_req = None
    except Exception:
        backendlogger.error('enrollUserForHealthProgram', request.GET, 'Invalid agent in request', logmessage)
        return HttpResponse("Invalid agent in request", status=403)

    #verify otp first
    cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
    cacheotp = cache.get(phone_req+"_otp")
    logmessage.append('cached otp is:%s'%(str(cacheotp)))
    # requestlogger.info('cached otp is:%s'%(str(cacheotp)))

    if(cacheotp == None):
        # universal otp
        if (str(otp) == str(config.BYPASSOTP)):
            logmessage.append('universal otp used for phone:' + phone_req)
        else:
            #this is done to create user from app without otp
            authenticated,djangouserstr = authenticateURL(request)
            if not authenticated:
                backendlogger.error('createPerson', data, 'Error authenticating', logmessage)
                return HttpResponse("Error authenticating ", status=401)

    elif str(cacheotp) != str(otp):
        backendlogger.info('createPersonNew', data, 'otp not verified', logmessage)
        return HttpResponse('otp not verified', status=400)

    usernameforuser = ''.join(["person",phone_req])
    passwordforuser =  phone_req[2:]
    #check if user already not present, then only create.
    # we do this to protect from condition where user partially created and failed
    # like only djangouser created etc

    djangouserexisted = False
    try:
        djangouser = User.objects.get(username=usernameforuser)
        djangouserexisted = True
    except ObjectDoesNotExist:
        namesplit = name_req.split(' ')
        firstname = namesplit[0]
        lastname = ""
        if len(namesplit) > 1:
            lastname = namesplit[1]
        djangouser = User.objects.create_user(username=usernameforuser,password=passwordforuser,
                                            first_name=firstname,last_name=lastname
                                            )

    newuser = PersonDetails.objects.create(djangouser= djangouser, name=name_req, phone=phone_req)
    # login(request,djangouser)

    g = Group.objects.get(name='persongroup')
    g.user_set.add(djangouser)

    if djangouserexisted:
        newuser.hierarchy = "slave"

    if hierarchy_req:
        if not djangouserexisted:
            newuser.hierarchy = "master:"+hierarchy_req
        else:
            newuser.hierarchy = "slave:"+hierarchy_req

    newuser.name = name_req
    if email_req!="":
        newuser.email = email_req
    if gender_req!="":
        newuser.gender = gender_req
    if dob_year_req > 0:
        newuser.dob_year = dob_year_req
        newuser.dob=dob
    if fingerprint_req!="":
        newuser.fingerprint = fingerprint_req
    if appuid_req != "":
        newuser.appuid = appuid_req
        newuser.appsinteracted.add(AppDetails.objects.get(appuid = appuid_req))
    if profilepic_req is not None:
        newuser.profilepic = savedfileurl

    newuser.preferredlanguage = preferredlanguage_req
    newuser.personuid = encode_id(newuser.personid)
    # newuser.hl7message = adt_a04.parse(newuser)
    newuser.save()

    externalid_req = ""
    externalsystem_req = ""
    try:
        externalid_req = data[ServerConstants.EXTERNALID]
        externalsystem_req = data[ServerConstants.EXTERNALSYSTEM]
        try:
            #if mapping already exists then overwrite it
            if data.get("append_personid"):
                externalid_req = externalid_req+"-"+str(newuser.personuid)
            existingmapping = PersonExternalIDMapping.objects.get(user = newuser,externalsystem = externalsystem_req)
            existingmapping.externalid = externalid_req
            existingmapping.save()
        except ObjectDoesNotExist:
            PersonExternalIDMapping.objects.create(user=newuser,externalsystem=externalsystem_req,externalid=externalid_req)
    except KeyError:
        pass

    jsondata = dict({
                     ServerConstants.PERSONUID:newuser.personuid ,
                     ServerConstants.USERNAME:newuser.name,
                     ServerConstants.PHONE:newuser.phone,
                     ServerConstants.PROFILEPIC:newuser.profilepic,
                     ServerConstants.EMAIL:newuser.email,
                     ServerConstants.AGE:timezone.now().year-newuser.dob_year,
                     ServerConstants.DOB:newuser.dob,
                     ServerConstants.GENDER:newuser.gender,
                     ServerConstants.APPUID:newuser.appuid,
                     ServerConstants.QBUSERNAME:newuser.qbusername,
                     ServerConstants.QBPASSWORD:newuser.qbpassword,
                     ServerConstants.QBID:newuser.qbid,
                     ServerConstants.EXTERNALID:externalid_req,
                     ServerConstants.EXTERNALSYSTEM:externalsystem_req})

    companyname = "DigiClinics"
    domain = config.DOMAIN

    if not djangouserexisted and appuid_req != "":
        appdetails = AppDetails.objects.get(appuid=appuid_req)
        try:
            Communications.utils.sendSMS(getwelcometemplate(preferredlanguage_req, appdetails.apptemplatedetails.brandname, appdetails.apptemplatedetails.host ),
                                         newuser.phone, appdetails.appsmssenderid,
                                         preferredlanguage_req != PreferredLanguageConstant.ENGLISH)
        except :
            pass

    #map to sales agent who registered this user
    if agent_req:
        PackageSalesAgentRegisterPersonMapping.objects.create(salesagent = agent_req,person = newuser )

    httpoutput = utils.successJson(jsondata)

    backendlogger.info('createPerson', data, httpoutput, logmessage)
    # requestlogger.info('createPersonResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

@csrf_exempt
def getPersonDetails(request):

    data = json.loads(request.body)
    logmessage = []
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('updatePersonProfile', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    personuid_req = ""
    person_req = ""

    try:
        personuid_req=data[ServerConstants.PERSONUID]
        try:
            person_req = PersonDetails.objects.get(personuid=personuid_req)
        except ObjectDoesNotExist:
            backendlogger.error('getPersonDetails', data, 'Person ID not found', logmessage)
            return HttpResponse("personuid not found", status=404)
    except KeyError:
            backendlogger.error('getPersonDetails', data, 'Provide personuid', logmessage)
            return HttpResponse("Provide personuid", status=400)

    logmessage.append('getPersonBookingDetailsRequest;personuid:%s'%(str(personuid_req)))
    # requestlogger.info('getPersonBookingDetailsRequest;personuid:%s,userphone%s'%(str(personuid_req),str(userphone_req)))

    externalid = ""
    externalsystem_req = ""
    try:
        externalsystem_req=data[ServerConstants.EXTERNALSYSTEM]
        try:
            externaldata = PersonExternalIDMapping.objects.get(user__personuid=personuid_req,externalsystem = externalsystem_req)
            externalid = externaldata.externalid
        except ObjectDoesNotExist:
            externalid = ""
            # backendlogger.error('getPersonDetails', logrequest, 'External ID not found', logmessage)
            # return HttpResponse("External ID not found", status=404)
    except KeyError:
        pass

    enrollments = HealthPackageEnrollment.objects.filter(person=person_req).order_by('-enrolldate')
    packagelist = list()
    for thisenrollment in enrollments:
        packagelist.append(dict({ServerConstants.STATUS: thisenrollment.getstatus(),
                                 ServerConstants.PACKAGENAME: thisenrollment.healthpackage.packagename,
                                 ServerConstants.STARTDATE: thisenrollment.startdate,
                                 ServerConstants.ENDDATE: thisenrollment.enddate}))

    apptag = "--"
    if person_req.appuid != None:
        apptag = AppDetails.objects.get(appuid=person_req.appuid).apptag


    jsondata = dict({ServerConstants.PERSONUID:person_req.personuid ,
                     ServerConstants.USERNAME:person_req.name,
                     ServerConstants.PHONE:person_req.phone,
                     ServerConstants.AGE:timezone.now().year-person_req.dob_year,
                     ServerConstants.DOB:person_req.dob,
                     ServerConstants.GENDER:person_req.gender,
                     ServerConstants.HEIGHT:person_req.userheight,
                     ServerConstants.WEIGHT:person_req.userweight,
                     ServerConstants.EMAIL:person_req.email,
                     ServerConstants.APPUID:person_req.appuid,
                     ServerConstants.APPTAG: apptag,
                     FileTypeConstants.USERPROFILEPIC:person_req.profilepic,
                     ServerConstants.EXTERNALSYSTEM:externalsystem_req,
                     ServerConstants.EXTERNALID: externalid,
                     ServerConstants.PACKAGELIST: packagelist,
                     })

    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getPersonDetails', data, httpoutput, logmessage)
    # requestlogger.info('getPersonBookingStatusResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)

@csrf_exempt
def updatePerson(request):
    # requestlogger.info('================updatePersonProfile=====================')

    data = json.loads(request.body)
    logmessage = []
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('updatePersonProfile', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    logmessage.append('updatePersonProfile;%s'%(str(data)))
    # requestlogger.info('updatePersonProfile;%s'%(str(data)))

    try:
        personuid_req = data[ServerConstants.PERSONUID]
        try:
            person = PersonDetails.objects.get(personuid=personuid_req)
        except ObjectDoesNotExist:
            backendlogger.error('updatePersonProfile', data, 'Person with personuid %s not found'%(str(personuid_req)), logmessage)
            # requestlogger.error('Person with personuid %s not found'%(str(personuid_req)))
            return HttpResponse("Person with given personuid not found",status=404)
    except KeyError:
            backendlogger.error('updatePersonProfile', data, 'personuid not provided', logmessage)
            # requestlogger.error('personuid/userphone not provided')
            return HttpResponse("Please provide personuid/phone",status=404)

    #name,email,dob,gender,fingerprint
    try:
        name = data[ServerConstants.USERNAME]
        person.name = name
    except KeyError:
        pass

    try:
        email = data[ServerConstants.EMAIL]
        person.email = email
    except KeyError:
        pass

    try:
        gender = data[ServerConstants.GENDER]
        person.gender = gender
    except KeyError:
        pass

    try:
        fingerprintdata = data[ServerConstants.FINGERPRINTDATA]
        person.fingerprint = fingerprintdata
    except KeyError:
        pass

    try:
        age_req = data[ServerConstants.AGE]
        person.dob_year = timezone.now().year - int(age_req)
    except KeyError:
        pass

    try:
        weight = data[ServerConstants.WEIGHT]
        person.userweight = weight
    except KeyError:
        pass

    try:
        height = data[ServerConstants.HEIGHT]
        person.userheight = height
    except KeyError:
        pass

    try:
        profilepic_req = data[ServerConstants.PROFILEPIC]
        profilepic_req += '==='
        # img_data = imgdata_req.decode("base64")
        img_data = base64.decodebytes(profilepic_req.encode())
        filetype = FileTypeConstants.USERPROFILEPIC
        filestoragedir = FileTypeDIRStorageMap[filetype]
        filestoragepath = os.path.join(settings.MEDIA_ROOT, filestoragedir)
        filenametosave = str(uuid.uuid4()) + ".png"
        fullpathondisk = ''.join([filestoragepath, '/', filenametosave])

        filetosave = open(fullpathondisk, "wb")
        filetosave.write(img_data)
        filetosave.close()

        savedfileurl = config.DOMAIN + os.path.join(settings.STATIC_URL, settings.UPLOADEDFILES_DIR,
                                                    filestoragedir, filenametosave)

        person.profilepic = savedfileurl
    except KeyError:
        pass

    person.save()

    try:
        externalid_req = data[ServerConstants.EXTERNALID]
        externalsystem_req = data[ServerConstants.EXTERNALSYSTEM]
        try:
            #if mapping already exists then overwrite it
            existingmapping = PersonExternalIDMapping.objects.get(person = person,externalsystem = externalsystem_req)
            existingmapping.externalid = externalid_req
        except ObjectDoesNotExist:
            PersonExternalIDMapping.objects.create(person=person,externalsystem=externalsystem_req,externalid=externalid_req)
    except KeyError:
        pass

    jsondata = dict({ServerConstants.PERSONUID:person.personuid,
                     ServerConstants.USERNAME:person.name,
                     ServerConstants.PHONE:person.phone,
                     ServerConstants.AGE:timezone.now().year-person.dob_year,
                     ServerConstants.DOB:person.dob,
                     ServerConstants.EMAIL:person.email,
                     ServerConstants.GENDER:person.gender,
                     ServerConstants.PROFILEPIC:person.profilepic,
                     ServerConstants.WEIGHT:person.userweight,
                     ServerConstants.HIERARCHY:person.hierarchy,
                     ServerConstants.HEIGHT:person.userheight})

    httpoutput = utils.successJson(jsondata)
    backendlogger.info('updatePersonProfile', data, httpoutput, logmessage)
    # requestlogger.info('updatePersonProfile;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE,status=200)

@csrf_exempt
def getAllPersonList(request):
    data = json.loads(request.body)
    logmessage = []
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('updatePersonProfile', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    logmessage.append('updatePersonProfile;%s' % (str(data)))

    searchparameter=''
    searchparametervalue=''

    offset=0
    limit=999
    allPersons=list()

    try:
        offset=data[ServerConstants.OFFSET]
        offset = int(offset)
    except KeyError:
        pass
    try:
        limit=data[ServerConstants.LIMIT]
        limit = int(limit)
    except KeyError:
        pass

    try:
        groupname_req=data[ServerConstants.GROUPNAME]
        group = Group.objects.get(name=groupname_req)
    except KeyError:
        groupname_req = None

    try:
        searchparameter=data[ServerConstants.SEARCHPARAMETER]
        try:
            searchparametervalue=data[ServerConstants.SEARCHPARAMETERVALUE]
        except KeyError:
            return HttpResponse("Search parameter value not sent",status=403)
    except KeyError:
        pass

    if not searchparameter:
        if groupname_req == None :
            allPersons = PersonDetails.objects.all()#the fact is queryset are lazy so database is accessed when it is printed so we can limit while printing
        else :
            alldjangoPersons = group.user_set.all()
            allPersons = PersonDetails.objects.filter(djangouser__in = alldjangoPersons, hierarchy ="master")

    elif searchparameter:
        if searchparameter.lower()=="name":
            allPersons=PersonDetails.objects.filter(name__icontains=searchparametervalue)
        elif searchparameter.lower()=="id":
            allPersons=PersonDetails.objects.filter(personuid=searchparametervalue)
        elif searchparameter.lower()=="phone":
            allPersons=PersonDetails.objects.filter(phone__icontains=searchparametervalue)
        elif searchparameter.lower()=="email":
            allPersons=PersonDetails.objects.filter(email__icontains=searchparametervalue)
        elif searchparameter.lower()=="appstr":
            apparray = searchparametervalue.split(',')
            allPersons=PersonDetails.objects.filter(appsinteracted__appuid__in=apparray)
        elif searchparameter.lower()=="externalsystem":
            externalsystem = searchparametervalue.split(':')[0]
            externalid = searchparametervalue.split(':')[1]
            try:
                usermap = PersonExternalIDMapping.objects.filter(externalsystem=externalsystem,externalid__startswith=externalid)
                allPersons=[mapping.user for mapping in usermap]
            except ObjectDoesNotExist:
                allPersons= []
        elif searchparameter == "json":
            kwargs = json.loads(searchparametervalue)
            if kwargs.get('age'):
                dob_year = datetime.today().year - (kwargs['age'])
                del kwargs['age']
                kwargs['dob__year'] = dob_year

            allPersons=PersonDetails.objects.filter(**kwargs)

    try:
        appstr_req=data[ServerConstants.APPSTR]
        apparray = appstr_req.split(',')
        allPersons=allPersons.filter(appuid__in=apparray)
    except KeyError:
        pass

    count=len(allPersons)
    personList = list()
    if count<offset+limit:
        final_limit=offset+count
    else:
        final_limit=offset+limit

    for person in allPersons[offset:final_limit]:
        enrollments = HealthPackageEnrollment.objects.filter(person=person).order_by('-enrolldate')
        packagelist = list()
        for thisenrollment in enrollments:
            packagelist.append(dict({ServerConstants.STATUS: thisenrollment.getstatus(),
                                     ServerConstants.PACKAGENAME: thisenrollment.healthpackage.packagename,
                                     ServerConstants.STARTDATE: thisenrollment.startdate,
                                     ServerConstants.ENDDATE: thisenrollment.enddate}))

        apptag = "--"
        if person.appuid != None:
            apptag = AppDetails.objects.get(appuid = person.appuid).apptag

        personList.append({
                            ServerConstants.PERSONUID:person.personuid,
                            ServerConstants.USERNAME:person.name,
                            ServerConstants.PHONE:person.phone,
                            ServerConstants.EMAIL:person.email,
                            ServerConstants.PROFILEPIC: person.profilepic,
                            ServerConstants.AGE:timezone.now().year-person.dob_year,
                            ServerConstants.GENDER:person.gender,
                            ServerConstants.HEIGHT:person.userheight,
                            ServerConstants.WEIGHT:person.userweight,
                            ServerConstants.REGISTRATIONDATE:person.datetime_joined,
                            ServerConstants.HIERARCHY:person.hierarchy,
                            ServerConstants.APPTAG:apptag,
                            ServerConstants.PACKAGELIST:packagelist
                        })

    personList = sorted(personList, key=itemgetter(ServerConstants.REGISTRATIONDATE), reverse=True)
    jsondata = dict({ServerConstants.PERSONLIST:personList, ServerConstants.TOTALPAGECOUNT:count})
    httpoutput = utils.successJson(jsondata)
    backendlogger.error('getAllPersonList', data, httpoutput, logmessage)
    # requestlogger.info('getAllPersonListResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)


'''
This api is generic and works with/without phone too
'''
@csrf_exempt
def verifyFingerPrint(request):
    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value

    data = request.GET[ServerConstants.FINGERPRINTDATA]
    logmessage.append('verifyFingerPrint;fpdata:%s'%(str(data)))
    # requestlogger.info('verifyFingerPrint;fpdata:%s'%(str(data)))

    matched = 0
    # matcheduser = ""
    # inputfingerprint=data
    #
    # try:
    #     appuid = request.GET[ServerConstants.appuid]
    #     applocation = request.GET[ServerConstants.APPLOCATION]
    # except KeyError:
    #     appuid = 0
    #     applocation = ""
    #
    # try:
    #     phone = request.GET[ServerConstants.PHONE]
    #     if phone!="":
    #         allusers_with_fingerprint_onthisphone = PersonDetails.objects.filter(fingerprint__isnull=False, phone=phone)
    #         for user in allusers_with_fingerprint_onthisphone:
    #             try:
    #                 if inputfingerprint[0:6]==user.fingerprint[0:6]=="Rk1SAC":
    #                     matched = veripy_v2(inputfingerprint,user.fingerprint)
    #                 else:
    #                     matched = veripy(inputfingerprint,user.fingerprint)
    #             except SystemError:
    #                 backendlogger.error('verifyFingerPrint', logrequest, 'System error in fp matching,did you run signer?', logmessage)
    #                 return HttpResponse('System error in fp matching,did you run signer?', status=400)
    #             if(matched):
    #                 matcheduser = user
    #                 break
    #         # this could be new user
    #         # or
    #         # no finger against this number matched then return false
    #         if not matched:
    #             jsondata = dict({ServerConstants.FINGERPRINTMATCHED:matched})
    #             httpoutput = utils.successJson(jsondata)
    #             logmessage.append('userFingerPrintLogin;%s'%(str(httpoutput)))
    #             response = HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)
    #             backendlogger.info('verifyFingerPrint', logrequest, response, logmessage)
    #             return response
    # except KeyError:
    #     pass
    #
    # if not matched:
    #     try:
    #         externalsystem_req = request.GET[ServerConstants.EXTERNALSYSTEM]
    #         externalid_req = request.GET[ServerConstants.EXTERNALID]
    #         userwiththisexternalid = None
    #         if externalid_req!="":
    #             try:
    #                 externaldata_users_with_externalsystem = PersonExternalIDMapping.objects.get(externalid=externalid_req,externalsystem = externalsystem_req)
    #                 userwiththisexternalid = externaldata_users_with_externalsystem.person
    #                 try:
    #                     matched = veripy_v2(inputfingerprint,userwiththisexternalid.fingerprint)
    #                 except SystemError:
    #                     backendlogger.error('verifyFingerPrint', logrequest, 'System error in fp matching,did you run signer?', logmessage)
    #                     return HttpResponse('System error in fp matching,did you run signer?', status=400)
    #             except ObjectDoesNotExist:
    #                 #user doesnt exist
    #                 matched = False
    #
    #             if(matched):
    #                 matcheduser = userwiththisexternalid
    #             # this could be new user
    #             # or
    #             # no finger against this number matched then return false
    #             if not matched:
    #                 jsondata = dict({ServerConstants.FINGERPRINTMATCHED:matched})
    #                 httpoutput = utils.successJson(jsondata)
    #                 logmessage.append('userFingerPrintLogin;%s'%(str(httpoutput)))
    #                 response = HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)
    #                 backendlogger.info('verifyFingerPrint', logrequest, response, logmessage)
    #                 return response
    #     except KeyError:
    #         pass
    #
    # if not matched:
    #
    #     #first check only users registered on this app as its less likely that user will go to another app
    #     allusers_with_fingerprint_onthisapp = PersonDetails.objects.filter(fingerprint__isnull=False, appuid=appid)
    #     for user in allusers_with_fingerprint_onthisapp:
    #         try:
    #             matched = veripy_v2(inputfingerprint,user.fingerprint)
    #         except SystemError:
    #             backendlogger.error('verifyFingerPrint', logrequest, 'System error in fp matching,did you run signer?', logmessage)
    #             return HttpResponse('System error in fp matching,did you run signer?', status=400)
    #         if(matched):
    #             matcheduser = user
    #             break
    #
    # if not matched:
    #     #now chk rest of app registrations
    #     allusers_with_fingerprint_exceptthisapp = PersonDetails.objects.exclude(appuid=appid).filter(fingerprint__isnull=False)
    #
    #     for user in allusers_with_fingerprint_exceptthisapp:
    #         try:
    #             matched = veripy_v2(inputfingerprint,user.fingerprint)
    #         except SystemError:
    #             backendlogger.error('verifyFingerPrint', logrequest, 'System error in fp matching,did you run signer?', logmessage)
    #             return HttpResponse('System error in fp matching,did you run signer?', status=400)
    #         if(matched):
    #             matcheduser = user
    #             break
    #
    #
    # if(matched):
    #     logmessage.append('finger print matched')
    #      #enter external id if sent
    #     externalid = ""
    #     externalsystem_req = ""
    #     try:
    #         externalsystem_req=request.GET[ServerConstants.EXTERNALSYSTEM]
    #         try:
    #             externaldata = PersonExternalIDMapping.objects.get(user=matcheduser,externalsystem = externalsystem_req)
    #             externalid = externaldata.externalid
    #         except ObjectDoesNotExist:
    #             backendlogger.error('verifyFingerPrint', logrequest, 'External ID not found', logmessage)
    #             matched = -1
    #             #return HttpResponse("External ID not found but fp matched", status=404)
    #     except KeyError:
    #         pass
    #
    #     djangouser = matcheduser.djangouser
    #     djangouser.backend = 'django.contrib.auth.backends.ModelBackend'
    #     if djangouser is not None:
    #         login(request,djangouser)
    #         logmessage.append('authenticated')
    #         # requestlogger.info('authenticated')
    #         jsondata = dict({ServerConstants.FINGERPRINTMATCHED:matched,
    #                          ServerConstants.personuid:matcheduser.personuid,
    #                          ServerConstants.USERNAME:matcheduser.name,
    #                          ServerConstants.PHONE:matcheduser.phone,
    #                          ServerConstants.AGE:timezone.now().year-matcheduser.dob_year,
    #                          ServerConstants.DOB:matcheduser.dob,
    #                          ServerConstants.GENDER:matcheduser.gender,
    #                          ServerConstants.HEIGHT:matcheduser.userheight,
    #                          ServerConstants.WEIGHT:matcheduser.userweight,
    #                          ServerConstants.EMAIL:matcheduser.email,
    #                          ServerConstants.EXTERNALSYSTEM:externalsystem_req,
    #                          ServerConstants.EXTERNALID:externalid})
    #     else:
    #         backendlogger.error('verifyFingerPrint', logrequest, 'inger print match but username/pass not authenticated', logmessage)
    #         # requestlogger.info('finger print match but username/pass not authenticated')
    #         return HttpResponse('Invalid username/password but fp matched', status=400)
    # else:
    #     jsondata = dict({ServerConstants.FINGERPRINTMATCHED:matched})

    httpoutput = utils.successJson(dict({ServerConstants.FINGERPRINTMATCHED:0}))
    logmessage.append('userFingerPrintLogin;%s'%(str(httpoutput)))
    # requestlogger.info('userFingerPrintLogin;%s'%(str(httpoutput)))
    response = HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

    backendlogger.info('verifyFingerPrint', logrequest, response, logmessage)
    return response


@csrf_exempt
def mapPersontypeToAppid(request):
    # requestlogger.info('================mapPersontypeToAppid=====================')

    data = json.loads(request.body)
    logmessage = []

    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('mapPersontypeToAppid', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    logmessage.append('mapPersontypeToAppid;%s'%(str(data)))
    # requestlogger.info('mapPersontypeToAppid;%s'%(str(data)))

    try:
        personphone_req = data[ServerConstants.PHONE]
    except KeyError as e:
        backendlogger.error('mapPersontypeToAppid', data, str(e), logmessage)
        return HttpResponse("Person not found",status=403)

    try:
        appuid_req = data[ServerConstants.APPUID]
    except KeyError as e:
        backendlogger.error('mapPersontypeToAppid', data, str(e), logmessage)
        return HttpResponse("WebApp id not found",status=403)

    try:
        persontype_req = data[ServerConstants.PERSONTYPE]
    except KeyError as e:
        backendlogger.error('mapPersontypeToAppid', data, str(e), logmessage)
        return HttpResponse("Type not found",status=403)

    try:
        action_req = data[ServerConstants.GROUPACTION]
    except KeyError as e:
        backendlogger.error('mapPersontypeToAppid', data, str(e), logmessage)
        return HttpResponse("Provide action to perform",status = 403)

    try:
        appdetails = AppDetails.objects.get(appuid=appuid_req)
    except ObjectDoesNotExist as e:
        backendlogger.error('mapPersontypeToAppid', data, str(e), logmessage)
        return HttpResponse("app doesnt exist",status=404)

    try:
        consultation_type_req = data[ServerConstants.CONSULTATIONTYPE] #this comes as string
        consultation_type_req = int(consultation_type_req)
    except KeyError:
        consultation_type_req = DoctorAppMapping.TELEMEDICINE

    if(persontype_req == "doctor"):   # doctor mapping
        mappeddoctor = DoctorAppMapping.objects.filter(doctor__phone=personphone_req, appdetails=appdetails, consultation_type=consultation_type_req)

        try:
            doctor = DoctorDetails.objects.get(phone=personphone_req)
        except ObjectDoesNotExist:
           return HttpResponse("Doctor doesnt exist",status=404)

        logmessage.append('mapping Doctor to app')
        # requestlogger.info('mapping Doctor to app')
        try:
            if (action_req=="add"):
                if mappeddoctor.exists():
                    return HttpResponse("Doctor already mapped",status=403)
                else:
                    DoctorAppMapping.objects.create(doctor=doctor, appdetails=appdetails, consultation_type=consultation_type_req)
            elif (action_req=="remove"):
                if mappeddoctor.exists():
                    mappeddoctor.delete()
                else:
                    return HttpResponse("Doctor not mapped can't remove",status=403)

            else:
                return HttpResponse("Mapping action not found",status=403)


        except IntegrityError:
             return HttpResponse("Mapping Failed",status=403)

    elif(persontype_req == "analyst"):    # Analyst mapping
        mappeduser = AnalystAppMapping.objects.filter(person__phone=personphone_req, person__hierarchy__startswith='master', appdetails=appdetails)

        try:
            person = PersonDetails.objects.get(phone=personphone_req, hierarchy__startswith='master')
        except (IndexError, ObjectDoesNotExist):
            return HttpResponse("Person doesnt exist",status=404)

        djangouser = User.objects.get(username = 'person'+str(personphone_req))
        if(djangouser.groups.filter(name='analystgroup').exists()):
            logmessage.append('mapping analyst to app')
            # requestlogger.info('mapping Doctor to app')
            try:
                if (action_req=="add"):
                    if mappeduser.exists():
                        return HttpResponse("Person already mapped",status=403)
                    else:
                        AnalystAppMapping.objects.create(person=person, appdetails=appdetails)
                elif (action_req=="remove"):
                    if mappeduser.exists():
                        mappeduser.delete()
                    else:
                        return HttpResponse("Person not mapped can't remove",status=403)
                else:
                    return HttpResponse("Mapping action not found",status=403)


            except IntegrityError:
                backendlogger.error('mapPersontypeToAppid', data, 'Mapping Failed', logmessage)
                return HttpResponse("Mapping Failed",status=403)
        else:
            backendlogger.error('mapPersontypeToAppid', data, 'Person not in analyst group', logmessage)
            return HttpResponse("Person not in analyst group",status=403)

    elif(persontype_req == "admin"):  # Admin mapping
        mappeduser = AdminAppMapping.objects.filter(person__phone=personphone_req, person__hierarchy__startswith='master', appdetails=appdetails)

        try:
            person = PersonDetails.objects.get(phone=personphone_req, hierarchy__startswith='master')
        except (IndexError, ObjectDoesNotExist):
            return HttpResponse("Person doesnt exist/Invalid phone",status=404)

        djangouser = User.objects.get(username = 'person'+str(personphone_req))
        if(djangouser.groups.filter(name='admingroup').exists()):
            logmessage.append('mapping admin to app')
            # requestlogger.info('mapping Doctor to app')
            try:
                if (action_req=="add"):
                    if mappeduser.exists():
                        return HttpResponse("Person already mapped",status=403)
                    else:
                        AdminAppMapping.objects.create(person=person,appdetails=appdetails)
                elif (action_req=="remove"):
                    if mappeduser.exists():
                        mappeduser.delete()
                    else:
                        return HttpResponse("Person not mapped can't remove",status=403)
                else:
                    return HttpResponse("Mapping action not found",status=403)

            except IntegrityError:
                backendlogger.error('mapPersontypeToAppid', data, 'Mapping Failed', logmessage)
                return HttpResponse("Mapping Failed",status=403)
        else:
            backendlogger.error('mapPersontypeToAppid', data, 'Person not in admin group', logmessage)
            return HttpResponse("Person not in admin group",status=403)
    elif (persontype_req == "packagesalesagent"):  # Admin mapping
        mappeduser = PackageSalesAgentAppMapping.objects.filter(salesagent__phone=personphone_req,
                                                    salesagent__hierarchy__startswith='master', appdetails=appdetails)

        try:
            person = PersonDetails.objects.get(phone=personphone_req, hierarchy__startswith='master')
        except (IndexError, ObjectDoesNotExist):
            return HttpResponse("Person doesnt exist/Invalid phone", status=404)

        djangouser = User.objects.get(username='person' + str(personphone_req))
        if (djangouser.groups.filter(name='packagesalesagentgroup').exists()):
            logmessage.append('mapping packagesalesagent to app')
            # requestlogger.info('mapping Doctor to app')
            try:
                if (action_req == "add"):
                    if mappeduser.exists():
                        return HttpResponse("Person already mapped", status=403)
                    else:
                        PackageSalesAgentAppMapping.objects.create(salesagent=person, appdetails=appdetails)
                elif (action_req == "remove"):
                    if mappeduser.exists():
                        mappeduser.delete()
                    else:
                        return HttpResponse("Person not mapped can't remove", status=403)
                else:
                    return HttpResponse("Mapping action not found", status=403)

            except IntegrityError:
                backendlogger.error('mapPersontypeToAppid', data, 'Mapping Failed', logmessage)
                return HttpResponse("Mapping Failed", status=403)
        else:
            backendlogger.error('mapPersontypeToAppid', data, 'Person not in admin group', logmessage)
            return HttpResponse("Person not in admin group", status=403)
    else:
        backendlogger.error('mapPersontypeToAppid', data, 'Person type not matched', logmessage)
        return HttpResponse("Filetype not matched",status=403)


    httpoutput = utils.successJson(dict())
    backendlogger.info('mapPersontypeToAppid', data, httpoutput, logmessage)
    # requestlogger.info('mapPersontypeToAppid;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

@csrf_exempt
def getAllAppMappingForPersonType(request):
    # requestlogger.info('================getAllAppMapping=====================')

    data = json.loads(request.body)
    logmessage = []

    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('mapPersontypeToAppid', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)


    type_req = data[ServerConstants.PERSONTYPE]

    try:
        appstr_req=data[ServerConstants.APPSTR]
        apparray = appstr_req.split(',')
    except KeyError:
        backendlogger.error('getAllAppMappingForPersonType', data, 'Provide appstr', logmessage)
        return HttpResponse("Provide appstr", status=403)

    personList = list()

    if(type_req=="analyst"):    # Analyst List from mapping table
        if(appstr_req=="all"):
            allanalystapp = AnalystAppMapping.objects.all()
        else:
            allanalystapp = AnalystAppMapping.objects.filter(appdetails__appuid__in=apparray)

        for appanalyst in allanalystapp:
            personList.append({
                               ServerConstants.APPUID:appanalyst.appdetails.appuid,
                               ServerConstants.APPTAG:appanalyst.appdetails.apptag,
                               ServerConstants.USERNAME:appanalyst.person.name,
                               ServerConstants.PHONE:appanalyst.person.phone
                            })

    elif(type_req=="admin"):    # Admin List from mapping table
        if(appstr_req=="all"):
            alladminapp = AdminAppMapping.objects.all()
        else:
            alladminapp = AdminAppMapping.objects.filter(appdetails__appuid__in=apparray)

        for appadmin in alladminapp:
            personList.append({
                               ServerConstants.APPUID:appadmin.appdetails.appuid,
                               ServerConstants.APPTAG:appadmin.appdetails.apptag,
                               ServerConstants.USERNAME:appadmin.person.name,
                               ServerConstants.PHONE:appadmin.person.phone
                            })

    elif (type_req == "doctor"):
        if(appstr_req=="all"):
            alldoctorapp = DoctorAppMapping.objects.all()
        else:
            alldoctorapp = DoctorAppMapping.objects.filter(appdetails__appuid__in=apparray)
        for appdoctor in alldoctorapp:
            personList.append({  ServerConstants.APPUID:appdoctor.appdetails.appuid,
                               ServerConstants.APPTAG:appdoctor.appdetails.apptag,
                               ServerConstants.USERNAME:appdoctor.doctor.name,
                               ServerConstants.PHONE:appdoctor.doctor.phone,
                               ServerConstants.CONSULTATIONTYPE: appdoctor.get_consultation_type_display()
                               })

    elif (type_req == "packagesalesagent"):
        if (appstr_req == None):
            allagentapp = PackageSalesAgentAppMapping.objects.all()
        else:
            allagentapp = PackageSalesAgentAppMapping.objects.filter(appdetails__appuid__in=apparray)
        for appagent in allagentapp:
            personList.append({ServerConstants.APPUID: appagent.appdetails.appuid,
                               ServerConstants.APPTAG: appagent.appdetails.apptag,
                               ServerConstants.USERNAME: appagent.person.name,
                               ServerConstants.PHONE: appagent.person.phone
                               })

    jsondata = dict({ServerConstants.PERSONLIST:personList})
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getAllAppMapping', data, httpoutput, logmessage)
    # requestlogger.info('getAllPersonListResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def assignRemoveGroup(request):
    # requestlogger.info('================addAndRemoveGroupToPerson=====================')
    data = json.loads(request.body)
    logmessage = []
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('addAndRemoveGroupToPerson', data, 'Error authetnicating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    logmessage.append('addAndRemoveGroupToPerson;%s'%(str(data)))
    # requestlogger.info('addAndRemoveGroupToPerson;%s'%(str(data)))
    personuid_req = None
    personphone_req =None
    djangouser = None

    try:
        action_req = data[ServerConstants.GROUPACTION]
    except KeyError:
        backendlogger.error('addAndRemoveGroupToPerson', data, 'Provide action to perform', logmessage)
        return HttpResponse("Provide action to perform",status = 403)

    try:
        personuid_req = data[ServerConstants.PERSONUID]
        try:
           user = PersonDetails.objects.get(personuid=personuid_req)
           djangouser = user.djangouser
        except ObjectDoesNotExist:
            backendlogger.error('addAndRemoveGroupToPerson', data, 'ID not found. Are you registered?', logmessage)
            return HttpResponse("ID not found. Are you registered?",status=404)
    except KeyError:
        try:
            personphone_req = data[ServerConstants.PHONE]
            djangouser = User.objects.get(username="person"+personphone_req)
            if djangouser == None:
                backendlogger.error('addAndRemoveGroupToPerson', data, 'Phone No not found. Are you registered?', logmessage)
                return HttpResponse("Phone No not found. Are you registered?",status=404)
        except KeyError:
            backendlogger.error('addAndRemoveGroupToPerson', data, 'Provide id / Phone number', logmessage)
            return HttpResponse("Provide id / Phone number",status = 403)

    try:
        group_req = data[ServerConstants.GROUPNAME]
    except KeyError:
        backendlogger.error('addAndRemoveGroupToPerson', data, 'Group not found', logmessage)
        return HttpResponse("Group not found",status=403)

    g = Group.objects.get(name=group_req)
    if action_req == "add":
        if(djangouser.groups.filter(name=group_req).exists()):
            backendlogger.error('addAndRemoveGroupToPerson', data, "Already in "+group_req, logmessage)
            return HttpResponse("Already in "+group_req ,status = 403)
        else:
            #add to usergroup, it helps in checking permissions
            g.user_set.add(djangouser)
    elif action_req == "remove":
        if(djangouser.groups.filter(name=group_req).exists()):
            #remove from usergroup, it helps in checking permissions
            g.user_set.remove(djangouser)
        else:
            backendlogger.error('addAndRemoveGroupToPerson', data, "Already in "+group_req, logmessage)
            return HttpResponse("Cant remove not mapped to "+group_req ,status = 403)

    httpoutput = utils.successJson(dict())
    backendlogger.info('addAndRemoveGroupToPerson', data, httpoutput, logmessage)
    # requestlogger.info('addAndRemoveGroupToPerson;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def setdoctorpersonmapping(request):
    data = json.loads(request.body)

    logmessage = []

    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('setdoctorpersonmapping', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        personuid_req = data[ServerConstants.PERSONUID]
    except KeyError:
        backendlogger.error('setdoctorpersonmapping', data, 'Please provide USER ID', logmessage)
        return HttpResponse("Please provide USER ID", status=400)

    try:
        doctoruid_req = data[ServerConstants.DOCTORUID]
    except KeyError:
        backendlogger.error('setdoctorpersonmapping', data, 'Please provide Doctor ID', logmessage)
        return HttpResponse("Please provide Doctor ID", status=400)


    try:
        person = PersonDetails.objects.get(personuid=personuid_req)
    except ObjectDoesNotExist:
        backendlogger.error('setdoctorpersonmapping', data, 'Person not found for given USER ID', logmessage)
        return HttpResponse("Person not found for given USERID", status=404)

    try:
        doctor = DoctorDetails.objects.get(doctoruid=doctoruid_req)
    except ObjectDoesNotExist:
        backendlogger.error('setdoctorpersonmapping', data, 'Doctor not found for given Doctor ID', logmessage)
        return HttpResponse("Doctor not found for given doctoruid", status=404)

    try:
        istracked_req = data[ServerConstants.ISTRACKED]
    except KeyError:
        istracked_req = False

    try:
        DoctorPersonMapping.objects.create(person=person, doctor=doctor, authorization_level="all", istracked = istracked_req)
    except IntegrityError:
        backendlogger.info('setdoctorpersonmapping', data, 'Mapping already exists', logmessage)

    httpresponse = dict()
    if istracked_req:
        try:
            if doctor.primaryapp:
                httpresponse[ServerConstants.APPUID] = doctor.primaryapp.appuid
        except ObjectDoesNotExist:
            backendlogger.error('setdoctorpersonmapping', data, 'Doctor does not have any primary app', logmessage)
            return HttpResponse("Doctor does not have any primary app", status=404)

    httpoutput = utils.successJson(httpresponse)
    backendlogger.info('setdoctorpersonmapping', data, httpoutput, logmessage)
    # requestlogger.info('mapPersontypeToAppid;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def getdoctorpersonmapping(request):
    data = json.loads(request.body)

    logmessage = []

    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('setdoctorpersonmapping', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        doctoruid_req = data[ServerConstants.DOCTORUID]
    except KeyError:
        backendlogger.error('getdoctorpersonmapping', data, 'doctoruid not sent', logmessage)
        return HttpResponse("Doctor ID not found", status=403)

    try:
        doctor = DoctorDetails.objects.get(doctoruid=doctoruid_req)
    except ObjectDoesNotExist:
        backendlogger.error('getdoctorpersonmapping', data, 'Doctor not found for given Doctor ID', logmessage)
        return HttpResponse("Doctor not found for given Doctor id", status=404)


    userList = list()

    mappedusers = DoctorPersonMapping.objects.filter(doctor=doctor)

    for mappeduser in mappedusers:
        userList.append({
            ServerConstants.USERNAME:mappeduser.person.name,
            ServerConstants.PHONE:mappeduser.person.phone,
            ServerConstants.PERSONUID:mappeduser.person.personuid,
            ServerConstants.AGE:mappeduser.person.get_age(),
            ServerConstants.GENDER:mappeduser.person.gender,
            ServerConstants.PROFILEPIC:mappeduser.person.profilepic,
            ServerConstants.ISTRACKED: mappeduser.istracked
        })

    jsondata = dict({ServerConstants.PERSONLIST: userList})
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getdoctorpersonmapping', data, httpoutput, logmessage)
    # requestlogger.info('getAllPersonListResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

@csrf_exempt
def getpersondoctormapping(request):

    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getpersondoctormapping', logrequest, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        personuid_req = request.GET[ServerConstants.PERSONUID]

    except KeyError:
        backendlogger.error('getpersondoctormapping', logrequest, 'personuid not sent', logmessage)
        return HttpResponse("Person ID not found", status=403)

    try:
        user = PersonDetails.objects.get(personuid=personuid_req)
    except ObjectDoesNotExist:
        backendlogger.error('getpersondoctormapping', logrequest, 'user not found for given user ID', logmessage)
        return HttpResponse("user not found for given user id", status=404)

    DoctorList_temp = list()

    mappedusers = DoctorPersonMapping.objects.filter(user=user)

    for mappeduser in mappedusers:
        DoctorList_temp.append({
            ServerConstants.DOCTORNAME:mappeduser.doctor.name,
            ServerConstants.PHONE:mappeduser.doctor.phone,
            ServerConstants.DOCTORUID:mappeduser.doctor.doctoruid,
            ServerConstants.AUTH_LEVEL:mappeduser.authorization_level
        })

    # Removed duplicate Entry from queryset
    userList= { each['doctoruid'] : each for each in DoctorList_temp }.values()

    jsondata = dict({ServerConstants.DOCTORLIST: userList})
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getpersondoctormapping', logrequest, httpoutput, logmessage)
    # requestlogger.info('getAllPersonListResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def getDetalisFromPersonUID(request):

    data = json.loads(request.body)
    logmessage = []

    try:
        personuid_req=data[ServerConstants.PERSONUID]
        try:
            person_req = PersonDetails.objects.get(personuid=personuid_req)
        except ObjectDoesNotExist:
            backendlogger.error('getDetalisFromPersonUID', data, 'Person ID not found', logmessage)
            return HttpResponse("personuid not found", status=404)
    except KeyError:
            backendlogger.error('getDetalisFromPersonUID', data, 'Provide personuid', logmessage)
            return HttpResponse("Provide personuid", status=400)

    logmessage.append('getDetalisFromPersonUID;personuid:%s'%(str(personuid_req)))
    # requestlogger.info('getPersonBookingDetailsRequest;personuid:%s,userphone%s'%(str(personuid_req),str(userphone_req)))


    jsondata = dict({
                     ServerConstants.PERSONUID:person_req.personuid ,
                     ServerConstants.USERNAME:person_req.name,
                     ServerConstants.AGE:timezone.now().year-person_req.dob_year,
                     ServerConstants.GENDER:person_req.gender
                     })

    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getDetalisFromPersonUID', data, httpoutput, logmessage)
    # requestlogger.info('getPersonBookingStatusResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)