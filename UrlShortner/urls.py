from django.urls import path

from .views import redirect_view

urlpatterns = [path('<slug:urlidcode>', view=redirect_view, name='short_url_view')]