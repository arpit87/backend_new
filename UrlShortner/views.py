import json
import logging

import requests
from django.shortcuts import redirect
from django.core.exceptions import ObjectDoesNotExist
from django.views.decorators.csrf import csrf_exempt

from Platform.views import authenticateURL
from UrlShortner.models import  URLDetails
from Platform.uid_code_converter import decode_uidcode , encode_id
from backend_new import  config
from Platform import backendlogger, utils
from django.http import HttpResponse

from backend_new.Constants import ServerConstants

requestlogger = logging.getLogger('apirequests')
import re
from django.shortcuts import render


def redirect_view(request, urlidcode):
    #first check chrome browser
    useragentstr = request.META['HTTP_USER_AGENT']
    requestlogger.info("got HTTP_USER_AGENT:" + useragentstr)
    data = dict({
        "urlidcode":urlidcode,
        "HTTP_USER_AGENT": useragentstr
    })
    # have stop user agent like : Mozilla/5.0 (Linux; U; Android 10; en-in; GM1901 Build/QKQ1.190716.003) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/71.0.3578.141 Mobile Safari/537.36 XiaoMi/MiuiBrowser/12.1.4-g
    # they have 'Chrome' but in the end have 'RealMeBroswer' etc
    # Mozilla/5.0 (Linux; U; Android 9; en-gb; RMX1941 Build/PPR1.180610.011) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.134 Mobile Safari/537.36 RealmeBrowser/35.5.0.8
    # return error if 1) No 'Safari' found and no Firefox , 2) string found after 'Safari/234/234' regex 3) 'Chrome' not found in first part of string
    splitagent = re.split('Safari/[0-9]+\.[0-9]+', useragentstr)
    if  len(splitagent) == 1:
        if "Firefox" not in splitagent[0]:
            backendlogger.info("redirect_view",data,"Please open the url in Google Chrome/Firefox browser",[] )
            return render(request,"openinchrome.html",{})
    elif splitagent[1] is not "" or ("Chrome" not in splitagent[0] and "CriOS" not in splitagent[0] and "Firefox" not in splitagent[0]):
        backendlogger.info("redirect_view", data, "Please open the url in Google Chrome/Firefox browser", [])
        return render(request,"openinchrome.html",{})

    requestlogger.info("redirecting urlidcode" + urlidcode)
    urlid = decode_uidcode(urlidcode)
    try:
        urldetails = URLDetails.objects.get(urlid = urlid)
        longurl = urldetails.longurl
        urldetails.clicked()
    except ObjectDoesNotExist:
        return redirect("/")

    return redirect(longurl)

@csrf_exempt
def createShortUrl(request):
    logmessage = []
    data = json.loads(request.body)
    # requestlogger.info('================generateInvoice=====================')
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('createShortUrl', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    logmessage.append('createShortUrl;%s'%(str(data)))

    try:
        longurl_req = data[ServerConstants.LONGURL]
    except KeyError:
        backendlogger.error('createShortUrl', data, 'Please provide longurl', logmessage)
        return HttpResponse("Please provide longurl", status=400)

    try:
        response = requests.get(longurl_req)
    except:
        return HttpResponse("Long URL GET failed", status=400)

    if response.status_code != 200:
        return HttpResponse("Long URL GET failed", status=400)

    shorturl = create_short_url(longurl_req)


    jsondata = dict({
        ServerConstants.SHORTURL: shorturl,
    })

    httpoutput = utils.successJson(jsondata)
    backendlogger.info('createShortUrl', data, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


def create_short_url(longurl):
    urldetails = URLDetails.objects.create(longurl = longurl)
    shorturl = config.SHORTURLDOMAIN +"/" + encode_id(urldetails.urlid)
    requestlogger.info("Long URL was %s, short URL is %s" % (longurl,shorturl ))
    urldetails.shorturl = shorturl
    urldetails.save()
    return shorturl
