from django.db import models
# Create your models here.



class URLDetails(models.Model):
    urlid = models.AutoField(primary_key=True, null=False)
    shorturl = models.CharField(max_length=40)
    longurl = models.CharField(max_length=512)
    created_at = models.DateTimeField(auto_now_add=True)
    clicks = models.IntegerField(default=0)

    def clicked(self):
        self.clicks += 1
        self.save()

    def __str__(self):
      return '{}-{}'.format(self.urlid, self.shorturl)

