from UrlShortner.models import URLDetails
from django.contrib import admin


class URLDetailsModelAdmin(admin.ModelAdmin):
    list_display = ('urlid','shorturl','longurl','created_at','clicks')

admin.site.register(URLDetails, URLDetailsModelAdmin)