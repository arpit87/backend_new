import datetime
import json
import logging
import os
import pylibmc
import requests
from django.conf import settings
from django.contrib.auth.models import Group, User
from django.contrib.sessions.models import Session
from django.core import serializers
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError, transaction
from django.http import HttpResponse
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime, timedelta
from django.core.exceptions import MultipleObjectsReturned
from Platform.uid_code_converter import encode_id
from QueueManagement.models import WaitingQueue
from QueueManagement.views import calc_queue_number
import Communications.utils
import Platform.utils
from Lab.models import AppLabMapping
from backend_new.Constants import ServerConstants
from Doctor.models import (DoctorAppMapping, AppConfig,
                           AppDetails, DoctorDetails,
                           DoctorWeeklyCalender, Services,
                           SkillNumToGroup, DoctorStatsNotification)
from Appointments.models import SlotBooking
from Platform import utils, backendlogger
from Platform.views import authenticateURL
from backend_new import config

requestlogger = logging.getLogger('apirequests')


@csrf_exempt
def createDoctor(request):
    data = json.loads(request.body)
    logmessage = []
    logmessage.append('createDoctor;%s' % (str(data)))
    name_req = data[ServerConstants.DOCTORNAME]
    phone_req = data[ServerConstants.DOCTORPHONE]
    skillarray_req = data[ServerConstants.SKILLARRAY]
    gender_req = data[ServerConstants.DOCTORGENDER]

    try:
        age_req = data[ServerConstants.AGE]
    except KeyError:
        age_req = False

    try:
        email_req = data[ServerConstants.EMAIL]
    except KeyError:
        email_req = ""

    try:
        registrationnum_req = data[ServerConstants.REGISTRATIONNUM]
    except KeyError:
        registrationnum_req = ""

    try:
        specialization_req = data[ServerConstants.SPECIALIZATION]
    except KeyError:
        specialization_req = ""

    try:
        qualification_req = data[ServerConstants.QUALIFICATION]
    except KeyError:
        qualification_req = ""

    try:
        experience_req = data[ServerConstants.EXPERIENCE]
    except KeyError:
        experience_req = ""

    try:
        biosketch_req = data[ServerConstants.DOCTORBIOSKETCH]
    except KeyError:
        biosketch_req = ""

    try:
        cityname_req = data[ServerConstants.CITYNAME]
    except KeyError:
        cityname_req = ""

    try:
        partner_req = data[ServerConstants.PARTNER]
    except KeyError:
        partner_req = ""

    usernameforDoctor = ''.join(["doctor", phone_req])
    passwordforDoctor = phone_req[2:]  # Platform.utils.getRandomPassword(6)
    namesplit = name_req.split(' ')
    firstname = namesplit[0]
    lastname = ""
    if len(namesplit) > 1:
        lastname = namesplit[1]

    try:
        djangouser = User.objects.create_user(username=usernameforDoctor, password=passwordforDoctor,
                                              first_name=firstname, last_name=lastname)
    except IntegrityError:
        backendlogger.error('createDoctor', data, 'Phone nnumber exists', logmessage)
        return HttpResponse("Phone number exists", status=400)

    servicesoffered = applydefaultservicesbyskillstr(skillarray_req)

    newdoctor = DoctorDetails(djangouser=djangouser, experience=experience_req,
                              name=name_req, phone=phone_req, email=email_req, skillstr="0",
                              gender=gender_req, yearofbirth=datetime.now().year - age_req,
                              registrationnum=registrationnum_req, specialization=specialization_req,
                              qualification=qualification_req, biosketch=biosketch_req,
                              servicesoffered=servicesoffered, city=cityname_req,
                              )
    newdoctor.save()

    for skillnum in skillarray_req:
        newdoctor.skills.add(SkillNumToGroup.objects.get(skillnum=skillnum))

    newdoctor.doctoruid = encode_id(newdoctor.doctorid)
    newdoctor.save()

    # add to Doctorgroup
    g = Group.objects.get(name='doctorgroup')
    g.user_set.add(djangouser)

    # is doctor? add to doctor group..etc
    for skill in skillarray_req:
        groupnameforthisskill = SkillNumToGroup.objects.get(skillnum=int(skill)).groupname
        g = Group.objects.get(name=groupnameforthisskill)
        g.user_set.add(djangouser)

    # create quickblox user
    # '{"user": {"login": "Lena", "password": "Lena", "email": "lena@domain.com", "external_user_id": "68764641", "facebook_id": "87964654",
    # "twitter_id": "65765413614", "full_name": "Lena Laktionova", "phone": "87654351", "website": "http://lena.com", "tag_list": "name,age"}}'
    usernameforqb = "doctor" + newdoctor.phone
    passwordforqb = Platform.utils.getRandomPassword(8)
    dataToSend = dict({"login": usernameforqb, "password": passwordforqb,
                       "full_name": newdoctor.name, "phone": newdoctor.phone,  # "tags":skill_req,
                       })

    qbsessiontoken = Platform.utils.getQBAppSessionToken(True)

    logmessage.append('createDoctor;data to QB:%s' % (json.dumps(dataToSend)))
    # requestlogger.info('createDoctor;data to QB:%s'%(json.dumps(dataToSend)))
    req = requests.post(config.QB_USER_API, data=json.dumps({"user": dataToSend}),
                        headers={'Content-Type': 'application/json',
                                 'QuickBlox-REST-API-Version': '0.1.0',
                                 'QB-Token': qbsessiontoken})

    # try with uncached new token
    if (req.status_code != 201):
        qbsessiontoken = Platform.utils.getQBAppSessionToken(False)
        logmessage.append(
            '*****alert**alert****cached token didnt work*****alert**alert****,created new:' + qbsessiontoken)
        # requestlogger.info('*****alert**alert****cached token didnt work*****alert**alert****')
        req = requests.post(config.QB_USER_API, data=json.dumps({"user": dataToSend}),
                            headers={'Content-Type': 'application/json',
                                     'QuickBlox-REST-API-Version': '0.1.0',
                                     'QB-Token': qbsessiontoken})

    if (req.status_code != 201):
        # requestlogger.info('*****alert**alert***error creating qb user******')
        # requestlogger.info(req.content)
        backendlogger.error('createDoctor', data, 'Error creatinng QB user %s' % (req.content), logmessage)
        return HttpResponse('Error creating QB Person:%r' % (req.content), status=403)

    response_data = json.loads(req.content)
    newdoctor.qbid = response_data["user"]["id"]
    newdoctor.qbusername = usernameforqb
    newdoctor.qbpassword = passwordforqb
    newdoctor.save()

    # qb counter user
    counterusernameforqb = "counter" + newdoctor.phone
    counterpasswordforqb = Platform.utils.getRandomPassword(8)
    dataToSend = dict({"login": counterusernameforqb, "password": counterpasswordforqb,
                       "full_name": newdoctor.name, "phone": newdoctor.phone,  # "tags":skill_req,
                       })

    qbsessiontoken = Platform.utils.getQBAppSessionToken(True)

    logmessage.append('createDoctor;counter data to QB:%s' % (json.dumps(dataToSend)))
    # requestlogger.info('createDoctor;data to QB:%s'%(json.dumps(dataToSend)))
    req = requests.post(config.QB_USER_API, data=json.dumps({"user": dataToSend}),
                        headers={'Content-Type': 'application/json',
                                 'QuickBlox-REST-API-Version': '0.1.0',
                                 'QB-Token': qbsessiontoken})

    # try with uncached new token
    if (req.status_code != 201):
        qbsessiontoken = Platform.utils.getQBAppSessionToken(False)
        logmessage.append(
            '*****alert**alert****cached token didnt work*****alert**alert****,created new:' + qbsessiontoken)
        # requestlogger.info('*****alert**alert****cached token didnt work*****alert**alert****')
        req = requests.post(config.QB_USER_API, data=json.dumps({"user": dataToSend}),
                            headers={'Content-Type': 'application/json',
                                     'QuickBlox-REST-API-Version': '0.1.0',
                                     'QB-Token': qbsessiontoken})

    if (req.status_code != 201):
        # requestlogger.info('*****alert**alert***error creating qb user******')
        # requestlogger.info(req.content)
        backendlogger.error('createDoctor', data, 'Error creatinng counter QB user %s' % (req.content), logmessage)
        return HttpResponse('Error creating counter QB Person:%r' % (req.content), status=403)

    response_data = json.loads(req.content)
    newdoctor.counterqbid = response_data["user"]["id"]
    newdoctor.counterqbusername = counterusernameforqb
    newdoctor.counterqbpassword = counterpasswordforqb
    newdoctor.save()

    Doctorgroups = []
    for group in djangouser.groups.all():
        Doctorgroups.append(group.name)

    jsondata = dict({ServerConstants.DOCTORUID: newdoctor.doctoruid,
                     ServerConstants.DOCTORPHONE: newdoctor.phone,  # tests need phone and passwrd
                     ServerConstants.DOCTORPASSWORD: passwordforDoctor,
                     ServerConstants.QBID: newdoctor.qbid,
                     ServerConstants.QBUSERNAME: newdoctor.qbusername,
                     ServerConstants.QBPASSWORD: newdoctor.qbpassword,
                     ServerConstants.DOCTORGROUPS: ",".join(Doctorgroups),
                     })

    # send sms
    # gcm.sendGCMToSendSMSToDoctors(newdoctor.doctoruid,"You have been registered, your login code is "+djangouser.password)
    message = ''.join(
        ['You have been registered at ', partner_req, '. Please use the password ', passwordforDoctor, ' to login'])
    Communications.utils.sendSMS(message, newdoctor.phone, None, None)

    httpoutput = utils.successJson(jsondata)
    backendlogger.info('createDoctor', data, httpoutput, logmessage)
    # requestlogger.info('createDoctorResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


def applydefaultservicesbyskillstr(skillarray_req):
    default = {
        "0": "1",  # gp
        "1": "2,3,4",  # nurse
        "2": "5,6,7",  # caretaker
        "3": "8",  # physio
        "4": "9",  # yoga
        "5": "1",  # ayurvedic
        "6": "1",  # cardio
        "7": "1",  # gynaec
        "8": "1",  # psych
        "9": "1",  # ayurvedic
        "10": "1",  # cardio
        "11": "1",  # gynaec
        "12": "1",  # psych
        "13": "1",  # ayurvedic
        "14": "1",  # cardio
        "15": "1",  # gynaec
        "16": "1",  # psych
        "17": "1",  # ayurvedic
        "18": "1",  # cardio
        "19": "1",  # gynaec
        "20": "1",  # psych
        "21": "1",  # ayurvedic
        "22": "1",  # cardio
        "23": "1",  # gynaec
        "24": "1",  # psych
        "25": "1",  # ayurvedic
        "26": "1",  # cardio
        "27": "1",  # gynaec

        "51": "1",  # gynaec
        "52": "1",  # psych
        "53": "1",  # ayurvedi
        "54": "1",  # cardio
        "55": "1",  # gynaec
        "56": "1",  # psych
        "57": "1",  # ayurvedic
        "58": "1",  # cardio

        "71": "20",

        "999": "1",  # demo doctor

    }
    servicesoffered = ""
    for skill in skillarray_req:
        try:
            if servicesoffered == "":
                servicesoffered = default[skill]
            else:
                servicesoffered = servicesoffered + "," + default[skill]
        except KeyError:
            pass

    return servicesoffered


@csrf_exempt
def updateDoctor(request):
    # requestlogger.info('================updateDoctor=====================')
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated: return HttpResponse("Error authenticating user", status=401)

    data = json.loads(request.body)
    logmessage = []
    logmessage.append('updateDoctorRequest;%s' % (str(data)))
    # requestlogger.info('updateDoctorRequest;%s'%(str(data)))

    doctoruid_req = data[ServerConstants.DOCTORUID]
    name_req = data[ServerConstants.DOCTORNAME]
    gender_req = data[ServerConstants.DOCTORGENDER]
    skillarray_req = data[ServerConstants.SKILLARRAY]

    try:
        yob_req = data[ServerConstants.YEAROFBIRTH]
    except KeyError:
        yob_req = None

    try:
        email_req = data[ServerConstants.EMAIL]
    except KeyError:
        email_req = ""

    try:
        registrationnum_req = data[ServerConstants.REGISTRATIONNUM]
    except KeyError:
        registrationnum_req = ""

    try:
        specialization_req = data[ServerConstants.SPECIALIZATION]
    except KeyError:
        specialization_req = ""

    try:
        qualification_req = data[ServerConstants.QUALIFICATION]
    except KeyError:
        qualification_req = ""

    try:
        experience_req = data[ServerConstants.EXPERIENCE]
    except KeyError:
        experience_req = ""

    try:
        biosketch_req = data[ServerConstants.DOCTORBIOSKETCH]
    except KeyError:
        biosketch_req = ""

    try:
        isactive_req = data[ServerConstants.ISACTIVE]
    except KeyError:
        isactive_req = True

    try:
        cityname_req = data[ServerConstants.CITYNAME]
    except KeyError:
        cityname_req = ""

    olddoctor = DoctorDetails.objects.get(doctoruid=doctoruid_req)

    olddoctor.name = name_req
    olddoctor.email = email_req
    olddoctor.gender = gender_req
    olddoctor.yearofbirth = yob_req
    olddoctor.registrationnum = registrationnum_req
    olddoctor.specialization = specialization_req
    olddoctor.qualification = qualification_req
    olddoctor.experience = experience_req
    olddoctor.biosketch = biosketch_req
    olddoctor.city = cityname_req
    olddoctor.isactive = isactive_req

    # skillset
    olddoctor.skills.clear()
    for skillnum in skillarray_req:
        olddoctor.skills.add(SkillNumToGroup.objects.get(skillnum=skillnum))

    olddoctor.save()

    # #update groups for this doctor..so remove all grps first and then add by skillstr
    # djangouser = User.objects.get(username="doctor"+olddoctor.phone)
    # djangouser.groups.clear()
    #
    # #add to Doctorgroup
    # g = Group.objects.get(name='doctorgroup')
    # g.user_set.add(djangouser)
    #
    # #is doctor? add to doctor group..etc
    # skills = skill_req.split(',')
    # for skill in skills:
    #     groupnameforthisskill = SkillNumToGroup.objects.get(skillnum = int(skill)).groupname
    #     g = Group.objects.get(name=groupnameforthisskill)
    #     g.user_set.add(djangouser)

    httpoutput = utils.successJson(dict({ServerConstants.DOCTORUID: doctoruid_req, }))
    backendlogger.info('updateDoctorResponse', data, httpoutput, logmessage)
    # requestlogger.info('updateDoctorResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
########## modificaitons by heet
def getAllDoctorList(request):
    data = json.loads(request.body)
    logmessage = []

    try:
        appstr_req = data[ServerConstants.APPSTR]
        if appstr_req != "":
            apparray = appstr_req.split(',')
        else:
            backendlogger.error('getAllDoctorList', data, 'Empty WebApp Str ', logmessage)
            return HttpResponse('Empty WebApp Str', status=403)
    except KeyError:
        backendlogger.error('getAllDoctorList', data, 'Appstr not sent/valid ', logmessage)
        return HttpResponse('Appstr not sent/valid', status=403)

    appdocmappingsforthisappid = DoctorAppMapping.objects.filter(appdetails__appuid__in=apparray)

    showhidden_req = False
    try:
        showhidden_req = data[ServerConstants.SHOWHIDDEN]
    except KeyError:
        pass

    if not showhidden_req:
        appdocmappingsforthisappid = appdocmappingsforthisappid.exclude(consultation_type=DoctorAppMapping.HIDDEN)

    doctorList = list()

    for thismapping in appdocmappingsforthisappid:
        try:
            host = thismapping.appdetails.apptemplatedetails.host
        except:
            host = None
        doctorList.append({
            ServerConstants.DOCTORUID: thismapping.doctor.doctoruid,
            ServerConstants.DOCTORNAME: thismapping.doctor.name,
            ServerConstants.DOCTORPHONE: thismapping.doctor.phone,
            ServerConstants.DOCTORGENDER: thismapping.doctor.gender,
            ServerConstants.EXPERIENCE: thismapping.doctor.experience,
            ServerConstants.PROFILEPIC: thismapping.doctor.profilepic,
            ServerConstants.REGISTRATIONNUM: thismapping.doctor.registrationnum,
            ServerConstants.SPECIALIZATION: thismapping.doctor.specialization,
            ServerConstants.QUALIFICATION: thismapping.doctor.qualification,
            ServerConstants.APPUID: thismapping.appdetails.appuid,
            ServerConstants.APPTAG: thismapping.appdetails.apptag,
            ServerConstants.HOST: host,
            ServerConstants.CONSULTATIONTYPESTR: thismapping.get_consultation_type_display(),
        })

    jsondata = dict({ServerConstants.DOCTORLIST: doctorList})
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getAllDoctorListResponse', data, httpoutput, logmessage)
    # requestlogger.info('getAllDoctorListResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def deleteDocumentPicture(request):
    # requestlogger.info('================deleteDocumentPicture=====================')
    data = json.loads(request.body)
    logmessage = []
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('deleteDocumentPicture', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    logmessage.append('deleteDocumentPicture;%s' % (str(data)))
    # requestlogger.info('deleteDocumentPicture;%s'%(str(data)))

    doctoruid_req = data[ServerConstants.DOCTORUID]
    docurl_req = data[ServerConstants.DOCURL]
    docdiskpath = docurl_req.replace(''.join([config.DOMAIN, settings.STATIC_URL]), "")
    fulldocdiskpath = ''.join([settings.STATICFILES_DIRS[0], '/', docdiskpath])

    try:
        doctor = DoctorDetails.objects.get(doctoruid=doctoruid_req)
        Doctordocs = doctor.docs.split(',')
        if docurl_req in Doctordocs:
            Doctordocs.remove(docurl_req)
            if os.path.isfile(fulldocdiskpath):
                os.remove(fulldocdiskpath)
            doctor.docs = ','.join(Doctordocs)
            doctor.save()
        else:
            httpresonse = utils.errorJson("Document not found")
            return HttpResponse(httpresonse, content_type=ServerConstants.RESPONSE_JSON_TYPE)

    except ObjectDoesNotExist:
        backendlogger.error('deleteDocumentPicture', data, 'Doctor not found', logmessage)
        httpresonse = utils.errorJson("Doctor not found")
        return HttpResponse(httpresonse, content_type=ServerConstants.RESPONSE_JSON_TYPE)

    httpoutput = utils.successJson(dict())
    backendlogger.info('deleteDocumentPicture', data, httpoutput, logmessage)
    # requestlogger.info('deleteDocumentPictureResponseDoctor;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def getDoctorDetails(request):
    # requestlogger.info('================getDoctorDetails=====================')
    data = json.loads(request.body)
    logmessage = []
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('deleteDocumentPicture', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        doctoruid_req = data[ServerConstants.DOCTORUID]
        try:
            doctor = DoctorDetails.objects.get(doctoruid=doctoruid_req)
        except ObjectDoesNotExist:
            backendlogger.error('getDoctorDetails', data, 'doctor for uid:' + doctoruid_req +" not found",
                                logmessage)
            return HttpResponse("doctor for uid not found", status=400)
    except KeyError:
        try:
            doctorphone_req = data[ServerConstants.PHONE]
            try:
                doctor = DoctorDetails.objects.get(phone=doctorphone_req)
            except ObjectDoesNotExist:
                backendlogger.error('getDoctorDetails', data, 'doctor for phone:' + doctorphone_req + " not found",
                                    logmessage)
                return HttpResponse("doctor for phone not found", status=400)
        except Exception as e:
            backendlogger.error('getDoctorDetails', data, str(e), logmessage)
            return HttpResponse("send doctor phone/uid", status=400)


    skilllist = doctor.skillstr.split(',')

    categoryList = ""
    for skill in skilllist:
        try:
            thisskill = SkillNumToGroup.objects.get(skillnum=skill)
        except ObjectDoesNotExist:
            backendlogger.error('getAllDoctorListRequest', data, 'skill number ' + skill + ' not mapped to group',
                                logmessage)
            return HttpResponse("skill number not mapped to group", status=403)

        categoryList = categoryList + " " + thisskill.categoryname + ","

    jsondata = dict({ServerConstants.DOCTORUID: doctor.doctoruid,
                     ServerConstants.DOCTORNAME: doctor.name,
                     ServerConstants.DOCTORPHONE: doctor.phone,
                     ServerConstants.EMAIL: doctor.email,
                     ServerConstants.SKILLSTR: doctor.skillstr,
                     ServerConstants.SKILLNAMESTR: categoryList,
                     ServerConstants.DOCTORGENDER: doctor.gender,
                     ServerConstants.QBUSERNAME: doctor.qbusername,
                     ServerConstants.QBPASSWORD: doctor.qbpassword,
                     ServerConstants.QBID: doctor.qbid,
                     ServerConstants.REGISTRATIONNUM: doctor.registrationnum,
                     ServerConstants.SPECIALIZATION: doctor.specialization,
                     ServerConstants.QUALIFICATION: doctor.qualification,
                     ServerConstants.CITYNAME: doctor.city,
                     ServerConstants.PROFILEPIC: doctor.profilepic,
                     ServerConstants.SIGNPIC: doctor.signpic,
                     ServerConstants.DOCTORDOCLIST: doctor.docs,
                     ServerConstants.EXPERIENCE: doctor.experience,
                     ServerConstants.YEAROFBIRTH: doctor.yearofbirth,
                     ServerConstants.DOCTORBIOSKETCH: doctor.biosketch,
                     ServerConstants.SERVICESOFFERED: doctor.servicesoffered,
                     ServerConstants.ISACTIVE: doctor.isactive,
                     })

    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getDoctorDetails', data, httpoutput, logmessage)
    # requestlogger.info('getDoctorDetailsResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def searchDoctors(request):
    # requestlogger.info('================searchDoctors=====================')

    data = json.loads(request.body)
    logmessage = []
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('searchDoctors', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    logmessage.append('searchDoctorsRequest:%s' % str(data))
    # requestlogger.info('searchDoctorsRequest:%s',str(data))
    searchstr_req = data[ServerConstants.SEARCHSTR]
    searchfield_req = data[ServerConstants.SEARCHFIELD]

    matchingdoctors = list()

    if (searchfield_req == ServerConstants.DOCTORUID):
        matchingdoctors = DoctorDetails.objects.filter(doctoruid=searchstr_req)

    if (searchfield_req == ServerConstants.EMAIL):
        matchingdoctors = DoctorDetails.objects.filter(email=searchstr_req)

    if (searchfield_req == ServerConstants.PHONE):
        matchingdoctors = DoctorDetails.objects.filter(phone__contains=searchstr_req)

    if (searchfield_req == ServerConstants.DOCTORNAME):
        matchingdoctors = DoctorDetails.objects.filter(name__contains=searchstr_req)

    if (searchfield_req == ServerConstants.DOCTORGENDER):
        matchingdoctors = DoctorDetails.objects.filter(gender__contains=searchstr_req)

    if (searchfield_req == ServerConstants.SPECIALIZATION):
        matchingdoctors = DoctorDetails.objects.filter(specialization__contains=searchstr_req)

    if (searchfield_req == ServerConstants.QUALIFICATION):
        matchingdoctors = DoctorDetails.objects.filter(qualification__contains=searchstr_req)

    count = 0
    doctorList = list()
    for doctor in matchingdoctors:
        count = count + 1
        doctorList.append({
            ServerConstants.DOCTORUID: doctor.doctoruid,
            ServerConstants.DOCTORNAME: doctor.name,
            ServerConstants.PHONE: doctor.phone,
            ServerConstants.EMAIL: doctor.email,
            ServerConstants.SKILLSTR: doctor.skillstr,
            ServerConstants.DOCTORNOTES: doctor.notes,
            ServerConstants.DOCTORGENDER: doctor.gender,
            ServerConstants.SPECIALIZATION: doctor.specialization,
            ServerConstants.QUALIFICATION: doctor.qualification,
            ServerConstants.EXPERIENCE: doctor.experience,
            ServerConstants.YEAROFBIRTH: doctor.yearofbirth,
            ServerConstants.ISFORHOMECARE: doctor.isforhomecare,
            ServerConstants.ISFORTELEMEDICINE: doctor.isfortelemedicine,
            ServerConstants.PROFILEPIC: doctor.profilepic,
            ServerConstants.DOCTORDOCLIST: doctor.docs,
        })
        if count >= 10:
            break

    jsondata = dict({ServerConstants.DOCTORLIST: doctorList})
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('searchDoctors', data, httpoutput, logmessage)
    # requestlogger.info('searchDoctorsResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def getServicesOfferedByDoctor(request):
    # requestlogger.info('================getServicesOfferedByDoctor=====================')
    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getServicesOfferedByDoctor', logrequest, "Error authenticating user", logmessage)
        return HttpResponse("Error authenticating user", status=401)

    doctoruid_req = request.GET[ServerConstants.DOCTORUID]

    logmessage.append('getServicesOfferedByDoctor;doctoruid%s' % (str(doctoruid_req)))
    # requestlogger.info('getServicesOfferedByDoctor;doctoruid%s'%(str(doctoruid_req)))
    doctor = DoctorDetails.objects.get(doctoruid=doctoruid_req)
    serviceList = convertServicesOfferStrToList(doctor.servicesoffered)
    jsondata = dict({ServerConstants.SERVICELIST: serviceList})
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getServicesOfferedByDoctor', logrequest, httpoutput, logmessage)
    # requestlogger.info('getServicesOfferedByDoctor;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


def convertServicesOfferStrToList(servicesstr):
    servicesList = list()
    servicesstrsplit = servicesstr.split(',')
    for serviceid in servicesstrsplit:
        try:
            thisservice = Services.objects.get(serviceid=serviceid)
            servicesList.append({
                ServerConstants.SERVICEID: thisservice.serviceid,
                ServerConstants.SERVICETITLE: thisservice.servicetitle,
                ServerConstants.SERVICETAG: thisservice.servicetag,
                ServerConstants.SERVICEDESCRIPTION: thisservice.servicedescription,
                ServerConstants.SERVICECHARGES: thisservice.servicecharges,
            })
        except ObjectDoesNotExist:
            pass
    return servicesList


@csrf_exempt
def getOnlineDoctors(request):
    # requestlogger.info('================getOnlineDoctorsForApp=====================')
    authenticated, djangouserstr = authenticateURL(request)
    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value
    if not authenticated:
        backendlogger.error('getOnlineDoctorsForApp', logrequest, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        appuid = request.GET[ServerConstants.APPUID]
    except KeyError:
        appuid = None

    try:
        skillstr = str(request.GET[ServerConstants.SKILLSTR])
    except Exception as e:
        skillstr = None

    # finding doctors in kiosk mapping table as per appid
    doctorgroup = Group.objects.get(name="doctorgroup")
    relevantdoctors = list()

    # here we look for online status of doctors which  are mapped to that kiosk
    # if no doctors mapped then exception thrown and code in ObjectDoesNotExist execeuted
    kioskdocmappingsforthisappid = DoctorAppMapping.objects.filter(appdetails__appuid=appuid)
    if len(kioskdocmappingsforthisappid) > 0:
        for thismapping in kioskdocmappingsforthisappid:
            relevantdoctors.append(thismapping.doctor.djangouser)
    else:
        # here we check for ol status of all doctors except ones which are mapped
        alldockioskmappings = DoctorAppMapping.objects.all()
        irrelevantdoctorsdjangousersid = list()
        for thismapping in alldockioskmappings:
            irrelevantdoctorsdjangousersid.append(thismapping.doctor.djangouser.id)

        relevantdoctors = doctorgroup.user_set.exclude(id__in=irrelevantdoctorsdjangousersid)
        # relevant doctors = alldoctors - mapped doctors

    # get all online doctors for now:
    # doctorgroup = Group.objects.get(name="doctorgroup")
    # alldoctors = doctorgroup.user_set.all()
    onlinedoctorslist = list()

    QB_session_token = Platform.utils.getQBAppSessionToken(False)

    logmessage.append('QB_session_token created ;%s' % (str(QB_session_token)))
    # requestlogger.info('QB_session_token created ;%s'%(str(QB_session_token)))

    livesessions = Session.objects.filter(expire_date__gt=timezone.now())
    uid_list = []
    # Build a list of user ids from that query
    for session in livesessions:
        data = session.get_decoded()
        uid_list.append(data.get('_auth_user_id', None))

    count = 0
    for doctor in relevantdoctors:
        if str(doctor.id) in uid_list:
            # Made by Arnav . Unicode to Str
            doctor = DoctorDetails.objects.get(djangouser=doctor)
            # requests.get(url,input)
            logmessage.append(''.join(['doctoruid:', str(doctor.doctoruid), ' is authenticated(loggedin)']))
            # requestlogger.info(''.join(['doctoruid:',str(doctor.doctoruid),' is authenticated(loggedin)']))
            if doctor.qbid == None or not doctor.isactive:
                continue

            doctoruid = doctor.doctoruid

            cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
            status = cache.get("onlinestatusdoctor_" + str(doctoruid))
            if status is None:
                logmessage.append("not found in cache, doctor is offline")
                continue

            skillArr = doctor.skillstr.split(',')
            if skillstr != None:
                if skillstr in skillArr:
                    onlinedoctorslist.append({
                        ServerConstants.DOCTORNAME: doctor.name,
                        ServerConstants.DOCTORUID: doctor.doctoruid,
                        ServerConstants.SKILLSTR: doctor.skillstr,
                        ServerConstants.PROFILEPIC: doctor.profilepic,
                        ServerConstants.QBID: doctor.qbid,
                        ServerConstants.REGISTRATIONNUM: doctor.registrationnum,
                        ServerConstants.SPECIALIZATION: doctor.specialization,
                        ServerConstants.QUALIFICATION: doctor.qualification,
                        ServerConstants.EXPERIENCE: doctor.experience,

                    })
            else:
                onlinedoctorslist.append({
                    ServerConstants.DOCTORNAME: doctor.name,
                    ServerConstants.DOCTORUID: doctor.doctoruid,
                    ServerConstants.SKILLSTR: doctor.skillstr,
                    ServerConstants.PROFILEPIC: doctor.profilepic,
                    ServerConstants.QBID: doctor.qbid,
                    ServerConstants.REGISTRATIONNUM: doctor.registrationnum,
                    ServerConstants.SPECIALIZATION: doctor.specialization,
                    ServerConstants.QUALIFICATION: doctor.qualification,
                    ServerConstants.EXPERIENCE: doctor.experience,

                })

    jsondata = dict({ServerConstants.DOCTORLIST: onlinedoctorslist})
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getOnlineDoctorsForApp', logrequest, httpoutput, logmessage)
    # requestlogger.info('searchDoctorsResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def getOnlineDoctors(request):
    authenticated, djangouserstr = authenticateURL(request)
    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value
    if not authenticated:
        backendlogger.error('getOnlineDoctors', logrequest, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        appuid = request.GET[ServerConstants.APPUID]
    except KeyError:
        appuid = None

    try:
        consultation_type = request.GET[ServerConstants.CONSULTATIONTYPE]
    except KeyError:
        consultation_type = None

    # finding doctors in kiosk mapping table as per appid
    relevantdoctors = list()

    kwargs = dict()
    if appuid:
        kwargs["appdetails__appuid"] = appuid
    if consultation_type:
        kwargs["consultation_type"] = consultation_type
    appdoctormappingforthisappid = DoctorAppMapping.objects.filter(**kwargs)
    if len(appdoctormappingforthisappid) > 0:
        requestlogger.info("got relevant docs")
        for thismapping in appdoctormappingforthisappid:
            relevantdoctors.append(thismapping.doctor.djangouser)

    onlinedoctorslist = list()
    cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})

    for doctor in relevantdoctors:
        # check if this doctors djanoguser is online in django session or has app token i.e. online from app
        doctor = DoctorDetails.objects.get(djangouser=doctor)
        uniqueid_str = cache.get("onlinestatusdoctor_" + str(doctor.doctoruid))
        if uniqueid_str != None and uniqueid_str != "":
            # requests.get(url,input)
            logmessage.append(''.join(['doctoruid:', str(doctor.doctoruid), ' is authenticated(loggedin)']))
            # requestlogger.info(''.join(['doctoruid:',str(doctor.doctoruid),' is authenticated(loggedin)']))
            if doctor.qbid == None or not doctor.isactive:
                continue

            doctorisbusy = cache.get("onlinestatusdoctor_" + str(doctor.doctoruid) + "_busy")
            if doctorisbusy == 1:
                logmessage.append("busy status found in cache, doctor is busy")
                # we still publish busy doctors but with isbusy tag
                doctorisbusy = True
            else:
                doctorisbusy = False

            start = datetime.today().replace(hour=00, minute=00, second=00)
            # end = person.created_at
            end = datetime.today().replace(hour=23, minute=59, second=59)
            todays_list_count = WaitingQueue.objects.filter(doctor=doctor, appdetails=None,
                                                            created_at__range=(start, end)).count()

            existing_queue_number = calc_queue_number(todays_list_count, "doctor" + str(doctor.doctoruid) + "q")
            queue_number = existing_queue_number + 1
            onlinedoctorslist.append({
                ServerConstants.DOCTORNAME: doctor.name,
                ServerConstants.DOCTORUID: doctor.doctoruid,
                ServerConstants.SKILLSTR: doctor.skillstr,
                ServerConstants.PROFILEPIC: doctor.profilepic,
                ServerConstants.QBID: doctor.qbid,
                ServerConstants.REGISTRATIONNUM: doctor.registrationnum,
                ServerConstants.SPECIALIZATION: doctor.specialization,
                ServerConstants.QUALIFICATION: doctor.qualification,
                ServerConstants.EXPERIENCE: doctor.experience,
                ServerConstants.SIGNPIC: doctor.signpic,
                ServerConstants.ISBUSY: doctorisbusy,
                ServerConstants.ISONLINE: True,
                ServerConstants.QUEUE_NUMBER: queue_number,
            })

    jsondata = dict({ServerConstants.DOCTORLIST: onlinedoctorslist})
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getOnlineDoctors', logrequest, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


"""
input: 0 logout ,1 available,2 busy after being available,3 available after being busy
+input: a  uniqueid string from each device
"""


@csrf_exempt
def setOnlineStatusForDoctor(request):
    # requestlogger.info('================getNearbyOnlineDoctors=====================')
    data = json.loads(request.body)
    logmessage = []
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('setOnlineStatusForDoctorNew', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    logmessage.append('setOnlineStatusForDoctorNew;%s' % (str(data)))
    # requestlogger.info('setOnlineStatusForDoctor;%s'%(str(data)))

    doctoruid_req = data[ServerConstants.DOCTORUID]
    status_req = data[ServerConstants.ONLINESTATUS]
    uniqueid_req = data[ServerConstants.UNIQUEID]


    cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
    uniqueid_str = cache.get(
        "onlinestatusdoctor_" + str(doctoruid_req))  # a comma separated list of uniqueids of online devices

    logmessage.append("doc online status in cache:%s" % (str(uniqueid_str)))

    if status_req == 1:
        # check if token already added comma separated list
        if uniqueid_str == None:
            uniqueid_str = uniqueid_req
        elif uniqueid_req not in uniqueid_str:
            uniqueid_str = ''.join([uniqueid_str, ',', uniqueid_req])
    elif status_req == 0 and uniqueid_str is not None:
        # logout request so remove that device unique id from list and write rest again in cache
        uniqueid_list = uniqueid_str.split(',')
        if uniqueid_req in uniqueid_list: uniqueid_list.remove(uniqueid_req)
        uniqueid_str = ','.join(uniqueid_list)  # this becomes empty string if last one removed
    elif status_req == 2:
        # busy after being online
        cache.set("onlinestatusdoctor_" + str(doctoruid_req) + "_busy", 1, 30)  # this is updated by VCConsumer socket
    elif status_req == 3:
        # again online after being busy
        cache.delete("onlinestatusdoctor_" + str(doctoruid_req) + "_busy")

    logmessage.append("updating doc %s online status to %s" % (str(doctoruid_req), uniqueid_str))
    requestlogger.info("updating doc %s online status to %s" % (str(doctoruid_req), uniqueid_str))

    isonline = False
    if uniqueid_str != None and uniqueid_str != "":
        cache.set("onlinestatusdoctor_" + str(doctoruid_req), uniqueid_str, 60 * 2)  # 2 min expiry
        isonline = True
    else:
        cache.delete("onlinestatusdoctor_" + str(doctoruid_req))
        cache.delete("onlinestatusdoctor_" + str(doctoruid_req) + "_busy")

    isbusy = cache.get("onlinestatusdoctor_" + str(doctoruid_req) + "_busy")
    if isbusy == 1:
        logmessage.append("busy status found in cache, doctor is busy")
        # we still publish busy doctors but with isbusy tag
        isbusy = True
    else:
        isbusy = False

    jsondata = dict({ServerConstants.ISONLINE: isonline,
                     ServerConstants.ISBUSY: isbusy})
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('setOnlineStatusForDoctorNew', data, httpoutput, logmessage)
    # requestlogger.info('setOnlineStatusForDoctor success')
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def getDoctorsOfCategory(request):
    # requestlogger.info('================getDoctorsOfCategory=====================')
    # authenticated,djangouserstr = authenticateURL(request)
    # if not authenticated:return HttpResponse("Error authenticating user", status=401)
    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value
    try:
        doctorskill_req = request.GET[ServerConstants.SKILLSTR]
    except:
        return HttpResponse("Select Doctor Category", status=400)
    try:
        offset_req = request.GET[ServerConstants.OFFSET]
    except KeyError:
        offset_req = 0
    try:
        limit_req = request.GET[ServerConstants.LIMIT]
    except KeyError:
        limit_req = None

    logmessage.append('getDoctorsOfCategory;doctorskill:%s' % (doctorskill_req))
    # requestlogger.info('getDoctorsOfCategory;doctorskill:%s'%(doctorskill_req))

    doctorList = list()
    alldoctors = DoctorDetails.objects.all()
    count = 0
    for thisdoctor in alldoctors:
        if not thisdoctor.isactive:
            continue

        skilllist = thisdoctor.skillstr.split(',')
        if doctorskill_req not in skilllist:
            continue

        count = count + 1
        if (limit_req != None):
            # offset starts from 0
            if (count - 1 < int(offset_req) or count > int(limit_req) + int(offset_req)):
                continue

        doctorList.append({
            ServerConstants.DOCTORNAME: thisdoctor.name,
            ServerConstants.DOCTORUID: thisdoctor.doctoruid,
            ServerConstants.DOCTORPHONE: thisdoctor.phone,
            ServerConstants.SKILLSTR: thisdoctor.skillstr,
            ServerConstants.PROFILEPIC: thisdoctor.profilepic,
            ServerConstants.QBID: thisdoctor.qbid,
            ServerConstants.REGISTRATIONNUM: thisdoctor.registrationnum,
            ServerConstants.SPECIALIZATION: thisdoctor.specialization,
            ServerConstants.QUALIFICATION: thisdoctor.qualification,
            ServerConstants.EXPERIENCE: thisdoctor.experience,
            ServerConstants.CITYNAME: thisdoctor.city,
            ServerConstants.HOSPITALNAME: thisdoctor.hospitalname,
        })

    # serializers = MySerialiser()
    # jsondata = serializers.serialize(bookingList)
    # jsondata = json.dumps(bookingList)
    jsondata = dict({ServerConstants.DOCTORLIST: doctorList,
                     ServerConstants.SKILLSTR: doctorskill_req})
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getDoctorsOfCategory', logrequest, httpoutput, logmessage)
    # requestlogger.info('getDoctorsOfCategoryResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def getDoctorWeeklyCalender(request):
    # requestlogger.info('================getDoctorWeeklyCalender=====================')
    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getDoctorWeeklyCalender', logrequest, "Error authenticating user", logmessage)
        return HttpResponse("Error authenticating user", status=401)

    doctoruid_req = None
    doctorphone_req = None

    try:
        doctoruid_req = request.GET[ServerConstants.DOCTORUID]
    except KeyError:
        try:
            doctorphone_req = request.GET[ServerConstants.DOCTORPHONE]
        except KeyError:
            backendlogger.error('getDoctorWeeklyCalender', logrequest, "Provide doctoruid or phone", logmessage)
            return HttpResponse("Provide doctoruid or phone", status=400)

    doctor = None

    if (doctoruid_req != None):
        try:
            doctor = DoctorDetails.objects.get(doctoruid=doctoruid_req)
            doctorcalender = DoctorWeeklyCalender.objects.filter(doctor=doctor)
        except ObjectDoesNotExist:
            return HttpResponse("doctoruid not found in calender", status=404)
    elif (doctorphone_req != None):
        try:
            doctor = DoctorDetails.objects.get(phone=doctorphone_req)
            doctorcalender = DoctorWeeklyCalender.objects.filter(doctor=doctor)
        except ObjectDoesNotExist:
            return HttpResponse("doctorphone not found in calender", status=404)

    dayList = list()
    for day in doctorcalender:
        dayList.append({
            ServerConstants.DAYOFWEEK: day.dayofweek,
            ServerConstants.AVAILABILITYSTART: day.availabilitystart,
            ServerConstants.AVAILABILITYEND: day.availabilityend
        })

    jsondata = dict({ServerConstants.WEEKLYCALENDER: dayList,
                     ServerConstants.DOCTORNAME: doctor.name,
                     ServerConstants.SKILLSTR: doctor.skillstr,
                     ServerConstants.PROFILEPIC: doctor.profilepic,
                     ServerConstants.SPECIALIZATION: doctor.specialization,
                     ServerConstants.QUALIFICATION: doctor.qualification,
                     ServerConstants.EXPERIENCE: doctor.experience,
                     ServerConstants.DOCTORUID: doctor.doctoruid
                     })

    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getDoctorWeeklyCalender', logrequest, httpoutput, logmessage)
    # requestlogger.info('getDoctorWeeklyCalenderResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def getDoctorsForApp(request):
    # requestlogger.info('================getDoctorsForAdmin=====================')
    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getDoctorsForApp', logrequest, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        appuid = request.GET[ServerConstants.APPUID]
    except KeyError:
        appuid = None

    try:
        consultation_type = request.GET[ServerConstants.CONSULTATIONTYPE]
    except KeyError:
        consultation_type = DoctorAppMapping.TELEMEDICINE

    # finding doctors in kiosk mapping table as per appid
    relevantdoctors = list()
    kwargs = {}
    if appuid:
        kwargs["appdetails__appuid"] = appuid
    if consultation_type:
        kwargs["consultation_type"] = consultation_type
    kioskdocmappingsforthisappid = DoctorAppMapping.objects.filter(**kwargs)

    if len(kioskdocmappingsforthisappid) > 0:
        for thismapping in kioskdocmappingsforthisappid:
            relevantdoctors.append({
                ServerConstants.DOCTORNAME: thismapping.doctor.name,
                ServerConstants.DOCTORUID: thismapping.doctor.doctoruid,
                ServerConstants.SKILLSTR: thismapping.doctor.skillstr,
                ServerConstants.PROFILEPIC: thismapping.doctor.profilepic,
                ServerConstants.QBID: thismapping.doctor.qbid,
                ServerConstants.REGISTRATIONNUM: thismapping.doctor.registrationnum,
                ServerConstants.SPECIALIZATION: thismapping.doctor.specialization,
                ServerConstants.QUALIFICATION: thismapping.doctor.qualification,
                ServerConstants.EXPERIENCE: thismapping.doctor.experience,

            })

    jsondata = dict({ServerConstants.DOCTORLIST: relevantdoctors})
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getDoctorsForAdmin', logrequest, httpoutput, logmessage)
    # requestlogger.info('getDoctorsOfCategoryResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def getAllDoctorCategories(request):
    data = json.loads(request.body)
    logmessage = []
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getAllDoctorCategories', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    logmessage.append('getAllDoctorCategories;%s' % (str(data)))

    try:
        doctoruid_req = data[ServerConstants.DOCTORUID]
        doctor_req = DoctorDetails.objects.get(doctoruid=doctoruid_req)
    except:
        doctor_req = None

    allcategories = SkillNumToGroup.objects.all()

    categoryList = list()
    for thiscategory in allcategories:
        skillselected = False
        if doctor_req is not None and doctor_req.skills.filter(skillnum=thiscategory.skillnum).exists():
            skillselected = True
        categoryList.append({
            ServerConstants.SKILLNUM: thiscategory.skillnum,
            ServerConstants.CATEGORYNAME: thiscategory.categoryname,
            ServerConstants.CATEGORYDESCRIPTION: thiscategory.categorydescription.split(','),
            ServerConstants.SKILLSELECTED: skillselected
        })

    jsondata = dict({ServerConstants.CATEGORYLIST: categoryList})
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getAllDoctorCategories', data, httpoutput, logmessage)
    # requestlogger.info('getAllDoctorCategories;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
@transaction.atomic
def createApp(request):
    # requestlogger.info('===================createApp==================')

    data = json.loads(request.body)
    logmessage = []
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('createApp', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    logmessage.append('createApp;%s' % (str(data)))
    # requestlogger.info('createApp;%s'%(str(data)))
    app_config = os.path.join(settings.BASE_DIR + '/app-config.json')

    config_data = json.load(open(app_config))

    kwargs = {}
    try:
        appdeviceid_req = data[ServerConstants.DEVICEID]
        kwargs['appdeviceid'] = appdeviceid_req
    except KeyError:
        pass

    try:
        app_loc_req = data[ServerConstants.APPLOCATION]
        kwargs['applocation'] = app_loc_req
    except KeyError:
        pass

    try:
        app_type_req = data[ServerConstants.APPTYPE]
        kwargs['apptype'] = app_type_req
    except KeyError:
        pass

    try:
        app_desc_req = data[ServerConstants.APPDESCRIPTION]
        kwargs['appdescription'] = app_desc_req
    except KeyError:
        pass

    try:
        app_licensestartdate = data[ServerConstants.LICENSESTARTDATE]
        kwargs['licensestartdate'] = app_licensestartdate
    except KeyError:
        return HttpResponse("License start time not provided", status=403)

    try:
        app_licenseenddate = data[ServerConstants.LICENSEENDDATE]
        kwargs['licenseenddate'] = app_licenseenddate
    except KeyError:
        return HttpResponse("License end time not provided", status=403)

    try:
        license_enabled = data[ServerConstants.LICENSEENABLED]
        kwargs['licenseenabled'] = license_enabled
    except KeyError:
        kwargs['licenseenabled'] = False

    try:
        partner = data[ServerConstants.PARTNER]
        kwargs['partner'] = partner
    except KeyError:
        pass

    try:
        payu_auth = data[ServerConstants.PAYUAUTHTOKEN]
        kwargs['payuauthtoken'] = payu_auth
    except KeyError:
        pass

    try:
        newapp = AppDetails.objects.create(**kwargs)
    except Exception as e:
        backendlogger.error('createApp', data, str(e), logmessage)
        return HttpResponse("Error creating app", status=403)

    appid_str = str(newapp.appid)
    newapp.apptag = newapp.applocation + "-" + appid_str
    djangouser = User.objects.create_user(username="app" + appid_str, password="app" + appid_str,
                                          first_name="app", last_name=appid_str)
    newapp.djangouser = djangouser
    newapp.appuid = encode_id(newapp.appid)
    newapp.save()

    # add to app group
    g = Group.objects.get(name='appgroup')
    g.user_set.add(djangouser)

    # Loop over json file and save in database
    for key, value in config_data.items():
        AppConfig.objects.create(appdetails=newapp, key=key, value=value)

    appnameforqb = config.DOMAIN + "_app" + appid_str
    passwordforqb = config.DOMAIN + "_app" + appid_str
    full_name = config.DOMAIN + " app" + appid_str

    dataToSend = dict({"login": appnameforqb, "password": passwordforqb,
                       "full_name": full_name, "phone": "9769462" + appid_str,  # "tags":skill_req,
                       })

    qbsessiontoken = Platform.utils.getQBAppSessionToken(True)
    # checking wheather the api is called for running test case

    logmessage.append('createApp;data to QB:%s' % (json.dumps(dataToSend)))
    # requestlogger.info('createApp;data to QB:%s'%(json.dumps(dataToSend)))
    req = requests.post(config.QB_USER_API, data=json.dumps({"user": dataToSend}),
                        headers={'Content-Type': 'application/json',
                                 'QuickBlox-REST-API-Version': '0.1.0',
                                 'QB-Token': qbsessiontoken})

    # try with uncached new token
    if (req.status_code != 201):
        logmessage.append('*****alert**alert****cached token didnt work*****alert**alert****')
        # requestlogger.info('*****alert**alert****cached token didnt work*****alert**alert****')
        qbsessiontoken = Platform.utils.getQBAppSessionToken(False)
        req = requests.post(config.QB_USER_API, data=json.dumps({"user": dataToSend}),
                            headers={'Content-Type': 'application/json',
                                     'QuickBlox-REST-API-Version': '0.1.0',

                                     'QB-Token': qbsessiontoken})

    if (req.status_code != 201):
        # requestlogger.info('*****alert**alert***error creating qb user******')
        # requestlogger.info(req.content)
        backendlogger.error('createApp', data, '*****alert**alert***error creating qb user****** %s' % (req.content),
                            logmessage)
        raise Exception("Error creating QB Person:{}".format(req.content))

    try:
        response_data = json.loads(req.content)
        newapp.qbid = response_data["user"]["id"]
        newapp.qbusername = appnameforqb
        newapp.qbpassword = passwordforqb
        newapp.save()
    except Exception:
        # delete QB user and then again raise exception to revert
        response = requests.delete("http://api.quickblox.com/users/" + str(newapp.qbid) + ".json",
                                   headers={'Content-Type': 'application/json',
                                            'QuickBlox-REST-API-Version': '0.1.0',
                                            'QB-Token': qbsessiontoken})
        raise Exception("Error saving qbid to database")

    jsonObj = dict({ServerConstants.APPUID: newapp.appuid,
                    ServerConstants.DEVICEID: newapp.appdeviceid,
                    ServerConstants.QBID: newapp.qbid,
                    ServerConstants.QBUSERNAME: newapp.qbusername,
                    ServerConstants.QBPASSWORD: newapp.qbpassword,
                    })

    httpoutput = utils.successJson(jsonObj)
    backendlogger.info('createApp', data, httpoutput, logmessage)
    # requestlogger.info('createAppResponse;success')
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def updateAppConfig(request):
    # requestlogger.info('===================updateApp==================')
    data = json.loads(request.body)
    logmessage = []
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('updateApp', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        appuid_req = data[ServerConstants.APPUID]
    except KeyError:
        backendlogger.error('updateAppConfig', data, "PRovide app uid", logmessage)
        return HttpResponse("Please provide app uid ", status=403)

    try:
        paramname = data[ServerConstants.PARAMNAME]
    except KeyError:
        backendlogger.error('updateAppConfig', data, "PRovide paramname", logmessage)
        return HttpResponse("Please provide paramname ", status=403)

    try:
        newvalue = data[ServerConstants.NEWVALUE]
    except KeyError:
        backendlogger.error('updateAppConfig', data, "PRovide new value", logmessage)
        return HttpResponse("Please provide newvalue ", status=403)

    try:
        appdetails = AppDetails.objects.get(appuid=appuid_req)
    except ObjectDoesNotExist:
        backendlogger.error('updateAppConfig', data, "app not found for uid:"+appuid_req, logmessage)
        return HttpResponse("app not found for uid:"+appuid_req, status=404)

    try:
        appConfig = AppConfig.objects.get(appdetails=appdetails, key = paramname )
        if newvalue == "delete":
            logmessage.append("Deleting config:"+appConfig.key + " from appid" + str(appdetails.appid))
            appConfig.delete()
        else:
            logmessage.append("Updating config:" + appConfig.key + " in appid" + str(appdetails.appid) + "to "+ newvalue)
            appConfig.value = newvalue
            appConfig.save()
    except ObjectDoesNotExist:
        if newvalue != "delete":
            logmessage.append(
                "Creating config:" + paramname + " in appid" + str(appdetails.appid) + "with value: " + newvalue)
            AppConfig.objects.create(appdetails=appdetails, key=paramname, value=newvalue)
    except MultipleObjectsReturned:
        backendlogger.error('updateAppConfig', data, "multiple config returned for paramname:" + paramname, logmessage)
        return HttpResponse("multiple config returned for paramname:" + paramname, status=403)

    jsondata = dict({})

    httpoutput = utils.successJson(jsondata)
    backendlogger.info('updateAppDetails', data, httpoutput, logmessage)
    # requestlogger.info('updateAppDetailsResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE, status=200)


@csrf_exempt
def getAllApps(request):
    # requestlogger.info('===================updateApp==================')
    data = json.loads(request.body)
    logmessage = []
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getAllApps', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    apparray = None
    try:
        appstr_req = data[ServerConstants.APPSTR]
        if appstr_req != "":
            apparray = appstr_req.split(',')
        else:
            backendlogger.error('getAllApps', data, 'Empty WebApp Str ', logmessage)
            return HttpResponse('Empty WebApp Str', status=403)
    except KeyError:
        if not config.ISTESTMODE:
            try:
                # ensure only superadmin gets to list all apps
                djangouser = User.objects.get(username=djangouserstr)
                if (djangouser.groups.filter(name='superadmingroup').exists()):
                    pass
                else:
                    backendlogger.error('getAllApps', data, 'Access Denied, user not superadmin ', logmessage)
                    return HttpResponse('Access denied', status=403)
            except Exception as e:
                backendlogger.error('getAllApps', data, str(e), logmessage)
                return HttpResponse('Access denied', status=403)

    if not apparray:
        allApp = AppDetails.objects.all()
    else:
        allApp = AppDetails.objects.filter(appuid__in=apparray)

    appList = list()

    for app in allApp:
        try:
            apptemplate = app.apptemplatedetails
            shorturl = apptemplate.hostshortlink
        except ObjectDoesNotExist:
            shorturl = ""

        appList.append({
                        ServerConstants.APP_ID: app.appid,
                        ServerConstants.APPUID: app.appuid,
                        ServerConstants.QBID: app.qbid,
                        ServerConstants.QBUSERNAME: app.qbusername,
                        ServerConstants.APPLOCATION: app.applocation,
                        ServerConstants.APPTAG: app.apptag,
                        ServerConstants.APPTYPE: app.apptype,
                        ServerConstants.HOSTSHORTURL: shorturl,
                        ServerConstants.LICENSESTARTDATE: app.licensestartdate,
                        ServerConstants.LICENSEENDDATE: app.licenseenddate,
                        })

    jsondata = dict({ServerConstants.APPLIST: appList})
    httpoutput = utils.successJson(jsondata)
    backendlogger.error('getAllApps', data, httpoutput, logmessage)
    # requestlogger.info('getAllAppsResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def updateAppDetails(request):
    # requestlogger.info('================updateAppDetails=====================')
    data = json.loads(request.body)
    logmessage = []
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('updateAppDetails', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    logmessage.append('updateAppDetails;%s' % (str(data)))
    # requestlogger.info('updateAppDetails;%s'%(str(data)))

    try:
        appuid_req = data[ServerConstants.APPUID]
    except KeyError:
        return HttpResponse("Please provide appid ", status=403)

    try:
        appdetails = AppDetails.objects.get(appuid=appuid_req)
    except ObjectDoesNotExist:
        return HttpResponse("app not found", status=404)

    try:
        app_loc = data[ServerConstants.APPLOCATION]
        appdetails.applocation = app_loc
    except KeyError:
        pass

    try:
        app_tag = data[ServerConstants.APPTAG]
        appdetails.apptag = app_tag
    except KeyError:
        pass

    try:
        app_type = data[ServerConstants.APPTYPE]
        appdetails.apptype = app_type
    except KeyError:
        pass

    try:
        app_desc = data[ServerConstants.APPDESCRIPTION]
        appdetails.appdescription = app_desc
    except KeyError:
        pass

    try:
        app_licensestartdate = data[ServerConstants.LICENSESTARTDATE]
        appdetails.licensestartdate = app_licensestartdate
    except KeyError:
        pass

    try:
        app_licenseenddate = data[ServerConstants.LICENSEENDDATE]
        appdetails.licenseenddate = app_licenseenddate
    except KeyError:
        pass

    try:
        licenseenabled = data[ServerConstants.LICENSEENABLED]
        appdetails.licenseenabled = licenseenabled
    except KeyError:
        pass

    appdetails.save()

    jsondata = dict({ServerConstants.APPUID: appuid_req})

    httpoutput = utils.successJson(jsondata)
    backendlogger.info('updateAppDetails', data, httpoutput, logmessage)
    # requestlogger.info('updateAppDetailsResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE, status=200)


@csrf_exempt
def getAppDetails(request):
    # requestlogger.info('================updateAppDetails=====================')
    data = json.loads(request.body)
    logmessage = []
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('updateAppDetails', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    logmessage.append('updateAppDetails;%s' % (str(data)))

    try:
        appuid_req = data[ServerConstants.APPUID]
    except KeyError:
        backendlogger.error('getAppDetails', data, 'Appid  not sent', logmessage)
        return HttpResponse("appid not sent", status=404)

    try:
        appdetails = AppDetails.objects.get(appuid=appuid_req)
    except ObjectDoesNotExist:
        backendlogger.error('getAppDetails', data, 'WebApp  not found', logmessage)
        return HttpResponse("app not found", status=404)

    appConfig = AppConfig.objects.filter(appdetails=appdetails)

    appconfigdict = dict()
    for appConfigDetails in appConfig:
        appconfigdict[appConfigDetails.key] = appConfigDetails.value

    # get inclinic doctors too
    mappeddoctors = list()
    appdocmappingsforthisappid = DoctorAppMapping.objects.filter(appdetails=appdetails)
    for thisdocmapping in appdocmappingsforthisappid:
        doctorname = thisdocmapping.doctor.name
        doctoruid = thisdocmapping.doctor.doctoruid
        consulttype = thisdocmapping.consultation_type
        mappeddoctors.append({ServerConstants.DOCTORNAME: doctorname, ServerConstants.DOCTORUID: doctoruid,
                              ServerConstants.CONSULTATIONTYPE: consulttype})

    lablist = []
    applabmapping = AppLabMapping.objects.filter(appdetails=appdetails)
    for thismap in applabmapping:
        lablist.append({ServerConstants.LABID: thismap.labdetails.id, ServerConstants.LABNAME: thismap.labdetails.name})

    jsondata = dict({ServerConstants.APPUID: appdetails.appuid,
                     ServerConstants.APPLOCATION: appdetails.applocation,
                     ServerConstants.APPTAG: appdetails.apptag,
                     ServerConstants.APPTYPE: appdetails.apptype,
                     ServerConstants.APPDESCRIPTION: appdetails.appdescription,
                     ServerConstants.LICENSESTARTDATE: appdetails.licensestartdate,
                     ServerConstants.LICENSEENDDATE: appdetails.licenseenddate,
                     ServerConstants.LICENSEENABLED: appdetails.licenseenabled,
                     ServerConstants.PARTNER: appdetails.partner,
                     ServerConstants.APPCONFIG: appconfigdict,
                     ServerConstants.QBCALLERID: appdetails.qbid,
                     ServerConstants.QBUSERNAME: appdetails.qbusername,
                     ServerConstants.QBPASSWORD: appdetails.qbpassword,
                     ServerConstants.QBAPPID: config.QB_APP_ID,
                     ServerConstants.QBAUTHKEY: config.QB_AUTH_KEY,
                     ServerConstants.QBAUTHSECRET: config.QB_AUTH_SECRET,
                     ServerConstants.QBACCOUNTKEY: config.QB_ACCOUNT_KEY,
                     ServerConstants.MAPPEDDOCTORLIST: mappeddoctors,
                     ServerConstants.LABLIST: lablist,
                     })
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getAppDetails', data, httpoutput, logmessage)
    # requestlogger.info('getAppDetailsResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE, status=200)


# edited by Vatsal
@csrf_exempt
def getAppServices(request):
    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getAppServices', logrequest, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        appuid_req = request.GET[ServerConstants.APPSTR]
    except KeyError:
        return HttpResponse("appId not found", status=403)
    try:
        kioskarray = appuid_req.split(",")
        thisappservice = Services.objects.filter(appdetails__appuid__in=kioskarray)
    except ObjectDoesNotExist:
        return HttpResponse("kiosk doesnt exist in Services", status=404)

    appservicelist = list()
    for appservice in thisappservice:
        if appservice.doctor != None:
            appservicelist.append({ServerConstants.SERVICEID: appservice.serviceid,
                                   ServerConstants.SERVICETAG: appservice.servicetag,
                                   ServerConstants.SERVICETITLE: appservice.servicetitle,
                                   ServerConstants.SERVICEDESCRIPTION: appservice.servicedescription,
                                   ServerConstants.SERVICECHARGES: appservice.servicecharges,
                                   ServerConstants.DOCTORUID: appservice.doctor.doctoruid,
                                   ServerConstants.DOCTORNAME: appservice.doctor.name,
                                   ServerConstants.APPUID: appservice.appdetails.appuid})
        else:
            appservicelist.append({ServerConstants.SERVICEID: appservice.serviceid,
                                   ServerConstants.SERVICETAG: appservice.servicetag,
                                   ServerConstants.SERVICETITLE: appservice.servicetitle,
                                   ServerConstants.SERVICEDESCRIPTION: appservice.servicedescription,
                                   ServerConstants.SERVICECHARGES: appservice.servicecharges})

    jsondata = dict({ServerConstants.APPSERVICESLIST: appservicelist})
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getAppServices', logrequest, httpoutput, logmessage)
    # requestlogger.info('getAllPersonListResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def createAppServices(request):
    data = json.loads(request.body)
    logmessage = []
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('createAppServices', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    logmessage.append('createAppServices;%s' % (str(data)))
    # requestlogger.info('createApp;%s'%(str(data)))
    doctorphone_req = None
    tagalreadyadded = None
    thisservice = None
    thisdoctor = None

    try:
        appuid_req = data[ServerConstants.APPUID]
    except KeyError:
        return HttpResponse("Please provide WebApp Id", status=400)

    try:
        servicetag_req = data[ServerConstants.SERVICETAG]
    except KeyError:
        return HttpResponse("Please Provide Service Tag", status=400)

    try:
        servicetitle_req = data[ServerConstants.SERVICETITLE]
    except KeyError:
        servicetitle_req = ""

    try:
        servicedescription_req = data[ServerConstants.SERVICEDESCRIPTION]
    except KeyError:
        servicedescription_req = ""

    try:
        servicecharge_req = data[ServerConstants.SERVICECHARGES]
    except KeyError:
        return HttpResponse("Please provide WebApp Service Charges", status=400)

    try:
        thisapp = AppDetails.objects.get(appuid=appuid_req)
    except ObjectDoesNotExist:
        return HttpResponse("AppId is invalid", status=400)

    try:
        doctorphone_req = data[ServerConstants.DOCTORPHONE]
        try:
            thisdoctor = DoctorDetails.objects.get(phone=doctorphone_req)
        except ObjectDoesNotExist:
            return HttpResponse("Doctor not found", status=400)
    except KeyError:
        pass

    try:
        thisservice = Services.objects.get(appdetails=thisapp, doctor=thisdoctor,
                                           servicetag=servicetag_req)  # Check if this servicetag already mapped or not
    except ObjectDoesNotExist:
        thisservice = None

    if thisservice:  # update if service already present
        thisservice.servicetitle = servicetitle_req
        if servicedescription_req:
            thisservice.servicedescription = servicedescription_req
        if thisdoctor:
            thisservice.doctor = thisdoctor
        thisservice.servicecharges = servicecharge_req
        thisservice.save()
    else:  # create new service for doctor consultation tag
        newappservice = Services(doctor=thisdoctor, appdetails=thisapp, servicetag=servicetag_req,
                                 servicetitle=servicetitle_req, servicedescription=servicedescription_req,
                                 servicecharges=servicecharge_req)
        newappservice.save()

    httpoutput = utils.successJson(dict({ServerConstants.APPUID: appuid_req, }))
    backendlogger.info('createAppServices', data, httpoutput, logmessage)
    # requestlogger.info('updateDoctorResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


def getAppAvailabilityString(date, appuid_req, starttime_req, endtime_req, slotlength):
    # get todays slots
    starttime = datetime.strptime(str(starttime_req), "%H:%M:%S")
    endtime = datetime.strptime(str(endtime_req), "%H:%M:%S")
    try:
        appdetails = AppDetails.objects.get(appuid=appuid_req)
    except ObjectDoesNotExist:
        return HttpResponse("kiosk not found", status=404)

    todaysbookedslotsforthiskiosk = SlotBooking.objects.filter(date=date, appdetails=appdetails,
                                                               starttime__range=(starttime, endtime))

    # make all slot starttime array
    totalslots = endtime - starttime
    totalslots = int(totalslots.seconds / (60 * slotlength))

    if len(todaysbookedslotsforthiskiosk) == 0:
        return "0" * totalslots

    docslotstarttimearray = list()
    for i in range(0, totalslots):
        docslotstarttimearray.append(starttime + timedelta(minutes=i * slotlength))

    kioskavstr = ""
    for docslotstarttime in docslotstarttimearray:
        thisslottaken = False
        docslotendtime = docslotstarttime + timedelta(minutes=slotlength)
        for slotbooked in todaysbookedslotsforthiskiosk:
            if thisslottaken:
                continue
            slotbookedstarttime = datetime.strptime(str(slotbooked.starttime), "%H:%M:%S")
            slotbookedendtime = slotbookedstarttime + timedelta(minutes=slotbooked.slotlength)
            if (docslotstarttime > slotbookedstarttime and docslotstarttime < slotbookedendtime):
                kioskavstr = kioskavstr + "1"
                thisslottaken = True
                continue
            if (docslotendtime > slotbookedstarttime and docslotendtime < slotbookedendtime):
                kioskavstr = kioskavstr + "1"
                thisslottaken = True
                continue

        if not thisslottaken:
            kioskavstr = kioskavstr + "0"

    return kioskavstr


@csrf_exempt
def preparedoctorfordemo(request):
    data = json.loads(request.body)
    logmessage = []

    try:
        doctoruid_req = data[ServerConstants.DOCTORUID]
        doctor = DoctorDetails.objects.get(doctoruid=doctoruid_req)
    except KeyError:
        backendlogger.error('preparedoctorfordemo', data, 'doctoruid not found', logmessage)
        return HttpResponse("doctoruid not found", status=400)

    try:
        appuid_req = data[ServerConstants.APPUID]
        appdetails = AppDetails.objects.get(appuid=appuid_req)
    except KeyError:
        backendlogger.error('preparedoctorfordemo', data, 'apppid not found', logmessage)
        return HttpResponse("appid not found", status=400)

    try:
        DoctorAppMapping.objects.create(doctor=doctor, appdetails=appdetails)
    except:
        pass

    appdetails.save()

    message = ''.join(['Demo request by Dr {}({}) on {}'.format(doctor.name, doctor.phone, appdetails.applocation)])
    Communications.utils.sendSMS(message, "919769465241", appdetails.appsmssenderid, None)
    if "easylifepatientcare" in appdetails.applocation:
        Communications.utils.sendSMS(message, "919372425913", appdetails.appsmssenderid, None)

    httpoutput = utils.successJson({})
    backendlogger.info('preparedoctorfordemo', data, httpoutput, logmessage)
    # requestlogger.info('getDoctorAvailabilityForDateAndApp;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def setMailTypeForDoctor(request):
    # requestlogger.info('================setMailTypeForDoctor=====================')
    data = json.loads(request.body)
    logmessage = []
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('setMailTypeForDoctor', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        doctoruid_req = data[ServerConstants.DOCTORUID]
    except KeyError:
        backendlogger.error('setMailTypeForDoctor', data, 'doctoruid not found', logmessage)
        return HttpResponse("doctoruid not found", status=400)

    logmessage.append('setMailTypeForDoctorRequest;doctoruid%s' % (str(doctoruid_req)))
    # requestlogger.info('setMailTypeForDoctorRequest;doctoruid%s'%(str(doctoruid_req)))
    try:
        doctor = DoctorDetails.objects.get(doctoruid=doctoruid_req)
    except ObjectDoesNotExist:
        return HttpResponse("doctoruid not found", status=400)

    try:
        mail_type_req = data[ServerConstants.MAIL_TYPE]
    except KeyError:
        backendlogger.error('setMailTypeForDoctor', data, 'mail type not found', logmessage)
        return HttpResponse("mail type not found", status=400)

    doctor_obj = DoctorStatsNotification.objects.create(doctor=doctor, mail_type=mail_type_req)
    httpoutput = serializers.serialize('json', doctor.doctoremailnotification_set.all())
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def deleteMailTypeForDoctor(request):
    # requestlogger.info('================deleteMailTypeForDoctor=====================')
    data = json.loads(request.body)
    logmessage = []
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('deleteMailTypeForDoctor', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        doctoruid_req = data[ServerConstants.DOCTORUID]
    except KeyError:
        backendlogger.error('deleteMailTypeForDoctor', data, 'doctoruid not found', logmessage)
        return HttpResponse("doctoruid not found", status=400)

    logmessage.append('deleteMailTypeForDoctorRequest;doctoruid%s' % (str(doctoruid_req)))
    # requestlogger.info('deleteMailTypeForDoctorRequest;doctoruid%s'%(str(doctoruid_req)))
    try:
        doctor = DoctorDetails.objects.get(doctoruid=doctoruid_req)
    except ObjectDoesNotExist:
        return HttpResponse("doctoruid not found", status=400)

    try:
        mail_type_req = data[ServerConstants.MAIL_TYPE]
    except KeyError:
        backendlogger.error('deleteMailTypeForDoctor', data, 'mail type not found', logmessage)
        return HttpResponse("mail type not found", status=400)

    doctor_obj = DoctorStatsNotification.objects.get(doctor=doctor, mail_type=mail_type_req)
    doctor_obj.delete()
    httpoutput = serializers.serialize('json', doctor.doctoremailnotification_set.all())
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)
