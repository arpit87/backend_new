from django.db import models
from django.contrib.auth.models import User

from backend_new import config,Constants
import os
from django.conf import settings
from datetime import date

class SkillNumToGroup(models.Model):
    skillnum = models.IntegerField(unique=True)
    groupname = models.CharField(max_length=50,null=True,blank=True)
    categoryname = models.CharField(max_length=50,null=True,blank=True)
    categorydescription = models.CharField(max_length=255,null=True,blank=True) # comma separated string

    def __str__(self):
      return str(self.categoryname)

class AppDetails(models.Model):

    appid = models.AutoField(primary_key=True)
    appuid = models.CharField(db_index=True, max_length=10, null=True)
    appdeviceid = models.CharField(max_length=20, null=True, blank=True)
    qbusername = models.CharField(max_length=50, null=True, blank=True)
    qbpassword = models.CharField(max_length=50, null=True, blank=True)
    qbid = models.IntegerField(null=True, blank=True)
    applocation = models.CharField(max_length=50, null=True, blank=True)
    apptag = models.CharField(max_length=50, null=True, blank=True)
    apptype = models.CharField(max_length=50)
    appdescription = models.CharField(max_length=255, null=True, blank=True)
    appversions = models.CharField(max_length=128, null=True, blank=True)
    licensestartdate = models.DateField()
    licenseenddate = models.DateField()
    licenseenabled = models.BooleanField(default=False)
    djangouser = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    partner = models.CharField(max_length=50)
    appsmssenderid = models.CharField(max_length=10, default="GAMVTC")
    payuauthtoken = models.CharField(max_length=50, default="lI6y4+RkUzNo80Vvah4S07bKp6h1mEaJLiqdPFFSNUY=")
    appadminphone = models.CharField(max_length=20, null=True)

    def __str__(self):
        return '{}-{}'.format(self.appid, self.appuid)

    def get_Doctors(self, consultation_type):
        kwargs = {}
        kwargs["appdetails"] = self
        if consultation_type:
            kwargs["consultation_type"] = consultation_type

        mappings = DoctorAppMapping.objects.filter(**kwargs)
        return [mapping.Doctor for mapping in mappings]

    def islicensevalid(self):
        datenow = date.today()
        if self.licensestartdate is not None and self.licenseenddate is not None and self.licenseenabled:
            if self.licensestartdate <= datenow and self.licenseenddate >= datenow:
                return True
            else:
                return False
        else:
            return False


class DoctorDetails(models.Model):
    doctorid = models.AutoField(primary_key=True, null=False)
    doctoruid = models.CharField(db_index=True, max_length=10, null=True)
    djangouser = models.OneToOneField(User,null=True, on_delete=models.CASCADE)
    qbusername = models.CharField(max_length=20,null=True,blank=True)
    qbpassword = models.CharField(max_length=20,null=True,blank=True)
    qbid = models.IntegerField(null=True,blank=True)
    counterqbusername = models.CharField(max_length=20,null=True,blank=True)
    counterqbpassword = models.CharField(max_length=20,null=True,blank=True)
    counterqbid = models.IntegerField(null=True,blank=True)
    name = models.CharField(max_length=255,null=False)
    phone = models.CharField(db_index=True,max_length=20,null=False)
    email = models.CharField(max_length=50,null=True,blank=True)
    yearofbirth = models.IntegerField(null=True)
    skillstr = models.CharField(max_length=20,null=False)
    skills = models.ManyToManyField(SkillNumToGroup, related_name="doctorsskills")
    gender = models.CharField(max_length=10,default="M")
    date_joined = models.DateField(auto_now_add=True)
    registrationnum = models.CharField(max_length=100,null=True,blank=True)
    experience = models.CharField(max_length=10,null=True,blank=True)
    specialization = models.CharField(max_length=255,null=True,blank=True)
    qualification = models.CharField(max_length=255,null=True,blank=True)
    biosketch = models.CharField(max_length=2047,null=True,blank=True)
    profilepic = models.CharField(max_length=255,null=False,blank=False,
                                  default=config.DOMAIN+os.path.join(settings.STATIC_URL,settings.UPLOADEDFILES_DIR,
                               Constants.FileStorageConstants.DOCTORPROFILEPICDIR,"default_doctor.png"))
    signpic = models.CharField(max_length=255,null=False,blank=False,
                                  default=config.DOMAIN+os.path.join(settings.STATIC_URL,settings.UPLOADEDFILES_DIR,
                               Constants.FileStorageConstants.DOCTORPROFILEPICDIR,"docsignpic.jpg"))
    docs = models.CharField(max_length=1023,null=True,blank=True)
    city=models.CharField(max_length=100,null=True,blank=True)
    servicesoffered = models.CharField(max_length=30,null=True,blank=True)
    isactive = models.BooleanField(default=True)
    primaryapp = models.ForeignKey(AppDetails, on_delete=models.SET_NULL, null=True, default=None)


    def __str__(self):
        return self.name


class DoctorWeeklyCalender(models.Model):
    doctor = models.OneToOneField('DoctorDetails', on_delete=models.CASCADE)
    dayofweek = models.IntegerField(null=True,blank=True) # 0 sunday,Monday...6 saturday
    availabilitystart = models.TimeField(null=False) #"10:00"
    availabilityend = models.TimeField(null=False) #"14:00"
    availabilitystr = models.CharField(max_length=255,null=True,blank=True)

    def __str__(self):
        return self.doctor.name


class AppConfig(models.Model):
    appdetails = models.ForeignKey(AppDetails, on_delete=models.CASCADE)
    key = models.CharField(max_length=40)
    value = models.CharField(max_length=200)

    def __str__(self):
        return str('{}-{}'.format(self.appdetails.appuid,self.key))

class DoctorAppMapping(models.Model):
    HOSPITAL_OWNER = 1 # All Cases
    INCLINIC = 2
    SCHOOL = 3
    REPORTREVIEWVER = 4
    HEALTHATMMONITOR = 5
    HOSPITAL_DOCTOR = 6 # My Cases
    HIDDEN = 7 # Not shown on the landing page
    TELEMEDICINE = 8  # seen remotely on healthbox and my Cases shown to doc


    TYPES = ((HOSPITAL_OWNER, 'All Cases'), (INCLINIC, 'Inclinic'), (REPORTREVIEWVER, 'Report Reviewver'),
             (HEALTHATMMONITOR, 'Healthatm Monitor'), (HOSPITAL_DOCTOR, 'My Case'),(HIDDEN, 'Hidden'), (TELEMEDICINE, 'Telemedicine'),)
    consultation_type = models.IntegerField(choices=TYPES, default=TELEMEDICINE)
    doctor = models.ForeignKey(DoctorDetails, on_delete=models.CASCADE)
    appdetails = models.ForeignKey(AppDetails, on_delete=models.CASCADE)

    class Meta:
        unique_together = (('doctor', 'appdetails', 'consultation_type'))

    def __str__(self):
      return str(self.doctor.doctoruid)

    def get_mapping(self):
        result = {}
        result['consultation_type_name'] = self.get_consultation_type_display()
        result['consultation_type'] = self.consultation_type
        result['Doctorname'] = self.doctor.name
        result['doctoruid'] = self.doctor.doctoruid
        result['Doctor_profilepic'] = self.doctor.profilepic
        result['specialization'] = self.doctor.specialization
        result['qualification'] = self.doctor.qualification
        result['experience'] = self.doctor.experience
        result['appuid'] = self.appdetails.appuid
        return result


class Services(models.Model):
    appdetails = models.ForeignKey(AppDetails, null=True, blank=True, on_delete=models.CASCADE)
    doctor = models.ForeignKey(DoctorDetails, null=True, blank=True, on_delete=models.CASCADE)
    serviceid = models.AutoField(primary_key=True, null=False)
    servicetag = models.CharField(max_length=20, null=False, blank=False)
    servicetitle = models.CharField(max_length=50, null=False, blank=False)
    servicedescription = models.CharField(max_length=1024, null=True, blank=True)
    servicecharges = models.IntegerField(null=False, blank=False)

    def __str__(self):
        return str(self.servicetag)

    class Meta:
        unique_together = ('doctor', 'servicetag', 'appdetails')


class DoctorStatsNotification(models.Model):
    DAILY = 1
    WEEKLY = 2
    BIWEEKLY = 3
    MONTHLY = 4
    freq_choices = (
        (DAILY, "daily"),
        (WEEKLY, "weekly"),
        (BIWEEKLY, "biweekly"),
        (MONTHLY, "monthly"),
    )
    doctor = models.ForeignKey(DoctorDetails, on_delete=models.CASCADE)
    sendemail = models.BooleanField(default = False)
    sendsms = models.BooleanField(default = False)
    freq = models.IntegerField(choices=freq_choices)

class HospitalCoordinatorDoctorMapping(models.Model):
    hospitalcoordinator = models.ForeignKey(DoctorDetails, on_delete=models.CASCADE, related_name="hospitalcoordinator")
    doctor = models.ForeignKey(DoctorDetails, on_delete=models.CASCADE, related_name="mappeddoctor")

    class Meta:
        unique_together = ("hospitalcoordinator","doctor")

    def __str__(self):
      return str(self.hospitalcoordinator.doctoruid)