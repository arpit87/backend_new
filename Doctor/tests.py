import datetime
from django.test import TestCase
import time

# Create your tests here.

#Test URLS
from Booking.models import BookingDetails, BookingTableDoctor,Services
from Platform.models import DOCTORAPPDATA,USERAPPDATA,AUTHDATA
import json
import Platform.Constants
import Platform.utils
from Person.models import PersonDetails
import backend_new.Constants
from Doctor.models import DoctorDetails, DoctorLocationAndAvailability ,DoctorWeeklyCalender , DoctorAvailability , SkillNumToGroup , AppDetails,DoctorAppMapping
from django.contrib.auth.models import Person
from django.contrib.auth.models import Group
from django.contrib.auth import authenticate,login,logout
import pylibmc
from Booking.models import SlotBooking
import backend_new.config
import requests


class DoctorAPITestCase(TestCase):
    multi_db = True
    def setUp(self):
        backend_new.config.ISTESTMODE = True
        DOCTORAPPDATA.objects.create(Doctorappuuid = "123",doctoruid=100)
        USERAPPDATA.objects.create(userappuuid = "1234",personuid=1)
        djangouser = Person.objects.create_user(username="Doctor9769465241",password="abc")
        djangouser1 = Person.objects.create_user(username="Doctor978637273",password="abc")
        djangouser4 = Person.objects.create_user(username="Doctor9167636253",password="abc")

        djangouser2 = Person.objects.create_user(username="user9736737272",password="abc")
        djangouser3 = Person.objects.create_user(username="user9736737273",password="abc")
        Doctornew = DoctorDetails.objects.create(djangouser=djangouser, doctoruid=100, name="arpitnew", phone="9769465241", skillstr="0,1,2,7,55",
                                               qbid=backend_new.config.QB_TEST_QBID,
                                               docs="testdoc1,testdoc2,http://yolohealth.in/static/uploadedfiles/Doctordocs/file1.png",
                                               servicesoffered = "1,2,3", qbusername=backend_new.config.
                                               QB_TEST_USER, qbpassword=backend_new.config.QB_TEST_PASSWORD, ishospitaladmin=True, dob="1986-10-07", city="pune")
        Doctornew1 = DoctorDetails.objects.create(djangouser=djangouser1,doctoruid=11,name="arpitnew1",phone="978637273",
                                                skillstr="0,1,2,7,5",hospitaladminnum = "9769465241",dob="1986-10-07",city="pune")
        Doctornew2 = DoctorDetails.objects.create(djangouser=djangouser4, doctoruid=300, name="hiren", phone="9167636253",
                                                skillstr="0,1,2,71", qbid=backend_new.config.QB_TEST_QBID2, qbusername=backend_new.config.
                                                QB_TEST_USER2, qbpassword=backend_new.config.QB_TEST_PASSWORD2, hospitaladminnum ="9769465241", dob="1986-10-07", city="pune")
        DoctorLocationAndAvailability.objects.create(Doctor=Doctornew,isavailable=True,latitude=12.1234570,longitude=19.1234575)
        DoctorLocationAndAvailability.objects.create(Doctor=Doctornew1,isavailable=True,latitude=12.7000000,longitude=19.5000000)
        usernew=PersonDetails.objects.create(djangouser=djangouser2, personuid=1, name='arpit', phone="9736737272")
        bookingnew=BookingDetails.objects.create(bookingid=1,bookingfor=2,landmark="Gopal Sharma",flat="b1701",
                                                 locality="powai",user=usernew,datetime=datetime.datetime.now(),bookingstatus=0)
        bookingaccepted=BookingDetails.objects.create(bookingid=2,bookingfor=2,landmark="Gopal Sharma",flat="b1701",
                                                 locality="powai",user=usernew,datetime=datetime.datetime.now(),bookingstatus=1,Doctor=Doctornew)
        BookingTableDoctor.objects.create(Doctor=Doctornew, booking=bookingnew,
                                         bookingstatusDoctor=backend_new.Constants.BookingConstants.PENDING)
        BookingTableDoctor.objects.create(Doctor=Doctornew, booking=bookingaccepted,
                                         bookingstatusDoctor=backend_new.Constants.BookingConstants.ACCEPTED)
        Group.objects.create(name="admingroup")
        Group.objects.create(name="usergroup")
        Group.objects.create(name="Doctorgroup")
        doctorgroup = Group.objects.create(name="doctorgroup")
        doctorgroup.user_set.add(djangouser)
        doctorgroup.user_set.add(djangouser1)
        doctorgroup.user_set.add(djangouser4)
        authenticate(username = djangouser.username , password="abc")
        authenticate(username = djangouser1.username , password="abc")
        Group.objects.create(name="nursegroup")
        Group.objects.create(name="caretakergroup")
        Group.objects.create(name="physiogroup")
        Group.objects.create(name="yogagroup")
        Group.objects.create(name="healthcoachgroup")
        Group.objects.create(name="kioskgroup")
        DoctorWeeklyCalender.objects.create(Doctor=Doctornew,dayofweek=1,availabilitystart="10:00",availabilityend="14:00")
        DoctorAvailability.objects.create(availabilityid = 100,Doctor = Doctornew,slotbooked="000000000000",slotidsbooked="",
                                          date = "2016-03-26",starttime= "12:00",endtime="1:00", recurringid=200)
        DoctorAvailability.objects.create(availabilityid = 150,Doctor = Doctornew,slotbooked="000000000000",slotidsbooked="",
                                          date = "2016-03-26",starttime= "14:00",endtime="1:00", recurringid=200)
        DoctorAvailability.objects.create(availabilityid = 170,Doctor = Doctornew,slotbooked="000000000000",slotidsbooked="",
                                          date = "2016-03-26",starttime= "4:00",endtime="1:00", recurringid=200)
        DoctorAvailability.objects.create(availabilityid = 400,Doctor = Doctornew,slotbooked="000000000000",slotidsbooked="",
                                          date = "2016-03-26",starttime= "2:00",endtime="3:00", recurringid=20)
        DoctorAvailability.objects.create(availabilityid = 200,Doctor = Doctornew,slotbooked="000000000000",slotidsbooked="200,300",
                                          date = "2016-03-2",starttime= "8:00",
                                          endtime="9:00",recurringid=200)
        DoctorAvailability.objects.create(availabilityid = 300,Doctor = Doctornew,slotbooked="000000000000",slotidsbooked="400",
                                          date = str(datetime.date.today()+datetime.timedelta(days=-3)),starttime= "11:00",
                                          endtime="12:00",recurringid=200)
        SkillNumToGroup.objects.create(skillnum=0,groupname="doctorgroup",categoryname="ENT and Opthamoligist",categorydescription="ear,nose,eye")
        SkillNumToGroup.objects.create(skillnum=1,groupname="doctorgroup",categoryname="Cardiac",categorydescription="ear,nose,eye")
        SkillNumToGroup.objects.create(skillnum=2,groupname="doctorgroup",categoryname="Ayurveda",categorydescription="ear,nose,eye")
        SkillNumToGroup.objects.create(skillnum=5,groupname="doctorgroup",categoryname="Oncho",categorydescription="cancer")
        SkillNumToGroup.objects.create(skillnum=7,groupname="doctorgroup",categoryname="ABCD",categorydescription="ear,nose,eye")
        SkillNumToGroup.objects.create(skillnum=55,groupname="doctorgroup",categoryname="Surgeon",categorydescription="surgery")
        SkillNumToGroup.objects.create(skillnum=71,groupname="healthcoachgroup",categoryname="HealthCoach",categorydescription="weightloss,stress")

        kiosk = AppDetails.objects.create(kioskid=100, kiosklocation="IOC", kiosktag="IOC-1", kiosktype=1, kioskdescription="IOC", glucosestrips=10, hbstrips=10, lipidstrips=10)
        # DoctorAppMapping.objects.create(Doctor = Doctornew2,kiosk = kiosk)
        # DoctorAppMapping.objects.create(Doctor = Doctornew,kiosk = kiosk)

        kiosk1 = AppDetails.objects.create(kioskid=1001, kiosklocation="MOT", kiosktag="MOT-11", kiosktype=1, kioskdescription="MOT", kioskauthkey="00-11-22-33-44-55")
        # DoctorAppMapping.objects.create(Doctor = Doctornew1,kiosk = kiosk)
        SlotBooking.objects.create(kiosk=kiosk,user=usernew,Doctor=Doctornew,date="2016-10-07",starttime="11:30",slotlength=15,slotno=3)
        SlotBooking.objects.create(slotbookingid="200",kiosk=kiosk,user=usernew,Doctor=Doctornew,
                                   date=datetime.date.today()+datetime.timedelta(days=1),starttime="8:15",slotlength=15,slotno=3)
        SlotBooking.objects.create(slotbookingid="300",kiosk=kiosk,user=usernew,Doctor=Doctornew,
                                   date=datetime.date.today()+datetime.timedelta(days=1),starttime="8:45",slotlength=15,slotno=3)
        SlotBooking.objects.create(slotbookingid="400",kiosk=kiosk,user=usernew,Doctor=Doctornew,
                                   date=datetime.date.today()+datetime.timedelta(days=-3),starttime="11:30",slotlength=15,slotno=3)

        AUTHDATA.objects.create(authkey="00-11-22-33-44-55",authsecret="Sdg4D456dAAfdfg")
        DoctorAppMapping.objects.create(Doctor=Doctornew1, kiosk=kiosk)
        DoctorAppMapping.objects.create(Doctor=Doctornew2, kiosk=kiosk)
        DoctorAppMapping.objects.create(Doctor=Doctornew2, kiosk=kiosk1)

        Services.objects.create(serviceid=4,servicetag="Consultation",servicetitle="Doctor Visit",
                                servicecharges=1000,servicedescription="Doctor general visit",kiosk=kiosk,Doctor=Doctornew)
        Services.objects.create(serviceid=5,servicetag="Nurse Visit",servicetitle="Nurse Visit",
                                servicecharges=100,servicedescription="Nurse general visit")
        Services.objects.create(serviceid=3,servicetag="Caretaker",servicetitle="Caretaker Visit",
                                servicecharges=200,servicedescription="Caretaker general visit")


    def test_createDoctor(self):
        input = json.dumps({backend_new.Constants.ServerConstants.DOCTORNAME: 'Arpit',
                            backend_new.Constants.ServerConstants.DOCTORNOTES: '1234',
                            backend_new.Constants.ServerConstants.DOCTORGENDER: 'M',
                            backend_new.Constants.ServerConstants.DOCTORPHONE:Platform.utils.getRandomNumber(10),
                            backend_new.Constants.ServerConstants.YEAROFBIRTH:time.mktime(datetime.datetime.now().timetuple()),
                            backend_new.Constants.ServerConstants.SPECIALIZATION: 'HEart',
                            backend_new.Constants.ServerConstants.QUALIFICATION: 'MBBS',
                            backend_new.Constants.ServerConstants.HOSPITALNAME: 'NANAVATI',
                            backend_new.Constants.ServerConstants.SKILLSTR: '1,2,7,71'})
        response = self.client.post('/Doctor/createDoctor/',input,Platform.Constants.RESPONSE_JSON_TYPE)

        self.assertEqual(response.status_code,200)

        #now delete the test Doctor created on qb
        Doctordata = json.loads(response.content)
        qbsessiontoken = Platform.utils.getQBPersonSessionToken(Doctordata["body"]["qbusername"],Doctordata["body"]["qbpassword"])
        response = requests.delete("http://api.quickblox.com/users/"+str(Doctordata["body"]["qbid"])+".json", headers={'Content-Type': 'application/json',
                                                                                         'QuickBlox-REST-API-Version': '0.1.0',
                                                                                          'QB-Token':qbsessiontoken})
        self.assertEqual(response.status_code,200)

    def test_createAndloginDoctorByOTP(self):
        #create Doctor first
        phone = Platform.utils.getRandomNumber(10)
        input = json.dumps({backend_new.Constants.ServerConstants.DOCTORNAME: 'Dr Strange',
                            backend_new.Constants.ServerConstants.DOCTORNOTES: '1234',
                            backend_new.Constants.ServerConstants.DOCTORGENDER: 'M',
                            backend_new.Constants.ServerConstants.DOCTORPHONE:phone,
                            backend_new.Constants.ServerConstants.YEAROFBIRTH:time.mktime(datetime.datetime.now().timetuple()),
                            backend_new.Constants.ServerConstants.SPECIALIZATION: 'HEart',
                            backend_new.Constants.ServerConstants.QUALIFICATION: 'MBBS',
                            backend_new.Constants.ServerConstants.SKILLSTR: '1,2'})
        response = self.client.post('/Doctor/createDoctor/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)


        #this is to delete the test qb user in the end from qb database
        Doctordata = json.loads(response.content)
        qbidcreated = str(Doctordata["body"]["qbid"])

        cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
        cache.set(phone+'_otp',7416)
        input = json.dumps({backend_new.Constants.ServerConstants.OTP:7416,
                            backend_new.Constants.ServerConstants.DOCTORPHONE:phone,
                            backend_new.Constants.ServerConstants.DOCTORAPPUUID: "asdf"})
        response = self.client.post('/Doctor/loginDoctor/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)


         #now delete the test Doctor created on qb database
        qbsessiontoken = Platform.utils.getQBPersonSessionToken(Doctordata["body"]["qbusername"],Doctordata["body"]["qbpassword"])
        response = requests.delete("http://api.quickblox.com/users/"+qbidcreated+".json", headers={'Content-Type': 'application/json',
                                                                                         'QuickBlox-REST-API-Version': '0.1.0',
                                                                                          'QB-Token':qbsessiontoken})
        self.assertEqual(response.status_code,200)


    def test_createAndloginDoctorByPass(self):
        #create Doctor first
        phone = Platform.utils.getRandomNumber(10)
        input = json.dumps({backend_new.Constants.ServerConstants.DOCTORNAME: 'Arpit',
                            backend_new.Constants.ServerConstants.DOCTORNOTES: '1234',
                            backend_new.Constants.ServerConstants.DOCTORGENDER: 'M',
                            backend_new.Constants.ServerConstants.DOCTORPHONE:phone,
                            backend_new.Constants.ServerConstants.YEAROFBIRTH:time.mktime(datetime.datetime.now().timetuple()),
                            backend_new.Constants.ServerConstants.SPECIALIZATION: 'HEart',
                            backend_new.Constants.ServerConstants.QUALIFICATION: 'MBBS',
                            backend_new.Constants.ServerConstants.SKILLSTR: '1,2'})
        response = self.client.post('/Doctor/createDoctor/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)

        data = json.loads(response.content)
         #this is to delete the test qb user in the end from qb database
        qbidcreated = str(data["body"]["qbid"])

        input = json.dumps({backend_new.Constants.ServerConstants.DOCTORPASSWORD:data["body"]["password"],
                            backend_new.Constants.ServerConstants.DOCTORPHONE:phone,
                            backend_new.Constants.ServerConstants.DOCTORAPPUUID: "asdf"})
        response = self.client.post('/Doctor/loginDoctor/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)


        #now delete the test Doctor created on qb database
        qbsessiontoken = Platform.utils.getQBPersonSessionToken(data["body"]["qbusername"],data["body"]["qbpassword"])
        response = requests.delete("http://api.quickblox.com/users/"+qbidcreated+".json", headers={'Content-Type': 'application/json',
                                                                                         'QuickBlox-REST-API-Version': '0.1.0',
                                                                                          'QB-Token':qbsessiontoken})
        self.assertEqual(response.status_code,200)


    def test_updateDoctor(self):
        input = json.dumps({backend_new.Constants.ServerConstants.DOCTORUID:100,
                            backend_new.Constants.ServerConstants.DOCTORNAME: 'Arpit',
                            backend_new.Constants.ServerConstants.DOCTORNOTES: '1234',
                            backend_new.Constants.ServerConstants.DOCTORGENDER: 'M',
                            backend_new.Constants.ServerConstants.DOCTORPHONE: '98716533',
                            backend_new.Constants.ServerConstants.YEAROFBIRTH:time.mktime(datetime.datetime.now().timetuple()),
                            backend_new.Constants.ServerConstants.SPECIALIZATION: 'HEart',
                            backend_new.Constants.ServerConstants.QUALIFICATION: 'MBBS',
                            backend_new.Constants.ServerConstants.HOSPITALNAME: 'NANAVATI',
                            backend_new.Constants.ServerConstants.SKILLSTR: '1,2'})
        response = self.client.post('/Doctor/updateDoctor/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)


    def test_changeAvailability(self):
        input = json.dumps({backend_new.Constants.ServerConstants.DOCTORUID:100,
                            backend_new.Constants.ServerConstants.LONGITUDE:12.232,
                            backend_new.Constants.ServerConstants.LATITUDE:-12.2,
                            backend_new.Constants.ServerConstants.ISONLINE:True,
                            })
        response = self.client.post('/Doctor/changeAvailability/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)


    def test_getThisDoctorsAllBookingOfStatus(self):
        input = dict({backend_new.Constants.ServerConstants.DOCTORUID:100,
                      backend_new.Constants.ServerConstants.BOOKINGSTATUSDOCTOR:1})
        response = self.client.get('/Doctor/getThisDoctorsAllBookingOfStatus/',input)
        self.assertEqual(response.status_code,200)


    def test_getAllDoctorList(self):
        response = self.client.get('/Doctor/getAllDoctorList/')
        self.assertEqual(response.status_code,200)


    def test_getAllDoctorList_FromAppStr(self):
        input = dict({backend_new.Constants.ServerConstants.KIOSKSTR: "1,11"})
        response = self.client.get('/Doctor/getAllDoctorList/',input)
        self.assertEqual(response.status_code,200)


    def test_getAllDoctorList_FromAppid(self):
        input = dict({backend_new.Constants.ServerConstants.KIOSKSTR:11})
        response = self.client.get('/Doctor/getAllDoctorList/',input)
        self.assertEqual(response.status_code,200)



    def test_getAllDoctorListSkillStr(self):
        input = dict({backend_new.Constants.ServerConstants.SKILLSTR:5})
        response = self.client.get('/Doctor/getAllDoctorList/',input)
        self.assertEqual(response.status_code,200)


    def test_getAllDoctorListSkillStrAppId(self):
        input = dict({backend_new.Constants.ServerConstants.SKILLSTR:5,
                      backend_new.Constants.ServerConstants.KIOSKID:1})
        response = self.client.get('/Doctor/getAllDoctorList/',input)
        self.assertEqual(response.status_code,200)


    def test_getNearbyDoctors(self):
        input = dict({backend_new.Constants.ServerConstants.SKILLSTR:2,
                      backend_new.Constants.ServerConstants.LATITUDE:12.1234567,
                      backend_new.Constants.ServerConstants.LONGITUDE:19.1234567})
        response = self.client.get('/Doctor/getNearbyDoctors/',input)
        self.assertEqual(response.status_code,200)


    def test_getDoctorLocation(self):
        input = dict({backend_new.Constants.ServerConstants.DOCTORUID:100
                      })
        response = self.client.get('/Doctor/getDoctorLocation/',input)
        self.assertEqual(response.status_code,200)


    def test_getDoctorDetails(self):
        input = dict({backend_new.Constants.ServerConstants.DOCTORUID:100
                      })
        response = self.client.get('/Doctor/getDoctorDetails/',input)
        self.assertEqual(response.status_code,200)


    # def test_deleteDocumentPicture(self):
    #      input = json.dumps({backend_new.Constants.ServerConstants.DOCTOR_ID:100,
    #                          backend_new.Constants.ServerConstants.DOCURL:"http://yolohealth.in/static/uploadedfiles/Doctordocs/file1.png",
    #                          })
    #      response = self.client.post('/Doctor/deleteDocumentPicture/',input,Platform.Constants.RESPONSE_JSON_TYPE)
    #      Doctor = DoctorDetails.objects.get(doctoruid=1)
    #      self.assertEqual(Doctor.docs,"testdoc1,testdoc2")
    #      self.assertEqual(response.status_code,200)
    #

    def test_searchDoctors(self):
        input = json.dumps({backend_new.Constants.ServerConstants.SEARCHSTR: "arp",
                            backend_new.Constants.ServerConstants.SEARCHFIELD: "Doctorname"
                            })
        response = self.client.post('/Doctor/searchDoctors/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)


    def test_setGCMRegistrationId(self):
        TESTGCM = "APA91bHPRgkF3JUikC4ENAHEeMrd41Zxv3hVZjC9KtT8OvPVGJ-hQMRKRrZuJAEcl7B338qju59zJMjw2DELjzEvxwYv7hH5Ynpc1ODQ0aT4U4OFEeco8ohsN5PjL1iC2dNtk2BAokeMCg2ZXKqpc8FXKmhX94kIxQ"
        input = json.dumps({backend_new.Constants.ServerConstants.DOCTORUID:100,
                            backend_new.Constants.ServerConstants.FCMTOKEN:TESTGCM})
        response = self.client.post('/Doctor/setGCMRegistrationId/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        thisDoctordata = DOCTORAPPDATA.objects.get(doctoruid=100)
        self.assertEqual(thisDoctordata.gcmregid,TESTGCM)


    def test_getServicesOfferedByDoctor(self):
        input = dict({backend_new.Constants.ServerConstants.DOCTORUID:100
                      })
        response = self.client.get('/Doctor/getServicesOfferedByDoctor/',input)
        self.assertEqual(response.status_code,200)


    def test_getOnlineDoctors(self):
        #returns an online mapped doctor
        input = json.dumps({backend_new.Constants.ServerConstants.PHONE: "9769465241",
                            backend_new.Constants.ServerConstants.PASSWORD: "abc",
                            backend_new.Constants.ServerConstants.USERTYPE: "doctor"})
        response = self.client.post('/Platform/logindoctor/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        input = json.dumps({backend_new.Constants.ServerConstants.DOCTORUID:100,
                            backend_new.Constants.ServerConstants.ONLINESTATUS:1})
        response = self.client.post('/Doctor/setOnlineStatusForDoctor/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        input = dict({backend_new.Constants.ServerConstants.KIOSKID:1})
        response = self.client.get('/Doctor/getOnlineDoctors/',input)
        self.assertEqual(response.status_code,200)


    def test_getOnlineDoctorsNew(self):
        #returns an online mapped doctor
        input = json.dumps({backend_new.Constants.ServerConstants.PHONE: "9167636253",

                            backend_new.Constants.ServerConstants.PASSWORD: "abc",
                            backend_new.Constants.ServerConstants.USERTYPE: "doctor"})
        response = self.client.post('/Platform/logindoctor/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        input = json.dumps({backend_new.Constants.ServerConstants.DOCTORUID:300,
                            backend_new.Constants.ServerConstants.ONLINESTATUS:1,
                            backend_new.Constants.ServerConstants.UNIQUEID: "uuid1"})
        response = self.client.post('/Doctor/setOnlineStatusForDoctorNew/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        input = dict({backend_new.Constants.ServerConstants.KIOSKID:1})
        response = self.client.get('/Doctor/getOnlineDoctorsNew/',input)
        self.assertEqual(response.status_code,200)



    # def test_setOnlineStatusForDoctor(self):
    #     input = json.dumps({backend_new.Constants.ServerConstants.DOCTOR_ID:11,
    #                         backend_new.Constants.ServerConstants.ONLINESTATUS:2})
    #     response = self.client.post('/Doctor/setOnlineStatusForDoctor/',input,Platform.Constants.RESPONSE_JSON_TYPE)
    #     print("response.content: ", response.content)
    #     self.assertEqual(response.status_code,200)
    #     cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
    #     status = cache.get("onlinestatus_11")
    #     self.assertLess(status,0) #negative online status means busy and numeric value is
    #     # number of simultaneous logins like from app,web etc etc


    def test_setOnlineStatusForDoctorNew(self):
        cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
        cache.delete("onlinestatusDoctor_11")
        input = json.dumps({backend_new.Constants.ServerConstants.DOCTORUID:11,
                            backend_new.Constants.ServerConstants.ONLINESTATUS:1,
                            backend_new.Constants.ServerConstants.UNIQUEID: "uuid1"})
        response = self.client.post('/Doctor/setOnlineStatusForDoctorNew/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        input = json.dumps({backend_new.Constants.ServerConstants.DOCTORUID:11,
                            backend_new.Constants.ServerConstants.ONLINESTATUS:1,
                            backend_new.Constants.ServerConstants.UNIQUEID: "uuid2"})
        response = self.client.post('/Doctor/setOnlineStatusForDoctorNew/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        status = cache.get("onlinestatusDoctor_11")
        self.assertEqual(status,"uuid1,uuid2")


    def test_setOnlineStatusForDoctorNew1(self):
        cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
        cache.delete("onlinestatusDoctor_11")
        input = json.dumps({backend_new.Constants.ServerConstants.DOCTORUID:11,
                            backend_new.Constants.ServerConstants.ONLINESTATUS:1,
                            backend_new.Constants.ServerConstants.UNIQUEID: "uuid1"})
        response = self.client.post('/Doctor/setOnlineStatusForDoctorNew/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        input = json.dumps({backend_new.Constants.ServerConstants.DOCTORUID:11,
                            backend_new.Constants.ServerConstants.ONLINESTATUS:1,
                            backend_new.Constants.ServerConstants.UNIQUEID: "uuid1"})
        response = self.client.post('/Doctor/setOnlineStatusForDoctorNew/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        status = cache.get("onlinestatusDoctor_11")
        self.assertEqual(status,"uuid1")
        input = json.dumps({backend_new.Constants.ServerConstants.DOCTORUID:11,
                            backend_new.Constants.ServerConstants.ONLINESTATUS:2,
                            backend_new.Constants.ServerConstants.UNIQUEID: "uuid1"})
        response = self.client.post('/Doctor/setOnlineStatusForDoctorNew/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        status = cache.get("onlinestatusDoctor_11_busy")
        self.assertEqual(status,1)
        status = cache.get("onlinestatusDoctor_11")
        self.assertEqual(status,"uuid1")
        input = json.dumps({backend_new.Constants.ServerConstants.DOCTORUID:11,
                            backend_new.Constants.ServerConstants.ONLINESTATUS:3,
                            backend_new.Constants.ServerConstants.UNIQUEID: "uuid1"})
        response = self.client.post('/Doctor/setOnlineStatusForDoctorNew/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        status = cache.get("onlinestatusDoctor_11_busy")
        self.assertEqual(status,None)
        input = json.dumps({backend_new.Constants.ServerConstants.DOCTORUID:11,
                            backend_new.Constants.ServerConstants.ONLINESTATUS:0,
                            backend_new.Constants.ServerConstants.UNIQUEID: "uuid1"})
        response = self.client.post('/Doctor/setOnlineStatusForDoctorNew/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        status = cache.get("onlinestatusDoctor_11")
        self.assertEqual(status,"")


    def test_getDoctorsOfCategory(self):
        input = dict({backend_new.Constants.ServerConstants.SKILLSTR:7,
                      backend_new.Constants.ServerConstants.OFFSET:0,
                      backend_new.Constants.ServerConstants.LIMIT:2,
                      })
        response = self.client.get('/Doctor/getDoctorsOfCategory/',input)
        self.assertEqual(response.status_code,200)



    def test_getDoctorsForApp(self):
        input = dict({backend_new.Constants.ServerConstants.APPUID:"jhgjhg"
                      })
        response = self.client.get('/Doctor/getDoctorsForApp/',input)
        self.assertEqual(response.status_code,200)


    def test_getDoctorWeeklyCalender(self):
        input = dict({backend_new.Constants.ServerConstants.DOCTORUID:100,
                      })
        response = self.client.get('/Doctor/getDoctorWeeklyCalender/',input)
        self.assertEqual(response.status_code,200)



    def test_setDoctorAvailability(self):
        input = json.dumps({backend_new.Constants.ServerConstants.DOCTORUID:100,
                            backend_new.Constants.ServerConstants.DATE: "2016-03-26",
                            backend_new.Constants.ServerConstants.STARTTIME: "1:00",
                            backend_new.Constants.ServerConstants.ENDTIME: "2:00",
                            backend_new.Constants.ServerConstants.SLOTLENGTH:15})
        response = self.client.post('/Doctor/setDoctorAvailability/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        print(response.content)

        # now testing
        docav = DoctorAvailability.objects.filter(Doctor__doctoruid=100).first()
        self.assertEqual(int(float(docav.slotbooked)),0000)

        self.assertEqual(response.status_code,200)

    def test_setDoctorCustomAvailability(self):
        input = json.dumps({backend_new.Constants.ServerConstants.DOCTORUID:11,
                            backend_new.Constants.ServerConstants.DATE: "2018-03-12",
                            backend_new.Constants.ServerConstants.STARTTIME: "1:00",
                            backend_new.Constants.ServerConstants.ENDTIME: "2:00",
                            backend_new.Constants.ServerConstants.SLOTLENGTH:15,
                            backend_new.Constants.ServerConstants.MONTH:2,
                            backend_new.Constants.ServerConstants.WEEKS:3,
                            backend_new.Constants.ServerConstants.DAYS: "0,2,4",
                            })
        response = self.client.post('/Doctor/setDoctorAvailability/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        print(response.content)
        self.assertEqual(response.status_code,200)

    def test_setDoctorAvailability_input_phone(self):
        input = json.dumps({backend_new.Constants.ServerConstants.DOCTORPHONE: 978637273,
                            backend_new.Constants.ServerConstants.DATE: "2016-03-26",
                            backend_new.Constants.ServerConstants.STARTTIME: "1:00",
                            backend_new.Constants.ServerConstants.ENDTIME: "2:00",
                            backend_new.Constants.ServerConstants.SLOTLENGTH:15})
        response = self.client.post('/Doctor/setDoctorAvailability/',input,Platform.Constants.RESPONSE_JSON_TYPE)

        # now testing
        docav = DoctorAvailability.objects.get(Doctor__doctoruid=11.)
        self.assertEqual(int(float(docav.slotbooked)),0000)

        self.assertEqual(response.status_code,200)


    def test_getDoctorAvailabilityForDateAndApp(self):
        input = dict({backend_new.Constants.ServerConstants.DATE: "2016-10-07",
                      backend_new.Constants.ServerConstants.KIOSKID:1,
                      })
        response = self.client.get('/Doctor/getDoctorAvailabilityForDateAndApp/',input)
        self.assertEqual(response.status_code,200)


    def test_getDoctorAvailabilityForDate(self):
        input = dict({backend_new.Constants.ServerConstants.DATE: "2016-10-07",
                      })
        response = self.client.get('/Doctor/getDoctorAvailabilityForDateAndApp/',input)
        self.assertEqual(response.status_code,200)


    def test_getCurrentAppBookingforDate(self):
        input = dict({backend_new.Constants.ServerConstants.DATE: "2016-10-07"
                      })
        response = self.client.get('/Doctor/getCurrentAppBookingforDate/',input)
        self.assertEqual(response.status_code,200)


    def test_getUpcomingConsultationsForDoctor(self):
        input = dict({backend_new.Constants.ServerConstants.DOCTORUID:100
                      })
        response = self.client.get('/Doctor/getUpcomingConsultationsForDoctor/',input)
        print(response.content)
        self.assertEqual(response.status_code,200)


    def test_getPastConsultationsForDoctor(self):
        input = dict({backend_new.Constants.ServerConstants.DOCTORUID:100,
                      backend_new.Constants.ServerConstants.ENDDATE: datetime.date.today() + datetime.timedelta(days=-1),
                      backend_new.Constants.ServerConstants.STARTDATE: datetime.date.today() + datetime.timedelta(days=-5)
                      })
        response = self.client.get('/Doctor/getPastConsultationsForDoctor/',input)
        self.assertEqual(response.status_code,200)


    def test_getPastConsultationsForDoctor_withEndDate(self):
        input = dict({backend_new.Constants.ServerConstants.DOCTORUID:100,
                      backend_new.Constants.ServerConstants.ENDDATE: datetime.date.today() + datetime.timedelta(days=-1)
                      })
        response = self.client.get('/Doctor/getPastConsultationsForDoctor/',input)
        self.assertEqual(response.status_code,200)


    def test_getPastConsultationsForDoctor_withStartDate(self):
        input = dict({backend_new.Constants.ServerConstants.DOCTORUID:100,
                      backend_new.Constants.ServerConstants.STARTDATE: datetime.date.today() + datetime.timedelta(days=-5)
                      })
        response = self.client.get('/Doctor/getPastConsultationsForDoctor/',input)
        self.assertEqual(response.status_code,200)


    def test_getPastConsultationsForDoctor_withDoctorId(self):
        input = dict({backend_new.Constants.ServerConstants.DOCTORUID:100
                      })
        response = self.client.get('/Doctor/getPastConsultationsForDoctor/',input)
        self.assertEqual(response.status_code,200)


    def test_getAllDoctorCategories(self):
        input = dict({backend_new.Constants.ServerConstants.CATEGORYTYPE: "doctor"
                      })
        response = self.client.get('/Doctor/getAllDoctorCategories/',input)
        self.assertEqual(response.status_code,200)


    def test_getAllDoctorCategoriesForApp(self):
        input = dict({backend_new.Constants.ServerConstants.KIOSKID:1
                      })
        response = self.client.get('/Doctor/getAllDoctorCategories/',input)
        self.assertEqual(response.status_code,200)


    def test_getAllDoctorCategories2(self):
        response = self.client.get('/Doctor/getAllDoctorCategories/')
        self.assertEqual(response.status_code,200)



# kiosk apis test

    def test_getAllApps(self):
        response = self.client.get('/Doctor/getAllApps/')
        self.assertEqual(response.status_code,200)


    def test_getAppDetails(self):
        input = dict({backend_new.Constants.ServerConstants.KIOSKID:100})
        response = self.client.get('/Doctor/getAppDetails/',input)
        self.assertEqual(response.status_code,200)


    def test_getAppConfidentials(self):
        # input = dict({backend_new.Constants.ServerConstants.KIOSKID:11
        #               })
        response = self.client.get('/Doctor/getAppConfidentials/')
        self.assertEqual(response.status_code,200)


    # def test_createApp(self):
    #     input = json.dumps({backend_new.Constants.ServerConstants.KIOSKLOCATION: "GJ",
    #                         backend_new.Constants.ServerConstants.CLIENTNAME:"ESSR",
    #                         backend_new.Constants.ServerConstants.AUTHKEY: "00-11-22-33-44-55",
    #                         backend_new.Constants.ServerConstants.AUTHSECRET:"Sdg4D456dAAfdfg",
    #                        })
    #     response = self.client.post('/Doctor/createApp/',input,Platform.Constants.RESPONSE_JSON_TYPE)
    #     print("test_createApp response: ", response.content)
    #     self.assertEqual(response.status_code,200)
    #     kioskdata = json.loads(response.content)
    #     qbsessiontoken = Platform.utils.getQBPersonSessionToken(kioskdata["body"]["qbusername"],kioskdata["body"]["qbpassword"])
    #     response = requests.delete("https://api.quickblox.com/users/"+str(kioskdata["body"]["qbid"])+".json", headers={
    #                                                                                      'QuickBlox-REST-API-Version': '0.1.0',
    #                                                                                       'QB-Token':qbsessiontoken})

    #     self.assertEqual(response.status_code,200)


# kiosk apis test
    def test_createAppServicesDoctorId(self): # doctor add for consultation service if present then update details
        input = json.dumps({backend_new.Constants.ServerConstants.KIOSKID: 100,
                            backend_new.Constants.ServerConstants.SERVICETAG: "consultation",
                            backend_new.Constants.ServerConstants.SERVICEDESCRIPTION: "Doctor Consultation",
                            backend_new.Constants.ServerConstants.SERVICECHARGES:250,
                            backend_new.Constants.ServerConstants.SERVICETITLE: "Consultation",
                            backend_new.Constants.ServerConstants.DOCTORPHONE:9769465241
                            })
        response = self.client.post('/Doctor/createAppServices/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)


    def test_createAppServices(self): # test add to services
        input = json.dumps({backend_new.Constants.ServerConstants.KIOSKID: 100,
                            backend_new.Constants.ServerConstants.SERVICETAG: "bp",
                            backend_new.Constants.ServerConstants.SERVICEDESCRIPTION: "Blood Pressure",
                            backend_new.Constants.ServerConstants.SERVICECHARGES:100,
                            backend_new.Constants.ServerConstants.SERVICETITLE: "Blood Pressure",
                            })
        response = self.client.post('/Doctor/createAppServices/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        print("response.content: ", response.content)
        self.assertEqual(response.status_code,200)

    def test_deleteDoctorAvailabilityWithAvailabilityId(self):
        input = json.dumps({backend_new.Constants.ServerConstants.AVAILABILITYID: "100"})
        response = self.client.post('/Doctor/deleteDoctorAvailability/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)

    def test_deleteDoctorAvailabilityWithRecurringId(self):
        input = json.dumps({backend_new.Constants.ServerConstants.RECURRINGID: "200"})
        input = json.dumps({
            backend_new.Constants.ServerConstants.RECURRINGID: "200",
            backend_new.Constants.ServerConstants.DAYS: 3
        })
        response = self.client.post('/Doctor/deleteDoctorAvailability/',input,Platform.Constants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)

    def test_getUpcomingConsultationsByMonth(self):
        input = dict({
            backend_new.Constants.ServerConstants.DOCTORUID: '100',
            backend_new.Constants.ServerConstants.MONTH: '3',
            backend_new.Constants.ServerConstants.YEAR: '2016'
        })
        response = self.client.get('/Doctor/getUpcomingConsultationsByMonth/',input)
        print(response.content)
        self.assertEqual(response.status_code,200)

    def test_getDoctorAvailabilityForDateAndCity(self):
        input = dict({backend_new.Constants.ServerConstants.DATE: "2016-03-26",
                      backend_new.Constants.ServerConstants.CITYNAME: "pune",
                      })
        response = self.client.get('/Doctor/getDoctorAvailabilityForDateAndCity/',input)
        self.assertEqual(response.status_code,200)
        print(response.content)








