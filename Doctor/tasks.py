import calendar
from datetime import timedelta, date, datetime

from celery import Celery
from django.core.mail import EmailMessage
from django.db.models import Sum, Case, When, Count
from django.http import HttpResponse

import Communications.utils
from Doctor.models import DoctorStatsNotification
from HealthCase.models import HealthCase
from Payment.models import HealthCaseInvoiceDetails
from Platform import backendlogger
from VideoConference.models import VCCodeDetails
from WebApp.models import WebAppDynamicLinkAnalytics
from backend_new import config
from backend_new.Constants import ServerConstants

app = Celery()

@app.task
def sendDoctorsStatsNotification():
    logmessage = []
    all_mapping_fornotification = DoctorStatsNotification.objects.all()
    '''
    daily is daily
    weekly is every sunday
    biweekly is sunday and wednesday
    monthly is last date of the month
    '''
    for thismapping in all_mapping_fornotification:
        all_apps = list(thisdoctorappmapping.appdetails.appuid for thisdoctorappmapping in thismapping.doctor.doctorappmapping_set.all())

        is_daily = False
        is_weekly = False
        is_biweekly = False
        is_monthly = False
        period = "daily"
        startdatetime = datetime.now().replace(hour=0, minute=0, second=0)
        enddatetime = datetime.now()
        lastdayofmonth = calendar.monthrange(date.today().year, date.today().month)[1]
        if thismapping.freq==DoctorStatsNotification.DAILY:
            is_daily = True
            period = "daily"
        elif thismapping.freq==DoctorStatsNotification.WEEKLY and date.today().weekday() == 6:
            is_weekly = True
            period = "weekly"
            startdatetime = startdatetime - timedelta(days=7)
        elif thismapping.freq==DoctorStatsNotification.BIWEEKLY:
            if date.today().day == 15 or date.today().day == lastdayofmonth:
                is_biweekly = True
                period = "biweekly"
                if date.today().day == 15:
                    startdatetime = startdatetime.replace(day=1)
                else:
                    startdatetime = startdatetime.replace(day=16)
        elif thismapping.freq==DoctorStatsNotification.MONTHLY and date.today().day == lastdayofmonth:
            is_monthly = True
            period = "monthly"
            startdatetime = startdatetime.replace(day=1)
        if is_daily is False and is_weekly is False and is_biweekly is False and is_monthly is False:
            continue

        healthcasesdata = HealthCase.objects.filter(lastupdated__range=(startdatetime, enddatetime), appdetails__appuid__in=all_apps,
                                                    doctor = thismapping.doctor)
        healthcasescount = healthcasesdata.count()

        videocalldata = VCCodeDetails.objects.filter(date__range=(startdatetime, enddatetime),healthcase__doctor = thismapping.doctor,
                                                     healthcase__appdetails__appuid__in=all_apps)
        videocallcount = videocalldata.count()

        invoicescreateddata = HealthCaseInvoiceDetails.objects.filter(created_at__range=(startdatetime, enddatetime), healthcase__doctor=thismapping.doctor,
                                                                      appdetails__appuid__in=all_apps)
        invoicescreateddatacount = invoicescreateddata.aggregate(Sum('invoiceamount'))["invoiceamount__sum"]
        if invoicescreateddatacount is None: invoicescreateddatacount = 0


        invoicespaiddata = HealthCaseInvoiceDetails.objects.filter(created_at__range=(startdatetime, enddatetime), healthcase__doctor=thismapping.doctor,
                                                                      appdetails__appuid__in=all_apps, paymentdone=True)
        invoicespaiddatacount = invoicespaiddata.aggregate(Sum('invoiceamount'))["invoiceamount__sum"]
        if invoicespaiddatacount is None: invoicespaiddatacount = 0

        msg = "DigiCL report {}:\nHealthCases : {}\nVideo Call Booked: {}\nInvoice created: Rs {}\nInvoice paid: Rs {}".format(period, healthcasescount,videocallcount, invoicescreateddatacount, invoicespaiddatacount)
        if thismapping.sendemail:
            email = EmailMessage()
            email.subject = "Report" + period
            email.body = msg
            email.from_email = config.EMAIL_FROM
            email.to = [thismapping.doctor.email]
            try:
                email.send()
                backendlogger.info('sendDoctorsEmailReport', msg, 'report sent', logmessage)
            except:
                backendlogger.error('sendDoctorsEmailReport', msg, 'error sending report to' + thismapping.doctor.email, logmessage)

        if thismapping.sendsms:
            Communications.utils.sendSMS(msg ,thismapping.doctor.phone, None, False)
            backendlogger.info('sendDoctorsEmailReport', msg, 'stats sms sent', logmessage)



