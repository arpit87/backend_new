from django.conf.urls import url

from Doctor import views

urlpatterns = [
    url(r'^createDoctor/', views.createDoctor, name='createDoctor'),
    url(r'^updateDoctor/', views.updateDoctor, name='updateDoctor'),
    url(r'^getDoctorDetails/', views.getDoctorDetails, name='getDoctorDetails'),
    url(r'^getServicesOfferedByDoctor/', views.getServicesOfferedByDoctor, name='getServicesOfferedByDoctor'),
    url(r'^searchDoctors/', views.searchDoctors, name='searchDoctors'),
    url(r'^getAllDoctorList/', views.getAllDoctorList, name='getAllDoctorList'),
    url(r'^getOnlineDoctors/', views.getOnlineDoctors, name='getOnlineDoctors'),
    url(r'^getDoctorsOfCategory/', views.getDoctorsOfCategory, name='getDoctorsOfCategory'),
    url(r'^getDoctorWeeklyCalender/', views.getDoctorWeeklyCalender, name='getDoctorWeeklyCalender'),
    url(r'^setOnlineStatusForDoctor/', views.setOnlineStatusForDoctor, name='setOnlineStatusForDoctor'),
    url(r'^getAllDoctorCategories/', views.getAllDoctorCategories, name='getAllDoctorCategories'),
    url(r'^getAllApps/', views.getAllApps, name='getAllApps'),
    url(r'^getAppDetails/', views.getAppDetails, name='getAppDetails'),
    url(r'^createApp/', views.createApp, name='createApp'),
    url(r'^updateAppDetails/', views.updateAppDetails, name='updateAppDetails'),
    url(r'^getAppServices/', views.getAppServices, name='getAppServices'),
    url(r'^createAppServices/', views.createAppServices, name='createAppServices'),
    url(r'^updateAppConfig/', views.updateAppConfig, name='updateAppConfig'),
    url(r'^getDoctorsForApp/', views.getDoctorsForApp, name='getDoctorsForApp'),

    # demo doctor

    url(r'^preparedoctorfordemo', views.preparedoctorfordemo, name="preparedoctorfordemo"),
    url(r'^setMailTypeForDcotor', views.setMailTypeForDoctor, name="setMailTypeForDoctor"),
    url(r'^deleteMailTypeForDoctor', views.deleteMailTypeForDoctor, name="deleteMailTypeForDoctor"),
]
