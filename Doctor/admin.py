from django.contrib import admin
from Doctor.models import DoctorDetails, DoctorWeeklyCalender, \
    SkillNumToGroup, AppDetails, DoctorAppMapping, \
    AppConfig,  DoctorStatsNotification
from import_export import resources
from import_export.admin import ImportExportModelAdmin,ImportExportMixin


#Creating import-export resource
class DoctorDetailsResource(resources.ModelResource):
    class Meta:
        model = DoctorDetails

class VSkillNumToGroupDetailsResource(resources.ModelResource):
    class Meta:
        model = SkillNumToGroup

class AppDetailsResource(resources.ModelResource):
    class Meta:
        model = AppDetails

# Register your models here.
class DoctorModelAdmin(ImportExportMixin,admin.ModelAdmin):
    list_display = ('doctorid', 'doctoruid','name','phone','primaryapp')
    resource_class = DoctorDetailsResource

admin.site.register(DoctorDetails,DoctorModelAdmin)


class DoctorWeeklyCalenderModelAdmin(admin.ModelAdmin):
    list_display = ('doctor','dayofweek','availabilitystart','availabilityend')

admin.site.register(DoctorWeeklyCalender,DoctorWeeklyCalenderModelAdmin)

class SkillNumToGroupModelAdmin(ImportExportMixin,admin.ModelAdmin):
    list_display = ('skillnum','categoryname','groupname','categorydescription')
    resource_class = VSkillNumToGroupDetailsResource

admin.site.register(SkillNumToGroup,SkillNumToGroupModelAdmin)

class AppDetailsModelAdmin(ImportExportMixin,admin.ModelAdmin):
    list_display = ('appid','appuid','applocation','apptag','appdeviceid','appsmssenderid','payuauthtoken')
    resource_class = AppDetailsResource

admin.site.register(AppDetails, AppDetailsModelAdmin)

class DoctorAppMappingModelAdmin(admin.ModelAdmin):
    list_display = ('doctor','appdetails','consultation_type')

admin.site.register(DoctorAppMapping, DoctorAppMappingModelAdmin)

class AppConfigModelAdmin(admin.ModelAdmin):
    list_display = ('appdetails','key','value')

admin.site.register(AppConfig, AppConfigModelAdmin)


class DoctorStatsNotificationModelAdmin(admin.ModelAdmin):
    list_display = ('doctor','sendemail','sendsms','freq')
admin.site.register(DoctorStatsNotification, DoctorStatsNotificationModelAdmin)