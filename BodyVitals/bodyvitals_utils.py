import requests, time, datetime, base64, math
from backend_new.Constants import (BMIRANGE, BPRANGE, PULSERANGE, MALEHYDRATIONRANGE, FEMALEHYDRATIONRANGE,
                                   FATRANGE, MALEBONEMASSRANGE, FEMALEBONEMASSRANGE,
                                   MUSCLERANGE, TEMPERATURERANGE, OXYGENSATRANGE, VFATRANGE, MUSCLEQUALITYRANGE, SFATRANGE,
                                   BLOODGLUCOSERANGE, ServerConstants)
from django.utils import timezone
from Crypto.Cipher import XOR
import random

def get_bmi_inference(bmi, age):
    try:
        user_bmi_range = BMIRANGE[str(age)]
    except KeyError:
        return {"underweight":18,"normal":27,"overweight":30}

    if bmi < user_bmi_range['underweight']:
        return 'Underweight', user_bmi_range,
    elif bmi >= user_bmi_range['underweight'] and bmi <= user_bmi_range['normal']:
        return 'Normal', user_bmi_range
    elif bmi >= user_bmi_range['normal'] and bmi <= user_bmi_range['overweight']:
        return 'Overweight', user_bmi_range
    elif bmi > user_bmi_range['overweight']:
        return 'Obese', user_bmi_range

def get_bmi_recommendations(height, bmi, age):
    try:
        user_bmi_range = BMIRANGE[str(age)]
    except KeyError:
        user_bmi_range = {"underweight":18,"normal":27,"overweight":30}

    if bmi < user_bmi_range['underweight']:
        maxkgstogain = (user_bmi_range['underweight'] - bmi) * pow((height / 100), 2)
        randomrecos = ["You need to gain around {:.1f} Kgs.".format(maxkgstogain)]
        randomrecos.extend(random.sample(BMIRECOMMENDATIONS['underweight'],math.ceil(len(BMIRECOMMENDATIONS['underweight'])/2)))
        return randomrecos
    elif bmi >= user_bmi_range['underweight'] and bmi <= user_bmi_range['normal']:
        return BMIRECOMMENDATIONS['normal']
    elif bmi >= user_bmi_range['normal'] and bmi <= user_bmi_range['overweight']:
        minkgstoloose = (bmi - user_bmi_range['normal']) * pow((height / 100), 2)
        randomrecos = ["You need to loose around {:.1f} Kgs.".format(minkgstoloose)]
        randomrecos.extend(random.sample(BMIRECOMMENDATIONS['overweight'], math.ceil(len(BMIRECOMMENDATIONS['overweight'])/2)))
        return randomrecos
    elif bmi > user_bmi_range['overweight']:
        minkgstoloose = (bmi - user_bmi_range['normal']) * pow((height / 100), 2)
        randomrecos = ["You need to loose around {:.1f} Kgs.".format(minkgstoloose)]
        randomrecos.extend(random.sample(BMIRECOMMENDATIONS['obese'], math.ceil(len(BMIRECOMMENDATIONS['obese'])/2)))
        return randomrecos

def get_vfat_inference(vfat):
    if vfat >= VFATRANGE["good"] and vfat <= VFATRANGE["high"]:
        return 'High'
    else:
        return 'Good'

def get_vfat_recommendations(vfat):
    if vfat >= VFATRANGE["good"] and vfat <= VFATRANGE["high"]:
        return random.sample(VFATRECOMMENDATIONS['high'], math.ceil(len(VFATRECOMMENDATIONS['high'])/2))
    else:
        return random.sample(VFATRECOMMENDATIONS['good'], math.ceil(len(VFATRECOMMENDATIONS['good'])/2))

def get_sfat_inference(sfat, gender):
    if gender.lower() in ['male', 'm']:
        if sfat <= SFATRANGE['m']['low']:
            return "Low"
        elif sfat <= SFATRANGE['m']["good"]:
            return 'Good'
        else:
            return 'High'
    else:
        if sfat <= SFATRANGE['f']['low']:
            return "Low"
        elif sfat <= SFATRANGE['f']["good"]:
            return 'Good'
        else:
            return 'High'


def get_bmr_inference(kcal, weight, height, age, gender):
    if gender.lower() in ['male', 'm']:
        bmr =  (10* weight) + (6.25 * height) - (5 * age) + 5
    else:
        bmr = (10 * weight) + (6.25 * height) - (6 * age) - 161

    if float(kcal) > float(bmr):
        return "Good", bmr
    else:
        return "Low", bmr


def get_bmr_recommendations(kcal, weight, height, age, gender):
    if gender.lower() in ['male', 'm']:
        bmr =  (10* weight) + (6.25 * height) - (5 * age) + 5
    else:
        bmr = (10 * weight) + (6.25 * height) - (6 * age) - 161

    if float(kcal) > float(bmr):
        return random.sample(KCALRECOMMENDATIONS['good'], math.ceil(len(KCALRECOMMENDATIONS['good']) / 2))
    else:
        return random.sample(KCALRECOMMENDATIONS['low'], math.ceil(len(KCALRECOMMENDATIONS['low']) / 2))

def get_metabolic_age_inference(metabolic_age, age):
    return "High" if metabolic_age > age else "Good"

def get_metabolic_age_recommendations(metabolic_age, age):
    if metabolic_age > age:
        return random.sample(BAGERECOMMENDATIONS['high'], math.ceil(len(BAGERECOMMENDATIONS['high']) / 2))
    else:
        return random.sample(BAGERECOMMENDATIONS['good'], math.ceil(len(BAGERECOMMENDATIONS['good']) / 2))

def get_bp_inference(bpdata, age):
    try:
        user_bp_range = BPRANGE[str(age)]
    except KeyError:
        user_bp_range = {"systolic_range": {"low": 90, "normal": 140, "prehyper": 150}, "diastolic_range": {"low": 60, "normal": 95, "prehyper": 100}}

    if bpdata["systolic"] < user_bp_range['systolic_range']["low"]:
        systolic_inference = 'Low'
    elif bpdata["systolic"] >= user_bp_range['systolic_range']["low"] and bpdata["systolic"] <= user_bp_range['systolic_range']["normal"]:
        systolic_inference = 'Normal'
    elif bpdata["systolic"] > user_bp_range['systolic_range']["normal"] and bpdata["systolic"] <= user_bp_range['systolic_range']["prehyper"]:
        systolic_inference = 'Prehyper'
    elif bpdata["systolic"] > user_bp_range['systolic_range']["prehyper"]:
        systolic_inference = 'High'

    if bpdata["diastolic"] < user_bp_range['diastolic_range']["low"]:
        diastolic_inference = 'Low'
    elif bpdata["diastolic"] >= user_bp_range['diastolic_range']["low"] and bpdata["diastolic"] <= user_bp_range['diastolic_range']["normal"]:
        diastolic_inference = 'Normal'
    elif bpdata["diastolic"] > user_bp_range['diastolic_range']["normal"] and bpdata["diastolic"] <= user_bp_range['diastolic_range']["prehyper"]:
        diastolic_inference = 'Prehyper'
    elif bpdata["diastolic"] > user_bp_range['diastolic_range']["prehyper"]:
        diastolic_inference = 'High'

    return systolic_inference, diastolic_inference, user_bp_range

def get_bp_recommendations(bpdata, age):
    bp_recommendations = []
    try:
        user_bp_range = BPRANGE[str(age)]
    except KeyError:
        user_bp_range = {"systolic_range": {"low": 90, "normal": 140, "prehyper": 150}, "diastolic_range": {"low": 60, "normal": 95, "prehyper": 100}}

    if bpdata["systolic"] < user_bp_range['systolic_range']["low"]:
        bp_recommendations.extend(random.sample(SYSTOLICRECOMMENDATIONS['low'], math.ceil(len(SYSTOLICRECOMMENDATIONS['low']) / 2)))
    elif bpdata["systolic"] >= user_bp_range['systolic_range']["low"] and bpdata["systolic"] <= user_bp_range['systolic_range']["normal"]:
        bp_recommendations.extend(random.sample(SYSTOLICRECOMMENDATIONS['normal'], math.ceil(len(SYSTOLICRECOMMENDATIONS['normal']) / 2)))
    elif bpdata["systolic"] > user_bp_range['systolic_range']["normal"] and bpdata["systolic"] <= user_bp_range['systolic_range']["prehyper"]:
        bp_recommendations.extend(random.sample(SYSTOLICRECOMMENDATIONS['prehyper'], math.ceil(len(SYSTOLICRECOMMENDATIONS['prehyper']) / 2)))
    elif bpdata["systolic"] > user_bp_range['systolic_range']["prehyper"]:
        bp_recommendations.extend(random.sample(SYSTOLICRECOMMENDATIONS['high'], math.ceil(len(SYSTOLICRECOMMENDATIONS['high']) / 2)))

    if bpdata["diastolic"] < user_bp_range['diastolic_range']["low"]:
        bp_recommendations.extend(random.sample(DIASTOLICRECOMMENDATIONS['low'], math.ceil(len(DIASTOLICRECOMMENDATIONS['low']) / 2)))
    elif bpdata["diastolic"] >= user_bp_range['diastolic_range']["low"] and bpdata["diastolic"] <= user_bp_range['diastolic_range']["normal"]:
        bp_recommendations.extend(random.sample(DIASTOLICRECOMMENDATIONS['normal'], math.ceil(len(DIASTOLICRECOMMENDATIONS['normal']) / 2)))
    elif bpdata["diastolic"] > user_bp_range['diastolic_range']["normal"] and bpdata["diastolic"] <= user_bp_range['diastolic_range']["prehyper"]:
        bp_recommendations.extend(random.sample(DIASTOLICRECOMMENDATIONS['prehyper'], math.ceil(len(DIASTOLICRECOMMENDATIONS['prehyper']) / 2)))
    elif bpdata["diastolic"] > user_bp_range['diastolic_range']["prehyper"]:
        bp_recommendations.extend(random.sample(DIASTOLICRECOMMENDATIONS['high'], math.ceil(len(DIASTOLICRECOMMENDATIONS['high']) / 2)))

    return bp_recommendations


def get_pulse_inference(pulse, age):
    try:
        user_pulse_range = PULSERANGE[str(age)]
    except KeyError:
        user_pulse_range = PULSERANGE["other"]

    if pulse < user_pulse_range['low']:
        return 'Low', user_pulse_range,
    elif pulse >= user_pulse_range['low'] and pulse < user_pulse_range['normal']:
        return 'Normal', user_pulse_range
    elif pulse >= user_pulse_range['normal']:
        return 'High', user_pulse_range

def get_pulse_recommendations(pulse, age):
    try:
        user_pulse_range = PULSERANGE[str(age)]
    except KeyError:
        user_pulse_range = PULSERANGE['other']

    if pulse < user_pulse_range['low']:
        return random.sample(PULSERECOMMENDATIONS['low'], math.ceil(len(PULSERECOMMENDATIONS['low']) / 2))
    elif pulse >= user_pulse_range['low'] and pulse < user_pulse_range['normal']:
        return random.sample(PULSERECOMMENDATIONS['normal'], math.ceil(len(PULSERECOMMENDATIONS['normal']) / 2))
    elif pulse >= user_pulse_range['normal']:
        return random.sample(PULSERECOMMENDATIONS['high'], math.ceil(len(PULSERECOMMENDATIONS['high']) / 2))

def download_file(download_url, filename):
    r = requests.get(download_url, stream = True, verify=False)
    filepath = "home/yolo/Downloads/{}.pdf".format(filename)
    with open(filepath,"wb") as pdf:
        for chunk in r.iter_content(chunk_size=1024):
             # writing one chunk at a time to pdf file
             if chunk:
                 pdf.write(chunk)

    print('Download Completed!')


def get_required_structure(data):
        # data = {"partner": "yolo",
    #         "recipient": "akash.wankhede@yolohealth.in",
    #         "reportdata": {"name":"akash W","age":25,"gender":"Male","kiosklocation":"Yolo-Baner","height":"5'9\"","date":"13 Nov 2018","weight":79,"bmi":25.72,"fat":0,"muscle":0,"hydration":0,"bonemass":0,"BMIrangesforage":{"underweight":18,"normal":27,"overweight":30},"bmiRange":"Normal","systolic":130,"diastolic":80,"pulse1":0,"systolic_range":{"low":90,"normal":130,"prehyper":140},"diastolic_range":{"low":60,"normal":90,"prehyper":100},"sysbpRange":"Normal","diabpRange":"Normal","pulse":90,"oxygensat":90,"pulseShowRange":{"low":60,"normal":90},"pulseRange":"High","oxygensatRange":"Normal","temperature":98,"temperatureincelsius":"36.67","healthscore":"66","healthscoreSmiley":"sad.png","location":"Yolo-Baner","partnerphone":"1800 1020 333","partneremail":"support@yolohealth.in","partnerwebsite":"www.yolohealth.in","partneraddress":"Yolohealth, Plot-3A, Balewadi Phata,Baner,Pune","personid_externalid":30202},
    #         "reporttype": 1}

    # data = {"weight": 68.8, "kiosklocation": "Yolo-Test-Prod", "historytype": "kioskreport", "datetime": 1539067845.0, "bpmachine": null, "reportkey": "1539067760dBboHpVk30091",
    #         "temperaturemachine": null, "malariamachine": null, "hemoglobinmachine": null, "oximetermachine": null, "eyemachine": null, "weightmachine": null, "spirometermachine": null,
    #         "email": "prajwal.yawale@gmail.com", "username": "prajwal", "denguemachine": null, "ecgmachine": null, "hivmachine": null, "pregnancymachine": {"c": 0.0, "t": 0.0},
    #         "reporttype": 9, "healthscore": "23", "height": 171.75, "bloodglucosemachine": null, "commentlist": [], "urinemachine": {"bilirubin": "Negative", "urobilinogen": "17",
    #         "gravity": "1.000", "nitrite": "Negative", "ketone": "Negative", "blood": "Negative", "ph": "5.0", "protein": "Negative", "leukocytes": "Negative", "glucose": "Negative"},
    #         "heightmachine": {"height": 171.75}, "gender": "Male", "age": 26, "personuid": 30091, "reportid": "dBboHpVk", "lipidmachine": null, "typhoidmachine": null}

    # height_in_inch = parse_height_to_inch(data.get("height"))
    # datestr = parse_epoch_to_datestr(data.get("datetime"))

    if data.get("weightmachine"):
        bmi = data.get("weightmachine")['bmi']
        fat = data.get("weightmachine")['fat']
        muscle = data.get("weightmachine")['muscle']
        hydration = data.get("weightmachine")['hydration']
        hydration_inference = data.get("weightmachine")['hydration_inference']
        bonemass_inference = data.get("weightmachine")['bonemass_inference']
        muscle_inference = data.get("weightmachine")['muscle_inference']
        bonemass = data.get("weightmachine")['bonemass']
        bmi_inference = data.get("weightmachine")['bmi_inference']
        fat_inference = data.get("weightmachine")['fat_inference']
        user_bmi_range = data.get("weightmachine")['user_bmi_range']
    else:
        bmi = fat = muscle = hydration = bonemass = bmi_inference = user_bmi_range = hydration_inference = fat_inference = bonemass_inference = muscle_inference = None

    if data.get("bpmachine"):
        systolic = data.get("bpmachine")['systolic']
        diastolic = data.get("bpmachine")['diastolic']
        pulse1 = data.get("bpmachine")['pulse']
        systolic_inference = data.get("bpmachine")['systolic_inference']
        diastolic_inference = data.get("bpmachine")['diastolic_inference']
        systolic_range = data.get("bpmachine")['systolic_range']
        diastolic_range = data.get("bpmachine")['diastolic_range']
    else:
        systolic = diastolic = systolic_inference = diastolic_inference = systolic_range = diastolic_range = pulse1 = None

    if data.get("oximetermachine"):
        pulse = data.get("oximetermachine")['pulse']
        oxygensat = data.get("oximetermachine")['oxygensat']
        pulse_inference = data.get("oximetermachine")['pulse_inference']
        pulse_range = data.get("oximetermachine")['pulse_range']
    else:
        pulse = oxygensat = pulse_inference = pulse_range = None

    if data.get("temperaturemachine"):
        temperature = data.get("temperaturemachine")['temperature']
        temperature_cels = parse_temperature_to_celsius(data.get("temperaturemachine")['temperature'])
    else:
        temperature = temperature_cels = None

    res = {"partner": "digiclinics",
           "recipient": data.get("email"),
           "reportdata": {"name": data.get("username"), "age": data.get("age"), "gender": data.get("gender"), "kiosklocation": data.get("kiosklocation"), "height": data.get("height"),
                          "date": data.get("datetime"), "weight": data.get("weight"), "bmi": bmi, "fat": fat, "fatRange": fat_inference, "muscle": muscle, "muscleRange": muscle_inference,
                          "hydration": hydration, "hydrationRange": hydration_inference, "bonemass": bonemass, "bonemassRange": bonemass_inference, "BMIrangesforage": user_bmi_range,
                          "bmiRange": bmi_inference , "systolic": systolic, "diastolic": diastolic, "pulse1":pulse1, "systolic_range": systolic_range, "diastolic_range": diastolic_range,
                          "sysbpRange": systolic_inference, "diabpRange": diastolic_inference, "pulse": pulse, "oxygensat": oxygensat, "pulseShowRange": pulse_range, "pulseRange": pulse_inference,
                          "oxygensatRange": "Normal", "temperature": temperature, "temperatureincelsius": temperature_cels, "healthscore": data.get("healthscore"), "healthscoreSmiley": "sad.png",
                          "location": data.get("kiosklocation"), "partnerphone": "1800 1020 333", "partneremail": "support@yolohealth.in", "partnerwebsite": "www.yolohealth.in",
                          "partneraddress": "Yolohealth, Plot-3A, Balewadi Phata,Baner,Pune", "personuid": 30202},
           "reporttype": 1}

    return res

# def parse_height_to_inch(height):
#     return float(height)/2.54

def parse_temperature_to_celsius(temp):
    return round((temp - 32) / 1.8, 2) if temp else None


def get_hydration_inference(value, age):
    HYDRATIONRANGE = MALEHYDRATIONRANGE if age.lower() == 'm' else FEMALEHYDRATIONRANGE
    if value < HYDRATIONRANGE['low']:
        return 'Low', HYDRATIONRANGE
    elif value >= HYDRATIONRANGE['low'] and value <= HYDRATIONRANGE['normal']:
        return 'Normal', HYDRATIONRANGE
    elif value > HYDRATIONRANGE['normal']:
        return 'Good', HYDRATIONRANGE

def get_hydration_recommendations(value, gender):
    HYDRATIONRANGE = MALEHYDRATIONRANGE if gender.lower() == 'm' else FEMALEHYDRATIONRANGE
    if value < HYDRATIONRANGE['low']:
        return random.sample(HYDRAIONRECOMMENDATIONS['low'],math.ceil(len(HYDRAIONRECOMMENDATIONS['low'])/2))
    elif value >= HYDRATIONRANGE['low'] and value <= HYDRATIONRANGE['normal']:
        return random.sample(HYDRAIONRECOMMENDATIONS['normal'],math.ceil(len(HYDRAIONRECOMMENDATIONS['normal'])/2))
    elif value > HYDRATIONRANGE['normal']:
        return random.sample(HYDRAIONRECOMMENDATIONS['good'],math.ceil(len(HYDRAIONRECOMMENDATIONS['good'])/2))

def get_fat_inference(value, gender, age):
    FATRANGEBYGENDER = FATRANGE["m"] if gender.lower() in ['male', 'm'] else FATRANGE["f"]
    if isinstance(age, int):
        if int(age) in range(2, 60):
            FATRANGEBYAGE = FATRANGEBYGENDER[str(age)]
        else:
            FATRANGEBYAGE = FATRANGEBYGENDER["other"]
    else:
        FATRANGEBYAGE = FATRANGEBYGENDER["other"]

    if value <= FATRANGEBYAGE['atheletic']:
        return 'Athletic', FATRANGEBYAGE
    elif value > FATRANGEBYAGE['atheletic'] and value <= FATRANGEBYAGE['good']:
        return 'Good', FATRANGEBYAGE
    elif value > FATRANGEBYAGE['good'] and value <= FATRANGEBYAGE['acceptable']:
        return 'Acceptable', FATRANGEBYAGE
    elif value > FATRANGEBYAGE['acceptable'] and value <= FATRANGEBYAGE['overweight']:
        return 'Overweight', FATRANGEBYAGE
    elif value > FATRANGEBYAGE['overweight']:
        return 'Obese', FATRANGEBYAGE

def get_fat_recommendations(value, gender, age):
    FATRANGEBYGENDER = FATRANGE["m"] if gender.lower() in ['male', 'm'] else FATRANGE["f"]
    if isinstance(age, int):
        if int(age) in range(2, 60):
            FATRANGEBYAGE = FATRANGEBYGENDER[str(age)]
        else:
            FATRANGEBYAGE = FATRANGEBYGENDER["other"]
    else:
        FATRANGEBYAGE = FATRANGEBYGENDER["other"]

    if value <= FATRANGEBYAGE['atheletic']:
        return random.sample(FATRECOMMENDATIONS['atheletic'],math.ceil(len(FATRECOMMENDATIONS['atheletic'])/2))
    elif value > FATRANGEBYAGE['atheletic'] and value <= FATRANGEBYAGE['good']:
        return random.sample(FATRECOMMENDATIONS['good'],math.ceil(len(FATRECOMMENDATIONS['atheletic'])/2))
    elif value > FATRANGEBYAGE['good'] and value <= FATRANGEBYAGE['acceptable']:
        return random.sample(FATRECOMMENDATIONS['acceptable'],math.ceil(len(FATRECOMMENDATIONS['atheletic'])/2))
    elif value > FATRANGEBYAGE['acceptable'] and value <= FATRANGEBYAGE['overweight']:
        return random.sample(FATRECOMMENDATIONS['overweight'],math.ceil(len(FATRECOMMENDATIONS['atheletic'])/2))
    elif value > FATRANGEBYAGE['overweight']:
        return random.sample(FATRECOMMENDATIONS['obese'],math.ceil(len(FATRECOMMENDATIONS['atheletic'])/2))

def get_muscle_quality_inference(value, gender, age):
    MUSCLEQUALITYRANGEBYGENDER = MUSCLEQUALITYRANGE["m"] if gender.lower() in ['male', 'm'] else MUSCLEQUALITYRANGE["f"]
    if isinstance(age, int):
        if int(age) in range(18, 30+1):
            MUSCLEQUALITYRANGEBYAGE = MUSCLEQUALITYRANGEBYGENDER["18"]
        elif int(age) in range(30, 40+1):
            MUSCLEQUALITYRANGEBYAGE = MUSCLEQUALITYRANGEBYGENDER["30"]
        elif int(age) in range(40, 50+1):
            MUSCLEQUALITYRANGEBYAGE = MUSCLEQUALITYRANGEBYGENDER["40"]
        elif int(age) in range(50, 60+1):
            MUSCLEQUALITYRANGEBYAGE = MUSCLEQUALITYRANGEBYGENDER["50"]
        elif int(age) in range(60, 70+1):
            MUSCLEQUALITYRANGEBYAGE = MUSCLEQUALITYRANGEBYGENDER["60"]
        elif int(age) in range(70, 80+1):
            MUSCLEQUALITYRANGEBYAGE = MUSCLEQUALITYRANGEBYGENDER["70"]
        elif int(age) > 80:
            MUSCLEQUALITYRANGEBYAGE = MUSCLEQUALITYRANGEBYGENDER["80"]
        else:
            MUSCLEQUALITYRANGEBYAGE = {"low": 0, "normal": 0}
    else:
        MUSCLEQUALITYRANGEBYAGE = {"low": 0, "normal": 0}

    if value < MUSCLEQUALITYRANGEBYAGE['low']:
        return 'Low', MUSCLEQUALITYRANGEBYAGE
    elif value >= MUSCLEQUALITYRANGEBYAGE['low'] and value < MUSCLEQUALITYRANGEBYAGE['normal']:
        return 'Normal', MUSCLEQUALITYRANGEBYAGE
    elif value >= MUSCLEQUALITYRANGEBYAGE['normal']:
        return 'Good', MUSCLEQUALITYRANGEBYAGE


def get_bonemass_inference(value, gender):
    BONEMASSRANGE = MALEBONEMASSRANGE if gender.lower() == 'm' else FEMALEBONEMASSRANGE
    if value < BONEMASSRANGE['low']:
        return 'Low', BONEMASSRANGE
    elif value >= BONEMASSRANGE['low'] and value <= BONEMASSRANGE['normal']:
        return 'Normal', BONEMASSRANGE
    elif value > BONEMASSRANGE['normal']:
        return 'Good', BONEMASSRANGE

def get_bonemass_recommendations(value, gender):
    BONEMASSRANGE = MALEBONEMASSRANGE if gender.lower() == 'm' else FEMALEBONEMASSRANGE
    if value < BONEMASSRANGE['low']:
        return random.sample(BONEMASSRECOMMENDATIONS['low'],math.ceil(len(BONEMASSRECOMMENDATIONS['low'])/2))
    elif value >= BONEMASSRANGE['low'] and value <= BONEMASSRANGE['normal']:
        return random.sample(BONEMASSRECOMMENDATIONS['normal'],math.ceil(len(BONEMASSRECOMMENDATIONS['normal'])/2))
    elif value > BONEMASSRANGE['normal']:
        return random.sample(BONEMASSRECOMMENDATIONS['good'],math.ceil(len(BONEMASSRECOMMENDATIONS['good'])/2))

def get_muscle_inference(value, gender, age):
    MUSCLERANGEBYGENDER = MUSCLERANGE["m"] if gender.lower() in ['male', 'm'] else MUSCLERANGE["f"]
    if isinstance(age, int):
        if int(age) in range(2, 19):
            MUSCLERANGEBYAGE = MUSCLERANGEBYGENDER[str(age)]
        else:
            MUSCLERANGEBYAGE = MUSCLERANGEBYGENDER["18"]
    else:
        MUSCLERANGEBYAGE = MUSCLERANGEBYGENDER["18"]

    if value < MUSCLERANGEBYAGE['low']:
        return 'Low', MUSCLERANGEBYAGE
    elif value >= MUSCLERANGEBYAGE['low'] and value <= MUSCLERANGEBYAGE['normal']:
        return 'Normal', MUSCLERANGEBYAGE
    elif value > MUSCLERANGEBYAGE['normal']:
        return 'Good', MUSCLERANGEBYAGE

def get_muscle_recommendations(value, gender, age):
    MUSCLERANGEBYGENDER = MUSCLERANGE["m"] if gender.lower() in ['male', 'm'] else MUSCLERANGE["f"]
    if isinstance(age, int):
        if int(age) in range(2, 19):
            MUSCLERANGEBYAGE = MUSCLERANGEBYGENDER[str(age)]
        else:
            MUSCLERANGEBYAGE = MUSCLERANGEBYGENDER["18"]
    else:
        MUSCLERANGEBYAGE = MUSCLERANGEBYGENDER["18"]

    if value < MUSCLERANGEBYAGE['low']:
        return random.sample(MUSCLERECOMMENDATIONS['low'],math.ceil(len(MUSCLERECOMMENDATIONS['low'])/2))
    elif value >= MUSCLERANGEBYAGE['low'] and value <= MUSCLERANGEBYAGE['normal']:
        return random.sample(MUSCLERECOMMENDATIONS['normal'],math.ceil(len(MUSCLERECOMMENDATIONS['normal'])/2))
    elif value > MUSCLERANGEBYAGE['normal']:
        return  random.sample(MUSCLERECOMMENDATIONS['good'],math.ceil(len(MUSCLERECOMMENDATIONS['good'])/2))

def get_temperature_recommendations(value):
    if value < TEMPERATURERANGE['low']:
        return random.sample(TEMPERATURERECOMMENDATIONS['low'],math.ceil(len(TEMPERATURERECOMMENDATIONS['low'])/2))
    elif value >= TEMPERATURERANGE['low'] and value <= TEMPERATURERANGE['normal']:
        return random.sample(TEMPERATURERECOMMENDATIONS['normal'],math.ceil(len(TEMPERATURERECOMMENDATIONS['normal'])/2))
    elif value > TEMPERATURERANGE['normal']:
        return random.sample(TEMPERATURERECOMMENDATIONS['high'],math.ceil(len(TEMPERATURERECOMMENDATIONS['high'])/2))

def get_temperature_inference(value):
    if value < TEMPERATURERANGE['low']:
        return 'Low', TEMPERATURERANGE
    elif value >= TEMPERATURERANGE['low'] and value <= TEMPERATURERANGE['normal']:
        return 'Normal', TEMPERATURERANGE
    elif value > TEMPERATURERANGE['normal']:
        return 'High', TEMPERATURERANGE

def get_oxygensat_inference(value):
    if value < OXYGENSATRANGE['low']:
        return 'Low', OXYGENSATRANGE
    elif value >= OXYGENSATRANGE['low'] and value <= OXYGENSATRANGE['normal']:
        return 'Normal', OXYGENSATRANGE
    elif value > OXYGENSATRANGE['normal']:
        return 'Good', OXYGENSATRANGE

def get_oxygensat_recommendations(value):
    if value < OXYGENSATRANGE['low']:
        return  random.sample(OXYGENSATRECOMMENDATIONS['low'],math.ceil(len(OXYGENSATRECOMMENDATIONS['low'])/2))
    elif value >= OXYGENSATRANGE['low'] and value <= OXYGENSATRANGE['normal']:
        return random.sample(OXYGENSATRECOMMENDATIONS['normal'],math.ceil(len(OXYGENSATRECOMMENDATIONS['normal'])/2))
    elif value > OXYGENSATRANGE['normal']:
        return random.sample(OXYGENSATRECOMMENDATIONS['good'],math.ceil(len(OXYGENSATRECOMMENDATIONS['good'])/2))

def get_bg_inference(value, testtypecode):
    if value < BLOODGLUCOSERANGE[testtypecode]["low"]:
        return 'Low', BLOODGLUCOSERANGE[testtypecode]
    elif value >= BLOODGLUCOSERANGE[testtypecode]["low"] and value <= BLOODGLUCOSERANGE[testtypecode]["normal"]:
        return 'Normal', BLOODGLUCOSERANGE[testtypecode]
    elif value >= BLOODGLUCOSERANGE[testtypecode]["normal"] and value <= BLOODGLUCOSERANGE[testtypecode]["prediabetic"]:
        return 'Pre Diabetic', BLOODGLUCOSERANGE[testtypecode]
    elif value > BLOODGLUCOSERANGE[testtypecode]["prediabetic"]:
        return 'High', BLOODGLUCOSERANGE[testtypecode]


def get_bg_recommendations(value, testtypecode):
    if value < BLOODGLUCOSERANGE[testtypecode]["low"]:
        return random.sample(GLUCOSERECOMMENDATIONS['low'],math.ceil(len(GLUCOSERECOMMENDATIONS['low'])/2))
    elif value >= BLOODGLUCOSERANGE[testtypecode]["low"] and value <= BLOODGLUCOSERANGE[testtypecode]["normal"]:
        return random.sample(GLUCOSERECOMMENDATIONS['normal'],math.ceil(len(GLUCOSERECOMMENDATIONS['normal'])/2))
    elif value >= BLOODGLUCOSERANGE[testtypecode]["normal"] and value <= BLOODGLUCOSERANGE[testtypecode]["prediabetic"]:
        return  random.sample(GLUCOSERECOMMENDATIONS['prediabetic'],math.ceil(len(GLUCOSERECOMMENDATIONS['prediabetic'])/2))
    elif value > BLOODGLUCOSERANGE[testtypecode]["prediabetic"]:
        return  random.sample(GLUCOSERECOMMENDATIONS['high'],math.ceil(len(GLUCOSERECOMMENDATIONS['high'])/2))

def apply_dynamic_ranges(report_data):
    if report_data.get('gender').lower() in ['male', 'm']:
        report_data['fat_range'] = FATRANGE["m"][str(report_data.get('age'))] if report_data.get('age') in range(2, 60) else FATRANGE["m"]["other"]
        report_data['bonemass_range'] = MALEBONEMASSRANGE
        report_data['hydration_range'] = MALEHYDRATIONRANGE
        report_data['muscle_range'] = MUSCLERANGE["m"][str(report_data.get('age'))] if report_data.get('age') in range(2, 19) else MUSCLERANGE["m"]["18"]
    else:
        report_data['fat_range'] = FATRANGE["f"][str(report_data.get('age'))] if report_data.get('age') in range(2, 60) else FATRANGE["f"]["other"]
        report_data['bonemass_range'] = FEMALEBONEMASSRANGE
        report_data['hydration_range'] = FEMALEHYDRATIONRANGE
        report_data['muscle_range'] = MUSCLERANGE["f"][str(report_data.get('age'))] if report_data.get('age') in range(2, 19) else MUSCLERANGE["f"]["18"]

    return report_data

LIPIDRANGE = {
    "tc": {"optimal": 200, "near_optimal": 240},
    "hdl": {"optimal": 40, "near_optimal": 59},
    "ldl": {"optimal": 100, "near_optimal": 160, "high": 190},
    "tg": {"optimal": 149, "near_optimal": 199, "high": 499},
    }


def get_tc_inference(value):
    if value < LIPIDRANGE["tc"]['optimal']:
        return 'Optimal', LIPIDRANGE["tc"]
    elif value >= LIPIDRANGE["tc"]['optimal'] and value <= LIPIDRANGE["tc"]['near_optimal']:
        return 'Near Optimal', LIPIDRANGE["tc"]
    elif value > LIPIDRANGE["tc"]['near_optimal']:
        return 'High', LIPIDRANGE["tc"]

def get_hdl_inference(value):
    if value < LIPIDRANGE["hdl"]['optimal']:
        return 'High', LIPIDRANGE["hdl"]
    elif value >= LIPIDRANGE["hdl"]['optimal'] and value <= LIPIDRANGE["hdl"]['near_optimal']:
        return 'Near Optimal', LIPIDRANGE["hdl"]
    elif value > LIPIDRANGE["hdl"]['near_optimal']:
        return 'Optimal', LIPIDRANGE["hdl"]

def get_ldl_inference(value):
    if value < LIPIDRANGE["ldl"]['optimal']:
        return 'Optimal', LIPIDRANGE["ldl"]
    elif value >= LIPIDRANGE["ldl"]['optimal'] and value <= LIPIDRANGE["ldl"]['near_optimal']:
        return 'Near Optimal', LIPIDRANGE["ldl"]
    elif value > LIPIDRANGE["ldl"]['near_optimal'] and value <= LIPIDRANGE["ldl"]['high']:
        return 'High', LIPIDRANGE["ldl"]
    elif value > LIPIDRANGE["ldl"]['high']:
        return 'Very High', LIPIDRANGE["ldl"]

def get_tg_inference(value):
    if value < LIPIDRANGE["tg"]['optimal']:
        return 'Optimal', LIPIDRANGE["tg"]
    elif value >= LIPIDRANGE["tg"]['optimal'] and value <= LIPIDRANGE["tg"]['near_optimal']:
        return 'Near Optimal', LIPIDRANGE["tg"]
    elif value > LIPIDRANGE["tg"]['near_optimal'] and value <= LIPIDRANGE["tg"]['high']:
        return 'High', LIPIDRANGE["tg"]
    elif value > LIPIDRANGE["tg"]['high']:
        return 'Very High', LIPIDRANGE["tg"]

HBRANGE = {
    "m": {"low": 13.5, "normal": 17.5},
    "f": {"low": 12, "normal": 15.5},
}

def get_hb_inference(value, gender):
    HBRANGEBYGENDER = HBRANGE["m"] if gender.lower() in ['male', 'm'] else HBRANGE["f"]
    if value < HBRANGEBYGENDER["low"]:
        return 'Low', HBRANGEBYGENDER
    elif value >= HBRANGEBYGENDER["low"] and value <= HBRANGEBYGENDER["normal"]:
        return 'Normal', HBRANGEBYGENDER
    elif value > HBRANGEBYGENDER["normal"]:
        return 'High', HBRANGEBYGENDER

# For test Count (Analyst)
def get_hiv_inference(hiv_req):
    testname = None
    try:
        c = hiv_req.c
        t1 = hiv_req.t1
        t2 = hiv_req.t2
    except AttributeError:
        c = hiv_req[ServerConstants.C]
        t1 = hiv_req[ServerConstants.T1]
        t2 = hiv_req[ServerConstants.T2]

    if c == 0 and t1 == 0 and t2 == 0:
       testname = "Invalid"
    elif c == 1 and t1 == 0 and t2 == 0:
        testname = "Negative"
    elif c == 1 and t1 == 1 and t2 == 0:
        testname = "HIV 1 Positive"
    elif c == 1 and t1 == 0 and t2 == 1:
        testname = "HIV 2 Positive"
    elif c == 1 and t1 == 1 and t2 == 1:
       testname = "HIV 1 & 2 Positive"

    return testname

def get_dengue_inference(dengue_req):
    testname = None
    try:
        c = dengue_req.c
        ns1 = dengue_req.ns1
    except AttributeError:
        c = dengue_req[ServerConstants.C]
        ns1 = dengue_req[ServerConstants.NS1]

    if c == 0 and ns1 == 0:
       testname = "Invalid"
    elif c == 0 and ns1 == 1:
        testname = "Invalid"
    elif c == 1 and ns1 == 0:
        testname = "Negative"
    elif c == 1 and ns1 == 1:
        testname = "Positive"

    return testname


def getPregnancyTestName(pregnancy_req):
    testname = None
    try:
        c = pregnancy_req.c
        t = pregnancy_req.t
    except AttributeError:
        c = pregnancy_req[ServerConstants.C]
        t = pregnancy_req[ServerConstants.T]


    if c == 0 and t == 0:
       testname = "Invalid"
    elif c == 0 and t == 1:
        testname = "Invalid"
    elif c == 1 and t == 0:
        testname = "Negative"
    elif c == 1 and t == 1:
        testname = "Positive"

    return testname


def get_malaria_inference(malaria_req):
    testname = None
    try:
        c = malaria_req.c
        t1 = malaria_req.t1
        t2 = malaria_req.t2
    except AttributeError:
        c = malaria_req[ServerConstants.C]
        t1 = malaria_req[ServerConstants.T1]
        t2 = malaria_req[ServerConstants.T2]

    if c == 0 and t1 == 0 and t2 == 0:
       testname = "Invalid"
    elif c == 1 and t1 == 0 and t2 == 0:
        testname = "Negative"
    elif c == 1 and t1 == 1 and t2 == 0:
        testname = "Malaria 1 Positive"
    elif c == 1 and t1 == 0 and t2 == 1:
        testname = "Malaria 2 Positive"
    elif c == 1 and t1 == 1 and t2 == 1:
       testname = "Malaria 1 & 2 Positive"

    return testname


def get_typhoid_inference(typhoid_req):
    testname = None
    try:
        mc = typhoid_req.mc
        mt = typhoid_req.mt
        gc = typhoid_req.gc
        gt = typhoid_req.gt
    except AttributeError:
        mc = typhoid_req[ServerConstants.MC]
        mt = typhoid_req[ServerConstants.MT]
        gc = typhoid_req[ServerConstants.GC]
        gt = typhoid_req[ServerConstants.GT]

    if mc == 0 and mt == 1 and gc == 0 and gt == 1:
        testname = "Invalid"
    elif mc == 0 and mt == 0 and gc == 0 and gt == 0:
        testname = "Invalid"
    elif mc == 1 and mt == 0 and gc == 1 and gt == 0:
        testname = "Negative"
    elif mc == 1 and mt == 1 and gc == 1 and gt == 0:
        testname = "Typhoid lgM Positive"
    elif mc == 1 and mt == 0 and gc == 1 and gt == 1:
        testname = "Typhoid lgG Positive"
    elif mc == 1 and mt == 1 and gc == 1 and gt == 1:
        testname = "Typhoid lgM & lgG Positive"

    return testname

def get_localize_datetime(server_datetime):
    return datetime.datetime.fromtimestamp((time.mktime(timezone.localtime(server_datetime).timetuple()))).strftime('%d %b %Y %H:%M')

def encrypt(key, plaintext):
  cipher = XOR.new(key)
  return base64.b64encode(cipher.encrypt(plaintext)).decode('utf-8')

def decrypt(key, ciphertext):
  cipher = XOR.new(key)
  return cipher.decrypt(base64.b64decode(ciphertext))

def convert_feets_to_cms(len_in_feets):
    return len_in_feets * 30.48

BMIRECOMMENDATIONS = {
    'underweight': [
        "Eat five to six times a day (3 large meals and 3 mid meal snacks)",
        "Eat foods with Full fat and avoid low calorie diet",
        "Recommended food options: Banana, Milk, Dried Fruits, avacados",
        "Consult a Dietician to help you gain weight in a healthy way with the foods you enjoy",
        "Increase calories by adding nut or seed toppings, cheese, and healthy side dishes",
        "Try almonds, sunflower seeds, fruit, or whole-grain, wheat toast",
        "Consider high-protein meats, which can help you to build muscle",
        "Choose nutritious carbohydrates, such as brown rice and other whole grains",
        "Enjoy snacks that contain plenty of protein and healthy carbohydrates",
        "Consider options like trail mix, protein bars or drinks, and crackers with hummus or peanut butter",
        "Consider eating smaller meals throughout the day to increase your calorie intake",
        "Include weightlifting or yoga in your daily routine. You gain weight by building muscle"
    ],
    'normal': [
        "Healthy weight can be maintained by regularly doing exercise and eating healthy food."
    ],
    'overweight':[
        "Recommending to reduce saturated fats like cream, cheese, butter, other whole milk dairy products and fatty meats which also contain dietary cholesterol",
        "Drink Coffee (Preferably Black).Just make sure not to add a bunch of sugar or other high-calorie ingredients to your coffee",
        "Reduce carbohydrate intake like roti rice, sugar, potatoes",
        "Try Intermittent Fasting.It may reduce the loss of muscle mass typically associated with low-calorie diets",
        "Cut Back on Added Sugar",
        "Eat less refined carbs and make sure to eat them with their natural fiber",
        "Eat fiber rich foods like fruits, salads, whole wheat grains",
        "Exercise Portion Control or Count Calories",
        "Eat More Vegetables and Fruits",
        "Increase physical activity by doing Cardio activities, Yoga, or any other form of exercise at least 5 times a week",
        "Lift Weights,Eat More Fiber"
    ],
    'obese':[
        "Eat at least four servings of vegetables and three servings of fruits daily",
        "Replace refined grains with whole grains",
        "Use modest amounts of healthy fats, such as olive oil, vegetable oils, avocados, nuts, and nut butters and oils",
        "Cut back on sugar",
        "Choose low-fat dairy products and lean meat and poultry in limited amounts",
        "Avoid Foods That Contain Trans Fats. We recommend you to take a diet plan to guide you loose weight",
        "Recommending to reduce saturated fats like cream, cheese, butter, other whole milk dairy products and fatty meats which also contain dietary cholesterol",
        "Eat fiber rich foods like fruits, salads, whole wheat grains",
        "Eat More Protein",
        "Increase physical activity by doing Cardio activities, Yoga, or any other form of exercise at least 5 times a week",
        "Don't Do Sugary Drinks, Including Soda and Fruit Juice",
        "Eat Whole, Single-Ingredient Foods.Don't Diet — Eat Healthy Instead",
    ]
}

FATRECOMMENDATIONS = {
    'atheletic':[],
    'good':[
        "To maintain the fate percentage at normal range, focus on exercising and eating healthy food."
    ],
    'acceptable':[
        "You are in the acceptable range of fat percentage, please focus on exercising " ,
         "Eat healthy food , low on carbohydrates and saturated fats."
    ],
    'overweight':["You are in category of OverFAT. Reduce the intake of sugar and refined carbohydrates",
                  "Fill up on non-starchy vegetables, fats and proteins, Exercise regularly." ,
                  "Reduce stress and Focus on getting enough sleep"],
    'obese':["You are falling in Obese category of FAT. Please reduce the intake of sugar and refined carbohydrates",
             "Eat non-starchy vegetables, fats and proteins, Exercise regularly, Reduce stress" ,
             "Focus on getting enough sleep."]
}

VFATRECOMMENDATIONS = {
    'high': [
         "Visceral fat is the fat that is in the internal abdominal cavity, surrounding the vital organs in the abdominal area. ",
         "Consider making changes in your lifestyle by changing your diet or exercising more",
         "Ensuring you have healthy levels of visceral fat may reduce the risk of certain diseases such as heart disease, high blood pressure, and the onset of type 2 diabetes"
        ],
    'good': [
        "Visceral fat is the fat that is in the internal abdominal cavity, surrounding the vital organs.Continue monitoring to ensure that it stays within this healthy range"
    ]
}

BAGERECOMMENDATIONS = {
    'high': [
        "Your metabolic age is higher than your actual age, it is an indication that you need to improve your metabolic rate "
    ],
    'good': [
        "Your metablic age is fine. You have healthy muscle tissue which improves your metabolic age "
    ]
}

KCALRECOMMENDATIONS = {
    'good': ["Basal Metabolic Rate (BMR) is the minimum level of energy your body needs when at rest to function effectively. "
             "This includes the functioning of your respiratory and circulatory organs, neural system, liver, kidneys, and other organs"],
    'low': [
        "Having a higher basal metabolism increases the number of calories used and helps decrease the amount of body fat.",
        "A low basal metabolic rate makes it harder to lose body fat and overall weight"]
}

MUSCLERECOMMENDATIONS = {
    'low': [
        "If your muscle mass is low, then exercise regularly and eat food rich in protein like pulses, fish, lean chicken, yogurt, eggs, soy product, dry fruits."
    ],
    'normal': [
        "Exercise regularly and eat food rich in protein like pulses, fish, lean chicken, yogurt, eggs, soy product, dry fruits to maintain your muscle mass."
    ],
    'good': [
        "This is an indication of a fit body. High muscle mass is usually present in people doing high intensity workout"
    ]
}

HYDRAIONRECOMMENDATIONS = {
    'low': [
        "We lose water constantly through sweat, urine and breathing.  To keep hydrated drink about 2 liters of water per day, more if you are physically active or exercising."
    ],
    'normal': [
        "Total Body Water Percentage is the total amount of fluid in a person’s body expressed as a percentage of their total weight."
    ],
    'good': [
        "Keep maintaining a healthy total body water percentage so that the body functions efficiently and reduces the risk of developing associated health problems"
    ]
}

BONEMASSRECOMMENDATIONS = {
    'low': [
        "To increase the bone mass, try exercising regularly, eating foods high in calcium like Milk, Yoghurt, Paneer. ",
        "Banana has high amount of Phosphorous and Vitamin D from egg yolk, cheese, fish, Foods fortified with vitamin D, like some dairy products, orange juice, soy milk"
    ],
    'normal': [
        "To maintain the bone mass, try exercising regularly, eating foods high in calcium like Milk, Yoghurt, Paneer. ",
        "Banana has high amount of Phosphorous and Vitamin D from egg yolk, cheese, fish, Foods fortified with vitamin D, like some dairy products, orange juice, soy milk."
    ],
    'good': [
        "Usually athletes and people doing high intensity workout has high bone mass, however if its related to an underlying cause, please consult doctor."
    ]
}

TEMPERATURERECOMMENDATIONS = {
    'high': ["You might be having fever, consult doctor if it persists"],
    'normal': [],
    'low': [" Temperature is low. Is it measured properly at correct distance? Measure again"]
}

GLUCOSERECOMMENDATIONS = {
    'low':["You have low blood glucose. Please consult doctor"],
    'normal': ["You have normal blood glucose"],
    'prediabetic': ["You are in pre-diabetic range. Please keep check your glucose levels regularly and consult doctor"],
    'high': ["You are in diabetic range, hope you are consulting doctor"]
}

SYSTOLICRECOMMENDATIONS = {
    'low': ["Eat more salt,avoid alcoholic beverages,cross legs while sitting. Drink water,eat small meals frequently or discuss medications with a doctor"],
    'normal': [],
    'prehyper': ["Exercise helps lower blood pressure,try meditation or deep breathing. Eat calcium-rich foods, cut added sugar and refined carbs, eat foods rich in magnesium."],
    'high': ["Walk and exercise regularly,reduce your sodium intake,drink less alcohol and quit smoking. Eat more potassium-rich foods,cut back on caffeine,eat dark chocolate or cocoa."]
}

DIASTOLICRECOMMENDATIONS = {
    'low': ["Eat a diet higher in salt. Drink lots of non-alcoholic fluids.Limit alcoholic beverages. Get regular exercise to promote blood flow. Consult the doctor."],
    'normal': [],
    'prehyper': ["Exercise helps lower blood pressure. Eat plenty of fruits, vegetables, whole grains, fish, and low-fat dairy"],
    'high': ["Losing weight, Quit smoking, Eating a healthy diet, including more fruits, vegetables, and low fat dairy products.Consult the doctor and take regular medication if required."]
}

PULSERECOMMENDATIONS = {
    'low': ["If low pulse rate is causing the conditions like, breathing difficulty, getting tired easily, or any other underlying cause please consult the doctor."],
    'normal': [],
    'high': ["Ideally pulse measurement is done on a resting body. High Beating Pulse suggest an underlying cause, we suggest to consult the doctor if it is persistent."]
}

OXYGENSATRECOMMENDATIONS = {
    'low': ["This can be because of various underlying causes we suggest to consult a doctor. Focus on iron-rich foods such as meat legumes and green leafy vegetables."],
    'normal': []
}

