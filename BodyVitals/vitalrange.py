__author__ = 'yolo'

from backend_new.Constants import ServerConstants , FileTypeConstants
from  BodyVitals.bodyvitals_utils import *

def getBPRange(bpdata):
    borderhighsys = 140
    borderhighdia = 90
    borderlowsys = 90
    borderlowdia = 60
    total = 0
    high = 0
    normal = 0
    low = 0
    for thisbp in bpdata:
        total+=1
        if thisbp.systolic > borderhighsys or thisbp.diastolic > borderhighdia:
            high +=1
        elif thisbp.systolic < borderlowsys or thisbp.diastolic < borderlowdia:
            low+=1
        else:
            normal+=1

    returnranges = dict({
        ServerConstants.HIGH:high,
        ServerConstants.NORMAL:normal,
        ServerConstants.LOW:low,
        ServerConstants.TOTAL : total
    })

    return returnranges

def getOximeterRange(oximeterdata):
    borderhighpulse = 90
    borderhighoxygensat = 100
    borderlowpulse = 60
    borderlowoxygensat = 90
    total = 0
    good = 0
    normal = 0
    low = 0
    for thisox in oximeterdata:
        total+=1
        if thisox.pulse > borderhighpulse or thisox.oxygensat > borderhighoxygensat:
            good +=1
        elif thisox.pulse < borderlowpulse or thisox.oxygensat < borderlowoxygensat:
            low+=1
        else:
            normal+=1

    returnranges = dict({
        ServerConstants.GOOD:good,
        ServerConstants.NORMAL:normal,
        ServerConstants.LOW:low,
        ServerConstants.TOTAL : total
    })

    return returnranges

def getBloodGlucoseRange(glucosedata):
    borderhighglucose = 140
    borderlowglucose = 60

    total = 0
    high = 0
    normal = 0
    low = 0
    for thisglucose in glucosedata:
        total+=1
        if thisglucose.glucose_level > borderhighglucose :
            high +=1
        elif thisglucose.glucose_level < borderlowglucose :
            low+=1
        else:
            normal+=1

    returnranges = dict({
        ServerConstants.HIGH:high,
        ServerConstants.NORMAL:normal,
        ServerConstants.LOW:low,
        ServerConstants.TOTAL : total
    })

    return returnranges

def getBMIRange(weightdata):
    borderhighbmi = 25
    borderlowbmi = 18
    total = 0
    high = 0
    normal = 0
    low = 0
    for thiswt in weightdata:
        total+=1
        if thiswt.bmi > borderhighbmi :
            high +=1
        elif thiswt.bmi < borderlowbmi :
            low+=1
        else:
            normal+=1

    returnranges = dict({
        ServerConstants.HIGH:high,
        ServerConstants.NORMAL:normal,
        ServerConstants.LOW:low,
        ServerConstants.TOTAL : total
    })

    return returnranges

def getTemperatureRange(tempdata):
    borderhightemp = 100
    borderlowtemp = 90
    total = 0
    high = 0
    normal = 0
    low = 0
    for thistemp in tempdata:
        total+=1
        if thistemp.temperature > borderhightemp :
            high +=1
        elif thistemp.temperature < borderlowtemp :
            low+=1
        else:
            normal+=1

    returnranges = dict({
        ServerConstants.HIGH:high,
        ServerConstants.NORMAL:normal,
        ServerConstants.LOW:low,
        ServerConstants.TOTAL : total
    })

    return returnranges

def getHBRange(hbdata):
    borderhighhbmale = 17.5
    borderlowhbmale = 13.5
    borderhighhbfemale = 15
    borderlowhbfemale = 12
    total = 0
    high = 0
    normal = 0
    low = 0
    for thishb in hbdata:
        if(thishb.person.gender == "m"):
            borderhigh = borderhighhbmale
            borderlow = borderlowhbmale
        else:
            borderhigh = borderhighhbfemale
            borderlow = borderlowhbfemale
        total+=1
        if thishb.hb > borderhigh:
            high +=1
        elif thishb.hb < borderlow:
            low+=1
        else:
            normal+=1

    returnranges = dict({
        ServerConstants.HIGH:high,
        ServerConstants.NORMAL:normal,
        ServerConstants.LOW:low,
        ServerConstants.TOTAL : total
    })

    return returnranges


def getTotalCholestrolRange(lipiddata):
    borderhightc = 200
    borderlowtc = 100
    total = 0
    high = 0
    normal = 0
    low = 0
    for thislipid in lipiddata:
        total+=1
        if thislipid.tc > borderhightc :
            high +=1
        elif thislipid.tc < borderlowtc :
            low+=1
        else:
            normal+=1

    returnranges = dict({
        ServerConstants.HIGH:high,
        ServerConstants.NORMAL:normal,
        ServerConstants.LOW:low,
        ServerConstants.TOTAL : total
    })

    return returnranges

def getHivRange(hivdata):
    borderinvalid = "Invalid"
    bordernegative = "Negative"

    total = 0
    positive = 0
    negative = 0
    invalid = 0
    for thishiv in hivdata:
        total+=1
        if get_hiv_inference(thishiv) == borderinvalid :
            invalid +=1
        elif getHivTestName(thishiv) == bordernegative :
            negative+=1
        else:
            positive+=1

    returnranges = dict({
        ServerConstants.POSITIVE:positive,
        ServerConstants.INVALID:invalid,
        ServerConstants.NEGATIVE:negative,
        ServerConstants.TOTAL : total
    })

    return returnranges

def getDengueRange(denguedata):
    borderinvalid = "Invalid"
    bordernegative = "Negative"

    total = 0
    positive = 0
    negative = 0
    invalid = 0
    for thishiv in denguedata:
        total+=1
        if get_dengue_inference(thishiv) == borderinvalid :
            invalid +=1
        elif get_dengue_inference(thishiv) == bordernegative :
            negative+=1
        else:
            positive+=1

    returnranges = dict({
        ServerConstants.POSITIVE:positive,
        ServerConstants.INVALID:invalid,
        ServerConstants.NEGATIVE:negative,
        ServerConstants.TOTAL : total
    })

    return returnranges

def getMalariaRange(malariadata):
    borderinvalid = "Invalid"
    bordernegative = "Negative"

    total = 0
    positive = 0
    negative = 0
    invalid = 0
    for thishiv in malariadata:
        total+=1
        if get_malaria_inference(thishiv) == borderinvalid :
            invalid +=1
        elif get_malaria_inference(thishiv) == bordernegative :
            negative+=1
        else:
            positive+=1

    returnranges = dict({
        ServerConstants.POSITIVE:positive,
        ServerConstants.INVALID:invalid,
        ServerConstants.NEGATIVE:negative,
        ServerConstants.TOTAL : total
    })

    return returnranges

def getTyphoidRange(typhoiddata):
    borderinvalid = "Invalid"
    bordernegative = "Negative"

    total = 0
    positive = 0
    negative = 0
    invalid = 0
    for thishiv in typhoiddata:
        total+=1
        if get_typhoid_inference(thishiv) == borderinvalid :
            invalid +=1
        elif get_typhoid_inference(thishiv) == bordernegative :
            negative+=1
        else:
            positive+=1

    returnranges = dict({
        ServerConstants.POSITIVE:positive,
        ServerConstants.INVALID:invalid,
        ServerConstants.NEGATIVE:negative,
        ServerConstants.TOTAL : total
    })

    return returnranges

def getDermaDataCount(scannedreportdata):
    total = 0
    for row in scannedreportdata:
        if row.reporttype ==   FileTypeConstants.DERMASCOPEIMG:
            total+=1

    return total

def getOtoDataCount(scannedreportdata):
    total = 0
    for row in scannedreportdata:
        if row.reporttype ==  FileTypeConstants.OTOSCOPESCOPEIMG:
            total+=1

    return total