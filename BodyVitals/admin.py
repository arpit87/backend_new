from django.contrib import admin
from import_export import resources
from import_export.admin import  ImportExportMixin
from BodyVitals.models import bp, weight, height, oximeter, temperature, \
    report, ecg, prescription, bloodglucose, imagereportdetails, spirometer, hemoglobin, lipid, rapidtest, urinetest, \
    eyetest


#Creating import-export resource
class bpResource(resources.ModelResource):
    class Meta:
        model = bp

class oximeterResource(resources.ModelResource):
    class Meta:
        model = oximeter

class weightResource(resources.ModelResource):
    class Meta:
        model = weight

class heightResource(resources.ModelResource):
    class Meta:
        model = height

class temperatureResource(resources.ModelResource):
    class Meta:
        model = temperature


class ecgResource(resources.ModelResource):
    class Meta:
        model = ecg

class bloodglucoseResource(resources.ModelResource):
    class Meta:
        model = bloodglucose

class prescriptionResource(resources.ModelResource):
    class Meta:
        model = prescription

class vitalreportResource(resources.ModelResource):
    class Meta:
        model = report

class imagereportdetailsResource(resources.ModelResource):
    class Meta:
        model = imagereportdetails

class hemoglobinResource(resources.ModelResource):
    class Meta:
        model = hemoglobin

class lipidResource(resources.ModelResource):
    class Meta:
        model = lipid


class spirometerResource(resources.ModelResource):
    class Meta:
        model = spirometer

class rapidtestResource(resources.ModelResource):
    class Meta:
        model = rapidtest

class urinetestResource(resources.ModelResource):
    class Meta:
        model = urinetest

class eyetestResource(resources.ModelResource):
    class Meta:
        model = eyetest

# bodyVitals report data models
class bpModelAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ('id', 'person', 'datetime', 'systolic', 'diastolic', 'pulse', 'appdetails')
    resource_class = bpResource

admin.site.register(bp, bpModelAdmin)

class oximeterModelAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ('id', 'person', 'datetime', 'oxygensat', 'pulse', 'appdetails')
    resource_class = oximeterResource

admin.site.register(oximeter, oximeterModelAdmin)

class weightModelAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ('id', 'person', 'datetime', 'bmi', 'fat', 'bonemass', 'weight', 'height', 'muscle', 'hydration', 'appdetails', 'kcal', 'vfat', 'bage', 'prot', 'muscle_quality_score', 'physique_rating')
    resource_class = weightResource

admin.site.register(weight, weightModelAdmin)

class heightModelAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ('id', 'person', 'datetime', 'height', 'appdetails')
    resource_class = heightResource

admin.site.register(height, heightModelAdmin)

class temperatureModelAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ('id', 'person', 'datetime', 'temperature', 'appdetails')
    resource_class = temperatureResource

admin.site.register(temperature, temperatureModelAdmin)


class ecgModelAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ('id', 'person', 'datetime', 'ecg_img', 'appdetails')


admin.site.register(ecg, ecgModelAdmin)

# prescription report data
class prescriptionModelAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ('prescriptionid', 'person', 'datetime', 'doctor', 'prescription', 'appdetails')
    resource_class = prescriptionResource

admin.site.register(prescription, prescriptionModelAdmin)

class VitalReportModelAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ('reportid', 'person', 'datetime', 'appdetails', 'reporttype')
    resource_class = vitalreportResource

admin.site.register(report, VitalReportModelAdmin)

class bloodglucoseModelAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ('id', 'person', 'datetime', 'glucose_level', 'testtypecode','measuremethod', 'appdetails')
    resource_class = bloodglucoseResource

admin.site.register(bloodglucose, bloodglucoseModelAdmin)

class imagereportdetailsModelAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ('imagereportid', 'person', 'reportimgurl', 'reporttitle', 'datetime')
    resource_class = imagereportdetailsResource

admin.site.register(imagereportdetails, imagereportdetailsModelAdmin)

class hemoglobinModelAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ('id', 'person', 'datetime', 'hb', 'hct', 'appdetails')
    resource_class = hemoglobinResource

admin.site.register(hemoglobin, hemoglobinModelAdmin)

class lipidModelAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ('id', 'person', 'datetime', 'tc', 'tg', 'hdl', 'ldl', 'hdlratio', 'nonhdl', 'appdetails')
    resource_class = lipidResource

admin.site.register(lipid, lipidModelAdmin)

class spirometerModelAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ('id', 'person', 'datetime','fvc', 'fev1','pef','fev1per','fef25','fef75','fef2575', 'appdetails')
    resource_class = spirometerResource

admin.site.register(spirometer, spirometerModelAdmin)

class rapidtestModelAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ('reportid', 'person', 'datetime','reportdata', 'appdetails')
    resource_class = rapidtestResource

admin.site.register(rapidtest, rapidtestModelAdmin)

class urinetestModelAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ('reportid', 'person', 'datetime','reportdata', 'appdetails')
    resource_class = urinetestResource

admin.site.register(urinetest, urinetestModelAdmin)

class eyetestModelAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ('reportid', 'person', 'datetime','reportdata', 'appdetails')
    resource_class = eyetestResource

admin.site.register(eyetest, eyetestModelAdmin)