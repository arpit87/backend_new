
from django.db import models
from Doctor.models import DoctorDetails,AppDetails
from HealthCase.models import HealthCase
from Person.models import PersonDetails
from datetime import datetime

from backend_new.Constants import GLUCOSETESTTYPES


class bp(models.Model):
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    datetime = models.DateTimeField(auto_now_add=True)
    systolic = models.FloatField(default=0.0)
    diastolic = models.FloatField(default=0.0)
    pulse = models.FloatField(default=0.0)
    healthcase = models.ForeignKey(HealthCase, null=True, on_delete=models.CASCADE)
    appdetails = models.ForeignKey(AppDetails, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
       return str(self.id)

    def getSummaryStr(self):
        strsummary = "{0:.0f},{1:.0f} mmHg".format(self.systolic, self.diastolic)
        return strsummary


class bloodglucose(models.Model):
    GLUCOSETESTTYPE_CODES = (
        (GLUCOSETESTTYPES.BEFOREBREAKFAST, ' Before Breakfast '),
        (GLUCOSETESTTYPES.AFTERBREAKFAST, ' After Breakfast '),
        (GLUCOSETESTTYPES.BEFORELUNCH, ' Before Lunch '),
        (GLUCOSETESTTYPES.AFTERLUNCH, ' After Lunch '),
        (GLUCOSETESTTYPES.BEFOREDINNER, ' Before Dinner '),
        (GLUCOSETESTTYPES.AFTERDINNER, ' After Dinner '),
        (GLUCOSETESTTYPES.POSTMIDNIGHT, ' Post Midnight '),
        (GLUCOSETESTTYPES.RANDOM, ' Random '),
    )
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    datetime = models.DateTimeField(auto_now_add=True)
    glucose_level = models.FloatField(default=0.0)
    testtypecode = models.IntegerField(choices=GLUCOSETESTTYPE_CODES, default=GLUCOSETESTTYPES.RANDOM)
    appdetails = models.ForeignKey(AppDetails, null=True, blank=True, on_delete=models.CASCADE)
    measuremethod = models.CharField(max_length=20,default="manual")
    healthcase = models.ForeignKey(HealthCase, null=True, on_delete=models.CASCADE)

    def __str__(self):
       return str(self.id)

    def getSummaryStr(self):
        strsummary = "{0:.0f} mg/dL ({1})".format(self.glucose_level, self.get_testtypecode_display())
        return strsummary


class oximeter(models.Model):
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    datetime = models.DateTimeField(auto_now_add=True)
    oxygensat = models.FloatField(default=0.0)
    pulse = models.FloatField(default=0.0)
    appdetails = models.ForeignKey(AppDetails, null=True, blank=True, on_delete=models.CASCADE)
    healthcase = models.ForeignKey(HealthCase, null=True, on_delete=models.CASCADE)

    def __str__(self):
       return str(self.id)

    def getSummaryStr(self):
        strsummary = ""
        if self.pulse: strsummary = strsummary + "Pulse:{0:.0f} bpm, ".format(self.pulse)
        if self.oxygensat: strsummary = strsummary + "Oxy Sat:{0:.0f}%".format(self.oxygensat)
        return strsummary


class weight(models.Model):
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    datetime = models.DateTimeField(auto_now_add=True)
    weight = models.FloatField(default=0.0)
    height = models.FloatField(default=0.0)
    fat = models.FloatField(default=0.0)
    bmi = models.FloatField(default=0.0)
    bonemass = models.FloatField(default=0.0)
    hydration = models.FloatField(default=0.0)
    muscle = models.FloatField(default=0.0)
    appdetails = models.ForeignKey(AppDetails, null=True, blank=True, on_delete=models.CASCADE)
    kcal = models.FloatField(default=0.0)
    vfat = models.FloatField(default=0.0)
    sfat = models.FloatField(default=0.0)
    bage = models.FloatField(default=0.0)
    prot = models.FloatField(default=0.0)
    muscle_quality_score = models.FloatField(default=0.0)
    physique_rating = models.FloatField(default=0.0)
    healthcase = models.ForeignKey(HealthCase, null=True, on_delete=models.CASCADE)

    def __str__(self):
       return str(self.id)

    def getSummaryStr(self):
        strsummary = ""
        if self.weight: strsummary = strsummary + "Weight:{0:.1f} kg, ".format(self.weight)
        if self.bmi: strsummary = strsummary + "BMI:{0:.1f}, ".format(self.bmi)
        if self.fat: strsummary = strsummary + "Fat:{0:.1f}%, ".format(self.fat)
        if self.muscle: strsummary = strsummary + "Muscle:{0:.1f}%, ".format(self.muscle)
        if self.bonemass: strsummary = strsummary + "Bonemass:{0:.1f}%, ".format(self.bonemass)
        if self.hydration: strsummary = strsummary + "Hydration:{0:.1f}%, ".format(self.hydration)
        if self.kcal: strsummary = strsummary + "BMR:{0:.1f}, ".format(self.kcal)
        if self.vfat: strsummary = strsummary + "VFat:{0:.1f}, ".format(self.vfat)
        if self.sfat: strsummary = strsummary + "SFat:{0:.1f}, ".format(self.sfat)
        if self.bage: strsummary = strsummary + "Body Age:{0:.0f} yrs, ".format(self.bage)
        if self.prot: strsummary = strsummary + "Protein:{0:.1f}, ".format(self.prot)
        if self.muscle_quality_score: strsummary = strsummary + "Muscle Quality:{0:.1f}, ".format(self.muscle_quality_score)
        if self.physique_rating: strsummary = strsummary + "Body type:{0:.1f}, ".format(self.physique_rating)
        return strsummary


class height(models.Model):
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    datetime = models.DateTimeField(auto_now_add=True)
    height = models.FloatField(default=0.0)
    appdetails = models.ForeignKey(AppDetails, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
       return str(self.id)


    def getSummaryStr(self):
        strsummary = ""
        if self.height: strsummary = strsummary + "Height:{:.0f} cm".format(self.height)
        return strsummary


class temperature(models.Model):
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    datetime = models.DateTimeField(auto_now_add=True)
    temperature = models.FloatField(default=0.0)
    appdetails = models.ForeignKey(AppDetails, null=True, blank=True, on_delete=models.CASCADE)
    healthcase = models.ForeignKey(HealthCase, null=True, on_delete=models.CASCADE)

    def __str__(self):
       return str(self.id)

    def getSummaryStr(self):
        strsummary = ""
        if self.temperature: strsummary = strsummary + "Temp:{0:.1f} F".format(self.temperature)
        return strsummary


class ecgdata(models.Model):
    leaddata = models.TextField(default="")

    def __str__(self):
       return self.leaddata


class ecg(models.Model):
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    datetime = models.DateTimeField(auto_now_add=True)
    lead1 = models.ForeignKey(ecgdata,related_name="lead1", on_delete=models.CASCADE)
    lead2 = models.ForeignKey(ecgdata,related_name="lead2", on_delete=models.CASCADE)
    lead3 = models.ForeignKey(ecgdata,related_name="lead3", on_delete=models.CASCADE)
    avr = models.ForeignKey(ecgdata,related_name="avr", on_delete=models.CASCADE)
    avl = models.ForeignKey(ecgdata,related_name="avl", on_delete=models.CASCADE)
    avf = models.ForeignKey(ecgdata,related_name="avf", on_delete=models.CASCADE)
    v1 = models.ForeignKey(ecgdata,related_name="v1", on_delete=models.CASCADE)
    v2 = models.ForeignKey(ecgdata,related_name="v2", on_delete=models.CASCADE)
    v3 = models.ForeignKey(ecgdata,related_name="v3", on_delete=models.CASCADE)
    v4 = models.ForeignKey(ecgdata,related_name="v4", on_delete=models.CASCADE)
    v5 = models.ForeignKey(ecgdata,related_name="v5", on_delete=models.CASCADE)
    v6 = models.ForeignKey(ecgdata,related_name="v6", on_delete=models.CASCADE)
    ecg_img = models.CharField(max_length=512,null=True,blank=True,default="")
    appdetails = models.ForeignKey(AppDetails, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
       return str(self.id)

class hemoglobin(models.Model):
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    datetime = models.DateTimeField(auto_now_add=True)
    hb = models.FloatField(default=0.0)
    hct = models.FloatField(default=0.0)
    appdetails = models.ForeignKey(AppDetails, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
       return str(self.id)

    def getSummaryStr(self):
        strsummary = ""
        if self.hb: strsummary = strsummary + "HB:{0:.1f} g/dl".format(self.hb)
        if self.hct: strsummary = strsummary + ", HCT:{0:.0f}%".format(self.hct)
        return strsummary

class lipid(models.Model):
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    datetime = models.DateTimeField(auto_now_add=True)
    tc = models.FloatField(default=0.0)
    tg = models.FloatField(default=0.0)
    hdl = models.FloatField(default=0.0)
    ldl = models.FloatField(default=0.0)
    hdlratio = models.FloatField(default=0.0)
    nonhdl = models.FloatField(default=0.0)
    appdetails = models.ForeignKey(AppDetails, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
       return str(self.id)

    def getSummaryStr(self):
        strsummary = ""
        if self.tc: strsummary = strsummary + "TC:{0:.0f} mg/dl, ".format(self.tc)
        if self.hdl: strsummary = strsummary + "HDL:{0:.0f} mg/dl, ".format(self.hdl)
        if self.ldl: strsummary = strsummary + "LDL:{0:.0f} mg/dl, ".format(self.ldl)
        if self.tg: strsummary = strsummary + "TG:{0:.0f} mg/dl, ".format(self.tg)
        if self.hdlratio: strsummary = strsummary + "HDLRatio:{0:.1f}, ".format(self.hdlratio)
        if self.nonhdl: strsummary = strsummary + "NonHDL:{0:.0f} mg/dl".format(self.nonhdl)
        return strsummary

class spirometer(models.Model):
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    datetime = models.DateTimeField(auto_now_add=True)
    fvc = models.FloatField(default=0.0)
    fev1 = models.FloatField(default=0.0)
    pef = models.FloatField(default=0.0)
    fev1per = models.FloatField(default=0.0)
    fef25 = models.FloatField(default=0.0)
    fef75 = models.FloatField(default=0.0)
    fef2575 = models.FloatField(default=0.0)
    appdetails = models.ForeignKey(AppDetails, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
       return str(self.id)

    def getSummaryStr(self):
        strsummary = ""
        if self.fvc: strsummary = strsummary + "FVC:{0:.0f} L, ".format(self.fvc)
        if self.fev1: strsummary = strsummary + "FEV1:{0:.0f} L, ".format(self.fev1)
        if self.pef: strsummary = strsummary + "PEF:{0:.0f} L/min, ".format(self.pef)
        if self.fev1per: strsummary = strsummary + "FEV1per:{0:.0f}, ".format(self.fev1per)
        if self.fef25: strsummary = strsummary + "FEF25:{0:.0f} L, ".format(self.fef25)
        if self.fef75: strsummary = strsummary + "FEF75:{0:.0f} L, ".format(self.fef75)
        if self.fef2575: strsummary = strsummary + "FEF2575:{0:.0f}".format(self.fef2575)
        return strsummary

class report(models.Model):
    reportid = models.CharField(db_index=True, max_length=20, null=True, unique=True)
    reporttype = models.IntegerField(null=False,default=0) #0 basic | 1 advance | 2 diabetec | 3 heart | 7 blood | 8 presReport | 9 urine | 10 eye | 11 self check | 12 fitness | 13 healthiness | 14 diabetes_Wellness | 15 complete_Wellness | 16 heartiness |
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    datetime = models.DateTimeField(auto_now_add=True)
    bp = models.ForeignKey(bp,null=True,blank=True, on_delete=models.CASCADE)
    weight=models.ForeignKey(weight,null=True,blank=True, on_delete=models.CASCADE)
    height=models.ForeignKey(height,null=True,blank=True, on_delete=models.CASCADE)
    temperature=models.ForeignKey(temperature,null=True,blank=True, on_delete=models.CASCADE)
    ecg=models.ForeignKey(ecg,null=True,blank=True, on_delete=models.CASCADE)
    oximeter = models.ForeignKey(oximeter,null=True,blank=True, on_delete=models.CASCADE)
    bloodglucose = models.ForeignKey(bloodglucose,null=True,blank=True, on_delete=models.CASCADE)
    spirometer = models.ForeignKey(spirometer,null=True,blank=True, on_delete=models.CASCADE)
    hemoglobin = models.ForeignKey(hemoglobin,null=True,blank=True, on_delete=models.CASCADE)
    lipid = models.ForeignKey(lipid,null=True,blank=True, on_delete=models.CASCADE)
    appdetails = models.ForeignKey(AppDetails, null=True, blank=True, on_delete=models.CASCADE)
    recommendations = models.TextField(null=True, default=None)

    def __str__(self):
       return str(self.reportid)

    def getDescription(self):
        descriptionlist = list()
        if self.height: descriptionlist.append("Height")
        if self.weight: descriptionlist.append("Weight")
        if self.bp: descriptionlist.append("BP")
        if self.oximeter: descriptionlist.append("SpO2")
        if self.temperature: descriptionlist.append("Temperature")
        if self.bloodglucose: descriptionlist.append("Glucose")
        if self.ecg: descriptionlist.append("ECG")
        if self.spirometer: descriptionlist.append("Spiro")
        if self.hemoglobin: descriptionlist.append("HB")
        if self.lipid: descriptionlist.append("Lipid")
        return str(descriptionlist)

    def getVitalsSummaryDataDict(self):
        vitalsDict = dict()
        if self.height: vitalsDict.update({"Height":"{0:.0f} cm".format(self.height.height)})
        if self.weight: vitalsDict.update({"Body Scale": self.weight.getSummaryStr()})
        if self.bp: vitalsDict.update({"BP":"{0:.0f},{1:.0f} mmHg".format(self.bp.systolic, self.bp.diastolic)})
        if self.oximeter: vitalsDict.update({"Pulse, Oxygensat":"{0:.0f} bpm, {1:.0f}".format(self.oximeter.pulse, self.oximeter.oxygensat)})
        if self.temperature: vitalsDict.update({"Temperature":"{0:.1f} F".format(self.temperature.temperature)})
        if self.bloodglucose: vitalsDict.update({"Glucose":"{0:.0f} mg/dL ({1})".format(self.bloodglucose.glucose_level, self.bloodglucose.get_testtypecode_display())})
        if self.spirometer: vitalsDict.update({"Spiro": self.spirometer.getSummaryStr()})
        if self.hemoglobin: vitalsDict.update({"Hemoglobin": self.hemoglobin.getSummaryStr()})
        if self.lipid: vitalsDict.update({"Lipid Profile": self.lipid.getSummaryStr()})
        return vitalsDict


class prescription(models.Model):
    prescriptionid = models.CharField(db_index=True, max_length=20,null=True,unique=True)
    prescription = models.CharField(max_length=6144, null=True,default="")   #json array of medicines
    investigation = models.CharField(max_length=2048, null=True,default="")
    clinicalnote = models.CharField(max_length=2048, null=True,default="")
    diagnosis = models.CharField(max_length=2048, null=True,default="")
    recommendation = models.CharField(max_length=1024, null=True,default="")
    datetime = models.DateTimeField(auto_now_add=True)
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    doctor = models.ForeignKey(DoctorDetails, on_delete=models.CASCADE)
    appdetails = models.ForeignKey(AppDetails, null=True, blank=True, on_delete=models.CASCADE)
    followupdatetime = models.DateTimeField(null=True)

    def __str__(self):
       return str(self.prescriptionid)


class imagereportdetails(models.Model):
    imagereportid = models.CharField(db_index=True, max_length=20, null=True, unique=True)
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    reportimgurl = models.CharField(max_length=512,null=False,blank=False)
    reporttitle = models.CharField(max_length=50,null=False,blank=False)
    reporttype = models.CharField(max_length=50,null=False,blank=False)
    reportdescription = models.CharField(max_length=1512,default="",null=True,blank=True)   #json array of medicines
    datetime = models.DateTimeField()
    appdetails = models.ForeignKey(AppDetails, null=True, blank=True, default=100, on_delete=models.CASCADE)

    def __str__(self):
       return str(self.imagereportid)


    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.datetime = datetime.now()
        return super(imagereportdetails, self).save(*args, **kwargs)



class videoreportdetails(models.Model):
    videoreportid = models.CharField(max_length=20,null=False,unique=True)
    videoreportkey = models.CharField(max_length=255,null=False,unique=True)
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    reporturl = models.CharField(max_length=512,null=False,blank=False)
    reporttitle = models.CharField(max_length=50,null=False,blank=False)
    reporttype = models.CharField(max_length=50,null=False,blank=False)
    reportdescription = models.CharField(max_length=1512,default="",null=True,blank=True)   #json array of medicines
    datetime = models.DateTimeField()
    appdetails = models.ForeignKey(AppDetails, null=True, blank=True, default=100, on_delete=models.CASCADE)

    def __str__(self):
       return str(self.videoreportid)


    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.datetime = datetime.now()
        return super(videoreportdetails, self).save(*args, **kwargs)


class urine(models.Model):
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    datetime = models.DateTimeField(auto_now_add=True)
    glucose = models.CharField(max_length=150,default="")
    bilirubin = models.CharField(max_length=150,default="")
    ketone = models.CharField(max_length=150,default="")
    gravity = models.CharField(max_length=150,default="")
    blood = models.CharField(max_length=150,default="")
    ph = models.CharField(max_length=150,default="")
    protein = models.CharField(max_length=150,default="")
    urobilinogen = models.CharField(max_length=150,default="")
    nitrite = models.CharField(max_length=150,default="")
    leukocytes = models.CharField(max_length=150,default="")
    appdetails = models.ForeignKey(AppDetails, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
       return str(self.id)



class rapidtest(models.Model):
    reportid = models.CharField(db_index=True, max_length=10,null=True)
    reportdata = models.CharField(max_length=1024, null=True,default="")
    datetime = models.DateTimeField(auto_now_add=True)
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    healthcase = models.ForeignKey(HealthCase, null=False, on_delete=models.CASCADE)
    appdetails = models.ForeignKey(AppDetails, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
       return str(self.reportid)

class urinetest(models.Model):
    reportid = models.CharField(db_index=True, max_length=10,null=True)
    reportdata = models.CharField(max_length=1024, null=True,default="")
    datetime = models.DateTimeField(auto_now_add=True)
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    healthcase = models.ForeignKey(HealthCase, null=False, on_delete=models.CASCADE)
    appdetails = models.ForeignKey(AppDetails, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
       return str(self.reportid)

class eyetest(models.Model):
    reportid = models.CharField(db_index=True, max_length=10,null=True)
    reportdata = models.CharField(max_length=1024, null=True,default="")
    datetime = models.DateTimeField(auto_now_add=True)
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    healthcase = models.ForeignKey(HealthCase, null=False, on_delete=models.CASCADE)
    appdetails = models.ForeignKey(AppDetails, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
       return str(self.reportid)
