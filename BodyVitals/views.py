import json, os, pdfkit
import logging
from operator import itemgetter

import pylibmc
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.db.models import Count

from HealthCase.models import HealthCase
from HealthPackage.models import HealthPackageEnrollment
from Platform.uid_code_converter import encode_id
from UploadFiles.views import save_uploaded_file_to_gcloud
from django.core.mail import EmailMessage
from django.core.exceptions import ObjectDoesNotExist

from django.conf import settings
from django.db import IntegrityError
from django.forms.models import model_to_dict
from django.http import HttpResponse
from django.db.models.aggregates import Avg

from django.views.decorators.csrf import csrf_exempt
from django.template import Context,loader
import Communications.utils
from django.utils import timezone
import uuid
from BodyVitals.models import (bloodglucose, bp, ecg, hemoglobin, lipid,
                               ecgdata, height, oximeter, rapidtest,
                               prescription, report, spirometer,
                               imagereportdetails, temperature,
                               weight, urine,
                               urinetest, eyetest)
from Doctor.models import DoctorDetails, AppDetails
from Platform import  utils, backendlogger
from Platform.views import authenticateURL
from Person.models import PersonDetails
from .vitalrange import *
from backend_new import config
from backend_new.Constants import (FileTypeConstants, FileTypeDIRStorageMap,
                                   ServerConstants, ReportTypeConstants, VFATRANGE, GLUCOSETESTTYPES, VitalTypeConstant)
from BodyVitals.bodyvitals_utils import (get_bmi_inference, get_bp_inference, get_pulse_inference, get_hb_inference,
                                         get_hydration_inference, get_fat_inference, get_tc_inference, get_hdl_inference,
                                         get_ldl_inference, get_tg_inference,
                                         get_bonemass_inference, get_muscle_inference, get_localize_datetime,
                                         apply_dynamic_ranges, get_vfat_inference, get_bmr_inference,
                                         get_metabolic_age_inference,
                                         get_muscle_quality_inference, get_oxygensat_inference, get_temperature_inference,
                                         get_bg_inference, get_hiv_inference, getPregnancyTestName, get_dengue_inference,
                                         get_typhoid_inference, get_malaria_inference)

from datetime import datetime, timedelta
requestlogger = logging.getLogger('apirequests')

@csrf_exempt
def setReportForPerson(request):
    #requestlogger.info('================setReportForPerson=====================')
    data = json.loads(request.body)
    logmessage = []
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error("setReportForPerson", data, "Error authenticating person", logmessage)
        return HttpResponse("Error authenticating person", status=401)

    # requestlogger.info('setReportForPerson;%s'%(str(data)))
    person=None

    try:
        personuid_req = data[ServerConstants.PERSONUID]
    except KeyError:
        backendlogger.error("setReportForPerson", data, "Person ID not found", logmessage)
        return HttpResponse("personuid not found",status=403)

    try:
        person = PersonDetails.objects.get(personuid=personuid_req)
    except ObjectDoesNotExist:
        backendlogger.error("setReportForPerson", data, "Person doesnt exist", logmessage)
        return HttpResponse("person doesnt exist",status=403)

    try:
        appuid_req = data[ServerConstants.APPUID]
    except KeyError:
        backendlogger.error("setReportForPerson", data, "WebApp ID not found", logmessage)
        return HttpResponse("appid not sent",status=403)

    try:
        appdetails = AppDetails.objects.get(appuid=appuid_req)
    except ObjectDoesNotExist:
        backendlogger.error("setReportForPerson", data, "WebApp doesnt exist", logmessage)
        return HttpResponse("kiosk doesnt exist",status=403)


    try:
        reportdata_req = data[ServerConstants.REPORTDATA]
    except KeyError:
        backendlogger.error("setReportForPerson", data, "Report data not found", logmessage)
        return HttpResponse("reportdata not found",status=403)

    logmessage.append('reportdata:'+json.dumps(reportdata_req))
    # requestlogger.info('reportdata:'+json.dumps(reportdata_req))

    reporttype_req = data[ServerConstants.REPORTTYPE]

    recommendation_dict = dict()

    try:
        bp_req = reportdata_req[ServerConstants.BPMACHINE]
    except KeyError:
        bp_req=None

    try:
        weight_req = reportdata_req[ServerConstants.WEIGHTMACHINE]
    except KeyError:
        weight_req=None

    try:
        height_req = reportdata_req[ServerConstants.HEIGHTMACHINE]
    except KeyError:
        height_req=None

    try:
        temperature_req = reportdata_req[ServerConstants.TEMPERATUREMACHINE]
    except KeyError:
        temperature_req=None

    try:
        oximeter_req = reportdata_req[ServerConstants.OXIMETERMACHINE]
    except KeyError:
        oximeter_req=None

    try:
        spirometer_req = reportdata_req[ServerConstants.SPIROMETERMACHINE]
    except KeyError:
        spirometer_req=None

    try:
        bloodglucose_req = reportdata_req[ServerConstants.BLOODGLUCOSEMACHINE]
    except KeyError:
        bloodglucose_req=None

    try:
        ecg_req = reportdata_req[ServerConstants.ECGMACHINE]
    except KeyError:
        ecg_req=None

    try:
        hemoglobin_req = reportdata_req[ServerConstants.HEMOGLOBINMACHINE]
    except KeyError:
        hemoglobin_req=None


    try:
        lipid_req = reportdata_req[ServerConstants.LIPIDMACHINE]
    except KeyError:
        lipid_req=None

    try:
        urine_req = reportdata_req[ServerConstants.URINEMACHINE]
    except KeyError:
        urine_req=None


    reportdata = None
    try:
        reportid = data[ServerConstants.REPORTID]
        try:
            reportdata = report.objects.get(reportid=reportid)
        except ObjectDoesNotExist:
            backendlogger.error("setReportForPerson", data, "Report ID doesnt exist", logmessage)
            return HttpResponse("Reportid does not exist",status=403)
    except KeyError:
        reportid = None
        backendlogger.error("setReportForPerson", data, "New report ", logmessage)

    bpobj = None
    if(bp_req):
        if reportdata and reportdata.bp:
            logmessage.append('updating bp data:' + json.dumps(bp_req))
            # requestlogger.info('updating bp data:'+json.dumps(bp_req))
            bpobj = reportdata.bp
            bpobj.systolic = bp_req[ServerConstants.SYSTOLIC]
            bpobj.diastolic = bp_req[ServerConstants.DIASTOLIC]
            bpobj.pulse = bp_req[ServerConstants.PULSE]
        else:
            logmessage.append('saving bp data:' + json.dumps(bp_req))
            # requestlogger.info('saving bp data:'+json.dumps(bp_req))
            bpobj = bp(person=person,
                        systolic = bp_req[ServerConstants.SYSTOLIC],
                        diastolic = bp_req[ServerConstants.DIASTOLIC],
                        pulse = bp_req[ServerConstants.PULSE],
                        appdetails = appdetails)

        bpobj.save()
        bpreco=get_bp_recommendations(model_to_dict(bpobj, fields=('systolic', 'diastolic', 'pulse')), person.get_age())
        if len(bpreco)>0:
            recommendation_dict["Blood Pressure"] = bpreco
        #may be updating..ie. reportid existed but if previous data didnt have this device at all then also
        #still we reach here and not only for first time save
        if reportdata and not reportdata.bp:
            reportdata.bp = bpobj

    oximeterobj = None
    if(oximeter_req):
        if reportdata and reportdata.oximeter:
            logmessage.append('updating oximeter data:' + json.dumps(oximeter_req))
            # requestlogger.info('updating oximeter data:'+json.dumps(oximeter_req))
            oximeterobj = reportdata.oximeter
            oximeterobj.pulse = oximeter_req[ServerConstants.PULSE]
            oximeterobj.oxygensat = oximeter_req[ServerConstants.OXYGENSAT]
        else:
            logmessage.append('saving oximeter data:' + json.dumps(oximeter_req))
            # requestlogger.info('saving oximeter data:'+json.dumps(oximeter_req))
            oximeterobj = oximeter(person=person,
                                    oxygensat = oximeter_req[ServerConstants.OXYGENSAT],
                                    pulse = oximeter_req[ServerConstants.PULSE],
                                    appdetails = appdetails)

        oximeterobj.save()
        oxireco = get_pulse_recommendations(oximeterobj.pulse, person.get_age()) + get_oxygensat_recommendations(oximeterobj.oxygensat)
        if len(oxireco)>0:
            recommendation_dict["Pulse and oxygen saturation"] = oxireco
        #may be updating..ie. reportid existed but if previous data didnt have this device at all then also
        #still we reach here and not only for first time save
        if reportdata and not reportdata.oximeter:
            reportdata.oximeter = oximeterobj

    spirometerobj = None
    if(spirometer_req):
        if reportdata and reportdata.spirometer:
            logmessage.append('updating spirometer data:' + json.dumps(spirometer_req))
            # requestlogger.info('updating spirometer data:'+json.dumps(spirometer_req))
            spirometerobj = reportdata.spirometer
            spirometerobj.pef = spirometer_req[ServerConstants.PEF]
            spirometerobj.fvc = spirometer_req[ServerConstants.FVC]
            spirometerobj.fev1 = spirometer_req[ServerConstants.FEV1]
            spirometerobj.fev1per = spirometer_req[ServerConstants.FEV1PER]
            spirometerobj.fef25 = spirometer_req[ServerConstants.FEF25]
            spirometerobj.fef75 = spirometer_req[ServerConstants.FEF75]
            spirometerobj.fef2575 = spirometer_req[ServerConstants.FEF2575]
        else:
            logmessage.append('saving spirometer data:' + json.dumps(spirometer_req))
            # requestlogger.info('saving spirometer data:'+json.dumps(spirometer_req))
            spirometerobj = spirometer(person=person,
                                       pef=spirometer_req[ServerConstants.PEF],
                                       fvc=spirometer_req[ServerConstants.FVC],
                                       fev1=spirometer_req[ServerConstants.FEV1],
                                       fev1per=spirometer_req[ServerConstants.FEV1PER],
                                       fef25=spirometer_req[ServerConstants.FEF25],
                                       fef75=spirometer_req[ServerConstants.FEF75],
                                       fef2575=spirometer_req[ServerConstants.FEF2575],
                                    appdetails = appdetails)

        spirometerobj.save()
        #may be updating..ie. reportid existed but if previous data didnt have this device at all then also
        #still we reach here and not only for first time save
        if reportdata and not reportdata.spirometer:
            reportdata.spirometer = spirometerobj

    heightobj = None
    if(height_req):
        if reportdata and reportdata.height:
            logmessage.append('updating height data:' + json.dumps(height_req))
            # requestlogger.info('updating height data:'+json.dumps(height_req))
            heightobj = reportdata.height
            heightobj.height = height_req[ServerConstants.HEIGHT]
        else:
            logmessage.append('saving height data:' + json.dumps(height_req))
            # requestlogger.info('saving height data:'+json.dumps(height_req))
            heightobj = height(person=person,
                                height = height_req[ServerConstants.HEIGHT],
                                appdetails = appdetails)

        heightobj.save()
        #may be updating..ie. reportid existed but if previous data didnt have this device at all then also
        #still we reach here and not only for first time save
        if reportdata and not reportdata.height:
            reportdata.height = heightobj
        # requestlogger.info('updating height in PersonDetails:'+str(height_req[ServerConstants.HEIGHT]))
        person.userheight = height_req[ServerConstants.HEIGHT]
        person.save()

    weightobj = None
    if(weight_req):
        if reportdata and reportdata.weight:
            logmessage.append('updating weight data:' + json.dumps(weight_req))
            # requestlogger.info('updating weight data:'+json.dumps(weight_req))
            weightobj = reportdata.weight
            weightobj.bmi = weight_req.get(ServerConstants.BMI, 0)
            weightobj.fat = weight_req.get(ServerConstants.FAT, 0)
            weightobj.bonemass = weight_req.get(ServerConstants.BONEMASS, 0)
            weightobj.weight = weight_req.get(ServerConstants.WEIGHT, 0)
            weightobj.muscle = weight_req.get(ServerConstants.MUSCLE, 0)
            weightobj.hydration = weight_req.get(ServerConstants.HYDRATION, 0)
            weightobj.physique_rating = weight_req.get(ServerConstants.PHYSIQUERATING, 0)
            weightobj.muscle_quality_score = weight_req.get(ServerConstants.MUSCLEQUALITYSCORE, 0)
            weightobj.kcal = weight_req.get(ServerConstants.KCAL, 0)
            weightobj.vfat = weight_req.get(ServerConstants.VFAT, 0)
            weightobj.sfat = weight_req.get(ServerConstants.SFAT, 0)
            weightobj.bage = weight_req.get(ServerConstants.BAGE, 0)
            weightobj.prot = weight_req.get(ServerConstants.PROT, 0)
        else:
            logmessage.append('saving weight data:' + json.dumps(weight_req))
            # requestlogger.info('saving weight data:'+json.dumps(weight_req))
            weightobj = weight( person=person,
                                bmi = weight_req.get(ServerConstants.BMI, 0),
                                fat = weight_req.get(ServerConstants.FAT, 0),
                                bonemass = weight_req.get(ServerConstants.BONEMASS, 0),
                                weight = weight_req.get(ServerConstants.WEIGHT, 0),
                                muscle = weight_req.get(ServerConstants.MUSCLE, 0),
                                hydration = weight_req.get(ServerConstants.HYDRATION, 0),
                                muscle_quality_score = weight_req.get(ServerConstants.MUSCLEQUALITYSCORE, 0),
                                physique_rating = weight_req.get(ServerConstants.PHYSIQUERATING, 0),
                                appdetails = appdetails,
                                kcal = weight_req.get(ServerConstants.KCAL, 0),
                                vfat = weight_req.get(ServerConstants.VFAT, 0),
                                sfat = weight_req.get(ServerConstants.SFAT, 0),
                                bage = weight_req.get(ServerConstants.BAGE, 0),
                                prot = weight_req.get(ServerConstants.PROT, 0))


        if height_req:
           weightobj.height = height_req[ServerConstants.HEIGHT]

        weightobj.save()
        bmireco = get_bmi_recommendations(weightobj.height, weightobj.bmi,person.get_age())
        if len(bmireco)>0:
            recommendation_dict["BMI"] = bmireco
        fatreco = get_fat_recommendations(weightobj.fat, person.gender,person.get_age())
        if len(fatreco)>0:
            recommendation_dict["FAT"] = fatreco
        bonemassreco = get_bonemass_recommendations(weightobj.bonemass, person.gender)
        if len(bonemassreco)>0:
            recommendation_dict["Bone Mass"] = bonemassreco
        hydreco = get_hydration_recommendations(weightobj.hydration, person.gender)
        if len(hydreco)>0:
            recommendation_dict["Body Water"] = hydreco
        musclereco = get_muscle_recommendations(weightobj.muscle,person.gender, person.get_age())
        if len(musclereco)>0:
            recommendation_dict["Muscle"] = musclereco
        vfatreco = get_vfat_recommendations(weightobj.vfat)
        if len(vfatreco)>0:
            recommendation_dict["Visceral FAT"] = vfatreco
        bagereco = get_metabolic_age_recommendations(weightobj.bage,person.get_age())
        if len(bagereco)>0:
            recommendation_dict["Body Age"] = bagereco
        bmrreco = get_bmr_recommendations(weightobj.kcal,weightobj.weight, weightobj.height,person.get_age(),person.gender)
        if len(bmrreco)>0:
            recommendation_dict["Basal Metabolic Rating (BMR)"] = bmrreco

        #may be updating..ie. reportid existed but if previous data didnt have this device at all then also
        #still we reach here and not only for first time save
        if reportdata and not reportdata.weight:
            reportdata.weight = weightobj

        person.userweight = weight_req[ServerConstants.WEIGHT]
        person.save()

    temperatureobj = None
    if(temperature_req):
        if reportdata and reportdata.temperature:
            logmessage.append('updating temperature data:' + json.dumps(temperature_req))
            # requestlogger.info('updating temperature data:'+json.dumps(temperature_req))
            temperatureobj = reportdata.temperature
            temperatureobj.temperature = temperature_req[ServerConstants.TEMPERATURE]
        else:
            logmessage.append('saving temperature data:' + json.dumps(temperature_req))
            # requestlogger.info('saving temperature data:'+json.dumps(temperature_req))
            temperatureobj = temperature(person=person,
                                            temperature = temperature_req[ServerConstants.TEMPERATURE],
                                            appdetails = appdetails)

        temperatureobj.save()
        tempreco = get_temperature_recommendations(temperatureobj.temperature)
        if len(tempreco) > 0:
            recommendation_dict["Body Temperature"] = tempreco
        #may be updating..ie. reportid existed but if previous data didnt have this device at all then also
        #still we reach here and not only for first time save
        if reportdata and not reportdata.temperature:
            reportdata.temperature = temperatureobj


    bloodglucoseobj = None
    if(bloodglucose_req):
        if reportdata and reportdata.bloodglucose:
            logmessage.append('updating bloodglucose data:'+json.dumps(bloodglucose_req))
            # requestlogger.info('updating bloodglucose data:'+json.dumps(bloodglucose_req))
            bloodglucoseobj = reportdata.bloodglucose
            bloodglucoseobj.glucose_level = bloodglucose_req[ServerConstants.BLOODGLUCOSE]
            try:
                bloodglucoseobj.testtypecode = bloodglucose_req[ServerConstants.TESTTYPECODE]
            except KeyError:
                bloodglucoseobj.testtypecode = GLUCOSETESTTYPES.RANDOM
        else:
            logmessage.append('saving bloodglucose data:'+json.dumps(bloodglucose_req))
            try:
                testtypecode = bloodglucose_req[ServerConstants.TESTTYPECODE]
            except KeyError:
                testtypecode = GLUCOSETESTTYPES.RANDOM
            bloodglucoseobj = bloodglucose(person=person,
                                            glucose_level = bloodglucose_req[ServerConstants.BLOODGLUCOSE],
                                            testtypecode = testtypecode,
                                            appdetails = appdetails)

        bloodglucoseobj.save()
        bgreco = get_bg_recommendations(bloodglucoseobj.glucose_level, bloodglucoseobj.testtypecode)
        if len(bgreco) > 0:
            recommendation_dict["Blood Glucose"]  = bgreco

        #may be updating..ie. reportid existed but if previous data didnt have this device at all then also
        #still we reach here and not only for first time save
        if reportdata and not reportdata.bloodglucose:
            reportdata.bloodglucose = bloodglucoseobj

    hemoglobinobj = None
    if(hemoglobin_req):
        if reportdata and reportdata.hemoglobin:
            logmessage.append('updating hemoglobin data:'+json.dumps(hemoglobin_req))
            # requestlogger.info('updating hemoglobin data:'+json.dumps(hemoglobin_req))
            hemoglobinobj = reportdata.hemoglobin
            hemoglobinobj.hb = hemoglobin_req[ServerConstants.HB]
        else:
            logmessage.append('saving hemoglobin data:'+json.dumps(hemoglobin_req))
            # requestlogger.info('saving hemoglobin data:'+json.dumps(hemoglobin_req))
            hemoglobinobj = hemoglobin(person=person,
                                            hb = hemoglobin_req[ServerConstants.HB],
                                            appdetails = appdetails)

        hemoglobinobj.save()
        #may be updating..ie. reportid existed but if previous data didnt have this device at all then also
        #still we reach here and not only for first time save
        if reportdata and not reportdata.hemoglobin:
            reportdata.hemoglobin = hemoglobinobj


    lipidobj = None
    if(lipid_req):
        if reportdata and reportdata.lipid:
            logmessage.append('updating lipid data:'+json.dumps(lipid_req))
            # requestlogger.info('updating lipid data:'+json.dumps(lipid_req))
            lipidobj = reportdata.lipid
            lipidobj.tc = lipid_req[ServerConstants.TC]
            lipidobj.tg = lipid_req[ServerConstants.TG]
            lipidobj.hdl = lipid_req[ServerConstants.HDL]
            lipidobj.ldl = lipid_req[ServerConstants.LDL]
            lipidobj.hdlratio = lipid_req[ServerConstants.HDLRATIO]
            lipidobj.nonhdl = lipid_req[ServerConstants.NONHDL]
        else:
            logmessage.append('saving lipid data:'+json.dumps(lipid_req))
            # requestlogger.info('saving lipid data:'+json.dumps(lipid_req))
            lipidobj = lipid(person=person,
                                tc = lipid_req[ServerConstants.TC],
                                tg = lipid_req[ServerConstants.TG],
                                hdl = lipid_req[ServerConstants.HDL],
                                ldl = lipid_req[ServerConstants.LDL],
                                hdlratio = lipid_req[ServerConstants.HDLRATIO],
                                nonhdl = lipid_req[ServerConstants.NONHDL],
                                appdetails = appdetails)
        lipidobj.save()
        #may be updating..ie. reportid existed but if previous data didnt have this device at all then also
        #still we reach here and not only for first time save
        if reportdata and not reportdata.lipid:
            reportdata.lipid = lipidobj

    urineobj = None
    if(urine_req):

        glucose = urine_req[ServerConstants.GLUCOSE]
        bilirubin = urine_req[ServerConstants.BILIRUBIN]
        ketone = urine_req[ServerConstants.KETONE]
        gravity = urine_req[ServerConstants.GRAVITY]
        blood = urine_req[ServerConstants.BLOOD]
        ph = urine_req[ServerConstants.PH]
        protein = urine_req[ServerConstants.PROTEIN]
        urobilinogen = urine_req[ServerConstants.UROBILINOGEN]
        nitrite = urine_req[ServerConstants.NITRITE]
        leukocytes = urine_req[ServerConstants.LEUKOCYTES]

        if reportdata and reportdata.urine:
            logmessage.append('updating urine data:'+json.dumps(urine_req))
            # requestlogger.info('updating urine data:'+json.dumps(urine_req))
            urineobj = reportdata.urine
            if glucose != "":
                urineobj.glucose = glucose
            if bilirubin != "":
                urineobj.bilirubin = bilirubin
            if ketone != "":
                urineobj.ketone = ketone
            if gravity != "":
                urineobj.gravity = gravity
            if blood != "":
                urineobj.blood = blood
            if ph != "":
                urineobj.ph = ph
            if protein != "":
                urineobj.protein = protein
            if urobilinogen != "":
                urineobj.urobilinogen = urobilinogen
            if nitrite != "":
                urineobj.nitrite = nitrite
            if leukocytes != "":
                urineobj.leukocytes = leukocytes
        else:
            logmessage.append('saving urine data:'+json.dumps(urine_req))
            # requestlogger.info('saving urine data:'+json.dumps(urine_req)
            urineobj = urine(person=person,
                                glucose = glucose,
                                bilirubin = bilirubin,
                                ketone = ketone,
                                gravity = gravity,
                                blood = blood,
                                ph = ph,
                                protein = protein,
                                urobilinogen = urobilinogen,
                                nitrite = nitrite,
                                leukocytes = leukocytes,
                                appdetails = appdetails)
        urineobj.save()
        #may be updating..ie. reportid existed but if previous data didnt have this device at all then also
        #still we reach here and not only for first time save
        if reportdata and not reportdata.urine:
            reportdata.urine = urineobj

    ecgobj = None
    if(ecg_req):
        if reportdata and reportdata.ecg:
            logmessage.append('updating ecg ')
            # requestlogger.info('updating ecg ')
            ecgobj = reportdata.ecg
            ecgobj.lead1.leaddata = ecg_req[ServerConstants.LEAD1]
            ecgobj.lead2.leaddata = ecg_req[ServerConstants.LEAD2]
            ecgobj.lead3.leaddata = ecg_req[ServerConstants.LEAD3]
            ecgobj.avr.leaddata = ecg_req[ServerConstants.AVR]
            ecgobj.avl.leaddata = ecg_req[ServerConstants.AVL]
            ecgobj.avf.leaddata = ecg_req[ServerConstants.AVF]
            ecgobj.v1.leaddata = ecg_req[ServerConstants.V1]
            ecgobj.v2.leaddata = ecg_req[ServerConstants.V2]
            ecgobj.v3.leaddata = ecg_req[ServerConstants.V3]
            ecgobj.v4.leaddata = ecg_req[ServerConstants.V4]
            ecgobj.v5.leaddata = ecg_req[ServerConstants.V5]
            ecgobj.v6.leaddata = ecg_req[ServerConstants.V6]
            try:
                ecgobj.ecg_img = ecg_req[ServerConstants.ECG_IMG]
            except KeyError:
                pass
        else:
            logmessage.append('saving ecg ')
            # requestlogger.info('saving ecg ')
            lead1data = ecgdata.objects.create(leaddata = ecg_req[ServerConstants.LEAD1])
            lead2data = ecgdata.objects.create(leaddata = ecg_req[ServerConstants.LEAD2])
            lead3data = ecgdata.objects.create(leaddata = ecg_req[ServerConstants.LEAD3])
            avrdata = ecgdata.objects.create(leaddata = ecg_req[ServerConstants.AVR])
            avldata = ecgdata.objects.create(leaddata = ecg_req[ServerConstants.AVL])
            avfdata = ecgdata.objects.create(leaddata = ecg_req[ServerConstants.AVF])
            v1data = ecgdata.objects.create(leaddata = ecg_req[ServerConstants.V1])
            v2data = ecgdata.objects.create(leaddata = ecg_req[ServerConstants.V2])
            v3data = ecgdata.objects.create(leaddata = ecg_req[ServerConstants.V3])
            v4data = ecgdata.objects.create(leaddata = ecg_req[ServerConstants.V4])
            v5data = ecgdata.objects.create(leaddata = ecg_req[ServerConstants.V5])
            v6data = ecgdata.objects.create(leaddata = ecg_req[ServerConstants.V6])
            ecg_imgbase64data = ecg_req[ServerConstants.ECG_IMG]
            #save file to disk/cloud
            filestoragedir = FileTypeDIRStorageMap[FileTypeConstants.ECGLEADIMG]
            filestoragepath = os.path.join(settings.MEDIA_ROOT, filestoragedir)
            ecg_imgbase64data += "==="
            img_data = base64.decodebytes(ecg_imgbase64data.encode())
            filenametosave = str(uuid.uuid4()) + ".png"
            filenametosave = utils.getRandomId(8) + str(time.time()) + filenametosave
            fullpathondisk = ''.join([filestoragepath, '/', filenametosave])
            with open(fullpathondisk, "wb") as imagefile:
                imagefile.write(img_data)

            logmessage.append("saved local to disk")
            logmessage.append("file storage dir:" + filestoragedir)
            if filestoragedir == config.GCSIMAGEREPORTBUCKET:
                logmessage.append("saving to GCS")
                foldername = "person" + str(personuid_req)
                savedfileurl = save_uploaded_file_to_gcloud(fullpathondisk, config.GCSIMAGEREPORTBUCKET,
                                                            filenametosave,foldername)

            else:
                savedfileurl = config.DOMAIN + os.path.join(settings.STATIC_URL, settings.UPLOADEDFILES_DIR,
                                                            filestoragedir, filenametosave)
            ecgobj = ecg(person=person,         lead1 = lead1data,
                                            lead2 = lead2data,
                                            lead3 = lead3data,
                                            avr = avrdata,
                                            avl = avldata,
                                            avf = avfdata,
                                            v1 = v1data,
                                            v2 = v2data,
                                            v3 = v3data,
                                            v4 = v4data,
                                            v5 = v5data,
                                            v6 = v6data,
                                            ecg_img=savedfileurl,
                                            appdetails = appdetails)

        ecgobj.save()
        #may be updating..ie. reportid existed but last save didnt have this device at all so then
        #still we reach here
        if reportdata and not reportdata.ecg:
            reportdata.ecg = ecgobj

    

    if not reportdata:
        try:
            logmessage.append('saving report data:')
            # requestlogger.info('saving report data:')
            reportdata = report.objects.create(person=person,reporttype=reporttype_req, bp=bpobj, oximeter=oximeterobj, weight=weightobj,spirometer=spirometerobj,
                                               height=heightobj, temperature=temperatureobj, bloodglucose=bloodglucoseobj,hemoglobin=hemoglobinobj,lipid=lipidobj,
                                                ecg=ecgobj, recommendations = json.dumps(recommendation_dict) if bool(recommendation_dict) else None,
                                               appdetails=appdetails)
            reportdata.reportid = encode_id(reportdata.id)
            reportdata.save()

        except Exception as e:
            backendlogger.error('setReportForPerson', data, str(e), logmessage)
            return HttpResponse("Error saving report", status=403)



    jsondata = dict({ServerConstants.REPORTID:reportdata.reportid })
    httpoutput = utils.successJson(jsondata)
    backendlogger.info("setReportForPerson", data, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def setPrescriptionReportForPerson(request):
    logmessage = []
    data = json.loads(request.body)

    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('setPrescriptionReportForPerson', data, 'error authenticating person', logmessage)
        return HttpResponse("Error authenticating person", status=401)

    logmessage.append('setPrescriptionReportForPerson;%s'%(str(data)))
    # requestlogger.info('setPrescriptionReportForPerson;%s'%(str(data)))
    person=None

    try:
        personuid_req = data[ServerConstants.PERSONUID]
    except KeyError:
        backendlogger.error('setPrescriptionReportForPerson', data, 'person id not found', logmessage)
        return HttpResponse("personuid not found",status=403)

    try:
        doctoruid_req = data[ServerConstants.DOCTORUID]
    except KeyError:
        backendlogger.error('setPrescriptionReportForPerson', data, 'doctoruid not found', logmessage)
        return HttpResponse("doctoruid not found",status=403)

    try:
        person = PersonDetails.objects.get(personuid=personuid_req)
    except ObjectDoesNotExist:
        backendlogger.error('setPrescriptionReportForPerson', data, 'person doesnt exist', logmessage)
        return HttpResponse("person doesnt exist",status=404)

    try:
        doctor = DoctorDetails.objects.get(doctoruid=doctoruid_req)
    except ObjectDoesNotExist:
        backendlogger.error('setPrescriptionReportForPerson', data, 'doctor doesnt exist', logmessage)
        return HttpResponse("doctor doesnt exist",status=404)

    kwargs = {}
    try:
        prescription_req = data[ServerConstants.PRESCRIPTION]
        kwargs['prescription'] = prescription_req
    except KeyError:
       pass

    try:
        investigation_req = data[ServerConstants.INVESTIGATION]
        kwargs['investigation'] = investigation_req
    except KeyError:
        pass

    try:
        clinicalnote_req = data[ServerConstants.CLINICALNOTE]
        kwargs['clinicalnote'] = clinicalnote_req
    except KeyError:
        pass

    try:
        diagnosis_req = data[ServerConstants.DIAGNOSIS]
        kwargs['diagnosis'] = diagnosis_req
    except KeyError:
        pass

    try:
        recommendation_req = data[ServerConstants.RECOMMENDATION]
        kwargs['recommendation'] = recommendation_req
    except KeyError:
        pass

    try:
        followupdays_req = data[ServerConstants.FOLLOWUPDAYS]
        kwargs['followupdatetime'] = datetime.now() + timedelta(days=followupdays_req)
    except KeyError:
        pass

    try:
        appuid_req = data[ServerConstants.APPUID]
    except KeyError:
        backendlogger.error('setPrescriptionReportForPerson', data, 'kioskid not sent', logmessage)
        return HttpResponse("appid not sent",status=403)

    try:
        appdetails = AppDetails.objects.get(appuid=appuid_req)
    except ObjectDoesNotExist:
        backendlogger.error('setPrescriptionReportForPerson', data, 'appdetails doesnt exist', logmessage)
        return HttpResponse("appdetails doesnt exist",status=404)

    try:
        prescriptionid_req = data[ServerConstants.PRESCRIPTIONID]
        prescriptionid = prescriptionid_req
        prescriptionobj = prescription.objects.get(prescriptionid = prescriptionid_req)
        requestlogger.info("Got presaved prescription with id:" + prescriptionid_req)
        for k, v in kwargs.items():
            setattr(prescriptionobj, k, v)
        prescriptionobj.save()
        requestlogger.info("Resaved prescription with id:" + prescriptionid_req)

    except:
        try:
            prescriptionobj = prescription.objects.create(person=person,
                                    doctor = doctor,
                                    appdetails=appdetails,
                                    **kwargs)
            prescriptionobj.prescriptionid = encode_id(prescriptionobj.id)
            prescriptionobj.save()
            prescriptionid = prescriptionobj.prescriptionid
        except Exception as e:
            backendlogger.error('setPrescriptionReportForPerson', data, str(e), logmessage)
            return HttpResponse("Error saving prescription", status=404)

    jsondata = dict({ServerConstants.PRESCRIPTIONID:prescriptionid})

    httpoutput = utils.successJson(jsondata)
    backendlogger.info('setPrescriptionReportForPerson', data, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

def retrieveLastPrescription(request):
    logmessage = []
    data = json.loads(request.body)

    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('retrieveLastPrescription', data, 'error authenticating person', logmessage)
        return HttpResponse("Error authenticating person", status=401)

    logmessage.append('retrieveLastPrescription;%s' % (str(data)))

    try:
        personuid_req = data[ServerConstants.PERSONUID]
    except KeyError:
        backendlogger.error('retrieveLastPrescription', data, 'person id not found', logmessage)
        return HttpResponse("personuid not found", status=403)

    try:
        doctoruid_req = data[ServerConstants.DOCTORUID]
    except KeyError:
        backendlogger.error('retrieveLastPrescription', data, 'doctoruid not found', logmessage)
        return HttpResponse("doctoruid not found", status=403)

    try:
        person = PersonDetails.objects.get(personuid=personuid_req)
    except ObjectDoesNotExist:
        backendlogger.error('retrieveLastPrescription', data, 'person doesnt exist', logmessage)
        return HttpResponse("person doesnt exist", status=404)

    try:
        doctor = DoctorDetails.objects.get(doctoruid=doctoruid_req)
    except ObjectDoesNotExist:
        backendlogger.error('retrieveLastPrescription', data, 'doctor doesnt exist', logmessage)
        return HttpResponse("doctor doesnt exist", status=404)

    # get all kiosk reports
    prescriptiondata = prescription.objects.filter(person=person, doctor=doctor).order_by('-datetime').first()
    if prescriptiondata is None:
        backendlogger.error('retrieveLastPrescription', data,
                            'retrieveLastPrescription: not found for doctoruid:{}, personuid: {}'.format(doctoruid_req, personuid_req), logmessage)
        # requestlogger.info('getPrescription:pres id not found:'+prescriptionid_req)
        return HttpResponse('retrieveLastPrescription: not found for doctoruid:{}, personuid: {}', status=403)

    jsondata = dict({
        ServerConstants.PRESCRIPTIONID: prescriptiondata.prescriptionid,
        ServerConstants.PRESCRIPTION: prescriptiondata.prescription,
        ServerConstants.INVESTIGATION: prescriptiondata.investigation,
        ServerConstants.CLINICALNOTE: prescriptiondata.clinicalnote,
        ServerConstants.DIAGNOSIS: prescriptiondata.diagnosis,
        ServerConstants.RECOMMENDATION: prescriptiondata.recommendation,
        ServerConstants.FOLLOWUPDATETIME: prescriptiondata.followupdatetime,
    })

    httpoutput = utils.successJson(jsondata)
    backendlogger.info('retrieveLastPrescription', data, httpoutput, logmessage)
    # requestlogger.info('getPrescriptionResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

@csrf_exempt
def getVitalReport(request):
    # requestlogger.info('================getAppReport=====================')
    logmessage = []
    logrequest = {}
    for key, value in request.GET.items():
        logrequest[key]=value

    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getAppReport', logrequest, 'Error authenticating person', logmessage)
        return HttpResponse("Error authenticating person", status=401)

    reportid_req = None
    personuid_req = None
    # logmessage = getGETRequstPAramJSON(request)
    try:
        reportid_req = request.GET[ServerConstants.REPORTID]
        logmessage.append('getAppReport:reportis;%s'%(str(reportid_req)))
        # requestlogger.info('getAppReport:reportis;%s'%(str(reportid_req)))
    except KeyError:
        try:
            personuid_req = request.GET[ServerConstants.PERSONUID]
            logmessage.append('getAppReport:personuid;%s'%(str(personuid_req)))
            # requestlogger.info('getAppReport:personuid;%s'%(str(personuid_req)))
        except KeyError:
            backendlogger.error('getAppReport', logrequest, 'reportid not sent in request', logmessage)
            # requestlogger.info('getAppReport:reportid/personuid not sent in request')
            return HttpResponse("reportid not sent in request",status=403)

    if(personuid_req!=None):
        userreports = report.objects.filter(person__personuid=personuid_req)
        if(len(userreports)>0):
            thisreport = userreports[len(userreports)-1]
        else:
            backendlogger.error('getAppReport', logrequest, 'getAppReport:personuid not found', logmessage)
            # requestlogger.info('getAppReport:personuid not found:'+personuid_req)
            return HttpResponse("reportid not found",status=403)
    else:
        try:
            thisreport = report.objects.get(reportid=reportid_req)
        except ObjectDoesNotExist:
            backendlogger.error('getAppReport', logrequest, 'getAppReport:reportid not found', logmessage)
            # requestlogger.info('getAppReport:reportid not found:'+reportid_req)
            return HttpResponse("reportid not found",status=403)


    if thisreport.bp==None :
        bpdata = None
    else:
        bpdata = model_to_dict(thisreport.bp, fields=('systolic', 'diastolic', 'pulse'))
        age = timezone.now().year-thisreport.person.dob_year if timezone.now().year-thisreport.person.dob_year <= 40 else 'other'
        systolic_inference, diastolic_inference, user_bp_range = get_bp_inference(bpdata, age) # second param is person's age
        bpdata['systolic_inference'] = systolic_inference
        bpdata['systolic_range'] = user_bp_range['systolic_range']
        bpdata['diastolic_inference'] = diastolic_inference
        bpdata['diastolic_range'] = user_bp_range['diastolic_range']
        backendlogger.error('getAppReport', logrequest, 'getAppReport:bpdata', bpdata)

    if thisreport.bloodglucose==None :
        bloodglucosedata = None
    else:
        bloodglucosedata = model_to_dict(thisreport.bloodglucose, fields=('glucose_level', 'testtypecode'))
        bloodglucosedata["bg_inference"], bloodglucosedata["bg_range"] = get_bg_inference(thisreport.bloodglucose.glucose_level, thisreport.bloodglucose.testtypecode)

    # if thisreport.weight==None :
    if thisreport.weight==None or (not thisreport.weight.bmi and not thisreport.weight.fat and not thisreport.weight.muscle and not thisreport.weight.hydration):
        weightdata = None
    else:
        weightdata = model_to_dict(thisreport.weight, fields=('weight', 'height', 'bmi', 'fat', 'bonemass', 'muscle', 'hydration','kcal','vfat','bage','prot', 'muscle_quality_score', 'physique_rating'))
        age = timezone.now().year-thisreport.person.dob_year if timezone.now().year-thisreport.person.dob_year <= 20 else 'other'
        bmi_inference, user_bmi_range = get_bmi_inference(thisreport.weight.bmi, age) # second param is person's age
        hydration_inference, hydration_range = get_hydration_inference(thisreport.weight.hydration, thisreport.person.gender)
        bonemass_inference, bonemass_range = get_bonemass_inference(thisreport.weight.bonemass, thisreport.person.gender)
        muscle_inference, muscle_range = get_muscle_inference(thisreport.weight.muscle, thisreport.person.gender, thisreport.person.get_age())
        fat_inference, fat_range = get_fat_inference(thisreport.weight.fat, thisreport.person.gender, thisreport.person.get_age())
        vfat_inference = get_vfat_inference(thisreport.weight.vfat)
        sfat_inference = get_sfat_inference(thisreport.weight.sfat,thisreport.person.gender)

        if thisreport.weight.kcal:
            bmr_inference, ideal_bmr = get_bmr_inference(thisreport.weight.kcal, thisreport.weight.weight,
                                                                                    thisreport.weight.height, thisreport.person.get_age(), thisreport.person.gender)
        else:
            bmr_inference = None
            ideal_bmr = None

        if thisreport.weight.muscle_quality_score:
            muscle_quality_inference, muscle_quality_range = get_muscle_quality_inference(thisreport.weight.muscle_quality_score, thisreport.person.gender, thisreport.person.get_age())
        else:
            muscle_quality_inference = None
            muscle_quality_range = None

        if thisreport.weight.bage:
            metabolic_age_inference = get_metabolic_age_inference(thisreport.weight.bage, thisreport.person.get_age())
        else:
            metabolic_age_inference = None

        weightdata['bmi_inference'] = bmi_inference
        weightdata['fat_inference'] = fat_inference
        weightdata['vfat_inference'] = vfat_inference
        weightdata['sfat_inference'] = sfat_inference
        weightdata['bmr_inference'] = bmr_inference
        weightdata['hydration_inference'] = hydration_inference
        weightdata['bonemass_inference'] = bonemass_inference
        weightdata['muscle_inference'] = muscle_inference
        weightdata['metabolic_age_inference'] = metabolic_age_inference
        weightdata['muscle_quality_inference'] = muscle_quality_inference
        weightdata['ideal_bmr'] = ideal_bmr

        weightdata['bmi_range'] = user_bmi_range
        weightdata['fat_range'] = fat_range
        weightdata['vfat_range'] = VFATRANGE
        weightdata['sfat_range'] =  SFATRANGE[thisreport.person.gender.lower()]
        weightdata['hydration_range'] = hydration_range
        weightdata['bonemass_range'] = bonemass_range
        weightdata['muscle_range'] = muscle_range
        weightdata['muscle_quality_range'] = muscle_quality_range

    #height also comes in body_stats but still keeping this on
    if thisreport.height==None :
        heightdata = None
    else:
        heightdata = model_to_dict(thisreport.height, fields=('height'))

    if thisreport.oximeter==None :
        oximeterdata = None
    else:
        oximeterdata = model_to_dict(thisreport.oximeter, fields=('pulse', 'oxygensat'))
        age_for_pulse_inference = timezone.now().year-thisreport.person.dob_year if timezone.now().year-thisreport.person.dob_year <= 15 else 'other'
        pulse_inference, pulse_range = get_pulse_inference(thisreport.oximeter.pulse, age_for_pulse_inference)
        oxygensat_inference, oxygensat_inference_range = get_oxygensat_inference(thisreport.oximeter.oxygensat)
        oximeterdata["pulse_inference"] = pulse_inference
        oximeterdata["pulse_range"] = pulse_range
        oximeterdata["oxygensat_inference"] = oxygensat_inference
        oximeterdata["oxygensat_range"] = oxygensat_inference_range

    if thisreport.temperature==None :
        temperaturedata = None
    else:
        temperaturedata = model_to_dict(thisreport.temperature, fields=('temperature'))
        temperaturedata['temperatureincelsius'] =(temperaturedata['temperature']-32)*5/9
        temprature_inference, temprature_range = get_temperature_inference(thisreport.temperature.temperature)
        temperaturedata["temperature_inference"] = temprature_inference
        temperaturedata["temperature_range"] = temprature_range

    if thisreport.ecg==None :
        ecgdata = None
    else:
        ecgdata = dict({
            ServerConstants.ECG_IMG: thisreport.ecg.ecg_img,
        })

    if thisreport.spirometer == None:
        spirometerdata = None
    else:
        spirometerdata = model_to_dict(thisreport.spirometer,
                                       fields=('fvc', 'fev1', 'pef', 'fev1per', 'fef25', 'fef75', 'fef2575'))

    if thisreport.hemoglobin==None :
        hemoglobindata = None
    else:
        hemoglobindata = model_to_dict(thisreport.hemoglobin, fields=('hb', 'hct'))
        hemoglobindata["hb_inference"], hemoglobindata["hb_range"] = get_hb_inference(thisreport.hemoglobin.hb, thisreport.person.gender)

    if thisreport.lipid==None :
        lipiddata = None
    else:
        lipiddata = model_to_dict(thisreport.lipid, fields=('tc', 'tg', 'hdl', 'ldl', 'hdlratio', 'nonhdl'))
        lipiddata["tc_inference"], lipiddata["tc_range"] = get_tc_inference(thisreport.lipid.tc)
        lipiddata["hdl_inference"], lipiddata["hdl_range"] = get_hdl_inference(thisreport.lipid.hdl)
        lipiddata["ldl_inference"], lipiddata["ldl_range"] = get_ldl_inference(thisreport.lipid.ldl)
        lipiddata["tg_inference"], lipiddata["tg_range"] = get_tg_inference(thisreport.lipid.tg)

    if thisreport.recommendations == None:
        recommendationdict = None
    else:
        recommendationdict = json.loads(thisreport.recommendations)


    jsondata = dict({   ServerConstants.HISTORYTYPE: "vitalreport",
                        ServerConstants.REPORTID: thisreport.reportid,
                        ServerConstants.REPORTTYPE: thisreport.reporttype,
                        ServerConstants.DATETIME: time.mktime(timezone.localtime(thisreport.datetime).timetuple()),
                        ServerConstants.USERNAME: thisreport.person.name,
                        ServerConstants.PERSONUID: thisreport.person.personuid,
                        ServerConstants.AGE: timezone.now().year-thisreport.person.dob_year,
                        ServerConstants.GENDER: thisreport.person.gender,
                        ServerConstants.EMAIL: thisreport.person.email,
                        ServerConstants.HEIGHT: thisreport.person.userheight,
                        ServerConstants.WEIGHT: thisreport.person.userweight,
                        ServerConstants.APPLOCATION: thisreport.appdetails.applocation,
                        ServerConstants.APPUID: thisreport.appdetails.appuid,
                        ServerConstants.PARTNER: thisreport.appdetails.partner,
                        ServerConstants.BPMACHINE: bpdata,
                        ServerConstants.BLOODGLUCOSEMACHINE: bloodglucosedata,
                        ServerConstants.SPIROMETERMACHINE: spirometerdata,
                        ServerConstants.HEMOGLOBINMACHINE: hemoglobindata,
                        ServerConstants.LIPIDMACHINE: lipiddata,
                        ServerConstants.ECGMACHINE: ecgdata,
                        ServerConstants.WEIGHTMACHINE: weightdata,
                        ServerConstants.HEIGHTMACHINE: heightdata,
                        ServerConstants.OXIMETERMACHINE: oximeterdata,
                        ServerConstants.TEMPERATUREMACHINE: temperaturedata,
                        ServerConstants.RECOMMENDATIONS: recommendationdict
                        })

    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getAppReport', logrequest, httpoutput, logmessage)
    # requestlogger.info('getAppReportResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)

@csrf_exempt
def getPrescription(request):
    logmessage = []
    logrequest = {}
    for key, value in request.GET.items():
        logrequest[key]=value
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getPrescription', logrequest, 'Error authenticating person', logmessage)
        return HttpResponse("Error authenticating person", status=401)

    try:
        prescriptionid_req = request.GET[ServerConstants.PRESCRIPTIONID]
        logmessage.append('getPrescription;%s'%(str(prescriptionid_req)))
        # requestlogger.info('getPrescription;%s'%(str(prescriptionid_req)))
    except KeyError:
        backendlogger.error('getPrescription', logrequest, 'getPrescription:personuid not sent in request', logmessage)
        # requestlogger.info('getPrescription:personuid not sent in request')
        return HttpResponse("PRESCRIPTIONID not sent in request",status=403)

    #get all kiosk reports
    try:
        prescriptiondata = prescription.objects.get(prescriptionid=prescriptionid_req)
    except ObjectDoesNotExist:
        backendlogger.error('getPrescription', logrequest, 'getPrescription:pres id not found:' + prescriptionid_req, logmessage)
        # requestlogger.info('getPrescription:pres id not found:'+prescriptionid_req)
        return HttpResponse("PRESCRIPTIONID not found",status=403)


    jsondata = dict({
                        ServerConstants.HISTORYTYPE:"prescription",
                        ServerConstants.PRESCRIPTIONID:prescriptiondata.prescriptionid,
                        ServerConstants.DOCTORNAME:prescriptiondata.doctor.name,
                        ServerConstants.DOCTORPHONE:prescriptiondata.doctor.phone,
                        ServerConstants.DOCTOREXPERIENCE:prescriptiondata.doctor.experience,
                        ServerConstants.DOCTORQUALIFICATIONS:prescriptiondata.doctor.qualification,
                        ServerConstants.REGISTRATIONNUM:prescriptiondata.doctor.registrationnum,
                        ServerConstants.DOCTORSPECIALIZATION:prescriptiondata.doctor.specialization,
                        ServerConstants.SIGNPIC:prescriptiondata.doctor.signpic,
                        ServerConstants.PRESCRIPTION:prescriptiondata.prescription,
                        ServerConstants.INVESTIGATION:prescriptiondata.investigation,
                        ServerConstants.PARTNER:prescriptiondata.appdetails.partner,
                        ServerConstants.CLINICALNOTE:prescriptiondata.clinicalnote,
                        ServerConstants.DIAGNOSIS: prescriptiondata.diagnosis,
                        ServerConstants.RECOMMENDATION: prescriptiondata.recommendation,
                        ServerConstants.USERNAME:prescriptiondata.person.name,
                        ServerConstants.PERSONUID:prescriptiondata.person.personuid,
                        ServerConstants.AGE:timezone.now().year-prescriptiondata.person.dob_year,
                        ServerConstants.GENDER:prescriptiondata.person.gender,
                        ServerConstants.APPLOCATION:prescriptiondata.appdetails.applocation,
                        ServerConstants.DATETIME:prescriptiondata.datetime,
                        ServerConstants.FOLLOWUPDATETIME: prescriptiondata.followupdatetime,
    })

    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getPrescription', logrequest, httpoutput, logmessage)
    # requestlogger.info('getPrescriptionResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)

@csrf_exempt
def getImageReport(request):
    # requestlogger.info('================getScannedReport=====================')
    logmessage = []
    logrequest = {}
    for key, value in request.GET.items():
        logrequest[key] = value
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getScannedReport', logrequest, 'Error authenticating person', logmessage)
        return HttpResponse("Error authenticating person", status=401)

    try:
        scanreportid_req = request.GET[ServerConstants.IMAGEREPORTID]
        logmessage.append('getScannedReport;%s'%(str(scanreportid_req)))
        # requestlogger.info('getScannedReport;%s'%(str(scanreportid_req)))
    except KeyError:
        backendlogger.error('getScannedReport', logrequest, 'getScannedReport:personuid not sent in request', logmessage)
        # requestlogger.info('getScannedReport:personuid not sent in request')
        return HttpResponse("getScannedReportID not sent in request",status=403)

    try:
        scannedreportdata = imagereportdetails.objects.get(imagereportid=scanreportid_req)
    except ObjectDoesNotExist:
        backendlogger.error('getScannedReport', logrequest, 'getScannedReport:pres id not found:' + scanreportid_req, logmessage)
        # requestlogger.info('getScannedReport:pres id not found:'+scanreportid_req)
        return HttpResponse("getScannedReportID not found",status=403)

    jsondata = dict({
                                ServerConstants.HISTORYTYPE:"imagereport",
                                ServerConstants.IMAGEREPORTID:scannedreportdata.imagereportid,
                                ServerConstants.IMAGEREPORTKEY:scannedreportdata.imagereportkey,
                                ServerConstants.IMAGEREPORTTITLE:scannedreportdata.reporttitle,
                                ServerConstants.IMAGEREPORTDESCRIPTION:scannedreportdata.reportdescription,
                                ServerConstants.IMAGEREPORTIMGURL:scannedreportdata.reportimgurl,
                                ServerConstants.USERNAME:scannedreportdata.person.name,
                                ServerConstants.PERSONUID:scannedreportdata.person.personuid,
                                ServerConstants.AGE:timezone.now().year-scannedreportdata.person.dob_year,
                                ServerConstants.GENDER:scannedreportdata.person.gender,
                                ServerConstants.DATETIME:time.mktime(timezone.localtime(scannedreportdata.datetime).timetuple()),
                               })

    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getScannedReport', logrequest, httpoutput, logmessage)
    # requestlogger.info('getScannedReportResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def getTestDataForDateRangeAndApp(request):
    # requestlogger.info('================getTestDataForDateRangeAndApp=====================')
    logmessage = []
    logrequest = {}
    for key, value in request.GET.items():
        logrequest[key] = value
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getTestDataForDateRangeAndApp', logrequest, 'Error authenticating person', logmessage)
        return HttpResponse("Error authenticating person", status=401)

    try:
        startdate = request.GET[ServerConstants.STARTDATE]
    except KeyError:
        startdate = datetime(1,2,1)
    try:
        enddate = request.GET[ServerConstants.ENDDATE]
    except KeyError:
        enddate = datetime.now().date()

    try:
        machines = request.GET[ServerConstants.MACHINESTR]
    except:
        machines = None

    try:
        appstr = request.GET[ServerConstants.APPSTR]
    except:
        appstr = None

    if appstr == None or machines == None:
        backendlogger.error('getTestDataForDateRangeAndApp', logrequest, 'Please specify atleast one machine and kiosk', logmessage)
        return HttpResponse("Please specify atleast one machine and kiosk",status=403)
    logmessage.append('getTestDataForDateRangeAndApp ForDates:%s,%s'%(str(startdate),str(enddate)))
    # requestlogger.info('getTestDataForDateRangeAndApp ForDates:%s,%s'%(str(startdate),str(enddate)))

    machinearray = machines.split(',')
    apparray = appstr.split(',')

    bpdata = list()
    bloodglucosedata = list()
    hbdata = list()
    weightdata = list()
    heightdata = list()
    oximeterdata = list()
    temperaturedata = list()
    ecgdata = list()
    lipiddata = list()
    hivdata = list()
    malariadata = list()
    denguedata = list()
    typhoiddata = list()
    eyedata = list()
    urinedata = list()
    dermadata = list()
    otodata = list()
    pregnancydata =list()
    userdata = list()
    userdemographics = list()
    vcdata =list()
    transactiondata = list()


    report_list = list()
    prescription_list = list()

    for machine in machinearray:
        if machine==ServerConstants.BPMACHINE:
            allbpdata = bp.objects.filter(datetime__range=(startdate,enddate),appdetails__appuid__in=apparray).order_by("-datetime")
            # bpdata = list()
            for thisbp in allbpdata:
                bpdata.append({
                                ServerConstants.DATETIME:get_localize_datetime(thisbp.datetime),
                                ServerConstants.PERSONUID:thisbp.person.personuid,
                                ServerConstants.SYSTOLIC:thisbp.systolic,
                                ServerConstants.DIASTOLIC:thisbp.diastolic,
                                ServerConstants.PULSE:thisbp.pulse,
                                ServerConstants.APPTAG:thisbp.appdetails.apptag,
                            })

        if machine==ServerConstants.OXIMETERMACHINE:
            alloximeterdata = oximeter.objects.filter(datetime__range=(startdate,enddate),appdetails__appuid__in=apparray).order_by("-datetime")
            # oximeterdata = list()
            for thisox in alloximeterdata:
                oximeterdata.append({
                                ServerConstants.DATETIME:get_localize_datetime(thisox.datetime),
                                ServerConstants.PERSONUID:thisox.person.personuid,
                                ServerConstants.OXYGENSAT:thisox.oxygensat,
                                ServerConstants.PULSE:thisox.pulse,
                                ServerConstants.APPTAG:thisox.appdetails.apptag,
                            })

        if machine==ServerConstants.BLOODGLUCOSEMACHINE:
            allbloodglucosedata = bloodglucose.objects.filter(datetime__range=(startdate,enddate),appdetails__appuid__in=apparray).order_by("-datetime")
            # bloodglucosedata = list()
            for thisbloodglucose in allbloodglucosedata:
                bloodglucosedata.append({
                                ServerConstants.DATETIME:get_localize_datetime(thisbloodglucose.datetime),
                                ServerConstants.PERSONUID:thisbloodglucose.person.personuid,
                                ServerConstants.BLOODGLUCOSE:thisbloodglucose.glucose_level,
                                ServerConstants.APPTAG:thisbloodglucose.appdetails.apptag,
                            })

        if machine==ServerConstants.WEIGHTMACHINE:
            allweightdata = weight.objects.filter(datetime__range=(startdate,enddate),appdetails__appuid__in=apparray).order_by("-datetime")
            # weightdata = list()
            for thisweight in allweightdata:
                weightdata.append({
                                ServerConstants.DATETIME:get_localize_datetime(thisweight.datetime),
                                ServerConstants.PERSONUID:thisweight.person.personuid,
                                ServerConstants.WEIGHT:thisweight.weight,
                                ServerConstants.HEIGHT:thisweight.height,
                                ServerConstants.FAT:thisweight.fat,
                                ServerConstants.BMI:thisweight.bmi,
                                ServerConstants.BONEMASS:thisweight.bonemass,
                                ServerConstants.HYDRATION:thisweight.hydration,
                                ServerConstants.MUSCLE:thisweight.muscle,
                                ServerConstants.KCAL:thisweight.kcal,
                                ServerConstants.VFAT:thisweight.vfat,
                                ServerConstants.SFAT:thisweight.sfat,
                                ServerConstants.BAGE:thisweight.bage,
                                ServerConstants.PROT:thisweight.prot,
                                ServerConstants.APPTAG:thisweight.appdetails.apptag,
                            })

        if machine==ServerConstants.HEIGHTMACHINE:
            allheightdata = height.objects.filter(datetime__range=(startdate,enddate),appdetails__appuid__in=apparray).order_by("-datetime")
            # heightdata = list()
            for thisheight in allheightdata:
                heightdata.append({
                                ServerConstants.DATETIME:get_localize_datetime(thisheight.datetime),
                                ServerConstants.PERSONUID:thisheight.person.personuid,
                                ServerConstants.WEIGHT:thisheight.height,
                                ServerConstants.APPTAG:thisheight.appdetails.apptag,
                            })

        if machine==ServerConstants.TEMPERATUREMACHINE:
            alltemperaturedata = temperature.objects.filter(datetime__range=(startdate,enddate),appdetails__appuid__in=apparray).order_by("-datetime")
            # temperaturedata = list()
            for thistemperature in alltemperaturedata:
                temperaturedata.append({
                                ServerConstants.DATETIME:get_localize_datetime(thistemperature.datetime),
                                ServerConstants.PERSONUID:thistemperature.person.personuid,
                                ServerConstants.TEMPERATURE:thistemperature.temperature,
                                ServerConstants.APPTAG:thistemperature.appdetails.apptag,
                            })

        if machine==ServerConstants.HEMOGLOBINMACHINE:
            allhbdata = hemoglobin.objects.filter(datetime__range=(startdate,enddate),appdetails__appuid__in=apparray).order_by("-datetime")
            # hbdata = list()
            for thishb in allhbdata:
                hbdata.append({
                                ServerConstants.DATETIME:get_localize_datetime(thishb.datetime),
                                ServerConstants.PERSONUID:thishb.person.personuid,
                                ServerConstants.HB:thishb.hb,
                                ServerConstants.HCT:thishb.hct,
                                ServerConstants.APPTAG:thishb.appdetails.apptag,
                            })

        if machine==ServerConstants.ECGMACHINE:
            allecgdata = ecg.objects.filter(datetime__range=(startdate,enddate),appdetails__appuid__in=apparray).order_by("-datetime")
            # ecgdata = list()
            for thisecg in allecgdata:
                ecgdata.append({
                                ServerConstants.DATETIME:get_localize_datetime(thisecg.datetime),
                                ServerConstants.PERSONUID:thisecg.person.personuid,
                                ServerConstants.APPTAG:thisecg.appdetails.apptag,
                            })

        if machine==ServerConstants.LIPIDMACHINE:
            alllipiddata = lipid.objects.filter(datetime__range=(startdate,enddate),appdetails__appuid__in=apparray).order_by("-datetime")
            # lipiddata = list()
            for thislipid in alllipiddata:
                lipiddata.append({
                                ServerConstants.DATETIME:get_localize_datetime(thislipid.datetime),
                                ServerConstants.PERSONUID:thislipid.person.personuid,
                                ServerConstants.TC:thislipid.tc,
                                ServerConstants.TG:thislipid.tg,
                                ServerConstants.HDL:thislipid.hdl,
                                ServerConstants.LDL:thislipid.ldl,
                                ServerConstants.HDLRATIO:thislipid.hdlratio,
                                ServerConstants.NONHDL:thislipid.nonhdl,
                                ServerConstants.APPTAG:thislipid.appdetails.apptag,
                            })


        if machine==ServerConstants.URINEMACHINE:
            allurinedata = urine.objects.filter(datetime__range=(startdate,enddate),appdetails__appuid__in=apparray).order_by("-datetime")
            for thisurine in allurinedata:
                urinedata.append({
                                ServerConstants.DATETIME:get_localize_datetime(thisurine.datetime),
                                ServerConstants.PERSONUID:thisurine.person.personuid,
                                ServerConstants.APPTAG:thisurine.appdetails.apptag,
                            })

        if machine==ServerConstants.OTOSCOPEMACHINE:
            allotodata = imagereportdetails.objects.filter(datetime__range=(startdate, enddate), appdetails__appuid__in=apparray).order_by("-datetime")
            for thisoto in allotodata:
                if thisoto.reporttype == "otoscopeimg" :
                    otodata.append({
                                ServerConstants.DATETIME:get_localize_datetime(thisoto.datetime),
                                ServerConstants.PERSONUID:thisoto.person.personuid,
                                ServerConstants.APPTAG:thisoto.appdetails.apptag,
                            })

        if machine==ServerConstants.DERMASCOPEMACHINE:
            alldermadata = imagereportdetails.objects.filter(datetime__range=(startdate, enddate), appdetails__appuid__in=apparray).order_by("-datetime")
            for thisderma in alldermadata:
                if thisderma.reporttype == "dermascopeimg" :
                    dermadata.append({
                                ServerConstants.DATETIME:get_localize_datetime(thisderma.datetime),
                                ServerConstants.PERSONUID:thisderma.person.personuid,
                                ServerConstants.APPTAG:thisderma.appdetails.apptag,
                            })


        # Getting Person Details here
        if machine==ServerConstants.PERSONLIST:
            enddate = datetime.strptime(str(enddate), '%Y-%m-%d') - timedelta(days=1)
            alluserdata = PersonDetails.objects.filter(date_joined__range=(startdate, enddate), appuid__in=apparray).order_by("-date_joined")
            # userdata = list()
            for thisuser in alluserdata:
                userdata.append({
                                ServerConstants.USERNAME:thisuser.name,
                                ServerConstants.EMAIL:thisuser.email,
                                ServerConstants.PHONE:thisuser.phone,
                                ServerConstants.GENDER:thisuser.gender,
                                ServerConstants.APPUID:thisuser.appuid,
                                ServerConstants.DATETIME:thisuser.date_joined.strftime('%d %b %Y'),
                            })

        # Getting Person Demographics here
        if machine==ServerConstants.USERDEMOGRAPHICS:
            enddate = datetime.strptime(enddate, '%Y-%m-%d') - timedelta(days=1)
            alluserdata = PersonDetails.objects.filter(date_joined__range=(startdate, enddate), appuid__in=apparray).order_by("-date_joined")
            # userdata = list()
            for thisuser in alluserdata:
                userdemographics.append({
                                ServerConstants.PERSONUID:thisuser.personuid,
                                ServerConstants.AGE:timezone.now().year-thisuser.dob_year,
                                ServerConstants.GENDER:thisuser.gender,
                                ServerConstants.APPLOCATION:AppDetails.objects.get(appuid=thisuser.appuid).kiosklocation,
                                ServerConstants.DATETIME:thisuser.date_joined.strftime('%d %b %Y')
                            })

       
        if machine==ServerConstants.BASICREPORTDATA:
            res = report.objects.filter(datetime__range=(startdate,enddate),appdetails__appuid__in=apparray,reporttype=0).order_by("-datetime")
            for _report in res:
                report_list.append({
                                ServerConstants.PERSONUID:_report.person.personuid,
                                ServerConstants.USERNAME: _report.person.name,
                                ServerConstants.REPORTID:_report.reportid,
                                ServerConstants.REPORTTYPE: _report.reporttype,
                                ServerConstants.DATETIME: get_localize_datetime(_report.datetime),
                                ServerConstants.APPUID: _report.appdetails.appuid,
                                ServerConstants.APPTAG: _report.appdetails.apptag,
                            })

        if machine==ServerConstants.PRESCRIPTIONDATA:
            res = prescription.objects.filter(datetime__range=(startdate,enddate),appdetails__appuid__in=apparray).order_by("-datetime")
            for _prescription in res:
                prescription_list.append({
                                ServerConstants.PRESCRIPTIONID:_prescription.prescriptionid,
                                ServerConstants.PRESCRIPTION: _prescription.prescription,
                                ServerConstants.DATETIME: get_localize_datetime(_prescription.datetime),
                                ServerConstants.APPUID: _prescription.appdetails.appuid,
                                ServerConstants.APPTAG: _prescription.appdetails.apptag,
                                ServerConstants.INVESTIGATION: _prescription.investigation,
                                ServerConstants.CLINICALNOTE: _prescription.clinicalnote,
                            })


    jsondata = dict({
        ServerConstants.BPMACHINE:bpdata,
        ServerConstants.BLOODGLUCOSEMACHINE:bloodglucosedata,
        ServerConstants.HEMOGLOBINMACHINE:hbdata,
        ServerConstants.LIPIDMACHINE:lipiddata,
        ServerConstants.WEIGHTMACHINE:weightdata,
        ServerConstants.HEIGHTMACHINE:heightdata,
        ServerConstants.OXIMETERMACHINE:oximeterdata,
        ServerConstants.TEMPERATUREMACHINE:temperaturedata,
        ServerConstants.URINEMACHINE:urinedata,
        ServerConstants.PREGNANCYMACHINE:pregnancydata,
        ServerConstants.HIVMACHINE:hivdata,
        ServerConstants.MALARIAMACHINE:malariadata,
        ServerConstants.DENGUEMACHINE:denguedata,
        ServerConstants.TYPHOIDMACHINE:typhoiddata,
        ServerConstants.ECGMACHINE:ecgdata,
        ServerConstants.PERSONLIST:userdata,
        ServerConstants.USERDEMOGRAPHICS:userdemographics,
        ServerConstants.CONSULTLIST:vcdata,
        ServerConstants.BASICREPORTDATA:report_list,
        ServerConstants.PRESCRIPTIONDATA:prescription_list
    })

    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getTestDataForDateRangeAndApp', logrequest, httpoutput, logmessage)
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def getAllTestCountForDateRangeAndApp(request):
    # requestlogger.info('================getAllTestCountForDateRangeAndApp=====================')
    # requestlogger.info('================setReportForPerson=====================')
    data = json.loads(request.body)
    logmessage = []
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error("getAllTestCountForDateRangeAndApp", data, "Error authenticating person", logmessage)
        return HttpResponse("Error authenticating person", status=401)

    try:
        start_date = data[ServerConstants.STARTDATE]
        startdate = datetime.strptime(start_date, '%d-%m-%Y').replace(hour=0, minute=0, second=0)
    except KeyError:
        startdate = datetime(1,2,1)
    try:
        end_date = data[ServerConstants.ENDDATE]
        enddate = datetime.strptime(end_date, '%d-%m-%Y').replace(hour=23, minute=59, second=59)
    except KeyError:
        enddate = datetime.now().date()

    try:
        appstr = data[ServerConstants.APPSTR]
    except:
        appstr = None

    if appstr == None:
        backendlogger.error('getAllTestCountForDateRangeAndApp', data, 'Please specify atleast one app', logmessage)
        return HttpResponse("Please specify atleast one app",status=403)

    logmessage.append('getTestDataForDateRangeAndApp ForDates:%s,%s'%(str(startdate),str(enddate)))
    # requestlogger.info('getTestDataForDateRangeAndApp ForDates:%s,%s'%(str(startdate),str(enddate)))

    apparray = appstr.split(',')

    bpdata = bp.objects.filter(datetime__range=(startdate,enddate),appdetails__appuid__in=apparray)
    bpdateaggregation = list(bpdata.extra({'date_created' : "date(datetime)"}).values('date_created').order_by('date_created').annotate(created_count=Count('id')))
    bpcount = bpdata.count()
    bpdata = { ServerConstants.TOTALCOUNT: bpcount, ServerConstants.COUNTLIST: bpdateaggregation }


    oximeterdata = oximeter.objects.filter(datetime__range=(startdate,enddate),appdetails__appuid__in=apparray)
    oximeterdateaggregation = list(oximeterdata.extra({'date_created': "date(datetime)"}).values('date_created').order_by('date_created').annotate(created_count=Count('id')))
    oximetercount = oximeterdata.count()
    oximeterdata = {ServerConstants.TOTALCOUNT: oximetercount, ServerConstants.COUNTLIST: oximeterdateaggregation}

    bloodglucosedata = bloodglucose.objects.filter(datetime__range=(startdate,enddate),appdetails__appuid__in=apparray)
    bloodglucosedateaggregation = list(bloodglucosedata.extra({'date_created': "date(datetime)"}).values('date_created').order_by('date_created').annotate(created_count=Count('id')))
    bloodglucosecount = bloodglucosedata.count()
    bloodglucosedata = {ServerConstants.TOTALCOUNT: bloodglucosecount, ServerConstants.COUNTLIST: bloodglucosedateaggregation}

    weightdata = weight.objects.filter(datetime__range=(startdate,enddate),appdetails__appuid__in=apparray)
    weightdateaggregation = list(weightdata.extra({'date_created': "date(datetime)"}).values('date_created').order_by('date_created').annotate( created_count=Count('id')))
    weightcount = weightdata.count()
    weightdata = {ServerConstants.TOTALCOUNT: weightcount, ServerConstants.COUNTLIST: weightdateaggregation}

    heightdata = height.objects.filter(datetime__range=(startdate,enddate),appdetails__appuid__in=apparray)
    heightdateaggregation = list(heightdata.extra({'date_created': "date(datetime)"}).values('date_created').order_by('date_created').annotate(created_count=Count('id')))
    heightcount = heightdata.count()
    heightdata = {ServerConstants.TOTALCOUNT: heightcount, ServerConstants.COUNTLIST: heightdateaggregation}

    temperaturedata = temperature.objects.filter(datetime__range=(startdate,enddate),appdetails__appuid__in=apparray)
    temperaturedateaggregation = list(temperaturedata.extra({'date_created': "date(datetime)"}).values('date_created').order_by('date_created').annotate(created_count=Count('id')))
    temperaturecount = temperaturedata.count()
    temperaturedata = {ServerConstants.TOTALCOUNT: temperaturecount, ServerConstants.COUNTLIST: temperaturedateaggregation}

    hbdata = hemoglobin.objects.filter(datetime__range=(startdate,enddate),appdetails__appuid__in=apparray)
    hbdateaggregation = list(hbdata.extra({'date_created': "date(datetime)"}).values('date_created').order_by('date_created').annotate( created_count=Count('id')))
    hbcount = hbdata.count()
    hbdata = {ServerConstants.TOTALCOUNT: hbcount, ServerConstants.COUNTLIST: hbdateaggregation}

    ecgdata = ecg.objects.filter(datetime__range=(startdate,enddate),appdetails__appuid__in=apparray)
    ecgdateaggregation = list(ecgdata.extra({'date_created': "date(datetime)"}).values('date_created').order_by('date_created').annotate(created_count=Count('id')))
    ecgcount = ecgdata.count()
    ecgdata = {ServerConstants.TOTALCOUNT: ecgcount, ServerConstants.COUNTLIST: ecgdateaggregation}

    lipiddata = lipid.objects.filter(datetime__range=(startdate,enddate),appdetails__appuid__in=apparray)
    lipiddateaggregation = list(lipiddata.extra({'date_created': "date(datetime)"}).values('date_created').order_by('date_created').annotate(created_count=Count('id')))
    lipidcount = lipiddata.count()
    lipiddata = {ServerConstants.TOTALCOUNT: lipidcount, ServerConstants.COUNTLIST: lipiddateaggregation}


    spirodata = spirometer.objects.filter(datetime__range=(startdate,enddate),appdetails__appuid__in=apparray)
    spirodateaggregation = list(spirodata.extra({'date_created': "date(datetime)"}).values('date_created').order_by('date_created').annotate(created_count=Count('id')))
    spirocount = spirodata.count()
    spirodata = {ServerConstants.TOTALCOUNT: spirocount, ServerConstants.COUNTLIST: spirodateaggregation}


    #little catch here. From front end we send enddate+1. all other models have DateTime but PersonDetails has Date so its giving false data
    persondata = PersonDetails.objects.filter(datetime_joined__range=(startdate, enddate), appuid__in=apparray)
    persondateaggregation = list(persondata.extra({'date_created': "date(datetime_joined)"}).values('date_created').order_by('date_created').annotate(created_count=Count('personuid')))
    personcount = persondata.count()
    persondata = {ServerConstants.TOTALCOUNT: personcount, ServerConstants.COUNTLIST: persondateaggregation}

    enrolldata = HealthPackageEnrollment.objects.filter(enrolldate__range=(startdate, enddate), appdetails__appuid__in=apparray)
    enrolldateaggregation = list( enrolldata.extra({'date_created': "date(enrolldate)"}).values('date_created').order_by('date_created').annotate(
            created_count=Count('enrollid')))
    enrollcount = enrolldata.count()
    enrolldata = {ServerConstants.TOTALCOUNT: enrollcount, ServerConstants.COUNTLIST: enrolldateaggregation}


    jsondata = list()
    jsondata.append({ServerConstants.NAME: "Person_Registered",ServerConstants.DATA: persondata})
    jsondata.append({ServerConstants.NAME: "Person_Enrolled",ServerConstants.DATA: enrolldata})
    jsondata.append({ServerConstants.NAME: ServerConstants.BPMACHINE,  ServerConstants.DATA:bpdata})
    jsondata.append({ServerConstants.NAME: ServerConstants.BLOODGLUCOSEMACHINE,ServerConstants.DATA:bloodglucosedata})
    jsondata.append({ServerConstants.NAME: ServerConstants.HEMOGLOBINMACHINE,ServerConstants.DATA:hbdata})
    jsondata.append({ServerConstants.NAME: ServerConstants.LIPIDMACHINE,ServerConstants.DATA:lipiddata})
    jsondata.append({ServerConstants.NAME: ServerConstants.WEIGHTMACHINE,ServerConstants.DATA:weightdata})
    jsondata.append({ServerConstants.NAME: ServerConstants.HEIGHTMACHINE,ServerConstants.DATA:heightdata})
    jsondata.append({ServerConstants.NAME: ServerConstants.ECGMACHINE,ServerConstants.DATA:ecgdata})
    jsondata.append({ServerConstants.NAME: ServerConstants.SPIROMETERMACHINE,ServerConstants.DATA:spirodata})
    jsondata.append({ServerConstants.NAME: ServerConstants.OXIMETERMACHINE,ServerConstants.DATA:oximeterdata})
    jsondata.append({ServerConstants.NAME: ServerConstants.TEMPERATUREMACHINE,ServerConstants.DATA:temperaturedata})

    httpoutput = utils.successJson({ServerConstants.ALLITEMARRAY: jsondata})
    backendlogger.info('getAllTestCountForDateRangeAndApp', data, httpoutput, logmessage)
    # requestlogger.info('getAllTestCountForDateRangeAndAppResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def getLatestPersonReport(request):

    # data = json.loads(request.body)
    logmessage = []
    logrequest = {}
    for key, value in request.GET.items():
        logrequest[key]=value

    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getLatestPersonReport', logrequest, 'Error authenticating person', logmessage)
        return HttpResponse("Error authenticating person", status=401)

    personuid_req = None
    userphone_req = None
    try:
        personuid_req = request.GET[ServerConstants.PERSONUID]
    except KeyError:
        backendlogger.error('getMachineDataForPerson', logrequest, 'getLatestPersonReport:person id not found in request', logmessage)
        # requestlogger.info('getMachineDataForPerson:person id/phone not found in request')
        return HttpResponse("person id found in request",status=403)

    if (personuid_req!=None):
        try:
            person = PersonDetails.objects.get(personuid=personuid_req)
        except ObjectDoesNotExist:
            backendlogger.error('getMachineDataForPerson', logrequest, 'getMachineDataForPerson:Person not found', logmessage)
            # requestlogger.info('getMachineDataForPerson:Person not found')
            return HttpResponse("Person not found",status=403)
    else :
        try:
            person = PersonDetails.objects.get(phone=userphone_req)
            personuid_req = person.personuid
        except ObjectDoesNotExist:
            backendlogger.error('getMachineDataForPerson', logrequest, 'getMachineDataForPerson:Person not found', logmessage)
            # requestlogger.info('getMachineDataForPerson:Person not found')
            return HttpResponse("Person not found",status=403)

    try:
        machinestr = request.GET[ServerConstants.MACHINESTR]
        machinearray = machinestr.split(',')
    except KeyError:
        backendlogger.error('getMachineDataForPerson', logrequest, 'getMachineDataForPerson:Machinestr not sent', logmessage)
        # requestlogger.info('getMachineDataForPerson:Person not found')
        return HttpResponse("Machinestr not sent",status=403)

    bpdata = None
    bloodglucosedata = None
    weightdata = None
    heightdata = None
    oximeterdata = None
    temperaturedata = None
    hemoglobindata = None
    lipiddata = None
    ecgdata = None
    dermadata = None

    for machine in machinearray:
        if machine == ServerConstants.WEIGHTMACHINE:
            try:
                weight_objs=weight.objects.filter(person=person)
                if len(weight_objs)>0:
                    weight_obj=weight_objs.latest('datetime')
                    weightdata = model_to_dict(weight_obj,fields=('weight','height','bmi','fat','bonemass','muscle','hydration','kcal','vfat','sfat','bage','prot'))
                    weightdata['datetime']=weight_obj.datetime
            except KeyError:
                pass

        if machine == ServerConstants.HEIGHTMACHINE:
            try:
                height_objs=height.objects.filter(person=person)
                if len(height_objs)>0:
                    height_obj=height_objs.latest('datetime')
                    heightdata=model_to_dict(height_obj,fields=('height'))
                    heightdata['datetime']=height_obj.datetime
            except KeyError:
                pass

        if machine == ServerConstants.BPMACHINE:
            try:
                bp_objs=bp.objects.filter(person=person)
                if len(bp_objs)>0:
                    bp_obj=bp_objs.latest('datetime')
                    bpdata = model_to_dict(bp_obj,fields=('systolic','diastolic','pulse'))
                    bpdata['datetime']=bp_obj.datetime
            except KeyError:
                pass

        if machine == ServerConstants.BLOODGLUCOSEMACHINE:
            try:
                bloodglucose_objs=bloodglucose.objects.filter(person=person)
                if len(bloodglucose_objs)>0:
                    bloodglucose_obj=bloodglucose_objs.latest('datetime')
                    bloodglucosedata=model_to_dict(bloodglucose_obj,fields=('glucose_level','testtype'))
                    bloodglucosedata['datetime']=bloodglucose_obj.datetime
            except KeyError:
                pass

        if machine == ServerConstants.OXIMETERMACHINE:
            try:
                oximeter_objs=oximeter.objects.filter(person=person)
                if len(oximeter_objs)>0:
                    oximeter_obj=oximeter_objs.latest('datetime')
                    oximeterdata=model_to_dict(oximeter_obj,fields=('oxygensat','pulse'))
                    oximeterdata['datetime']=oximeter_obj.datetime
            except KeyError:
                pass

        if machine == ServerConstants.TEMPERATUREMACHINE:
            try:
                temperature_objs=temperature.objects.filter(person=person)
                if len(temperature_objs)>0:
                    temperature_obj=temperature_objs.latest('datetime')
                    temperaturedata=model_to_dict(temperature_obj,fields=('temperature'))
                    temperaturedata['datetime']=temperature_obj.datetime
            except KeyError:
                pass

        if machine == ServerConstants.HEMOGLOBINMACHINE:
            try:
                hemoglobin_objs=hemoglobin.objects.filter(person=person)
                if len(hemoglobin_objs)>0:
                    hemoglobin_obj=hemoglobin_objs.latest('datetime')
                    hemoglobindata=model_to_dict(hemoglobin_obj,fields=('hb','hct'))
                    hemoglobindata['datetime']=hemoglobin_obj.datetime
            except KeyError:
                pass

        if machine == ServerConstants.LIPIDMACHINE:
            try:
                lipid_objs=lipid.objects.filter(person=person)
                if len(lipid_objs)>0:
                    lipid_obj=lipid_objs.latest('datetime')
                    lipiddata=model_to_dict(lipid_obj,fields=('tc','tg','hdl','ldl','hdlratio','nonhdl'))
                    lipiddata['datetime']=lipid_obj.datetime
            except KeyError:
                pass



    user_info = model_to_dict(person, fields=('qbusername', 'gender', 'profilepic', 'personuid'))
    user_info['age'] = person.get_age()
    jsondata = dict({
                        ServerConstants.BPMACHINE: bpdata,
                        ServerConstants.BLOODGLUCOSEMACHINE: bloodglucosedata,
                        ServerConstants.WEIGHTMACHINE: weightdata,
                        ServerConstants.HEIGHTMACHINE: heightdata,
                        ServerConstants.OXIMETERMACHINE: oximeterdata,
                        ServerConstants.TEMPERATUREMACHINE: temperaturedata,
                        ServerConstants.HEMOGLOBINMACHINE: hemoglobindata,
                        ServerConstants.LIPIDMACHINE: lipiddata,
                        ServerConstants.PersonDetails: user_info,
                        })

    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getLatestPersonReport', logrequest, httpoutput, logmessage)
    # requestlogger.info('getAppReportResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)

@csrf_exempt
def download_app_report(request):
    # requestlogger.info('================sendReportSms=====================')
    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('download_kiosk_report', logrequest, 'Error authenticating person', logmessage)
        return HttpResponse("Error authenticating person", status=401)

    res = getVitalReport(request)
    data = json.loads(res.content)
    requestlogger.info('download_kiosk_report;%s' % (str(data)))
    try:
        reportid_req = request.GET[ServerConstants.REPORTID]
        # requestlogger.info('sendReportSms:reportis;%s'%(str(reportid_req)))
        logmessage.append('sendReportEmail:reportis;%s'%(str(reportid_req)))
    except KeyError:
        backendlogger.error('sendReportEmail', logrequest, 'reportid not sent in request', logmessage)
        # requestlogger.info('getAppReport:reportid/personuid not sent in request')
        return HttpResponse("reportid not sent in request",status=403)


    try:
        partner = data[ServerConstants.PARTNER]
    except:
        partner = ""


    subject_name = "Your Health Report"
    css_file = "emailtemplate.css"
    report_template = "vitalsreport.html"
    print("report_template: ", report_template)
    # requestlogger.info('reportdata:%s;to:%s'%(reportdata,recipient))
    css = os.path.join(settings.CSS_DIR, css_file)
    reportpath = os.path.join(settings.STATIC_ROOT, settings.REPORTS_DIR,
                              reportid_req + '_report.pdf')
    reporttemplate = loader.get_template(report_template)
    reportdata = data
    reportdata = apply_dynamic_ranges(reportdata)
    # requestlogger.info(reportdata)

    # dummyreportdata = dict(Communications.utils.BLANKREPORTDATA)
    # dummyreportdata.update(reportdata)
    # reportdata = dummyreportdata
    reportdata["reporticons"] = settings.REPORT_ICONS + "/"
    reportdata["reporticonspartner"] =  settings.REPORT_ICONS + "/partner/"
    reportdata["partner"] = partner

    c = Context(reportdata)
    filledreport = reporttemplate.render(c.flatten())
    #logmessage.append("filledreport:==="+filledreport)
    requestlogger.info("filledreport:==="+filledreport)

    options = {
        'page-size': 'A4',
        'encoding': "UTF-8",
        'margin-top': '0in',
        'margin-right': '0in',
        'margin-bottom': '0in',
        'margin-left': '0in',
        'no-outline': None
    }
    requestlogger.info("Template:" + report_template)
    pdfkit.from_string(filledreport, reportpath, css=css, options=options)
    with open(reportpath, 'rb') as pdf:
        response = HttpResponse(pdf.read(), content_type='application/pdf')
        response['Content-Disposition'] = 'inline;filename=healthreport.pdf'
    pdf.close()
    # os.remove(reportpath)  # remove the locally created pdf file.
    return response  # returns the response.


@csrf_exempt
def download_prescription(request):
    res = getPrescription(request)
    data = json.loads(res.content)
    requestlogger.info('sendAppReport;%s'%(str(data)))
    report_data = data

    try:
        partner = data[ServerConstants.PARTNER]
    except:
        partner = ""

    try:
        prescriptionid= data[ServerConstants.PRESCRIPTIONID]
    except:
        prescriptionid = None

    subject_name = "Your Health Report"
    css_file = "prescription.css"
    report_template = "prescription.html"
    subject_name = "Your Prescription Report"

    # requestlogger.info('reportdata:%s;to:%s'%(reportdata,recipient))
    css = os.path.join(settings.CSS_DIR, css_file)
    filename = prescriptionid+".pdf"
    reportpath = os.path.join(settings.STATIC_ROOT,settings.REPORTS_DIR, filename)

    reporttemplate = loader.get_template(report_template)
    reportdata = report_data
    reportdata["reporticons"] = settings.REPORT_ICONS + "/"
    reportdata["reporticonspartner"] =  settings.REPORT_ICONS + "/partner/"
    reportdata["partner"]=partner
    #this we need to do as json.loads doesnt accept single apostophe(') in json e.g. {'medicine': 'crocin'} is not accepted
    reportdata["prescription"] = json.loads(reportdata['prescription'])
    reportdata["investigation"] = json.loads(reportdata['investigation'])
    reportdata["datetime"] = datetime.strptime(reportdata["datetime"],'%Y-%m-%dT%H:%M:%S.%fZ').strftime("%d %B, %Y")
    if reportdata["followupdatetime"] != None:
        try:
            reportdata["followupdatefmt"] = datetime.strptime(reportdata["followupdatetime"],'%Y-%m-%dT%H:%M:%S.%fZ').strftime("%d %b, %Y")
        except ValueError:
            reportdata["followupdatefmt"] = datetime.strptime(reportdata["followupdatetime"],'%Y-%m-%dT%H:%M:%SZ').strftime("%d %b, %Y")

    c = Context(reportdata)
    filledreport = reporttemplate.render(c.flatten())
    #logmessage.append("filledreport:==="+filledreport)
    requestlogger.info("filledreport:==="+filledreport)

    # footertemplate = loader.get_template("report_footer.html")
    # d = Context(reportdata)
    # footerhtml = footertemplate.render(d.flatten())
    #
    # footerpath = os.path.join(settings.STATIC_ROOT,settings.REPORTS_DIR,'prescription_footer.html')
    # footerfile = open(footerpath,"w")
    # footerfile.write(footerhtml)
    # footerfile.close()

    options = {
        'page-size': 'A4',
        'encoding': "UTF-8",
    }

    pdfkit.from_string(filledreport, reportpath, css=css, options=options)
    with open(reportpath, 'rb') as pdf:
        response = HttpResponse(pdf.read(), content_type='application/pdf')
        response['Content-Disposition'] = 'inline;filename=prescription.pdf'
    pdf.close()
    # os.remove(reportpath)  # remove the locally created pdf file.
    return response  # returns the response.


@csrf_exempt
def sendVitalReport(request):
    # requestlogger.info("================sendAppReport============")
    requestlogger.info(request.body)
    logmessage = []
    data = json.loads(str(request.body.decode("utf-8")))
    requestlogger.info('sendAppReport;%s' % (str(data)))
    report_data = data[ServerConstants.REPORTDATA]
    recipient = data[ServerConstants.RECIPIENT]
    try:
        partner = data[ServerConstants.PARTNER]
    except:
        partner = ""

    try:
        reporttype = data[ServerConstants.REPORTTYPE]
    except:
        reporttype = None

    subject_name = "Your Health Report"

    if reporttype == ReportTypeConstants.BASICREPORT:
        css_file = "emailtemplate.css"
        report_template = "vitalsreport.html"
    elif reporttype == ReportTypeConstants.PRESCRIPTION:
        css_file = "prescription.css"
        report_template = "prescription.html"
        subject_name = "Your Prescription"

    print("report_template: ", report_template)
    # requestlogger.info('reportdata:%s;to:%s'%(reportdata,recipient))
    css = os.path.join(settings.CSS_DIR, css_file)
    reportpath = os.path.join(settings.STATIC_ROOT, settings.REPORTS_DIR,
                              recipient + '_report.pdf')
    reporttemplate = loader.get_template(report_template)
    reportdata = json.loads(report_data)
    if reporttype in [1, 3]:  # Basic health checkup OR Heart test
        reportdata = apply_dynamic_ranges(reportdata)
    # requestlogger.info(reportdata)

    reportdata["reporticons"] = settings.REPORT_ICONS + "/"
    reportdata["reporticonspartner"] =  settings.REPORT_ICONS + "/partner/"
    reportdata["partner"] = partner

    try:
        reportdata["subpartner"] = data[ServerConstants.SUBPARTNER]
    except:
        pass

    try:
        height_in_feets = reportdata["height"].replace("'", ".").replace('"', '')
        height_in_cms = float(height_in_feets)*30.48
        bmr_inference, ideal_bmr = get_bmr_inference(float(reportdata["kcal"]), reportdata["weight"], height_in_cms,
                                                     reportdata["age"], reportdata["gender"])
        reportdata["standard_kcal"] = ideal_bmr
    except Exception as e:
        print("Exception while calculating BMR: ", str(e))
        pass

    c = Context(reportdata)
    filledreport = reporttemplate.render(c.flatten())

    footertemplate = loader.get_template("report_footer.html")
    d = Context(reportdata)
    footerhtml = footertemplate.render(d.flatten())

    footerpath = os.path.join(settings.STATIC_ROOT, settings.REPORTS_DIR,
                              recipient + '_footer.html')
    footerfile = open(footerpath, "w")
    footerfile.write(footerhtml)
    footerfile.close()

    options = {
        'page-size': 'A4',
        'encoding': "UTF-8",
        'margin-top': '0in',
        'margin-right': '0in',
        'margin-bottom': '0in',
        'margin-left': '0in',
        'no-outline': None
    }

    pdfkit.from_string(filledreport, reportpath, css=css, options=options)
    email = EmailMessage()
    email.subject = subject_name
    email.body =  "Dear "+reportdata['name']+"\n\nGreetings from "+partner+".\n\nKindly find attached "+subject_name+".\n\nWe wish you a healthy life ahead!\n\nWarm Regards,\nTeam "+partner

    email.from_email = partner + "<" + config.EMAIL_FROM + ">"
    email.to = [recipient]
    email.attach_file(reportpath)
    footerfile.close()
    try:
        response = email.send()
        if response:
            backendlogger.info('sendAppReport', data, 'report sent', logmessage)
            return HttpResponse('report sent', status=200)
        else:
            backendlogger.error('sendAppReport', data, 'error sending report to' + recipient, logmessage)
            # requestlogger.info('error sending report to'+recipient)
            return HttpResponse('error sending report', status=400)
    except:
        backendlogger.error('sendAppReport', data, 'error sending report to' + recipient, logmessage)
        # requestlogger.info('error sending report to'+recipient)
        return HttpResponse('error sending report', status=400)
    
@csrf_exempt
def getHealthHistoryForPerson(request):
    # requestlogger.info('================getHealthHistoryForPerson=====================')
    logmessage = []
    logrequest = {}
    for key, value in request.GET.items():
        logrequest[key] = value
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getHealthHistoryForPerson', logrequest, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    personuid_req = None
    qbid_req = None
    try:
        personuid_req = request.GET[ServerConstants.PERSONUID]
        logmessage.append('getKioskReport:userid;%s'%(str(personuid_req)))
        # requestlogger.info('getKioskReport:userid;%s'%(str(userid_req)))
    except KeyError:
        try:
            qbid_req = request.GET[ServerConstants.QBID]
            logmessage.append('getKioskReport:qbid;%s' % (str(qbid_req)))
        except:
            backendlogger.error('getHealthHistoryForPerson', logrequest,
                             'getHealthHistoryForPerson:userid/qbid not sent in request', logmessage)
            return HttpResponse("userid/qbid not sent in request",status=403)

    report_type = None

    try:
         report_type = request.GET[ServerConstants.REPORTTYPE]
         logmessage.append('getKioskReport:reporttype;%s'%(str(report_type)))
    except KeyError:
       pass

    combinedlist = list()

    if(personuid_req):
        try:
            person = PersonDetails.objects.get(personuid=personuid_req)
        except:
            backendlogger.error('getHealthHistoryForPerson', logrequest,
                             'getHealthHistoryForPerson:user not found for given userid', logmessage)
            return HttpResponse("user not found for given userid",status=403)
    elif(qbid_req):
        try:
            person = PersonDetails.objects.get(qbid=qbid_req)
        except:
            backendlogger.error('getHealthHistoryForPerson', logrequest,
                             'getHealthHistoryForPerson:user not found for given qbid', logmessage)
            return HttpResponse("user not found for given qbid",status=403)

    if(report_type == None or report_type == "vitalreport"):

        allreports = report.objects.filter(person=person).exclude(reporttype = 8)
        reportList = list()
        for thisreport in allreports:
            applocation = ""
            if(thisreport.appdetails):
                applocation = thisreport.appdetails.applocation
            reportList.append({ ServerConstants.HISTORYTYPE:"vitalreport",
                                ServerConstants.REPORTID:thisreport.reportid,
                                # ServerConstants.RECOMMENDATIONS:thisreport.recommendations.split(";") if thisreport.recommendations else None,
                                ServerConstants.REPORTTYPE:thisreport.reporttype,
                                ServerConstants.APPLOCATION:applocation,
                                ServerConstants.DATETIME:time.mktime(timezone.localtime(thisreport.datetime).timetuple()),
                               })

        combinedlist = reportList

    if(report_type == None or report_type == "prescriptionreport"):
        allprescriptions = prescription.objects.filter(person=person)
        prescriptionList = list()
        for prescriptiondata in allprescriptions:
            prescriptionList.append({
                                ServerConstants.HISTORYTYPE:"prescription",
                                ServerConstants.PRESCRIPTIONID:prescriptiondata.prescriptionid,
                                ServerConstants.DOCTORNAME:prescriptiondata.doctor.name,
                                ServerConstants.APPLOCATION:prescriptiondata.appdetails.applocation,
                                ServerConstants.DATETIME:time.mktime(timezone.localtime(prescriptiondata.datetime).timetuple()),
                               })

        combinedlist = combinedlist+prescriptionList

    if(report_type == None or report_type == "imagereport"):
        allimagereports = imagereportdetails.objects.filter(person=person)
        allimagereportsList = list()
        for imagereportdata in allimagereports:
            allimagereportsList.append({
                                ServerConstants.HISTORYTYPE:"imagereport",
                                ServerConstants.IMAGEREPORTID:imagereportdata.imagereportid,
                                ServerConstants.IMAGEREPORTTITLE:imagereportdata.reporttitle,
                                ServerConstants.IMAGEREPORTIMGURL:imagereportdata.reportimgurl,
                                ServerConstants.REPORTTYPE:imagereportdata.reporttype,
                                ServerConstants.DATETIME:time.mktime(timezone.localtime(imagereportdata.datetime).timetuple()),
                               })

        combinedlist = combinedlist+allimagereportsList

    combinedlist = sorted(combinedlist, key=itemgetter(ServerConstants.DATETIME),reverse=True)

    jsondata = dict({ServerConstants.HEALTHHISTORY:combinedlist})
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getHealthHistoryForPerson', logrequest, httpoutput, logmessage)
    # requestlogger.info('getHealthHistoryForPersonResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)

@csrf_exempt
def sendReportSms(request):
    # requestlogger.info('================sendReportSms=====================')
    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('sendReportSms', logrequest, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        reportid_req = request.GET[ServerConstants.REPORTID]
        # requestlogger.info('sendReportSms:reportis;%s'%(str(reportid_req)))
        logmessage.append('sendReportSms:reportis;%s' % (str(reportid_req)))
    except KeyError:
        backendlogger.error('sendReportSms', logrequest, 'reportid not sent in request', logmessage)
        # requestlogger.info('getKioskReport:reportid/personuid not sent in request')
        return HttpResponse("reportid not sent in request", status=403)

    try:
        personuid_req = request.GET[ServerConstants.PERSONUID]
        # requestlogger.info('sendReportSms:personuid;%s'%(str(personuid_req)))
        logmessage.append('sendReportSms:personuid;%s' % (str(personuid_req)))
    except KeyError:
        backendlogger.error('sendReportSms', logrequest, 'sendReportSms:personuid not found:', logmessage)
        # requestlogger.info('sendReportSms:personuid not found:'+personuid_req)
        return HttpResponse("sendReportSms:personuid not found:", status=403)

    try:
        person = PersonDetails.objects.get(personuid=personuid_req)
    except ObjectDoesNotExist:
        backendlogger.error('sendReportSms', logrequest, 'person doesnt exist', logmessage)
        return HttpResponse("person doesnt exist", status=404)

    try:
        thisreport = report.objects.get(reportid=reportid_req)
    except ObjectDoesNotExist:
        backendlogger.error('sendReportSms', logrequest, 'sendReportSms:reportid not found:' + reportid_req,
                         logmessage)
        # requestlogger.info('sendReportSms:reportid not found:'+reportid_req)
        return HttpResponse("report not found", status=403)

    try:
        teststaken_req = request.GET[ServerConstants.TESTSTAKEN]
    except:
        teststaken_req = ""

    machinesrun = teststaken_req.split(',')

    if any("bpmachine" in machine for machine in machinesrun) or teststaken_req == "" and thisreport.bp != None:
        bpdata = model_to_dict(thisreport.bp, fields=('systolic', 'diastolic', 'pulse'))
    else:
        bpdata = None

    if any("bloodglucosemachine" in machine for machine in
           machinesrun) or teststaken_req == "" and thisreport.bloodglucose != None:
        bloodglucosedata = model_to_dict(thisreport.bloodglucose, fields=('glucose_level'))
    else:
        bloodglucosedata = None

    if any("weightmachine" in machine for machine in
           machinesrun) or teststaken_req == "" and thisreport.weight != None:
        weightdata = model_to_dict(thisreport.weight, fields=(
        'weight', 'height', 'bmi', 'fat', 'bonemass', 'muscle', 'hydration', 'kcal', 'vfat','sfat', 'bage', 'prot'))
    else:
        weightdata = None

    if any("heightmachine" in machine for machine in
           machinesrun) or teststaken_req == "" and thisreport.height != None:
        heightdata = model_to_dict(thisreport.height, fields=('height'))
    else:
        heightdata = None

    if any("oximetermachine" in machine for machine in
           machinesrun) or teststaken_req == "" and thisreport.oximeter != None:
        oximeterdata = model_to_dict(thisreport.oximeter, fields=('pulse', 'oxygensat'))
    else:
        oximeterdata = None

    if any("temperaturemachine" in machine for machine in
           machinesrun) or teststaken_req == "" and thisreport.temperature != None:
        temperaturedata = model_to_dict(thisreport.temperature, fields=('temperature'))
    else:
        temperaturedata = None

    if any("ecgmachine" in machine for machine in machinesrun) or teststaken_req == "" and thisreport.ecg != None:
        ecgdata = model_to_dict(thisreport.ecg, fields=(
        'lead1', 'lead2', 'lead3', 'avr', 'avl', 'avf', 'v1', 'v2', 'v3', 'v4', 'v5', 'v6'))
    else:
        ecgdata = None

    if any("hemoglobinmachine" in machine for machine in
           machinesrun) or teststaken_req == "" and thisreport.hemoglobin != None:
        hemoglobindata = model_to_dict(thisreport.hemoglobin, fields=('hb', 'hct'))
    else:
        hemoglobindata = None

    if any("lipidmachine" in machine for machine in
           machinesrun) or teststaken_req == "" and thisreport.lipid != None:
        lipiddata = model_to_dict(thisreport.lipid, fields=('tc', 'tg', 'hdl', 'ldl', 'hdlratio', 'nonhdl'))
    else:
        lipiddata = None

    response = sendMessageReport(person, bpdata, weightdata, oximeterdata, heightdata, temperaturedata,
                                 bloodglucosedata, hemoglobindata, lipiddata, None, None, None,
                                 None, None, None, None)

    # httpoutput = utils.successJson(jsondata)
    if (response):
        b = 'sendReportSmsResponse;%s', (str(response.status_code))
        logmessage.append(b)
        # requestlogger.info('sendReportSmsResponse;%s',(str(response.status_code)))
    else:
        logmessage.append('sendReportSmsResponse; send sms is off')
        # requestlogger.info('sendReportSmsResponse; send sms is off');
    backendlogger.info('sendReportSms', logrequest, 'success', logmessage)
    httpoutput = utils.successJson({})
    # requestlogger.info('getHealthHistoryForPersonResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

def sendMessageReport(user, bp_req, weight_req, oximeter_req, height_req, temperature_req, bloodglucose_req, hemoglobin_req, lipid_req, hiv_req, dengue_req, pregnancy_req, malaria_req, typhoid_req, urine_req, eye_req):

    sender_Num = user.phone
    requestlogger.info("try to send sms to:"+user.phone)
    report_to_send= ''
    logmessage= []

    if(bp_req):
        bp_reading= ''.join(['*BP (',str(int(bp_req[ServerConstants.SYSTOLIC])),',',str(int(bp_req[ServerConstants.DIASTOLIC])),') mmHg\n\n'])
        report_to_send= ''.join([report_to_send,bp_reading])

    if(weight_req): #if weight is not done then we save only weight from previous weight of user and we save height too
        getheightfeet = utils.cmstofeetinch(weight_req[ServerConstants.HEIGHT])
        weight_reading= ''.join(['*Body Stats\nHeight: ',str(weight_req[ServerConstants.HEIGHT]),' cm(',str(getheightfeet[0]),'\'',str(getheightfeet[1]),'\")','\nWeight: ',str(round(weight_req[ServerConstants.WEIGHT],1)),' kg'])
        if weight_req[ServerConstants.BMI]>0:
            weight_reading= ''.join([weight_reading,'\nBMI: ',str(round(weight_req[ServerConstants.BMI],1))])
        if weight_req[ServerConstants.FAT]>0:
            # weight_reading= ''.join([weight_reading,',\nFat: ',str(weight_req[ServerConstants.FAT]), '%,\nMuscle: ',str(weight_req[ServerConstants.MUSCLE]),' %,\nHydration: ',str(weight_req[ServerConstants.HYDRATION]),' %,\nbonemass: ',str(weight_req[ServerConstants.BONEMASS])])
            weight_reading= ''.join([weight_reading,',\nFat: ',str(round(weight_req[ServerConstants.FAT],1)), '%,\nVFat: ',str(round(weight_req[ServerConstants.VFAT],1)),'%,\nMuscle: ',str(round(weight_req[ServerConstants.MUSCLE],1)),' %,\nBonemass: ',str(round(weight_req[ServerConstants.BONEMASS],1)),'%'])
        if weight_req[ServerConstants.HYDRATION]>0:
            weight_reading= ''.join([weight_reading,'\nHydration: ',str(round(weight_req[ServerConstants.HYDRATION],1)),'%'])
        weight_reading= ''.join([weight_reading,'\n\n'])
        report_to_send= ''.join([report_to_send,weight_reading])

    if(oximeter_req):
        oximeter_reading= ''.join(['*Oximeter\nSpO2: ',str(int(oximeter_req[ServerConstants.OXYGENSAT])), ' % ,\nPulse Rate: ',str(int(oximeter_req[ServerConstants.PULSE])),' bpm \n\n'])
        report_to_send= ''.join([report_to_send,oximeter_reading])

    if(height_req and not weight_req): #dont send the height as its sent in weight. If only height done then send height
        getheightfeet = utils.cmstofeetinch(height_req[ServerConstants.HEIGHT])
        height_reading= ''.join(['*Height: ',str(height_req[ServerConstants.HEIGHT]),' cm(',str(getheightfeet[0]),'\'',str(getheightfeet[1]),'\")','\n\n'])
        report_to_send= ''.join([report_to_send,height_reading])

    if(temperature_req):
        temperature_reading_fahrenheit = temperature_req[ServerConstants.TEMPERATURE]
        temperature_reading_celcius = round((temperature_reading_fahrenheit-32)/1.8,1)
        temperature_reading= ''.join(['*Temp: ',str(round(temperature_reading_fahrenheit,1)),' F,',str(round(temperature_reading_celcius,1)),' C','\n\n'])
        report_to_send= ''.join([report_to_send,temperature_reading])

    if(bloodglucose_req):
        bloodglucose_reading= ''.join(['*Glucose: ',str(bloodglucose_req[ServerConstants.BLOODGLUCOSE]),' mg/dL \n\n'])
        report_to_send= ''.join([report_to_send,bloodglucose_reading])

    if(hemoglobin_req):
        hemoglobin_reading= ''.join(['* Hb: ',str(hemoglobin_req[ServerConstants.HB]),' g/dL','\n\n'])
        report_to_send= ''.join([report_to_send,hemoglobin_reading])

    if(lipid_req):
        lipid_reading= ''.join(['*Lipid Profile\nTC: ',str(lipid_req[ServerConstants.TC]),'mg/dL',',\nTG: ',str(lipid_req[ServerConstants.TG])
                                ,',\nHDL: ',str(lipid_req[ServerConstants.HDL]),',\nLDL: ',str(lipid_req[ServerConstants.LDL]),'\n\n'])
        report_to_send= ''.join([report_to_send,lipid_reading])

    if(eye_req):
        eye_reading = ''.join(['*Eye Checkup\nFar Vision Score: ',str(eye_req[ServerConstants.FARVISION]),',\nNear Vision Score: ',str(eye_req[ServerConstants.NEARVISION]),'\n',
                                  'Near Vision Jscore: ',str(eye_req[ServerConstants.NEARVISIONJSCORE]),',\nEye Score: ',str(eye_req[ServerConstants.EYESCORE]),'\n\n'])
        report_to_send= ''.join([report_to_send,eye_reading])

    if(urine_req):
        urine_reading= ''.join(['*Urine test\nGlucose: ',str(urine_req[ServerConstants.GLUCOSE]),
                                ',\nBilirubin: ',str(urine_req[ServerConstants.BILIRUBIN]),
                                ',\nKetone: ',str(urine_req[ServerConstants.KETONE]),
                                ',\nSpecific Gravity: ',str(urine_req[ServerConstants.GRAVITY]),
                                ',\nBlood: ',str(urine_req[ServerConstants.BLOOD]),
                                ',\npH: ',str(urine_req[ServerConstants.PH]),
                                ',\nProtein: ',str(urine_req[ServerConstants.PROTEIN]),
                                ',\nUrobilinogen: ',str(urine_req[ServerConstants.UROBILINOGEN]),
                                ',\nNitrite: ',str(urine_req[ServerConstants.NITRITE]),
                                ',\nLeukocytes: ',str(urine_req[ServerConstants.LEUKOCYTES]),'\n\n'])
        report_to_send= ''.join([report_to_send,urine_reading])

    report_to_send= ''.join([report_to_send,''])

    #report_to_send= ''.join([bp_reading,weight_reading,oximeter_reading,height_reading,temperature_reading])

    requestlogger.info( report_to_send+" "+sender_Num)
    response = Communications.utils.sendSMS(report_to_send,sender_Num, None, None)
    if(response):
        if(response.status_code == 200):
            logmessage.append('Report %s send to mobile %s'%(report_to_send,sender_Num))
            # requestlogger.info('Report %s send to mobile %s'%(report_to_send,sender_Num))
        else:
            logmessage.append('error occurred in sending report:%d'%(response.status_code))
            # requestlogger.info('error occurred in sending report:%d'%(response.status_code))
    else:
        logmessage.append('sms report is off')
        # requestlogger.info('sms report is off')

    backendlogger.info('sendMessageReport',sender_Num, response, logmessage)

    return response

@csrf_exempt
def saveRapidTestReport(request):
    logmessage = []
    data = json.loads(request.body)

    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('saveRapidTestReport', data, 'error authenticating person', logmessage)
        return HttpResponse("Error authenticating person", status=401)

    logmessage.append('saveRapidTestReport;%s'%(str(data)))
    # requestlogger.info('saveRapidTestReport;%s'%(str(data)))
    person=None

    try:
        personuid_req = data[ServerConstants.PERSONUID]
    except KeyError:
        backendlogger.error('saveRapidTestReport', data, 'person id not found', logmessage)
        return HttpResponse("personuid not found",status=403)

    try:
        healthcaseid_req = data[ServerConstants.HEALTHCASEID]
    except KeyError:
        backendlogger.error('saveRapidTestReport', data, 'healthcaseid not found', logmessage)
        return HttpResponse("doctoruid not found",status=403)

    try:
        person = PersonDetails.objects.get(personuid=personuid_req)
    except ObjectDoesNotExist:
        backendlogger.error('saveRapidTestReport', data, 'person doesnt exist', logmessage)
        return HttpResponse("person doesnt exist",status=404)

    try:
        healthcase = HealthCase.objects.get(healthcaseid=healthcaseid_req)
    except ObjectDoesNotExist:
        backendlogger.error('saveRapidTestReport', data, 'healthcase doesnt exist', logmessage)
        return HttpResponse("doctor doesnt exist",status=404)

    try:
        reportdata_req = data[ServerConstants.REPORTDATA]
    except KeyError:
        backendlogger.error('saveRapidTestReport', data, 'kioskid not sent', logmessage)
        return HttpResponse("appid not sent", status=403)

    try:
        appuid_req = data[ServerConstants.APPUID]
    except KeyError:
        backendlogger.error('saveRapidTestReport', data, 'kioskid not sent', logmessage)
        return HttpResponse("appid not sent",status=403)

    try:
        appdetails = AppDetails.objects.get(appuid=appuid_req)
    except ObjectDoesNotExist:
        backendlogger.error('saveRapidTestReport', data, 'appdetails doesnt exist', logmessage)
        return HttpResponse("appdetails doesnt exist",status=404)

    try:
        rapidtestobj = rapidtest.objects.create(reportdata = json.dumps(reportdata_req) ,person = person, appdetails = appdetails, healthcase = healthcase)
        rapidtestobj.reportid = encode_id(rapidtestobj.id)
        rapidtestobj.save()
    except Exception as e:
        backendlogger.error('saveRapidTestReport', data, str(e), logmessage)
        return HttpResponse("Error saving prescription", status=404)

    jsondata = dict({ServerConstants.REPORTID: rapidtestobj.reportid})

    httpoutput = utils.successJson(jsondata)
    backendlogger.info('saveRapidTestReport', data, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

@csrf_exempt
def saveUrineTestReport(request):
    logmessage = []
    data = json.loads(request.body)

    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('saveUrineTestReport', data, 'error authenticating person', logmessage)
        return HttpResponse("Error authenticating person", status=401)

    logmessage.append('saveUrineTestReport;%s'%(str(data)))
    # requestlogger.info('saveRapidTestReport;%s'%(str(data)))
    person=None

    try:
        personuid_req = data[ServerConstants.PERSONUID]
    except KeyError:
        backendlogger.error('saveUrineTestReport', data, 'person id not found', logmessage)
        return HttpResponse("personuid not found",status=403)

    try:
        healthcaseid_req = data[ServerConstants.HEALTHCASEID]
    except KeyError:
        backendlogger.error('saveUrineTestReport', data, 'healthcaseid not found', logmessage)
        return HttpResponse("doctoruid not found",status=403)

    try:
        person = PersonDetails.objects.get(personuid=personuid_req)
    except ObjectDoesNotExist:
        backendlogger.error('saveUrineTestReport', data, 'person doesnt exist', logmessage)
        return HttpResponse("person doesnt exist",status=404)

    try:
        healthcase = HealthCase.objects.get(healthcaseid=healthcaseid_req)
    except ObjectDoesNotExist:
        backendlogger.error('saveUrineTestReport', data, 'healthcase doesnt exist', logmessage)
        return HttpResponse("doctor doesnt exist",status=404)

    try:
        reportdata_req = data[ServerConstants.REPORTDATA]
    except KeyError:
        backendlogger.error('saveUrineTestReport', data, 'kioskid not sent', logmessage)
        return HttpResponse("appid not sent", status=403)

    try:
        appuid_req = data[ServerConstants.APPUID]
    except KeyError:
        backendlogger.error('saveUrineTestReport', data, 'kioskid not sent', logmessage)
        return HttpResponse("appid not sent",status=403)

    try:
        appdetails = AppDetails.objects.get(appuid=appuid_req)
    except ObjectDoesNotExist:
        backendlogger.error('saveUrineTestReport', data, 'appdetails doesnt exist', logmessage)
        return HttpResponse("appdetails doesnt exist",status=404)

    try:
        urinetestobj = urinetest.objects.create(reportdata = json.dumps(reportdata_req) ,person = person, appdetails = appdetails, healthcase = healthcase)
        urinetestobj.reportid = encode_id(urinetestobj.id)
        urinetestobj.save()
    except Exception as e:
        backendlogger.error('saveUrineTestReport', data, str(e), logmessage)
        return HttpResponse("Error saving prescription", status=404)

    jsondata = dict({ServerConstants.REPORTID: urinetestobj.reportid})

    httpoutput = utils.successJson(jsondata)
    backendlogger.info('saveUrineTestReport', data, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

@csrf_exempt
def saveEyeTestReport(request):
    logmessage = []
    data = json.loads(request.body)

    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('saveEyeTestReport', data, 'error authenticating person', logmessage)
        return HttpResponse("Error authenticating person", status=401)

    logmessage.append('saveEyeTestReport;%s'%(str(data)))
    # requestlogger.info('saveEyeTestReport;%s'%(str(data)))
    person=None

    try:
        personuid_req = data[ServerConstants.PERSONUID]
    except KeyError:
        backendlogger.error('saveEyeTestReport', data, 'person id not found', logmessage)
        return HttpResponse("personuid not found",status=403)

    try:
        healthcaseid_req = data[ServerConstants.HEALTHCASEID]
    except KeyError:
        backendlogger.error('saveEyeTestReport', data, 'healthcaseid not found', logmessage)
        return HttpResponse("doctoruid not found",status=403)

    try:
        person = PersonDetails.objects.get(personuid=personuid_req)
    except ObjectDoesNotExist:
        backendlogger.error('saveEyeTestReport', data, 'person doesnt exist', logmessage)
        return HttpResponse("person doesnt exist",status=404)

    try:
        healthcase = HealthCase.objects.get(healthcaseid=healthcaseid_req)
    except ObjectDoesNotExist:
        backendlogger.error('saveEyeTestReport', data, 'healthcase doesnt exist', logmessage)
        return HttpResponse("doctor doesnt exist",status=404)

    try:
        reportdata_req = data[ServerConstants.REPORTDATA]
    except KeyError:
        backendlogger.error('saveEyeTestReport', data, 'kioskid not sent', logmessage)
        return HttpResponse("appid not sent", status=403)

    try:
        appuid_req = data[ServerConstants.APPUID]
    except KeyError:
        backendlogger.error('saveEyeTestReport', data, 'kioskid not sent', logmessage)
        return HttpResponse("appid not sent",status=403)

    try:
        appdetails = AppDetails.objects.get(appuid=appuid_req)
    except ObjectDoesNotExist:
        backendlogger.error('saveEyeTestReport', data, 'appdetails doesnt exist', logmessage)
        return HttpResponse("appdetails doesnt exist",status=404)

    try:
        eyetestobj = eyetest.objects.create(reportdata = json.dumps(reportdata_req) ,person = person, appdetails = appdetails, healthcase = healthcase)
        eyetestobj.reportid = encode_id(eyetestobj.id)
        eyetestobj.save()
    except Exception as e:
        backendlogger.error('saveEyeTestReport', data, str(e), logmessage)
        return HttpResponse("Error saving prescription", status=404)

    jsondata = dict({ServerConstants.REPORTID: eyetestobj.reportid})

    httpoutput = utils.successJson(jsondata)
    backendlogger.info('saveEyeTestReport', data, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

@csrf_exempt
def getRapidTestReport(request):
    logmessage = []
    data = json.loads(request.body)

    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getRapidTestReport', data, 'error authenticating person', logmessage)
        return HttpResponse("Error authenticating person", status=401)

    logmessage.append('getRapidTestReport;%s'%(str(data)))
    # requestlogger.info('getRapidTestReport;%s'%(str(data)))
    person=None

    try:
        reportid_req = data[ServerConstants.REPORTID]
        thisreport = rapidtest.objects.get(reportid = reportid_req)
    except KeyError:
        backendlogger.error('getRapidTestReport', data, 'report id not found', logmessage)
        return HttpResponse("personuid not found",status=403)

    jsondata = dict({
        ServerConstants.HISTORYTYPE: "rapidtestreport",
        ServerConstants.REPORTID: thisreport.reportid,
        ServerConstants.DATETIME: thisreport.datetime,
        ServerConstants.USERNAME: thisreport.person.name,
        ServerConstants.PERSONUID: thisreport.person.personuid,
        ServerConstants.AGE: timezone.now().year - thisreport.person.dob_year,
        ServerConstants.GENDER: thisreport.person.gender,
        ServerConstants.HEIGHT: thisreport.person.userheight,
        ServerConstants.WEIGHT: thisreport.person.userweight,
        ServerConstants.APPLOCATION: thisreport.appdetails.applocation,
        ServerConstants.APPUID: thisreport.appdetails.appuid,
        ServerConstants.PARTNER: thisreport.appdetails.partner,
        ServerConstants.REPORTDATA: thisreport.reportdata,
    })

    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getLatestPersonReport', data, httpoutput, logmessage)
    # requestlogger.info('getAppReportResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

@csrf_exempt
def setVitalsForTracking(request):

    logmessage = []
    data = json.loads(request.body)

    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('setVitalForTracking', data, 'error authenticating person', logmessage)
        return HttpResponse("Error authenticating person", status=401)

    logmessage.append('getRapidTestReport;%s' % (str(data)))

    try:
        personuid_req = data[ServerConstants.PERSONUID]
    except KeyError:
        backendlogger.error('setVitalForTracking', data, 'person id not found', logmessage)
        return HttpResponse("personuid not found",status=403)

    try:
        person = PersonDetails.objects.get(personuid=personuid_req)
    except ObjectDoesNotExist:
        backendlogger.error('setVitalForTracking', data, 'person doesnt exist', logmessage)
        return HttpResponse("person doesnt exist",status=404)

    try:
        reportdata_req = data[ServerConstants.REPORTDATA]
    except KeyError:
        backendlogger.error('setVitalForTracking', data, 'reportdata not sent', logmessage)
        return HttpResponse("reportdata not sent", status=403)

    try:
        appuid_req = data[ServerConstants.APPUID]
    except KeyError:
        backendlogger.error('setVitalForTracking', data, 'appuid not sent', logmessage)
        return HttpResponse("appuid not sent",status=403)

    try:
        appdetails = AppDetails.objects.get(appuid=appuid_req)
    except ObjectDoesNotExist:
        backendlogger.error('setVitalForTracking', data, 'appdetails doesnt exist', logmessage)
        return HttpResponse("appdetails doesnt exist",status=404)

    try:
        healthcaseid_req = data[ServerConstants.HEALTHCASEID]
        healthcase_req = HealthCase.objects.get(healthcaseid=healthcaseid_req)
    except (KeyError, ObjectDoesNotExist):
        healthcase_req = None

    try:
        bp_req = reportdata_req[ServerConstants.BPMACHINE]
    except KeyError:
        bp_req=None

    try:
        weight_req = reportdata_req[ServerConstants.WEIGHTMACHINE]
    except KeyError:
        weight_req=None

    try:
        temperature_req = reportdata_req[ServerConstants.TEMPERATUREMACHINE]
    except KeyError:
        temperature_req=None

    try:
        oximeter_req = reportdata_req[ServerConstants.OXIMETERMACHINE]
    except KeyError:
        oximeter_req=None

    try:
        bloodglucose_req = reportdata_req[ServerConstants.BLOODGLUCOSEMACHINE]
    except KeyError:
        bloodglucose_req=None


    if (bp_req):
        bpobj = bp.objects.create(person=person,
                   systolic=bp_req[ServerConstants.SYSTOLIC],
                   diastolic=bp_req[ServerConstants.DIASTOLIC],
                   pulse=bp_req[ServerConstants.PULSE],
                   appdetails=appdetails,
                   healthcase=healthcase_req)


    if (oximeter_req):
        oximeterobj = oximeter.objects.create(person=person,
                               oxygensat=oximeter_req[ServerConstants.OXYGENSAT],
                               pulse=oximeter_req[ServerConstants.PULSE],
                               appdetails=appdetails,
                               healthcase=healthcase_req)


    if (weight_req):
        weightobj = weight.objects.create(person=person,
                           bmi=weight_req.get(ServerConstants.BMI, 0),
                           fat=weight_req.get(ServerConstants.FAT, 0),
                           bonemass=weight_req.get(ServerConstants.BONEMASS, 0),
                           weight=weight_req.get(ServerConstants.WEIGHT, 0),
                           muscle=weight_req.get(ServerConstants.MUSCLE, 0),
                           hydration=weight_req.get(ServerConstants.HYDRATION, 0),
                           muscle_quality_score=weight_req.get(ServerConstants.MUSCLEQUALITYSCORE, 0),
                           physique_rating=weight_req.get(ServerConstants.PHYSIQUERATING, 0),
                           appdetails=appdetails,
                           kcal=weight_req.get(ServerConstants.KCAL, 0),
                           vfat=weight_req.get(ServerConstants.VFAT, 0),
                           sfat=weight_req.get(ServerConstants.SFAT, 0),
                           bage=weight_req.get(ServerConstants.BAGE, 0),
                           prot=weight_req.get(ServerConstants.PROT, 0),
                           healthcase=healthcase_req)

    if (temperature_req):
        temperature_obj = temperature.objects.create(person=person,
                                     temperature=temperature_req[ServerConstants.TEMPERATURE],
                                     appdetails=appdetails,
                                    healthcase=healthcase_req)



    if (bloodglucose_req):
        bloodglucoseobj = bloodglucose.objects.create(person=person,
                                       glucose_level=bloodglucose_req[ServerConstants.BLOODGLUCOSE],
                                       testtypecode=bloodglucose_req[ServerConstants.TESTTYPECODE],
                                       measuremethod=bloodglucose_req[ServerConstants.MEASUREMETHOD],
                                       appdetails=appdetails,
                                        healthcase=healthcase_req)

    if healthcase_req:  # just update the healthcase updated_at field so that it appears on top to doctor
        healthcase_req.save()

    httpoutput = utils.successJson(dict())
    backendlogger.info('setVitalForTracking', data, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def getVitalsForTracking(request):

    logmessage = []
    data = json.loads(request.body)

    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getVitalsForTracking', data, 'error authenticating person', logmessage)
        return HttpResponse("Error authenticating person", status=401)

    try:
        personuid_req = data[ServerConstants.PERSONUID]
    except KeyError:
        backendlogger.error('getVitalsForTracking', data, 'person id not found', logmessage)
        return HttpResponse("personuid not found",status=403)

    try:
        person = PersonDetails.objects.get(personuid=personuid_req)
    except ObjectDoesNotExist:
        backendlogger.error('getVitalsForTracking', data, 'person doesnt exist', logmessage)
        return HttpResponse("person doesnt exist",status=404)

    try:
        healthcaseid_req = data[ServerConstants.HEALTHCASEID]
        healthcase_req = HealthCase.objects.get(healthcaseid=healthcaseid_req)
    except:
        backendlogger.error('getVitalsForTracking', data, 'healthcase doesnt exist', logmessage)
        return HttpResponse("person doesnt exist", status=404)


    try:
        vitaltype_req = data[ServerConstants.VITALTYPE]
    except:
        backendlogger.error('getVitalsForTracking', data, 'vitaltype not sent', logmessage)
        return HttpResponse("person doesnt exist", status=403)

    try:
        days_req = data[ServerConstants.DAYS]
    except:
        backendlogger.error('getVitalsForTracking', data, 'days_req not sent', logmessage)
        return HttpResponse("days not sent", status=403)

    enddate = datetime.now()
    startdate = enddate + timedelta(days=-days_req)
    startdate.replace(minute=00, hour=00, second=00)

    jsondata = list()

    if vitaltype_req == VitalTypeConstant.BPMACHINE:
        bpobj = bp.objects.filter(person=person, healthcase=healthcase_req, datetime__range=(startdate, enddate)).order_by('-datetime')
        if len(bpobj) > 0:
            systoliclist = list(map(lambda thisbpobj: {ServerConstants.DATETIME:thisbpobj.datetime, ServerConstants.VITALVALUE: thisbpobj.systolic }, bpobj))
            diastoliclist = list(map(lambda thisbpobj: {ServerConstants.DATETIME:thisbpobj.datetime, ServerConstants.VITALVALUE: thisbpobj.diastolic }, bpobj))
            systolicavg = bpobj.aggregate(Avg('systolic'))['systolic__avg']
            diastolicavg = bpobj.aggregate(Avg('diastolic'))['diastolic__avg']
            bpinfernceinput = {ServerConstants.SYSTOLIC: systolicavg, ServerConstants.DIASTOLIC: diastolicavg}
            bpinference = get_bp_inference(bpinfernceinput, person.get_age())
            systolicdata = {ServerConstants.INFERENCE: bpinference[0], ServerConstants.VITALVALUESLIST: systoliclist}
            diastolicdata = {ServerConstants.INFERENCE: bpinference[1], ServerConstants.VITALVALUESLIST: diastoliclist}
            jsondata.append({ServerConstants.NAME: "Systolic (mmHg)", ServerConstants.DATA: systolicdata})
            jsondata.append({ServerConstants.NAME: "Diastolic (mmHg)", ServerConstants.DATA: diastolicdata})

    elif vitaltype_req == VitalTypeConstant.OXIMETERMACHINE:
        oximeterobj = oximeter.objects.filter(person=person, healthcase=healthcase_req, datetime__range=(startdate, enddate)).order_by('-datetime')
        if len(oximeterobj) > 0:
            pulselist = list(map(lambda thisoximeterobj: {ServerConstants.DATETIME:thisoximeterobj.datetime, ServerConstants.VITALVALUE: thisoximeterobj.pulse }, oximeterobj))
            oxygensatlist = list(map(lambda thisoximeterobj: {ServerConstants.DATETIME:thisoximeterobj.datetime, ServerConstants.VITALVALUE: thisoximeterobj.oxygensat }, oximeterobj))
            pulseavg = oximeterobj.aggregate(Avg('pulse'))['pulse__avg']
            oxygensatavg = oximeterobj.aggregate(Avg('oxygensat'))['oxygensat__avg']
            pulsedata = {ServerConstants.INFERENCE: get_pulse_inference(pulseavg,person.get_age())[0], ServerConstants.VITALVALUESLIST: pulselist}
            oxygensatdata = {ServerConstants.INFERENCE: get_oxygensat_inference(oxygensatavg)[0], ServerConstants.VITALVALUESLIST: oxygensatlist}
            jsondata.append({ServerConstants.NAME: "Pulse (bpm)", ServerConstants.DATA: pulsedata})
            jsondata.append({ServerConstants.NAME: "SpO2", ServerConstants.DATA: oxygensatdata})

    elif vitaltype_req == VitalTypeConstant.TEMPERATUREMACHINE:
        temperatureobj = temperature.objects.filter(person=person, healthcase=healthcase_req, datetime__range=(startdate, enddate)).order_by('-datetime')
        if len(temperatureobj) > 0:
            temperaturelist = list(map(lambda thistemperatureobj: {ServerConstants.DATETIME:thistemperatureobj.datetime, ServerConstants.VITALVALUE: thistemperatureobj.temperature }, temperatureobj))
            temperatureavg = temperatureobj.aggregate(Avg('temperature'))['temperature__avg']
            temperaturedata = {ServerConstants.INFERENCE: get_temperature_inference(temperatureavg)[0], ServerConstants.VITALVALUESLIST: temperaturelist}
            jsondata.append({ServerConstants.NAME: "Temperature (°F)", ServerConstants.DATA: temperaturedata})

    httpoutput = utils.successJson({ServerConstants.ALLVITALSARRAY: jsondata})
    backendlogger.info('getVitalsForTracking', data, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

@csrf_exempt
def getLatestVitalsForTracking(request):

    logmessage = []
    data = json.loads(request.body)

    try:
        personuid_req = data[ServerConstants.PERSONUID]
    except KeyError:
        backendlogger.error('getLatestVitalsForTracking', data, 'person id not found', logmessage)
        return HttpResponse("personuid not found",status=403)

    try:
        person = PersonDetails.objects.get(personuid=personuid_req)
    except ObjectDoesNotExist:
        backendlogger.error('getLatestVitalsForTracking', data, 'person doesnt exist', logmessage)
        return HttpResponse("person doesnt exist",status=404)


    bpdata = None
    ecgdata = None
    oximeterdata = None
    temperaturedata = None

    try:
        bpobj = bp.objects.filter(person=person).latest('datetime')
        bpdata = {ServerConstants.SYSTOLIC: bpobj.systolic, ServerConstants.DIASTOLIC: bpobj.diastolic, ServerConstants.DATETIME: bpobj.datetime.astimezone(timezone.get_default_timezone())}
    except Exception as e:
        logmessage.append(str(e))
        pass

    try :
        ecgobj = ecg.objects.filter(person=person).latest('datetime')
        ecgdata = {ServerConstants.ECG_IMG: ecgobj.ecg_img, ServerConstants.DATETIME: ecgobj.datetime.astimezone(timezone.get_default_timezone())}

    except Exception as e:
        logmessage.append(str(e))
        pass

    try:
        oximeterobj = oximeter.objects.filter(person=person).latest('datetime')
        oximeterdata = {ServerConstants.PULSE: oximeterobj.pulse, ServerConstants.OXYGENSAT: oximeterobj.oxygensat,
                  ServerConstants.DATETIME: oximeterobj.datetime.astimezone(timezone.get_default_timezone())}
    except Exception as e:
        logmessage.append(str(e))
        pass

    try:
        temperatureobj = temperature.objects.filter(person=person).latest('datetime')
        temperaturedata = {ServerConstants.TEMPERATURE: temperatureobj.temperature,
                  ServerConstants.DATETIME: temperatureobj.datetime.astimezone(timezone.get_default_timezone())}
    except Exception as e:
        logmessage.append(str(e))
        pass

    jsondata = dict({
                    ServerConstants.BPMACHINE: bpdata,
                    ServerConstants.ECGMACHINE: ecgdata,
                    ServerConstants.OXIMETERMACHINE: oximeterdata,
                    ServerConstants.TEMPERATUREMACHINE: temperaturedata,
    })

    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getLatestVitalsForTracking', data, httpoutput, logmessage)
    # requestlogger.info('getAppReportResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)