from django.conf.urls import url

from BodyVitals import views

urlpatterns = [
                        url(r'^sendVitalReport/', views.sendVitalReport, name='sendVitalReport'),
                        url(r'^setReportForPerson/',views.setReportForPerson,name='setReportForPerson'),
                       url(r'^setPrescriptionReportForPerson/',views.setPrescriptionReportForPerson,name='setPrescriptionReportForPerson'),
                       url(r'^retrieveLastPrescription/',views.retrieveLastPrescription,name='retrieveLastPrescription'),
                       url(r'^getVitalReport/',views.getVitalReport,name='getVitalReport'),
                       url(r'^getPrescription/',views.getPrescription,name='getPrescription'),
                       url(r'^getImageReport/',views.getImageReport,name='getImageReport'),
                       url(r'^getHealthHistoryForPerson/',views.getHealthHistoryForPerson,name='getHealthHistoryForPerson'),
                       url(r'^getTestDataForDateRangeAndApp/',views.getTestDataForDateRangeAndApp,name='getTestDataForDateRangeAndApp'),
                       url(r'^getAllTestCountForDateRangeAndApp/',views.getAllTestCountForDateRangeAndApp,name='getAllTestCountForDateRangeAndApp'),
                       url(r'^getLatestPersonReport/',views.getLatestPersonReport,name='getLatestPersonReport'),

                       url(r'^downloadAppReport/', views.download_app_report, name='downloadAppReport'),
                       url(r'^downloadPrescription/',views.download_prescription,name='downloadPrescription'),

                       url(r'^sendReportSms/', views.sendReportSms, name='sendReportSms'),

                       url(r'^saveRapidTestReport/', views.saveRapidTestReport, name='saveRapidTestReport'),
                       url(r'^saveUrineTestReport/', views.saveUrineTestReport, name='saveUrineTestReport'),
                       url(r'^saveEyeTestReport/', views.saveEyeTestReport, name='saveEyeTestReport'),
                       url(r'^getRapidTestReport/', views.getRapidTestReport, name='getRapidTestReport'),

                        # vital tracking
                       url(r'^setVitalsForTracking/', views.setVitalsForTracking, name='setVitalsForTracking'),
                       url(r'^getVitalsForTracking/', views.getVitalsForTracking, name='getVitalsForTracking'),
                       url(r'^getLatestVitalsForTracking/', views.getLatestVitalsForTracking, name='getLatestVitalsForTracking'),

]
