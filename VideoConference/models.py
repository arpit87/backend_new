from __future__ import unicode_literals
from datetime import datetime

from django.db import models
from Person.models import PersonDetails
from Doctor.models import DoctorDetails, AppDetails
from django.forms.models import model_to_dict
from backend_new.Constants import  ServerConstants
from django.contrib.auth.models import User
from Appointments.models import SlotBooking
from HealthCase.models import HealthCase
# Create your models here.

class VCDetails(models.Model):
    vcid = models.AutoField(primary_key=True,null=False)
    appdetails = models.ForeignKey(AppDetails, on_delete=models.CASCADE)
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    doctor = models.ForeignKey(DoctorDetails,null=True,blank=True,related_name="doctor", on_delete=models.CASCADE)
    date = models.DateField(auto_now_add=True)  #yyyy-mm-dd
    starttime = models.TimeField(auto_now_add=True)
    endtime = models.TimeField(null=True,blank=True)


    def __str__(self):
       return str(self.vcid)


class VCCodeDetails(models.Model):
    slotbooking =  models.ForeignKey(SlotBooking, on_delete=models.CASCADE)
    vccode = models.CharField(max_length=10, null=False)
    date = models.DateField()  # yyyy-mm-dd
    shorturlpatient = models.CharField(max_length=50, default="")
    healthcase = models.ForeignKey(HealthCase, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('vccode', 'date',)

    def __str__(self):
       return "{}-{}".format(self.slotbooking.person, self.vccode)

