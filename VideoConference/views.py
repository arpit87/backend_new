import logging

from asgiref.sync import async_to_sync
from django.shortcuts import render
from django.utils.safestring import mark_safe
import json
from datetime import datetime
from django.http import HttpResponse
from Appointments.models import SlotBooking
from Doctor.models import AppDetails, DoctorDetails
from Person.models import PersonDetails
from backend_new.Constants import ServerConstants, AppointmentStatusConstant, PreferredLanguageConstant, \
    HealthCaseConstant
from backend_new import config
from VideoConference.models import VCCodeDetails, VCDetails
import Communications.utils
import Platform.utils
from HealthCase.models import HealthCase
requestlogger = logging.getLogger('apirequests')
from datetime import datetime
from QueueManagement.views import generate_token
import time
from Platform.views import authenticateURL
from Platform import backendlogger
import base64
from django.contrib.auth import authenticate,login,logout
import pylibmc
from UrlShortner.views import create_short_url
from Communications.smstemplates import getvccodetemplate
from django.core.exceptions import ObjectDoesNotExist
from Platform.fcm_message import send_to_token
from firebase_admin._messaging_utils import UnregisteredError
from requests import HTTPError

def generatevccode(request):
    data = json.loads(request.body)
    logmessage = []
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('generatevccode', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        slotbookingid_req = data[ServerConstants.SLOTBOOKINGID]
        slotbooking_req = SlotBooking.objects.get(slotbookingid=slotbookingid_req)
    except Exception as e:
        backendlogger.error('generatevccode', data, "Slot booking id not sent", logmessage)
        return HttpResponse("Slot booking id not sent", status=403)

    try:
        healthcaseid_req = data[ServerConstants.HEALTHCASEID]
        healthcase_req = HealthCase.objects.get(healthcaseid=healthcaseid_req)
    except Exception as e:
        backendlogger.error('generatevccode', data, "HEalthCAse id not sent/invalid", logmessage)
        return HttpResponse("HEalthCAse id not sent/invalid", status=403)

    # hoping in 2 tries the same random code on same date will not be generated ever!!!
    try:
        randomcode = Platform.utils.getRandomOTP()
        vcdetails = VCCodeDetails.objects.create(slotbooking = slotbooking_req, vccode=randomcode, date=slotbooking_req.doctoravailability.date, healthcase= healthcase_req)
    except:
        try:
            randomcode = Platform.utils.getRandomOTP()
            vcdetails = VCCodeDetails.objects.create(slotbooking=slotbooking_req, vccode=randomcode, date=slotbooking_req.doctoravailability.date, healthcase = healthcase_req)
        except:
            backendlogger.error('generatevccode', data, 'Error generating unique code', logmessage)
            return HttpResponse("Error generating unique code", status=401)

    try:
        origin = request.headers['Origin']
        if "localhost" in origin:
            origin = config.DOMAIN
    except:
        origin = config.DOMAIN

    urlparamstr = "vccode=" + randomcode
    healthcaseurl = origin + "/videocall/vccode?" + urlparamstr
    shorturl = create_short_url(healthcaseurl)
    vcdetails.shorturlpatient = shorturl
    vcdetails.save()

    dateformatted = vcdetails.date.strftime("%d %b")
    timeformatted = slotbooking_req.starttime.strftime("%I:%M %p")
    Communications.utils.sendSMS(getvccodetemplate(healthcase_req.person.preferredlanguage, dateformatted, timeformatted, randomcode, shorturl ),
                                 slotbooking_req.person.phone, healthcase_req.appdetails.appsmssenderid,healthcase_req.person.preferredlanguage != PreferredLanguageConstant.ENGLISH)
    healthcase_req.status = HealthCaseConstant.VCBOOKED
    healthcase_req.save()
    httpoutput = Platform.utils.successJson(dict({ServerConstants.VCCODE: randomcode}))
    backendlogger.info('generatevccode', data, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


def getvcdetailsfromcode(request):
    data = json.loads(request.body)
    logmessage = []

    try:
        vccode_req = data[ServerConstants.VCCODE]
        date_req = data[ServerConstants.DATE]
        vcdetails = VCCodeDetails.objects.get(vccode=vccode_req, date=date_req)
    except Exception as e:
        backendlogger.error('generatevccode', data, "Code not valid today", logmessage)
        return HttpResponse("Code not valid today / कोड आज मान्य नहीं है ", status=403)

    if datetime.now().time() < vcdetails.slotbooking.starttime:
        backendlogger.error('generatevccode', data, "Called Before Time", logmessage)
        return HttpResponse("Called Before Time / समय से पहले कॉल किया", status=403)

    if  vcdetails.slotbooking.consultstatus == AppointmentStatusConstant.COMPLETED:
        backendlogger.error('generatevccode', data, "code used", logmessage)
        return HttpResponse("Code has been used / कोड का इस्तेमाल किआ जा चूका है", status=403)

    vcdoctor= vcdetails.slotbooking.doctoravailability.doctor
    vcpatient= vcdetails.slotbooking.person
    qbtoken = Platform.utils.getQBUserSessionToken(vcdoctor.counterqbusername, vcdoctor.counterqbpassword)
    vcpatient.djangouser.backend = 'django.contrib.auth.backends.ModelBackend'
    # need to login patient for generate_token to authenticate
    login(request, vcpatient.djangouser)
    res = generate_token(request)
    if res.status_code == 200:
        jsondata = json.loads(res.content)
        eta = jsondata[ServerConstants.ETA]
        token_number = jsondata[ServerConstants.TOKENNUMBER]
        queue_number = jsondata[ServerConstants.QUEUE_NUMBER]
    else:
        backendlogger.error('generatevccode', data, "error in generate token", logmessage)
        return HttpResponse("error in generate token", status=403)

    #login this user
    djangouser = vcdetails.slotbooking.person.djangouser
    djangouser.backend = 'django.contrib.auth.backends.ModelBackend'
    login(request, djangouser)
    request.session.set_expiry(60*15)

    # get doctor online status
    cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
    status = cache.get("onlinestatusdoctor_" + str(vcdoctor.doctoruid))
    isbusy = cache.get("onlinestatusdoctor_" + str(vcdoctor.doctoruid) + "_busy")

    if status is not None:
        logmessage.append("doctor online status: " + str(status))
        isonline = True
    else:
        isonline = False

    if isbusy is not None:
        logmessage.append("doctor busy status found in cache, doctor is busy")
        isbusy = True
    else:
        isbusy = False

    patientdetails = {
        ServerConstants.USERNAME: vcpatient.name,
        ServerConstants.PERSONUID: vcpatient.personuid,
        ServerConstants.AGE: vcpatient.get_age(),
        ServerConstants.GENDER: vcpatient.gender,
        ServerConstants.PROFILEPIC: vcpatient.profilepic,
        ServerConstants.APPLOCATION: vcdetails.healthcase.location,
        ServerConstants.PROBLEM: vcdetails.healthcase.problem,
        ServerConstants.HEALTHCASEID: vcdetails.healthcase.healthcaseid,
        ServerConstants.SLOTBOOKINGID: vcdetails.slotbooking.slotbookingid,
        ServerConstants.APPUID: vcdetails.healthcase.appdetails.appuid,
    }

    counterdata = {
        ServerConstants.QBAPPID: config.QB_APP_ID,
        ServerConstants.QBID: vcdoctor.counterqbid,
        ServerConstants.QBUSERNAME: vcdoctor.counterqbusername,
        ServerConstants.QBPASSWORD: vcdoctor.counterqbpassword,
        ServerConstants.QBTOKEN: qbtoken,
    }

    jsonstr = json.dumps(counterdata)
    paramsencryptedcounterdata = str(base64.urlsafe_b64encode((''.join(chr(ord(a) + 10) for a in jsonstr)).encode("utf-8")),
                          "utf-8")

    jsondata = dict({
        ServerConstants.SESSIONDATA: paramsencryptedcounterdata,
        ServerConstants.DOCTORNAME: vcdoctor.name,
        ServerConstants.DOCTORQBID: vcdoctor.qbid,
        ServerConstants.DOCTORUID: vcdoctor.doctoruid,
        ServerConstants.PATIENTDETAILS : patientdetails,
        ServerConstants.ISONLINE: isonline,
        ServerConstants.ISBUSY: isbusy,
        ServerConstants.ETA: eta,
        ServerConstants.TOKENNUMBER: token_number,
        ServerConstants.QUEUE_NUMBER: queue_number,
    })

    httpoutput = Platform.utils.successJson(jsondata)
    backendlogger.info('generatevccode', data, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

def startvideocallbackendtasks(request):
    data = json.loads(request.body)
    logmessage = []
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('startvideocallbackendtasks', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        appuid_req = data[ServerConstants.APPUID]
        appdetails_req = AppDetails.objects.get(appuid=appuid_req)
    except Exception as e:
        backendlogger.error('startvideocallbackendtasks', data, "appid notsent/invalid", logmessage)
        return HttpResponse("appid notsent/invalid", status=403)


    try:
        doctoruid_req = data[ServerConstants.DOCTORUID]
        doctor_req = DoctorDetails.objects.get(doctoruid=doctoruid_req)
    except Exception as e:
        backendlogger.error('startvideocallbackendtasks', data, "doctoruid notsent/invalid", logmessage)
        return HttpResponse("doctoruid notsent/invalid", status=403)

    try:
        healthcaseid_req = data[ServerConstants.HEALTHCASEID]
    except Exception as e:
        backendlogger.error('startvideocallbackendtasks', data, "healthcaseid_req notsent/invalid", logmessage)
        return HttpResponse("healthcaseid_req notsent/invalid", status=403)


    try:
        personuid_req = data[ServerConstants.PERSONUID]
        person_req = PersonDetails.objects.get(personuid=personuid_req)
    except Exception as e:
        backendlogger.error('startvideocallbackendtasks', data, "personuid notsent/invalid", logmessage)
        return HttpResponse("personuid notsent/invalid", status=403)

    vcdetails = VCDetails.objects.create(appdetails=appdetails_req, doctor=doctor_req, person=person_req)
    fcmresponse = ""
    try:
        doctorfcmtoken = doctor_req.doctorfcmtokenmapping.fcmtoken
        fcmresponse = send_to_token(doctorfcmtoken, {"command":"incomingcall","healthcaseid":healthcaseid_req, "caller":person_req.name})
    except  (ObjectDoesNotExist, HTTPError, UnregisteredError):
        pass

    jsondata = dict({
        ServerConstants.VCID: vcdetails.vcid,
        ServerConstants.FCMTOKEN: fcmresponse,
    })

    httpoutput = Platform.utils.successJson(jsondata)
    backendlogger.info('startvideocallbackendtasks', data, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

