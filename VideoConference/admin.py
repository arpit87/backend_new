from VideoConference.models import VCDetails , VCCodeDetails
from django.contrib import admin


class VCDetailsModelAdmin(admin.ModelAdmin):
    list_display = ('vcid','appdetails','person','doctor','date','starttime','endtime')

admin.site.register(VCDetails, VCDetailsModelAdmin)

class VCCodeDetailsModelAdmin(admin.ModelAdmin):
    list_display = ('vccode','date','shorturlpatient','healthcase')

admin.site.register(VCCodeDetails, VCCodeDetailsModelAdmin)


