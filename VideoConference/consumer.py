import asyncio
import json
import logging

import pylibmc
from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer, AsyncWebsocketConsumer

from Platform import backendlogger
from QueueManagement.models import TVAppDetails
from channels.db import database_sync_to_async

from backend_new.Constants import ServerConstants
websocketlogger = logging.getLogger('websocket')

class VCConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        websocketlogger.info("connected")
        doctoruid = self.scope['url_route']['kwargs']['doctoruid']
        vcgroupname = "vc"+doctoruid
        print('adding to group:{}, channel:{}'.format(vcgroupname, self.channel_name))
        await self.channel_layer.group_add(vcgroupname, self.channel_name)
        await self.accept()
        await self.send(text_data=json.dumps(
            {ServerConstants.MESSAGE_TYPE: 'connection_message', 'status': 'connected', 'doctoruid':doctoruid }
        ))

    async def receive(self, **kwargs):
        websocketlogger.info("receive:", kwargs['text_data'])
        doctoruid = self.scope['url_route']['kwargs']['doctoruid']
        text_data_json = json.loads(kwargs['text_data'])
        try:
            type = text_data_json[ServerConstants.MESSAGE_TYPE]
            websocketlogger.info('Got message for vcconsumer:{}, type:{}'.format(doctoruid, type))
            cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
            if type == "heartbeat":
                # continue to keep person in waiting queue when call started as call may drop
                # and dotor may choose call dropped so same person should be able to call doctor again
                token_number = text_data_json[ServerConstants.TOKENNUMBER]
                doctorq = "doctor" + doctoruid + "q"
                cache.touch(doctorq + str(token_number), 30)
            elif type == "doctorheartbeat":
                # continue to keep doctor in busy state
                cache.set("onlinestatusdoctor_" + str(doctoruid) + "_busy", 1, 30)  # this is updated by VCConsumer socket
            elif type == ServerConstants.DOCTORRINGING:
                websocketlogger.info("got doctorringing, sending to group")
                # received "isrining" from doctor. Send it back on group so patient receives it
                # doctor will receive it too but we will igone at his end
                msg = {"type": "doctorringing.message" }
                await self.channel_layer.group_send("vc"+doctoruid,msg)
            elif type == ServerConstants.CALLACCEPTEDFROMWEB:
                websocketlogger.info("got callacceptedfromweb, sending to group")
                # received "isrining" from doctor. Send it back on group so patient receives it
                # doctor will receive it too but we will igone at his end
                msg = {"type": "callacceptedfromweb.message" }
                await self.channel_layer.group_send("vc"+doctoruid,msg)

        except:
            websocketlogger.info("not found message_type in:", kwargs['text_data'])


    async def disconnect(self, close_code):
        websocketlogger.info("disconnect")
        doctoruid = self.scope['url_route']['kwargs']['doctoruid']
        if doctoruid is not None:
            websocketlogger.info('disconnecting from vc doctor:' + str(doctoruid))
            vccaachename = "vc" + doctoruid
            cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
            cache.delete(vccaachename)  # expiry time in seconds

    async def prescription_message(self, message):
        websocketlogger.info("got prescription msg:" + str(message))
        if message[ServerConstants.MESSAGE_TYPE] == ServerConstants.GOTPRESCRIPTION:
            await self.send(text_data=json.dumps(
                {ServerConstants.MESSAGE_TYPE : ServerConstants.GOTPRESCRIPTION, ServerConstants.PRESCRIPTIONID: message[ServerConstants.PRESCRIPTIONID]}
            ))

    async def doctorringing_message(self, message):
        websocketlogger.info("sending doctorringing msg:" + str(message))
        await self.send(text_data=json.dumps(
            {ServerConstants.MESSAGE_TYPE : ServerConstants.DOCTORRINGING}
        ))

    async def callacceptedfromweb_message(self, message):
        websocketlogger.info("sending callacceptedfromweb msg:" + str(message))
        await self.send(text_data=json.dumps(
            {ServerConstants.MESSAGE_TYPE : ServerConstants.CALLACCEPTEDFROMWEB}
        ))
