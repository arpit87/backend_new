from django.conf.urls import url
from django.urls import path
from VideoConference import views

urlpatterns = [
      url(r'^generatevccode/', views.generatevccode, name='generatevccode'),
      url(r'^getvcdetailsfromcode/', views.getvcdetailsfromcode, name='getvcdetailsfromcode'),
      url(r'^startvideocallbackendtasks/', views.startvideocallbackendtasks, name='startvideocallbackendtasks'),

]

         