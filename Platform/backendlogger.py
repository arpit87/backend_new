'''
    created by sinwar
'''

# for json format log

import logging
from django.utils import timezone
import time
import json


requestlogger = logging.getLogger('backendlog')
logglylogger = logging.getLogger('loggly_logs')


# timestamp, level, api, personuid, doctoruid,
def debug(api,request, response, message):
    #calc timestamp ,platform (android,kiosk,web)
    log = jsonlog_builder(api,request, response,message)
    requestlogger.debug(log)     # platform
    logglylogger.debug(log)

def info(api,request, response,message):
    log = jsonlog_builder(api,request, response,message)
    requestlogger.info(log)
    logglylogger.info(log)

def error(api,request, response,message):
    log = jsonlog_builder(api,request, response,message)
    requestlogger.error(log)
    logglylogger.error(log)

def warning(api,request, response,message):
    log = jsonlog_builder(api,request, response,message)
    requestlogger.warning(log)
    logglylogger.warning(log)

def critical(api,request, response,message):
    log = jsonlog_builder(api,request, response,message)
    requestlogger.critical(log)
    logglylogger.critical(log)

def jsonlog_builder(api,request, response, message):
    json_data = {}
    json_data["api"] = api
    json_data["timestamp"] = time.mktime(timezone.now().timetuple())
    json_data["request"] = str(request)
    json_data["response"] = str(response)
    json_data["message"] = str(message)
    log = json.dumps(json_data)
    return log