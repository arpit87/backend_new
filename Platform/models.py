from django.db import models
from django.utils import timezone
from Doctor.models import DoctorDetails, AppDetails
from Person.models import PersonDetails


class FlavourLatestAppVersionMapping(models.Model):
    flavour = models.CharField(max_length=50,unique=True)
    versioncode = models.IntegerField()
    ismandatoryupdate = models.BooleanField(default=False)
    lastupdated = models.DateTimeField(auto_now=True)
    apkpath = models.CharField(max_length=255)

    def __str__(self):
       return str(self.id)

class FlavourLatestDocAppVersionMapping(models.Model):
    flavour = models.CharField(max_length=50,unique=True)
    versioncode = models.IntegerField()
    ismandatoryupdate = models.BooleanField(default=False)
    lastupdated = models.DateTimeField(auto_now=True)
    apkpath = models.CharField(max_length=255)

    def __str__(self):
       return str(self.id)

class FlavourLatestGlucoAppVersionMapping(models.Model):
    flavour = models.CharField(max_length=50,unique=True)
    versioncode = models.IntegerField()
    ismandatoryupdate = models.BooleanField(default=False)
    lastupdated = models.DateTimeField(auto_now=True)
    apkpath = models.CharField(max_length=255)

    def __str__(self):
       return str(self.id)

class FlavourLatestCovidCareAppVersionMapping(models.Model):
    flavour = models.CharField(max_length=50,unique=True)
    versioncode = models.IntegerField()
    ismandatoryupdate = models.BooleanField(default=False)
    lastupdated = models.DateTimeField(auto_now=True)
    apkpath = models.CharField(max_length=255)

    def __str__(self):
       return str(self.id)

class DoctorFCMTokenMapping(models.Model):
    doctordetails = models.OneToOneField(DoctorDetails, on_delete=models.CASCADE)
    fcmtoken = models.CharField(max_length=1024, null=False)
    lastupdated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.id)

class PersonFCMTokenMapping(models.Model):
    persondetails = models.OneToOneField(PersonDetails, on_delete=models.CASCADE)
    fcmtoken = models.CharField(max_length=1024, null=False)
    lastupdated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.id)

class AppFCMTokenMapping(models.Model):
    appdetails = models.OneToOneField(AppDetails , on_delete=models.CASCADE)
    fcmtoken = models.CharField(max_length=1024,null=False)
    lastupdated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.id)


class HIMSPermissions(models.Model):
    VIEW = 1
    CREATE = 2
    EDIT = 3
    DELETE = 4
    PERMISSION_TYPES = ((VIEW, 'View'),(CREATE,'Create'),(EDIT,'Edit'),(DELETE,'Delete'))
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    permission = models.IntegerField(choices = PERMISSION_TYPES, default= VIEW)
    resource = models.CharField(max_length=30)

class AdminPermissions(models.Model):
    VIEW = 1
    CREATE = 2
    EDIT = 3
    DELETE = 4
    PERMISSION_TYPES = ((VIEW, 'View'),(CREATE,'Create'),(EDIT,'Edit'),(DELETE,'Delete'))
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    permission = models.IntegerField(choices = PERMISSION_TYPES, default= VIEW)
    resource = models.CharField(max_length=30)