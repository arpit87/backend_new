import base64

import Utils
import backend_new.Constants


from HealthPackage.models import PackageSalesAgentAppMapping, PackageSalesAgentHealthPackageMapping
from Lab.models import PersonLabMapping
from Platform import utils
import json
from django.http import HttpResponse
from django.template import Context, loader
import logging
from django.conf import settings
from django.utils import timezone
from django.contrib.auth import authenticate,login,logout
from Doctor.models import DoctorDetails, AppDetails,  DoctorAppMapping, HospitalCoordinatorDoctorMapping
from QueueManagement.models import  TVAppDetails
from Person.models import PersonDetails, AnalystAppMapping, AdminAppMapping, HIMSAppMapping
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from backend_new import config
from django.forms.models import model_to_dict
from Platform.models import FlavourLatestAppVersionMapping, AppFCMTokenMapping, DoctorFCMTokenMapping, \
    FlavourLatestDocAppVersionMapping, HIMSPermissions, FlavourLatestGlucoAppVersionMapping, PersonFCMTokenMapping, \
    AdminPermissions, FlavourLatestCovidCareAppVersionMapping
from backend_new.Constants import ServerConstants

import pylibmc
requestlogger = logging.getLogger('apirequests')
from Platform import backendlogger

### this function returns (isauthenticated)
def authenticateURL(request):

    if backend_new.config.ISTESTMODE==True:
        # print("Test mode is ON")
        return True,None

    #here we check if request from website and user logged in
    try:
        isloggedin = request.user.is_authenticated
        if isloggedin:
            request.session.modified = True
            return True,request.user.username
    except AttributeError:
        #this is not request from website as no user object
        # so check if mobile request
        pass

    tokencheck = authenticateAppToken(request)
    if tokencheck[0]:
        return True,tokencheck[1]

    if authenticateAPPSECRET(request) and config.DEBUG:
        return True,"Unknown-AppSecret"

    #otp and phone can be sent along with any api to authenticate it
    otpcheck = authenticateOTP(request)
    if otpcheck[0]:
        return True,otpcheck[1]

    print('Last return: Invalid Auth params')
    return False,None


def authenticateAPPSECRET(request):
    if backend_new.config.ISTESTMODE==True:
        return True

    if request.method == "POST":
        data = json.loads(request.body)
        try:
            app_secret = data[ServerConstants.APPSECRET]
            if app_secret == config.APPSECRET:
                return True
            else:
                return False
        except KeyError:
            return False

    elif request.method == "GET":
        try:
            app_secret = request.GET[ServerConstants.APPSECRET]
            if app_secret == config.APPSECRET:
                return True
            else:
                return False
        except KeyError:
            return False


def authenticateAppToken(request):
    if request.method == "POST":
        data = json.loads(request.body)
        try:
            authtoken = data[ServerConstants.APPSESSIONTOKEN]
        except KeyError:
            return False,None

    elif request.method == "GET":
        try:
            authtoken = request.GET[ServerConstants.APPSESSIONTOKEN]
        except KeyError:
            return False,None

    cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
    #authdata has djangouserstr
    djangouserstr = cache.get(authtoken)
    if djangouserstr == None:
        return False,None
    else:
        #refresh the token if it hasnt expired yet
        cache.set(key=authtoken,val=djangouserstr,time=60*60*24*15)
        return True,djangouserstr


def authenticateOTP(request):
    if backend_new.config.ISTESTMODE==True:
        return True,None

    if request.method == "POST":
        data = json.loads(request.body)
        try:
            otp = data[ServerConstants.OTP]
            phone = data[ServerConstants.PHONE]
            if checkotp(otp,phone):
                return True,"person"+str(phone)
        except KeyError:
            return False,None

    elif request.method == "GET":
        try:
            otp = request.GET[ServerConstants.OTP]
            phone = request.GET[ServerConstants.PHONE]
            if checkotp(otp,phone):
                return True,"person"+str(phone)
        except KeyError:
            return False,None

    return False,None


def checkotp(otp,phone):
    cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
    cacheotp = cache.get(phone+"_otp")

    if(cacheotp == None):
        requestlogger.info("Cached otp for phone"+phone+"is None")
        return False

    requestlogger.info("sent otp for phone"+phone+"is "+otp)
    requestlogger.info("cached otp for phone"+phone+"is "+cacheotp)
    if str(cacheotp) == str(otp):
        return True
    else:
        return False

def loginperson(request):

    data = json.loads(request.body)
    logmessage = []
    phone = ""
    password = ""
    otp = ""
    appsessiontoken = ""
    djangouser = None
    try:
        phone = data[ServerConstants.PHONE]
    except KeyError:
        pass

    try:
        password = data[ServerConstants.PASSWORD]
    except KeyError:
        pass

    try:
        otp = data[ServerConstants.OTP]
    except KeyError:
        pass

    try:
        appsessiontoken = data[ServerConstants.APPSESSIONTOKEN]
    except KeyError:
        pass

    #first see if username/password provided
    if(phone!="" and password!=""):
        username = "person"+phone
        logmessage.append('login;%s'%(str(data)))
        # requestlogger.info('login;%s'%(str(data)))
        logmessage.append('authenticating username %s,pass:%s'%(username,password))
        # requestlogger.info('authenticating username %s,pass:%s'%(username,password))
        djangouser = authenticate(username=username,password=password)
        if djangouser is None:
            backendlogger.error('loginuser', data, 'Invalid username / password', logmessage)
            return HttpResponse('Invalid username/password', status=401)
        else:
            login(request,djangouser)
            request.session.set_expiry(config.SESSION_EXPIRY_TIME_PERSON)
    elif (phone!="" and otp!="") :   #login with otp
        logmessage.append('otp provided for phone:%s;otp:%s;'%(phone,otp))
        cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
        cacheotp = cache.get(phone+"_otp")
        if str(cacheotp) == str(otp) or str(config.BYPASSOTP) == str(otp):
            #cache.delete(phone+"_otp") do not delete otp as it might be used further in createuser
            logmessage.append('otp verified')
        else:
            logmessage.append('otp not verified ')
            backendlogger.error('loginuser', data, 'OTP not verified', logmessage)
            return HttpResponse('OTP not verified', status=401)
        username = "person"+phone
        logmessage.append('login;%s'%(str(data)))
        logmessage.append('authenticating username %s,pass:%s'%(username,password))
        try :
            djangouser = User.objects.get(username=username)
            #login without password
            djangouser.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request,djangouser)
            request.session.set_expiry(config.SESSION_EXPIRY_TIME_PERSON)
        except:
            backendlogger.error('loginuser', data, "Person doesnt exist", logmessage)
            return HttpResponse('Person doesnt exist', status=404)
    elif (appsessiontoken!="") :   #login with otp
        logmessage.append('appsessiontoken provided ')
        isvalid,djangouserstr = authenticateAppToken(request)
        if isvalid:
            djangouser = User.objects.get(username=djangouserstr)
            djangouser.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request,djangouser)
            request.session.set_expiry(config.SESSION_EXPIRY_TIME_PERSON)
    elif request.user.is_authenticated:
            #if already logged in user then dont login again just send details
            djangouser = request.user
            logmessage.append('Person already has session,skipping authentication')
    else:
        #if no username/pass and not even already existing session then provide pass
        backendlogger.error('loginrequest', data, 'Provide username', logmessage)
        return HttpResponse('Provide username/password/usertype', status=400)

    #now we have django user and what type of login required
    if djangouser is not None:
        role = "person"
        allpersonsonthisnumber = PersonDetails.objects.filter(djangouser=djangouser)

        userlist = list()
        masteruid = None
        mastername = None
        masterphone = None # all user phone same though
        mastergender = None
        masterage = None
        masterprofilepic = None
        for thisperson in allpersonsonthisnumber:
            if thisperson.hierarchy == "master":
                masteruid = thisperson.personuid
                mastername = thisperson.name
                masterphone = thisperson.phone
                mastergender = thisperson.gender
                masterage = timezone.now().year - thisperson.dob_year
                masterprofilepic = thisperson.profilepic
            userlist.append({
                         ServerConstants.USERNAME:thisperson.name ,
                         ServerConstants.PERSONUID:thisperson.personuid ,
                         ServerConstants.AGE: timezone.now().year - thisperson.dob_year,
                         ServerConstants.GENDER:thisperson.gender,
                         ServerConstants.PROFILEPIC: thisperson.profilepic,
            })

        appsessiontoken = utils.getRandomCharacters(10)
        cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
        cache.set(appsessiontoken,djangouser.username,60*24*15) #expiry time in seconds

        jsondata = dict({
                         ServerConstants.PERSONLIST:userlist,
                         ServerConstants.APPSESSIONTOKEN : appsessiontoken,
                         ServerConstants.ROLE:role,
                         ServerConstants.PERSONUID: masteruid,
                         ServerConstants.USERNAME: mastername,
                         ServerConstants.GENDER: mastergender,
                         ServerConstants.AGE: masterage,
                         ServerConstants.PROFILEPIC: masterprofilepic,
                         ServerConstants.PHONE: masterphone,
        })

        logmessage.append('authenticated')
        jsonstr = json.dumps(jsondata)
        paramsencrypted = str(base64.urlsafe_b64encode((''.join(chr(ord(a) + 10) for a in jsonstr)).encode("utf-8")),
                              "utf-8")
        httpoutput = utils.successJson({ServerConstants.SESSIONDATA: paramsencrypted})
        backendlogger.info('loginuser', data, httpoutput, logmessage)
        response = HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)
        return response

    backendlogger.info('loginuser', data, 'not authenticated', logmessage)
    #if not authenticated
    return HttpResponse('Invalid username/password', status=401)


def logoutrequest(request):
    # requestlogger.info("================logout req============")
    logmessage =[]
    logout(request)

    try:
        data = json.loads(request.body)
        authtoken = data[ServerConstants.APPSESSIONTOKEN]
        cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
        cache.delete(authtoken)
    except:
        pass

    logmessage.append('logged out')
    httpoutput = utils.successJson({})
    response = HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)
    backendlogger.info('logoutrequest', {}, httpoutput, logmessage)
    # requestlogger.info("logout successful")
    return response


def setPassword(request):
    # requestlogger.info("================set password req============")

    if request.method == "POST":
        data = json.loads(request.body)
        logmessage = []
        logmessage.append('Set password;%s'%(str(data)))
        # requestlogger.info('Set password;%s'%(str(data)))
        try:
            otp = data[ServerConstants.OTP]
        except KeyError:
            backendlogger.error('setPassword', data, "otp not sent", logmessage)
            return HttpResponse('otp not sent', status=404)

        try:
            phone = data[ServerConstants.PHONE]
            password = data[ServerConstants.PASSWORD]
            usertype = data[ServerConstants.USERTYPE]
        except KeyError:
            backendlogger.error('setPassword', data, "phone/pass/usertype not sent", logmessage)
            return HttpResponse('phone/pass/usertype not sent', status=404)

        username = ""

        if(usertype.lower()=="doctor"):
            username = "doctor"+phone
        elif(usertype.lower() == "person"):
            username = "person"+phone
        elif(usertype.lower() == "admin"):
            username = "person"+phone
        elif(usertype.lower() == "analyst"):
            username = "person"+phone

        try:
            djangouser = User.objects.get(username=username)
        except ObjectDoesNotExist:
            # requestlogger.info('Phone number not registered')
            backendlogger.error('setPassword', data, 'Phone number not registered', logmessage)
            return HttpResponse('Phone number not registered', status=400)

        if otp is not None:
            logmessage.append('otp provided for phone:%s;otp:%s;'%(phone,otp))
            # requestlogger.info('otp provided for phone:%s;otp:%s;'%(phone,otp))

            cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
            cacheotp = cache.get(phone+"_otp")

            if str(cacheotp) == str(otp):
                cache.delete(phone+"_otp")
                logmessage.append('otp verified')
                # requestlogger.info('otp verified')
            else:
                logmessage.append('otp not verified ')
                # requestlogger.info('otp not verified ')
                backendlogger.error('setPassword', data, 'OTP not verified', logmessage)
                return HttpResponse('OTP not verified', status=400)
        elif djangouser.is_authenticated:
                logmessage.append('Person logged in...setting password')
                # requestlogger.info('Person logged in...setting password')
        else:
            backendlogger.error('setPassword', data, 'otp not provide/ not logged in', logmessage)
            # requestlogger.info('otp not provide/not logged in ')
            return HttpResponse('You are not Authenticate Person', status=401)

        #set pass
        djangouser.set_password(password)
        djangouser.save()
        backendlogger.info('setPassword', data, 'password set', logmessage)
        # requestlogger.info('password set')
        jsondata = dict({ServerConstants.MESSAGE: "Password set"})
        httpoutput = utils.successJson(jsondata)
        # response = HttpResponse("Password set",status=200)
        return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)
    else:
        backendlogger.error('setPassword', " ", 'POST request required', "")
        # requestlogger.info('POST request required')
        response = HttpResponse("Person not found", status =401)
        return response

def logindoctor(request):

    logmessage = []
    data = json.loads(request.body)
    phone = ""
    password = ""
    otp = ""
    appsessiontoken = ""
    djangouser = None
    try:
        phone = data[ServerConstants.PHONE]
    except KeyError:
        pass

    try:
        password = data[ServerConstants.PASSWORD]
    except KeyError:
        pass

    try:
        otp = data[ServerConstants.OTP]
    except KeyError:
        pass

    try:
        appsessiontoken = data[ServerConstants.APPSESSIONTOKEN]
    except KeyError:
        pass

    #first see if username/password provided
    if(phone!="" and password!=""):
        username = "doctor"+phone
        logmessage.append('login;%s'%(str(data)))
        logmessage.append('authenticating username %s,pass:%s'%(username,password))
        djangouser = authenticate(username=username,password=password)
        if djangouser is None:
            backendlogger.error('login doctor', data, "Invalid username/ Password", logmessage)
            return HttpResponse('Invalid username/password', status=401)
        else:
            login(request,djangouser)
    elif phone!="" and otp!="" :   #login with otp
        logmessage.append('otp provided for phone:%s;otp:%s;'%(phone,otp))
        cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
        cacheotp = cache.get(phone+"_otp")
        if str(cacheotp) == otp:
            cache.delete(phone+"_otp")
            logmessage.append('otp verified')
        else:
            logmessage.append('otp not verified ')
            backendlogger.error('logindoctor', data, 'OTP not verified', logmessage)
            return HttpResponse('OTP not verified', status=401)
        username = "person"+phone
        logmessage.append('login;%s'%(str(data)))
        logmessage.append('authenticating username %s,pass:%s'%(username,password))
        djangouser = User.objects.get(username=username)
        if djangouser is None:
            backendlogger.error('logindoctor', data, "Person doesnt exist", logmessage)
            return HttpResponse('Person doesnt exist', status=401)
        else: #login without password
            djangouser.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request,djangouser)
    elif (appsessiontoken!="") :   #login with appsessiontoken
        logmessage.append('appsessiontoken provided ')
        isvalid,djangouserstr = authenticateAppToken(request)
        if isvalid:
            djangouser = User.objects.get(username=djangouserstr)
            djangouser.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request,djangouser)
            request.session.set_expiry(config.SESSION_EXPIRY_TIME_PERSON)
    elif request.user.is_authenticated:
            #if already logged in user then dont login again just send details
            djangouser = request.user
            logmessage.append('Person already has session,skipping authentication')
    else:
        #if no username/pass and not even already existing session then provide pass
        backendlogger.error('logindoctor', data, 'Provide username/password/usertype', logmessage)
        return HttpResponse('Provide username/password/usertype', status=400)

    #now we have django user and what type of login required
    if djangouser is not None:
        if(djangouser.groups.filter(name='doctorgroup').exists()):
            usertype = "doctor"
            doctor = DoctorDetails.objects.get(djangouser=djangouser)
            if not doctor.isactive:
                return HttpResponse('Access revoked', status=403)
            name = doctor.name
            uid = doctor.doctoruid
            profilepic = doctor.profilepic
            signpic = doctor.signpic
            specialization = doctor.specialization
            qbusername = doctor.qbusername
            qbpassword = doctor.qbpassword
            qbid = doctor.qbid
            qbtoken = utils.getQBUserSessionToken(qbusername,qbpassword)
            phone = doctor.phone
            email = doctor.email
            qualification = doctor.qualification
            experience = doctor.experience
            regnum = doctor.registrationnum
            age = 40
            gender = doctor.gender
        else:
            backendlogger.error('logindoctor', data, 'Access denied', logmessage)
            return HttpResponse('Access denied', status=403)

        appstr = ""
        apptags = ""
        consultation_type_list = []
        applist = list()
        try:
            doctorappmappings = DoctorAppMapping.objects.filter(doctor__doctoruid=uid)
            # appstr and apptags sequence should be corresponding to each other
            for mappedapplist in doctorappmappings:
                if appstr == "":
                    appstr = str(mappedapplist.appdetails.appuid)
                    apptags = mappedapplist.appdetails.apptag
                else:
                    appstr = appstr + "," + str(mappedapplist.appdetails.appuid)
                    apptags = apptags + "," + mappedapplist.appdetails.apptag
                applist.append(model_to_dict(mappedapplist.appdetails, fields=('appuid')))
                consultation_type_list.append(mappedapplist.consultation_type)
        except ObjectDoesNotExist:
            pass

        #this token is valid for 15 days is saved in mobile app sent in every subsiquent requests
        appsessiontoken = utils.getRandomCharacters(10)
        cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
        cache.set(appsessiontoken,djangouser.username,60*24*15) #expiry time in seconds
        jsondata = dict({
                         ServerConstants.USERNAME:name,
                         ServerConstants.CONSULTATIONTYPE: consultation_type_list,
                         ServerConstants.PHONE:phone,
                         ServerConstants.PERSONUID:uid,
                         ServerConstants.EMAIL:email,
                         ServerConstants.PROFILEPIC:profilepic,
                         ServerConstants.SIGNPIC:signpic,
                         ServerConstants.ROLE:usertype,
                         ServerConstants.REGISTRATIONNUM:regnum,
                         ServerConstants.QUALIFICATION:qualification,
                         ServerConstants.SPECIALIZATION:specialization,
                         ServerConstants.EXPERIENCE:experience,
                         ServerConstants.QBUSERNAME : qbusername,
                         ServerConstants.QBPASSWORD : qbpassword,
                         ServerConstants.QBTOKEN : qbtoken,
                         ServerConstants.QBID : qbid,
                         ServerConstants.QBAPPID : backend_new.config.QB_APP_ID,
                         ServerConstants.DOMAIN : backend_new.config.DOMAIN,
                         ServerConstants.AGE:age,
                         ServerConstants.GENDER:gender,
                         ServerConstants.APPSTR:appstr,
                         ServerConstants.APPLIST:applist,
                         ServerConstants.APPTAG:apptags,
                         ServerConstants.APPSESSIONTOKEN:appsessiontoken,
        })

        logmessage.append('authenticated')
        jsonstr = json.dumps(jsondata)
        paramsencrypted = str(base64.urlsafe_b64encode((''.join(chr(ord(a) + 10) for a in jsonstr)).encode("utf-8")),
                              "utf-8")
        httpoutput = utils.successJson({ServerConstants.SESSIONDATA: paramsencrypted})
        backendlogger.info('logindoctor', data, httpoutput, logmessage)
        response = HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)
        return response

    backendlogger.info('logindoctor', data, 'not authenticated', logmessage)
    return HttpResponse('Invalid username/password', status=401)

def loginanalyst(request):
    #this has become lil complex...ask for advice if you cant understand something
    # but be careful before making change
    # requestlogger.info("================login req============")

    logmessage = []
    data = json.loads(request.body)
    phone = ""
    password = ""
    otp= ""
    try:
        phone = data[ServerConstants.PHONE]
        password = data[ServerConstants.PASSWORD]
        otp = data[ServerConstants.OTP]
    except KeyError:
        pass

    #first see if username/password provided
    if(phone!="" and password!=""):
        username = "person"+phone

        logmessage.append('login;%s'%(str(data)))
        logmessage.append('authenticating username %s,pass:%s'%(username,password))
        djangouser = authenticate(username=username,password=password)
        if djangouser is None:
            backendlogger.error('login analyast', data, "Invalid username/ Password", logmessage)
            return HttpResponse('Invalid username/password', status=401)
        else:
            login(request,djangouser)
            request.session.set_expiry(config.SESSION_EXPIRY_TIME_ANALYST)
    elif phone!="" and otp!="" :   #login with otp
        logmessage.append('otp provided for phone:%s;otp:%s;'%(phone,otp))
        cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
        cacheotp = cache.get(phone+"_otp")
        if str(cacheotp) == otp:
            cache.delete(phone+"_otp")
            logmessage.append('otp verified')
        else:
            logmessage.append('otp not verified ')
            backendlogger.error('loginanalyst', data, 'OTP not verified', logmessage)
            return HttpResponse('OTP not verified', status=401)
        username = "person"+phone
        logmessage.append('login;%s'%(str(data)))
        logmessage.append('authenticating username %s,pass:%s'%(username,password))
        djangouser = User.objects.get(username=username)
        if djangouser is None:
            backendlogger.error('loginanalyst', data, "Person doesnt exist", logmessage)
            return HttpResponse('Person doesnt exist', status=401)
        else: #login without password
            djangouser.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request,djangouser)
            request.session.set_expiry(config.SESSION_EXPIRY_TIME_ANALYST)
    elif request.user.is_authenticated:
            #if already logged in user then dont login again just send details
            djangouser = request.user
            logmessage.append('Person already has session,skipping authentication')
    else:
        #if no username/pass and not even already existing session then provide pass
        backendlogger.error('loginanlyst', data, 'Provide username/password/usertype', logmessage)
        return HttpResponse('Provide username/password/usertype', status=400)

    #now we have django user and what type of login required
    if djangouser is not None:
        if(djangouser.groups.filter(name='analystgroup').exists()):
            usertype = "analyst"
            try:
                person = PersonDetails.objects.get(djangouser=djangouser, hierarchy__startswith="master")
            except ObjectDoesNotExist:
                backendlogger.error('loginanlyst', data, 'only 1 user should have hierarchy level master to be analyst', logmessage)
                return HttpResponse('Person should have hierarchy level master to be analyst', status=400)
            name = person.name
            uid=person.personuid
            phone = person.phone
            email = person.email
            #get app list for this analyst
            allmappings = AnalystAppMapping.objects.filter(person=person)
            applist = list()
            for thismapping in allmappings:
                applist.append(model_to_dict(thismapping.appdetails, fields=('appuid','applocation','apptag','apptype')))
        else:
            backendlogger.error('loginanlyst', data, 'Access denied', logmessage)
            return HttpResponse('Access denied', status=403)

        # this token is valid for 15 days is saved in mobile app sent in every subsiquent requests
        # it is for mobile app
        appsessiontoken = utils.getRandomCharacters(10)
        cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
        cache.set(appsessiontoken, djangouser.username, 60 * 24 * 15)  # expiry time in seconds

        jsondata = dict({ServerConstants.USERNAME:name ,
                         ServerConstants.PHONE:phone ,
                         ServerConstants.PERSONUID:uid ,
                         ServerConstants.EMAIL:email ,
                         ServerConstants.ROLE:usertype ,
                         ServerConstants.APPLIST:applist ,
                         ServerConstants.APPSESSIONTOKEN: appsessiontoken,
                         })

        logmessage.append('authenticated')
        jsonstr = json.dumps(jsondata)
        paramsencrypted = str(base64.urlsafe_b64encode((''.join(chr(ord(a) + 10) for a in jsonstr)).encode("utf-8")),
                              "utf-8")
        httpoutput = utils.successJson({ServerConstants.SESSIONDATA: paramsencrypted})
        response = HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)
        backendlogger.info('loginanlyst', data, 'authenticated', logmessage)
        return response

    backendlogger.info('loginanlyst', data, 'not authenticated', logmessage)

    #if not authenticated
    # requestlogger.info('not authenticated')
    return HttpResponse('Invalid username/password', status=401)

def loginadmin(request):
    logmessage = []
    data = json.loads(request.body)
    phone = ""
    password = ""
    otp = ""
    try:
        phone = data[ServerConstants.PHONE]
        password = data[ServerConstants.PASSWORD]
        otp = data[ServerConstants.OTP]
    except KeyError:
        pass

    #first see if username/password provided
    if(phone!="" and password!=""):
        username = "person"+phone
        logmessage.append('login;%s'%(str(data)))
        logmessage.append('authenticating username %s,pass:%s'%(username,password))
        djangouser = authenticate(username=username,password=password)
        if djangouser is None:
            backendlogger.error('loginadmin', data, "Invalid username/ Password", logmessage)
            return HttpResponse('Invalid username/password', status=401)
        else:
            login(request,djangouser)
            request.session.set_expiry(config.SESSION_EXPIRY_TIME_ADMIN)
    elif phone!="" and otp!="" :   #login with otp
        logmessage.append('otp provided for phone:%s;otp:%s;'%(phone,otp))
        cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
        cacheotp = cache.get(phone+"_otp")
        if str(cacheotp) == otp:
            cache.delete(phone+"_otp")
            logmessage.append('otp verified')
        else:
            logmessage.append('otp not verified ')
            backendlogger.error('loginadmin', data, 'OTP not verified', logmessage)
            return HttpResponse('OTP not verified', status=401)
        username = "person"+phone
        logmessage.append('login;%s'%(str(data)))
        logmessage.append('authenticating username %s,pass:%s'%(username,password))
        djangouser = User.objects.get(username=username)
        if djangouser is None:
            backendlogger.error('loginadmin', data, "Person doesnt exist", logmessage)
            return HttpResponse('Person doesnt exist', status=401)
        else: #login without password
            djangouser.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request,djangouser)
            request.session.set_expiry(config.SESSION_EXPIRY_TIME_ADMIN)
    elif request.user.is_authenticated:
            #if already logged in user then dont login again just send details
            djangouser = request.user
            logmessage.append('Person already has session,skipping authentication')
    else:
        #if no username/pass and not even already existing session then provide pass
        backendlogger.error('loginadmin', data, 'Provide username/password/usertype', logmessage)
        return HttpResponse('Provide username/password/usertype', status=400)

    #now we have django user and what type of login required
    if djangouser is not None:
        if djangouser.groups.filter(name='admingroup').exists():
            usertype = "admin"
            if djangouser.groups.filter(name='superadmingroup').exists():
                usertype = "superadmin"
            try:
                person = PersonDetails.objects.get(djangouser=djangouser, hierarchy__startswith="master")
            except :
                backendlogger.error('loginadmin', data, 'only 1 user should have hierarchy level master to be admin', logmessage)
                return HttpResponse('Person should have hierarchy level master to be admin', status=400)
            name = person.name
            uid=person.personuid
            profilepic = person.profilepic
            phone = person.phone
            email = person.email
            #get app list for this analyst
            allmappings = AdminAppMapping.objects.filter(person=person)
            applist = list()
            for thismapping in allmappings:
                appdata = dict({
                    ServerConstants.APPUID: thismapping.appdetails.appuid,
                    ServerConstants.APPTAG: thismapping.appdetails.apptag,
                    ServerConstants.APPLOCATION: thismapping.appdetails.applocation,
                })
                try:
                    webapp = thismapping.appdetails.apptemplatedetails
                    appdata[ServerConstants.HOST] = webapp.host
                    appdata[ServerConstants.HOSTSHORTURL] = webapp.hostshortlink
                    appdata[ServerConstants.BRANDNAME] = webapp.brandname
                except:
                    pass
                applist.append(appdata)
        else:
            return HttpResponse('Access denied', status=403)

        alladminpermissionsthisperson = AdminPermissions.objects.filter(person=person)
        permissiondict = dict({ServerConstants.VIEW: [permissionobj.resource for permissionobj in
                                                      alladminpermissionsthisperson.filter(
                                                          permission=AdminPermissions.VIEW)],
                               ServerConstants.CREATE: [permissionobj.resource for permissionobj in
                                                        alladminpermissionsthisperson.filter(
                                                            permission=AdminPermissions.CREATE)],
                               ServerConstants.EDIT: [permissionobj.resource for permissionobj in
                                                      alladminpermissionsthisperson.filter(
                                                          permission=AdminPermissions.EDIT)],
                               ServerConstants.DELETE: [permissionobj.resource for permissionobj in
                                                        alladminpermissionsthisperson.filter(
                                                            permission=AdminPermissions.DELETE)]})

        jsondata = dict({ServerConstants.USERNAME:name ,
                         ServerConstants.PHONE:phone ,
                         ServerConstants.PERSONUID:uid ,
                         ServerConstants.EMAIL:email ,
                         ServerConstants.ROLE:usertype ,
                         ServerConstants.PROFILEPIC:profilepic,
                         ServerConstants.APPLIST:applist,
                         ServerConstants.PERMISSIONS: permissiondict,
                         })

        logmessage.append('authenticated')
        jsonstr = json.dumps(jsondata)
        paramsencrypted = str(base64.urlsafe_b64encode((''.join(chr(ord(a) + 10) for a in jsonstr)).encode("utf-8")),
                              "utf-8")
        httpoutput = utils.successJson({ServerConstants.SESSIONDATA: paramsencrypted})
        response = HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)
        backendlogger.info('loginadmin', data, 'authenticated', logmessage)
        return response

    backendlogger.info('loginadmin', data, 'not authenticated', logmessage)

    #if not authenticated
    # requestlogger.info('not authenticated')
    return HttpResponse('Invalid username/password', status=401)

def loginsalesagent(request):
    logmessage = []
    data = json.loads(request.body)
    phone = ""
    password = ""
    otp = ""
    try:
        phone = data[ServerConstants.PHONE]
        password = data[ServerConstants.PASSWORD]
        otp = data[ServerConstants.OTP]
    except KeyError:
        pass

    #first see if username/password provided
    if(phone!="" and password!=""):
        username = "person"+phone
        logmessage.append('login;%s'%(str(data)))
        logmessage.append('authenticating username %s,pass:%s'%(username,password))
        djangouser = authenticate(username=username,password=password)
        if djangouser is None:
            backendlogger.error('loginadmin', data, "Invalid username/ Password", logmessage)
            return HttpResponse('Invalid username/password', status=401)
        else:
            login(request,djangouser)
            request.session.set_expiry(config.SESSION_EXPIRY_TIME_ADMIN)
    elif phone!="" and otp!="" :   #login with otp
        logmessage.append('otp provided for phone:%s;otp:%s;'%(phone,otp))
        cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
        cacheotp = cache.get(phone+"_otp")
        if str(cacheotp) == otp:
            cache.delete(phone+"_otp")
            logmessage.append('otp verified')
        else:
            logmessage.append('otp not verified ')
            backendlogger.error('loginadmin', data, 'OTP not verified', logmessage)
            return HttpResponse('OTP not verified', status=401)
        username = "person"+phone
        logmessage.append('login;%s'%(str(data)))
        logmessage.append('authenticating username %s,pass:%s'%(username,password))
        djangouser = User.objects.get(username=username)
        if djangouser is None:
            backendlogger.error('loginadmin', data, "Person doesnt exist", logmessage)
            return HttpResponse('Person doesnt exist', status=401)
        else: #login without password
            djangouser.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request,djangouser)
            request.session.set_expiry(config.SESSION_EXPIRY_TIME_ADMIN)
    elif request.user.is_authenticated:
            #if already logged in user then dont login again just send details
            djangouser = request.user
            logmessage.append('Person already has session,skipping authentication')
    else:
        #if no username/pass and not even already existing session then provide pass
        backendlogger.error('loginadmin', data, 'Provide username/password/usertype', logmessage)
        return HttpResponse('Provide username/password/usertype', status=400)

    #now we have django user and what type of login required
    if djangouser is not None:
        if(djangouser.groups.filter(name='salesagentgroup').exists()):
            usertype = "salesagent"
            try:
                person = PersonDetails.objects.get(djangouser=djangouser, hierarchy__startswith="master")
            except :
                backendlogger.error('loginanlyst', data, 'only 1 user should have hierarchy level master to be admin', logmessage)
                return HttpResponse('Person should have hierarchy level master to be admin', status=400)
            name = person.name
            uid=person.personuid
            profilepic = person.profilepic
            phone = person.phone
            email = person.email
            #get app list for this analyst
            allmappings = AdminAppMapping.objects.filter(person=person)
            applist = list()
            for thismapping in allmappings:
                applist.append(model_to_dict(thismapping.appdetails , fields=('appuid','applocation','apptag','apptype')))
        else:
            return HttpResponse('Access denied', status=403)

        jsondata = dict({ServerConstants.USERNAME:name ,
                         ServerConstants.PHONE:phone ,
                         ServerConstants.PERSONUID:uid ,
                         ServerConstants.EMAIL:email ,
                         ServerConstants.ROLE:usertype ,
                         ServerConstants.PROFILEPIC:profilepic,
                         ServerConstants.APPLIST:applist
                         })

        logmessage.append('authenticated')
        jsonstr = json.dumps(jsondata)
        paramsencrypted = str(base64.urlsafe_b64encode((''.join(chr(ord(a) + 10) for a in jsonstr)).encode("utf-8")),
                              "utf-8")
        httpoutput = utils.successJson({ServerConstants.SESSIONDATA: paramsencrypted})
        response = HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)
        backendlogger.info('loginadmin', data, 'authenticated', logmessage)
        return response

    backendlogger.info('loginadmin', data, 'not authenticated', logmessage)

    #if not authenticated
    # requestlogger.info('not authenticated')
    return HttpResponse('Invalid username/password', status=401)

def check_license(request):
    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value

    islicensevalid = False
    isdeviceregistered = False
    appuid = ""
    appid = -1
    licensekey = None
    appsessiontoken = None
    appupdateavailable = False
    apkpath = ""
    latestversioncode = -1
    ismandatoryupdate = False

    try:
        deviceid_req = request.GET[ServerConstants.DEVICEID]
    except:
        backendlogger.error('check_license', logrequest, 'Device ID not sent', logmessage)
        return HttpResponse("Device ID not sent", status=403)

    try:
        versioncode_req = request.GET[ServerConstants.VERSIONCODE]
        flavour_req = request.GET[ServerConstants.FLAVOUR]
    except:
        backendlogger.error('check_license', logrequest, 'versioncode/flaour not sent', logmessage)
        return HttpResponse("versioncode/flavour not sent", status=403)


    try:
        app_details = AppDetails.objects.get(appdeviceid=deviceid_req)
        appuid = app_details.appuid
        appid = app_details.appid
        isdeviceregistered = True
    except:
        backendlogger.error('check_license', logrequest, 'Device not registered', logmessage)

    if isdeviceregistered:
        #check license first
        if not app_details.islicensevalid():
            logmessage.append('License expired for WebApp ID'+str(app_details.appuid))
        else:
            islicensevalid = True
            appsessiontoken = utils.getRandomCharacters(10)
            cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
            cache.set(appsessiontoken, 'app'+appuid, 60 * 24 * 15)  # expiry time in seconds

    try:
        lastestversionmapping = FlavourLatestAppVersionMapping.objects.get(flavour=flavour_req)
        latestversioncode = lastestversionmapping.versioncode
        if latestversioncode > int(versioncode_req):
            appupdateavailable = True
            apkpath = lastestversionmapping.apkpath
            ismandatoryupdate = lastestversionmapping.ismandatoryupdate
    except Exception as e:
        pass

    jsondata = dict({
                    ServerConstants.ISLICENSEVALID:islicensevalid,
                    ServerConstants.ISDEVICEREGISTERED: isdeviceregistered,
                    ServerConstants.APPUID: appuid,
                    ServerConstants.APP_ID: appid,
                    ServerConstants.APPSESSIONTOKEN: appsessiontoken,
                    ServerConstants.APPUPDATEAVAILABLE: appupdateavailable,
                    ServerConstants.APKPATH: apkpath,
                    ServerConstants.LATESTVERSIONCODE: latestversioncode,
                    ServerConstants.ISMANDATORYUPDATE: ismandatoryupdate
                     })

    httpoutput = utils.successJson(jsondata)

    logmessage.append('check_license;%s' % (str(httpoutput)))
    backendlogger.info('check_license', logrequest, 'check_license return', logmessage)
    response = HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)
    return response

def check_license_tv(request):

    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value

    islicensevalid = False
    isdeviceregistered = False
    tvappid = None
    appsessiontoken = None
    queueappid = None

    try:
        deviceid_req = request.GET[ServerConstants.DEVICEID]
    except:
        backendlogger.error('check_license_tv', logrequest, 'Device ID not sent', logmessage)
        return HttpResponse("Device ID not sent", status=403)

    try:
        tvappdetails = TVAppDetails.objects.get(appdeviceid=deviceid_req)
        tvappid = tvappdetails.tvappid
        queueappid = tvappdetails.queueappdetails.appuid
        isdeviceregistered = True
    except:
        backendlogger.error('check_license_tv', logrequest, 'Device not registered', logmessage)

    if isdeviceregistered:
        #check license first
        if not tvappdetails.islicensevalid():
            logmessage.append('License expired for WebApp ID'+str(tvappdetails.appuid))
        else:
            islicensevalid = True
            appsessiontoken = utils.getRandomCharacters(10)
            cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
            cache.set(appsessiontoken, 'tvapp'+str(tvappid), 60 * 24 * 15)  # expiry time in seconds
            djangouser = tvappdetails.djangouser
            djangouser.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request, djangouser)

    jsondata = dict({
                    ServerConstants.ISLICENSEVALID:islicensevalid,
                    ServerConstants.ISDEVICEREGISTERED: isdeviceregistered,
                    ServerConstants.TVAPPID: tvappid,
                    ServerConstants.QUEUEAPPID: queueappid,
                     ServerConstants.APPSESSIONTOKEN: appsessiontoken
                     })

    httpoutput = utils.successJson(jsondata)

    logmessage.append('check_license_tv;%s' % (str(httpoutput)))
    backendlogger.info('check_license_tv', logrequest, 'check_license_tv return', logmessage)
    response = HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)
    return response

def loginpackagesalesagent(request):

    # requestlogger.info("================login req============")

    logmessage = []
    data = json.loads(request.body)
    phone = ""
    password = ""
    otp= ""
    try:
        phone = data[ServerConstants.PHONE]
        password = data[ServerConstants.PASSWORD]
        otp = data[ServerConstants.OTP]
    except KeyError:
        pass

    #first see if username/password provided
    if(phone!="" and password!=""):
        username = "person"+phone
        logmessage.append('login;%s'%(str(data)))
        logmessage.append('authenticating username %s,pass:%s'%(username,password))
        djangouser = authenticate(username=username,password=password)
        if djangouser is None:
            backendlogger.error('loginpackagesalesagent', data, "Invalid username/ Password", logmessage)
            return HttpResponse('Invalid username/password', status=401)
        else:
            login(request,djangouser)
            request.session.set_expiry(config.SESSION_EXPIRY_TIME_PACKAGESALESAGENT)
    elif phone!="" and otp!="" :   #login with otp
        logmessage.append('otp provided for phone:%s;otp:%s;'%(phone,otp))
        cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
        cacheotp = cache.get(phone+"_otp")
        if str(cacheotp) == otp:
            cache.delete(phone+"_otp")
            logmessage.append('otp verified')
        else:
            logmessage.append('otp not verified ')
            backendlogger.error('loginanalyst', data, 'OTP not verified', logmessage)
            return HttpResponse('OTP not verified', status=401)
        username = "person"+phone
        logmessage.append('login;%s'%(str(data)))
        logmessage.append('authenticating username %s,pass:%s'%(username,password))
        djangouser = User.objects.get(username=username)
        if djangouser is None:
            backendlogger.error('loginanalyst', data, "Person doesnt exist", logmessage)
            return HttpResponse('Person doesnt exist', status=401)
        else: #login without password
            djangouser.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request,djangouser)
            request.session.set_expiry(config.SESSION_EXPIRY_TIME_PACKAGESALESAGENT)
    elif request.user.is_authenticated:
            #if already logged in user then dont login again just send details
            djangouser = request.user
            logmessage.append('Person already has session,skipping authentication')
    else:
        #if no username/pass and not even already existing session then provide pass
        backendlogger.error('loginpackagesalesagent', data, 'Provide username/password/usertype', logmessage)
        return HttpResponse('Provide username/password/usertype', status=400)

    #now we have django user and what type of login required
    if djangouser is not None:
        if(djangouser.groups.filter(name='packagesalesagentgroup').exists()):
            usertype = "packagesalesagent"
            try:
                person = PersonDetails.objects.get(djangouser=djangouser, hierarchy__startswith="master")
            except ObjectDoesNotExist:
                backendlogger.error('loginpackagesalesagent', data, 'only 1 user should have hierarchy level master to be analyst', logmessage)
                return HttpResponse('Person should have hierarchy level master to be analyst', status=400)
            name = person.name
            agentid=person.personuid
            phone = person.phone
            email = person.email
            #get app list for this agent
            allmappings = PackageSalesAgentAppMapping.objects.filter(salesagent=person)
            applist = list()
            for thismapping in allmappings:
                applist.append(dict({ServerConstants.APPUID: thismapping.appdetails.appuid, ServerConstants.APPTAG: thismapping.appdetails.apptag}))

            # get package list for this agent
            allmappings = PackageSalesAgentHealthPackageMapping.objects.filter(salesagent=person)
            packagelist = list()
            for thismapping in allmappings:
                packagelist.append(model_to_dict(thismapping.healthpackage))
        else:
            backendlogger.error('loginpackagesalesagent', data, 'Access denied', logmessage)
            return HttpResponse('Access denied', status=403)

        # this token is valid for 15 days is saved in mobile app sent in every subsiquent requests
        # it is for mobile app
        appsessiontoken = utils.getRandomCharacters(10)
        cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
        cache.set(appsessiontoken, djangouser.username, 60 * 24 * 15)  # expiry time in seconds

        jsondata = dict({ServerConstants.USERNAME:name ,
                         ServerConstants.PHONE:phone ,
                         ServerConstants.AGENT_ID:agentid ,
                         ServerConstants.EMAIL:email ,
                         ServerConstants.ROLE:usertype ,
                         ServerConstants.APPLIST:applist ,
                         ServerConstants.PACKAGELIST:packagelist ,
                         ServerConstants.APPSESSIONTOKEN: appsessiontoken,
                         })

        logmessage.append('authenticated')
        jsonstr = json.dumps(jsondata)
        paramsencrypted = str(base64.urlsafe_b64encode((''.join(chr(ord(a) + 10) for a in jsonstr)).encode("utf-8")),
                              "utf-8")
        httpoutput = utils.successJson({ServerConstants.SESSIONDATA: paramsencrypted})
        backendlogger.info('loginpackagesalesagent', data, httpoutput, logmessage)
        response = HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)
        return response

def setFCMtoken(request):
    # requestlogger.info('================setGCMRegistrationId=====================')
    data = json.loads(request.body)
    logmessage = []
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('setFCMtoken', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    logmessage.append('setFCMtoken;data:%s'%(str(data)))
    # requestlogger.info('setGCMRegistrationIdRequestUser;userid:%s'%(str(data)))

    usertype_req = data[ServerConstants.USERTYPE]
    fcmtoken_req = data[ServerConstants.FCMTOKEN]

    if usertype_req == "app":
        try:
            appuid_req = data[ServerConstants.APPUID]
            appdetails = AppDetails.objects.get(appuid=appuid_req)
            try:
                appmapping = AppFCMTokenMapping.objects.get(appdetails=appdetails)
                appmapping.fcmtoken = fcmtoken_req
                appmapping.save()
            except ObjectDoesNotExist:
                AppFCMTokenMapping.objects.create(appdetails=appdetails, fcmtoken=fcmtoken_req)
        except ObjectDoesNotExist:
            backendlogger.error('setFCMtoken', data, 'Invaid appuid', logmessage)
            return HttpResponse("Invalid appuid", status=403)
    elif usertype_req == "doctor":
        try:
            doctoruid_req = data[ServerConstants.DOCTORUID]
            doctordetails = DoctorDetails.objects.get(doctoruid=doctoruid_req)
            try:
                docmapping = DoctorFCMTokenMapping.objects.get(doctordetails=doctordetails)
                docmapping.fcmtoken = fcmtoken_req
                docmapping.save()
            except ObjectDoesNotExist:
                DoctorFCMTokenMapping.objects.create(doctordetails=doctordetails, fcmtoken=fcmtoken_req)
        except:
            backendlogger.error('setFCMtoken', data, 'Invaid doctoruid', logmessage)
            return HttpResponse("Invalid doctoruid", status=403)
    elif usertype_req == "person":
        try:
            personid_req = data[ServerConstants.PERSONUID]
            persondetails = PersonDetails.objects.get(personid=personid_req)
            try:
                personmapping = PersonFCMTokenMapping.objects.get(persondetails=persondetails)
                personmapping.fcmtoken = fcmtoken_req
                personmapping.save()
            except ObjectDoesNotExist:
                PersonFCMTokenMapping.objects.create(persondetails=persondetails, fcmtoken=fcmtoken_req)
        except:
            backendlogger.error('setFCMtoken', data, 'Invaid personid', logmessage)
            return HttpResponse("Invalid personid", status=403)
    else:
        backendlogger.error('setFCMtoken', data, 'Invaid usertype', logmessage)
        return HttpResponse("Invalid usertype", status=403)

    httpoutput = utils.successJson({})
    backendlogger.info('setFCMtoken', data, httpoutput, logmessage)
    # requestlogger.info('setGCMRegistrationIdResponseUser;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

def checkDocAppUpdate(request):
    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value

    appupdateavailable = False
    apkpath = ""
    latestversioncode = -1
    ismandatoryupdate = False
    try:
        versioncode_req = request.GET[ServerConstants.VERSIONCODE]
        flavour_req = request.GET[ServerConstants.FLAVOUR]
    except:
        backendlogger.error('check_license', logrequest, 'versioncode/flaour not sent', logmessage)
        return HttpResponse("versioncode/flavour not sent", status=403)

    try:
        lastestversionmapping = FlavourLatestDocAppVersionMapping.objects.get(flavour=flavour_req)
        latestversioncode = lastestversionmapping.versioncode
        if latestversioncode > int(versioncode_req):
            appupdateavailable = True
            apkpath = lastestversionmapping.apkpath
            ismandatoryupdate = lastestversionmapping.ismandatoryupdate
    except Exception as e:
        pass

    jsondata = dict({
        ServerConstants.APPUPDATEAVAILABLE: appupdateavailable,
        ServerConstants.APKPATH: apkpath,
        ServerConstants.LATESTVERSIONCODE: latestversioncode,
        ServerConstants.ISMANDATORYUPDATE: ismandatoryupdate

    })

    httpoutput = utils.successJson(jsondata)

    logmessage.append('checkDocAppUpdate;%s' % (str(httpoutput)))
    backendlogger.info('checkDocAppUpdate', logrequest, 'check_license return', logmessage)
    response = HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)
    return response

def checkGlucoAppUpdate(request):
    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value

    appupdateavailable = False
    apkpath = ""
    latestversioncode = -1
    ismandatoryupdate = False
    try:
        versioncode_req = request.GET[ServerConstants.VERSIONCODE]
        flavour_req = request.GET[ServerConstants.FLAVOUR]
    except:
        backendlogger.error('checkGlucoAppUpdate', logrequest, 'versioncode/flaour not sent', logmessage)
        return HttpResponse("versioncode/flavour not sent", status=403)

    try:
        lastestversionmapping = FlavourLatestGlucoAppVersionMapping.objects.get(flavour=flavour_req)
        latestversioncode = lastestversionmapping.versioncode
        if latestversioncode > int(versioncode_req):
            appupdateavailable = True
            apkpath = lastestversionmapping.apkpath
            ismandatoryupdate = lastestversionmapping.ismandatoryupdate
    except Exception as e:
        pass

    jsondata = dict({
        ServerConstants.APPUPDATEAVAILABLE: appupdateavailable,
        ServerConstants.APKPATH: apkpath,
        ServerConstants.LATESTVERSIONCODE: latestversioncode,
        ServerConstants.ISMANDATORYUPDATE: ismandatoryupdate

    })

    httpoutput = utils.successJson(jsondata)

    logmessage.append('checkGlucoAppUpdate;%s' % (str(httpoutput)))
    backendlogger.info('checkGlucoAppUpdate', logrequest, 'check_license return', logmessage)
    response = HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)
    return response

def checkCovidCareAppUpdate(request):
    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value

    appupdateavailable = False
    apkpath = ""
    latestversioncode = -1
    ismandatoryupdate = False
    try:
        versioncode_req = request.GET[ServerConstants.VERSIONCODE]
        flavour_req = request.GET[ServerConstants.FLAVOUR]
    except:
        backendlogger.error('checkCovidCareAppUpdate', logrequest, 'versioncode/flaour not sent', logmessage)
        return HttpResponse("versioncode/flavour not sent", status=403)

    try:
        lastestversionmapping = FlavourLatestCovidCareAppVersionMapping.objects.get(flavour=flavour_req)
        latestversioncode = lastestversionmapping.versioncode
        if latestversioncode > int(versioncode_req):
            appupdateavailable = True
            apkpath = lastestversionmapping.apkpath
            ismandatoryupdate = lastestversionmapping.ismandatoryupdate
    except Exception as e:
        logmessage.append(e)
        pass

    jsondata = dict({
        ServerConstants.APPUPDATEAVAILABLE: appupdateavailable,
        ServerConstants.APKPATH: apkpath,
        ServerConstants.LATESTVERSIONCODE: latestversioncode,
        ServerConstants.ISMANDATORYUPDATE: ismandatoryupdate

    })

    httpoutput = utils.successJson(jsondata)

    logmessage.append('checkCovidCareAppUpdate;%s' % (str(httpoutput)))
    backendlogger.info('checkCovidCareAppUpdate', logrequest, 'check_license return', logmessage)
    response = HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)
    return response

def checkPatientMonitorAppUpdate(request):
    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value

    appupdateavailable = False
    apkpath = ""
    latestversioncode = -1
    ismandatoryupdate = False
    try:
        versioncode_req = request.GET[ServerConstants.VERSIONCODE]
        flavour_req = request.GET[ServerConstants.FLAVOUR]
    except:
        backendlogger.error('checkPatientMonitorAppUpdate', logrequest, 'versioncode/flaour not sent', logmessage)
        return HttpResponse("versioncode/flavour not sent", status=403)

    try:
        lastestversionmapping = FlavourLatestCovidCareAppVersionMapping.objects.get(flavour=flavour_req)
        latestversioncode = lastestversionmapping.versioncode
        if latestversioncode > int(versioncode_req):
            appupdateavailable = True
            apkpath = lastestversionmapping.apkpath
            ismandatoryupdate = lastestversionmapping.ismandatoryupdate
    except Exception as e:
        logmessage.append(e)
        pass

    jsondata = dict({
        ServerConstants.APPUPDATEAVAILABLE: appupdateavailable,
        ServerConstants.APKPATH: apkpath,
        ServerConstants.LATESTVERSIONCODE: latestversioncode,
        ServerConstants.ISMANDATORYUPDATE: ismandatoryupdate

    })

    httpoutput = utils.successJson(jsondata)

    logmessage.append('checkPatientMonitorAppUpdate;%s' % (str(httpoutput)))
    backendlogger.info('checkPatientMonitorAppUpdate', logrequest, 'check_license return', logmessage)
    response = HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)
    return response

def loginlab(request):
    #this has become lil complex...ask for advice if you cant understand something
    # but be careful before making change
    # requestlogger.info("================login req============")

    logmessage = []
    data = json.loads(request.body)
    phone = ""
    password = ""
    otp= ""
    try:
        phone = data[ServerConstants.PHONE]
        password = data[ServerConstants.PASSWORD]
        otp = data[ServerConstants.OTP]
    except KeyError:
        pass

    #first see if username/password provided
    if(phone!="" and password!=""):
        username = "person"+phone

        logmessage.append('login;%s'%(str(data)))
        logmessage.append('authenticating username %s,pass:%s'%(username,password))
        djangouser = authenticate(username=username,password=password)
        if djangouser is None:
            backendlogger.error('login analyast', data, "Invalid username/ Password", logmessage)
            return HttpResponse('Invalid username/password', status=401)
        else:
            login(request,djangouser)
            request.session.set_expiry(config.SESSION_EXPIRY_TIME_ANALYST)
    elif phone!="" and otp!="" :   #login with otp
        logmessage.append('otp provided for phone:%s;otp:%s;'%(phone,otp))
        cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
        cacheotp = cache.get(phone+"_otp")
        if str(cacheotp) == otp:
            cache.delete(phone+"_otp")
            logmessage.append('otp verified')
        else:
            logmessage.append('otp not verified ')
            backendlogger.error('loginlab', data, 'OTP not verified', logmessage)
            return HttpResponse('OTP not verified', status=401)
        username = "person"+phone
        logmessage.append('login;%s'%(str(data)))
        logmessage.append('authenticating username %s,pass:%s'%(username,password))
        djangouser = User.objects.get(username=username)
        if djangouser is None:
            backendlogger.error('loginlab', data, "Person doesnt exist", logmessage)
            return HttpResponse('Person doesnt exist', status=401)
        else: #login without password
            djangouser.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request,djangouser)
            request.session.set_expiry(config.SESSION_EXPIRY_TIME_ANALYST)
    elif request.user.is_authenticated:
            #if already logged in user then dont login again just send details
            djangouser = request.user
            logmessage.append('Person already has session,skipping authentication')
    else:
        #if no username/pass and not even already existing session then provide pass
        backendlogger.error('loginlab', data, 'Provide username/password/usertype', logmessage)
        return HttpResponse('Provide username/password/usertype', status=400)

    #now we have django user and what type of login required
    if djangouser is not None:
        if(djangouser.groups.filter(name='labgroup').exists()):
            usertype = "lab"
            try:
                person = PersonDetails.objects.get(djangouser=djangouser, hierarchy__startswith="master")
            except ObjectDoesNotExist:
                backendlogger.error('loginlab', data, 'only 1 user should have hierarchy level master to be analyst', logmessage)
                return HttpResponse('Person should have hierarchy level master to be analyst', status=400)
            name = person.name
            uid=person.personuid
            phone = person.phone
            email = person.email
            #get app list for this analyst
            allmappings = AnalystAppMapping.objects.filter(person=person)
            applist = list()
            for thismapping in allmappings:
                applist.append(model_to_dict(thismapping.appdetails , fields=('appuid','applocation','apptag','apptype')))
        else:
            backendlogger.error('loginlab', data, 'Access denied', logmessage)
            return HttpResponse('Access denied', status=403)

        # this token is valid for 15 days is saved in mobile app sent in every subsiquent requests
        # it is for mobile app
        appsessiontoken = utils.getRandomCharacters(10)
        cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
        cache.set(appsessiontoken, djangouser.username, 60 * 24 * 15)  # expiry time in seconds

        lablist = []
        applabmapping = PersonLabMapping.objects.filter(persondetails=person)
        for thismap in applabmapping:
            lablist.append(
                {ServerConstants.LABID: thismap.labdetails.id, ServerConstants.LABNAME: thismap.labdetails.name})

        jsondata = dict({ServerConstants.USERNAME:name ,
                         ServerConstants.PHONE:phone ,
                         ServerConstants.PERSONUID:uid ,
                         ServerConstants.EMAIL:email ,
                         ServerConstants.ROLE:usertype,
                         ServerConstants.LABLIST:lablist
                         })

        logmessage.append('authenticated')
        jsonstr = json.dumps(jsondata)
        paramsencrypted = str(base64.urlsafe_b64encode((''.join(chr(ord(a) + 10) for a in jsonstr)).encode("utf-8")),
                              "utf-8")
        httpoutput = utils.successJson({ServerConstants.SESSIONDATA: paramsencrypted})
        backendlogger.info('loginpackagesalesagent', data, httpoutput, logmessage)
        response = HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)
        return response

    backendlogger.info('loginlab', data, 'not authenticated', logmessage)

    #if not authenticated
    # requestlogger.info('not authenticated')
    return HttpResponse('Invalid username/password', status=401)

def loginhims(request):
    logmessage = []
    data = json.loads(request.body)
    phone = ""
    password = ""
    otp = ""
    try:
        phone = data[ServerConstants.PHONE]
        password = data[ServerConstants.PASSWORD]
        otp = data[ServerConstants.OTP]
    except KeyError:
        pass

    #first see if username/password provided
    if(phone!="" and password!=""):
        username = "person"+phone
        logmessage.append('login;%s'%(str(data)))
        logmessage.append('authenticating username %s,pass:%s'%(username,password))
        djangouser = authenticate(username=username,password=password)
        if djangouser is None:
            backendlogger.error('loginhims', data, "Invalid username/ Password", logmessage)
            return HttpResponse('Invalid username/password', status=401)
        else:
            login(request,djangouser)
            request.session.set_expiry(config.SESSION_EXPIRY_TIME_ADMIN)
    elif phone!="" and otp!="" :   #login with otp
        logmessage.append('otp provided for phone:%s;otp:%s;'%(phone,otp))
        cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
        cacheotp = cache.get(phone+"_otp")
        if str(cacheotp) == otp:
            cache.delete(phone+"_otp")
            logmessage.append('otp verified')
        else:
            logmessage.append('otp not verified ')
            backendlogger.error('loginhims', data, 'OTP not verified', logmessage)
            return HttpResponse('OTP not verified', status=401)
        username = "person"+phone
        logmessage.append('login;%s'%(str(data)))
        logmessage.append('authenticating username %s,pass:%s'%(username,password))
        djangouser = User.objects.get(username=username)
        if djangouser is None:
            backendlogger.error('loginhims', data, "Person doesnt exist", logmessage)
            return HttpResponse('Person doesnt exist', status=401)
        else: #login without password
            djangouser.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request,djangouser)
            request.session.set_expiry(config.SESSION_EXPIRY_TIME_ADMIN)
    elif request.user.is_authenticated:
            #if already logged in user then dont login again just send details
            djangouser = request.user
            logmessage.append('Person already has session,skipping authentication')
    else:
        #if no username/pass and not even already existing session then provide pass
        backendlogger.error('loginhims', data, 'Provide username/password/usertype', logmessage)
        return HttpResponse('Provide username/password/usertype', status=400)

    #now we have django user and what type of login required
    if djangouser is not None:
        if djangouser.groups.filter(name='himsgroup').exists():
            usertype = "himsuser"
            try:
                person = PersonDetails.objects.get(djangouser=djangouser, hierarchy__startswith="master")
            except :
                backendlogger.error('loginhims', data, 'only 1 user should have hierarchy level master to be admin', logmessage)
                return HttpResponse('Person should have hierarchy level master to be admin', status=400)
            name = person.name
            uid=person.personuid
            profilepic = person.profilepic
            phone = person.phone
            #get app list for this analyst
            allmappings = HIMSAppMapping.objects.filter(person=person)
            applist = list()
            for thismapping in allmappings:
                applist.append(model_to_dict(thismapping.appdetails, fields=('appuid','apptag')))
        else:
            return HttpResponse('Access denied', status=403)

        allhimspermissionsthisperson = HIMSPermissions.objects.filter(person = person)
        permissiondict = dict({ServerConstants.VIEW : [permissionobj.resource for permissionobj in allhimspermissionsthisperson.filter(permission = HIMSPermissions.VIEW)],
                               ServerConstants.CREATE : [permissionobj.resource for permissionobj in allhimspermissionsthisperson.filter(permission = HIMSPermissions.CREATE)],
                               ServerConstants.EDIT : [permissionobj.resource for permissionobj in allhimspermissionsthisperson.filter(permission = HIMSPermissions.EDIT)],
                               ServerConstants.DELETE : [permissionobj.resource for permissionobj in allhimspermissionsthisperson.filter(permission = HIMSPermissions.DELETE)]})

        jsondata = dict({ServerConstants.USERNAME:name ,
                         ServerConstants.PHONE:phone ,
                         ServerConstants.PERSONUID:uid ,
                         ServerConstants.ROLE:usertype ,
                         ServerConstants.PROFILEPIC:profilepic,
                         ServerConstants.APPLIST:applist,
                         ServerConstants.PERMISSIONS:permissiondict,
                         })

        jsonstr = json.dumps(jsondata)
        logmessage.append('authenticated: data:'+ jsonstr)

        paramsencrypted = str(base64.urlsafe_b64encode((''.join(chr(ord(a) + 10) for a in jsonstr)).encode("utf-8")),
                              "utf-8")
        httpoutput = utils.successJson({ServerConstants.SESSIONDATA: paramsencrypted})
        backendlogger.info('loginhims', data, httpoutput, logmessage)
        response = HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)
        return response

    backendlogger.info('loginhims', data, 'not authenticated', logmessage)

    #if not authenticated
    # requestlogger.info('not authenticated')
    return HttpResponse('Invalid username/password', status=401)