from django.test import TestCase
import backend_new.Constants
from HealthPackage.models import PackageSalesAgentAppMapping, PackageSalesAgentHealthPackageMapping
from Platform.views import authenticateURL,loginperson,loginpackagesalesagent
from django.test.client import RequestFactory
import json
from Platform import utils
from backend_new.Constants import ServerConstants
from django.contrib.auth.models import User, Group
from django.contrib.auth import authenticate,login,logout
import pylibmc
from backend_new import config
from Person.models import PersonDetails,AnalystAppMapping,QBDetails
from Doctor.models import AppDetails,DoctorDetails
from HealthPackage.models import HealthPackage

# Create your tests here.
class AuthenticationTestCase(TestCase):
    multi_db = True
    def setUp(self):
        config.ISTESTMODE = False
         # Every test needs access to the request factory.
        self.factory = RequestFactory()
        djangouser1 = User.objects.create_user(username = "Doctor9769465241",password="arpit123")
        doctorgroup = Group.objects.create(name="doctorgroup")
        doctorgroup.user_set.add(djangouser1)
        Doctor = DoctorDetails.objects.create(djangouser=djangouser1, name="arpit", phone="9769465241",
                                            skillstr="0,1,2,71", qbid=config.QB_TEST_QBID2, qbusername=config.
                                            QB_TEST_USER2, qbpassword=config.QB_TEST_PASSWORD2, dob="1986-10-07")
        djangouser = User.objects.create_user(username = "user9998705087",password="abhishek981996")
        djangousernew = User.objects.create_user(username = "user919769465241",password="arpit")
        PersonDetails.objects.create(djangouser=djangouser, personuid=5, name='abhishek', phone="9998705087", dob_year=1996)
        PersonDetails.objects.create(djangouser=djangousernew, personuid=10, name='arpit', phone="919769465241", dob_year=1987, dob="1987-05-26")

        djangouser = User.objects.create_user(username = "person919167636253",password="hiren123")
        personnew=PersonDetails.objects.create(djangouser=djangouser, personuid=100, name='hiren', phone="919167636253", dob_year=1987)

        appdetails = AppDetails.objects.create(appuid=80, apptag="Mumbai-80", applocation="Mumbai")
        analystgroup = Group.objects.create(name="analystgroup")
        analystgroup.user_set.add(djangouser)
        admingroup = Group.objects.create(name="admingroup")
        admingroup.user_set.add(djangouser)
        developergroup = Group.objects.create(name="developergroup")
        developergroup.user_set.add(djangouser)
        AnalystAppMapping.objects.create(person=personnew, appdetails=appdetails)

        #create Qb details for phone 9769465241 so that it doesnt try create qblogin in loginuser api call
        QBDetails.objects.create(phone="9769465241",qbid=123234,qbusername="dummy",qbpassword="dummy")

        #packagesalesagent
        packagesalesagentgroup = Group.objects.create(name="packagesalesagentgroup")
        packagesalesagentgroup.user_set.add(djangouser)
        healthpackage = HealthPackage.objects.create(packagename="Health365",packagecode="hcg365",description="all year",packagefee=365)
        PackageSalesAgentAppMapping.objects.create(salesagent=personnew, appdetails=appdetails)
        PackageSalesAgentHealthPackageMapping.objects.create(salesagent=personnew, healthpackage=healthpackage)


    def test_authenticatePOSTURLTestFromMobile(self):
        input = json.dumps({backend_new.Constants.ServerConstants.PERSONUID:100,
                            backend_new.Constants.ServerConstants.USERAPPUUID: "asdfarpitm"})
        request = self.factory.post('/Doctor/createDoctor/',input,'application/json')
        result = authenticateURL(request)
        self.assertEqual(result[0],True)

    def test_withoutpasswordlogin(self):
        djangouser = User.objects.create_user(username = "user9769465241",password="hiren123")
        PersonDetails.objects.create(djangouser=djangouser, name='arpit', phone="9769465241", qbid=123)
        input = json.dumps({backend_new.Constants.ServerConstants.USERTYPE: "user"})
        request = self.factory.post('/Platform/loginperson/',input,ServerConstants.RESPONSE_JSON_TYPE)
        request.user = djangouser
        response = loginperson(request)
        self.assertEqual(response.status_code,200)


    def test_getQBSessionToken(self):
        result = utils.getQBAppSessionToken(False)
        self.assertNotEqual(result,None)



    def test_setPasswordOTP(self):
        #set temp OTP
        cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
        otp = utils.getRandomOTP()
        cache.set("9769465241_otp",otp,600)
        input = json.dumps({backend_new.Constants.ServerConstants.PHONE: "9769465241",
                            backend_new.Constants.ServerConstants.PASSWORD: "123arpit",
                            backend_new.Constants.ServerConstants.USERTYPE: "doctor",
                            backend_new.Constants.ServerConstants.OTP:otp})

        response = self.client.post('/Platform/setPassword/',input,ServerConstants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        check = authenticate(username="Doctor9769465241",password="123arpit")
        self.assertNotEquals(check,None)


    def test_setPasswordLogin(self):
        djangouser = User.objects.create_user(username = "Doctor9999999999",password="hiren123")
        authenticate(username = "Doctor9999999999",password="hiren123")
        input = json.dumps({backend_new.Constants.ServerConstants.PHONE: "9999999999",
                            backend_new.Constants.ServerConstants.PASSWORD: "123hiren",
                            backend_new.Constants.ServerConstants.USERTYPE: "doctor"})

        response = self.client.post('/Platform/setPassword/',input,ServerConstants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        check = authenticate(username="Doctor9999999999",password="123hiren")
        self.assertNotEquals(check,None)


    def test_loginadmin(self):
        input = json.dumps({backend_new.Constants.ServerConstants.PHONE: "9167636253",
                            backend_new.Constants.ServerConstants.PASSWORD: "hiren123",
                            })

        response = self.client.post('/Platform/loginadmin/',input,ServerConstants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        check = authenticate(username="user9167636253",password="hiren123")
        self.assertNotEquals(check,None)


    def test_loginanalyst(self):
        input = json.dumps({backend_new.Constants.ServerConstants.PHONE: "9167636253",
                            backend_new.Constants.ServerConstants.PASSWORD: "hiren123",
                            })

        response = self.client.post('/Platform/loginanalyst/',input,ServerConstants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        check = authenticate(username="user9167636253",password="hiren123")
        self.assertNotEquals(check,None)


    def test_logindoctor(self):
        input = json.dumps({backend_new.Constants.ServerConstants.PHONE: "9769465241",
                            backend_new.Constants.ServerConstants.PASSWORD: "arpit123",
                            })

        response = self.client.post('/Platform/logindoctor/',input,ServerConstants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        check = authenticate(username="Doctor9769465241",password="arpit123")
        self.assertNotEquals(check,None)



    def test_logindeveloper(self):
        input = json.dumps({backend_new.Constants.ServerConstants.PHONE: "9167636253",
                            backend_new.Constants.ServerConstants.PASSWORD: "hiren123",
                            })

        response = self.client.post('/Platform/logindeveloper/',input,ServerConstants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        check = authenticate(username="user919167636253",password="hiren123")
        self.assertNotEquals(check,None)


    def test_loginpackagesalesagent(self):
        input = json.dumps({backend_new.Constants.ServerConstants.PHONE: "919167636253",
                            backend_new.Constants.ServerConstants.PASSWORD: "hiren123",
                            })

        response = self.client.post('/Platform/loginpackagesalesagent/',input,ServerConstants.RESPONSE_JSON_TYPE)
        self.assertEqual(response.status_code,200)
        check = authenticate(username="person919167636253",password="hiren123")
        self.assertNotEquals(check,None)


