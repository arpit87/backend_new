from django.contrib import admin
from Platform.models import FlavourLatestAppVersionMapping, DoctorFCMTokenMapping, AppFCMTokenMapping, \
    FlavourLatestDocAppVersionMapping, HIMSPermissions, AdminPermissions, FlavourLatestGlucoAppVersionMapping,\
    FlavourLatestCovidCareAppVersionMapping


class FlavourLatestAppVersionMappingModelAdmin(admin.ModelAdmin):
    list_display = ('flavour','versioncode','lastupdated','apkpath')
admin.site.register(FlavourLatestAppVersionMapping, FlavourLatestAppVersionMappingModelAdmin)


class FlavourLatestDocAppVersionMappingModelAdmin(admin.ModelAdmin):
    list_display = ('flavour','versioncode','lastupdated','apkpath')
admin.site.register(FlavourLatestDocAppVersionMapping, FlavourLatestDocAppVersionMappingModelAdmin)


class FlavourLatestGlucoAppVersionMappingModelAdmin(admin.ModelAdmin):
    list_display = ('flavour','versioncode','lastupdated','apkpath')
admin.site.register(FlavourLatestGlucoAppVersionMapping, FlavourLatestGlucoAppVersionMappingModelAdmin)

class FlavourLatestCovidCareAppVersionMappingModelAdmin(admin.ModelAdmin):
    list_display = ('flavour','versioncode','lastupdated','apkpath')
admin.site.register(FlavourLatestCovidCareAppVersionMapping, FlavourLatestCovidCareAppVersionMappingModelAdmin)

class DoctorFCMTokenMappingModelAdmin(admin.ModelAdmin):
    list_display = ('lastupdated', 'doctordetails','fcmtoken')

admin.site.register(DoctorFCMTokenMapping, DoctorFCMTokenMappingModelAdmin)

class AppFCMTokenMappingModelAdmin(admin.ModelAdmin):
    list_display = ('lastupdated', 'appdetails', 'fcmtoken')

admin.site.register(AppFCMTokenMapping, AppFCMTokenMappingModelAdmin)

class HIMSPermissionsModelAdmin(admin.ModelAdmin):
    list_display = ('person', 'permission', 'resource')

admin.site.register(HIMSPermissions, HIMSPermissionsModelAdmin)


class AdminPermissionsModelAdmin(admin.ModelAdmin):
    list_display = ('person', 'permission', 'resource')

admin.site.register(AdminPermissions, AdminPermissionsModelAdmin)