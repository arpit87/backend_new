from __future__ import absolute_import, unicode_literals

from celery import shared_task
import logging
from firebase_admin._messaging_utils import UnregisteredError
from requests import HTTPError
from django.core.exceptions import ObjectDoesNotExist

from Platform.fcm_message import send_to_token
from Platform.models import DoctorFCMTokenMapping

celerylogger = logging.getLogger('celery')

@shared_task
def doctoronlinecheck():
    celerylogger.info("in doctoronlinecheck")
    try:
        allmappings = DoctorFCMTokenMapping.objects.all()
        for thismapping in allmappings:
            doctorfcmtoken = thismapping.fcmtoken
            if doctorfcmtoken is not None and doctorfcmtoken != "":
                fcmresponse = send_to_token(doctorfcmtoken,
                                            {"command": "onlinecheck"})
                celerylogger.info("send fcm msg  response:"+fcmresponse)
            else:
                celerylogger.info("no fcm token for:" + thismapping.doctordetails.name)

    except (ObjectDoesNotExist, HTTPError, UnregisteredError):
        pass

    celerylogger.info("finished periodicdoctoronlinecheck")
