import json

from Crypto.Cipher import AES
from django.core.serializers.python import Serializer
import random,string
from django.contrib.auth import authenticate
from hashlib import sha1,sha256
import hmac
import math
import pylibmc
import time
import logging
import requests
from django.utils import timezone
from backend_new import config
from backend_new.Constants import ServerConstants
from Crypto import Random
from decimal import Decimal
import base64
from hashlib import md5
from django.core.serializers.json import DjangoJSONEncoder

BLOCK_SIZE = 16

requestlogger = logging.getLogger('apirequests')

def date_handler(obj):
    print(obj)
    if hasattr(obj, 'isoformat'):
        return obj.isoformat()
    else:
        raise TypeError(
            "Unserializable object {} of type {}".format(obj, type(obj))
        )

def successJson(jsonObj):
    return json.dumps(jsonObj, cls=DjangoJSONEncoder)


def errorJson(jsonObj):
    jsonreturnObj = dict({"status":"error" , "error":jsonObj})
    return json.dumps(jsonreturnObj,sort_keys=True,indent=1, cls=DjangoJSONEncoder)

def getRandomId(len):
    myrg = random.SystemRandom()
    length = len
    # If you want non-English characters, remove the [0:52]
    alphabet = string.ascii_lowercase + string.ascii_uppercase + string.digits
    pw = str().join(myrg.choice(alphabet) for _ in range(length))
    return pw

def getRandomPassword(len):
    myrg = random.SystemRandom()
    length = len
    # If you want non-English characters, remove the [0:52]
    alphabet = string.ascii_letters[0:52] + string.digits
    pw = str().join(myrg.choice(alphabet) for _ in range(length))
    return pw

def getRandomNumber(len):
    myrg = random.SystemRandom()
    length = len
    digits = string.digits
    pw = str().join(myrg.choice(digits) for _ in range(length))
    return pw

def getRandomOTP():
    myrg = random.SystemRandom()
    length = len
    digits = string.digits
    #we want first digit > 0 else the numberfield in kiosk converts otp like 0243 to 243
    firstdigit = myrg.choice(digits)
    while firstdigit == '0':
       firstdigit = myrg.choice(digits)
    pw = str().join(myrg.choice(digits) for _ in range(3))
    return firstdigit+pw

def getRandomCharacters(len):
    myrg = random.SystemRandom()
    length = len
    # If you want non-English characters, remove the [0:52]
    alphabet = string.ascii_uppercase
    pw = str().join(myrg.choice(alphabet) for _ in range(length))
    return pw

def cmstofeetinch(heightincms):
    feet = int(math.floor(heightincms / 30.48))
    inches = int(math.ceil((heightincms / 2.54) - 12*feet))
    return feet,inches

def fahrenheitTocelsius(tempinfahrenheit):
    celsius= (tempinfahrenheit - 32) * .5556
    return Decimal(celsius).quantize(Decimal('1.00'))


def glucose_mgPerdl_To_mmolPerL(glucosemgperdl):
    mmolPerL= glucosemgperdl / 18
    return Decimal(mmolPerL).quantize(Decimal('1.00'))

def authenticateUser(username,password):
    user = authenticate(username=username, password=password)
    if user is not None:
        return user
    else:
        return False

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def get_hmac_sha256(str,key):
    key = bytes(key, "ascii")
    str = bytes(str, "ascii")
    hashed = hmac.new(key, str, sha256).hexdigest()
    return hashed

def get_hmac_sha(str,key):
    key = bytes(key, "ascii")
    str = bytes(str, "ascii")
    hashed = hmac.new(key, str, sha1).hexdigest()
    return hashed

def getQBAppSessionToken(trycache):
    requestlogger.info("================getQBAppSessionToken============")
    cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
    if trycache:
        token = cache.get(ServerConstants.QBSESSIONTOKEN)
        if token!= None:
            requestlogger.info("found in cache")
            return token

    qbappid = config.QB_APP_ID
    qbauthkey = config.QB_AUTH_KEY
    qbauthsecret = config.QB_AUTH_SECRET
    nonce = str(random.randint(1, 10000))
    timestamp = str(int(time.time()))
    signaturestr  = ("application_id=" + qbappid + "&auth_key=" + qbauthkey +
            "&nonce=" + nonce + "&timestamp=" + timestamp)
    dataToSend = dict({"application_id": qbappid ,
                       "auth_key": qbauthkey,
                       "timestamp": timestamp,
                       "nonce": nonce,
                       "signature":get_hmac_sha(signaturestr,qbauthsecret),
                })


    requestlogger.info('updateQBSessionToken;datatoserver:%s'%(json.dumps(dataToSend)))
    req = requests.post(config.QB_SESSION_API, data=json.dumps(dataToSend),
                        headers={'Content-Type': 'application/json','QuickBlox-REST-API-Version': '0.1.0'})

    if(req.status_code!=201):
        requestlogger.error("Couldnt update QB token:%r"%(req.content))
        return

    req_json = json.loads(req.content)
    appid_ret = req_json["session"]["application_id"]
    token_ret = req_json["session"]["token"]
    ts_ret = req_json["session"]["ts"]

    expiry_in_sec = int((int(ts_ret) - time.mktime(timezone.now().timetuple()))/1000)
    #cache.set(Constants.QBSESSIONTOKEN,token_ret,expiry_in_sec-1000) #refresh before expiry
    requestlogger.info('updateQBSessionToken;updated:token%r'%(token_ret))
    return token_ret

def getQBUserSessionToken(username,password):
    #cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
    #token = cache.get("qbsession_"+username)
    #if token!= None:
    #    requestlogger.info("found in cache")
    #    return token

    qbappid = config.QB_APP_ID
    qbauthkey = config.QB_AUTH_KEY
    qbauthsecret = config.QB_AUTH_SECRET
    nonce = str(random.randint(1, 10000))
    timestamp = str(int(time.time()))
    signaturestr  = ("application_id=" + qbappid + "&auth_key=" + qbauthkey +
                     "&nonce=" + nonce + "&timestamp=" + timestamp+
                     "&user[login]=" + username+"&user[password]=" + password)
    dataToSend = dict({"application_id": qbappid ,
                       "auth_key": qbauthkey,
                       "timestamp": timestamp,
                       "nonce": nonce,
                       "user":{"login":username,"password":password},
                       "signature":get_hmac_sha(signaturestr,qbauthsecret),
                })


    requestlogger.info('updateQBUserSessionToken;datatoserver:%s'%(json.dumps(dataToSend)))
    req = requests.post(config.QB_SESSION_API, data=json.dumps(dataToSend),
                        headers={'Content-Type': 'application/json','QuickBlox-REST-API-Version': '0.1.0'})

    if(req.status_code!=201):
        requestlogger.error("Couldnt update QB User token:%r"%(req.content))
        return

    req_json = json.loads(req.content)
    appid_ret = req_json["session"]["application_id"]
    token_ret = req_json["session"]["token"]
    sessionid_ret = req_json["session"]["id"]
    ts_ret = req_json["session"]["ts"]

    expiry_in_sec = int((int(ts_ret) - time.mktime(timezone.now().timetuple()))/1000)
    #cache.set("qbsession_"+username,token_ret,expiry_in_sec-1000) #refresh before expiry
    requestlogger.info('updateQBSessionToken;updated:token%r'%(token_ret))
    return token_ret

def logoutQBUserSession(username):
    requestlogger.info("================logoutQBUserSession============")
    cache = pylibmc.Client(["127.0.0.1"], binary=True,behaviors={"tcp_nodelay": True,"ketama": True})
    token = cache.get("qbsession_"+username)
    if token == None:
        requestlogger.error(" not found in cache")
        return

    qbappid = config.QB_APP_ID
    qbauthkey = config.QB_AUTH_KEY
    qbauthsecret = config.QB_AUTH_SECRET
    nonce = str(random.randint(1, 10000))
    timestamp = str(int(time.time()))
    signaturestr  = ("application_id=" + qbappid + "&auth_key=" + qbauthkey +
                     "&nonce=" + nonce + "&timestamp=" + timestamp+
                     "token=" + token)
    dataToSend = dict({"application_id": qbappid ,
                       "auth_key": qbauthkey,
                       "timestamp": timestamp,
                       "nonce": nonce,
                       "token":token,
                       "signature":get_hmac_sha(signaturestr,qbauthsecret),
                })


    requestlogger.info('deleteQBUserSessionToken;datatoserver:%s'%(json.dumps(dataToSend)))
    req = requests.delete(config.QB_SESSION_API, data=json.dumps(dataToSend),
                          headers={'Content-Type': 'application/json','QuickBlox-REST-API-Version': '0.1.0'})

    if(req.status_code!=201):
        requestlogger.error("Couldnt delete QB User token:%r"%(req.content))
        return

    cache.delete("qbsession_"+username)
    requestlogger.info("deleteQBSessionToken success")
    return


class MySerialiser(Serializer):
    def end_object( self, obj ):
        self._current['id'] = obj._get_pk_val()
        self.objects.append( self._current )

def pad(message):
    return message + (BLOCK_SIZE - len(message) % BLOCK_SIZE) * chr(BLOCK_SIZE - len(message) % BLOCK_SIZE)

def unpad(data):
    return data[:-(data[-1] if type(data[-1]) == int else ord(data[-1]))]

def bytes_to_key(data, salt, output=48):
    # extended from https://gist.github.com/gsakkis/4546068
    assert len(salt) == 8, len(salt)
    data += salt
    key = md5(data).digest()
    final_key = key
    while len(final_key) < output:
        key = md5(key + data).digest()
        final_key += key
    return final_key[:output]

def encrypt(key, key1, passphrase):
    key.replace('/','')
    key1.replace('/','')
    message = key + key1
    salt = Random.new().read(8)
    key_iv = bytes_to_key(passphrase.encode(), salt, 32+16)
    key = key_iv[:32]
    iv = key_iv[32:]
    aes = AES.new(key, AES.MODE_CBC, iv)
    return base64.b64encode(b"Salted__" + salt + aes.encrypt(pad(message))).decode('utf-8')

def decrypt(key, key1, passphrase):
    encrypted = base64.b64decode(passphrase)
    assert encrypted[0:8] == b"Salted__"
    salt = encrypted[8:16]
    key_iv = bytes_to_key(passphrase, salt, 32+16)
    key = key_iv[:32]
    iv = key_iv[32:]
    aes = AES.new(key, AES.MODE_CBC, iv)
    return unpad(aes.decrypt(encrypted[16:])).decode('utf-8')

def createfcmdynamiclink(deeplinkurl, androidpackagename):
    shortLink = ""
    linkrequest =  {
       "longDynamicLink": config.FIREBASE_DYNAMICLINKDOMAIN + "/?link=" +  deeplinkurl +"&apn=" + androidpackagename,
        "suffix": {
            "option": "SHORT"
        }
    }
    requestHeaders = {
        "Content-type": "application/json",
    }

    r = requests.post("https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key="+config.FIREBASE_WEBAPIKEY,
                      data=json.dumps(linkrequest),
                      headers=requestHeaders)

    if (r.status_code == requests.codes.ok):
        link = r.json()
        shortLink = link["shortLink"]
        requestlogger.info("shortLink is %s" % (shortLink))
    else:
        requestlogger.error("firebase error:"+str(r.content))
    return shortLink

