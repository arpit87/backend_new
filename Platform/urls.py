from django.conf.urls import url
from Platform import views

urlpatterns = [
                        url(r'^logout/',views.logoutrequest),
                        url(r'^setPassword/',views.setPassword),
                        url(r'^loginanalyst/',views.loginanalyst),
                        url(r'^logindoctor/',views.logindoctor),
                        url(r'^loginperson/',views.loginperson),
                        url(r'^loginpackagesalesagent/', views.loginpackagesalesagent),
                        url(r'^loginadmin/',views.loginadmin),
                        url(r'^loginlab/',views.loginlab),
                        url(r'^checklicense/', views.check_license),
                        url(r'^checklicensetv/', views.check_license_tv),
                        url(r'^setFCMtoken/', views.setFCMtoken),
                        url(r'^checkDocAppUpdate/', views.checkDocAppUpdate),
                        url(r'^checkGlucoAppUpdate/', views.checkGlucoAppUpdate),
                        url(r'^checkCovidCareAppUpdate/', views.checkCovidCareAppUpdate),
                        url(r'^loginhims/', views.loginhims),

]


