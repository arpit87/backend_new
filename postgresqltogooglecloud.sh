#!/bin/sh
#dev bucket devdbdump
# prod bucket proddbdump
GSBUCKET=$1
DATABASE=backenddb
DATABASE_HOST=$2
ENC_PASS=$3
#tmp path.
TMP_PATH=${HOME}/postgresqldump/

DAY=$(date +"%d")
echo "Date: $DAY"
FILENAME=dbpgbackupfordate$DAY

pg_dump -h ${DATABASE_HOST} ${DATABASE} > ${TMP_PATH}${FILENAME}.bak
echo "Done backing up the database to a file."

rm ${TMP_PATH}${FILENAME}.7z
echo "Removed old compressed file."

echo "Starting compression of new file..."

7z a -p${ENC_PASS} -mx=9 -mhe -t7z ${TMP_PATH}${FILENAME}.7z ${TMP_PATH}${FILENAME}.bak

echo "Done compressing the backup file."

echo "Uploading the new backup..."
gsutil cp ${TMP_PATH}${FILENAME}.7z gs://${GSBUCKET}/
echo "New backup uploaded."
rm ${TMP_PATH}${FILENAME}.bak
echo "All done."
