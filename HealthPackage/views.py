import json
from datetime import datetime , timedelta, date

import pdfkit
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
import time

from django.db.models import Count
from django.utils import timezone
from django.http import HttpResponse
import Communications.utils
from django.conf import settings
from django.template import loader
# Create your views here.
from django.utils.datastructures import MultiValueDictKeyError
from django.views.decorators.csrf import csrf_exempt, requires_csrf_token
from django.db import IntegrityError

from BodyVitals.models import report, prescription, imagereportdetails
from Doctor.models import AppDetails
from HealthPackage.models import HealthPackage, HealthPackageEnrollment, PackageSalesAgentRegisterPersonMapping, \
    PackageSalesAgentAppMapping
from Payment.models import HealthPackageInvoiceDetails
from  Platform import utils
import os
from base64 import b64decode
from Person.models import PersonDetails

from Person.views import  createPerson
from Platform.uid_code_converter import encode_id
from Platform.views import authenticateURL

import logging
from backend_new.Constants import ServerConstants
import pylibmc
import requests
from bs4 import BeautifulSoup
from Platform import backendlogger
import pytz
from backend_new import settings


requestlogger = logging.getLogger('apirequests')

def checkenrollmentstatusforhealthpackage(request):
    # requestlogger.info('================enrollUserForHealthProgram=====================')
    logrequest = {}
    logmessage = []
    # print(request.GET.items)
    for key, value in request.GET.items():
        logrequest[key] = value
    # requestlogger.info('================getPersonDetails=====================')
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getPersonDetails', logrequest, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        personuid_req = request.GET[ServerConstants.PERSONUID]
        person = PersonDetails.objects.get(personuid = personuid_req)
    except (KeyError,ObjectDoesNotExist):
        backendlogger.error('enrollUserForHealthProgram', request.GET, 'User  not found nin request', logmessage)
        return HttpResponse("User not found in request",status=404)


    try:
        packageid_req = request.GET[ServerConstants.PACKAGEID]
        healthpackage = HealthPackage.objects.get(packageid=packageid_req)
    except :
        backendlogger.error('enrollUserForHealthProgram', logrequest, 'Invalid package id', logmessage)
        return HttpResponse("Package id doesn't found in request",status=404)


    enrolments = HealthPackageEnrollment.objects.filter(person=person,healthpackage=healthpackage).order_by('-enrolldate')
    if len(enrolments)>0:
        backendlogger.error('enrollUserForHealthProgram', logrequest, 'enrolled with status:'+str(enrolments[0].getstatus()) , logmessage)
        httpoutput = utils.successJson(dict({ServerConstants.STATUS: enrolments[0].getstatus(),
                                             ServerConstants.STARTDATE:enrolments[0].startdate,
                                             ServerConstants.ENDDATE:enrolments[0].enddate}))
        backendlogger.info('enrollforhealthpackage', logrequest, httpoutput, logmessage)
        # requestlogger.info('getAllInvoiceList;%s'%(str(httpoutput)))
        return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)
    else:
        backendlogger.error('enrollUserForHealthProgram', logrequest, 'Not enrolled', logmessage)
        httpoutput = utils.successJson(dict({ServerConstants.STATUS:0}))
        backendlogger.info('enrollforhealthpackage', logrequest, httpoutput, logmessage)
        # requestlogger.info('getAllInvoiceList;%s'%(str(httpoutput)))
        return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

@csrf_exempt
def enrollforhealthpackage(request):
    # requestlogger.info('================enrollUserForHealthProgram=====================')

    data = json.loads(request.body)
    logmessage = []
    logrequest = {}
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('enrollUserForHealthProgram', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    logmessage.append('enrollUserForHealthProgram;%s'%(str(data)))
    # requestlogger.info('enrollUserForHealthProgram;%s'%(str(data)))

    try:
        personuid_req = data[ServerConstants.PERSONUID]
        person = PersonDetails.objects.get(personuid = personuid_req)
    except KeyError:
        backendlogger.error('enrollUserForHealthProgram', data, 'Person  not found nin request', logmessage)
        return HttpResponse("Person not found in request",status=404)

    try:
        agentid_req = data[ServerConstants.AGENT_ID]
        agent = PersonDetails.objects.get(personuid = agentid_req)
    except :
        agent = None

    try:
        appidid_req = data[ServerConstants.APPUID]
        appdetails = AppDetails.objects.get(appuid=appidid_req)
    except KeyError:
        backendlogger.error('enrollUserForHealthProgram', data, 'Appid  not found nin request', logmessage)
        return HttpResponse("Appid not found in request", status=404)

    try:
        packageid_req = data[ServerConstants.PACKAGEID]
        healthpackage = HealthPackage.objects.get(packageid=packageid_req)
    except :
        backendlogger.error('enrollUserForHealthProgram', data, 'Invalid package id', logmessage)
        return HttpResponse("Package id doesn't found in request",status=404)

    timezone = pytz.timezone(settings.TIME_ZONE)
    startdate_req = timezone.localize(datetime.today())
    enddate_req = timezone.localize(datetime.today()) +timedelta(days=healthpackage.packageduration)

    try:
        healthpackageenrollment = HealthPackageEnrollment.objects.get(person=person,healthpackage=healthpackage, enrollment_status=HealthPackageEnrollment.ENROLLED)
    except ObjectDoesNotExist:
        try:
            healthpackageenrollment = HealthPackageEnrollment.objects.create(person=person,appdetails=appdetails,healthpackage = healthpackage,startdate=startdate_req,enddate=enddate_req,salesagent=agent)
            healthpackageenrollment.enrolluid = encode_id(healthpackageenrollment.enrollid)
            healthpackageenrollment.save()
        except Exception as e:
            backendlogger.error('enrollUserForHealthProgram', data, str(e), logmessage)
            # requestlogger.info('Unable to create user : %s'%(response))
            return HttpResponse('Unable to enroll person', status=404)

    httpoutput = utils.successJson({ServerConstants.ENROLLMENTSTATUS:healthpackageenrollment.getstatus(),
                                    ServerConstants.ENROLLUID: healthpackageenrollment.enrolluid})
    backendlogger.info('enrollforhealthpackage', logrequest, httpoutput, logmessage)
    # requestlogger.info('getAllInvoiceList;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

def gethealthpackageenrollmentsbyagent(request):
    # requestlogger.info('================gethealthpackageenrollmentsbyagent=====================')
    logrequest = {}
    logmessage = []
    # print(request.GET.items)
    for key, value in request.GET.items():
        logrequest[key] = value
    # requestlogger.info('================gethealthpackageenrollmentsbyagent=====================')
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('gethealthpackageenrollmentsbyagent', logrequest, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        agentid_req = request.GET[ServerConstants.AGENT_ID]
        agent_req = PersonDetails.objects.get(personuid=agentid_req)
        username = "person" + agent_req.phone
        logmessage.append('checking username %s, as agent' % (username))
        djangouser = User.objects.get(username=username)
        if djangouser is not None and djangouser.groups.filter(name='packagesalesagentgroup').exists():
            pass
        else:
            raise Exception('Invalid agent in request')
    except :
        backendlogger.error('enrollUserForHealthProgram', request.GET, 'Invalid agent in request', logmessage)
        return HttpResponse("Invalid agent in request", status=403)

    try:
        startdate = request.GET[ServerConstants.STARTDATE]
    except KeyError:
        startdate = datetime(2,1,1)
    try:
        enddate = request.GET[ServerConstants.ENDDATE]
    except KeyError:
        enddate = datetime.now()

    enrollmentinvoices = HealthPackageInvoiceDetails.objects.filter(datetime_generated__range=(startdate, enddate), packagesalesagent=agent_req)

    personList = list()
    for thisinvoice in enrollmentinvoices:
        personList.append({
            ServerConstants.PERSONUID: thisinvoice.person.personuid,
            ServerConstants.USERNAME: thisinvoice.person.name,
            ServerConstants.PHONE: thisinvoice.person.phone,
            ServerConstants.AGE: thisinvoice.person.get_age(),
            ServerConstants.GENDER: thisinvoice.person.gender,
            ServerConstants.INVOICEAMOUNT:thisinvoice.invoiceamount,
            ServerConstants.PAYMENTDONE: thisinvoice.paymentdone,
            ServerConstants.DATE: thisinvoice.datetime_generated,
        })

    jsondata = dict({ServerConstants.PERSONLIST: personList})
    httpoutput = utils.successJson(jsondata)
    backendlogger.error('gethealthpackageenrollmentsbyagent', logrequest, httpoutput, logmessage)
    # requestlogger.info('getAllPersonListResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

def getpersonsregisteredbyagent(request):
    # requestlogger.info('================gethealthpackageenrollmentsbyagent=====================')
    logrequest = {}
    logmessage = []
    # print(request.GET.items)
    for key, value in request.GET.items():
        logrequest[key] = value
    # requestlogger.info('================gethealthpackageenrollmentsbyagent=====================')
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getpersonsregisteredbyagent', logrequest, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        agentid_req = request.GET[ServerConstants.AGENT_ID]
        agent_req = PersonDetails.objects.get(personuid=agentid_req)
        username = "person" + agent_req.phone
        logmessage.append('checking username %s, as agent' % (username))
        djangouser = User.objects.get(username=username)
        if djangouser is not None and djangouser.groups.filter(name='packagesalesagentgroup').exists():
            pass
        else:
            raise Exception('Invalid agent in request')
    except:
        backendlogger.error('getpersonsregisteredbyagent', request.GET, 'Invalid agent in request', logmessage)
        return HttpResponse("Invalid agent in request", status=403)

    try:
        startdate = request.GET[ServerConstants.STARTDATE]
    except KeyError:
        startdate = datetime(2, 1, 1)
    try:
        enddate = request.GET[ServerConstants.ENDDATE]
    except KeyError:
        enddate = datetime.now()

    agentpersonmapping = PackageSalesAgentRegisterPersonMapping.objects.filter(datetime_generated__range=(startdate, enddate),
                                                                    salesagent=agent_req).order_by('-datetime_generated')

    personList = list()
    for thismapping in agentpersonmapping:
        thispersonenrollments = HealthPackageEnrollment.objects.filter(person = thismapping.person)
        if len(thispersonenrollments)>0:
            for thisenrollment in thispersonenrollments:

                # invoice list
                healthpackageinvoices = HealthPackageInvoiceDetails.objects.filter(healthpackageenrollment=thisenrollment)
                invoicelist = []
                for thisinvoice in healthpackageinvoices:
                    invoicelist.append({ServerConstants.INVOICENUM: thisinvoice.invoicenum,
                                        ServerConstants.INVOICEAMOUNT: thisinvoice.invoiceamount,
                                        ServerConstants.UPDATED_AT: thisinvoice.updated_at.astimezone(
                                            timezone.get_current_timezone()).strftime('%d-%b-%y,%I:%M %p'),
                                        ServerConstants.PAYMENTDONE: thisinvoice.paymentdone})

                personList.append({
                    ServerConstants.PERSONUID: thismapping.person.personuid,
                    ServerConstants.USERNAME: thismapping.person.name,
                    ServerConstants.PHONE: thismapping.person.phone,
                    ServerConstants.AGE: thismapping.person.get_age(),
                    ServerConstants.GENDER: thismapping.person.gender,
                    ServerConstants.ENROLLUID: thisenrollment.enrolluid,
                    ServerConstants.ENROLLMENTSTATUS: thisenrollment.getstatus(),
                    ServerConstants.PACKAGENAME: thisenrollment.healthpackage.packagename,
                    ServerConstants.STARTDATE: thisenrollment.startdate,
                    ServerConstants.ENDDATE: thisenrollment.enddate,
                    ServerConstants.INVOICELIST: invoicelist,
                    ServerConstants.ENROLLDATE: thisenrollment.enrolldate,
                })
        else:
            personList.append({
                ServerConstants.PERSONUID: thismapping.person.personuid,
                ServerConstants.USERNAME: thismapping.person.name,
                ServerConstants.PHONE: thismapping.person.phone,
                ServerConstants.AGE: thismapping.person.get_age(),
                ServerConstants.GENDER: thismapping.person.gender,
                ServerConstants.REGISTRATIONDATE: thismapping.datetime_generated,
            })

    jsondata = dict({ServerConstants.PERSONLIST: personList})
    httpoutput = utils.successJson(jsondata)
    backendlogger.error('getpersonsregisteredbyagent', logrequest, httpoutput, logmessage)
    # requestlogger.info('getAllPersonListResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


def getpersonshealthpackage(request):

    data = json.loads(request.body)
    logmessage = []
    logrequest = {}
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getpersonshealthpackage', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    logmessage.append('getpersonshealthpackage;%s' % (str(data)))

    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getpersonshealthpackage', logrequest, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        personuid_req = data[ServerConstants.PERSONUID]
        person = PersonDetails.objects.get(personuid = personuid_req)
    except KeyError:
        backendlogger.error('getpersonshealthpackage', logrequest, 'User  not found nin request', logmessage)
        return HttpResponse("User not found in request",status=404)


    enrollments = HealthPackageEnrollment.objects.filter(person=person).order_by('-enrolldate')
    packagelist = list()
    for thisenrollment in enrollments:
        try:
            invoicenum = thisenrollment.healthpackageinvoice.get().invoicenum
        except Exception as e:
            logmessage.append(str(e))
            invoicenum = None

        packagelist.append(dict({ServerConstants.STATUS: thisenrollment.getstatus(),
                                 ServerConstants.INVOICENUM: invoicenum,
                                 ServerConstants.PACKAGENAME:thisenrollment.healthpackage.packagename,
                                 ServerConstants.ENROLLUID:thisenrollment.enrolluid,
                                 ServerConstants.STARTDATE:thisenrollment.startdate,
                                 ServerConstants.ENDDATE:thisenrollment.enddate}))

    httpoutput = utils.successJson({ServerConstants.PACKAGELIST:packagelist})
    backendlogger.info('getpersonshealthpackage', logrequest, httpoutput, logmessage)
    # requestlogger.info('getAllInvoiceList;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def getenrollmentsforappagentwise(request):

    data = json.loads(request.body)
    logmessage = []
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error("getenrollmentsforappagentwise", data, "Error authenticating person", logmessage)
        return HttpResponse("Error authenticating person", status=401)

    try:
        start_date = data[ServerConstants.STARTDATE]
        startdate = datetime.strptime(start_date, '%d-%m-%Y').replace(hour=0, minute=0, second=0)
    except KeyError:
        startdate = datetime(1,2,1)
    try:
        end_date = data[ServerConstants.ENDDATE]
        enddate = datetime.strptime(end_date, '%d-%m-%Y').replace(hour=23, minute=59, second=59)
    except KeyError:
        enddate = datetime.now().date()

    try:
        appstr = data[ServerConstants.APPSTR]
    except:
        appstr = None

    if appstr == None:
        backendlogger.error('getenrollmentsforappagentwise', data, 'Please specify atleast one app', logmessage)
        return HttpResponse("Please specify atleast one app",status=403)

    logmessage.append('getenrollmentsforappagentwise ForDates:%s,%s'%(str(startdate),str(enddate)))
    # requestlogger.info('getTestDataForDateRangeAndApp ForDates:%s,%s'%(str(startdate),str(enddate)))

    apparray = appstr.split(',')

    agentlist = []

    for appuid in apparray:
        mapping = PackageSalesAgentAppMapping.objects.filter(appdetails__appuid=appuid)
        for thismapping in mapping:
            agentlist.append(thismapping.salesagent)

    agentlist = list(set(agentlist))

    jsondata = list()
    for agent in agentlist:
        enrolldata = HealthPackageEnrollment.objects.filter(enrolldate__range=(startdate, enddate), salesagent=agent)
        enrolldateaggregation = list( enrolldata.extra({'date_created': "date(enrolldate)"}).values('date_created').annotate(created_count=Count('enrollid')))
        enrollcount = enrolldata.count()
        enrolldata = {ServerConstants.TOTALCOUNT: enrollcount, ServerConstants.COUNTLIST: enrolldateaggregation}
        jsondata.append({ServerConstants.NAME: agent.name,ServerConstants.DATA: enrolldata})

    httpoutput = utils.successJson({ServerConstants.ALLITEMARRAY: jsondata})
    backendlogger.info('getenrollmentsforappagentwise', data, httpoutput, logmessage)
    # requestlogger.info('getAllTestCountForDateRangeAndAppResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def download_enrollmentreceipt(request):
    requestlogger.info("download_enrollmentreceipt")
    data=dict()
    logmessage = []
    try:
        enrolluid_req = request.GET[ServerConstants.ENROLLUID]
    except Exception as e:
        backendlogger.error('download_enrollmentreceipt', data, str(e), logmessage)
        return HttpResponse("ENROLLMENT UID not sent", status=403)

    try:
        healthpackageenrollment = HealthPackageEnrollment.objects.get(enrolluid = enrolluid_req)
    except Exception as e:
        backendlogger.error('download_enrollmentreceipt', data, str(e), logmessage)
        return HttpResponse("ENROLLMENT not found for uid", status=403)

    css_file = "healthpackagereceipt.css"
    report_template = "healthpackagereceipt.html"

    # requestlogger.info('receiptdata:%s;to:%s'%(receiptdata,recipient))
    css = os.path.join(settings.CSS_DIR, css_file)
    filename = enrolluid_req+".pdf"
    receiptpath = os.path.join(settings.STATIC_ROOT,settings.REPORTS_DIR, filename)

    receipttemplate = loader.get_template(report_template)
    receiptdata = dict()
    receiptdata["reporticons"] = settings.REPORT_ICONS + "/"
    receiptdata["reporticonspartner"] =  settings.REPORT_ICONS + "/partner/"
    receiptdata["partner"]=healthpackageenrollment.appdetails.partner

    invoicedetails = list()
    for row in json.loads(healthpackageenrollment.healthpackageinvoice.get().invoicedetailsarray) :
        requestlogger.info("Row:" + str(row))
        invoicedetails.append({"name":row['description'].split("|")[0], "validity":row['description'].split("|")[1], "amount": row['amount']})

    receiptdata["invoicedetailsarray"]   = invoicedetails

    filledreport = receipttemplate.render(receiptdata)
    #logmessage.append("filledreport:==="+filledreport)
    requestlogger.info("filledreceipt:==="+filledreport)

    options = {
        'page-size': 'A4',
        'encoding': "UTF-8",
    }

    pdfkit.from_string(filledreport, receiptpath, css=css, options=options)
    with open(receiptpath, 'rb') as pdf:
        response = HttpResponse(pdf.read(), content_type='application/pdf')
        response['Content-Disposition'] = 'inline;filename=enrollment_receipt_'+filename
    pdf.close()
    # os.remove(reportpath)  # remove the locally created pdf file.
    return response  # returns the response.


@csrf_exempt
def gethealthpackagesforapp(request):

    data = json.loads(request.body)
    logmessage = []

    try:
        appidid_req = data[ServerConstants.APPUID]
        appdetails = AppDetails.objects.get(appuid=appidid_req)
    except KeyError:
        backendlogger.error('gethealthpackagesforapp', data, 'Appid  not found/valid request', logmessage)
        return HttpResponse("Appuid not found/valid in request", status=404)

    healthpackages = HealthPackage.objects.filter(appdetails = appdetails)
    healthpackageslist = list()

    for thishealthpackage in healthpackages:
        healthpackageslist.append({
            ServerConstants.PACKAGENAME: thishealthpackage.packagename,
            ServerConstants.PACKAGEID: thishealthpackage.packageid,
            ServerConstants.PACKAGECODE: thishealthpackage.packagecode,
            ServerConstants.PACKAGEFEE: thishealthpackage.packagefee,
            ServerConstants.PACKAGEDURATION: thishealthpackage.packageduration,
            ServerConstants.PACKAGETYPE: thishealthpackage.packagetype,
            ServerConstants.PACKAGEDESCRIPTION: thishealthpackage.packagedescription,
            ServerConstants.PACKAGEDETAILS: thishealthpackage.packagedetails,
        })

    httpoutput = utils.successJson({ServerConstants.PACKAGELIST: healthpackageslist})
    backendlogger.info('gethealthpackagesforapp', data, httpoutput, logmessage)
    # requestlogger.info('getAllTestCountForDateRangeAndAppResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


