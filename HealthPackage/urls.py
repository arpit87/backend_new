from django.conf.urls import url

from HealthPackage import views

urlpatterns = [
    #healthpackage
    url(r'^enrollforhealthpackage/', views.enrollforhealthpackage, name='enrollforhealthpackage'),
    url(r'^checkenrollmentstatusforhealthpackage/', views.checkenrollmentstatusforhealthpackage, name='checkenrollmentstatusforhealthpackage'),
    url(r'^gethealthpackageenrollmentsbyagent/', views.gethealthpackageenrollmentsbyagent, name='gethealthpackageenrollmentsbyagent'),
    url(r'^getpersonsregisteredbyagent/', views.getpersonsregisteredbyagent, name='getpersonsregisteredbyagent'),
    url(r'^getpersonshealthpackage/', views.getpersonshealthpackage, name='getpersonshealthpackage'),
    url(r'^getenrollmentsforappagentwise/', views.getenrollmentsforappagentwise, name='getenrollmentsforappagentwise'),
    url(r'^download_enrollmentreceipt/', views.download_enrollmentreceipt, name='download_enrollmentreceipt'),

    url(r'^gethealthpackagesforapp/', views.gethealthpackagesforapp, name='gethealthpackagesforapp'),

]