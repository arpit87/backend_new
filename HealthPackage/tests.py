import datetime
import json
import uuid
from datetime import timedelta
import pylibmc
import requests
from django.conf import settings
from django.contrib.auth.models import Group, User
import backend_new.config
from HealthPackage.models import (HealthPackage, HealthPackageEnrollment, PackageSalesAgentAppMapping,
                                  PackageSalesAgentHealthPackageMapping, PackageSalesAgentRegisterPersonMapping)
from Doctor.models import (DoctorAppMapping, AppDetails, DoctorDetails,
                          SkillNumToGroup)
from Payment.models import HealthPackageInvoiceDetails, HealthPackageTransactionDetails
from Person.models import PersonDetails
from backend_new import config
from backend_new.Constants import (FileStorageConstants, FileTypeConstants,  ServerConstants)
from django.test import TestCase

class HealthPackageAPITestCase(TestCase):
     multi_db = True

     def setUp(self):
        backend_new.config.ISTESTMODE = True

        today = datetime.datetime.now()
        djangouser = User.objects.create_user(username="person9769465241",password="abc")
        djangouser1 = User.objects.create_user(username="person9769465242",password="abc")
        usernew=PersonDetails.objects.create(djangouser=djangouser, personuid=10, name='arpit', phone="9769465241", gender="M", dob_year=1987, email="arpit87@gmail.com", appuid=80)
        usernew1=PersonDetails.objects.create(djangouser=djangouser1, personuid=2, name='arpit1', phone="9769465242", gender="M", dob_year=1987, email="arpit871@gmail.com", appuid=80)
        usernew2=PersonDetails.objects.create(djangouser=djangouser1, personuid=5, name='arpit5', phone="9769465242", gender="M", dob_year=1987, email="arpit871@gmail.com", appuid=80)
        movernew = DoctorDetails.objects.create(doctoruid=1,name="arpitnew",phone="978637272",skillstr="0,1,2,5",
                                               docs="testdoc1,testdoc2",
                                               servicesoffered = "1,2,3")
        appdetails = AppDetails.objects.create(appuid=80, applocation="IOC", apptag="IOC-80", apptype=1,
                                             appdescription="IOC")
        movernew1 = DoctorDetails.objects.create(doctoruid=2,name="hirennew",phone="9167636253",skillstr="0,5",
                                               docs="testdoc1,testdoc2", servicesoffered = "1,2,3")
        newpackage = HealthPackage.objects.create(packagename = "Weight Loss",packagecode="WL-MOT-1",description = "This weight loss program condected by motilal",
                                                  packageduration = 30, packagefee = "500", packageid=1)
        packageone = HealthPackage.objects.create(packagename = "Fitness",packagecode="FIT-MOT-1",description = "This Fitness program condected by IOC",
                                                  packageduration = 80,  packagefee = "700", packageid=2)
        transaction = HealthPackageTransactionDetails.objects.create(person=usernew, transactionid="123", transactionamount=200, transactionstatus=1, transactiontype=0, transactiondescription="cash")
        healthpackageinvoice = HealthPackageInvoiceDetails.objects.create(person=usernew2, appdetails=appdetails,invoiceamount=200,paymentdone=True,transaction=transaction,packagesalesagent=usernew1)
        HealthPackageEnrollment.objects.create(person = usernew, healthpackage = newpackage, invoice = healthpackageinvoice,
                                                    enrolldate = datetime.datetime.now(),
                                                    startdate = datetime.datetime.now(),
                                                    enddate = today+datetime.timedelta(days=30))

        HealthPackageEnrollment.objects.create(person = usernew1, healthpackage = newpackage,  invoice = healthpackageinvoice,
                                                    enrolldate = datetime.datetime.now(),
                                                    startdate = today+datetime.timedelta(days=5),
                                                    enddate = today+datetime.timedelta(days=30))

        PackageSalesAgentRegisterPersonMapping.objects.create(salesagent = usernew1,person=usernew1)
        PackageSalesAgentRegisterPersonMapping.objects.create(salesagent = usernew1,person=usernew)
        PackageSalesAgentRegisterPersonMapping.objects.create(salesagent = usernew1,person=usernew2)

        appdetails = AppDetails.objects.create(appuid=1, applocation="IOC", apptag="IOC-1", apptype=1, appdescription="IOC")
        DoctorAppMapping.objects.create(doctor = movernew1, appdetails = appdetails)

        appdetails1 = AppDetails.objects.create(appuid=11, applocation="MOT", apptag="MOT-11", apptype=1, appdescription="MOT")

        Group.objects.create(name="admingroup")
        Group.objects.create(name="persongroup")
        packagesalesagentgroup = Group.objects.create(name="packagesalesagentgroup")
        Group.objects.create(name="doctorgroup")

        packagesalesagentgroup.user_set.add(djangouser)
        packagesalesagentgroup.user_set.add(djangouser1)

     def test_createPersonByAgent(self):
         cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
         otp = config.BYPASSOTP
         cache.set("9876534123" + "_otp", otp, 10)
         input = json.dumps({ServerConstants.USERNAME: 'Arpit',
                             ServerConstants.PHONE: '919876534123',
                             ServerConstants.AGENT_ID: 10,
                             ServerConstants.OTP: config.BYPASSOTP})
         response = self.client.post('/Person/createPerson/', input, ServerConstants.RESPONSE_JSON_TYPE)
         self.assertEqual(response.status_code, 200)
         responsejson = json.loads(response.content)
         personuid = responsejson["body"][ServerConstants.PERSONUID]
         try:
             PackageSalesAgentRegisterPersonMapping.objects.get(salesagent__personuid=10,person__personuid=personuid)
         except:
             self.fail("agent person not mapped")

     def test_checkenrollmentstatusforhealthpackage(self):
         input = dict({ServerConstants.PERSONUID: 10,
                       ServerConstants.PACKAGEID:1})
         response = self.client.get('/HealthPackage/checkenrollmentstatusforhealthpackage/', input)
         print(response)
         self.assertEqual(response.status_code, 200)

     def test_enrollforhealthpackage(self):
         input = dict({ServerConstants.PERSONUID: 5,
                       ServerConstants.PACKAGEID: 1})
         response = self.client.get('/HealthPackage/checkenrollmentstatusforhealthpackage/', input)
         self.assertEqual(response.status_code, 200)
         responsejson = json.loads(response.content)
         status = responsejson["body"][ServerConstants.STATUS]
         if status != 1:
             transactionid = str(uuid.uuid4())
             maketransactioninput = json.dumps({ServerConstants.TRANSACTIONID: transactionid,
                                 ServerConstants.PERSONUID: 2,
                                                ServerConstants.TRANSACTIONAMOUNT: 100,
                                                ServerConstants.TRANSACTIONTYPE: 0,
                                                ServerConstants.TRANSACTIONSTATUS: 1,
                                                })
             response = self.client.post('/Payment/makeTransaction/', maketransactioninput, ServerConstants.RESPONSE_JSON_TYPE)
             self.assertEqual(response.status_code, 200)
             generatehealthpackageinvoiceinput = json.dumps({ServerConstants.PACKAGEID: 1,
                                                ServerConstants.APPUID: 80,
                                                ServerConstants.PERSONUID: 5,
                                                ServerConstants.INVOICEAMOUNT: 100,
                                                ServerConstants.TRANSACTIONID: transactionid,
                                                ServerConstants.AGENT_ID: 10,
                                                })
             response = self.client.post('/Payment/generateHealthPackageInvoice/', generatehealthpackageinvoiceinput,
                                         ServerConstants.RESPONSE_JSON_TYPE)
             self.assertEqual(response.status_code, 200)
             responsejson = json.loads(response.content)
             invoicenum = responsejson["body"][ServerConstants.INVOICENUM]
             enrollforhealhpackageinput = json.dumps({ServerConstants.PACKAGEID: 1,
                                                             ServerConstants.PERSONUID: 5,
                                                             ServerConstants.INVOICENUM: invoicenum,

                                                             })
             response = self.client.post('/HealthPackage/enrollforhealthpackage/', enrollforhealhpackageinput,
                                         ServerConstants.RESPONSE_JSON_TYPE)
             self.assertEqual(response.status_code, 200)

     def test_gethealthpackageenrollmentsbyagent(self):
         input = dict({ServerConstants.AGENT_ID: 2})
         response = self.client.get('/HealthPackage/gethealthpackageenrollmentsbyagent/', input)
         print(response)
         self.assertEqual(response.status_code, 200)

     def test_getpersonsregisteredbyagent(self):
         input = dict({ServerConstants.AGENT_ID: 2})
         response = self.client.get('/HealthPackage/getpersonsregisteredbyagent/', input)
         print(response)
         self.assertEqual(response.status_code, 200)