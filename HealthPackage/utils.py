from HealthCase.models import  HealthCase , commentshealthcase
from Payment.models import HealthCaseInvoiceDetails
from Platform import backendlogger
import pylibmc
from Platform.uid_code_converter import encode_id, decode_uidcode
import base64
from UrlShortner.views import create_short_url
from Communications.smstemplates import getprescriptiontemplate , getdoctorresponsetemplate , getpatientresponsetemplate, \
                                        getnewhealthcasepatienttemplate
import Communications.utils
from backend_new.Constants import ServerConstants, FileTypeDIRStorageMap, FileTypeConstants, PreferredLanguageConstant
from Platform import utils
import json

def takeactiononpackageenrollment(healthpackageenrollment):
    data = {}
    logmessage = []
    if healthpackageenrollment.healthpackage.packagetag.startswith("covid"):
        person = healthpackageenrollment.person
        problem = healthpackageenrollment.healthpackage.packagename
        appdetails = healthpackageenrollment.appdetails
        location = "CovidCareApp"
        origin = "https://covidcareathome.in"
        #create new health case
        try:
            newhealthcase = HealthCase.objects.create(person=person, problem=problem, appdetails=appdetails, location=location, tags="covid" )
            person.appsinteracted.add(appdetails)

            # settoken of 15 days for this url to be valid
            appsessiontoken = utils.getRandomCharacters(10)
            cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
            cache.set(key=appsessiontoken, val=person.djangouser.username, time=60 * 60 * 24 * 15)

            healthcaseid = encode_id(newhealthcase.id)
            urlparamstr = "healthcaseid=" + healthcaseid + "&role=patient&uid=" + str(
                person.personuid) + "&appsessiontoken=" + appsessiontoken
            urlparamsencrypted = str(
                base64.urlsafe_b64encode((''.join(chr(ord(a) + 10) for a in urlparamstr)).encode("utf-8")), "utf-8")
            healthcaseurl = origin + "/healthcasetimeline?token=" + urlparamsencrypted
            shorturl = create_short_url(healthcaseurl)
            newhealthcase.shorturlpatient = shorturl
            newhealthcase.healthcaseid = healthcaseid
            newhealthcase.save()

            Communications.utils.sendSMS(getnewhealthcasepatienttemplate(newhealthcase.person.preferredlanguage, shorturl),
                                         person.phone, appdetails.appsmssenderid,
                                         newhealthcase.person.preferredlanguage != PreferredLanguageConstant.ENGLISH)

            invoicedetailsarray = list()
            description = person.name + "--" + healthpackageenrollment.healthpackage.packagename
            invoicedetailsarray.append(
                {"description": description, "amount": healthpackageenrollment.healthpackage.packagefee})
            # 40% given to doctor
            HealthCaseInvoiceDetails.objects.create(healthcase=newhealthcase,
                                                                        appdetails=appdetails,
                                                                        invoicedetailsarray=json.dumps(invoicedetailsarray),
                                                                        invoiceamount=(healthpackageenrollment.healthpackage.packagefee*0.4),
                                                                        healthpackageenrollment=healthpackageenrollment,
                                                                        paymentdone=True)

            # put a health case comment also
            commentdict = dict({"Welcome":"Communication with doctor will happen here. You can type some query in comment box, record a video to send to doctor or upload any reports"})
            commentshealthcase.objects.create(healthcase=newhealthcase,
                                              commenttext=json.dumps(commentdict),
                                              commenteruid=newhealthcase.person.personuid, commenterrole="patient")
        except Exception as e:
            backendlogger.error('takeactiononpackageenrollment', data, str(e), logmessage)


