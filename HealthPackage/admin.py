from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin,ImportExportMixin
from HealthPackage.models import HealthPackage,HealthPackageEnrollment,PackageSalesAgentHealthPackageMapping,PackageSalesAgentAppMapping,PackageSalesAgentRegisterPersonMapping

####################33
class HealthPackageResource(resources.ModelResource):
    class Meta:
        model = HealthPackage

class HealthPackageModelAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ('packageid', 'packagename', 'packagedescription', 'packageduration', 'packagefee','packagetype','packagetag')
    resource_class = HealthPackageResource

admin.site.register(HealthPackage, HealthPackageModelAdmin)

####################3

class HealthPackageEnrollmentResource(resources.ModelResource):
    class Meta:
        model = HealthPackageEnrollment

class HealthPackageEnrollmentModelAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ('enrolluid', 'person', 'healthpackage', 'startdate', 'enddate', "enrolldate")
    resource_class = HealthPackageEnrollmentResource

admin.site.register(HealthPackageEnrollment, HealthPackageEnrollmentModelAdmin)

####################3

class PackageSalesAgentHealthPackageMappingResource(resources.ModelResource):
    class Meta:
        model = PackageSalesAgentHealthPackageMapping

class PackageSalesAgentHealthPackageMappingModelAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ('salesagent', 'healthpackage')
    resource_class = PackageSalesAgentHealthPackageMappingResource

admin.site.register(PackageSalesAgentHealthPackageMapping, PackageSalesAgentHealthPackageMappingModelAdmin)

#################3333

class PackageSalesAgentAppMappingResource(resources.ModelResource):
    class Meta:
        model = PackageSalesAgentAppMapping

class PackageSalesAgentAppMappingModelAdmin(ImportExportMixin,admin.ModelAdmin):
    list_display = ('salesagent','appdetails')
    resource_class = PackageSalesAgentHealthPackageMappingResource

admin.site.register(PackageSalesAgentAppMapping,PackageSalesAgentAppMappingModelAdmin)

#################3333

class PackageSalesAgentRegisterPersonMappingResource(resources.ModelResource):
    class Meta:
        model = PackageSalesAgentRegisterPersonMapping

class PackageSalesAgentRegisterPersonMappingModelAdmin(ImportExportMixin,admin.ModelAdmin):
    list_display = ('salesagent','person')
    resource_class = PackageSalesAgentRegisterPersonMappingResource

admin.site.register(PackageSalesAgentRegisterPersonMapping,PackageSalesAgentRegisterPersonMappingModelAdmin)
