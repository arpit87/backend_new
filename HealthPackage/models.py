from django.db import models

import backend_new.settings
from Doctor.models import DoctorDetails,AppDetails
from Person.models import PersonDetails
from django.utils import timezone
import datetime
import pytz
# Create your models here.

class HealthPackage(models.Model):
    INDIVIDUAL = 1
    FAMILY = 2
    CORPORATE = 3
    TYPES = ((INDIVIDUAL, 'Individual'), (FAMILY, 'Family'), (CORPORATE, 'Corporate'))

    packageid = models.AutoField(primary_key=True)
    packagecode = models.CharField(max_length=50,null=True)
    packagename = models.CharField(max_length=50,null=False,unique=True)
    packagedescription = models.CharField(max_length=512,null=True)
    packagedetails = models.TextField(null=True)
    packageduration = models.IntegerField(default=0,null=False)
    packagefee = models.IntegerField(default=0,null=False)
    packagetype = models.IntegerField(choices=TYPES, default=INDIVIDUAL)
    packagetag = models.CharField(max_length=50, default="")
    appdetails = models.ManyToManyField(AppDetails)

    def __str__(self):
       return str(self.packageid)


class HealthPackageEnrollment(models.Model):
    ENROLLED = 1
    EXPIRED = -1
    PENDING = 0

    STATUS = ((ENROLLED, 'ENROLLED'), (EXPIRED, 'EXPIRED'), (PENDING, 'PENDING'))

    enrollid = models.AutoField(primary_key=True)
    enrolluid = models.CharField(max_length=10, db_index=True,null=True)
    appdetails = models.ForeignKey(AppDetails, on_delete=models.CASCADE)
    person = models.ForeignKey(PersonDetails,related_name='enrolledperson', on_delete=models.CASCADE)
    healthpackage = models.ForeignKey(HealthPackage, on_delete=models.CASCADE)
    startdate = models.DateTimeField()
    enddate = models.DateTimeField()
    enrolldate = models.DateTimeField(auto_now_add=True)
    salesagent = models.ForeignKey(PersonDetails, related_name='healthpackage', on_delete=models.SET_NULL, null=True)
    enrollment_status = models.IntegerField(choices = STATUS, default=PENDING)

    def getstatus(self):
        if self.enrollment_status == self.PENDING:
            return self.PENDING
        timezone = pytz.timezone(backend_new.settings.TIME_ZONE)
        if timezone.localize(datetime.datetime.today()) >= self.startdate and timezone.localize(datetime.datetime.today()) <= self.enddate:
            self.enrollment_status = self.ENROLLED
            self.save()
            return self.ENROLLED
        else:
            self.enrollment_status = self.EXPIRED
            self.save()
            return self.EXPIRED

    def __str__(self):
       return str(self.enrollid)


class PackageSalesAgentHealthPackageMapping(models.Model):
    salesagent = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    healthpackage = models.ForeignKey(HealthPackage, on_delete=models.CASCADE)

    def __str__(self):
       return str(self.id)

    class Meta:
        unique_together = ('salesagent', 'healthpackage')


class PackageSalesAgentAppMapping(models.Model):
    salesagent = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    appdetails = models.ForeignKey(AppDetails, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id)


    class Meta:
        unique_together = ('salesagent', 'appdetails')


class PackageSalesAgentRegisterPersonMapping(models.Model):
    salesagent = models.ForeignKey(PersonDetails, related_name='salesagent',on_delete=models.CASCADE)
    person = models.ForeignKey(PersonDetails,related_name='registeredperson', on_delete=models.CASCADE)
    datetime_generated = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.id)