import asyncio
import json

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer, AsyncWebsocketConsumer
from django.core.exceptions import ObjectDoesNotExist
from firebase_admin._messaging_utils import UnregisteredError
from requests import HTTPError
from Platform.fcm_message import send_to_token
from Platform.models import DoctorFCMTokenMapping
from QueueManagement.models import TVAppDetails
from Doctor.models import AppDetails
from channels.db import database_sync_to_async
from QueueManagement.views import calc_queue_number
from backend_new import config
from backend_new.Constants import ServerConstants
from firebase_admin._messaging_utils import UnregisteredError
import pylibmc
import logging
from Person.models import PersonDetails

websocketlogger = logging.getLogger('websocket')

class PatientVitalsConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        websocketlogger.info("connected")
        personuid = self.scope['url_route']['kwargs']['personuid']
        websocketlogger.info('Connecting to personuid:'+personuid)
        person = await self.get_person(personuid)
        if person is None:
            await self.send(text_data=json.dumps(
                {ServerConstants.MESSAGE_TYPE: 'connection_message', 'status': 'person not found'}
            ))
            await self.disconnect()
        else:
            persontracking = "person_" + personuid + "_tracking"
            websocketlogger.info('adding to group:{}, channel:{}'.format(persontracking,self.channel_name))
            await self.channel_layer.group_add( persontracking, self.channel_name )
            await self.accept()
            await self.send(text_data=json.dumps(
                {ServerConstants.MESSAGE_TYPE: 'connection_message', 'status': 'connected', 'personuid':personuid }
            ))

    @database_sync_to_async
    def get_person(self,personuid):
        person = None
        try:
            person =  PersonDetails.objects.get(personuid=personuid)
        except:
            pass
        return person

    async def receive(self, **kwargs):
        websocketlogger.info("receive")
        text_data_json = json.loads(kwargs['text_data'])
        personuid = self.scope['url_route']['kwargs']['personuid']
        websocketlogger.info('Got message from personuid:{}'.format(personuid))
        patienttrackinggroup = "person_" + personuid + "_tracking"
        msg = {"type": "group.message", "command": "vitalspacket", "data": text_data_json, "sender_channel_name" : self.channel_name }
        await self.channel_layer.group_send(patienttrackinggroup, msg)

    async def disconnect(self, close_code):
        websocketlogger.info("disconnect")
        personuid = self.scope['url_route']['kwargs']['personuid']
        websocketlogger.info('disconnecting from person:' + personuid)
        person = await self.get_person(personuid)
        if person is not None:
            websocketlogger.info('removing from group:{}, channel:{}'.format("person_" + personuid + "_tracking", self.channel_name))
            await self.channel_layer.group_discard(
                "person_" + personuid + "_tracking",
                self.channel_name
            )

    async def group_message(self, message):
        websocketlogger.info("got group msg:")
        if message['command'] == "vitalspacket" and self.channel_name != message['sender_channel_name']:
            await self.send(text_data=json.dumps(
                {ServerConstants.MESSAGE_TYPE: 'vitalspacket', ServerConstants.MACHINESDATA : message['data']}
            ))
            websocketlogger.info("sent to channel:" + self.channel_name)
        else:
            websocketlogger.info("Not sent to channel:"+self.channel_name)
