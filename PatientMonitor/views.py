import logging

from asgiref.sync import async_to_sync
from django.shortcuts import render
from django.utils.safestring import mark_safe
import json
from datetime import datetime
from django.http import HttpResponse

from Person.models import PersonDetails
from Doctor.models import AppDetails
from backend_new.Constants import ServerConstants

from Platform.views import authenticateURL
from Platform import backendlogger, utils
import Communications.utils
from PatientMonitor.models import PatientVitalsSnapShot
from django.views.decorators.csrf import csrf_exempt

requestlogger = logging.getLogger('apirequests')


@csrf_exempt
def getPersonSnapShotsForDateRange(request):
    # requestlogger.info('================setPatientSnapshot=====================')
    data = json.loads(request.body)
    logmessage = []
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error("setPatientSnapshot", data, "Error authenticating person", logmessage)
        return HttpResponse("Error authenticating person", status=401)

    personuid_req = None

    try:
        personuid_req = request.GET[ServerConstants.PERSONUID]
        logmessage.append('getPersonSnapShotsForDateRange:personuid;%s' % (str(personuid_req)))
        # requestlogger.info('getPersonSnapShotsForDateRange:personuid;%s'%(str(personuid_req)))
    except KeyError:
        backendlogger.error('getPersonSnapShotsForDateRange', logrequest, 'reportid not sent in request', logmessage)
        # requestlogger.info('getPersonSnapShotsForDateRange:reportid/personuid not sent in request')
        return HttpResponse("reportid not sent in request", status=403)

    try:
        startdate = request.GET[ServerConstants.STARTDATE]
    except KeyError:
        startdate = datetime(1,2,1)
    try:
        enddate = request.GET[ServerConstants.ENDDATE]
    except KeyError:
        enddate = datetime.now().date()


    personsnapshots = PersonDetails.objects.filter(person__personuid=personuid_req, datetime__range=(startdate,enddate))

    snapshotlist = list()

    for thisnapshot in personsnapshots:
        if thisreport.bp == None:
            bpdata = None
        else:
            bpdata = model_to_dict(thisreport.bp, fields=('systolic', 'diastolic', 'pulse'))
            age = timezone.now().year - thisreport.person.dob_year if timezone.now().year - thisreport.person.dob_year <= 40 else 'other'
            systolic_inference, diastolic_inference, user_bp_range = get_bp_inference(bpdata,
                                                                                      age)  # second param is person's age
            bpdata['systolic_inference'] = systolic_inference
            bpdata['systolic_range'] = user_bp_range['systolic_range']
            bpdata['diastolic_inference'] = diastolic_inference
            bpdata['diastolic_range'] = user_bp_range['diastolic_range']

        if thisreport.oximeter == None:
            oximeterdata = None
        else:
            oximeterdata = model_to_dict(thisreport.oximeter, fields=('pulse', 'oxygensat'))
            age_for_pulse_inference = timezone.now().year - thisreport.person.dob_year if timezone.now().year - thisreport.person.dob_year <= 15 else 'other'
            pulse_inference, pulse_range = get_pulse_inference(thisreport.oximeter.pulse, age_for_pulse_inference)
            oxygensat_inference, oxygensat_inference_range = get_oxygensat_inference(thisreport.oximeter.oxygensat)
            oximeterdata["pulse_inference"] = pulse_inference
            oximeterdata["pulse_range"] = pulse_range
            oximeterdata["oxygensat_inference"] = oxygensat_inference
            oximeterdata["oxygensat_range"] = oxygensat_inference_range

        if thisreport.temperature == None:
            temperaturedata = None
        else:
            temperaturedata = model_to_dict(thisreport.temperature, fields=('temperature'))
            temperaturedata['temperatureincelsius'] = (temperaturedata['temperature'] - 32) * 5 / 9
            temprature_inference, temprature_range = get_temperature_inference(thisreport.temperature.temperature)
            temperaturedata["temperature_inference"] = temprature_inference
            temperaturedata["temperature_range"] = temprature_range

        if thisreport.ecg == None:
            ecgdata = None
        else:
            ecgdata = dict({
                ServerConstants.ECG_IMG: thisreport.ecg.ecg_img,
            })

        snapshotlist.append({

                         ServerConstants.SNAPSHOTID: thisnapshot.snapshotid,
                         ServerConstants.DATETIME: time.mktime(timezone.localtime(thisnapshot.datetime).timetuple()),
                         ServerConstants.APPLOCATION: thisnapshot.appdetails.applocation,
                         ServerConstants.APPUID: thisnapshot.appdetails.appuid,
                         ServerConstants.BPMACHINE: bpdata,
                         ServerConstants.ECGMACHINE: ecgdata,
                         ServerConstants.OXIMETERMACHINE: oximeterdata,
                         ServerConstants.TEMPERATUREMACHINE: temperaturedata,
                         })


    jsondata = dict({ServerConstants.SNAPSHOTLIST})
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getPersonSnapShotsForDateRange', logrequest, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def setPatientSnapshot(request):
    # requestlogger.info('================setPatientSnapshot=====================')
    data = json.loads(request.body)
    logmessage = []
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error("setPatientSnapshot", data, "Error authenticating person", logmessage)
        return HttpResponse("Error authenticating person", status=401)

    try:
        personuid_req = data[ServerConstants.PERSONUID]
    except KeyError:
        backendlogger.error("setPatientSnapshot", data, "Person ID not found", logmessage)
        return HttpResponse("personuid not found", status=403)

    try:
        person = PersonDetails.objects.get(personuid=personuid_req)
    except ObjectDoesNotExist:
        backendlogger.error("setPatientSnapshot", data, "Person doesnt exist", logmessage)
        return HttpResponse("person doesnt exist", status=403)

    try:
        appuid_req = data[ServerConstants.APPUID]
    except KeyError:
        backendlogger.error("setPatientSnapshot", data, "WebApp ID not found", logmessage)
        return HttpResponse("appid not sent", status=403)

    try:
        appdetails = AppDetails.objects.get(appuid=appuid_req)
    except ObjectDoesNotExist:
        backendlogger.error("setPatientSnapshot", data, "WebApp doesnt exist", logmessage)
        return HttpResponse("kiosk doesnt exist", status=403)

    try:
        reportdata_req = data[ServerConstants.REPORTDATA]
    except KeyError:
        backendlogger.error("setPatientSnapshot", data, "Report data not found", logmessage)
        return HttpResponse("reportdata not found", status=403)

    logmessage.append('reportdata:' + json.dumps(reportdata_req))
  
    try:
        bp_req = reportdata_req[ServerConstants.BPMACHINE]
    except KeyError:
        bp_req = None

    try:
        temperature_req = reportdata_req[ServerConstants.TEMPERATUREMACHINE]
    except KeyError:
        temperature_req = None

    try:
        oximeter_req = reportdata_req[ServerConstants.OXIMETERMACHINE]
    except KeyError:
        oximeter_req = None

    try:
        ecg_req = reportdata_req[ServerConstants.ECGMACHINE]
    except KeyError:
        ecg_req = None

    snapshotdata = None
    try:
        snapshotid = data[ServerConstants.SNAPSHOTID]
        try:
            snapshotdata = PatientVitalsSnapShot.objects.get(snapshotid=snapshotid)
        except ObjectDoesNotExist:
            backendlogger.error("setPatientSnapshot", data, "Snapshot ID doesnt exist", logmessage)
            return HttpResponse("snapshotid does not exist", status=403)
    except KeyError:
        snapshotid = None
        backendlogger.error("setPatientSnapshot", data, "New report ", logmessage)

    bpobj = None
    if (bp_req):
        if snapshotdata and snapshotdata.bp:
            logmessage.append('updating bp data:' + json.dumps(bp_req))
            # requestlogger.info('updating bp data:'+json.dumps(bp_req))
            bpobj = snapshotdata.bp
            bpobj.systolic = bp_req[ServerConstants.SYSTOLIC]
            bpobj.diastolic = bp_req[ServerConstants.DIASTOLIC]
            bpobj.pulse = bp_req[ServerConstants.PULSE]
        else:
            logmessage.append('saving bp data:' + json.dumps(bp_req))
            # requestlogger.info('saving bp data:'+json.dumps(bp_req))
            bpobj = bp(person=person,
                       systolic=bp_req[ServerConstants.SYSTOLIC],
                       diastolic=bp_req[ServerConstants.DIASTOLIC],
                       pulse=bp_req[ServerConstants.PULSE],
                       appdetails=appdetails)

        bpobj.save()
        bpreco = get_bp_recommendations(model_to_dict(bpobj, fields=('systolic', 'diastolic', 'pulse')),
                                        person.get_age())
        if len(bpreco) > 0:
            recommendation_dict["Blood Pressure"] = bpreco
        # may be updating..ie. snapshotid existed but if previous data didnt have this device at all then also
        # still we reach here and not only for first time save
        if snapshotdata and not snapshotdata.bp:
            snapshotdata.bp = bpobj

    oximeterobj = None
    if (oximeter_req):
        if snapshotdata and snapshotdata.oximeter:
            logmessage.append('updating oximeter data:' + json.dumps(oximeter_req))
            # requestlogger.info('updating oximeter data:'+json.dumps(oximeter_req))
            oximeterobj = snapshotdata.oximeter
            oximeterobj.pulse = oximeter_req[ServerConstants.PULSE]
            oximeterobj.oxygensat = oximeter_req[ServerConstants.OXYGENSAT]
        else:
            logmessage.append('saving oximeter data:' + json.dumps(oximeter_req))
            # requestlogger.info('saving oximeter data:'+json.dumps(oximeter_req))
            oximeterobj = oximeter(person=person,
                                   oxygensat=oximeter_req[ServerConstants.OXYGENSAT],
                                   pulse=oximeter_req[ServerConstants.PULSE],
                                   appdetails=appdetails)

        oximeterobj.save()
        oxireco = get_pulse_recommendations(oximeterobj.pulse, person.get_age()) + get_oxygensat_recommendations(
            oximeterobj.oxygensat)
        if len(oxireco) > 0:
            recommendation_dict["Pulse and oxygen saturation"] = oxireco
        # may be updating..ie. snapshotid existed but if previous data didnt have this device at all then also
        # still we reach here and not only for first time save
        if snapshotdata and not snapshotdata.oximeter:
            snapshotdata.oximeter = oximeterobj

  
    temperatureobj = None
    if (temperature_req):
        if snapshotdata and snapshotdata.temperature:
            logmessage.append('updating temperature data:' + json.dumps(temperature_req))
            # requestlogger.info('updating temperature data:'+json.dumps(temperature_req))
            temperatureobj = snapshotdata.temperature
            temperatureobj.temperature = temperature_req[ServerConstants.TEMPERATURE]
        else:
            logmessage.append('saving temperature data:' + json.dumps(temperature_req))
            # requestlogger.info('saving temperature data:'+json.dumps(temperature_req))
            temperatureobj = temperature(person=person,
                                         temperature=temperature_req[ServerConstants.TEMPERATURE],
                                         appdetails=appdetails)

        temperatureobj.save()
        tempreco = get_temperature_recommendations(temperatureobj.temperature)
        if len(tempreco) > 0:
            recommendation_dict["Body Temperature"] = tempreco
        # may be updating..ie. snapshotid existed but if previous data didnt have this device at all then also
        # still we reach here and not only for first time save
        if snapshotdata and not snapshotdata.temperature:
            snapshotdata.temperature = temperatureobj

    
    ecgobj = None
    if (ecg_req):
        if snapshotdata and snapshotdata.ecg:
            logmessage.append('updating ecg ')
            # requestlogger.info('updating ecg ')
            ecgobj = snapshotdata.ecg
            ecgobj.lead1.leaddata = ecg_req[ServerConstants.LEAD1]
            ecgobj.lead2.leaddata = ecg_req[ServerConstants.LEAD2]
            ecgobj.lead3.leaddata = ecg_req[ServerConstants.LEAD3]
            ecgobj.avr.leaddata = ecg_req[ServerConstants.AVR]
            ecgobj.avl.leaddata = ecg_req[ServerConstants.AVL]
            ecgobj.avf.leaddata = ecg_req[ServerConstants.AVF]
            ecgobj.v1.leaddata = ecg_req[ServerConstants.V1]
            ecgobj.v2.leaddata = ecg_req[ServerConstants.V2]
            ecgobj.v3.leaddata = ecg_req[ServerConstants.V3]
            ecgobj.v4.leaddata = ecg_req[ServerConstants.V4]
            ecgobj.v5.leaddata = ecg_req[ServerConstants.V5]
            ecgobj.v6.leaddata = ecg_req[ServerConstants.V6]
            try:
                ecgobj.ecg_img = ecg_req[ServerConstants.ECG_IMG]
            except KeyError:
                pass
        else:
            logmessage.append('saving ecg ')
            # requestlogger.info('saving ecg ')
            lead1data = ecgdata.objects.create(leaddata=ecg_req[ServerConstants.LEAD1])
            lead2data = ecgdata.objects.create(leaddata=ecg_req[ServerConstants.LEAD2])
            lead3data = ecgdata.objects.create(leaddata=ecg_req[ServerConstants.LEAD3])
            avrdata = ecgdata.objects.create(leaddata=ecg_req[ServerConstants.AVR])
            avldata = ecgdata.objects.create(leaddata=ecg_req[ServerConstants.AVL])
            avfdata = ecgdata.objects.create(leaddata=ecg_req[ServerConstants.AVF])
            v1data = ecgdata.objects.create(leaddata=ecg_req[ServerConstants.V1])
            v2data = ecgdata.objects.create(leaddata=ecg_req[ServerConstants.V2])
            v3data = ecgdata.objects.create(leaddata=ecg_req[ServerConstants.V3])
            v4data = ecgdata.objects.create(leaddata=ecg_req[ServerConstants.V4])
            v5data = ecgdata.objects.create(leaddata=ecg_req[ServerConstants.V5])
            v6data = ecgdata.objects.create(leaddata=ecg_req[ServerConstants.V6])
            ecg_imgbase64data = ecg_req[ServerConstants.ECG_IMG]
            # save file to disk/cloud
            filestoragedir = FileTypeDIRStorageMap[FileTypeConstants.ECGLEADIMG]
            filestoragepath = os.path.join(settings.MEDIA_ROOT, filestoragedir)
            ecg_imgbase64data += "==="
            img_data = base64.decodebytes(ecg_imgbase64data.encode())
            filenametosave = str(uuid.uuid4()) + ".png"
            filenametosave = utils.getRandomId(8) + str(time.time()) + filenametosave
            fullpathondisk = ''.join([filestoragepath, '/', filenametosave])
            with open(fullpathondisk, "wb") as imagefile:
                imagefile.write(img_data)

            logmessage.append("saved local to disk")
            logmessage.append("file storage dir:" + filestoragedir)
            if filestoragedir == config.GCSIMAGEREPORTBUCKET:
                logmessage.append("saving to GCS")
                foldername = "person" + str(personuid_req)
                savedfileurl = save_uploaded_file_to_gcloud(fullpathondisk, config.GCSIMAGEREPORTBUCKET,
                                                            filenametosave, foldername)

            else:
                savedfileurl = config.DOMAIN + os.path.join(settings.STATIC_URL, settings.UPLOADEDFILES_DIR,
                                                            filestoragedir, filenametosave)
            ecgobj = ecg(person=person, lead1=lead1data,
                         lead2=lead2data,
                         lead3=lead3data,
                         avr=avrdata,
                         avl=avldata,
                         avf=avfdata,
                         v1=v1data,
                         v2=v2data,
                         v3=v3data,
                         v4=v4data,
                         v5=v5data,
                         v6=v6data,
                         ecg_img=savedfileurl,
                         appdetails=appdetails)

        ecgobj.save()
        # may be updating..ie. snapshotid existed but last save didnt have this device at all so then
        # still we reach here
        if snapshotdata and not snapshotdata.ecg:
            snapshotdata.ecg = ecgobj

    if not snapshotdata:
        try:
            logmessage.append('saving report data:')
            # requestlogger.info('saving report data:')
            snapshotdata = PatientVitalsSnapShot.objects.create(person=person, bp=bpobj, oximeter=oximeterobj,                                        
                                               temperature=temperatureobj, ecg=ecgobj, appdetails=appdetails)
            snapshotdata.snapshotid = encode_id(snapshotdata.id)
            snapshotdata.save()

        except Exception as e:
            backendlogger.error('setPatientSnapshot', data, str(e), logmessage)
            return HttpResponse("Error saving report", status=403)

    jsondata = dict({ServerConstants.SNAPSHOTID: snapshotdata.snapshotid})
    httpoutput = utils.successJson(jsondata)
    backendlogger.info("setPatientSnapshot", data, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)