from __future__ import unicode_literals
from datetime import datetime

from django.db import models
from Person.models import PersonDetails
from Doctor.models import DoctorDetails, AppDetails
from django.forms.models import model_to_dict
from backend_new.Constants import  ServerConstants
from django.contrib.auth.models import User
# Create your models here.
from BodyVitals.models import bp, oximeter, ecg, temperature

class PatientVitalsSnapShot(models.Model):
    snapshotid = models.CharField(db_index=True, max_length=20, null=True, unique=True)
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    datetime = models.DateTimeField(auto_now_add=True)
    bp = models.ForeignKey(bp, null=True, blank=True, on_delete=models.CASCADE)
    temperature = models.ForeignKey(temperature, null=True, blank=True, on_delete=models.CASCADE)
    ecg = models.ForeignKey(ecg, null=True, blank=True, on_delete=models.CASCADE)
    oximeter = models.ForeignKey(oximeter, null=True, blank=True, on_delete=models.CASCADE)
    appdetails = models.ForeignKey(AppDetails, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
      return '{}-{}'.format(self.snapshotid, self.person)

