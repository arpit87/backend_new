from PatientMonitor.models import PatientVitalsSnapShot
from django.contrib import admin


class PatientVitalsSnapShotModelAdmin(admin.ModelAdmin):
    list_display = ('snapshotid','person','datetime','ecg','bp','oximeter','temperature','appdetails')

admin.site.register(PatientVitalsSnapShot, PatientVitalsSnapShotModelAdmin)


