from django.conf.urls import url
from django.urls import path
from PatientMonitor import views

urlpatterns = [
    url(r'^getPersonSnapShotsForDateRange',views.getPersonSnapShotsForDateRange,name='getSnapShotsForDateRange'),
    url(r'^setPatientSnapshot', views.setPatientSnapshot, name='setPatientSnapshot'),
]

         