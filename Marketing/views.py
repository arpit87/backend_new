import json
import logging

import requests
from django.shortcuts import redirect
from django.core.exceptions import ObjectDoesNotExist
from django.views.decorators.csrf import csrf_exempt

from Platform.views import authenticateURL

from backend_new import  config
from Platform import backendlogger, utils
from django.http import HttpResponse
from Marketing.models import Lead , commentlead
from backend_new.Constants import ServerConstants

requestlogger = logging.getLogger('apirequests')
import re
from django.shortcuts import render
from django.utils import timezone

@csrf_exempt
def setLeadData(request):
    logmessage = []
    data = json.loads(request.body)

    logmessage.append('setLeadData;%s'%(str(data)))
    try:
        leadid_req = data[ServerConstants.LEADID]
        lead_req = Lead.objects.get(id = leadid_req)
    except:
        phone_req = data[ServerConstants.PHONE]
        lead_req = Lead.objects.create(phone = phone_req)
        leadid_req = lead_req.id

    try:
        name_req = data[ServerConstants.USERNAME]
        lead_req.username = name_req
    except:
        pass

    try:
        age_req = int(data[ServerConstants.AGE])
        dob_year_req = timezone.now().year - age_req
        lead_req.dob_year = dob_year_req
    except KeyError:
        pass

    try:
        gender_req = data[ServerConstants.GENDER]
        lead_req.gender = gender_req
    except KeyError:
        pass

    try:
        description_req = data[ServerConstants.DESCRIPTION]
        lead_req.description = description_req
    except KeyError:
        pass

    try:
        status_req = data[ServerConstants.STATUS]
        lead_req.status = status_req
    except KeyError:
        pass

    lead_req.save()

    jsondata = dict({ServerConstants.LEADID: leadid_req})

    httpoutput = utils.successJson(jsondata)
    backendlogger.info('setLeadData', data, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def putCommentOnLead(request):
    logmessage = []
    data = json.loads(request.body)
    # requestlogger.info('================generateInvoice=====================')
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('putCommentOnLead', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    logmessage.append('putCommentOnLead;%s'%(str(data)))

    try:
        leadid_req = data[ServerConstants.LEADID]
        lead_req = Lead.objects.get(leadid = leadid_req)
    except:
        backendlogger.error('putCommentOnLead', logrequest, 'LEad id not found nin request', logmessage)
        return HttpResponse("LEad id not found in request", status=404)

    try:
        commenttext_req = data[ServerConstants.COMMENTTEXT]
        commenterrole_req = data[ServerConstants.COMMENTERROLE]
        commenteruid_req = data[ServerConstants.COMMENTERUID]
    except  KeyError:
        backendlogger.error('putCommentOnLead', data, 'comment(er)text/role/id not sent', logmessage)
        return HttpResponse("comment(er)text/role/id not sent",status= 403)


    commentlead.objects.create(lead=lead_req, commenttext=commenttext_req,
                                      commenteruid=commenteruid_req, commenterrole=commenterrole_req)

    httpoutput = utils.successJson(dict())
    backendlogger.info('putcommentofflineconsult', data, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)