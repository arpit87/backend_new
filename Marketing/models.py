from django.db import models
# Create your models here.
from backend_new.Constants import PreferredLanguageConstant, LeadConstant

class Lead(models.Model):
    PREFERRED_LANGUAGE = (
        (PreferredLanguageConstant.ENGLISH, 'English'), (PreferredLanguageConstant.HINDI, 'Hindi'),
        (PreferredLanguageConstant.GUJARATI, 'Gujarati'), (PreferredLanguageConstant.MARATHI, 'Marathi'))
    LEAD_STATUS = (
        (LeadConstant.NOSTATUS, 'NOSTATUS'), (LeadConstant.OTPNOTVERIFIED, 'OTP not verified'),
        (LeadConstant.OTPVERIFIED, 'OTP Verified'), (LeadConstant.PERSONSELECTED, 'Person Selected'), (LeadConstant.PERSONREGISTERED, 'Person Registered'),
        (LeadConstant.PERSONENROLLED, 'Person Enrolled'),(LeadConstant.DISCARD, 'DISCARD'),(LeadConstant.CLOSE, 'CLOSE'))
    preferredlanguage = models.IntegerField(choices=PREFERRED_LANGUAGE, default=PreferredLanguageConstant.ENGLISH)
    username = models.CharField(max_length=255, null=True)
    phone = models.CharField(max_length=20, null=False)
    dob_year = models.IntegerField(default=2000, null=True)
    gender = models.CharField(max_length=5, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    status = models.IntegerField(choices=LEAD_STATUS, default=LeadConstant.NOSTATUS)
    description = models.CharField(max_length=255, null=True)  # what product is this lead for

    def __str__(self):
        return "{}-{}".format(self.id,self.username)

class commentlead(models.Model):
    commentid = models.AutoField(primary_key=True)
    lead = models.ForeignKey(Lead, null=False, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    commenterrole = models.CharField(max_length=20,null=False)
    commenttext = models.CharField(max_length=4056)
    commenteruid = models.CharField(max_length=10,null=True)

    def __str__(self):
       return "{}-{}"(self.lead,self.commentid)
