from Marketing.models import Lead
from django.contrib import admin


class LeadModelAdmin(admin.ModelAdmin):
    list_display = ('id','username','phone','description','status','updated_at','created_at')

admin.site.register(Lead, LeadModelAdmin)