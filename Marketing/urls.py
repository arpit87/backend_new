from django.urls import path
from django.conf.urls import url
from Marketing import views

urlpatterns = [
            url(r'^setLeadData', views.setLeadData, name='setLeadData'),
            url(r'^putCommentOnLead', views.putCommentOnLead, name='putCommentOnLead'),
]