import json
from datetime import datetime , date
from django.forms.models import model_to_dict
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.core.exceptions import ObjectDoesNotExist
import time
from django.utils import timezone
from django.http import HttpResponse
from requests import HTTPError
from firebase_admin._messaging_utils import UnregisteredError

import Communications.utils
# Create your views here.
from django.utils.datastructures import MultiValueDictKeyError
from django.views.decorators.csrf import csrf_exempt, requires_csrf_token
from django.db import IntegrityError

import Platform
from BodyVitals.models import report, prescription, imagereportdetails, rapidtest, \
    eyetest, urinetest, bp, oximeter, temperature, bloodglucose, weight
from  Platform import utils
import os
from Person.models import PersonDetails , QBDetails, DoctorPersonMapping
from Doctor.models import DoctorDetails, SkillNumToGroup, DoctorAppMapping, AppDetails
from HealthCase.models import   HealthCase,  uploadedvideo, attachedreporthealthcase,\
    commentshealthcase, uploadedaudio ,  HealthCaseConstant
from Doctor.models import Services
from Platform.fcm_message import send_to_token
from Platform.uid_code_converter import encode_id, decode_uidcode
from Platform.views import authenticateURL
from UrlShortner.models import URLDetails
from backend_new.Constants import ServerConstants, FileTypeDIRStorageMap, FileTypeConstants, PreferredLanguageConstant, \
    AppointmentStatusConstant, VitalTypeConstant
import logging
from backend_new.Constants import ServerConstants , HistoryTypeConstants
import pylibmc
from Lab.models import BookedInvestigation
import requests
from bs4 import BeautifulSoup
from Platform import backendlogger
from operator import itemgetter
from backend_new import config,Constants
import os
from django.conf import settings
import base64
from Payment.models import HealthCaseInvoiceDetails, HealthCaseTransactionDetails
from UrlShortner.views import create_short_url
from Communications.smstemplates import getprescriptiontemplate , getdoctorresponsetemplate , getpatientresponsetemplate, \
                                        getnewhealthcasepatienttemplate, getnewhealthcasedoctortemplate
from VideoConference.models import VCCodeDetails
from django.db.models import Count , Sum, Avg
from WebApp.models import WebAppDynamicLinkAnalytics

requestlogger = logging.getLogger('apirequests')


def startnewhealthcase(request):
    data = json.loads(request.body)
    logmessage = []

    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('startnewhealthcase', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        personuid_req = data[ServerConstants.PERSONUID]
        person = PersonDetails.objects.get(personuid=personuid_req)
    except (KeyError,ObjectDoesNotExist):
        backendlogger.error('startnewhealthcase', data, 'personuid not send/user not found', logmessage)
        return HttpResponse("personuid/Person not found",status= 403)

    try:
        appuid_req = data[ServerConstants.APPUID]
        appdetails = AppDetails.objects.get(appuid=appuid_req)
    except (KeyError,ObjectDoesNotExist):
        backendlogger.error('startnewhealthcase', data, 'appid not send/app not found', logmessage)
        return HttpResponse("Appid/app not found",status= 403)

    try:
        doctoruid_req = data[ServerConstants.DOCTORUID]
        doctor_req = DoctorDetails.objects.get(doctoruid=doctoruid_req)
    except (KeyError,ObjectDoesNotExist):
        doctor_req = None
        pass

    problem = data[ServerConstants.PROBLEM]

    kwargs = {}
    try:
        tags = data[ServerConstants.TAGS]
        kwargs['tags'] = tags
    except KeyError:
        pass

    try:
        location = data[ServerConstants.LOCATION]
        kwargs['location'] = location
    except KeyError:
        pass

    try:
        origin = request.headers['Origin']
        if "localhost" in origin:
            origin = config.DOMAIN
    except:
        origin = config.DOMAIN

    try:
        newhealthcase = HealthCase.objects.create(person = person, problem=problem,  appdetails=appdetails,doctor=doctor_req, **kwargs)
        person.appsinteracted.add(appdetails)
        if doctor_req: # from healthbox there is no doctor sent when case created
            newhealthcase.doctorsconsulted.add(doctor_req)
    except Exception as e: # try a second time if health case id duplicated
        backendlogger.error('startnewhealthcase', data, str(e), logmessage)
        return HttpResponse("error create healthcase", status=403)

    # settoken of 15 days for this url to be valid
    appsessiontoken = utils.getRandomCharacters(10)
    cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
    cache.set(key=appsessiontoken, val=person.djangouser.username, time=60 * 60 * 24 * 15)

    healthcaseid = encode_id(newhealthcase.id)
    urlparamstr = "healthcaseid=" + healthcaseid + "&role=patient&uid=" + str(personuid_req) + "&appsessiontoken=" + appsessiontoken
    urlparamsencrypted = str(base64.urlsafe_b64encode((''.join(chr(ord(a)+10) for a in urlparamstr)).encode("utf-8")),"utf-8")
    healthcaseurl = origin + "/healthcasetimeline?token=" + urlparamsencrypted
    requestlogger.info("urlenc:"+urlparamsencrypted)
    shorturl = create_short_url(healthcaseurl)
    newhealthcase.shorturlpatient = shorturl
    newhealthcase.healthcaseid = healthcaseid
    newhealthcase.save()

    Communications.utils.sendSMS(getnewhealthcasepatienttemplate(newhealthcase.person.preferredlanguage, shorturl),
                                 person.phone, appdetails.appsmssenderid, newhealthcase.person.preferredlanguage != PreferredLanguageConstant.ENGLISH)

    if doctor_req:
        appsessiontoken = utils.getRandomCharacters(10)
        cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
        cache.set(key=appsessiontoken, val=newhealthcase.doctor.djangouser, time=60 * 60 * 24 * 15)
        urlparamstr = "healthcaseid=" + newhealthcase.healthcaseid + "&role=doctor&uid=" + str( newhealthcase.doctor.doctoruid) + "&appsessiontoken=" + appsessiontoken
        urlparamsencrypted = str( base64.urlsafe_b64encode((''.join(chr(ord(a) + 10) for a in urlparamstr)).encode("utf-8")), "utf-8")
        healthcaseurl = origin + "/healthcasetimeline?token=" + urlparamsencrypted
        requestlogger.info("urlenc:" + urlparamsencrypted)
        # healthcaseurl = origin + "/healthcasetimeline?" + urlparamstr
        shorturl = create_short_url(healthcaseurl)
        newhealthcase.shorturldoctor = shorturl
        newhealthcase.save()

        #send fcm to doctor
        try:
            doctorfcmtoken = doctor_req.doctorfcmtokenmapping.fcmtoken
            send_to_token(doctorfcmtoken, {"command": "newhealthcase", "shorturldoctor": shorturl,
                                            "patientname": person.name,  "patientid": str(person.personid)})
        except (ObjectDoesNotExist, HTTPError, UnregisteredError):
            Communications.utils.sendSMS(getnewhealthcasedoctortemplate(newhealthcase.person.preferredlanguage, newhealthcase.person.name,
                                                 shorturl),
                                         newhealthcase.doctor.phone, appdetails.appsmssenderid,
                                         newhealthcase.person.preferredlanguage != PreferredLanguageConstant.ENGLISH)

        # make doctor patient mapping
        try:
            DoctorPersonMapping.objects.create(person = person, doctor = doctor_req)
        except IntegrityError:
            backendlogger.info('startnewhealthcase', data, 'Mapping already exists', logmessage)

    httpoutput = utils.successJson(dict({ServerConstants.HEALTHCASEID:newhealthcase.healthcaseid}))
    backendlogger.info('startnewhealthcase', data, httpoutput, logmessage)
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)

def maphealthcasetodoctor(request):
    data = json.loads(request.body)
    logmessage = []

    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('maphealthcasetodoctor', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        doctoruid_req = data[ServerConstants.DOCTORUID]
        doctor_req = DoctorDetails.objects.get(doctoruid=doctoruid_req)
    except (KeyError,ObjectDoesNotExist):
        backendlogger.error('maphealthcasetodoctor', data, 'doctoruid not send/user not found', logmessage)
        return HttpResponse("doctoruid not send/user not found", status=403)

    try:
        healthcaseid_req = data[ServerConstants.HEALTHCASEID]
        healthcase_req = HealthCase.objects.get(healthcaseid=healthcaseid_req)
    except (ObjectDoesNotExist, KeyError):
        backendlogger.error('updatehealthcasestatus', data, 'healthcaseid/corresponding consult not found', logmessage)
        return HttpResponse("healthcaseid/corresponding consult not found",status= 403)

    appsessiontoken = utils.getRandomCharacters(10)
    cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
    cache.set(key=appsessiontoken, val=doctor_req.djangouser, time=60 * 60 * 24 * 15)
    urlparamstr = "healthcaseid=" + healthcase_req.healthcaseid + "&role=doctor&uid=" + str(
        doctor_req.doctoruid) + "&appsessiontoken=" + appsessiontoken
    urlparamsencrypted = str(base64.urlsafe_b64encode((''.join(chr(ord(a) + 10) for a in urlparamstr)).encode("utf-8")),
                             "utf-8")
    try:
        origin = request.headers['Origin']
        if "localhost" in origin:
            origin = config.DOMAIN
    except:
        origin = config.DOMAIN
    healthcaseurl = origin + "/healthcasetimeline?token=" + urlparamsencrypted
    # healthcaseurl = origin + "/healthcasetimeline?" + urlparamstr
    shorturl = create_short_url(healthcaseurl)
    healthcase_req.shorturldoctor = shorturl
    healthcase_req.doctor = doctor_req
    healthcase_req.doctorsconsulted.add(doctor_req)
    healthcase_req.save()

    # send fcm to doctor
    try:
        doctorfcmtoken = doctor_req.doctorfcmtokenmapping.fcmtoken
        send_to_token(doctorfcmtoken, {"command": "newhealthcase", "shorturldoctor": shorturl,
                                       "patientname": healthcase_req.person.name, "patientid": str(healthcase_req.person.personid)})
    except (ObjectDoesNotExist, HTTPError, UnregisteredError):
        Communications.utils.sendSMS(getnewhealthcasedoctortemplate(healthcase_req.person.preferredlanguage, healthcase_req.person.name,
                                             shorturl),
                                     healthcase_req.doctor.phone, appdetails.appsmssenderid,
                                     healthcase_req.person.preferredlanguage != PreferredLanguageConstant.ENGLISH)

    # make doctor patient mapping
    try:
        DoctorPersonMapping.objects.create(person=healthcase_req.person, doctor=doctor_req)
    except IntegrityError:
        backendlogger.info('maphealthcasetodoctor', data, 'Mapping already exists', logmessage)

    jsondata = dict()
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('maphealthcasetodoctor', data, httpoutput, logmessage)
    # requestlogger.info('getHealthPackageOfDoctorResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

def getusershealthcaselist(request):
    data = json.loads(request.body)
    logmessage = []

    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getusershealthcaselist', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    personarray = []
    try:
        personuid_req = data[ServerConstants.PERSONUID]
        person = PersonDetails.objects.get(personuid=personuid_req)
        personarray.append(person)
    except (KeyError,ObjectDoesNotExist):
        try:
            phone_req = data[ServerConstants.PHONE]
            personarray = PersonDetails.objects.filter(phone=phone_req)
        except (KeyError, ObjectDoesNotExist):
            backendlogger.error('getusershealthcases', data, 'personuid/phone not send/user not found', logmessage)
            return HttpResponse("personuid/phone not found", status=403)

    # try:
    #     status_req = request.GET[ServerConstants.STATUS]
    # except KeyError:
    #     status_req = "".join([HealthCaseStatusConstants.OPEN, ',', HealthCaseStatusConstants.NEW, ',', HealthCaseStatusConstants.ONGOING_USERRESPONDED, ',', HealthCaseStatusConstants.ONGOING_DOCTORRESPONDED])

    # status_req_list = status_req.split(',')
    allhealthcase = HealthCase.objects.filter(person__in = personarray).order_by('-lastupdated')

    allhealthcaselist = list()
    for thishealthcase in allhealthcase:
        allhealthcaselist.append({
            ServerConstants.HEALTHCASEID: thishealthcase.healthcaseid,
            ServerConstants.USERNAME: thishealthcase.person.name,
            ServerConstants.PERSONUID: thishealthcase.person.personuid,
            ServerConstants.AGE: thishealthcase.person.get_age(),
            ServerConstants.GENDER: thishealthcase.person.gender,
            ServerConstants.DATETIME: thishealthcase.lastupdated,
            ServerConstants.PROBLEM:thishealthcase.problem,
            ServerConstants.LOCATION: thishealthcase.location,
            ServerConstants.STATUSCODE: thishealthcase.status,
            ServerConstants.STATUS: thishealthcase.get_status_display(),
        })

    # datetimesortedlist = sorted(allconsultslist,key=itemgetter(ServerConstants.DATETIME),reverse=True)
    # finalsortedlist = []
    # for thisstatus in status_req_list:
    #     finalsortedlist = finalsortedlist + [ x for x in datetimesortedlist if  x[ServerConstants.STATUS] == thisstatus]
    jsondata = dict({ServerConstants.HEALTHCASELIST:allhealthcaselist})
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getusershealthcaselist', data, httpoutput, logmessage)
    # requestlogger.info('getHealthPackageOfDoctorResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)

def getallhealthcasebydate(request):

    data = json.loads(request.body)
    logmessage = []

    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getallhealthcasebydate', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    kwargs = {}

    try:
        start_date = data[ServerConstants.STARTDATE]
        start = datetime.strptime(start_date, '%d-%m-%Y').replace(hour=0, minute=0, second=0)
        kwargs['lastupdated__gte'] = start
    except KeyError:
        pass

    try:
        end_date = data[ServerConstants.ENDDATE]
        end = datetime.strptime(end_date, '%d-%m-%Y').replace(hour=23, minute=59, second=59)
        kwargs['lastupdated__lte'] = end
    except KeyError:
        pass


    allhealthcases = HealthCase.objects.none()

    # for hims login we send only apptr
    try:
        appstr = data[ServerConstants.APPSTR]
        appuidarray = appstr.split(',')
        try:
            # this is the case for doctor login
            doctoruid_req = data[ServerConstants.DOCTORUID]
            for thisappuid in appuidarray:
                # get consults for each app doctor is mapped.
                # if a doctor is "hospital_doctor" on any app then he sees only his cases for that app
                # this is to show cases doctorwise for hospitals
                doctorappmapping = DoctorAppMapping.objects.filter(appdetails__appuid=thisappuid,
                                                                   doctor__doctoruid=doctoruid_req)
                for thismapping in doctorappmapping:
                    if thismapping.consultation_type == DoctorAppMapping.HOSPITAL_DOCTOR:
                        allhealthcases = allhealthcases | HealthCase.objects.filter(doctorsconsulted=thismapping.doctor,
                                                                                    appdetails=thismapping.appdetails,
                                                                                    **kwargs).order_by('-lastupdated')
                    elif thismapping.consultation_type == DoctorAppMapping.HOSPITAL_OWNER or thismapping.consultation_type == DoctorAppMapping.HIDDEN:
                        allhealthcases = allhealthcases | HealthCase.objects.filter(appdetails=thismapping.appdetails,
                                                                                    **kwargs).order_by('-lastupdated')
        except Exception as e:
            # this is the case for hims login
            allhealthcases = HealthCase.objects.filter(appdetails__appuid__in=appuidarray, **kwargs)
    except Exception as e:
        backendlogger.error('getallhealthcasebydate', data, str(e), logmessage)
        return HttpResponse("appstr not sent", status=403)



    allhealthcaselist = list()
    for thishealthcase in allhealthcases:
        try:
            lastvideo = uploadedvideo.objects.filter(healthcase=thishealthcase).order_by('-datetime')[0]
            lastvideo = lastvideo.videourl
        except:
            lastvideo = ""

        try:
            lastcomment = commentshealthcase.objects.filter(healthcase=thishealthcase).order_by('-datetime')[0]
            lastcommenttext = lastcomment.commenttext
            lastcommenterrole = lastcomment.commenterrole
        except:
            lastcommenttext = ""
            lastcommenterrole = ""

        healthcaseinvoices = HealthCaseInvoiceDetails.objects.filter(healthcase = thishealthcase)
        invoicelist = []
        for thisinvoice in healthcaseinvoices :
            invoicelist.append({ServerConstants.INVOICENUM: thisinvoice.invoicenum,
                                ServerConstants.INVOICEAMOUNT : thisinvoice.invoiceamount,
                                ServerConstants.UPDATED_AT: thisinvoice.updated_at.astimezone(
                                    timezone.get_current_timezone()).strftime('%d-%b-%y,%I:%M %p'),
                                ServerConstants.PAYMENTDONE: thisinvoice.paymentdone})

        vccodedetailslist = list()
        try:
            allvccodedetails = VCCodeDetails.objects.filter(healthcase=thishealthcase,
                                                      slotbooking__consultstatus__in=[AppointmentStatusConstant.PENDING,AppointmentStatusConstant.STARTED,AppointmentStatusConstant.DROPPED])
            for vccodedetails in allvccodedetails:
                vccodedetailsdict = {ServerConstants.VCCODE:vccodedetails.vccode,
                                     ServerConstants.VCDATE: vccodedetails.date,
                                     ServerConstants.VCTIME:vccodedetails.slotbooking.starttime.strftime("%I:%M %p"),
                                     ServerConstants.VCCODEID: vccodedetails.id,
                                     ServerConstants.ISVALID: True if vccodedetails.date == date.today() else False,
                                     ServerConstants.CONSULTSTATUS: vccodedetails.slotbooking.get_consultstatus_display()}
                vccodedetailslist.append(vccodedetailsdict)
        except Exception as e:
            logmessage.append(str(e))
            pass

        allhealthcaselist.append({
            ServerConstants.HEALTHCASEID: thishealthcase.healthcaseid,
            ServerConstants.APPUID: thishealthcase.appdetails.appuid,
            ServerConstants.DOCTORSCONSULTED:  [ doctor.name for doctor in thishealthcase.doctorsconsulted.all()],
            ServerConstants.PERSONUID:  thishealthcase.person.personuid,
            ServerConstants.DATETIME: thishealthcase.created_at,
            ServerConstants.LASTUPDATED: thishealthcase.lastupdated,
            ServerConstants.PROBLEM: thishealthcase.problem,
            ServerConstants.VIDEOURL: lastvideo,
            ServerConstants.COMMENTTEXT: lastcommenttext,
            ServerConstants.COMMENTERROLE: lastcommenterrole,
            ServerConstants.USERNAME:thishealthcase.person.name,
            ServerConstants.AGE: thishealthcase.person.get_age(),
            ServerConstants.GENDER: thishealthcase.person.gender,
            ServerConstants.PHONE: thishealthcase.person.phone,
            ServerConstants.TAGS: thishealthcase.tags,
            ServerConstants.LOCATION: thishealthcase.location,
            ServerConstants.STATUS: thishealthcase.get_status_display(),
            ServerConstants.STATUSCODE: thishealthcase.status,
            ServerConstants.INVOICELIST : invoicelist,
            ServerConstants.VCCODEDETAILSLIST: vccodedetailslist
        })

    allhealthcaselistsorted = sorted(allhealthcaselist,key=lambda x:x[ServerConstants.STATUSCODE])
    requestlogger.info(str(data))
    jsondata = dict({ServerConstants.HEALTHCASELIST:allhealthcaselistsorted})
    backendlogger.info('getallhealthcasebydate', data, jsondata, logmessage)
    httpoutput = utils.successJson(jsondata)
    # requestlogger.info('getHealthPackageOfDoctorResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)

def getvideodetails(video):
    name=""
    role=""
    profilepic=""
    id=""
    if video.doctor!=None:
        role = "doctor"
        profilepic = video.doctor.profilepic
        name = video.doctor.name
        id = video.doctor.doctoruid
    else:
        #this could be user or admin but admin switched off for now
        #djangouser = video.user.djangouser
        # if(djangouser.groups.filter(name='admingroup').exists()):
        #     #this is admin
        #     role = "Admin"
        # else:
        #     role = "Person"
        role="patient"
        profilepic = video.person.profilepic
        name = video.person.name
        id=video.person.personuid
    return video.videourl , name, role , profilepic, id

def getaudiodetails(audio):
    name=""
    role=""
    profilepic=""
    id=""
    if audio.doctor!=None:
        role = "doctor"
        profilepic = audio.doctor.profilepic
        name = audio.doctor.name
        id = audio.doctor.doctoruid
    else:
        #this could be user or admin but admin switched off for now
        #djangouser = audio.user.djangouser
        # if(djangouser.groups.filter(name='admingroup').exists()):
        #     #this is admin
        #     role = "Admin"
        # else:
        #     role = "Person"
        role="patient"
        profilepic = audio.person.profilepic
        name = audio.person.name
        id=audio.person.personuid
    return audio.audiourl , name, role , profilepic, id


def gethealthcasedetails(healthcaseid):
    videolist = uploadedvideo.objects.filter(healthcase__healthcaseid=healthcaseid)
    count_req = videolist.count()
    if count_req  == 0:
        defaultvideo = config.DOMAIN+os.path.join(settings.STATIC_URL,settings.UPLOADEDFILES_DIR,
                               config.GCSCONSULTVIDEOBUCKET,"default_video.mp4")
        return count_req,defaultvideo,"","","",""
    videodetails = getvideodetails(videolist[len(videolist) - 1])
    return count_req,videodetails[0],videodetails[1],videodetails[2],videodetails[3],videodetails[4]


def updatehealthcasestatus(request):
    data = json.loads(request.body)
    logmessage = []

    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('startnewhealthcase', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        healthcaseid_req = data[ServerConstants.HEALTHCASEID]
        healthcase_req = HealthCase.objects.get(healthcaseid=healthcaseid_req)
    except (ObjectDoesNotExist, KeyError):
        backendlogger.error('updatehealthcasestatus', data, 'healthcaseid/corresponding consult not found', logmessage)
        return HttpResponse("healthcaseid/corresponding consult not found",status= 403)

    try:
        status_req = data[ServerConstants.STATUS]
        if status_req == HealthCaseConstant.REOPEN: # patient link might have expired then reopen case, create new link and send link again to patient
            # settoken of 15 days for this url to be valid
            appsessiontoken = utils.getRandomCharacters(10)
            cache = pylibmc.Client(["127.0.0.1"], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
            cache.set(key=appsessiontoken, val=healthcase_req.person.djangouser.username, time=60 * 60 * 24 * 15)

            try:
                origin = request.headers['Origin']
                if "localhost" in origin:
                    origin = config.DOMAIN
            except:
                origin = config.DOMAIN

            urlparamstr = "healthcaseid=" + healthcase_req.healthcaseid + "&role=patient&uid=" + str(
                healthcase_req.person.personuid) + "&appsessiontoken=" + appsessiontoken
            urlparamsencrypted = str(
                base64.urlsafe_b64encode((''.join(chr(ord(a) + 10) for a in urlparamstr)).encode("utf-8")), "utf-8")
            healthcaseurl = origin + "/healthcasetimeline?token=" + urlparamsencrypted
            requestlogger.info("urlenc:" + urlparamsencrypted)
            shorturl = create_short_url(healthcaseurl)
            healthcase_req.shorturlpatient = shorturl

            Communications.utils.sendSMS(getnewhealthcasepatienttemplate(healthcase_req.person.preferredlanguage, shorturl),
                                         healthcase_req.person.phone, healthcase_req.appdetails.appsmssenderid,
                                         healthcase_req.person.preferredlanguage != PreferredLanguageConstant.ENGLISH)

        healthcase_req.status= status_req
        healthcase_req.save()
    except:
        backendlogger.error('updatehealthcasestatus', data, 'status not sent',logmessage)
        return HttpResponse("status not sent", status=403)

    httpoutput = utils.successJson({})
    backendlogger.info('updatehealthcasestatus', data, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

def getvideourlsforconsultid(request):
    logmessage = []
    logrequest = {}
    for key, value in request.GET.items():
        logrequest[key] = value
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getvideourlsforconsultid', logrequest, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        consultid_req = request.GET[ServerConstants.HEALTHCASEID]
        offlineconsult_req = HealthCase.objects.get(healthcaseid=consultid_req)
    except (ObjectDoesNotExist, KeyError):
        backendlogger.error('getvideourlsforconsultid', logrequest, 'consultid/corresponding consult not found', logmessage)
        return HttpResponse("consultid/corresponding consult not found",status= 403)

    videourllist = list()
    uploadedvideolist = uploadedvideo.objects.filter(offlineconsult = offlineconsult_req)
    problem = offlineconsult_req.problem
    details = offlineconsult_req.details
    for video in uploadedvideolist:
        videodetails = getvideodetails(video)
        videourllist.append({
                                ServerConstants.VIDEOID:video.videoid,
                                ServerConstants.VIDEOURL:video.videourl,
                                ServerConstants.DATETIME:video.datetime,
                                ServerConstants.NAME:videodetails[1],
                                ServerConstants.ROLE:videodetails[2],
                                ServerConstants.ID:videodetails[4],
                                ServerConstants.PROFILEPIC: videodetails[3]
                            })

    jsondata = dict({ServerConstants.VIDEOURLLIST:videourllist , ServerConstants.PROBLEM:problem,ServerConstants.DETAILS:details})
    httpoutput = utils.successJson(jsondata)

    backendlogger.info('getvideourlsforconsultid', logrequest, httpoutput, logmessage)
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)

def attachreporthealthcase(request):
    data = json.loads(request.body)
    logmessage = []

    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('attachreporthealthcase', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        healthcaseid_req = data[ServerConstants.HEALTHCASEID]
        healthcase_req = HealthCase.objects.get(healthcaseid=healthcaseid_req)
    except (ObjectDoesNotExist, KeyError):
        backendlogger.error('attachreporthealthcase', data, 'consultid/corresponding consult not found', logmessage)
        return HttpResponse("consultid/corresponding consult not found",status= 403)


    try:
        uploaderrole_req = data[ServerConstants.UPLOADERROLE]
        uploaderuid_req = data[ServerConstants.UPLOADERUID]
    except KeyError:
        backendlogger.error('attachreporthealthcase', data, 'uploader/role/id not sent', logmessage)
        return HttpResponse("role/id not sent",status= 403)

    try:
        reportid_req = data[ServerConstants.REPORTID]
        reporttype_req = data[ServerConstants.REPORTTYPE]
        reporttitle_req = data[ServerConstants.REPORTTITLE]
        reportdescription_req = data[ServerConstants.REPORTDESCRIPTION]
    except  KeyError:
        backendlogger.error('attachreporthealthcase', data, 'reportid not sent', logmessage)
        return HttpResponse("reportid not sent",status= 403)

    try:
        attachedreporthealthcase.objects.create(healthcase=healthcase_req, attachedreportid=reportid_req,uploaderrole= uploaderrole_req, uploaderuid = uploaderuid_req,
                                                attachedreporttype=reporttype_req, attachedreporttitle=reporttitle_req,attachedreportdescription=reportdescription_req)
    except Exception as e:
        logmessage.append(e)
        backendlogger.error('attachreporthealthcase', data, 'report already attached', logmessage)
        return HttpResponse("Report already attached",status= 403)

    if uploaderrole_req.lower() == "doctor":
        # in case of helathbox the doctor is not assigned for health case so we set it first
        if healthcase_req.doctor is None:
            doctor = DoctorDetails.objects.get(doctoruid = uploaderuid_req)
            healthcase_req.doctor = doctor
            healthcase_req.save()

        try:
            urlidcode = healthcase_req.shorturlpatient.split('/')[-1]
            urlid = decode_uidcode(urlidcode)
            urldetails = URLDetails.objects.get(urlid=urlid)
            newlongurl = urldetails.longurl + "&expandprescriptionid=" + reportid_req
            newshorturl = create_short_url(newlongurl)
            Communications.utils.sendSMS(getprescriptiontemplate(healthcase_req.person.preferredlanguage, healthcase_req.doctor.name,
                                          newshorturl),
                                         healthcase_req.person.phone, None,
                                         healthcase_req.person.preferredlanguage != PreferredLanguageConstant.ENGLISH)
        except:
            pass

        healthcase_req.status = HealthCaseConstant.ONGOING_DOCTORRESPONDED
        healthcase_req.save()

        if reporttype_req == HistoryTypeConstants.PRESCRIPTIONREPORT:
            # send channels update to patient when doctor sets prescription
            # send it on gorup. doctor will receive this message too but we will ignore it there
            channel_layer = get_channel_layer()
            msg = {"type": "prescription.message", ServerConstants.MESSAGE_TYPE: ServerConstants.GOTPRESCRIPTION,
                   ServerConstants.PRESCRIPTIONID: reportid_req}
            async_to_sync(channel_layer.group_send)("vc" + str(uploaderuid_req), msg)

    httpoutput = utils.successJson(dict())
    backendlogger.info('attachreporthealthcase', data, httpoutput, logmessage)
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)

def getattachedreporthealthcase(request):
    logmessage = []
    logrequest = {}
    for key, value in request.GET.items():
        logrequest[key] = value
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getsharedreportofflineconsult', logrequest, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        healthcaseid_req = request.GET[ServerConstants.HEALTHCASEID]
        healthcase_req = HealthCase.objects.get(healthcaseid=healthcaseid_req)
    except (ObjectDoesNotExist, KeyError):
        backendlogger.error('getlatestunmappedconsultforadmin', logrequest, 'consultid/corresponding consult not found', logmessage)
        return HttpResponse("consultid/corresponding consult not found",status= 403)

    attachedreportslist = list()
    attachedreports=attachedreporthealthcase.objects.filter(healthcase=healthcase_req)

    for thisreport in attachedreports:
        attachedreportslist.append({
            ServerConstants.REPORTID:thisreport.attachedreportid,
            ServerConstants.REPORTTYPE:thisreport.attachedreporttype,
            ServerConstants.REPORTTITLE:thisreport.attachedreporttitle
        })

    jsondata = dict({ServerConstants.ATTACHEDREPORTLIST:attachedreportslist})
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getattachedreporthealthcase', logrequest, httpoutput, logmessage)
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)


def updatecommentshealthcase(request):
    data = json.loads(request.body)
    logmessage = []

    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('updatecommentshealthcase', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        commentid_req = data[ServerConstants.COMMENTID]
        comment_req = commentshealthcase.objects.get(commentid=commentid_req)
    except (ObjectDoesNotExist, KeyError):
        backendlogger.error('updatecommentshealthcase', data, 'commentid/corresponding object not found', logmessage)
        return HttpResponse("commentid/corresponding object not found", status=403)

    try:
        commenttext_req = data[ServerConstants.COMMENTTEXT]
    except  KeyError:
        backendlogger.error('updatecommentshealthcase', data, 'commenttext not sent', logmessage)
        return HttpResponse("commenttext not sent", status=403)

    comment_req.commenttext = commenttext_req
    comment_req.save()

    httpoutput = utils.successJson({})
    backendlogger.info('updatecommentshealthcase', data, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

def putcommentshealthcase(request):
    data = json.loads(request.body)
    logmessage = []

    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('putcommentofflineconsult', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        healthcaseid_req = data[ServerConstants.HEALTHCASEID]
        healthcase_req = HealthCase.objects.get(healthcaseid=healthcaseid_req)
    except (ObjectDoesNotExist, KeyError):
        backendlogger.error('putcommentofflineconsult', data, 'consultid/corresponding consult not found', logmessage)
        return HttpResponse("consultid/corresponding consult not found",status= 403)

    try:
        commenttext_req = data[ServerConstants.COMMENTTEXT]
        commenterrole_req = data[ServerConstants.COMMENTERROLE]
        commenteruid_req = data[ServerConstants.COMMENTERUID]
    except  KeyError:
        backendlogger.error('putcommentofflineconsult', data, 'comment(er)text/role/id not sent', logmessage)
        return HttpResponse("comment(er)text/role/id not sent",status= 403)


    commentshealthcase.objects.create(healthcase=healthcase_req, commenttext=commenttext_req,
                                      commenteruid=commenteruid_req, commenterrole=commenterrole_req)


    # #notify patient
    if commenterrole_req.lower() == "doctor":
        Communications.utils.sendSMS( getdoctorresponsetemplate(healthcase_req.person.preferredlanguage, healthcase_req.doctor.name, healthcase_req.shorturlpatient ),
                                      healthcase_req.person.phone, None, healthcase_req.person.preferredlanguage != PreferredLanguageConstant.ENGLISH)
        healthcase_req.status = HealthCaseConstant.ONGOING_DOCTORRESPONDED
        healthcase_req.save()
    elif healthcase_req.status is not HealthCaseConstant.NEW:
        healthcase_req.status = HealthCaseConstant.ONGOING_PERSONRESPONDED
        healthcase_req.save()
        # send fcm to doctor
        try:
            doctorfcmtoken = healthcase_req.doctor.doctorfcmtokenmapping.fcmtoken
            send_to_token(doctorfcmtoken, {"command": "patientresponse", "shorturldoctor": healthcase_req.shorturldoctor,
                                           "patientname": healthcase_req.person.name,  "patientid": str(healthcase_req.person.personid)})
        except (ObjectDoesNotExist, HTTPError, UnregisteredError):
            Communications.utils.sendSMS(getpatientresponsetemplate(healthcase_req.person.preferredlanguage, healthcase_req.person.name,
                                             healthcase_req.shorturldoctor),
                                         healthcase_req.doctor.phone, None,
                                         healthcase_req.person.preferredlanguage != PreferredLanguageConstant.ENGLISH)

    httpoutput = utils.successJson({})
    backendlogger.info('putcommentofflineconsult', data, httpoutput, logmessage)
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)

def getcommentshealthcase(request):
    logmessage = []
    logrequest = {}
    for key, value in request.GET.items():
        logrequest[key] = value
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getcommentofflineconsult', logrequest, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        consultid_req = request.GET[ServerConstants.HEALTHCASEID]
        offlineconsult_req = HealthCase.objects.get(healthcaseid=consultid_req)
    except (ObjectDoesNotExist, KeyError):
        backendlogger.error('getcommentofflineconsult', logrequest, 'consultid/corresponding consult not found', logmessage)
        return HttpResponse("consultid/corresponding consult not found",status= 403)

    commentlist = list()
    comments=commentshealthcase.objects.filter(offlineconsult=offlineconsult_req)

    for thiscomment in comments:
        role = thiscomment.commenterrole
        commenteruid=thiscomment.commenteruid
        profilepic=""
        name = ""
        if str(role).lower() == "patient" :
            try :
                user = PersonDetails.objects.get(personuid=commenteruid)
            except ObjectDoesNotExist:
                backendlogger.error('getcommentofflineconsult', logrequest, 'personuid/corresponding user not found:id:' + str(commenteruid), logmessage)
                return HttpResponse("personuid/corresponding user not found:id:"+str(commenteruid),status= 403)
            profilepic = user.profilepic
            name=user.name
        elif str(role).lower() == "doctor" :
            try:
                doctor = DoctorDetails.objects.get(doctoruid=commenteruid)
            except ObjectDoesNotExist:
                backendlogger.error('getcommentofflineconsult', logrequest, 'doctoruid/corresponding Doctor not found:id:' + str(commenteruid), logmessage)
                return HttpResponse("doctoruid/corresponding Doctor not found:id:"+str(commenteruid),status= 403)
            profilepic = doctor.profilepic
            name=doctor.name

        commentlist.append({
            ServerConstants.COMMENTID:thiscomment.commentid,
            ServerConstants.COMMENTERROLE:thiscomment.commenterrole,
            ServerConstants.COMMENTTEXT:thiscomment.commenttext,
            ServerConstants.PROFILEPIC:profilepic,
            ServerConstants.DATE:thiscomment.datetime,
            ServerConstants.COMMENTER:name
        })

    jsondata = dict({ServerConstants.COMMENTLIST:commentlist})
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getcommentofflineconsult', logrequest, httpoutput, logmessage)
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)



def getallitemshealthcase(request):

    data = json.loads(request.body)
    logmessage = []

    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getallitemshealthcase', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        healthcaseid_req = data[ServerConstants.HEALTHCASEID]
        healthcase_req = HealthCase.objects.get(healthcaseid=healthcaseid_req)
    except (ObjectDoesNotExist, KeyError):
        backendlogger.error('getallitemshealthcase', data, 'consultid/corresponding consult not found', logmessage)
        return HttpResponse("consultid/corresponding consult not found",status= 403)

    commentlist = list()
    comments=commentshealthcase.objects.filter(healthcase=healthcase_req).order_by('datetime')

    videourllist = list()
    uploadedvideolist = uploadedvideo.objects.filter(healthcase = healthcase_req).order_by('datetime')

    audiourllist = list()
    uploadedaudiolist = uploadedaudio.objects.filter(healthcase=healthcase_req).order_by('datetime')

    rapidtestlist = list()
    rapidtestsdone = rapidtest.objects.filter(healthcase=healthcase_req).order_by('datetime')

    eyetestlist = list()
    eyetestsdone = eyetest.objects.filter(healthcase=healthcase_req).order_by('datetime')

    urinetestlist = list()
    urinetestsdone = urinetest.objects.filter(healthcase=healthcase_req).order_by('datetime')

    attachedreportslist = list()
    attachedreports=attachedreporthealthcase.objects.filter(healthcase=healthcase_req).order_by('datetime')

    attachedbookedinvestiagtionlist = list()
    attachedbookedinvestiagtions = BookedInvestigation.objects.filter(healthcase=healthcase_req).order_by('booking_date')

    for thiscomment in comments:
        role = thiscomment.commenterrole
        commenteruid=thiscomment.commenteruid
        profilepic=""
        name = ""

        if str(role).lower() == "doctor" :
            try:
                doctor = DoctorDetails.objects.get(doctoruid=commenteruid)
            except ObjectDoesNotExist:
                backendlogger.error('getallitemshealthcase', data, 'doctoruid/corresponding doctor not found:id:' + commenteruid, logmessage)
                return HttpResponse("moverid/corresponding mover not found:id:"+commenteruid, status= 403)
            profilepic = doctor.profilepic
            name=doctor.name
        else:
            try :
                user = PersonDetails.objects.get(personuid=commenteruid)
            except ObjectDoesNotExist:
                backendlogger.error('getallitemshealthcase', data, 'personuid/corresponding user not found:id:' + commenteruid, logmessage)
                return HttpResponse("personuid/corresponding user not found:id:"+commenteruid, status= 403)
            profilepic = user.profilepic
            name=user.name

        commentlist.append({
            ServerConstants.DATETIME:thiscomment.datetime,
            ServerConstants.COMMENTID:thiscomment.commentid,
            ServerConstants.ROLE:thiscomment.commenterrole,
            ServerConstants.COMMENTTEXT:thiscomment.commenttext,
            ServerConstants.PROFILEPIC:profilepic,
            ServerConstants.COMMENTER:name,
            ServerConstants.ITEMTYPE : ServerConstants.COMMENT
        })

    for video in uploadedvideolist:
        videodetails = getvideodetails(video)
        videourllist.append({
                            ServerConstants.DATETIME:video.datetime,
                            ServerConstants.VIDEOID:video.videoid,
                            ServerConstants.VIDEOURL:video.videourl,
                            ServerConstants.NAME:videodetails[1],
                            ServerConstants.ROLE:videodetails[2],
                            ServerConstants.ID:videodetails[4],
                            ServerConstants.PROFILEPIC: videodetails[3],
                            ServerConstants.ITEMTYPE: ServerConstants.VIDEO
                            })

    for audio in uploadedaudiolist:
        audiodetails = getaudiodetails(audio)
        audiourllist.append({
            ServerConstants.DATETIME: audio.datetime,
            ServerConstants.AUDIOID: audio.audioid,
            ServerConstants.AUDIOURL: audio.audiourl,
            ServerConstants.NAME: audiodetails[1],
            ServerConstants.ROLE: audiodetails[2],
            ServerConstants.ID: audiodetails[4],
            ServerConstants.PROFILEPIC: audiodetails[3],
            ServerConstants.ITEMTYPE: ServerConstants.AUDIO
        })

    for thisrapidtest in rapidtestsdone:
        rapidtestlist.append({
            ServerConstants.DATETIME: thisrapidtest.datetime,
            ServerConstants.NAME: thisrapidtest.person.name,
            ServerConstants.ROLE: "patient",
            ServerConstants.REPORTDATA: thisrapidtest.reportdata,
            ServerConstants.ITEMTYPE: ServerConstants.RAPIDTESTREPORT
        })

    for thisurinetest in urinetestsdone:
        urinetestlist.append({
            ServerConstants.DATETIME: thisurinetest.datetime,
            ServerConstants.NAME: thisurinetest.person.name,
            ServerConstants.ROLE: "patient",
            ServerConstants.REPORTDATA: thisurinetest.reportdata,
            ServerConstants.ITEMTYPE: ServerConstants.URINETESTREPORT
        })

    for thiseyetest in eyetestsdone:
        eyetestlist.append({
            ServerConstants.DATETIME: thiseyetest.datetime,
            ServerConstants.NAME: thiseyetest.person.name,
            ServerConstants.ROLE: "patient",
            ServerConstants.REPORTDATA: thiseyetest.reportdata,
            ServerConstants.ITEMTYPE: ServerConstants.EYETESTREPORT
        })

    for thisbookedinvestigation in attachedbookedinvestiagtions:
        labreporturl = None
        labreportuploaderrole = None
        labreportuploadername = None
        if thisbookedinvestigation.labreport:
            labreporturl = thisbookedinvestigation.labreport.reporturl
            labreportuploaderrole = thisbookedinvestigation.labreport.uploaderrole
            uploaderperson = PersonDetails.objects.get(personuid = thisbookedinvestigation.labreport.uploaderuid)
            labreportuploadername = uploaderperson.name

        attachedbookedinvestiagtionlist.append({
            ServerConstants.LABREPORTURL : labreporturl,
            ServerConstants.UPLOADERROLE : labreportuploaderrole,
            ServerConstants.UPLOADERNAME : labreportuploadername,
            ServerConstants.INVESTIGATIONNAME : thisbookedinvestigation.investigation.investigationname,
            ServerConstants.BOOKINGID : thisbookedinvestigation.id,
            ServerConstants.MARKETPRICE : thisbookedinvestigation.investigation.marketprice,
            ServerConstants.PRICETOPERSON : thisbookedinvestigation.investigation.pricetoperson,
            ServerConstants.DATETIME : thisbookedinvestigation.booking_date,
            ServerConstants.STATUSSTR : thisbookedinvestigation.get_status_display(),
            ServerConstants.STATUS : thisbookedinvestigation.status,
            ServerConstants.ROLE : thisbookedinvestigation.uploaderrole,
            ServerConstants.NAME : thisbookedinvestigation.person.name,
            ServerConstants.REPORTTYPE:  HistoryTypeConstants.BOOKEDINVESTIGATION,
            ServerConstants.ITEMTYPE: ServerConstants.BOOKEDINVESTIGATION,
        })

    for thisreport in attachedreports:
        reportdata = dict()
        role = thisreport.uploaderrole
        if str(role).lower() == "doctor" :
            try:
                doctor = DoctorDetails.objects.get(doctoruid=thisreport.uploaderuid)
            except ObjectDoesNotExist:
                backendlogger.error('getallitemshealthcase', data, 'moverid/corresponding mover not found:id:' + str(thisreport.uploaderuid), logmessage)
                return HttpResponse("moverid/corresponding mover not found:id:"+str(thisreport.uploaderuid),status= 403)
            profilepic = doctor.profilepic
            name=doctor.name
        else:
            try :
                user = PersonDetails.objects.get(personuid=thisreport.uploaderuid)
            except ObjectDoesNotExist:
                backendlogger.error('getallitemshealthcase', data, 'personuid/corresponding user not found:id:' + str(thisreport.uploaderuid), logmessage)
                return HttpResponse("personuid/corresponding user not found:id:"+str(thisreport.uploaderuid),status= 403)
            profilepic = user.profilepic
            name=user.name

        if thisreport.attachedreporttype == HistoryTypeConstants.VITALSREPORT:
            thisvitalreport = report.objects.get(reportid=thisreport.attachedreportid)
            reportdata = thisvitalreport.getVitalsSummaryDataDict()
        elif thisreport.attachedreporttype == HistoryTypeConstants.PRESCRIPTIONREPORT:
            thisprescriptionreport = prescription.objects.get(prescriptionid=thisreport.attachedreportid)
            reportdata[ServerConstants.DOCTORNAME] = thisprescriptionreport.doctor.name
        elif thisreport.attachedreporttype == HistoryTypeConstants.IMAGEREPORT:
            thisscanreport = imagereportdetails.objects.get(imagereportid=thisreport.attachedreportid)
            reportdata[ServerConstants.IMAGEURL] = thisscanreport.reportimgurl

        attachedreportslist.append({
            ServerConstants.DATETIME:thisreport.datetime,
            ServerConstants.REPORTID:thisreport.attachedreportid,
            ServerConstants.REPORTTYPE:thisreport.attachedreporttype,
            ServerConstants.REPORTTITLE:thisreport.attachedreporttitle,
            ServerConstants.REPORTDATA: reportdata,
            ServerConstants.ITEMTYPE: ServerConstants.ATTACHEDREPORT,
            ServerConstants.ROLE: role,
            ServerConstants.NAME: name
        })

    itemlist = attachedreportslist + commentlist + attachedbookedinvestiagtionlist + videourllist + audiourllist + rapidtestlist + urinetestlist + eyetestlist
    try:
        requestlogger.info("loogin for bp")
        bpobj = bp.objects.filter(healthcase=healthcase_req).latest('datetime')
        lastattachedvitalsbp=model_to_dict(bpobj, fields=('systolic','diastolic','pulse'))
        lastattachedvitalsbp[ServerConstants.ITEMTYPE] = ServerConstants.HEALTHCASEVITAL
        lastattachedvitalsbp[ServerConstants.VITALTYPE] = VitalTypeConstant.BPMACHINE
        lastattachedvitalsbp[ServerConstants.REPORTTITLE] = "Blood Pressure"
        lastattachedvitalsbp[ServerConstants.ROLE] = "patient"
        lastattachedvitalsbp[ServerConstants.NAME] =  healthcase_req.person.name
        lastattachedvitalsbp[ServerConstants.DATETIME] =  bpobj.datetime
        itemlist.append(lastattachedvitalsbp)
        requestlogger.info("appending bp:"+str(itemlist))
    except Exception as e:
        requestlogger.info(str(e))
        logmessage.append(str(e))
        pass

    try:
        oxyobj = oximeter.objects.filter(healthcase=healthcase_req).latest('datetime')
        lastattachedvitalsoxy = model_to_dict(oxyobj,fields=('pulse','oxygensat','datetime'))
        lastattachedvitalsoxy[ServerConstants.ITEMTYPE] = ServerConstants.HEALTHCASEVITAL
        lastattachedvitalsoxy[ServerConstants.VITALTYPE] = VitalTypeConstant.OXIMETERMACHINE
        lastattachedvitalsoxy[ServerConstants.REPORTTITLE] = "SpO2, Pulse"
        lastattachedvitalsoxy[ServerConstants.ROLE] = "patient"
        lastattachedvitalsoxy[ServerConstants.NAME] =  healthcase_req.person.name
        lastattachedvitalsoxy[ServerConstants.DATETIME] =  oxyobj.datetime
        itemlist.append(lastattachedvitalsoxy)
    except:
        pass

    try:
        tempobj = temperature.objects.filter(healthcase=healthcase_req).latest('datetime')
        lastattachedvitalstempobj = model_to_dict(tempobj,fields=('temperature','datetime'))
        lastattachedvitalstempobj[ServerConstants.ITEMTYPE] = ServerConstants.HEALTHCASEVITAL
        lastattachedvitalstempobj[ServerConstants.REPORTTITLE] = "Temperature"
        lastattachedvitalstempobj[ServerConstants.VITALTYPE] = VitalTypeConstant.TEMPERATUREMACHINE
        lastattachedvitalstempobj[ServerConstants.ROLE] = "patient"
        lastattachedvitalstempobj[ServerConstants.NAME] =  healthcase_req.person.name
        lastattachedvitalstempobj[ServerConstants.DATETIME] =  tempobj.datetime
        itemlist.append(lastattachedvitalstempobj)
    except:
        pass

    try:
        glucoseobj = bloodglucose.objects.filter(healthcase=healthcase_req).latest('datetime')
        lastattachedvitalsglucose = model_to_dict(glucoseobj,fields=('glucose_level','testtypecode','datetime'))
        lastattachedvitalsglucose[ServerConstants.VITALTYPE] = VitalTypeConstant.BLOODGLUCOSEMACHINE
        lastattachedvitalsglucose[ServerConstants.ITEMTYPE] = ServerConstants.HEALTHCASEVITAL
        lastattachedvitalsglucose[ServerConstants.REPORTTITLE] = "Blood Glucose"
        lastattachedvitalsglucose[ServerConstants.NAME] =  healthcase_req.person.name
        lastattachedvitalsglucose[ServerConstants.DATETIME] =  glucoseobj.datetime
        itemlist.append(lastattachedvitalsglucose)
    except:
        pass

    finalitemlist = sorted(itemlist, key=itemgetter( ServerConstants.DATETIME),reverse=True)

    #invoice list
    healthcaseinvoices = HealthCaseInvoiceDetails.objects.filter(healthcase=healthcase_req)
    invoicelist = []
    for thisinvoice in healthcaseinvoices:
        invoicelist.append({ServerConstants.INVOICENUM: thisinvoice.invoicenum,
                            ServerConstants.INVOICEAMOUNT: thisinvoice.invoiceamount,
                            ServerConstants.UPDATED_AT: thisinvoice.updated_at.astimezone(timezone.get_current_timezone()).strftime('%d-%b-%y,%I:%M %p'),
                            ServerConstants.PAYMENTDONE: thisinvoice.paymentdone})

    healthcasedetails = {
            ServerConstants.USERNAME:healthcase_req.person.name,
            ServerConstants.PERSONUID:healthcase_req.person.personuid,
            ServerConstants.AGE: healthcase_req.person.get_age(),
            ServerConstants.GENDER: healthcase_req.person.gender,
            ServerConstants.PHONE: healthcase_req.person.phone,
            ServerConstants.PROBLEM: healthcase_req.problem,
            ServerConstants.TAGS: healthcase_req.tags,
            ServerConstants.LOCATION: healthcase_req.location,
            ServerConstants.STATUSCODE: healthcase_req.status,
            ServerConstants.STATUS: healthcase_req.get_status_display(),
            ServerConstants.HEALTHCASEID: healthcase_req.healthcaseid,
            ServerConstants.INVOICELIST: invoicelist
          }
    jsondata = dict({ServerConstants.ALLITEMARRAY:finalitemlist, ServerConstants.HEALTHCASEDETAILS:healthcasedetails})
    httpoutput = utils.successJson(jsondata)
    backendlogger.info('getallitemshealthcase', data, httpoutput, logmessage)
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)


@csrf_exempt
def getAllVCCountForDateRangeAndApp(request):
    # requestlogger.info('================getTestDataForDateRangeAndApp=====================')
    data = json.loads(request.body)
    logmessage = []

    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getAllVCCountForDateRangeAndApp', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        startdate = data[ServerConstants.STARTDATE]
        startdate = datetime.strptime(startdate, '%Y-%m-%d').replace(hour=0, minute=0, second=0)
    except KeyError:
        startdate = datetime(1, 2, 1)

    try:
        enddate = data[ServerConstants.ENDDATE]
        enddate = datetime.strptime(enddate, '%Y-%m-%d').replace(hour=23, minute=59, second=59)
    except KeyError:
        enddate = datetime.now()

    try:
        appstr = data[ServerConstants.APPSTR]
    except:
        appstr = None

    if appstr == None or appstr == "":
        backendlogger.error('getAllVCCountForDateRangeAndApp', data, 'Please specify atleast one app',
                            logmessage)
        return HttpResponse("Please specify atleast one app", status=403)

    logmessage.append('getVCDataForDateRangeAndApp ForDates:%s,%s' % (str(startdate), str(enddate)))

    apparray = appstr.split(',')

    healthcasesdata = HealthCase.objects.filter(lastupdated__range=(startdate, enddate ), appdetails__appuid__in=apparray)
    healthcasesdateaggregation = list(
        healthcasesdata.extra({'date_created': "date(lastupdated)"}).values('date_created').order_by('date_created').annotate(
            created_count=Count('healthcaseid')))
    healthcasescount = healthcasesdata.count()
    healthcasesdata = {ServerConstants.TOTALCOUNT: healthcasescount, ServerConstants.COUNTLIST: healthcasesdateaggregation}

    videocalldata = VCCodeDetails.objects.filter(date__range=(startdate, enddate ), healthcase__appdetails__appuid__in=apparray)
    videocallaggre = list(
        videocalldata.extra({'date_created': "date"}).values('date_created').order_by('date_created').annotate(
            created_count=Count('id')))
    videocallcount = videocalldata.count()
    videocalldata = {ServerConstants.TOTALCOUNT: videocallcount, ServerConstants.COUNTLIST: videocallaggre}

    phoneconsultdata = uploadedaudio.objects.filter(datetime__range=(startdate, enddate ), healthcase__appdetails__appuid__in=apparray)
    phoneconsultaggre = list(
        phoneconsultdata.extra({'date_created': "date(datetime)"}).values('date_created').order_by('date_created').annotate(
            created_count=Count('audioid')))
    phoneconsultcount = phoneconsultdata.count()
    phoneconsultdata = {ServerConstants.TOTALCOUNT: phoneconsultcount, ServerConstants.COUNTLIST: phoneconsultaggre}

    invoicescreateddata = HealthCaseInvoiceDetails.objects.filter(created_at__range=(startdate, enddate ), appdetails__appuid__in=apparray)
    invoicescreateddataaggre = list(
        invoicescreateddata.extra({'date_created': "date(created_at)"}).values('date_created').order_by(
            'date_created').annotate(
            created_count=Sum('invoiceamount')))
    invoicescreateddatacount = invoicescreateddata.aggregate(Sum('invoiceamount'))["invoiceamount__sum"]
    invoicescreateddata = {ServerConstants.TOTALCOUNT: invoicescreateddatacount, ServerConstants.COUNTLIST: invoicescreateddataaggre}


    invoicespaiddata = HealthCaseInvoiceDetails.objects.filter(created_at__range=(startdate, enddate ), appdetails__appuid__in=apparray, paymentdone=True)
    invoicespaiddataaggre = list(
        invoicespaiddata.extra({'date_created': "date(created_at)"}).values('date_created').order_by(
            'date_created').annotate(
            created_count=Sum('invoiceamount')))
    invoicespaiddatacount = invoicespaiddata.aggregate(Sum('invoiceamount'))["invoiceamount__sum"]
    invoicespaiddata = {ServerConstants.TOTALCOUNT: invoicespaiddatacount, ServerConstants.COUNTLIST: invoicespaiddataaggre}

    webappclickanalytics = WebAppDynamicLinkAnalytics.objects.filter(created_on__range=(startdate, enddate), appdetails__appuid__in=apparray, event="CLICK")
    webappclickanalyticsaggre = list(
        webappclickanalytics.extra({'date_created': "created_on"}).values('date_created').order_by('date_created').annotate( created_count=Sum('count')))
    webappclickanalyticscount = webappclickanalytics.aggregate(Sum('count'))["count__sum"]
    webappclickanalytics = {ServerConstants.TOTALCOUNT: webappclickanalyticscount,
                        ServerConstants.COUNTLIST: webappclickanalyticsaggre}

    webappinstallanalytics = WebAppDynamicLinkAnalytics.objects.filter(created_on__range=(startdate, enddate), appdetails__appuid__in=apparray, event="APP_INSTALL")
    webappinstallanalyticsaggre = list(
        webappinstallanalytics.extra({'date_created': "created_on"}).values('date_created').order_by('date_created').annotate( created_count=Sum('count')))
    webappinstallanalyticscount = webappinstallanalytics.aggregate(Sum('count'))["count__sum"]
    webappinstallanalytics = {ServerConstants.TOTALCOUNT: webappinstallanalyticscount,
                        ServerConstants.COUNTLIST: webappinstallanalyticsaggre}

    welcomesmsanalytics = WebAppDynamicLinkAnalytics.objects.filter(created_on__range=(startdate, enddate),
                                                                       appdetails__appuid__in=apparray,
                                                                       event="WELCOMESMSLINKCLICK",count__gt=0)
    welcomesmsanalyticsaggre = list(
        welcomesmsanalytics.extra({'date_created': "created_on"}).values('date_created').order_by(
            'date_created').annotate(created_count=Sum('count')))
    welcomesmsanalyticscount = welcomesmsanalytics.aggregate(Sum('count'))["count__sum"]
    welcomesmsanalytics = {ServerConstants.TOTALCOUNT: welcomesmsanalyticscount,
                              ServerConstants.COUNTLIST: welcomesmsanalyticsaggre}

    jsondata = list()
    jsondata.append({ServerConstants.NAME: "Health Cases Activity Seen",ServerConstants.DATA: healthcasesdata})
    jsondata.append({ServerConstants.NAME: "Video Consults Booked",ServerConstants.DATA: videocalldata})
    jsondata.append({ServerConstants.NAME: "Phone calls by doctors",  ServerConstants.DATA:phoneconsultdata})
    jsondata.append({ServerConstants.NAME: "Invoices Created",  ServerConstants.DATA:invoicescreateddata})
    jsondata.append({ServerConstants.NAME: "Invoices Paid",  ServerConstants.DATA:invoicespaiddata})
    jsondata.append({ServerConstants.NAME: "Welcome SMS Link Click",  ServerConstants.DATA:welcomesmsanalytics})
    jsondata.append({ServerConstants.NAME: "Go To Playstore click",  ServerConstants.DATA:webappclickanalytics})
    jsondata.append({ServerConstants.NAME: "App Install from link",  ServerConstants.DATA:webappinstallanalytics})

    httpoutput = utils.successJson({ServerConstants.ALLITEMARRAY: jsondata})
    backendlogger.info('getAllVCCountForDateRangeAndApp', data, httpoutput, logmessage)
    # requestlogger.info('getAllTestCountForDateRangeAndAppResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput,content_type=ServerConstants.RESPONSE_JSON_TYPE)


def getallhealthcaseofpersonbydate(request):
    data = json.loads(request.body)
    logmessage = []

    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getallhealthcaseofpersonbydate', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    kwargs = {}

    try:
        start_date = data[ServerConstants.STARTDATE]
        start = datetime.strptime(start_date, '%d-%m-%Y').replace(hour=0, minute=0, second=0)
        kwargs['lastupdated__gte'] = start
    except KeyError:
        pass

    try:
        end_date = data[ServerConstants.ENDDATE]
        end = datetime.strptime(end_date, '%d-%m-%Y').replace(hour=23, minute=59, second=59)
        kwargs['lastupdated__lte'] = end
    except KeyError:
        pass

    try:
        appstr_req = data[ServerConstants.APPSTR]
        appuidarray = appstr_req.split(',')
        kwargs ['appdetails__appuid__in'] = appuidarray
    except Exception as e:
        pass

    try:
        personuid_req = data[ServerConstants.PERSONUID]
        person = PersonDetails.objects.get(personuid = personuid_req)
    except Exception as e:
        backendlogger.error('getallhealthcaseofpersonbydate', data, str(e), logmessage)
        return HttpResponse("personuid/person not found", status=403)

    allhealthcases = HealthCase.objects.filter(person = person,**kwargs).order_by('-lastupdated')
    allhealthcaselist = list()
    for thishealthcase in allhealthcases:
        try:
            lastvideo = uploadedvideo.objects.filter(healthcase=thishealthcase).order_by('-datetime')[0]
            lastvideo = lastvideo.videourl
        except:
            lastvideo = ""

        try:
            lastcomment = commentshealthcase.objects.filter(healthcase=thishealthcase).order_by('-datetime')[0]
            lastcommenttext = lastcomment.commenttext
            lastcommenterrole = lastcomment.commenterrole
        except:
            lastcommenttext = ""
            lastcommenterrole = ""

        healthcaseinvoices = HealthCaseInvoiceDetails.objects.filter(healthcase=thishealthcase)
        invoicelist = []
        for thisinvoice in healthcaseinvoices:
            invoicelist.append({ServerConstants.INVOICENUM: thisinvoice.invoicenum,
                                ServerConstants.INVOICEAMOUNT: thisinvoice.invoiceamount,
                                ServerConstants.UPDATED_AT: thisinvoice.updated_at.astimezone(
                                    timezone.get_current_timezone()).strftime('%d-%b-%y,%I:%M %p'),
                                ServerConstants.PAYMENTDONE: thisinvoice.paymentdone})

        vccodedetailslist = list()
        try:
            allvccodedetails = VCCodeDetails.objects.filter(healthcase=thishealthcase,
                                                            slotbooking__consultstatus__in=[
                                                                AppointmentStatusConstant.PENDING,
                                                                AppointmentStatusConstant.STARTED,
                                                                AppointmentStatusConstant.DROPPED])
            for vccodedetails in allvccodedetails:
                vccodedetailsdict = {ServerConstants.VCCODE: vccodedetails.vccode,
                                     ServerConstants.VCDATE: vccodedetails.date,
                                     ServerConstants.VCTIME: vccodedetails.slotbooking.starttime.strftime("%I:%M %p"),
                                     ServerConstants.VCCODEID: vccodedetails.id,
                                     ServerConstants.ISVALID: True if vccodedetails.date == date.today() else False,
                                     ServerConstants.CONSULTSTATUS: vccodedetails.slotbooking.get_consultstatus_display()}
                vccodedetailslist.append(vccodedetailsdict)
        except Exception as e:
            logmessage.append(str(e))
            pass

        allhealthcaselist.append({
            ServerConstants.HEALTHCASEID: thishealthcase.healthcaseid,
            ServerConstants.APPUID: thishealthcase.appdetails.appuid,
            ServerConstants.DOCTORSCONSULTED:  [ doctor.name for doctor in thishealthcase.doctorsconsulted.all()],
            ServerConstants.PERSONUID: thishealthcase.person.personuid,
            ServerConstants.DATETIME: thishealthcase.created_at,
            ServerConstants.LASTUPDATED: thishealthcase.lastupdated,
            ServerConstants.PROBLEM: thishealthcase.problem,
            ServerConstants.VIDEOURL: lastvideo,
            ServerConstants.COMMENTTEXT: lastcommenttext,
            ServerConstants.COMMENTERROLE: lastcommenterrole,
            ServerConstants.USERNAME: thishealthcase.person.name,
            ServerConstants.AGE: thishealthcase.person.get_age(),
            ServerConstants.GENDER: thishealthcase.person.gender,
            ServerConstants.PHONE: thishealthcase.person.phone,
            ServerConstants.TAGS: thishealthcase.tags,
            ServerConstants.LOCATION: thishealthcase.location,
            ServerConstants.STATUS: thishealthcase.get_status_display(),
            ServerConstants.STATUSCODE: thishealthcase.status,
            ServerConstants.INVOICELIST: invoicelist,
            ServerConstants.VCCODEDETAILSLIST: vccodedetailslist
        })

    allhealthcaselistsorted = sorted(allhealthcaselist, key=lambda x: x[ServerConstants.STATUSCODE])
    jsondata = dict({ServerConstants.HEALTHCASELIST: allhealthcaselistsorted})
    backendlogger.info('getallhealthcaseofpersonbydate', data, jsondata, logmessage)
    httpoutput = utils.successJson(jsondata)
    # requestlogger.info('getHealthPackageOfDoctorResponse;%s'%(str(httpoutput)))
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

