from django.contrib import admin
from HealthCase.models import uploadedvideo,HealthCase,commentshealthcase,attachedreporthealthcase, uploadedaudio
from import_export import resources
from import_export.admin import ImportExportModelAdmin,ImportExportMixin
# Register your models here.
class HealthEnrollmentModelAdmin(admin.ModelAdmin):
    list_display = ('enrollid','person','healthprogram','activationcode','status','startdate','enddate','enrolldate')

class HealthPackageModelAdmin(admin.ModelAdmin):
    list_display = ('packageid','packagename','packagecode','packageperiod','description','doctor','packageduration','packagefee')

class foodtrackModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'person', 'datetime', 'mealtime','itemname', 'unit', 'unittext', 'calories', 'fat', 'carbs', 'prot')

class uploadedvideoResource(resources.ModelResource):
    class Meta:
        model = uploadedvideo

class healthcaseResource(resources.ModelResource):
    class Meta:
        model = HealthCase


class commentshealthcaseResource(resources.ModelResource):
    class Meta:
        model = commentshealthcase

class attachedreportshealthcaseResource(resources.ModelResource):
    class Meta:
        model = attachedreporthealthcase

class uploadedvideoModelAdmin(admin.ModelAdmin):
    list_display = ('videoid','healthcase','videourl','person','doctor','datetime')

admin.site.register(uploadedvideo,uploadedvideoModelAdmin)


class uploadedaudioModelAdmin(admin.ModelAdmin):
    list_display = ('audioid','healthcase','audiourl','person','doctor','datetime')

admin.site.register(uploadedaudio,uploadedaudioModelAdmin)

class healthcaseModelAdmin(ImportExportMixin,admin.ModelAdmin):
    list_display = ('healthcaseid','appdetails','doctor','location','person','lastupdated', 'created_at')
    resource_class = healthcaseResource

admin.site.register(HealthCase, healthcaseModelAdmin)

class healthcaseDoctorModelAdmin(ImportExportMixin,admin.ModelAdmin):
    list_display = ('healthcase','doctor','datetime')
    resource_class = healthcaseResource


class commentshealthcaseModelAdmin(admin.ModelAdmin):
    list_display = ('commentid', 'healthcase', 'datetime', 'updatedatetime','commenterrole', 'commenterid', 'commenteruid','commenttext')
    resource_class = commentshealthcaseResource


admin.site.register(commentshealthcase, commentshealthcaseModelAdmin)

class attachedreporthealthcaseModelAdmin(admin.ModelAdmin):
    list_display = ('healthcase', 'datetime','attachedreportid', 'uploaderrole','uploaderid','uploaderuid', 'attachedreporttype')
    resource_class = attachedreportshealthcaseResource


admin.site.register(attachedreporthealthcase, attachedreporthealthcaseModelAdmin)





