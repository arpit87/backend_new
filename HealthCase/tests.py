import base64
import datetime
import json
from datetime import timedelta

import requests
from django.conf import settings
from django.contrib.auth.models import Group, Person
from django.test import TestCase, override_settings
from django.test.client import MULTIPART_CONTENT

import Platform
import Platform.Constants
import Platform.utils
import backend_new.config
from HealthPrograms.models import (HealthEnrollment, HealthPackage,
                                   commentshealthcase, healthcase,
                                   healthcasemover,
                                   attachedreporthealthcase, uploadedvideo)
from Doctor.models import (DoctorAppMapping, AppDetails, DoctorDetails,
                          SkillNumToGroup)
from Platform.models import DOCTORAPPDATA, USERAPPDATA
from Person.models import PersonDetails
from backend_new.Constants import (FileStorageConstants, FileTypeConstants,
                                      HealthCaseStatusConstants,
                                      ServerConstants)
from backend_new.config import AUTHKEY, AUTHSECRET
# Create your tests here.

class HealthProgramsAPITestCase(TestCase):
     multi_db = True

     def setUp(self):
        backend_new.config.ISTESTMODE = True

        today = datetime.datetime.now()
        djangouser = Person.objects.create_user(username="user9769465241",password="abc")
        djangouser1 = Person.objects.create_user(username="user9769465242",password="abc")
        usernew=PersonDetails.objects.create(djangouser=djangouser, personuid=10, name='arpit', phone="9769465241", gender="M", dob_year=1987, email="arpit87@gmail.com", kioskid=100)
        usernew1=PersonDetails.objects.create(djangouser=djangouser1, personuid=2, name='arpit1', phone="9769465242", gender="M", dob_year=1987, email="arpit871@gmail.com", kioskid=100)
        movernew = DoctorDetails.objects.create(moverid=1,name="arpitnew",phone="978637272",skillstr="0,1,2,5",
                                               docs="testdoc1,testdoc2,http://digiclinic.in/static/uploadedfiles/moverdocs/file1.png",
                                               servicesoffered = "1,2,3",profilepic="http://yolohealth.in/static/uploadedfiles/moverdocs/file1.png")

        movernew1 = DoctorDetails.objects.create(moverid=2,name="hirennew",phone="9167636253",skillstr="0,5",
                                               docs="testdoc1,testdoc2,http://yolohealth.in/static/uploadedfiles/moverdocs/file1.png",
                                               servicesoffered = "1,2,3",profilepic="http://yolohealth.in/static/uploadedfiles/moverdocs/file1.png")
        newpackage = HealthPackage.objects.create(packagename = "Weight Loss",packagecode="WL-MOT-1",packageperiod="Jun-Aug 16",description = "This weight loss program condected by motilal",
                                                  packageduration = "30 days", mover = movernew, packagefee = "500", packageid=1)
        packageone = HealthPackage.objects.create(packagename = "Fitness",packagecode="FIT-MOT-1",packageperiod="Jun-Aug 16",description = "This Fitness program condected by IOC",
                                                  packageduration = "15 days", mover = movernew, packagefee = "700", packageid=2)
        HealthEnrollment.objects.create(user = usernew, healthprogram = newpackage,activationcode="500MOT",
                                                    status= 'isactive',
                                                    enrolldate = datetime.datetime.now(),
                                                    startdate = today+datetime.timedelta(days=5),
                                                    enddate = today+datetime.timedelta(days=30))

        HealthEnrollment.objects.create(user = usernew, healthprogram = newpackage,activationcode="501MOT",
                                                    status= 'isactive',
                                                    enrolldate = datetime.datetime.now(),
                                                    startdate = today+datetime.timedelta(days=5),
                                                    enddate = today+datetime.timedelta(days=30))
        #kiosk details
        kiosk = AppDetails.objects.create(kioskid=1, kiosklocation="IOC", kiosktag="IOC-1", kiosktype=1, kioskdescription="IOC")
        DoctorAppMapping.objects.create(mover = movernew1, kiosk = kiosk)
        # DoctorAppMapping.objects.create(mover = movernew,kiosk = kiosk)

        kiosk1 = AppDetails.objects.create(kioskid=11, kiosklocation="MOT", kiosktag="MOT-11", kiosktype=1, kioskdescription="MOT")

        # Offline consult Test
        consultnew = healthcase.objects.create(healthcaseid=10, user=usernew, status=HealthCaseStatusConstants.NEW, skillnum=5)
        consultnew2 = healthcase.objects.create(healthcaseid=3, user=usernew, status=HealthCaseStatusConstants.OPEN, kiosk=kiosk, skillnum=5)
        consultnew1 = healthcase.objects.create(healthcaseid=20, user=usernew1, status=HealthCaseStatusConstants.OPEN)
        consultnew3 = healthcase.objects.create(healthcaseid=4, user=usernew, status=HealthCaseStatusConstants.ONGOING_USERRESPONDED)

        offlinemovernew = healthcasemover.objects.create(offlineconsult=consultnew, mover=movernew, datetime=today)
        #offlinemovernew1 = offlineconsultmover.objects.create(offlineconsult=consultnew1, mover=movernew, datetime=today)

        uploadedvideo.objects.create( id=1, offlineconsult=consultnew, videourl="https://www.healthatm.in.global.prod.fastly.net/930T35A51474012513.5videoconsult.webm", user=usernew, datetime=today)
        uploadedvideo.objects.create( id=2, offlineconsult=consultnew1, videourl="https://www.healthatm.in.global.prod.fastly.net/930T35A51474012513.5videoconsult.webm", user=usernew1, datetime=today)

        attachedreporthealthcase.objects.create(offlineconsult=consultnew, sharedreportid="ASDFAS", sharedreporttype="kioskreport", sharedreporttitle="BP,WEIGHT")
        commentshealthcase.objects.create(offlineconsult=consultnew, commenterid=1, commenterrole="doctor", commenttext="oo la la")
        DOCTORAPPDATA.objects.create(moverappuuid = "asasdfdfarpitm",moverid=1,gcmregid="APA91bEZuYrK5lAkcmO3vxV5t3H17xJSHKdrj6v7-_aZedfb_tGE_y8JNxYmFXD21f59Fqacb846MA-fxiXIHIRFL0mJzN32rEpHx95r9dP0Xjn8UDbJZb-w6dL3ElRrRJk4brBG0T2S")
        USERAPPDATA.objects.create(userappuuid = "asdfarpitm",personuid=2,gcmregid="APA91bEZuYrK5lAkcmO3vxV5t3H17xJSHKdrj6v7-_aZedfb_tGE_y8JNxYmFXD21f59Fqacb846MA-fxiXIHIRFL0mJzN32rEpHx95r9dP0Xjn8UDbJZb-w6dL3ElRrRJk4brBG0T2S")

        Group.objects.create(name="admingroup")
        Group.objects.create(name="usergroup")
        Group.objects.create(name="movergroup")
        Group.objects.create(name="doctorgroup")

        SkillNumToGroup.objects.create( skillnum=5, groupname="doctorgroup", categoryname="Ayurveda", categorydescription= "Ayurveda")

     def test_getHealthPackageForPerson(self):
        input = dict({ServerConstants.PERSONUID:10})
        response = self.client.get('/HealthPrograms/getHealthPackageForPerson/',input)
        self.assertEqual(response.status_code,200)


     def test_getAllHealthPackage(self):
        input = dict({})
        response = self.client.get('/HealthPrograms/getAllHealthPackage/',input)
        self.assertEqual(response.status_code,200)


     def test_activateHealthPackageByCode(self):
         input = json.dumps({ServerConstants.ACTIVATIONCODE :'500MOT'})
         response = self.client.post('/HealthPrograms/activateHealthPackageByCode/',input,Platform.Constants.RESPONSE_JSON_TYPE)
         self.assertEqual(response.status_code,200)


     def test_getHealthPackageDetails(self):
         input = dict({ServerConstants.PACKAGE_ID:1 })
         response = self.client.get('/HealthPrograms/getHealthPackageDetails/',input)
         self.assertEqual(response.status_code,200)


    #  def test_enrollPersonForHealthProgram(self):
    #      input = json.dumps({ServerConstants.PHONE:"7218072091",
    #                          ServerConstants.USERNAME:'abhishek',
    #                          ServerConstants.PACKAGE_ID:1,
    #                          ServerConstants.STARTDATE:"2016-05-26",
    #                          ServerConstants.ENDDATE:"2016-08-26",
    #                          ServerConstants.OTP:'123321',"authkey": AUTHKEY,"authsecret": AUTHSECRET})
    #      response = self.client.post('/HealthPrograms/enrollPersonForHealthProgram/',input,Platform.Constants.RESPONSE_JSON_TYPE)
    #      self.assertEqual(response.status_code,200)

    #      #now delete the test mover created on qb
    #      moverdata = json.loads(response.content)
    #      qbsessiontoken = Platform.utils.getQBPersonSessionToken(moverdata["body"]["qbusername"],moverdata["body"]["qbpassword"])
    #      response = requests.delete("http://api.quickblox.com/users/"+str(moverdata["body"]["qbid"])+".json", headers={'Content-Type': 'application/json',
    #                                                                                      'QuickBlox-REST-API-Version': '0.1.0',
    #                                                                                       'QB-Token':qbsessiontoken})
    #      print("test_enrollPersonForHealthProgram response: ", response.content)
    #      self.assertEqual(response.status_code,200)

     def test_getHealthPackageOfDoctor(self):
        input = dict({ServerConstants.DOCTORUID:1})
        response = self.client.get('/HealthPrograms/getHealthPackageOfDoctor/',input)
        self.assertEqual(response.status_code,200)


     def test_getHealthPackageEnrolledPersons(self):
            input = dict({ServerConstants.PACKAGE_ID:1 })
            response = self.client.get('/HealthPrograms/getHealthPackageEnrolledPersons/',input)
            self.assertEqual(response.status_code,200)



     def test_getfoodtracksearch(self):
         input = dict({"searchword":'roti'})
         response = self.client.get('/HealthPrograms/getfoodtracksearch/',input)
         self.assertEqual(response.status_code, 200)


     def test_createfoodtrackview(self):
         input = json.dumps({ServerConstants.PERSONUID:10, ServerConstants.DATETIME: "2016-05-26", ServerConstants.ITEMNAME: "chay", ServerConstants.UNIT:1, ServerConstants.UNITTEXT: "per 1 medium", ServerConstants.CARBS:2, ServerConstants.PROT:4, ServerConstants.CALORIES:6, ServerConstants.FAT:6, ServerConstants.MEALTIME: "lunch"})
         response = self.client.post('/HealthPrograms/createfoodtrack/',input,Platform.Constants.RESPONSE_JSON_TYPE)
         self.assertEqual(response.status_code, 200)


     def test_deletefoodtrack(self):
         input = json.dumps({ServerConstants.PERSONUID:10, ServerConstants.DATETIME: "2016-05-26", ServerConstants.ITEMNAME: "chay", ServerConstants.UNIT:1, ServerConstants.UNITTEXT: "per 1 medium", ServerConstants.CARBS:2, ServerConstants.PROT:4, ServerConstants.FAT:6, ServerConstants.MEALTIME: "lunch"})
         response = self.client.post('/HealthPrograms/createfoodtrack/',input,Platform.Constants.RESPONSE_JSON_TYPE)
         self.assertEqual(response.status_code, 200)
         data = json.loads(response.content)
         input = json.dumps({ServerConstants.FOODTRACKID:data["body"]["foodtrackid"]})
         response = self.client.post('/HealthPrograms/deletefoodtrack/',input,Platform.Constants.RESPONSE_JSON_TYPE)
         self.assertEqual(response.status_code, 200)


     def test_createofflineconsult(self):

         input = json.dumps({
                 ServerConstants.PERSONUID:10,
                 ServerConstants.PROBLEM:"Cold",
                 ServerConstants.DETAILS:"last 2 days",
                 ServerConstants.SKILLNUM:5,
                 ServerConstants.KIOSKID:1
                })
         response = self.client.post('/HealthPrograms/startnewofflineconsult/',input,Platform.Constants.RESPONSE_JSON_TYPE)
         self.assertEqual(response.status_code,200)


     def test_createofflineconsultmappedtomover(self):
         input = json.dumps({
                 ServerConstants.PERSONUID:10,
                 ServerConstants.DOCTORPHONE:'978637272',
                 ServerConstants.PROBLEM:"Cold",
                 ServerConstants.DETAILS:"last 2 days",
                 ServerConstants.KIOSKID:1
                })
         response = self.client.post('/HealthPrograms/startnewofflineconsult/',input,Platform.Constants.RESPONSE_JSON_TYPE)
         self.assertEqual(response.status_code,200)


     def test_getvideourlsforconsultid(self):

         input = dict({
            ServerConstants.HEALTHCASEID:10,
            })
         response = self.client.get('/HealthPrograms/getvideourlsforconsultid/',input)
         self.assertEqual(response.status_code,200)



     def test_getlatestunmappedconsultforadmin(self):
         response = self.client.get('/HealthPrograms/getlatestunmappedconsultforadmin/')
         self.assertEqual(response.status_code, 200)


     def test_setdoctorofflineconsultmapping(self):
         input = json.dumps({
             ServerConstants.HEALTHCASEID: 20,
             ServerConstants.DOCTORPHONE: '978637272',
         })
         response = self.client.post('/HealthPrograms/setdoctorofflineconsultmapping/',input,Platform.Constants.RESPONSE_JSON_TYPE)
         self.assertEqual(response.status_code, 200)


     def test_getdoctorofflineconsultmapping(self):
         response = self.client.get('/HealthPrograms/getdoctorofflineconsultmapping/')
         self.assertEqual(response.status_code, 200)


     def test_getdoctorsofflineconsults(self):
         input = dict({
             ServerConstants.DOCTORUID: 1,
             ServerConstants.OFFSET:0,
             ServerConstants.STATUS:"open",
             ServerConstants.LIMIT:10
         })
         response = self.client.get('/HealthPrograms/getdoctorsofflineconsults/',input)
         print("test_getdoctorsofflineconsults response: ", response.content)
         self.assertEqual(response.status_code, 200)


     def test_getdoctorsofflineconsultsforopenstatus(self):
         input = dict({
             ServerConstants.DOCTORUID: 2,
             ServerConstants.OFFSET:0,
             ServerConstants.LIMIT:10,
             ServerConstants.STATUS:HealthCaseStatusConstants.OPEN,
             ServerConstants.KIOSKID:1
         })
         response = self.client.get('/HealthPrograms/getdoctorsofflineconsults/',input)
         self.assertEqual(response.status_code, 200)


     def test_getusersofflineconsults(self):
         input = dict({
             ServerConstants.PERSONUID: 10,
         })
         response = self.client.get('/HealthPrograms/getusersofflineconsults/',input)
         self.assertEqual(response.status_code, 200)


     def test_deleteofflineconsults(self):
         input = dict({
             ServerConstants.HEALTHCASEID: 3,
         })
         response = self.client.get('/HealthPrograms/deleteofflineconsult/',input)
         self.assertEqual(response.status_code, 200)


     def test_getconsulttobedeleted(self):
         input = dict({
             ServerConstants.PERSONUID: 10,
         })
         response = self.client.get('/HealthPrograms/getconsulttobedeleted/',input)
         self.assertEqual(response.status_code, 200)



     def test_getusersofflineconsultswithstatus(self):
         input = dict({
             ServerConstants.PERSONUID: 10,
             ServerConstants.STATUS:"ongoing_userresponded"
         })
         response = self.client.get('/HealthPrograms/getusersofflineconsults/',input)
         self.assertEqual(response.status_code, 200)



     def test_sharereportofflineconsult(self):
         input = json.dumps({
             ServerConstants.HEALTHCASEID: 20,
             ServerConstants.ID:1,
             ServerConstants.ROLE:'doctor',
             ServerConstants.REPORTID: 'KSWER',
             ServerConstants.REPORTTYPE: 'kioskreport',
             ServerConstants.REPORTTITLE: 'BP Report',
         })
         response = self.client.post('/HealthPrograms/sharereportofflineconsult/',input,Platform.Constants.RESPONSE_JSON_TYPE)
         self.assertEqual(response.status_code, 200)

         response = self.client.post('/HealthPrograms/sharereportofflineconsult/',input,Platform.Constants.RESPONSE_JSON_TYPE)
         self.assertEqual(response.status_code, 403)


     def test_getsharedreportofflineconsult(self):
         input = dict({
            ServerConstants.HEALTHCASEID:10,
            })
         response = self.client.get('/HealthPrograms/getsharedreportofflineconsult/',input)
         self.assertEqual(response.status_code, 200)


     def test_putcommentofflineconsult(self):
         input = json.dumps({
             ServerConstants.HEALTHCASEID: 20,
             ServerConstants.COMMENTER: 'Arpit',
             ServerConstants.COMMENTERROLE: 'Person',
             ServerConstants.COMMENTTEXT: 'Measure bp again',
             ServerConstants.COMMENTERUID:2
         })
         response = self.client.post('/HealthPrograms/putcommentofflineconsult/',input,Platform.Constants.RESPONSE_JSON_TYPE)
         self.assertEqual(response.status_code, 200)


     def test_getcommentofflineconsult(self):
         input = dict({
            ServerConstants.HEALTHCASEID:10,
            })
         response = self.client.get('/HealthPrograms/getcommentofflineconsult/',input)
         self.assertEqual(response.status_code, 200)


     def test_updatestatusofflineconsult(self):
         input = json.dumps({
             ServerConstants.HEALTHCASEID: 10,
             ServerConstants.STATUS: 'offlineconsultcomplete',
             ServerConstants.DOCTORUID: 1,
         })
         response = self.client.post('/HealthPrograms/updatestatusofflineconsult/',input,Platform.Constants.RESPONSE_JSON_TYPE)
         print("test_updatestatusofflineconsult response: ", response.content)
         self.assertEqual(response.status_code, 200)
