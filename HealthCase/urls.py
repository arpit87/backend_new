from django.conf.urls import url

from HealthCase import views

urlpatterns = [
                       #healthcase
                       url(r'^startnewhealthcase/', views.startnewhealthcase, name='startnewhealthcase'),
                       url(r'^maphealthcasetodoctor/', views.maphealthcasetodoctor, name='maphealthcasetodoctor'),
                       url(r'^getusershealthcaselist/', views.getusershealthcaselist, name='getusershealthcaselist'), # used in selftest
                       url(r'^updatehealthcasestatus/', views.updatehealthcasestatus, name='updatehealthcasestatus'),
                       url(r'^getallhealthcasebydate/', views.getallhealthcasebydate, name='getallhealthcasebydate'), # this is for doctor/hims
                       url(r'^getvideourlsforconsultid/', views.getvideourlsforconsultid, name='getvideourlsforconsultid'),
                       url(r'^attachreporthealthcase/', views.attachreporthealthcase, name='attachreporthealthcase'),
                       url(r'^getattachedreporthealthcase/', views.getattachedreporthealthcase, name='getattachedreporthealthcase'),
                       url(r'^putcommentshealthcase/', views.putcommentshealthcase, name='putcommentshealthcase'),
                       url(r'^updatecommentshealthcase/', views.updatecommentshealthcase, name='updatecommentshealthcase'),
                       url(r'^getcommentshealthcase/', views.getcommentshealthcase, name='getcommentshealthcase'),
                       url(r'^getallitemshealthcase/', views.getallitemshealthcase, name='getallitemshealthcase'),
                       url(r'^getAllVCCountForDateRangeAndApp/', views.getAllVCCountForDateRangeAndApp, name='getAllVCCountForDateRangeAndApp'),
                       url(r'^getallhealthcaseofpersonbydate/', views.getallhealthcaseofpersonbydate,name='getallhealthcaseofpersonbydate'),

]