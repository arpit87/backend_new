from django.db import models
from Doctor.models import DoctorDetails,AppDetails
from Person.models import PersonDetails
from django.utils import timezone
from backend_new.Constants import HealthCaseConstant, VitalTypeConstant, ServerConstants


class HealthCase(models.Model):
    HEALTHCASE_STATUSES = (
        (HealthCaseConstant.NEW, ' New '), (HealthCaseConstant.ONGOING_PERSONRESPONDED, 'Ongoing:Person Responded'),
        (HealthCaseConstant.ONGOING_DOCTORRESPONDED, 'Ongoing:Doctor Responded'),
        (HealthCaseConstant.CLOSED, 'Closed'),
        (HealthCaseConstant.DISCARDED, 'Dicarded'),
         (HealthCaseConstant.REOPEN, 'Reopen'), (HealthCaseConstant.VCBOOKED, 'VC Booked'),)
    healthcaseid = models.CharField(db_index=True, max_length=20,unique=True,null=True)
    problem = models.CharField(max_length=254,default="")
    tags = models.CharField(max_length=1024,default="")
    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    lastupdated = models.DateTimeField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    status = models.IntegerField(choices=HEALTHCASE_STATUSES, default=HealthCaseConstant.NEW)
    appdetails = models.ForeignKey(AppDetails, null=True, on_delete=models.CASCADE)
    location = models.CharField(max_length=50,default="")
    shorturlpatient = models.CharField(max_length=50, default="")
    shorturldoctor = models.CharField(max_length=50, default="")
    doctorsconsulted = models.ManyToManyField(DoctorDetails, related_name="healthcases") # list of all doctors who can see case
    doctor = models.ForeignKey(DoctorDetails, null=True, default = None, on_delete=models.SET_NULL) # main doctor

    def __str__(self):
       return str(self.healthcaseid)

    def save(self, *args, **kwargs):
        self.lastupdated = timezone.now()
        if self.location == "":
            self.location = self.appdetails.applocation
        return super(HealthCase, self).save(*args, **kwargs)

class uploadedvideo(models.Model):
    videoid = models.AutoField(primary_key=True)
    healthcase = models.ForeignKey(HealthCase, null=False, on_delete=models.CASCADE)
    videourl = models.CharField(max_length=512,null=False,blank=False)
    datetime = models.DateTimeField()
    person = models.ForeignKey(PersonDetails, null=True, on_delete=models.CASCADE)
    doctor = models.ForeignKey(DoctorDetails,null=True, on_delete=models.CASCADE)

    def __str__(self):
       return str(self.videoid)

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.videoid:
            self.datetime = timezone.now()
        return super(uploadedvideo, self).save(*args, **kwargs)

class uploadedaudio(models.Model):
    audioid = models.AutoField(primary_key=True)
    healthcase = models.ForeignKey(HealthCase, null=False, on_delete=models.CASCADE)
    audiourl = models.CharField(max_length=512, null=False, blank=False)
    datetime = models.DateTimeField(auto_now_add=True)
    person = models.ForeignKey(PersonDetails, null=True, on_delete=models.CASCADE)
    doctor = models.ForeignKey(DoctorDetails, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.audioid)

class attachedreporthealthcase(models.Model):
    healthcase = models.ForeignKey(HealthCase, null=False, on_delete=models.CASCADE)
    attachedreportid = models.CharField(max_length=20,null=False,blank=False)
    attachedreporttype = models.IntegerField()
    attachedreporttitle = models.CharField(max_length=255,null=True,blank=True)
    attachedreportdescription = models.CharField(max_length=512,null=True,blank=True)
    datetime = models.DateTimeField(auto_now_add=True)
    uploaderrole = models.CharField(max_length=50, default="patient")  # who is uploading this report, it could be patient himself or doctor or healthworker etc
    uploaderid = models.IntegerField(null=True)
    uploaderuid = models.CharField(max_length=10, null=True)

    def __str__(self):
       return str(self.id)

    class Meta:
        unique_together = ('healthcase', 'attachedreportid')

class commentshealthcase(models.Model):
    commentid = models.AutoField(primary_key=True)
    healthcase = models.ForeignKey(HealthCase, null=False, on_delete=models.CASCADE)
    datetime = models.DateTimeField(auto_now_add=True)
    updatedatetime = models.DateTimeField()
    commenterrole = models.CharField(max_length=20,null=False)
    commenttext = models.CharField(max_length=4056)
    isvalid = models.BooleanField(default=True)
    commenterid = models.IntegerField(null=True)
    commenteruid = models.CharField(max_length=10,null=True)

    def __str__(self):
       return str(self.commentid)

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.commentid:
            self.updatedatetime = timezone.now()
        return super(commentshealthcase, self).save(*args, **kwargs)



