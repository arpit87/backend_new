from django.contrib import admin
from Lab.models import LabDetails, Investigation, LabReport, BookedInvestigation, AppLabMapping, PersonLabMapping
from import_export import resources
from import_export.admin import ImportExportModelAdmin,ImportExportMixin
# Register your models here.
class LabDetailsModelAdmin(admin.ModelAdmin):
    list_display = ('id','name','address','city','code')

class InvestigationModelAdmin(admin.ModelAdmin):
    list_display = ('labdetails','investigationcode','investigationname','includedtests','pricetolab','pricetoperson','marketprice')

class LabReportModelAdmin(admin.ModelAdmin):
    list_display = ('person', 'reporttitle', 'reporturl',  'uploaded_at')

class BookedInvestigationModelAdmin(admin.ModelAdmin):
        list_display = ('person', 'appdetails','investigation' , 'labreport', 'booking_date', 'status')

class AppLabMappingModelAdmin(admin.ModelAdmin):
    list_display = ('appdetails', 'labdetails')

class PersonLabMappingModelAdmin(admin.ModelAdmin):
    list_display = ('persondetails', 'labdetails')

admin.site.register(LabDetails,LabDetailsModelAdmin)
admin.site.register(Investigation,InvestigationModelAdmin)
admin.site.register(LabReport,LabReportModelAdmin)
admin.site.register(BookedInvestigation,BookedInvestigationModelAdmin)
admin.site.register(AppLabMapping,AppLabMappingModelAdmin)
admin.site.register(PersonLabMapping,PersonLabMappingModelAdmin)






