import json, requests
import pandas as pd
from django.http import HttpRequest, HttpResponse
import os, sys, django, datetime
project_path = os.path.split(os.path.dirname(os.path.realpath(__file__)))[-2] # path to manage.py
sys.path.append(project_path)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "backend_new.settings")
django.setup()

from Lab.models import Investigation , LabDetails

TARGETPATH = "/home/arpit/Downloads/labdata.csv"
LABNAME = "LAB1"

def main():
    raw_df = pd.read_csv(TARGETPATH)
    df = raw_df.where((pd.notnull(raw_df)), None)
    investigationdata = df.T.to_dict().values()
    print('data: ', investigationdata)

    for thisinvestigation in investigationdata:
        print("iname:"+str(thisinvestigation))
        labdetails = LabDetails.objects.get(id=thisinvestigation.get('labid'))
        Investigation.objects.create(
            labdetails=labdetails,
            investigationname=thisinvestigation.get('investigationname'),
            includedtests=thisinvestigation.get('includedtests'),
            marketprice=thisinvestigation.get('marketprice'),
            pricetolab=thisinvestigation.get('pricetolab'),
            pricetoperson=thisinvestigation.get('pricetoperson'))

if __name__ == "__main__":
    main()