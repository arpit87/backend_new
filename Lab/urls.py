from django.conf.urls import url

from Lab import views

urlpatterns = [
            url(r'^getInvestigationDetails', views.get_investigation_details, name='getInvestigationDetails'),
            url(r'^getBookedInvestigations', views.get_booked_investigations, name='getBookedInvestigations'),
            url(r'^bookInvestigation', views.book_investigation, name='bookInvestigation'),
            url(r'^updateBookedInvestigation', views.update_booked_investigation, name='updateBookedInvestigation'),
            url(r'^getInvestigationSuggestions', views.get_investigation_suggestions, name='getInvestigationSuggestions'),
            url(r'^getInvestigationsForLab', views.get_investigations_for_lab, name='getInvestigationsForLab'),
            url(r'^getAllTestsOfLab', views.get_all_tests_of_lab, name='getAllTestsOfLab'),
]