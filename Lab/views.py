import json, datetime
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.http import HttpResponse

from HealthCase.models import HealthCase
from Lab.models import BookedInvestigation, AppDetails, LabDetails
from Lab.models import Investigation
from Person.models import PersonDetails
from backend_new.Constants import ServerConstants , BookedInvestigationConstant
from Platform.views import authenticateURL
from Platform import backendlogger, utils
import Communications
from Communications.views import sendSMS

BOOK_INVESTIGATION_MESSAGE = 'Your test has been booked. Kindly pay Rs.{} to the attendant.'

def get_investigation_details(request):
    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('get_investigation_details', logrequest, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        investigation_id = request.GET[ServerConstants.INVESTIGATIONID]
    except KeyError as e:
        logmessage.append("Investigation Id not found")
        backendlogger.error('get_investigation_details', logrequest, logmessage, logmessage)
        return HttpResponse("Investigation Id not found", status=403)

    try:
        investigation = Investigation.objects.get(id=investigation_id)
    except ObjectDoesNotExist as e:
        logmessage.append("Investigation not found")
        backendlogger.error('get_investigation_details', logrequest, logmessage, logmessage)
        return HttpResponse("Investigation not found", status=403)

    httpoutput = utils.successJson(dict({ServerConstants.INVESTIGATION:investigation.get_dict()}))
    backendlogger.info('get_investigation_details', logrequest, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

def update_booked_investigation(request):
    data = json.loads(request.body)
    logmessage = []
    logrequest = {}
    authenticated, djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('book_investigation', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        bookingid_req = data[ServerConstants.BOOKINGID]
    except KeyError as e:
        logmessage.append("Booking Id not sent")
        backendlogger.error('update_booked_investigation', logrequest, logmessage, logmessage)
        return HttpResponse("Booking Id not sent", status=403)

    try:
        booking_req = BookedInvestigation.objects.get(id=bookingid_req)
    except ObjectDoesNotExist as e:
        logmessage.append("Booking does not exist")
        backendlogger.error('update_booked_investigation', logrequest, logmessage, logmessage)
        return HttpResponse("Booking does not exist", status=403)

    try:
        status_req = data[ServerConstants.STATUS]
    except KeyError as e:
        logmessage.append("Status not sent")
        backendlogger.error('update_booked_investigation', logrequest, logmessage, logmessage)
        return HttpResponse("Status not sent", status=403)


    booking_req.status = status_req
    booking_req.save()

    httpoutput = utils.successJson(dict())
    logmessage.append('updated booking id: {} with status {}'.format(booking_req.id,status_req))
    backendlogger.info('update_booked_investigation', logrequest, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)



def book_investigation(request):
    data = json.loads(request.body)
    logmessage = []
    logrequest = {}
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('book_investigation', data, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        appuid = data[ServerConstants.APPUID]
    except KeyError as e:
        logmessage.append("Kiosk Id not found")
        backendlogger.error('book_investigation', logrequest, logmessage, logmessage)
        return HttpResponse("Kiosk Id not found", status=403)

    try:
        investigation_id = data[ServerConstants.INVESTIGATIONID]
    except KeyError as e:
        logmessage.append("Investigation Id not found")
        backendlogger.error('book_investigation', logrequest, logmessage, logmessage)
        return HttpResponse("Investigation Id not found", status=403)

    try:
        personuid = data[ServerConstants.PERSONUID]
    except KeyError as e:
        logmessage.append("Person id not found")
        backendlogger.error('book_investigation', logrequest, logmessage, logmessage)
        return HttpResponse("Person id not found", status=403)

    try:
        appdetails = AppDetails.objects.get(appuid=appuid)
    except ObjectDoesNotExist as e:
        logmessage.append("WebApp not found")
        backendlogger.error('book_investigation', logrequest, logmessage, logmessage)
        return HttpResponse("WebApp not found", status=403)

    try:
        investigation = Investigation.objects.get(id=investigation_id)
    except ObjectDoesNotExist as e:
        logmessage.append("Investigation not found")
        backendlogger.error('book_investigation', logrequest, logmessage, logmessage)
        return HttpResponse("Investigation not found", status=403)

    try:
        person = PersonDetails.objects.get(personuid=personuid)
    except ObjectDoesNotExist as e:
        logmessage.append("Person not found")
        backendlogger.error('book_investigation', logrequest, logmessage, logmessage)
        return HttpResponse("Person not found", status=403)

    try:
        status_req = data[ServerConstants.STATUS]
    except KeyError as e:
        status_req = BookedInvestigationConstant.BOOKED

    healthcase = None
    try:
        healthcaseid_req = data[ServerConstants.HEALTHCASEID]
        healthcase = HealthCase.objects.get(healthcaseid = healthcaseid_req)
    except KeyError as e:
        pass
    except ObjectDoesNotExist as e:
        logmessage.append("Invalid health case id")
        backendlogger.error('book_investigation', logrequest, logmessage, logmessage)
        return HttpResponse("Invalid health case id", status=403)

    try:
        new_booking = BookedInvestigation.objects.create(person=person, investigation=investigation,
                                                         appdetails=appdetails,status=status_req,healthcase=healthcase,
                                                         uploaderrole="patient", uploaderuid=personuid)
    except :
        logmessage.append("Booking failed")
        backendlogger.error('book_investigation', logrequest, logmessage, logmessage)
        return HttpResponse("Booking failed", status=403)

    # Send booking confirmation sms
    # if status_req == BookedInvestigationConstant.BOOKED:
    #      sendSMS(BOOK_INVESTIGATION_MESSAGE.format(investigation.mrp), person.phone)
    httpoutput = utils.successJson(dict({'booking_id': new_booking.id}))
    logmessage.append('created new booking of id: {}'.format(new_booking.id))
    backendlogger.info('book_investigation', logrequest, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


def get_booked_investigations(request):

    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('get_booked_investigations', logrequest, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    start_date = request.GET.get(ServerConstants.STARTDATE)
    end_date = request.GET.get(ServerConstants.ENDDATE)
    personuid = request.GET.get(ServerConstants.PERSONUID)
    labid = request.GET.get(ServerConstants.LABID)

    if personuid:
        try:
            PersonDetails.objects.get(personuid=personuid)
        except (KeyError,ObjectDoesNotExist) as e:
            backendlogger.error('get_booked_investigations', logrequest, e, logmessage)
            return HttpResponse("invalid user_id", status=404)

    kwargs = {}
    if start_date:
        start = datetime.datetime.strptime(start_date, '%d-%m-%Y').replace(hour=0, minute=0, second=0)
        kwargs['booking_date__gte'] = start

    if end_date:
        end = datetime.datetime.strptime(end_date, '%d-%m-%Y').replace(hour=23, minute=59, second=59)
        kwargs['booking_date__lte'] = end

    if personuid:
        kwargs['person__personid'] = personuid

    if labid:
        kwargs['investigation__labdetails__id'] = labid

    try:
        bookedinvestigations = BookedInvestigation.objects.filter(**kwargs).order_by('-updated_at')
    except ObjectDoesNotExist as e:
        logmessage.append(e)
        backendlogger.error('get_booked_investigations', logrequest, logmessage, logmessage)
        return HttpResponse("No investigation found for this user",status=403)

    data = [investigation.get_dict() for investigation in bookedinvestigations]
    #print([investigation.get_dict() for investigation in bookedinvestigations])

    httpoutput = utils.successJson(dict({ServerConstants.INVESTIGATIONLIST:data}))
    backendlogger.info('get_investigation_suggestions', logrequest, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


def get_investigation_suggestions(request):
    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getInvestigationSuggestion', logrequest, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        labid_req = request.GET[ServerConstants.LABID]
        labdetails = LabDetails.objects.get(id=labid_req)
    except (KeyError, ObjectDoesNotExist) as e:
        logmessage.append("Lab not found")
        backendlogger.error('get_investigation_for_lab', logrequest, logmessage, logmessage)
        return HttpResponse("Lab not found", status=403)

    try:
        searchstr_req = request.GET[ServerConstants.SEARCHSTR]
    except (KeyError, ObjectDoesNotExist) as e:
        logmessage.append("searh str not found")
        backendlogger.error('get_investigation_for_lab', logrequest, logmessage, logmessage)
        return HttpResponse("searh str not found", status=403)

    investigationlist = []

    try:
        suggestions = Investigation.objects.filter(investigationname__icontains=searchstr_req,labdetails=labdetails).order_by("investigationname")[:10]
        if suggestions:
            for investigation in suggestions:
                investigationlist.append(investigation.get_dict())
        else:
            backendlogger.error('get_investigation_suggestions', logrequest, 'No test found using this keywords', logmessage)
            return HttpResponse("No test found using this keywords", status=403)
    except Exception as e:
        print(e)
        backendlogger.error('get_investigation_suggestions', logrequest, 'No test found using this keywords', logmessage)
        return HttpResponse("No test found using this keywords", status=403)

    httpoutput = utils.successJson(dict({ServerConstants.INVESTIGATIONLIST:investigationlist}))
    backendlogger.info('get_investigation_suggestions', logrequest, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)


def get_investigations_for_lab(request):
    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value
    authenticated,djangouserstr = authenticateURL(request)
    if not authenticated:
        backendlogger.error('getInvestigationSuggestion', logrequest, 'Error authenticating user', logmessage)
        return HttpResponse("Error authenticating user", status=401)

    try:
        labid_req = request.GET[ServerConstants.LABID]
        labdetails = LabDetails.objects.get(id=labid_req)
    except (KeyError, ObjectDoesNotExist) as e:
        logmessage.append("Lab not found")
        backendlogger.error('get_investigation_for_lab', logrequest, logmessage, logmessage)
        return HttpResponse("Lab not found", status=403)

    investigationlist = []
    investigations = Investigation.objects.filter(labdetails = labdetails)
    for investigation in investigations:
        investigationlist.append(investigation.get_dict())

    httpoutput = utils.successJson(dict({ServerConstants.INVESTIGATIONLIST: investigationlist}))
    backendlogger.info('get_investigations_for_lab', logrequest, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)

def get_all_tests_of_lab(request):
    logrequest = {}
    logmessage = []
    for key, value in request.GET.items():
        logrequest[key] = value
    # authenticated,djangouserstr = authenticateURL(request)
    # if not authenticated:
    #     backendlogger.error('getInvestigationSuggestion', logrequest, 'Error authenticating user', logmessage)
    #     return HttpResponse("Error authenticating user", status=401)

    try:
        labid_req = request.GET[ServerConstants.LABID]
        labdetails = LabDetails.objects.get(id=labid_req)
    except (KeyError, ObjectDoesNotExist) as e:
        logmessage.append("Lab not found")
        backendlogger.error('get_investigation_for_lab', logrequest, logmessage, logmessage)
        return HttpResponse("Lab not found", status=403)

    investigationlist = []

    try:
        suggestions = Investigation.objects.filter(labdetails=labdetails).order_by("investigationname")
        if suggestions:
            for investigation in suggestions:
                investigationlist.append(investigation.get_dict())
        else:
            backendlogger.error('get_investigation_suggestions', logrequest, 'No test found using this keywords', logmessage)
            return HttpResponse("No test found using this keywords", status=403)
    except Exception as e:
        print(e)
        backendlogger.error('get_investigation_suggestions', logrequest, 'No test found using this keywords', logmessage)
        return HttpResponse("No test found using this keywords", status=403)

    httpoutput = utils.successJson(dict({ServerConstants.INVESTIGATIONLIST:investigationlist}))
    backendlogger.info('get_investigation_suggestions', logrequest, httpoutput, logmessage)
    return HttpResponse(httpoutput, content_type=ServerConstants.RESPONSE_JSON_TYPE)