from django.db import models
from django.forms import model_to_dict

from HealthCase.models import HealthCase
from Doctor.models import DoctorDetails,AppDetails
from Person.models import PersonDetails
from django.utils import timezone

from backend_new.Constants import ServerConstants, BookedInvestigationConstant
class LabDetails(models.Model):
    name = models.CharField(max_length=255, null=False)
    address = models.CharField(max_length=1024, null=True)
    city = models.CharField(max_length=20, null=False)
    code = models.CharField(max_length=50, null=False, unique=True)

    def __str__(self):
        return '{}-{}-{}'.format(self.name, self.city, self.code)

class Investigation(models.Model):

    labdetails = models.ForeignKey(LabDetails, null=False, on_delete=models.CASCADE)
    investigationcode = models.CharField(max_length=10, null=True, blank=True)
    investigationname = models.CharField(max_length=200, null=True, blank=True)
    includedtests = models.TextField(null=True, blank=True)
    pricetolab = models.FloatField(default=0, null=False)
    pricetoperson = models.FloatField(default=0, null=False)
    marketprice = models.FloatField(default=0.00, null=False, blank=False)

    def __str__(self):
        return '{}-{}'.format(self.investigationname, self.labdetails.name)

    def get_dict(self):
        res = model_to_dict(self, fields=(
        'id','labdetails', 'investigationcode', 'investigationname', 'includedtests', 'marketprice',
         'pricetoperson','pricetolab'))
        return res


class LabReport(models.Model):

    person = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)
    reporturl = models.CharField(max_length=512,null=False,blank=False)
    reporttitle = models.CharField(max_length=50,null=False,blank=False)
    uploaded_at = models.DateTimeField(auto_now=True)
    uploaderrole = models.CharField(max_length=50,default="patient")
    uploaderuid = models.CharField(max_length=10, null=True,default="6gm8qy6")

    def __str__(self):
       return '%s %s' % (str(self.id), str(self.person.name))

    def get_dict(self):
        return model_to_dict(self, fields=('id', 'reporturl', 'reporttitle'))


class BookedInvestigation(models.Model):
    BOOKED_INVESTIGATION_STATUSES = ((BookedInvestigationConstant.PRESCRIBED, 'Prescribed'),(BookedInvestigationConstant.BOOKED, 'Booked'), (BookedInvestigationConstant.INPROCESS, 'Inprocess'),
                                     (BookedInvestigationConstant.COMPLETED, 'Completed'), (BookedInvestigationConstant.CANCELLED, 'Cancelled'), (BookedInvestigationConstant.SAMPLE_COLLECTED, 'Sample Collected'))
    person = models.ForeignKey(PersonDetails, null=False, on_delete=models.CASCADE)
    labreport = models.ForeignKey(LabReport, null=True, blank=True, on_delete=models.CASCADE)
    investigation = models.ForeignKey(Investigation, on_delete=models.CASCADE)
    appdetails = models.ForeignKey(AppDetails, null=False, blank=False, on_delete=models.CASCADE)
    booking_date = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    status = models.IntegerField(choices=BOOKED_INVESTIGATION_STATUSES, default=BookedInvestigationConstant.BOOKED)
    healthcase = models.ForeignKey(HealthCase, null=True, blank=True,on_delete=models.CASCADE)
    uploaderrole = models.CharField(max_length=50, default="patient")
    uploaderuid = models.CharField(max_length=10, null=True, default="6gm8qy6")

    def __str__(self):
        return str(self.id)

    def get_dict(self):
        res = {}
        res[ServerConstants.BOOKINGID] = self.id
        res[ServerConstants.PERSONUID] = self.person.personuid
        res[ServerConstants.USERNAME] = self.person.name
        res[ServerConstants.AGE] = self.person.get_age()
        res[ServerConstants.GENDER] = self.person.gender
        res[ServerConstants.APPUID] = self.appdetails.appuid
        res[ServerConstants.APPLOCATION] = self.appdetails.applocation
        res[ServerConstants.LABREPORTURL] = self.labreport.reporturl if self.labreport else None
        res[ServerConstants.INVESTIGATIONNAME] = self.investigation.investigationname
        res[ServerConstants.MARKETPRICE] = self.investigation.marketprice
        res[ServerConstants.PRICETOPERSON] = self.investigation.pricetoperson
        res[ServerConstants.PRICETOLAB] = self.investigation.pricetolab
        res[ServerConstants.BOOKINGDATE] = self.booking_date.strftime('%d %b %Y')
        res[ServerConstants.STATUSSTR] = self.get_status_display()
        res[ServerConstants.STATUS] = self.status
        return res


class AppLabMapping(models.Model):
    labdetails = models.ForeignKey(LabDetails, on_delete=models.CASCADE)
    appdetails = models.ForeignKey(AppDetails, on_delete=models.CASCADE)

    def __str__(self):
        return '{}-{}'.format(self.labdetails.name,self.appdetails.apptag)


class PersonLabMapping(models.Model):
    labdetails = models.ForeignKey(LabDetails, on_delete=models.CASCADE)
    persondetails = models.ForeignKey(PersonDetails, on_delete=models.CASCADE)

    def __str__(self):
        return '{}-{}'.format(self.labdetails.name,self.persondetails.personuid)