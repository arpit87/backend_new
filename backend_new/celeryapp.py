from __future__ import absolute_import, unicode_literals

import os

from celery.app.base import Celery

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'backend_new.settings')

app = Celery('backend_new')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()

app.conf.update(
    result_expires=3600,
)
import logging

@app.task(bind=True)
def debug_task(self):
    logging.getLogger('celery').info('Request: {0!r}'.format(self.request))