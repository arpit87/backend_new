# from django.conf.urls import include, url # Original
from django.conf.urls import url #Extra
from django.urls import include, path #Extra
from django.contrib import admin
admin.autodiscover()

urlpatterns = [
    # Examples:
    # url(r'^$', 'backend_server.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),


    url(r'^api/super/', admin.site.urls),
    url(r'^api/Appointments/', include('Appointments.urls')),
    url(r'^api/Doctor/',include('Doctor.urls')),
    url(r'^api/Person/',include('Person.urls')),
    url(r'^api/UploadFiles/',include('UploadFiles.urls')),
    url(r'^api/Communications/',include('Communications.urls')),
    url(r'^api/Payment/',include('Payment.urls')),
    url(r'^api/BodyVitals/',include('BodyVitals.urls')),
    url(r'^api/Platform/',include('Platform.urls')),
    url(r'^api/HealthPackage/',include('HealthPackage.urls')),
    url(r'^api/HealthCase/', include('HealthCase.urls')),
    url(r'^api/QueueManagement/', include('QueueManagement.urls')),
    url(r'^api/Lab/', include('Lab.urls')),
    url(r'^api/VideoConference/', include('VideoConference.urls')),
    url(r'^api/q/', include('UrlShortner.urls')),
    url(r'^api/WebApp/', include('WebApp.urls')),
    url(r'^api/UrlShortner/', include('UrlShortner.urls2')),
    url(r'^api/Marketing/', include('Marketing.urls')),
    url(r'^api/PatientMonitor/', include('PatientMonitor.urls')),
]
