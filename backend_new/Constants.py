from backend_new import config
from django.test.client import MULTIPART_CONTENT


class ServerConstants:
    MAIL_TYPE = "mail_type"
    ITEMTYPE = "itemtype"
    APPSECRET = "appsecret"

    HIVMACHINE = "hivmachine"
    MALARIAMACHINE = "malariamachine"
    C = "c"
    T1 = "t1"
    T2 = "t2"
    TYPHOIDMACHINE = "typhoidmachine"
    MC = "mc"
    MT = "mt"
    GC = "gc"
    GT = "gt"
    DENGUEMACHINE = "denguemachine"
    NS1 = "ns1"
    PREGNANCYMACHINE = "pregnancymachine"
    T = "t"
    URINEMACHINE = "urinemachine"
    GLUCOSE = "glucose"
    BILIRUBIN = "bilirubin"
    KETONE = "ketone"
    GRAVITY = "gravity"
    BLOOD = "blood"
    PH = "ph"
    PROTEIN = "protein"
    UROBILINOGEN = "urobilinogen"
    NITRITE = "nitrite"
    LEUKOCYTES = "leukocytes"

    HEMOGLOBINMACHINE = "hemoglobinmachine"
    LIPIDMACHINE = "lipidmachine"
    HB = "hb"
    HCT = "hct"
    TC = "tc"
    TG = "tg"
    HDL = "hdl"
    LDL = "ldl"
    HDLRATIO = "hdlratio"
    NONHDL = "nonhdl"

    SPIROMETERMACHINE = "spirometermachine"
    PEF = "pef"
    FVC = "fvc"
    FEV1 = "fev1"
    FEV1PER = "fev1per"
    FEF25 = "fef25"
    FEF75 = "fef75"
    FEF2575 = "fef2575"

    RESPONSE_XML_TYPE = 'application/xml'
    RESPONSE_JSON_TYPE = 'application/json'
    RESPONSE_TEXT_TYPE = 'text/html'
    MULTIPART_CONTENT = MULTIPART_CONTENT

    STARTDATE = "startdate"
    ENDDATE = "enddate"
    GET_REQUEST = "GET"
    POST_REQUEST = "POST"

    QBSESSIONTOKEN = "qbsessiontoken"

    TEST = "test"
    APPNAME = 'appname'
    ITEAMTYPE = 'iteamtype'

    ALLITEMARRAY = 'allitemarray'
    SERVER_V = 'server_v'
    KIOSK_V = 'kiosk_v'
    RPI_V = 'rpi_v'

    DOMAIN = 'domain'
    USERFEEDBACK = 'userfeedback'
    MAXFEEDBACK = 'maxfeedback'
    PERSONUID = 'personuid'
    PREFERREDLANGUAGE = 'preferredlanguage'
    PERSONDATA = 'persondata'
    DOCTORUID = 'doctoruid'
    AGENT_ID = 'agentid'
    AGENTPHONE = 'agentphone'
    HIERARCHY = "hierarchy"

    # data
    TOTALCOUNT = "totalcount"
    COUNTLIST = "countlist"

    COMPANYNAME = 'companyname'
    COMPANYDOMAIN = 'companydomain'

    # SMS
    HASH = "hash"
    SENDERID = "senderid"
    ISUNICODE = "isunicode"
    COUNT = "count"

    # createdoctor
    PHONE = 'phone'
    USERPHONE = 'userphone'
    DOCTORPHONE = 'doctorphone'
    OLDDOCTORPHONE = 'olddoctorphone'
    NEWDOCTORPHONE = 'newdoctorphone'
    EMAIL = 'email'
    AGE = 'age'
    DOB = 'dob'
    GENDER = 'gender'
    YEAROFBIRTH = "yearofbirth"
    QBUSERNAME = "qbusername"
    QBPASSWORD = "qbpassword"
    QBID = "qbid"
    QBTOKEN = "qbtoken"
    QBSESSIONID = "qbsessionid"
    QBAPPID = "qbappid"
    QBAUTHKEY = "qbauthkey"
    QBCALLERID = "qbcallerid"
    QBAUTHSECRET = "qbauthsecret"
    QBACCOUNTKEY = "qbaccountkey"
    SKILLSTR = "skillstr"
    SKILLARRAY = "skillarray"
    SKILLSELECTED = "skillselected"
    SKILLNAMESTR = "skillnamestr"
    DOCTORGROUPS = "doctorgroups"
    DOCTORAPPUUID = 'doctorappuuid'
    USERAPPUUID = 'userappuuid'
    LATITUDE = "latitude"
    ISACTIVE = "isactive"
    LONGITUDE = "longitude"
    LANDMARK = "landmark"
    FLAT = "flat"
    LOCALITY = "locality"
    BOOKINGID = "bookingid"
    NEWBOOKINGID = "newbookingid"
    BOOKINGSTATUSDOCTOR = "bookingstatusdoctor"
    SERVICESOFFERED = "servicesoffered"
    BOOKINGSTATUS = "bookingstatus"
    BOOKINGLIST = "bookinglist"
    ISBOOKED = "isbooked"
    BOOKINGFOR = "bookingfor"
    DATETIME = "datetime"
    USERNAME = "username"
    KIOSKUSERNAME = "kioskusername"
    PERSONLIST = "personlist"
    USERDEMOGRAPHICS = "userdemographics"
    DOCTORNAME = "doctorname"
    AUTH_LEVEL = "authlevel"
    DOCTORGENDER = "gender"
    DOCTORPASSWORD = "password"
    DOCTORNOTES = "notes"
    DOCTORBIOSKETCH = "biosketch"
    ADDRESS = "address"
    ADDRESS1 = "address1"
    ADDRESS2 = "address2"
    MOBILE = "mobile"
    DOCTORLIST = "doctorlist"
    FCMTOKEN = "fcmtoken"
    FCMRESPONSE = "fcmresponse"
    PERSONTYPE = "persontype"
    FILENAMES = "filenames"
    FILEURLS = "fileurls"
    SAVEDFILESURL = "savedfilesurl"
    SMSMESSAGE = "message"
    DOCURL = "docurl"
    URL = "url"
    REGISTRATIONNUM = "registrationnum"
    SPECIALIZATION = "specialization"
    QUALIFICATION = "qualification"
    ISFORHOMECARE = "isforhomecare"
    ISFORTELEMEDICINE = "isfortelemedicine"
    PROFILEPIC = "profilepic"
    SIGNPIC = "signpic"
    DOCTORDOCLIST = "doctordoclist"
    EXPERIENCE = "experience"
    LASTREQUESTAT = "lastrequestat"
    HOSPITALNAME = "hospitalname"
    GROUPNAME = "groupname"
    GROUPACTION = "groupaction"
    GROUPS = "groups"

    # exotel
    CALLDIRECTION = "calldirection"
    CALLSID = "CallSid"
    EVENTTYPE = "EventType"
    EXOSTATUS = "Status"
    EXOSTARTTIME = "StartTime"
    EXOENDTIME = "EndTime"
    RECORDINGURL = "RecordingUrl"
    CONVERSATIONDURATION = "ConversationDuration"
    VIRTUALNUMBER = "VirtualNumber"
    NUMBER = "Number"
    LANGUAGE = "Language"

    # health package
    PACKAGELIST = "packagelist"
    PACKAGEID = "packageid"
    PACKAGECODE = "packagecode"
    PACKAGEFEE = "packagefee"
    PACKAGEDURATION = "packageduration"
    PACKAGENAME = "packagename"
    PACKAGETYPE = "packagetype"
    PACKAGEDESCRIPTION = "packagedescription"
    PACKAGEDETAILS = "packagedetails"
    PAYMENTDONE = "paymentdone"
    REGISTRATIONDATE = "registrationdate"
    PAYUAUTHTOKEN = "payuauthtoken"
    ENROLLUID = "enrolluid"
    ENROLLDATE = "enrolldate"
    ENROLLMENTSTATUS = "enrollmentstatus"

    # appointment booking
    STARTTIME = "starttime"
    STARTTIME_TIMESTAMP = "starttime_timestamp"
    DATE = "date"
    DAY = "day"
    SLOTLENGTH = "slotlength"
    SLOTNO = "slotno"
    SLOTBOOKINGID = "slotbookingid"
    SLOTBOOKED = "slotbooked"
    SLOTDETAILS = "slotdetails"
    SLOTDETAILSLENGTH = "slotdetailslength"
    BOOKEDSLOTLIST = "bookedslotlist"

    # upload
    FILETYPE = "filetype"
    CALLDURATION = "callduration"
    CALLSTATUS = "callstatus"
    UPLOADERROLE = "uploaderrole"
    UPLOADERNAME = "uploadername"
    UPLOADERUID = "uploaderuid"

    # doctor calender
    WEEKLYCALENDER = "weeklycalender"
    DAYOFWEEK = "dayofweek"
    AVAILABILITYSTART = "availabilitystart"
    AVAILABILITYEND = "availabilityend"
    AVAILABILITYLIST = "availabilitylist"
    WEEKS = "weeks"
    DAYS = "days"

    # create user
    EXTERNALID = "externalid"
    EXTERNALSYSTEM = "externalsystem"

    PARAMNAME = "paramname"
    NEWVALUE = "newvalue"

    # doc availability
    ENDTIME = "endtime"
    ENDTIME_TIMESTAMP = "endtime_timestamp"
    AVAILABILITYID = "availabilityid"
    RECURRINGID = "recurringid"
    CONSULTSTATUS = "consultstatus"
    CUSTOMLIST = "customlist"

    # search
    SEARCHSTR = "searchstr"
    SEARCHFIELD = "searchfield"

    # login
    PASSWORD = "password"
    USERTYPE = "usertype"

    # contact us
    CONTACTUSEMAIL = "email"
    CONTACTUSPHONE = "phone"
    CONTACTUSMESSAGE = "message"
    CONTACTUSSUBJECT = "subject"
    CONTACTUSNAME = "name"

    # email
    EMAILDATA = "emaildata"
    SUBJECT = "subject"

    # support
    MESSAGE = "message"

    # generateInvoice
    SERVICEID = "serviceid"
    SERVICETITLE = "servicetitle"
    SERVICETAG = "servicetag"
    SERVICECHARGES = "servicecharges"
    SERVICEDESCRIPTION = "servicedescription"
    SERVICELIST = "servicelist"
    VCRECEIPTLIST = "vcreceiptlist"

    # receipt
    INVOICENUM = "invoicenum"
    INVOICEDETAILSARRAY = "invoicedetailsarray"
    REPORTINVOICELIST = "reportinvoicelist"
    INVOICEAMOUNT = "invoiceamount"
    UPDATED_AT = "updated_at"
    INVOICECOUNT = "invoicecount"
    INVOICEAMOUNTUNPAID = "invoiceamountunpaid"
    INVOICEDATA = "invoicedata"
    INVOICELIST = "invoicelist"

    CHECKUPSTR = "checkupstr"

    FINGERPRINTDATA = "fingerprintdata"
    FINGERPRINTMATCHED = "fingerprintmatched"

    # transaction
    TRANSACTIONID = "transactionid"
    TRANSACTIONAMOUNT = "transactionamount"
    TRANSACTIONSTATUS = "transactionstatus"
    TRANSACTIONTYPE = "transactiontype"
    TRANSACTIONLIST = "transactionlist"
    TRANSACTIONDETAILS = "transactiondetails"
    TOTALTRANSACTION = "totaltransaction"
    TRANSACTIONCOUNT = "transactioncount"
    TRANSACTIONUNPAID = "transactionunpaid"
    TRANSACTIONDESCRIPTION = "transactiondescription"
    TRANSACTIONEXTRAS = "transactionextras"

    OTP = "otp"

    REPORTDATA = "reportdata"

    BASICREPORTDATA = "basicreportdata"

    RECIPIENT = "recipient"

    PARTNER = "partner"
    SUBPARTNER = "subpartner"

    BPMACHINE = "bpmachine"
    SYSTOLIC = "systolic"
    DIASTOLIC = "diastolic"
    PULSE = "pulse"
    WEIGHTMACHINE = "weightmachine"
    BMI = "bmi"
    BONEMASS = "bonemass"
    FAT = "fat"
    HYDRATION = "hydration"
    MUSCLE = "muscle"
    KCAL = 'kcal'
    VFAT = "vfat"
    SFAT = "sfat"
    BAGE = "bage"
    PROT = "prot"
    MUSCLEQUALITYSCORE = "muscle_quality_score"
    PHYSIQUERATING = "physique_rating"
    METABOLICAGE = "metabolic_age"
    BASALMETABOLICRATING = "basal_metabolic_rating"
    WEIGHT = "weight"
    HEIGHTMACHINE = "heightmachine"
    HEIGHT = "height"
    SPIROMETERMACHINE = "spirometermachine"
    OXIMETERMACHINE = "oximetermachine"
    OXYGENSAT = "oxygensat"
    HEMOGLOBINMACHINE = "hemoglobinmachine"
    LIPIDMACHINE = "lipidmachine"
    TEMPERATUREMACHINE = "temperaturemachine"
    TEMPERATURE = "temperature"
    BLOODGLUCOSEMACHINE = "bloodglucosemachine"
    BLOODGLUCOSE = "glucose_level"
    TESTTYPE = "testtype"
    TESTTYPECODE = "testtypecode"
    MEASUREMETHOD = "measuremethod"
    TESTSTAKEN = "teststaken"
    DENGUEMACHINE = "denguemachine"
    NS1 = "ns1"
    PREGNANCYMACHINE = "pregnancymachine"
    T = "t"
    GLUCOSE = "glucose"
    ECGMACHINE = "ecgmachine"
    REPORTID = "reportid"
    REPORTKEY = "reportkey"
    REPORTTYPE = "reporttype"
    REPORTCOMMENT = "report_comment"
    REPORTLIST = "reportlist"
    UNIQUEID = "uniqueid"
    RECOMMENDATIONS = "recommendations"

    MACHINENAME = "machinename"

    NUMOFCOMMENTS = "numofcomments"

    # Trends
    VALUE = "value"

    # ECG data
    LEAD1 = "lead1"
    LEAD2 = "lead2"
    LEAD3 = "lead3"
    AVR = "avr"
    AVL = "avl"
    AVF = "avf"
    V1 = "v1"
    V2 = "v2"
    V3 = "v3"
    V4 = "v4"
    V5 = "v5"
    V6 = "v6"

    ECG_IMG = "ecg_img"
    LEADNAME = "leadname"
    LEADDATA = "leaddata"

    # prescription_data
    PRESCRIPTIONDATA = "prescriptiondata"
    PRESCRIPTION = "prescription"
    INVESTIGATION = "investigation"
    PRESCRIPTIONID = "prescriptionid"
    PRESCRIPTIONKEY = "prescriptionkey"
    DOCTORQUALIFICATIONS = "doctorqualifications"
    DOCTOREXPERIENCE = "doctorexperience"
    DOCTORSPECIALIZATION = "doctorspecialization"
    NOTES = "notes"
    CLINICALNOTE = "clinicalnote"
    DIAGNOSIS = "diagnosis"
    RECOMMENDATION = "recommendation"
    FOLLOWUPDAYS = "followupdays"
    FOLLOWUPDATETIME = "followupdatetime"

    # doctor online status
    ONLINESTATUS = "onlinestatus"
    ISBUSY = "isbusy"
    ISONLINE = "isonline"

    # healthreport
    HEALTHHISTORY = "healthhistory"
    HISTORYTYPE = "historytype"

    # getDoctors
    OFFSET = "offset"
    LIMIT = "limit"

    # getallPersons
    TOTALPAGECOUNT = "totalpagecount"
    SEARCHPARAMETER = "searchParameter"
    SEARCHPARAMETERVALUE = "searchParameterValue"

    UPCOMINGCONSULTLIST = "upcomingconsultlist"
    PASTCONSULTLIST = "pastconsultlist"

    ISHOSPITALCOORDINATOR = "ishospitalcoordinator"
    COORDINATORMAPPEDDOCTORS = "coordinatormappeddoctors"
    CONSULTATIONTYPE = "consultation_type"
    CONSULTATIONTYPESTR = "consultation_typestr"
    COORDINATORBOOKSFORDOCTOR = "coordinatorbooksfordoctor"

    # doctor categories
    CATEGORYNAME = "categoryname"
    CATEGORYDESCRIPTION = "categorydescription"
    CATEGORYLIST = "categorylist"
    CATEGORYTYPE = "categorytype"
    SKILLNUM = "skillnum"
    NUMOFDOCS = "numofdocs"

    # IMAGE report
    IMAGEREPORTTITLE = "reporttitle"
    IMAGEREPORTDESCRIPTION = "reportdescription"
    IMAGEREPORTIMGURL = "reportimgurl"
    IMAGEREPORTID = "reportid"
    IMAGEREPORTKEY = "reportkey"

    # Display images
    IMAGEURL = "imageurl"

    # IMAGE report
    REPORTTITLE = "reporttitle"
    REPORTDESCRIPTION = "reportdescription"

    # APP
    APPLOCATION = "applocation"
    APPUID = "appuid"
    APP_ID = "appid"
    APPTOKEN = "apptoken"
    APPMACID = "appmacid"
    APPTAG = "apptag"
    APPTYPE = "apptype"
    APPDESCRIPTION = "appdescription"
    APPLIST = "applist"
    APPSTR = "appstr"
    SHOWHIDDEN = "showhidden"
    APPSERVICESLIST = "appserviceslist"
    INCLINICDOCTORLIST = "inclinicdoctorlist"
    MAPPEDDOCTORLIST = "mappeddoctorlist"
    HOST = "host"
    HOSTSHORTURL = "hostshorturl"
    SLOGAN = "slogan"
    TEMPLATETYPE = "templatetype"
    SPECIALITY = "speciality"
    CONTACTNUMBERS = "contactnumbers"
    IMAGESSTR = "imagesstr"
    LOGO = "logo"
    BRANDNAME = "brandname"
    DYNAMICLINK = "dynamiclink"

    DEFAULTLANGUAGE = "defaultlanguage"
    OFFICIALEMAIL = "officialemail"
    OXIMETER = "oximeter"
    BPMACHINE = "bpmachine"
    ECGMACHINE = "ecgmachine"
    TEMPERATURE = "temperature"
    GLUCOSEMACHINE = "glucosemachine"
    FINGERPRINT = "fingerprint"
    HEIGHTMACHINE = "heightmachine"
    PARTNER = "partner"
    PARTNERPHONE = "partnerphone"
    PARTNEREMAIL = "partneremail"
    PARTNERWEBSITE = "partnerwebsite"
    PARTNERADDRESS = "partneraddress"
    INITIALCOUNTRY = "initialCountry"
    PREFERREDCOUNTRIES = "preferredCountries"

    # clinic
    CLINICCODE = "cliniccode"

    # lab report
    LABENABLED = "labenabled"
    LABLIST = "lablist"
    LABID = "labid"
    LABNAME = "labname"
    LABREPORTTITLE = "reporttitle"
    LABREPORTDESCRIPTION = "reportdescription"
    LABREPORTURL = "reporturl"
    LABREPORTID = "reportid"
    LABREPORTKEY = "reportkey"
    INVESTIGATIONLIST = "investigationlist"
    BOOKEDINVESTIGATION = "bookedinvestigation"
    INVESTIGATIONID = "investigationid"
    INVESTIGATIONCODE = "investigationcode"
    INVESTIGATIONNAME = "investigationname"
    MARKETPRICE = "marketprice"
    BOOKINGDATE = "bookingdate"
    STATUSSTR = "statusstr"
    SUGGESTEDLIST = "suggestedlist"
    PRICETOLAB = "pricetolab"
    PRICETOPERSON = "pricetoperson"

    CLIENTNAME = "clientname"
    MACHINESTR = "machinestr"
    BPMACHINEHEADER = "bpmachineheader"

    # vital range
    HIGH = "high"
    NORMAL = "normal"
    LOW = "low"
    TOTAL = "total"
    INVALID = "invalid"
    NEGATIVE = "negative"
    POSITIVE = "positive"
    GOOD = "good"

    # queue management
    TVAPPID = "tvappid"
    QUEUEAPPID = "queueappid"

    APPSESSIONTOKEN = "appsessiontoken"

    # comments
    COMMENT = "comment"
    COMMENTID = "commentid"
    COMMENTTEXT = "commenttext"
    COMMENTERROLE = "commenterrole"
    COMMENTERUID = "commenteruid"
    COMMENTER = "commenter"
    LOCATION = "location"
    LOCATIONARRAY = "locationarray"

    HEALTHCASEID = "healthcaseid"
    HEALTHCASEDETAILS = "healthcasedetails"
    VIDEOID = "videoid"
    AUDIOID = "audioid"
    VIDEO = "video"
    AUDIO = "audio"
    ATTACHEDREPORT = "attachedreport"
    ISCLOSED = "isclosed"
    STATUS = "status"
    STATUSCODE = "statuscode"
    STATUSNOTE = "statusnote"
    COMPLETENOTE = "completenote"
    REPORTNOTE = "reportnote"
    REJECTNOTE = "rejectnote"
    CONSULTLIST = "consultlist"
    HEALTHCASELIST = "healthcaselist"
    VIDEOURLLIST = "videourllist"
    VIDEOURL = "videourl"
    AUDIOURL = "audiourl"
    VIDEOCOUNT = "videocount"
    PROBLEM = "problem"
    TAGS = "tags"
    DETAILS = "details"
    ROLE = "role"
    NAME = "name"
    DATA = "data"
    ID = "id"
    UID = "uid"
    ATTACHEDREPORTLIST = "attachedreportlist"
    PersonDetails = "PersonDetails"
    DOCTORDETAILS = "doctordetails"
    OPENCONSULTCOUNT = "openconsultcount"

    # vc analystics
    HEALTHCASESACTIVITY = "healthcasesactivity"
    VIDEOCONSULTAPPOINTMENTS = "videoconsultappointments"
    PHONECONSULTS = "phoneconsults"


    # license
    ISLICENSEVALID = "islicensevalid"
    ISDEVICEREGISTERED = "isdeviceregistered"
    LICENSEKEY = "licensekey"
    LICENSESTARTTIME = "licensestarttime"
    LICENSESTARTDATE = "licensestartdate"
    LICENSEENDTIME = "licenseendtime"
    LICENSEENDDATE = "licenseenddate"
    LICENSEENABLED = "licenseenabled"

    # appconfig
    APPCONFIG = "appconfig"

    # videoconf
    VCID = "vcid"
    VCCODE = "vccode"
    VCCODEDETAILS = "vccodedetails"
    VCCODEDETAILSLIST = "vccodedetailslist"
    VCCODEID = "vccodeid"
    VCTIME = "vctime"
    VCDATE = "vcdate"
    ISVALID = "isvalid"
    DOCTORQBID = "doctorqbid"
    PATIENTDETAILS = "patientdetails"
    SESSIONDATA = "sessiondata"

    TOKENNUMBER = "tokennumber"
    QUEUE_NUMBER = "queue_number"
    MONTH = "month"
    YEAR = "year"
    ETA = "eta"

    # sendsms bulk
    TO = "to"
    MSG = "msg"
    ROUTE = "route"

    VERIFICATIONKEY = "verificationkey"
    VERIFICATIONVALUE = "verificationvalue"

    CITYNAME = "cityname"

    DEVICEID = "deviceid"

    IMGDATA = "imgdata"
    DERMAIMG = "dermaimg"
    DERMASCOPEMACHINE = "dermascopemachine"
    OTOSCOPEMACHINE = "otoscopemachine"

    # websockets
    MESSAGE_TYPE = "message_type"
    GOTPRESCRIPTION = "gotprescription"
    CALLOVER = "call_over"
    COMMAND = "command"
    AVAILABILITYCHANGE = "availabilitychange"
    DOCTORRINGING = "doctorringing"
    CALLACCEPTEDFROMWEB = "callacceptedfromweb"

    # app update
    VERSIONCODE = "versioncode"
    FLAVOUR = "flavour"
    APPUPDATEAVAILABLE = "appupdateavailable"
    APKPATH = "apkpath"
    LATESTVERSIONCODE = "latestversioncode"
    ISMANDATORYUPDATE = "ismandatoryupdate"

    # urlshortner
    LONGURL = "longurl"
    SHORTURL = "shorturl"

    #webappemail
    PATIENTNAME = "patientname"
    PATIENTEMAIL = "patientemail"
    PATIENTPHONE = "patientphone"

    #healthcaselist
    LASTUPDATED = "lastupdated"
    DOCTORSCONSULTED = "doctorsconsulted"

    #hims permissions
    PERMISSIONS = "permissions"
    VIEW = "view"
    CREATE = "create"
    EDIT = "edit"
    DELETE = "delete"

    #rapid test json str
    RAPIDTESTREPORT = "rapidtestreport"
    URINETESTREPORT = "urinetestreport"
    EYETESTREPORT = "eyetestreport"

    #patient vital tracking
    ISTRACKED = "istracked"
    VITALID = "vitalid"
    VITALTYPE = "vitaltype"
    HEALTHCASEVITAL = "healthcasevital"
    INFERENCE = "inference"
    VITALVALUESLIST = "vitalvalueslist"
    VITALVALUE = "vitalvalue"
    ALLVITALSARRAY = "allvitalsarray"


    #marketing lead
    LEADID = "leadid"
    DESCRIPTION = "description"

    #snapshot
    SNAPSHOTID = "snapshotid"
    SNAPSHOTLIST = "snapshotlist"

    #vital tracking
    MACHINESDATA = "machinesdata"


class FileStorageConstants():
    # these dir are for localhost
    DOCTORPROFILEPICDIR = "doctorprofilepics"
    DOCTORSIGNPICDIR = "doctorsignpics"
    USERPROFILEPICDIR = "userprofilepics"
    DOCTORDOCSDIR = "doctordocs"
    IMAGEREPORTSDIR = "imagereports"
    CONSULTVIDEODIR = "consultvideo"
    AUDIOREPORTDIR = "audioreport"
    LABREPORTDIR = "labreport"
    APPBACKGROUNDIMAGEDIR = "apptemplatedata"
    APPLOGODIR = "apptemplatedata"

    # this is bucket name on google cloud
    if config.GCSIMAGEREPORTBUCKET:
        IMAGEREPORTSDIR = config.GCSIMAGEREPORTBUCKET
    if config.GCSCONSULTVIDEOBUCKET:
        CONSULTVIDEODIR = config.GCSCONSULTVIDEOBUCKET
    if config.GCSAUDIOREPORTBUCKET:
        AUDIOREPORTDIR = config.GCSAUDIOREPORTBUCKET
    if config.GCSAPPTEMPLATEIMAGESBUCKET:
        APPBACKGROUNDIMAGEDIR = config.GCSAPPTEMPLATEIMAGESBUCKET
        APPLOGODIR = config.GCSAPPTEMPLATEIMAGESBUCKET


class FileTypeConstants():
    DOCTORPROFILEPIC = str("profilepic")
    DOCTORSIGNPIC = str("signpic")
    DOCTORDOCPIC = str("docpic")
    IMAGEREPORT = str("imagereport")
    USERPROFILEPIC = str("userprofilepic")
    ECGLEADIMG = str("ecgleadimg")
    ECGIMG = str("ecgimg")
    CONSULTVIDEO = str("consultvideo")
    MOVERPROFILEPIC = str("profilepic")
    MOVERSIGNPIC = str("signpic")
    MOVERDOCPIC = str("docpic")
    DERMASCOPEIMG = str("dermascopeimg")
    OTOSCOPESCOPEIMG = str("otoscopeimg")
    ENDOSCOPEIMG = str("endoscopeimg")
    FUNDUSIMG = str("fundusimg")
    AUDIOREPORT = str("audioreport")
    LABREPORT = str("labreport")
    APPBACKGROUNDIMAGE = str("appbackgroundimage")
    APPLOGO = str("applogo")


FileTypeDIRStorageMap = {
    FileTypeConstants.DOCTORPROFILEPIC: FileStorageConstants.DOCTORPROFILEPICDIR,
    FileTypeConstants.DOCTORSIGNPIC: FileStorageConstants.DOCTORSIGNPICDIR,
    FileTypeConstants.USERPROFILEPIC: FileStorageConstants.USERPROFILEPICDIR,
    FileTypeConstants.DOCTORDOCPIC: FileStorageConstants.DOCTORDOCSDIR,
    FileTypeConstants.IMAGEREPORT: FileStorageConstants.IMAGEREPORTSDIR,
    FileTypeConstants.ECGLEADIMG: FileStorageConstants.IMAGEREPORTSDIR,
    FileTypeConstants.ECGIMG: FileStorageConstants.IMAGEREPORTSDIR,
    FileTypeConstants.CONSULTVIDEO: FileStorageConstants.CONSULTVIDEODIR,
    FileTypeConstants.AUDIOREPORT: FileStorageConstants.AUDIOREPORTDIR,
    FileTypeConstants.LABREPORT: FileStorageConstants.LABREPORTDIR,
    FileTypeConstants.APPBACKGROUNDIMAGE: FileStorageConstants.APPBACKGROUNDIMAGEDIR,
    FileTypeConstants.APPLOGO: FileStorageConstants.APPLOGODIR,
}

BMIRANGE = {
    "2": {"underweight": 14, "normal": 18, "overweight": 19},
    "3": {"underweight": 14, "normal": 17, "overweight": 18},
    "4": {"underweight": 13, "normal": 16, "overweight": 17},
    "5": {"underweight": 13, "normal": 16, "overweight": 18},
    "6": {"underweight": 13, "normal": 17, "overweight": 18},
    "7": {"underweight": 13, "normal": 17, "overweight": 19},
    "8": {"underweight": 13, "normal": 18, "overweight": 20},
    "9": {"underweight": 13, "normal": 18, "overweight": 21},
    "10": {"underweight": 14, "normal": 19, "overweight": 22},
    "11": {"underweight": 14, "normal": 20, "overweight": 23},
    "12": {"underweight": 14, "normal": 21, "overweight": 24},
    "13": {"underweight": 15, "normal": 21, "overweight": 25},
    "14": {"underweight": 15, "normal": 22, "overweight": 26},
    "15": {"underweight": 16, "normal": 23, "overweight": 27},
    "16": {"underweight": 17, "normal": 24, "overweight": 27},
    "17": {"underweight": 17, "normal": 25, "overweight": 28},
    "18": {"underweight": 18, "normal": 25, "overweight": 29},
    "19": {"underweight": 18, "normal": 26, "overweight": 30},
    "20": {"underweight": 18, "normal": 27, "overweight": 30},
    "other": {"underweight": 18, "normal": 27, "overweight": 30}
}

BPRANGE = {
    "3": {"systolic_range": {"low": 76, "normal": 112, "prehyper": 130},
          "diastolic_range": {"low": 46, "normal": 72, "prehyper": 80}},
    "4": {"systolic_range": {"low": 78, "normal": 112, "prehyper": 130},
          "diastolic_range": {"low": 46, "normal": 72, "prehyper": 80}},
    "5": {"systolic_range": {"low": 80, "normal": 112, "prehyper": 130},
          "diastolic_range": {"low": 46, "normal": 72, "prehyper": 80}},
    "6": {"systolic_range": {"low": 82, "normal": 115, "prehyper": 130},
          "diastolic_range": {"low": 57, "normal": 76, "prehyper": 80}},
    "7": {"systolic_range": {"low": 84, "normal": 115, "prehyper": 130},
          "diastolic_range": {"low": 57, "normal": 76, "prehyper": 80}},
    "8": {"systolic_range": {"low": 86, "normal": 115, "prehyper": 130},
          "diastolic_range": {"low": 57, "normal": 76, "prehyper": 80}},
    "9": {"systolic_range": {"low": 88, "normal": 115, "prehyper": 130},
          "diastolic_range": {"low": 57, "normal": 76, "prehyper": 80}},
    "10": {"systolic_range": {"low": 90, "normal": 120, "prehyper": 130},
           "diastolic_range": {"low": 61, "normal": 80, "prehyper": 90}},
    "11": {"systolic_range": {"low": 90, "normal": 120, "prehyper": 130},
           "diastolic_range": {"low": 61, "normal": 80, "prehyper": 90}},
    "12": {"systolic_range": {"low": 90, "normal": 130, "prehyper": 140},
           "diastolic_range": {"low": 64, "normal": 83, "prehyper": 90}},
    "13": {"systolic_range": {"low": 90, "normal": 130, "prehyper": 140},
           "diastolic_range": {"low": 64, "normal": 83, "prehyper": 90}},
    "14": {"systolic_range": {"low": 90, "normal": 130, "prehyper": 140},
           "diastolic_range": {"low": 64, "normal": 83, "prehyper": 90}},
    "15": {"systolic_range": {"low": 90, "normal": 130, "prehyper": 140},
           "diastolic_range": {"low": 64, "normal": 83, "prehyper": 90}},
    "16": {"systolic_range": {"low": 90, "normal": 120, "prehyper": 140},
           "diastolic_range": {"low": 60, "normal": 79, "prehyper": 90}},
    "17": {"systolic_range": {"low": 90, "normal": 120, "prehyper": 140},
           "diastolic_range": {"low": 60, "normal": 79, "prehyper": 90}},
    "18": {"systolic_range": {"low": 90, "normal": 120, "prehyper": 140},
           "diastolic_range": {"low": 60, "normal": 79, "prehyper": 90}},
    "19": {"systolic_range": {"low": 90, "normal": 120, "prehyper": 140},
           "diastolic_range": {"low": 60, "normal": 79, "prehyper": 90}},
    "20": {"systolic_range": {"low": 90, "normal": 120, "prehyper": 140},
           "diastolic_range": {"low": 60, "normal": 79, "prehyper": 90}},
    "21": {"systolic_range": {"low": 90, "normal": 130, "prehyper": 140},
           "diastolic_range": {"low": 60, "normal": 90, "prehyper": 100}},
    "22": {"systolic_range": {"low": 90, "normal": 130, "prehyper": 140},
           "diastolic_range": {"low": 60, "normal": 90, "prehyper": 100}},
    "23": {"systolic_range": {"low": 90, "normal": 130, "prehyper": 140},
           "diastolic_range": {"low": 60, "normal": 90, "prehyper": 100}},
    "24": {"systolic_range": {"low": 90, "normal": 130, "prehyper": 140},
           "diastolic_range": {"low": 60, "normal": 90, "prehyper": 100}},
    "25": {"systolic_range": {"low": 90, "normal": 130, "prehyper": 140},
           "diastolic_range": {"low": 60, "normal": 90, "prehyper": 100}},
    "26": {"systolic_range": {"low": 90, "normal": 130, "prehyper": 140},
           "diastolic_range": {"low": 60, "normal": 90, "prehyper": 100}},
    "27": {"systolic_range": {"low": 90, "normal": 130, "prehyper": 140},
           "diastolic_range": {"low": 60, "normal": 90, "prehyper": 100}},
    "28": {"systolic_range": {"low": 90, "normal": 130, "prehyper": 140},
           "diastolic_range": {"low": 60, "normal": 90, "prehyper": 100}},
    "29": {"systolic_range": {"low": 90, "normal": 130, "prehyper": 140},
           "diastolic_range": {"low": 60, "normal": 90, "prehyper": 100}},
    "30": {"systolic_range": {"low": 90, "normal": 130, "prehyper": 140},
           "diastolic_range": {"low": 60, "normal": 90, "prehyper": 100}},
    "31": {"systolic_range": {"low": 90, "normal": 130, "prehyper": 140},
           "diastolic_range": {"low": 60, "normal": 90, "prehyper": 100}},
    "32": {"systolic_range": {"low": 90, "normal": 130, "prehyper": 140},
           "diastolic_range": {"low": 60, "normal": 90, "prehyper": 100}},
    "33": {"systolic_range": {"low": 90, "normal": 130, "prehyper": 140},
           "diastolic_range": {"low": 60, "normal": 90, "prehyper": 100}},
    "34": {"systolic_range": {"low": 90, "normal": 130, "prehyper": 140},
           "diastolic_range": {"low": 60, "normal": 90, "prehyper": 100}},
    "35": {"systolic_range": {"low": 90, "normal": 130, "prehyper": 140},
           "diastolic_range": {"low": 60, "normal": 90, "prehyper": 100}},
    "36": {"systolic_range": {"low": 90, "normal": 130, "prehyper": 140},
           "diastolic_range": {"low": 60, "normal": 90, "prehyper": 100}},
    "37": {"systolic_range": {"low": 90, "normal": 130, "prehyper": 140},
           "diastolic_range": {"low": 60, "normal": 90, "prehyper": 100}},
    "38": {"systolic_range": {"low": 90, "normal": 130, "prehyper": 140},
           "diastolic_range": {"low": 60, "normal": 90, "prehyper": 100}},
    "39": {"systolic_range": {"low": 90, "normal": 130, "prehyper": 140},
           "diastolic_range": {"low": 60, "normal": 90, "prehyper": 100}},
    "40": {"systolic_range": {"low": 90, "normal": 130, "prehyper": 140},
           "diastolic_range": {"low": 60, "normal": 90, "prehyper": 100}},
    "other": {"systolic_range": {"low": 90, "normal": 140, "prehyper": 150},
              "diastolic_range": {"low": 60, "normal": 95, "prehyper": 100}},

}

PULSERANGE = {
    "3": {"low": 80, "normal": 120},
    "4": {"low": 80, "normal": 120},
    "5": {"low": 80, "normal": 120},
    "6": {"low": 75, "normal": 118},
    "7": {"low": 75, "normal": 118},
    "8": {"low": 75, "normal": 118},
    "9": {"low": 75, "normal": 118},
    "10": {"low": 75, "normal": 118},
    "11": {"low": 75, "normal": 118},
    "12": {"low": 60, "normal": 100},
    "13": {"low": 60, "normal": 100},
    "14": {"low": 60, "normal": 100},
    "15": {"low": 60, "normal": 100},
    "other": {"low": 60, "normal": 90}
}
TEMPERATURERANGE = {"low": 93, "normal": 99}
OXYGENSATRANGE = {"low": 90, "normal": 100}

MALEHYDRATIONRANGE = {"low": 50, "normal": 65}
FEMALEHYDRATIONRANGE = {"low": 45, "normal": 60}

FATRANGE = {
    "f": {
        "2": {"atheletic": 11, "good": 15, "acceptable": 20, "overweight": 24},
        "3": {"atheletic": 11, "good": 16, "acceptable": 20, "overweight": 24},
        "4": {"atheletic": 12, "good": 17, "acceptable": 21, "overweight": 25},
        "5": {"atheletic": 13, "good": 17, "acceptable": 21, "overweight": 25},
        "6": {"atheletic": 13, "good": 18, "acceptable": 22, "overweight": 26},
        "7": {"atheletic": 14, "good": 19, "acceptable": 24, "overweight": 28},
        "8": {"atheletic": 14, "good": 20, "acceptable": 25, "overweight": 29},
        "9": {"atheletic": 15, "good": 21, "acceptable": 26, "overweight": 30},
        "10": {"atheletic": 15, "good": 21, "acceptable": 27, "overweight": 31},
        "11": {"atheletic": 15, "good": 22, "acceptable": 28, "overweight": 32},
        "12": {"atheletic": 15, "good": 22, "acceptable": 28, "overweight": 32},
        "13": {"atheletic": 15, "good": 22, "acceptable": 28, "overweight": 32},
        "14": {"atheletic": 15, "good": 22, "acceptable": 29, "overweight": 33},
        "15": {"atheletic": 15, "good": 22, "acceptable": 29, "overweight": 33},
        "16": {"atheletic": 15, "good": 22, "acceptable": 29, "overweight": 33},
        "17": {"atheletic": 15, "good": 22, "acceptable": 29, "overweight": 34},
        "18": {"atheletic": 16, "good": 23, "acceptable": 30, "overweight": 35},
        "19": {"atheletic": 18, "good": 25, "acceptable": 31, "overweight": 36},
        "20": {"atheletic": 20, "good": 26, "acceptable": 32, "overweight": 38},
        "21": {"atheletic": 20, "good": 26, "acceptable": 32, "overweight": 38},
        "22": {"atheletic": 20, "good": 26, "acceptable": 32, "overweight": 38},
        "23": {"atheletic": 20, "good": 26, "acceptable": 32, "overweight": 38},
        "24": {"atheletic": 20, "good": 26, "acceptable": 32, "overweight": 38},
        "25": {"atheletic": 20, "good": 26, "acceptable": 32, "overweight": 38},
        "26": {"atheletic": 20, "good": 26, "acceptable": 32, "overweight": 38},
        "27": {"atheletic": 20, "good": 26, "acceptable": 32, "overweight": 38},
        "28": {"atheletic": 20, "good": 26, "acceptable": 32, "overweight": 38},
        "29": {"atheletic": 20, "good": 26, "acceptable": 32, "overweight": 38},
        "30": {"atheletic": 20, "good": 26, "acceptable": 32, "overweight": 38},
        "31": {"atheletic": 20, "good": 26, "acceptable": 32, "overweight": 38},
        "32": {"atheletic": 20, "good": 26, "acceptable": 32, "overweight": 38},
        "33": {"atheletic": 20, "good": 26, "acceptable": 32, "overweight": 38},
        "34": {"atheletic": 20, "good": 26, "acceptable": 32, "overweight": 38},
        "35": {"atheletic": 20, "good": 26, "acceptable": 32, "overweight": 38},
        "36": {"atheletic": 20, "good": 26, "acceptable": 32, "overweight": 38},
        "37": {"atheletic": 20, "good": 26, "acceptable": 32, "overweight": 38},
        "38": {"atheletic": 20, "good": 26, "acceptable": 32, "overweight": 38},
        "39": {"atheletic": 20, "good": 26, "acceptable": 32, "overweight": 38},
        "40": {"atheletic": 22, "good": 28, "acceptable": 33, "overweight": 39},
        "41": {"atheletic": 22, "good": 28, "acceptable": 33, "overweight": 39},
        "42": {"atheletic": 22, "good": 28, "acceptable": 33, "overweight": 39},
        "43": {"atheletic": 22, "good": 28, "acceptable": 33, "overweight": 39},
        "44": {"atheletic": 22, "good": 28, "acceptable": 33, "overweight": 39},
        "45": {"atheletic": 22, "good": 28, "acceptable": 33, "overweight": 39},
        "46": {"atheletic": 22, "good": 28, "acceptable": 33, "overweight": 39},
        "47": {"atheletic": 22, "good": 28, "acceptable": 33, "overweight": 39},
        "48": {"atheletic": 22, "good": 28, "acceptable": 33, "overweight": 39},
        "49": {"atheletic": 22, "good": 28, "acceptable": 33, "overweight": 39},
        "50": {"atheletic": 22, "good": 28, "acceptable": 33, "overweight": 39},
        "51": {"atheletic": 22, "good": 28, "acceptable": 33, "overweight": 39},
        "52": {"atheletic": 22, "good": 28, "acceptable": 33, "overweight": 39},
        "53": {"atheletic": 22, "good": 28, "acceptable": 33, "overweight": 39},
        "54": {"atheletic": 22, "good": 28, "acceptable": 33, "overweight": 39},
        "55": {"atheletic": 22, "good": 28, "acceptable": 33, "overweight": 39},
        "56": {"atheletic": 22, "good": 28, "acceptable": 33, "overweight": 39},
        "57": {"atheletic": 22, "good": 28, "acceptable": 33, "overweight": 39},
        "58": {"atheletic": 22, "good": 28, "acceptable": 33, "overweight": 39},
        "59": {"atheletic": 22, "good": 28, "acceptable": 33, "overweight": 39},
        "other": {"atheletic": 23, "good": 29, "acceptable": 35, "overweight": 41},
    },
    "m": {
        "2": {"atheletic": 12, "good": 10, "acceptable": 16, "overweight": 21},
        "3": {"atheletic": 12, "good": 10, "acceptable": 16, "overweight": 21},
        "4": {"atheletic": 12, "good": 10, "acceptable": 17, "overweight": 22},
        "5": {"atheletic": 11, "good": 15, "acceptable": 18, "overweight": 22},
        "6": {"atheletic": 11, "good": 15, "acceptable": 19, "overweight": 23},
        "7": {"atheletic": 12, "good": 16, "acceptable": 19, "overweight": 24},
        "8": {"atheletic": 12, "good": 16, "acceptable": 20, "overweight": 25},
        "9": {"atheletic": 12, "good": 17, "acceptable": 21, "overweight": 26},
        "10": {"atheletic": 12, "good": 17, "acceptable": 22, "overweight": 27},
        "11": {"atheletic": 12, "good": 17, "acceptable": 22, "overweight": 27},
        "12": {"atheletic": 11, "good": 17, "acceptable": 22, "overweight": 27},
        "13": {"atheletic": 11, "good": 16, "acceptable": 21, "overweight": 26},
        "14": {"atheletic": 10, "good": 15, "acceptable": 20, "overweight": 25},
        "15": {"atheletic": 9, "good": 15, "acceptable": 20, "overweight": 24},
        "16": {"atheletic": 9, "good": 14, "acceptable": 19, "overweight": 23},
        "17": {"atheletic": 9, "good": 14, "acceptable": 19, "overweight": 23},
        "18": {"atheletic": 9, "good": 15, "acceptable": 20, "overweight": 23},
        "19": {"atheletic": 8, "good": 16, "acceptable": 21, "overweight": 24},
        "20": {"atheletic": 8, "good": 17, "acceptable": 23, "overweight": 26},
        "21": {"atheletic": 8, "good": 17, "acceptable": 23, "overweight": 26},
        "22": {"atheletic": 8, "good": 17, "acceptable": 23, "overweight": 26},
        "23": {"atheletic": 8, "good": 17, "acceptable": 23, "overweight": 26},
        "24": {"atheletic": 8, "good": 17, "acceptable": 23, "overweight": 26},
        "25": {"atheletic": 12, "good": 20, "acceptable": 26, "overweight": 29},
        "26": {"atheletic": 12, "good": 20, "acceptable": 26, "overweight": 29},
        "27": {"atheletic": 12, "good": 20, "acceptable": 26, "overweight": 29},
        "28": {"atheletic": 12, "good": 20, "acceptable": 26, "overweight": 29},
        "29": {"atheletic": 12, "good": 20, "acceptable": 26, "overweight": 29},
        "30": {"atheletic": 12, "good": 20, "acceptable": 26, "overweight": 29},
        "31": {"atheletic": 12, "good": 20, "acceptable": 26, "overweight": 29},
        "32": {"atheletic": 12, "good": 20, "acceptable": 26, "overweight": 29},
        "33": {"atheletic": 12, "good": 20, "acceptable": 26, "overweight": 29},
        "34": {"atheletic": 12, "good": 20, "acceptable": 26, "overweight": 29},
        "35": {"atheletic": 12, "good": 20, "acceptable": 26, "overweight": 29},
        "36": {"atheletic": 12, "good": 20, "acceptable": 26, "overweight": 29},
        "37": {"atheletic": 12, "good": 20, "acceptable": 26, "overweight": 29},
        "38": {"atheletic": 12, "good": 20, "acceptable": 26, "overweight": 29},
        "39": {"atheletic": 12, "good": 20, "acceptable": 26, "overweight": 29},
        "40": {"atheletic": 13, "good": 23, "acceptable": 29, "overweight": 34},
        "41": {"atheletic": 13, "good": 23, "acceptable": 29, "overweight": 34},
        "42": {"atheletic": 13, "good": 23, "acceptable": 29, "overweight": 34},
        "43": {"atheletic": 13, "good": 23, "acceptable": 29, "overweight": 34},
        "44": {"atheletic": 13, "good": 23, "acceptable": 29, "overweight": 34},
        "45": {"atheletic": 13, "good": 23, "acceptable": 29, "overweight": 34},
        "46": {"atheletic": 13, "good": 23, "acceptable": 29, "overweight": 34},
        "47": {"atheletic": 13, "good": 23, "acceptable": 29, "overweight": 34},
        "48": {"atheletic": 13, "good": 23, "acceptable": 29, "overweight": 34},
        "49": {"atheletic": 13, "good": 23, "acceptable": 29, "overweight": 34},
        "50": {"atheletic": 13, "good": 23, "acceptable": 29, "overweight": 34},
        "51": {"atheletic": 13, "good": 23, "acceptable": 29, "overweight": 34},
        "52": {"atheletic": 13, "good": 23, "acceptable": 29, "overweight": 34},
        "53": {"atheletic": 13, "good": 23, "acceptable": 29, "overweight": 34},
        "54": {"atheletic": 13, "good": 23, "acceptable": 29, "overweight": 34},
        "55": {"atheletic": 13, "good": 23, "acceptable": 29, "overweight": 34},
        "56": {"atheletic": 13, "good": 23, "acceptable": 29, "overweight": 34},
        "57": {"atheletic": 13, "good": 23, "acceptable": 29, "overweight": 34},
        "58": {"atheletic": 13, "good": 23, "acceptable": 29, "overweight": 34},
        "59": {"atheletic": 13, "good": 23, "acceptable": 29, "overweight": 34},
        "other": {"atheletic": 15, "good": 27, "acceptable": 33, "overweight": 36},
    }
}

MALEBONEMASSRANGE = {"low": 3.4, "normal": 5}
FEMALEBONEMASSRANGE = {"low": 3.6, "normal": 4.8}

# MALEMUSCLERANGE = {"low": 33, "normal": 42}
# FEMALEMUSCLERANGE = {"low": 23, "normal": 32}

MUSCLERANGE = {
    "m": {
        "2": {"low": 10.8, "normal": 11.2},
        "3": {"low": 11.5, "normal": 12.7},
        "4": {"low": 13.1, "normal": 14.3},
        "5": {"low": 14.8, "normal": 18.8},
        "6": {"low": 16.0, "normal": 20.2},
        "7": {"low": 17.9, "normal": 22.3},
        "8": {"low": 19.2, "normal": 24.2},
        "9": {"low": 21.6, "normal": 27},
        "10": {"low": 23.4, "normal": 29.8},
        "11": {"low": 25.3, "normal": 33.1},
        "12": {"low": 28, "normal": 37.2},
        "13": {"low": 30.7, "normal": 41.1},
        "14": {"low": 36, "normal": 46},
        "15": {"low": 39, "normal": 50.4},
        "16": {"low": 41, "normal": 54.6},
        "17": {"low": 43.1, "normal": 56.5},
        "18": {"low": 43.1, "normal": 56.5}
    },
    "f": {
        "2": {"low": 9.6, "normal": 10.1},
        "3": {"low": 10.8, "normal": 11.7},
        "4": {"low": 12.3, "normal": 13.4},
        "5": {"low": 13.9, "normal": 18.1},
        "6": {"low": 14.49, "normal": 19.69},
        "7": {"low": 16.6, "normal": 21.4},
        "8": {"low": 17.8, "normal": 24.8},
        "9": {"low": 19.1, "normal": 26.3},
        "10": {"low": 20.7, "normal": 29.1},
        "11": {"low": 23.4, "normal": 32.4},
        "12": {"low": 25.6, "normal": 33.8},
        "13": {"low": 27.5, "normal": 35.9},
        "14": {"low": 28.8, "normal": 36.4},
        "15": {"low": 30.7, "normal": 37.5},
        "16": {"low": 30.7, "normal": 38.3},
        "17": {"low": 30.8, "normal": 42.2},
        "18": {"low": 30.8, "normal": 42.2}
    }
}

KCALBMRFORGOOD = 1.2
KCALBMRFORLOW = 1.375

MUSCLEQUALITYRANGE = {
    "m": {
        "18": {"low": 48, "normal": 73},
        "30": {"low": 46, "normal": 72},
        "40": {"low": 43, "normal": 69},
        "50": {"low": 38, "normal": 63},
        "60": {"low": 32, "normal": 55},
        "70": {"low": 24, "normal": 45},
        "80": {"low": 20, "normal": 38}
    },
    "f": {
        "18": {"low": 47, "normal": 67},
        "30": {"low": 47, "normal": 69},
        "40": {"low": 44, "normal": 68},
        "50": {"low": 40, "normal": 66},
        "60": {"low": 33, "normal": 60},
        "70": {"low": 25, "normal": 53},
        "80": {"low": 21, "normal": 49}
    }
}

VFATRANGE = {"good": 13, "high": 59}

SFATRANGE = {"m": {"low": 8.6, "good": 16.7}, "f": {"low": 18.5, "good": 26.7}}

class GLUCOSETESTTYPES():
    BEFOREBREAKFAST = 1
    AFTERBREAKFAST = 2
    BEFORELUNCH = 3
    AFTERLUNCH = 4
    BEFOREDINNER = 5
    AFTERDINNER = 6
    POSTMIDNIGHT = 7
    RANDOM = 8

BLOODGLUCOSERANGE = {
    GLUCOSETESTTYPES.RANDOM: {"low": 70, "normal": 140, "prediabetic": 180},
    GLUCOSETESTTYPES.BEFOREBREAKFAST: {"low": 70, "normal": 100, "prediabetic": 120},
    GLUCOSETESTTYPES.AFTERBREAKFAST: {"low": 70, "normal": 140, "prediabetic": 180},
    GLUCOSETESTTYPES.BEFORELUNCH: {"low": 70, "normal": 100, "prediabetic": 120},
    GLUCOSETESTTYPES.AFTERLUNCH: {"low": 70, "normal": 140, "prediabetic": 180},
    GLUCOSETESTTYPES.BEFOREDINNER: {"low": 70, "normal": 100, "prediabetic": 120},
    GLUCOSETESTTYPES.AFTERDINNER: {"low": 70, "normal": 140, "prediabetic": 180},
    GLUCOSETESTTYPES.POSTMIDNIGHT: {"low": 70, "normal": 100, "prediabetic": 120},

}


class ReportTypeConstants():
    BASICREPORT = 0
    PRESCRIPTION = 1


class HistoryTypeConstants():
    VITALSREPORT = 1
    PRESCRIPTIONREPORT = 2
    IMAGEREPORT = 3
    AUDIOREPORT = 4
    VIDEOREPORT = 5
    COMMENTREPORT = 6
    BOOKEDINVESTIGATION = 7


class BookedInvestigationConstant():
    BOOKED = 1
    INPROCESS = 2
    COMPLETED = 3
    CANCELLED = 4
    PRESCRIBED = 5
    SAMPLE_COLLECTED = 6


class HealthCaseConstant():
    NEW = 0
    ONGOING_PERSONRESPONDED = 1
    ONGOING_DOCTORRESPONDED = 2
    CLOSED = 3
    DISCARDED = 4
    REOPEN = 5
    VCBOOKED = 6

class VitalTypeConstant():  # for vital tracking
    BPMACHINE = 0
    OXIMETERMACHINE = 1
    TEMPERATUREMACHINE = 2
    WEIGHTMACHINE = 3
    BLOODGLUCOSEMACHINE = 4

class AppointmentTypeConstant():
    VC = 1
    INCLINIC = 2


class AppointmentStatusConstant():
    PENDING = 0
    STARTED = 1
    DROPPED = 2
    COMPLETED = 3


class PreferredLanguageConstant():
    ENGLISH = 0
    HINDI = 1
    GUJARATI = 2
    MARATHI = 3

class LeadConstant():
    NOSTATUS = 0
    OTPNOTVERIFIED = 1
    OTPVERIFIED = 2
    PERSONSELECTED = 3
    PERSONREGISTERED = 4
    PERSONENROLLED = 5
    DISCARD = 6
    CLOSE = 7

class SmsTemplateType():
    WELCOME = "WELCOME"
    NEWHEALTHCASEPATIENT = "NEWHEALTHCASEPATIENT"
    NEWHEALTHCASEHEALTHCHECKUP = "NEWHEALTHCASEHEALTHCHECKUP"
    NEWHEALTHCASEDOCTOR = "NEWHEALTHCASEDOCTOR"
    PATIENTRESPONSE = "PATIENTRESPONSE"
    DOCTORRESPONSE = "DOCTORRESPONSE"
    PRESCRIPTIONBYDOCTOR = "PRESCRIPTIONBYDOCTOR"
    VIDEOBYDOCTOR = "VIDEOBYDOCTOR"
    VIDEOBYPATIENT = "VIDEOBYPATIENT"
    APPOINTMENTBOOKED = "APPOINTMENTBOOKED"
    VIDEOCALLBOOKED = "VIDEOCALLBOOKED"
    PAYMENTREQUEST = "PAYMENTREQUEST"
    PROMOTION = "PROMOTION"




