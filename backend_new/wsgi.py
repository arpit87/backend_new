
import os
from backend_new import config
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "backend_new.settings")
os.environ.setdefault("GOOGLE_APPLICATION_CREDENTIALS", config.GCPSERVICEKEYPATH)

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
