from django.conf.urls import url
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddleware, AuthMiddlewareStack
from channels.security.websocket import AllowedHostsOriginValidator,OriginValidator

from Communications.exotel import ExotelCallConsumer
from QueueManagement.consumer import WaitingQueueConsumer , DoctorWaitingQueueConsumer
from VideoConference.consumer import VCConsumer
from PatientMonitor.consumer import PatientVitalsConsumer
from django.conf.urls import url
from django.urls import include, path, re_path
from django.contrib import admin
admin.autodiscover()

application = ProtocolTypeRouter({

    # WebSocket chat handler
      'websocket': AuthMiddlewareStack(
        URLRouter([
           url(r"^ws/queuemanagement/queue/(?P<deviceid>[\w.@+-]+)$", WaitingQueueConsumer),
           url(r"^ws/queuemanagement/doctorqueue/(?P<doctoruid>[\w.@+-]+)$", DoctorWaitingQueueConsumer),
           url(r"^ws/vc/doctoruid/(?P<doctoruid>[\w.@+-]+)$", VCConsumer),
           path("ws/audiocall/<str:doctoruid>/<str:healthcaseid>/", ExotelCallConsumer),
           url(r"^ws/patientmonitor/person/(?P<personuid>[\w.@+-]+)$", PatientVitalsConsumer),

        ])
        ),
})

