
import os
from backend_new import config
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

DEBUG = config.DEBUG
SECRET_KEY = '8lu*6g0lg)9z!ba+a$ehk)xt)x%rxgb$i1&amp;022shmi1jcgihb*'
LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Asia/Calcutta'

USE_I18N = True

USE_L10N = True

USE_TZ = True

ALLOWED_HOSTS = ['*']

CELERY_BROKER_URL = 'redis://localhost:6379'

#: Only add pickle to this list if your broker is secured
#: from unwanted access (see userguide/security.html)
CELERY_ACCEPT_CONTENT = ['pickle']
CELERY_RESULT_BACKEND = 'django-db'
CELERY_TASK_SERIALIZER = 'pickle'
CELERY_CACHE_BACKEND = 'django-cache'

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'import_export',
    'corsheaders',
    'Doctor',
    'Platform',
    'Person',
    'UploadFiles',
    'Communications',
    'Payment',
    'BodyVitals',
    'Utils',
    'HealthPackage',
    'HealthCase',
    'QueueManagement',
    'VideoConference',
    'Lab',
    'Appointments',
    'UrlShortner',
    'WebApp',
    'Marketing',
    'PatientMonitor',
    'channels',
    'django_celery_results',
    'django_celery_beat'
)

MIDDLEWARE = (
    'corsheaders.middleware.CorsMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware'
)

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

ROOT_URLCONF = 'backend_new.urls'
LOGIN_URL = '/api/super/'
DATA_UPLOAD_MAX_MEMORY_SIZE = 20242880
DEFAULT_CURRENCY = 'INR'
WSGI_APPLICATION = 'backend_new.wsgi.application'
ASGI_APPLICATION = 'backend_new.routing.application'

DATABASES={
#     'backendlogdb':{
# 'ENGINE': 'django.db.backends.postgresql_psycopg2',
# 'NAME': 'backendlogdb',
# 'USER': config.DATABASE_USER,
# 'PASSWORD': config.DATABASE_PASSWORD,
# 'HOST':config.DATABASE_HOST,
# 'PORT':config.DATABASE_PORT
#
# },
'default': {
'ENGINE': 'django.db.backends.postgresql_psycopg2',
'NAME': 'backenddb',
'USER': config.DATABASE_USER,
'PASSWORD': config.DATABASE_PASSWORD,
'HOST':config.DATABASE_HOST,
'PORT':config.DATABASE_PORT
}
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue'
        }
    },
    'formatters': {
        'simple': {
            'format': '[%(asctime)s] %(levelname)s %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S'
            },
        'verbose': {
            'format': '[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s',
        'datefmt': '%Y-%m-%d %H:%M:%S'
        },
        'django': {
            'format':'django: %(message)s',
        },
    },
    'handlers': {
    'console': {
            'level': 'INFO',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'error_file':{
            'level' : 'ERROR',
            'class' : 'logging.handlers.RotatingFileHandler',
            'filename' : os.path.join(BASE_DIR,  'logs/error.log'),
            'maxBytes': 1024*1024*50, # 50 MB
            'backupCount' : 7,
            'formatter': 'verbose',
        },
        'requests_file':{
            'level' : 'INFO',
            'class' : 'logging.handlers.RotatingFileHandler',
            'filename' : os.path.join(BASE_DIR,  'logs/requests.log'),
            'maxBytes': 1024*1024*50, # 50 MB
            'backupCount' : 7,
            'formatter': 'simple',
        },
        'backendlog_file': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_DIR,'logs','backendlog.log'),
            'maxBytes': 1024 * 1024 * 50,  # 50 MB
            'backupCount': 10,
        },
        'websocketlog_file': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_DIR, 'logs', 'websocketlog.log'),
            'maxBytes': 1024 * 1024 * 50,  # 50 MB
            'backupCount': 10,
        },
        'logging.handlers.SysLogHandler': {
                    'level': 'ERROR',
                    'class': 'logging.handlers.SysLogHandler',
                    'facility': 'local7',
                    'formatter': 'django',
                    'address' : '/dev/log',
                },
        'celery': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_DIR, 'logs', 'celery.log'),
            'maxBytes': 1024 * 1024 * 100,  # 100 mb
        },
    },
    'loggers': {
        'apirequests': {
        'handlers': ['requests_file'],
        'level': 'DEBUG',
        },
        'backendlog':{
            'handlers': ['backendlog_file'],
            'level': 'DEBUG',
        },
        'django': {
        'handlers': ['error_file','requests_file','backendlog_file'],
        'propagate': False,
        },
        'py.warnings': {
            'handlers': ['error_file', 'backendlog_file'],
        },
        'daphne': {
            'handlers': [
                'console', 'websocketlog_file'
            ],
            'level': 'DEBUG'
        },
        'websocket': {
            'handlers': [
                'websocketlog_file'
            ],
            'level': 'DEBUG'
        },
        'celery': {
            'handlers': ['celery'],
            'level': 'DEBUG',
            'propagate': True,
        },

    }
}


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_ROOT=os.path.join(BASE_DIR, 'static')
STATIC_URL = "/djangostatic/"
UPLOADEDFILES_DIR = 'uploadedfiles'
REPORTS_DIR = 'generatedreports'
REPORT_ICONS = os.path.abspath(os.path.join(BASE_DIR, 'reporticons'))
MEDIA_ROOT = os.path.join(BASE_DIR,'static',UPLOADEDFILES_DIR)

TEMPLATES = [{
    'BACKEND': 'django.template.backends.django.DjangoTemplates',
    'DIRS': [],
    'APP_DIRS': True,
    'OPTIONS': {
        'context_processors': [
            'django.template.context_processors.debug',
            'django.template.context_processors.request',
            'django.contrib.auth.context_processors.auth',
            'django.contrib.messages.context_processors.messages',
        ],
    },
}]

CSS_DIR = os.path.join(BASE_DIR,  'css')
CORS_ORIGIN_ALLOW_ALL = True
CORS_ORIGIN_WHITELIST = (
    # 'ionic://localhost',
    # 'capacitor://localhost',
    # 'http://localhost:8100',
    # 'localhost:8080',
    # 'localhost:4200',
    # 'healthatm.in:8080',
    # 'yolohealth.in:8080',

    # Made By Arnav.
    'ionic://localhost',
    'capacitor://localhost',
    'https://localhost:8100',
    'https://localhost:8080',
    'https://localhost:4200',
)

SESSION_COOKIE_SAMESITE =  None

CORS_ALLOW_CREDENTIALS = True

SESSION_COOKIE_AGE = 60*15   #seconds

EMAIL_USE_TLS = config.EMAIL_USE_TLS
EMAIL_HOST = config.EMAIL_HOST
EMAIL_HOST_USER = config.EMAIL_HOST_USER
EMAIL_PORT = config.EMAIL_PORT
EMAIL_HOST_PASSWORD = config.EMAIL_HOST_PASSWORD

CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [('127.0.0.1', 6379)],
        },
    },
}
