"""
ASGI entrypoint. Configures Django and then runs the application
defined in the ASGI_APPLICATION setting.
"""

import os
import django
from channels.routing import get_default_application
from backend_new import config

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "backend_new.settings")
os.environ.setdefault("GOOGLE_APPLICATION_CREDENTIALS", config.GCPSERVICEKEYPATH)

django.setup()
application = get_default_application()